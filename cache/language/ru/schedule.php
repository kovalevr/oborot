<?php
/**
 * Кэш языковых значений модулей
 * Файл генерируется автоматически.
 */

$aLang = array(
        'invalid_value' => 'Некорректное значение параметра "{name}"',
        'try_task_confirm' => 'Запустить сейчас?',
        'task_executed_error' => 'Ошибка при запуске задачи',
        'task_executed' => 'Задача была запушена',
        'error_execute' => 'Ошибка',
        'success_execute' => 'Успешно',
        'try_task' => 'Запустить',
        'status_paused' => 'временно остановлена',
        'status_stopped' => 'остановлена',
        'status_active' => 'активна',
        'target_system' => 'система в целом',
        'target_server' => 'сервер',
        'target_site' => 'площадка',
        'resource_critical' => 'критическая',
        'resource_normal' => 'обычная',
        'resource_intensive' => 'ресурсоемкая',
        'resource_background' => 'фоновая',
        'priority_high' => 'высокий',
        'priority_critical' => 'критический',
        'priority_normal' => 'обычный',
        'addTabName' => 'Добавление записи планировшика',
        'priority_low' => 'низкий',
        'addOk' => 'Запись добавлени',
        'editTabName' => 'Редактирование записи планировшика',
        'validateError' => 'Ошибка валидации:',
        'updateOk' => 'Запись обновлена',
        'add' => 'Добавление',
        'selectError' => 'Ошибка выбора записи',
        'edit' => 'Редактирование',
        'taskList' => 'Список задач',
        'c_dow' => 'День недели',
        'c_month' => 'Месяц',
        'c_day' => 'День',
        'c_hour' => 'Час',
        'c_min' => 'Минута',
        'status' => 'Статус',
        'target_area' => 'Область применения',
        'resource_use' => 'Ресурсоемкость',
        'priority' => 'Приоритет',
        'command' => 'Команда для старта',
        'name' => 'Имя задания',
        'title' => 'Название задания',
        'Schedule.Tool.tab_name' => 'Планировщик задач',
        'id' => 'ID задания',
    );

return $aLang;