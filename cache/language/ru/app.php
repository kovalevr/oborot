<?php
/**
 * Кэш языковых значений модулей
 * Файл генерируется автоматически.
 */

$aLang = array(
        'main' => 'Главная',
        'root' => 'Id корневого раздела для админки',
        '404' => '404',
        'topMenu' => 'Верхнее меню',
        'leftMenu' => 'Левое меню',
        'tools' => 'Служебные разделы',
        'serviceMenu' => 'Сервисное меню',
        'auth' => 'Авторизация',
        'lang_root' => 'Главный раздел языка',
        'landingPageTpl' => 'Шаблон Landing Page',
        'templates' => 'Шаблоны',
        'library' => 'Библиотеки',
        'tplNew' => 'Шаблон нового раздела',
        'profile' => 'Личный кабинет',
        'cart' => 'Корзина',
        'search' => 'Поиск',
        'sitemap' => 'Карта сайта',
        'subscribe' => 'Рассылка',
        'payment_success' => 'Успешная оплата',
        'payment_fail' => 'Отказ от платежа',
        'orderForm' => 'Форма заказа',
        'site_label' => 'название сайта',
        'site_label_description' => 'название сайта',
        'url_label' => 'адрес сайта',
        'url_label_description' => 'URL адрес сайта',
        'site_link' => 'ссылка сайта',
        'site_link_description' => 'ссылка на сайт',
        'site_type_info' => 'Информационный сайт',
        'site_type_catalog' => 'Каталог',
        'site_type_shop' => 'Интернет-магазин',
        'site_mode_cluster' => 'в кластере',
        'site_mode_cluster_own_files' => 'отцепленный',
        'site_mode_hosting' => 'сторонний хостинг',
    );

return $aLang;