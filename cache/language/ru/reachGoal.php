<?php
/**
 * Кэш языковых значений модулей
 * Файл генерируется автоматически.
 */

$aLang = array(
        'error_name_exists' => 'Такое имя события уже существует',
        'error_wrong_name' => 'Имя цели должно состоять из только из латинских букв, цифр и подчеркиваний',
        'used_in_' => 'Удаление невозможно! Цель используется в модулях: ',
        'target_exists' => 'Цель с таким идентификатором уже существует',
        'ys_counter_saved' => 'Счетчик для Яндекса сохранен [{0}]',
        'field_selector' => 'Селектор',
        'field_title_selector' => 'Наименование селектора',
        'field_yandex_target' => 'Событие Yandex',
        'btn_selectors' => 'События селекторов',
        'field_google_target' => 'Событие Google',
        'btn_add_selector' => 'Добавить селектор',
        'yaCounter' => 'Номер счетчика Яндекс',
        'btn_add_yandex' => 'Добавить Yandex',
        'btn_add_google' => 'Добавить Google',
        'btn_settings' => 'Настройки',
        'field_type' => 'Тип',
        'field_title' => 'Название',
        'field_category' => 'Категория',
        'field_name' => 'Идентификатор цели',
        'tab_name' => 'Цели',
    );

return $aLang;