<?php
/**
 * Кэш языковых значений модулей
 * Файл генерируется автоматически.
 */

$aLang = array(
        'error_image_invalid_format' => 'Ошибка при загрузке изображения: недопустимый формат файла!',
        'error_image_max_size' => 'Ошибка при загрузке изображения: файл превысил максимально допустимый размер!',
        'error_image_not_found' => 'Ошибка при загрузке изображения: файл не найден!',
        'error_invalid_file_format' => 'Недопустимый формат файла %s',
        'error_invalid_file_type' => 'Недопустимый тип файла %s',
        'error_invalid_format' => 'Файл имеет неверный формат %s',
        'error_invalid_min_size' => 'Изображение меньше допустимых размеров %s',
        'error_invalid_size' => 'Изображение превысило допустимые размеры %s',
        'error_no_image' => 'Файл не является изображением!',
        'error_not_required' => 'Файл не требуется.',
        'error_max_size' => 'Превышен максимально допустимый размер файла: %s',
        'error_upload_not_post' => 'Файл не был загружен через POST HTTP.',
        'delCntItems' => 'шт.',
        'error_upload' => 'Ошибка загрузки файла.',
        'delRowNoName' => 'Запись',
        'fileBrowserNoSelection' => ' Файл не выбран ',
        'fileBrowserSelect' => 'Выбрать',
        'fileBrowserFile' => 'Файл',
        'loadFile' => 'Загрузка файла',
        'loadFiles' => 'Загрузка файлов',
        'noDeleteFiles' => 'Ни одного файла не удалено!',
        'deleteFiles' => 'Удаление файлов',
        'deletingPro' => 'Удалено файлов: {0, number} из {1, number}',
        'noDelete' => 'Файл не удален!',
        'delete' => 'Файл удален.',
        'load' => 'Загрузить',
        'deleting' => 'Удаление файла',
        'select' => 'Выбрать',
        'noLoaded' => 'Ни одного файла не загружено!',
        'loadingPro' => 'Загружено {0, number} из {1, number}',
        'loadingError' => 'Ошибка загрузки данных',
        'filesList' => 'Список файлов',
        'serverPath' => 'Адрес на сервере',
        'ext' => 'Тип файла',
        'webPathShort' => 'Web адрес без хоста',
        'modifyDate' => 'Дата модификации',
        'webPath' => 'Web адрес',
        'name' => 'Имя файла',
        'size' => 'Размер',
        'Files.Adm.tab_name' => 'Файлы',
        'FileBrowser.Cms.tab_name' => 'Файловый менеджер',
        'fileBrowserPanelTitle' => 'Менеджер файлов',
    );

return $aLang;