<?php
/**
 * Кэш языковых значений модулей
 * Файл генерируется автоматически.
 */

$aLang = array(
        'detail_title' => 'Подробнее',
        'start' => 'Запустить',
        'back' => 'Назад',
        'field_datetime' => 'Время',
        'field_status' => 'Статус',
        'field_message' => 'Сообщения',
        'field_test_status' => 'Статус',
        'field_test_type' => 'Тип',
        'field_test_name' => 'Название теста',
        'status_error' => 'Ошибка',
        'status_fail' => 'Тест провалился',
        'status_not_changed' => 'Статус не изменился',
        'status_ok' => 'Пройден',
        'st_skip' => 'Пропущен',
        'st_fail' => 'Провалился',
        'st_error' => 'Ошибка',
        'st_warning' => 'Предупреждение',
        'st_undefined' => 'Не запущен',
        'st_ok' => 'Пройден',
        'tab_name' => 'Тестирование сайта',
    );

return $aLang;