<?php
/**
 * Кэш языковых значений модулей
 * Файл генерируется автоматически.
 */

$aLang = array(
        'Cache.Cms.tab_name' => 'Сброс кэша',
        'drop_cache_act' => 'Сбросить кэш',
        'drop_cache_text' => 'Кэш сброшен',
        'cache_flag_on' => 'Сбрасывать кэш',
        'cache_flag_off' => 'Не сбрасывать кэш',
    );

return $aLang;