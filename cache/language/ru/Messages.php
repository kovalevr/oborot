<?php
/**
 * Кэш языковых значений модулей
 * Файл генерируется автоматически.
 */

$aLang = array(
        'field_now' => 'Только что',
        'field_longago' => '{0} в {1}',
        'field_yesterday' => 'Вчера в {0}',
        'field_today' => 'Сегодня в {0}',
        'field_minutes' => '{0, number} {1} назад',
        'messages_3' => 'новых сообщения',
        'messages_2' => 'новое сообщение',
        'messages_1' => 'новых сообщений',
        'minutes_3' => 'минуты',
        'minutes_2' => 'минута',
        'minutes_1' => 'минут',
        'type_warning' => 'Предупреждение',
        'type_important' => 'Важное',
        'type_extra' => 'Экстренное',
        'type_default' => 'Обычное',
        'back' => 'Назад',
        'field_sendread' => 'Послать сообщение о прочтении',
        'field_send_id' => 'ID посылки',
        'field_date' => 'Дата',
        'field_new' => 'Новое',
        'field_status' => 'Статус',
        'field_text' => 'Текст',
        'field_title' => 'Тема',
        'field_id' => 'ID сообщения',
        'tab_name' => 'Сообщения',
        'Messages.Cms.tab_name' => 'Сообщения',
    );

return $aLang;