<?php
/**
 * Кэш языковых значений модулей
 * Файл генерируется автоматически.
 */

$aLang = array(
        'upd_css_successful' => 'CSS стили модуля {0}\\{1} успешно обновлены',
        'upddict_successful' => 'Словари модуля {0}\\{1} успешно обновлены',
        'updconf_successful' => 'Конфигурация модуля {0}\\{1} успешно обновлена',
        'reinstalled_successful' => 'Модуль {0}\\{1} успешно переустановлен',
        'remove_successful' => 'Модуль {0}\\{1} успешно удален',
        'install_successful' => 'Модуль {0}\\{1} успешно установлен',
        'upd_css_btn' => 'Обновить CSS',
        'upddict_btn' => 'Обновить словари',
        'upddict_confirm' => 'Вы действительно хотите обновить данные локализации модуля?',
        'updconf_btn' => 'Обновить настройки',
        'updconf_confirm' => 'Вы действительно хотите обновить данные конфигурации модуля?',
        'reinstall_btn' => 'Переустановить',
        'reinstall_confirm' => 'Вы действительно хотите полностью переустановить этот модуль? <br /> <span style="color: #cd3712; font-weight: bold;">Все данные модуля будут потеряны</span>',
        'remove_btn' => 'Удалить',
        'remove_confirm' => 'Вы действительно хотите удалить этот модуль? <br /> <span style="color: #cd3712; font-weight: bold;">Все данные модуля будут потеряны</span>',
        'install_btn' => 'Установить',
        'install_confirm' => 'Вы действительно хотите установить этот модуль?',
        'wrong_dependency' => '<span style="color: #cd3712;">{0}</span>',
        'wrong_dependency_header' => 'Зависимости не построены',
        'deptree_delim' => 'Зависимости',
        'list_view_btn' => 'Смотреть детальную информацию',
        'tolist_btn' => 'К списку',
        'module_status_notinstalled' => '<span style="color: #f0ad4e;">Не установлен</span>',
        'module_status_installed' => '<span style="color: #2b862e;">Установлен</span>',
        'field_status' => 'Статус',
        'field_version' => 'Версия',
        'field_layer' => 'Слой',
        'field_module_title' => 'Название модуля',
        'field_module_name' => 'Имя модуля',
        'layer_name_LandingAdm' => 'Админская часть (LandingPage)',
        'layer_name_LandingPage' => 'Публичная часть (LandingPage)',
        'layer_name_Design' => 'Дизайнерский режим',
        'layer_name_Catalog' => 'Каталог',
        'layer_name_Adm' => 'Система администрирования (Adm)',
        'layer_name_Cms' => 'Интерфейс системы администрирования (Cms)',
        'layer_name_Tool' => 'Панель управления (Tool)',
        'layer_name_Page' => 'Публичная часть (Page)',
        'tab_name' => 'Модули',
    );

return $aLang;