<?php
/**
 * Кэш языковых значений модулей
 * Файл генерируется автоматически.
 */

$aLang = array(
        'staticContent2' => 'Текст раздела 2',
        'staticContent' => 'Текст раздела',
        'titleModule' => 'Текстовый блок',
        'Text.Page.tab_name' => 'Текст',
    );

return $aLang;