<?php
/**
 * Кэш языковых значений модулей
 * Файл генерируется автоматически.
 */

$aLang = array(
        'execution_status' => 'Статус выполнения',
        'count_all_records' => 'Количество всех записей',
        'total_count_records_processed' => 'Всего обработано',
        'count_records_processed_for_iteration' => 'Обработано за итерацию',
        'sitemap_update_msg' => 'sitemap обновлен',
        'sitemap_update_error' => 'Ошибка обновления',
        'new_update_msg' => 'Запустите скрипт по новой, не все записи обновлены',
        'record_update' => 'Обновленно записей',
        'search_drop_index' => 'Поисковый индекс очищен',
        'view_error' => 'Логи ошибок',
        'view_access' => 'Логи доступа',
        'collision' => 'Обнаружены коллизии "{collisions}"',
        'rebuildLanguages' => 'Перестроить языковые значения',
        'SearchDropAll' => 'Удалить всё',
        'rebuildSitemap' => 'Перестроить sitemap',
        'reindex' => 'Обновить индекс',
        'resetActive' => 'Сброс метки поискового индекса',
        'reset_search_index' => 'Сброс поискового индекса',
        'optimize_db_text' => 'БД оптимизирована',
        'optimize_db' => 'Оптимизация БД',
        'search' => 'Поиск',
        'logs' => 'Логи',
        'clear_logs' => 'Очистить логи',
        'renew_act' => 'Обновить страницу',
        'tab_name' => 'Утилиты',
    );

return $aLang;