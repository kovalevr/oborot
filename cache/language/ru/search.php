<?php
/**
 * Кэш языковых значений модулей
 * Файл генерируется автоматически.
 */

$aLang = array(
        'hidePlaceHolder' => 'Скрывать текст placeholder при наведении',
        'countSearch' => 'Количество выводимых позиций поиска',
        'typeTpl' => 'Тип вывода товаров',
        'showSectionSelect' => 'Раздел для поиска',
        'showTypeSelect' => 'Критерии поиска',
        'catalogSearch' => 'Поиск по каталогу',
        'type_all' => 'Общий',
        'type_catalog' => 'Каталожный',
        'type_info' => 'Информационный',
        'search_type' => 'Тип поиска',
        'title_default_type' => 'Критерий поиска по умолчанию',
        'SearchSettings.Tool.tab_name' => 'Поиск',
        'all_site' => 'Весь сайт',
        'search_results' => 'Результаты поиска',
        'no_results' => 'Поиск не дал результатов! Уточните свой поисковый запрос, смените критерий поиска.',
        'placeholder_mini_form' => 'Поиск',
        'search' => 'Поиск',
        'required_fields' => 'обязательные для заполнения поля',
        'select_section' => 'выберите раздел для поиска по сайту',
        'search_section' => 'Раздел для поиска',
        'any_words' => 'по любому из слов',
        'phrase_criteria' => 'по точному соответствию фразе',
        'all_words' => 'по всем словам',
        'select_criteria' => 'выберите критерий поиска',
        'Search.Page.tab_name' => 'Поиск',
        'phrase' => 'Фраза для поиска',
        'criteria' => 'Критерий поиска',
        'Search.Cms.tab_name' => 'Панель поиска',
        'searchSubText' => 'Поиск работает при вводе 2 и более символов',
        'field_search_title' => 'Заголовок',
        'field_search_text' => 'Текст',
        'field_text' => 'Полный текст для поиска',
        'field_status' => 'Cостояние записи',
        'field_href' => 'Ссылка',
        'field_class_name' => 'Имя модуля',
        'field_object_id' => 'Идентификатор записи',
        'field_language' => 'Идентификатор языка',
        'field_section_id' => 'ID раздела',
        'field_use_in_search' => 'Флаг добавления в поисковый индекс',
        'field_priority' => 'Приоритет страницы для Sitemap',
        'field_frequency' => 'Частота обновления страницы для Sitemap',
        'field_use_in_sitemap' => 'Флаг использования в Sitemap',
        'field_modify_date' => 'Дата обновления записи',
    );

return $aLang;