<?php
/**
 * Кэш языковых значений модулей
 * Файл генерируется автоматически.
 */

$aLang = array(
        'ban_del_dict' => 'Запрет на удаление',
        'err_dict' => 'Ошибка в справочнике',
        'err_dict_cycle' => 'Зацикливание сущностей для справочника',
        'btn_back' => 'Назад',
        'field_f_def_value' => 'Значение по умолчанию',
        'btn_add_field' => 'Добавить поле',
        'field_f_editor' => 'Тип отображения',
        'field_f_link_id' => 'Сущность',
        'field_f_name' => 'Техническое имя',
        'field_f_title' => 'Имя поля',
        'head_card_name' => 'Карточка "{0}"',
        'title_new_field' => 'Добавление нового поля',
        'title_edit_field' => 'Редактирование поля справочника',
        'error_del_usage_dict' => 'Обнаружены следующие поля, использующие удаляемый справочник:',
        'error_field_cant_removed' => 'Это поле не может быть удалено',
        'error_not_selected' => 'Не выбран справочник для удаления',
        'error_name_bosy' => 'Справочник с таким именем уже существует',
        'error_noname_seted' => 'Не задано имя справочника',
        'error_row_not_found' => 'Не найдена запись',
        'error_dict_not_found' => 'Не найден справочник',
        'del' => 'Удалить',
        'add' => 'Добавить',
        'back' => 'Назад',
        'title' => 'Название',
        'struct' => 'Структура',
        'new_dict' => 'Создание нового справочника',
        'dict_panel_name' => 'Справочник: {0}',
        'dict_list_for_cat' => 'Список справочников',
        'create_dict' => 'Создать справочник',
        'system_name' => 'Техническое имя',
        'dict_sys_name' => 'Системное имя',
        'dict_name' => 'Имя справочника',
        'tab_name' => 'Справочники',
        'Dictionary.Tool.tab_name' => 'Справочники',
        'Dictionary.Catalog.tab_name' => 'Справочники',
    );

return $aLang;