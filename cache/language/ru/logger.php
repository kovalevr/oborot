<?php
/**
 * Кэш языковых значений модулей
 * Файл генерируется автоматически.
 */

$aLang = array(
        'field_description' => 'Подробное описание события',
        'field_external_id' => 'ID записи в общем хранилище + маркер была ли запись экспортирована',
        'field_proxy_ip' => 'IP пользователя (proxy)',
        'field_ip' => 'IP пользователя',
        'field_initiator' => 'ID пользователя, 0 - если система',
        'field_module_title' => 'Название модуля',
        'field_module' => 'Название модуля',
        'field_title' => 'Название события',
        'field_log_title' => 'Журнал событий',
        'field_log_type' => 'Журнал событий',
        'field_event_title' => 'Уровень события',
        'field_event_type' => 'Уровень события',
        'field_event_time' => 'Дата и время',
        'field_id' => 'Идентификатор записи',
        'type_debug' => 'Журнал отладки',
        'type_system' => 'Системный журнал',
        'type_cron' => 'Планировщик заданий',
        'type_user' => 'Действия пользователей',
        'level_notice' => 'Уведомление',
        'level_warning' => 'Предупреждение',
        'level_error' => 'Ошибка',
        'deleteLogs' => 'Очистить логи',
        'deleteLogsText' => 'Удалить все записи?',
        'level_critical' => 'Критический',
        'log_type' => 'Тип журнала',
        'date' => 'Дата',
        'event_type' => 'Уровень события',
        'user' => 'Пользователь',
        'module' => 'Название модуля',
        'detailPage' => 'Открыть детальную страницу',
        'tab_name' => 'Журнал событий',
    );

return $aLang;