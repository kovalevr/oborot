<?php
/**
 * Кэш языковых значений модулей
 * Файл генерируется автоматически.
 */

$aLang = array(
        'back' => 'Назад к покупкам',
        'Order.Tool.tab_name' => 'Заказы',
        'success_1_click' => 'Заказ успешно создан. Вы будете перемещены на страницу оплаты.',
        'head_mail_text' => 'Метки для замены:<br>
[{0}] - Название сайта<br>
[{1}] - Адрес сайта<br>
[link] - Ссылка на заказ<br>
[order_id] - Номер заказа<br>
[before_status] - Информация о заказе<br>
[after_status] - Старый статус<br>
[order_info] - Новый статус<br>
',
        'order_data' => 'Данные заказа',
        'your_order' => 'Ваш заказ',
        'new_order' => 'Новый заказ',
        'change_status' => 'Смена статуса',
        'mail_title_to_change_status_on_paid' => 'Заголовок для письма "Заказ оплачен" для администратора',
        'mail_title_to_change_status_on_app' => 'Заголовок для письма "Заказ ожидает оплаты"',
        'mail_title_to_change_status' => 'Заголовок письма смены статуса',
        'mail_title_to_admin' => 'Заголовок письма для админа',
        'mail_title_to_user' => 'Заголовок письма для пользователя',
        'mail_to_change_status_on_paid' => 'Письмо смены статуса на "Заказ оплачен" для администратора ',
        'mail_to_change_status_on_app' => 'Письмо смены статуса на "Заказ ожидает оплаты" ',
        'mail_to_change_status' => 'Письмо смены статуса',
        'mail_to_admin' => 'Письмо для админа',
        'mail_to_user' => 'Письмо для пользователя',
        'payable' => 'Сумма к оплате',
        'items_count' => 'Число товарных позиций',
        'total' => 'Итого',
        'current_measure' => 'шт.',
        'current_currency' => 'руб',
        'goods_info' => 'Описание товара',
        'payment_field_title' => 'Название',
        'payment_field_payment' => 'Тип оплаты',
        'button_label' => 'Оплатить',
        'order_description' => 'Заказ № {0, number}',
        'field_goods_id_order' => 'id Товара',
        'license_edit' => 'Лицензионное соглашение',
        'field_goods_price' => 'Цена',
        'field_goods_total' => 'Итого',
        'settings' => 'Настройки',
        'field_goods_count' => 'Количество',
        'field_goods_title' => 'Товары',
        'field_goods_title_edit' => 'Редактор заказа',
        'field_onpage_cms' => 'Максимальное число заказов на одной странице (cms)',
        'field_onpage_profile' => 'Максимальное число заказов на одной странице личного кабинета',
        'field_order_max_size' => 'Максимальное число позиций в одном заказе',
        'field_mailtext' => 'Редактор писем',
        'field_orderstatus_title' => 'Статусы',
        'valid_data_error' => 'Некорректные данные',
        'status_delete_error' => 'Статус является системным и не может быть удален!',
        'field_new_status' => 'Статус',
        'field_old_status' => 'Старый статус',
        'history_list' => 'История изменения статусов',
        'field_change_data' => 'Дата изменения',
        'order_number' => 'Номер заказа',
        'field_goods_article' => 'Артикул',
        'field_user_id' => 'User id',
        'field_notes' => 'Комментарии менеджера',
        'field_text' => 'Дополнительные пожелания',
        'status_title_lang' => 'Название статуса ({0})',
        'status_title' => 'Название статуса',
        'status_name' => 'Имя статуса',
        'field_status' => 'Статус',
        'field_payment' => 'Тип оплаты',
        'field_delivery' => 'Тип доставки',
        'field_postcode' => 'Почтовый индекс',
        'field_mail' => 'E-mail',
        'field_phone' => 'Телефон',
        'field_address' => 'Адрес доставки',
        'field_contact_face' => 'Контактное лицо',
        'field_date' => 'Дата',
        'field_id' => '№',
        'Order.Adm.tab_name' => 'Заказы',
        'close_link' => 'Продолжить покупки',
        'order' => 'Товаров',
        'add_error_count' => 'Достигнут максимальный предел позиций заказа!',
        'add_success2' => 'Перейти в корзину',
        'add_success1' => 'Товар добавлен в корзину',
        'add_to_card' => 'Добавление товара в корзину',
        'basket_empty' => 'Корзина пуста',
        'MiniCart.Page.tab_name' => 'Мини-корзина',
        'error_form' => 'Отсутствует необходимая форма',
        'msg_dell_all' => 'Вы действительно хотите удалить все товары?',
        'msg_count_gt_zero' => 'Количество должно быть целым положительным числом',
        'good_not_available' => 'Товар недоступен для покупки.<br>Пожалуйста, удалите его из корзины.',
        'to_main' => 'На главную',
        'message_pay2' => 'Заказ успешно отправлен! В ближайшее время с вами свяжется менеджер.',
        'message_pay' => 'Для оплаты нажмите кнопку ниже.',
        'cart_empty' => 'Корзина пуста',
        'delete' => 'Удалить',
        'clear' => 'Очистить',
        'sum' => 'Сумма',
        'article' => 'Артикул',
        'price' => 'Цена',
        'count' => 'Количество',
        'title' => 'Наименование',
        'photo' => 'Фото',
        'incor_email' => 'Некорректно заполнено поле',
        'accepts_the_licen_detail' => 'Подробнее',
        'tp_pay_3' => 'Безналичный расчет',
        'tp_pay_2' => 'Наличными',
        'tp_pay_1' => 'Оплата on-line',
        'paymentTypeList' => 'Варианты оплаты',
        'tp_deliv_3' => 'Транспортная компания',
        'tp_deliv_2' => 'Самовывоз',
        'tp_deliv_1' => 'Курьером',
        'text' => 'Дополнительные пожелания',
        'accepts_the_licen' => 'Я согласен с условиями лицензионного соглашения',
        'tp_pay' => 'Способ оплаты',
        'tp_deliv' => 'Способ доставки',
        'mail' => 'E-mail',
        'phone' => 'Телефон',
        'address' => 'Адрес доставки',
        'postcode' => 'Почтовый индекс',
        'person' => 'Контактное лицо',
        'back_to_cart' => 'Вернуться в корзину',
        'title_checkout' => 'Оформление заказа',
        'checkout' => 'Оформить заказ',
        'Cart.Page.tab_name' => 'Корзина',
    );

return $aLang;