<?php
/**
 * Кэш языковых значений модулей
 * Файл генерируется автоматически.
 */

$aLang = array(
        'field_type_int' => 'число',
        'field_type_float' => 'дробное число',
        'field_type_money' => 'деньги',
        'field_type_string' => 'строка',
        'field_type_text' => 'текст',
        'field_type_html' => 'html-редактор',
        'field_type_wyswyg' => 'wyswyg-редактор',
        'field_type_select' => 'справочник',
        'field_type_check' => 'галочка',
        'field_type_file' => 'файл',
        'field_type_gallery' => 'галерея',
        'field_type_date' => 'дата',
        'field_type_time' => 'время',
        'field_type_hide' => 'скрытое поле',
        'field_type_datetime' => 'Дата и время',
        'field_type_dictionary' => 'Справочник',
        'error_validator_set' => 'Поле должно быть задано',
        'error_validator_sys_name' => 'Допустимы английские буквы, цифры и символ подчеркивания',
        'error_validator_unique' => 'Запись с таким значением уже существует',
    );

return $aLang;