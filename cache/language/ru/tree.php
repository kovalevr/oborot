<?php
/**
 * Кэш языковых значений модулей
 * Файл генерируется автоматически.
 */

$aLang = array(
        'error_lang_root_delete' => 'Раздел нельзя удалить, так как он является главным для языковой ветки.<br>
Для удаления раздела удалите языковую ветку.',
        'error_can_not_create_alias' => 'Невозможно сгенерировать уникальный alias',
        'urlCollisionFlag' => 'Сработала защита от коллизий. Установлен alias: "{alias}"',
        'no_index_main' => 'Нельзя скрыть от индексации главную',
        'paramFormClose' => 'Закрыть',
        'paramFormSaveUpd' => 'Сохранить',
        'visibleHiddenFromIndex' => 'Скрыт от индексации',
        'visibleHiddenFromPath' => 'Скрыт из пути',
        'visibleVisible' => 'Видимый',
        'visibleHiddenFromMenu' => 'Скрыт из меню',
        'treeTitleVisible' => 'Видимость',
        'treeFormTitleTemplate' => 'Тип раздела',
        'treeFormTitleLink' => 'Ссылка',
        'treeFormTitleParent' => 'Родительский раздел',
        'treeFormTitleAlias' => 'Псевдоним',
        'treeFormTitleTitle' => 'Название раздела',
        'folder' => 'Папка',
        'changeSectionPosition' => 'Смена положения раздела',
        'treeFormHeaderUpd' => 'Редактирование раздела',
        'treeFormHeaderAdd' => 'Добавление раздела',
        'siteSettings' => 'Настройка сайта',
        'treeNewSection' => 'Новый раздел',
        'treeErrorParentNotSelected' => 'Не выбран раздел для добавления',
        'add' => 'Добавить',
        'treeErrorOnDelete' => 'Ошибка при удалении',
        'treeErrorNoParent' => 'При отображении раздела не найден родительский',
        'treePanelHeader' => 'Дерево разделов',
        'treeDelMsg' => 'С этим разделом будут удалены все ресурсы',
        'treeDelRow' => 'Удалить раздел "',
        'treeDelRowHeader' => 'Удаление раздела',
        'section_deleting' => 'Удаление раздела',
        'section_editing' => 'Редактирование раздела',
        'section_creating' => 'Создание раздела',
        'file_tree_title' => 'Файлы',
        'lib_tree_title' => 'Библиотеки',
        'main_tree_title' => 'Разделы',
        'tpl_tree_title' => 'Шаблоны',
        'tab_name' => 'Разделы',
    );

return $aLang;