<?php
/**
 * Кэш языковых значений модулей
 * Файл генерируется автоматически.
 */

$aLang = array(
        'robots_update_msg' => 'robots.txt обновлен',
        'robots_update_error' => 'Ошибка обновления',
        'rebuildRobots' => 'Перестроить файл',
        'subtext' => 'Не указывайте директивы "host" и "sitemap" - они формируются динамически.',
        'field_robots_content' => 'Текущее содержимое файла',
        'rebuild_warningtext' => 'Внимание! В файл были внесены изменения. Вы хотите перестроить robots.txt?',
        'RobotsTxt.Tool.tab_name' => 'Настройка robots.txt',
    );

return $aLang;