<?php
/**
 * Кэш языковых значений модулей
 * Файл генерируется автоматически.
 */

$aLang = array(
        'error_maxvalue_field_title' => 'Значение поля "{fieldName}" не может быть более {maxValue}',
        'section_all' => 'Раздел для ссылки "все статьи"',
        'all_section_link' => 'Все статьи',
        'author' => 'Автор',
        'param_on_page' => 'Статей на главной',
        'error_title' => 'Название не задано',
        'titleOnMain' => 'Заголовок статей для главной',
        'new_article' => 'Статья',
        'on_column' => 'Записей на главной и в колонке',
        'on_page' => 'Статей на странице',
        'title_on_main' => 'Заголовок статей на главной',
        'field_modifydate' => 'Дата последнего изменения',
        'field_source_link' => 'Источник',
        'field_hyperlink' => 'Ссылка',
        'field_on_main' => 'На главной',
        'field_active' => 'Активность',
        'field_fulltext' => 'Полный текст',
        'field_preview' => 'Анонс',
        'field_time' => 'Время публикации',
        'field_date' => 'Дата публикации',
        'field_title' => 'Название',
        'field_author' => 'Автор',
        'field_parent' => 'Родительский раздел',
        'field_alias' => 'Псевдоним',
        'field_id' => 'ID',
        'groups_articles' => 'Параметры вывода статей',
        'tab_name' => 'Статьи',
    );

return $aLang;