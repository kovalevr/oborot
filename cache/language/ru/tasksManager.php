<?php
/**
 * Кэш языковых значений модулей
 * Файл генерируется автоматически.
 */

$aLang = array(
        'clear_confirm' => 'Удалить все задачи из очереди?',
        'status_timeout' => 'Повисла',
        'status_close' => 'Закрыта',
        'status_complete' => 'Завершена',
        'status_wait' => 'В ожидании',
        'status_frozen' => 'Ждет выполнения',
        'status_interapt' => 'Прервана',
        'status_process' => 'В работе',
        'status_init' => 'Инициализирована',
        'invalid_parameters' => 'Ошибка при восстановлении конфигурации импорта. Попробуйте поменять тип task.parameters на mediumtext',
        'weight_critical' => 'Критический',
        'weight_high' => 'Высокий',
        'weight_normal' => 'Обычный',
        'weight_low' => 'Низкий',
        'priority_critical' => 'Критический',
        'priority_high' => 'Высокий',
        'priority_normal' => 'Обычный',
        'priority_low' => 'Низкий',
        'status_waiting_replay' => 'Ожидает повтора',
        'status_hang' => 'Зависла',
        'status_error' => 'Ошибка',
        'status_done' => 'Выполнена',
        'status_running' => 'Запущена',
        'status_new' => 'Новая',
        'status_put' => 'Ставится',
        'exec_counter' => 'Количество вызовов',
        'parameters' => 'Параметры',
        'class' => 'Класс',
        'status' => 'Статус',
        'mutex' => 'Мьютекс',
        'upd_time' => 'Время модификации',
        'target_area' => 'Область действия',
        'resource_use' => 'Ресурсоемкость',
        'priority' => 'Приоритет',
        'command' => 'Команда',
        'title' => 'Название задания',
        'global_id' => 'Глобальный id',
        'id' => 'id задания',
        'tasks' => 'Список процессов',
        'TasksManager.Tool.tab_name' => 'Менеджер процессов',
    );

return $aLang;