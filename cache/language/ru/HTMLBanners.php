<?php
/**
 * Кэш языковых значений модулей
 * Файл генерируется автоматически.
 */

$aLang = array(
        'bannerRight_title' => 'Баннеры справа',
        'bannerLeft_title' => 'Баннеры слева',
        'bannerContentTop_title' => 'Баннер (верх)',
        'new_banner' => 'Баннер',
        'bannerContentBottom_title' => 'Баннер (низ)',
        'position_bottom' => 'Низ контентной области',
        'position_right' => 'Правая колонка',
        'position_top' => 'Верх контентной области',
        'position_left' => 'Левая колонка',
        'deleteBanner' => 'Удаление баннера',
        'addBanner' => 'Создание баннера',
        'editBanner' => 'Редактирование баннера',
        'field_sort' => 'Порядок',
        'field_section' => 'Раздел для показа',
        'field_location_title' => 'Позиция',
        'field_location' => 'Позиция',
        'field_include' => 'На внутренних страницах',
        'field_allpages' => 'На всех страницах',
        'field_onmain' => 'На главной',
        'field_active' => 'Активность',
        'field_content' => 'Текст баннера',
        'field_title' => 'Название',
        'field_id' => 'Номер баннера',
        'tab_name' => 'Баннеры',
    );

return $aLang;