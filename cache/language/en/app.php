<?php
/**
 * Кэш языковых значений модулей
 * Файл генерируется автоматически.
 */

$aLang = array(
        'main' => 'Main',
        'root' => 'Id root partition to the admin',
        '404' => '404',
        'topMenu' => 'Top menu',
        'leftMenu' => 'Left menu',
        'tools' => 'Tools',
        'serviceMenu' => 'Service menu',
        'auth' => 'Auth',
        'lang_root' => 'Language root',
        'subscribe' => 'Subscribe',
        'landingPageTpl' => 'Template Landing Page',
        'templates' => 'templates',
        'library' => 'library',
        'tplNew' => 'Template the new partition',
        'profile' => 'My account',
        'cart' => 'Cart',
        'search' => 'Search',
        'sitemap' => 'Site Map',
        'payment_success' => 'Successful Payment',
        'payment_fail' => 'Waiver of payment',
        'orderForm' => 'Order Form',
        'site_label' => 'site name',
        'site_label_description' => 'site name',
        'url_label' => 'url',
        'url_label_description' => 'url',
        'site_link' => 'site link',
        'site_link_description' => 'site link',
        'site_type_info' => 'Info site',
        'site_type_catalog' => 'Catalog',
        'site_type_shop' => 'Shop',
        'site_mode_cluster' => 'in cluster',
        'site_mode_cluster_own_files' => 'own files',
        'site_mode_hosting' => 'on hosting',
    );

return $aLang;