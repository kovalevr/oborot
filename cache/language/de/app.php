<?php
/**
 * Кэш языковых значений модулей
 * Файл генерируется автоматически.
 */

$aLang = array(
        '404' => '404',
        'site_type_shop' => 'shop',
        'site_type_info' => 'info',
        'site_type_catalog' => 'catalog',
        'site_mode_hosting' => 'on hosting',
        'site_mode_cluster_own_files' => 'own_files',
        'site_mode_cluster' => 'cluster',
        'search' => 'Suche',
        'root' => 'Id des Stammabschnitts für CMS',
        'profile' => 'Account',
        'payment_success' => 'Zahlung erfolgreich durchgeführt',
        'payment_fail' => 'Zahlungsverweigerung',
        'orderForm' => 'Bestellformular',
        'main' => 'Startseite',
        'library' => 'Bibliotheken',
        'leftMenu' => 'Linkes Menü',
        'lang_root' => 'Stammabschnitt für Sprache',
        'cart' => 'Warenkorb',
        'landingPageTpl' => 'Landing Page Vorlage',
        'auth' => 'Anmeldung',
        'url_label_description' => 'URL der Website',
        'serviceMenu' => 'Service-Menü',
        'sitemap' => 'Sitemap',
        'site_label' => 'Webseitenname',
        'site_label_description' => 'Webseitenname',
        'subscribe' => 'Abonnieren',
        'templates' => 'Vorlagen',
        'tools' => 'Service Abschnitte',
        'topMenu' => 'oberes Menü',
        'tplNew' => 'Vorlage des neuen Abschnitts',
        'url_label' => 'website adresse',
        'site_link' => 'website link',
        'site_link_description' => ' Link zur Website',
    );

return $aLang;