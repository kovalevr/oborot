<?php

/* historyList.twig */
class __TwigTemplate_8e3f9400a677029587d6ba30f3b43dac extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        // line 1
        echo "<h2></h2>
<table style=\"border-top: 1px solid #dfe9f5; width: 800px;\">
    <tr>
        <th style=\"padding: 10px; text-align: left; background-color: #dfe9f5; font-weight: bold; border: 1px solid #dfe9f5;\">";
        // line 4
        echo $this->getAttribute((isset($context["Lang"]) ? $context["Lang"] : null), "get", array(0 => "order.field_change_data"), "method");
        echo "</th>
        <th style=\"text-align: right; background-color: #dfe9f5; font-weight: bold; border: 1px solid #dfe9f5;\">";
        // line 5
        echo $this->getAttribute((isset($context["Lang"]) ? $context["Lang"] : null), "get", array(0 => "order.field_new_status"), "method");
        echo "</th>
    </tr>
    ";
        // line 7
        $context['_parent'] = (array) $context;
        $context['_seq'] = twig_ensure_traversable((isset($context["historyList"]) ? $context["historyList"] : null));
        foreach ($context['_seq'] as $context["_key"] => $context["item"]) {
            // line 8
            echo "        <tr>
            <td style=\"padding: 10px; font-size: 14px; text-align: left; border-bottom: 1px solid #dfe9f5;\">";
            // line 9
            echo $this->getAttribute((isset($context["item"]) ? $context["item"] : null), "change_date");
            echo "</td>
            <td style=\"font-size: 14px; text-align: right; border-bottom: 1px solid #dfe9f5;\">";
            // line 10
            echo $this->getAttribute((isset($context["item"]) ? $context["item"] : null), "title_new_status");
            echo "</td>
        </tr>
    ";
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['_key'], $context['item'], $context['_parent'], $context['loop']);
        $context = array_intersect_key($context, $_parent) + $_parent;
        // line 13
        echo "</table>";
    }

    public function getTemplateName()
    {
        return "historyList.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  53 => 13,  44 => 10,  40 => 9,  37 => 8,  33 => 7,  28 => 5,  24 => 4,  19 => 1,);
    }
}
