<?php

/* fancy_panel.twig */
class __TwigTemplate_ab07ede635c99f99bb8da962765e42a2 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        // line 1
        if ((!(isset($context["errorCount"]) ? $context["errorCount"] : null))) {
            // line 2
            echo "    <div class=\"b-editor b-editor--nomar js_cart_fancy_add\">
        <div class=\"b-basket-notice\">
            <div class=\"basket-title\">";
            // line 4
            echo $this->getAttribute((isset($context["Lang"]) ? $context["Lang"] : null), "get", array(0 => "order.add_success1"), "method");
            echo "</div>
            <div class=\"js-basket-name basket-name\">";
            // line 5
            echo (isset($context["title"]) ? $context["title"] : null);
            echo "</div>
            <div class=\"basket-btn\"><a class=\"b-btnbox b-btnboxfull\" href=\"";
            // line 6
            echo (isset($context["cartSectionLink"]) ? $context["cartSectionLink"] : null);
            echo "\">";
            echo $this->getAttribute((isset($context["Lang"]) ? $context["Lang"] : null), "get", array(0 => "order.add_success2"), "method");
            echo "</a></div>
            <div class=\"basket-link\"><a class=\"js-basket-close\" href=\"#\">";
            // line 7
            echo $this->getAttribute((isset($context["Lang"]) ? $context["Lang"] : null), "get", array(0 => "order.close_link"), "method");
            echo "</a></div>
        </div>
    </div>
";
        } else {
            // line 11
            echo "    <div class=\"b-editor js_cart_fancy_error\">
        <div class=\"b-basket-notice\">
            <div class=\"basket-title\">";
            // line 13
            echo $this->getAttribute((isset($context["Lang"]) ? $context["Lang"] : null), "get", array(0 => "order.add_error_count"), "method");
            echo "</div>
            <br>
            <div class=\"basket-btn\"><a class=\"b-btnbox b-btnboxfull\" href=\"";
            // line 15
            echo (isset($context["cartSectionLink"]) ? $context["cartSectionLink"] : null);
            echo "\">";
            echo $this->getAttribute((isset($context["Lang"]) ? $context["Lang"] : null), "get", array(0 => "order.add_success2"), "method");
            echo "</a></div>
        </div>
    </div>
";
        }
    }

    public function getTemplateName()
    {
        return "fancy_panel.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  55 => 15,  50 => 13,  46 => 11,  39 => 7,  33 => 6,  29 => 5,  25 => 4,  21 => 2,  19 => 1,);
    }
}
