<?php

/* paginator.twig */
class __TwigTemplate_ab2f6de5783f41eb27f65e12c823205e extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        // line 1
        if (((twig_length_filter($this->env, $this->getAttribute((isset($context["aPages"]) ? $context["aPages"] : null), "items")) > 1) || ($this->getAttribute((isset($context["aPages"]) ? $context["aPages"] : null), "page") > 1))) {
            // line 2
            echo "
    ";
            // line 3
            if ($this->getAttribute((isset($context["aPages"]) ? $context["aPages"] : null), "parameters")) {
                // line 4
                echo "        ";
                $context["PageParams"] = ("&" . $this->getAttribute((isset($context["aPages"]) ? $context["aPages"] : null), "parameters"));
                // line 5
                echo "    ";
            } else {
                // line 6
                echo "        ";
                $context["PageParams"] = "";
                // line 7
                echo "    ";
            }
            // line 8
            echo "
    <div class=\"b-pageline\">

        ";
            // line 11
            if ($this->getAttribute((isset($context["aPages"]) ? $context["aPages"] : null), "firstPage")) {
                // line 12
                echo "            ";
                if ($this->getAttribute($this->getAttribute((isset($context["aPages"]) ? $context["aPages"] : null), "firstPage"), "isActive")) {
                    // line 13
                    echo "                <a class=\"pageline__back1\" href=\"[";
                    echo $this->getAttribute((isset($context["aPages"]) ? $context["aPages"] : null), "sectionId");
                    echo "]";
                    if ($this->getAttribute((isset($context["aPages"]) ? $context["aPages"] : null), "parameters")) {
                        echo "[";
                        echo $this->getAttribute((isset($context["aPages"]) ? $context["aPages"] : null), "module");
                        echo "?";
                        echo $this->getAttribute((isset($context["aPages"]) ? $context["aPages"] : null), "parameters");
                        echo "]";
                    }
                    echo "\">";
                    echo $this->getAttribute($this->getAttribute((isset($context["aPages"]) ? $context["aPages"] : null), "firstPage"), "title");
                    echo "</a>
            ";
                }
                // line 15
                echo "        ";
            }
            // line 16
            echo "
        ";
            // line 17
            if ($this->getAttribute((isset($context["aPages"]) ? $context["aPages"] : null), "prevGroup")) {
                // line 18
                echo "            ";
                if ($this->getAttribute($this->getAttribute((isset($context["aPages"]) ? $context["aPages"] : null), "prevGroup"), "isActive")) {
                    // line 19
                    echo "                <a class=\"pageline__back1\" href=\"[";
                    echo $this->getAttribute((isset($context["aPages"]) ? $context["aPages"] : null), "sectionId");
                    echo "][";
                    echo $this->getAttribute((isset($context["aPages"]) ? $context["aPages"] : null), "module");
                    echo "?page=";
                    echo $this->getAttribute($this->getAttribute((isset($context["aPages"]) ? $context["aPages"] : null), "prevGroup"), "page");
                    echo (isset($context["PageParams"]) ? $context["PageParams"] : null);
                    echo "]\">";
                    echo $this->getAttribute($this->getAttribute((isset($context["aPages"]) ? $context["aPages"] : null), "prevGroup"), "title");
                    echo "</a>
            ";
                }
                // line 21
                echo "        ";
            }
            // line 22
            echo "
        ";
            // line 23
            if ($this->getAttribute((isset($context["aPages"]) ? $context["aPages"] : null), "prevPage")) {
                // line 24
                echo "            ";
                if ($this->getAttribute($this->getAttribute((isset($context["aPages"]) ? $context["aPages"] : null), "prevPage"), "isActive")) {
                    // line 25
                    echo "                <a class=\"pageline__back2\" href=\"[";
                    echo $this->getAttribute((isset($context["aPages"]) ? $context["aPages"] : null), "sectionId");
                    echo "]";
                    if (($this->getAttribute($this->getAttribute((isset($context["aPages"]) ? $context["aPages"] : null), "prevPage"), "page") > 1)) {
                        echo "[";
                        echo $this->getAttribute((isset($context["aPages"]) ? $context["aPages"] : null), "module");
                        echo "?page=";
                        echo $this->getAttribute($this->getAttribute((isset($context["aPages"]) ? $context["aPages"] : null), "prevPage"), "page");
                        echo (isset($context["PageParams"]) ? $context["PageParams"] : null);
                        echo "]";
                    } elseif ($this->getAttribute((isset($context["aPages"]) ? $context["aPages"] : null), "parameters")) {
                        echo "[";
                        echo $this->getAttribute((isset($context["aPages"]) ? $context["aPages"] : null), "module");
                        echo "?";
                        echo $this->getAttribute((isset($context["aPages"]) ? $context["aPages"] : null), "parameters");
                        echo "]";
                    }
                    echo "\">";
                    echo $this->getAttribute($this->getAttribute((isset($context["aPages"]) ? $context["aPages"] : null), "prevPage"), "title");
                    echo "</a>
            ";
                }
                // line 27
                echo "        ";
            }
            // line 28
            echo "
        ";
            // line 29
            if ($this->getAttribute((isset($context["aPages"]) ? $context["aPages"] : null), "itemsIsActive")) {
                // line 30
                echo "            ";
                $context['_parent'] = (array) $context;
                $context['_seq'] = twig_ensure_traversable($this->getAttribute((isset($context["aPages"]) ? $context["aPages"] : null), "items"));
                foreach ($context['_seq'] as $context["_key"] => $context["page"]) {
                    // line 31
                    echo "                ";
                    if ($this->getAttribute((isset($context["page"]) ? $context["page"] : null), "isActive")) {
                        // line 32
                        echo "                    <a class=\"pageline__on\" href=\"\">";
                        echo $this->getAttribute((isset($context["page"]) ? $context["page"] : null), "title");
                        echo "</a>
                ";
                    } else {
                        // line 34
                        echo "                    <a href=\"[";
                        echo $this->getAttribute((isset($context["aPages"]) ? $context["aPages"] : null), "sectionId");
                        echo "]";
                        if (($this->getAttribute((isset($context["page"]) ? $context["page"] : null), "page") > 1)) {
                            echo "[";
                            echo $this->getAttribute((isset($context["aPages"]) ? $context["aPages"] : null), "module");
                            echo "?page=";
                            echo $this->getAttribute((isset($context["page"]) ? $context["page"] : null), "page");
                            echo (isset($context["PageParams"]) ? $context["PageParams"] : null);
                            echo "]";
                        } elseif ($this->getAttribute((isset($context["aPages"]) ? $context["aPages"] : null), "parameters")) {
                            echo "[";
                            echo $this->getAttribute((isset($context["aPages"]) ? $context["aPages"] : null), "module");
                            echo "?";
                            echo $this->getAttribute((isset($context["aPages"]) ? $context["aPages"] : null), "parameters");
                            echo "]";
                        }
                        echo "\">";
                        echo $this->getAttribute((isset($context["page"]) ? $context["page"] : null), "title");
                        echo "</a>
                ";
                    }
                    // line 36
                    echo "            ";
                }
                $_parent = $context['_parent'];
                unset($context['_seq'], $context['_iterated'], $context['_key'], $context['page'], $context['_parent'], $context['loop']);
                $context = array_intersect_key($context, $_parent) + $_parent;
                // line 37
                echo "        ";
            }
            // line 38
            echo "
        ";
            // line 39
            if ($this->getAttribute((isset($context["aPages"]) ? $context["aPages"] : null), "nextPage")) {
                // line 40
                echo "            ";
                if ($this->getAttribute($this->getAttribute((isset($context["aPages"]) ? $context["aPages"] : null), "nextPage"), "isActive")) {
                    // line 41
                    echo "                <a class=\"pageline__next2\" href=\"[";
                    echo $this->getAttribute((isset($context["aPages"]) ? $context["aPages"] : null), "sectionId");
                    echo "][";
                    echo $this->getAttribute((isset($context["aPages"]) ? $context["aPages"] : null), "module");
                    echo "?page=";
                    echo $this->getAttribute($this->getAttribute((isset($context["aPages"]) ? $context["aPages"] : null), "nextPage"), "page");
                    echo (isset($context["PageParams"]) ? $context["PageParams"] : null);
                    echo "]\">";
                    echo $this->getAttribute($this->getAttribute((isset($context["aPages"]) ? $context["aPages"] : null), "nextPage"), "title");
                    echo "</a>
            ";
                }
                // line 43
                echo "        ";
            }
            // line 44
            echo "
        ";
            // line 45
            if ($this->getAttribute((isset($context["aPages"]) ? $context["aPages"] : null), "nextGroup")) {
                // line 46
                echo "            ";
                if ($this->getAttribute($this->getAttribute((isset($context["aPages"]) ? $context["aPages"] : null), "nextGroup"), "isActive")) {
                    // line 47
                    echo "                <a class=\"pageline__next1\" href=\"[";
                    echo $this->getAttribute((isset($context["aPages"]) ? $context["aPages"] : null), "sectionId");
                    echo "][";
                    echo $this->getAttribute((isset($context["aPages"]) ? $context["aPages"] : null), "module");
                    echo "?page=";
                    echo $this->getAttribute($this->getAttribute((isset($context["aPages"]) ? $context["aPages"] : null), "nextGroup"), "page");
                    echo (isset($context["PageParams"]) ? $context["PageParams"] : null);
                    echo "]\">";
                    echo $this->getAttribute($this->getAttribute((isset($context["aPages"]) ? $context["aPages"] : null), "nextGroup"), "title");
                    echo "</a>
            ";
                }
                // line 49
                echo "        ";
            }
            // line 50
            echo "
        ";
            // line 51
            if ($this->getAttribute((isset($context["aPages"]) ? $context["aPages"] : null), "lastPage")) {
                // line 52
                echo "            ";
                if ($this->getAttribute($this->getAttribute((isset($context["aPages"]) ? $context["aPages"] : null), "lastPage"), "isActive")) {
                    // line 53
                    echo "                <a class=\"pageline__next1\" href=\"[";
                    echo $this->getAttribute((isset($context["aPages"]) ? $context["aPages"] : null), "sectionId");
                    echo "][";
                    echo $this->getAttribute((isset($context["aPages"]) ? $context["aPages"] : null), "module");
                    echo "?page=";
                    echo $this->getAttribute($this->getAttribute((isset($context["aPages"]) ? $context["aPages"] : null), "lastPage"), "page");
                    echo (isset($context["PageParams"]) ? $context["PageParams"] : null);
                    echo "]\">";
                    echo $this->getAttribute($this->getAttribute((isset($context["aPages"]) ? $context["aPages"] : null), "lastPage"), "title");
                    echo "</a>
            ";
                }
                // line 55
                echo "        ";
            }
            // line 56
            echo "
    </div>
";
        }
    }

    public function getTemplateName()
    {
        return "paginator.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  248 => 56,  245 => 55,  232 => 53,  229 => 52,  227 => 51,  224 => 50,  205 => 46,  203 => 45,  197 => 43,  181 => 40,  179 => 39,  176 => 38,  173 => 37,  167 => 36,  144 => 34,  138 => 32,  135 => 31,  130 => 30,  128 => 29,  125 => 28,  122 => 27,  99 => 25,  96 => 24,  91 => 22,  72 => 18,  70 => 17,  67 => 16,  64 => 15,  48 => 13,  45 => 12,  43 => 11,  38 => 8,  24 => 3,  330 => 75,  324 => 71,  306 => 69,  301 => 68,  298 => 67,  292 => 65,  290 => 64,  283 => 59,  273 => 57,  271 => 56,  267 => 54,  264 => 53,  260 => 51,  254 => 50,  249 => 48,  246 => 47,  240 => 46,  237 => 45,  234 => 44,  228 => 43,  221 => 49,  215 => 41,  212 => 40,  208 => 47,  200 => 44,  191 => 36,  184 => 41,  182 => 32,  175 => 31,  172 => 30,  169 => 29,  164 => 28,  155 => 27,  145 => 26,  141 => 25,  117 => 24,  112 => 21,  106 => 19,  103 => 18,  97 => 16,  94 => 23,  88 => 21,  86 => 12,  82 => 10,  55 => 9,  41 => 8,  35 => 7,  29 => 5,  26 => 4,  79 => 22,  75 => 19,  71 => 20,  63 => 15,  58 => 14,  52 => 12,  49 => 11,  36 => 9,  32 => 6,  27 => 6,  34 => 7,  30 => 4,  28 => 3,  21 => 2,  19 => 1,);
    }
}
