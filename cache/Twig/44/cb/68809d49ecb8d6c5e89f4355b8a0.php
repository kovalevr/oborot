<?php

/* /el/select.twig */
class __TwigTemplate_44cb68809d49ecb8d6c5e89f4355b8a0 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        // line 1
        $context["sClassField"] = $this->getAttribute((isset($context["oField"]) ? $context["oField"] : null), "getVal", array(0 => "label_position"), "method");
        // line 2
        echo "<div class=\"form__col-";
        echo $this->getAttribute((isset($context["oField"]) ? $context["oField"] : null), "getClassVal", array(), "method");
        echo "\">
    <div class=\"form__item form__item--label-";
        // line 3
        echo $this->getAttribute((isset($context["oField"]) ? $context["oField"] : null), "getVal", array(0 => "label_position"), "method");
        echo "\">
        ";
        // line 4
        if (($this->getAttribute((isset($context["oField"]) ? $context["oField"] : null), "getVal", array(0 => "group"), "method") == "0")) {
            // line 5
            echo "            ";
            if (($this->getAttribute((isset($context["oField"]) ? $context["oField"] : null), "getVal", array(0 => "label_position"), "method") != "right")) {
                // line 6
                echo "                <div class=\"form__label\">";
                echo $this->getAttribute((isset($context["oField"]) ? $context["oField"] : null), "getVal", array(0 => "param_title"), "method");
                if (($this->getAttribute((isset($context["oField"]) ? $context["oField"] : null), "getVal", array(0 => "param_required"), "method") == 1)) {
                    echo " <ins class=\"form__mark\">*</ins>";
                }
                echo "</div>
            ";
            }
            // line 8
            echo "        ";
        }
        // line 9
        echo "        <div class=\"form__input form__input--select\">
            <select data-hash=\"";
        // line 10
        echo (isset($context["formHash"]) ? $context["formHash"] : null);
        echo "\" data-name=\"";
        echo $this->getAttribute((isset($context["oField"]) ? $context["oField"] : null), "getVal", array(0 => "param_title"), "method");
        echo "\" class=\"js-form-select\" id=\"";
        echo $this->getAttribute((isset($context["oField"]) ? $context["oField"] : null), "getVal", array(0 => "param_name"), "method");
        echo "_";
        echo (isset($context["formHash"]) ? $context["formHash"] : null);
        echo "\" name=\"";
        echo $this->getAttribute((isset($context["oField"]) ? $context["oField"] : null), "getVal", array(0 => "param_name"), "method");
        echo "\"  ";
        echo $this->getAttribute((isset($context["oField"]) ? $context["oField"] : null), "getVal", array(0 => "param_man_params"), "method");
        echo " data-placeholder=\"";
        echo $this->getAttribute((isset($context["oField"]) ? $context["oField"] : null), "getVal", array(0 => "param_title"), "method");
        echo "\" ";
        echo $this->getAttribute((isset($context["oField"]) ? $context["oField"] : null), "getVal", array(0 => "param_man_params"), "method");
        echo ">
                ";
        // line 11
        $context["aItems"] = $this->getAttribute((isset($context["oField"]) ? $context["oField"] : null), "getViewItems", array(), "method");
        // line 12
        echo "                ";
        if (((isset($context["sClassField"]) ? $context["sClassField"] : null) == "none")) {
            // line 13
            echo "                    <option selected disabled>";
            echo $this->getAttribute((isset($context["oField"]) ? $context["oField"] : null), "getVal", array(0 => "param_title"), "method");
            echo "</option>
                ";
        }
        // line 15
        echo "                ";
        $context['_parent'] = (array) $context;
        $context['_seq'] = twig_ensure_traversable((isset($context["aItems"]) ? $context["aItems"] : null));
        foreach ($context['_seq'] as $context["_key"] => $context["aItem"]) {
            // line 16
            echo "                    <option value=\"";
            echo $this->getAttribute((isset($context["aItem"]) ? $context["aItem"] : null), "value");
            echo "\">";
            echo $this->getAttribute((isset($context["aItem"]) ? $context["aItem"] : null), "title");
            echo "</option>
                ";
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['_key'], $context['aItem'], $context['_parent'], $context['loop']);
        $context = array_intersect_key($context, $_parent) + $_parent;
        // line 18
        echo "            </select>
        </div>
        ";
        // line 20
        if (($this->getAttribute((isset($context["oField"]) ? $context["oField"] : null), "getVal", array(0 => "label_position"), "method") == "right")) {
            // line 21
            echo "            <div class=\"form__label\">";
            echo $this->getAttribute((isset($context["oField"]) ? $context["oField"] : null), "getVal", array(0 => "param_title"), "method");
            if (($this->getAttribute((isset($context["oField"]) ? $context["oField"] : null), "getVal", array(0 => "param_required"), "method") == 1)) {
                echo " <ins class=\"form__mark\">*</ins>";
            }
            echo "</div>
        ";
        }
        // line 23
        echo "    </div>
    ";
        // line 24
        if (($this->getAttribute((isset($context["oField"]) ? $context["oField"] : null), "getVal", array(0 => "param_description"), "method") != "")) {
            echo "<div class=\"form__info\">";
            echo $this->getAttribute((isset($context["oField"]) ? $context["oField"] : null), "getVal", array(0 => "param_description"), "method");
            echo "</div>";
        }
        // line 25
        echo "</div>";
    }

    public function getTemplateName()
    {
        return "/el/select.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  119 => 25,  113 => 24,  110 => 23,  99 => 20,  95 => 18,  84 => 16,  79 => 15,  70 => 12,  68 => 11,  50 => 10,  47 => 9,  44 => 8,  35 => 6,  32 => 5,  21 => 2,  85 => 15,  83 => 14,  75 => 12,  48 => 9,  45 => 8,  42 => 7,  33 => 5,  30 => 4,  24 => 2,  346 => 108,  341 => 105,  339 => 104,  334 => 102,  327 => 101,  320 => 100,  314 => 99,  310 => 98,  306 => 97,  302 => 96,  298 => 95,  294 => 93,  289 => 90,  283 => 88,  277 => 86,  275 => 85,  271 => 83,  266 => 80,  260 => 78,  254 => 76,  252 => 75,  249 => 74,  241 => 72,  239 => 71,  236 => 70,  234 => 69,  225 => 63,  219 => 62,  213 => 59,  209 => 58,  204 => 56,  201 => 55,  195 => 52,  192 => 51,  190 => 50,  187 => 49,  180 => 45,  170 => 40,  165 => 38,  158 => 36,  152 => 32,  149 => 31,  147 => 30,  143 => 28,  135 => 23,  129 => 22,  124 => 20,  120 => 18,  118 => 17,  115 => 16,  101 => 21,  97 => 14,  94 => 17,  76 => 12,  73 => 13,  66 => 10,  64 => 11,  62 => 10,  34 => 5,  28 => 3,  26 => 3,  19 => 1,);
    }
}
