<?php

/* admin.goods.twig */
class __TwigTemplate_97e003662d9eaa8ee6e5033d36d18bc4 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        // line 1
        echo "<h2>";
        echo $this->getAttribute((isset($context["Lang"]) ? $context["Lang"] : null), "get", array(0 => "order.order_number"), "method");
        echo ": ";
        echo (isset($context["id"]) ? $context["id"] : null);
        echo "</h2>
<table style=\"border-top: 1px solid #dfe9f5; width: 800px;\">
    <tr>
        <th style=\"padding: 10px; text-align: left; background-color: #dfe9f5; font-weight: bold; border: 1px solid #dfe9f5;\">";
        // line 4
        echo $this->getAttribute((isset($context["Lang"]) ? $context["Lang"] : null), "get", array(0 => "order.field_goods_title"), "method");
        echo "</th>
        <th style=\"text-align: right; background-color: #dfe9f5; font-weight: bold; border: 1px solid #dfe9f5;\">";
        // line 5
        echo $this->getAttribute((isset($context["Lang"]) ? $context["Lang"] : null), "get", array(0 => "order.field_goods_article"), "method");
        echo "</th>
        <th style=\"text-align: right; background-color: #dfe9f5; font-weight: bold; border: 1px solid #dfe9f5;\">";
        // line 6
        echo $this->getAttribute((isset($context["Lang"]) ? $context["Lang"] : null), "get", array(0 => "order.field_goods_count"), "method");
        echo "</th>
        <th style=\"text-align: right; background-color: #dfe9f5; font-weight: bold; border: 1px solid #dfe9f5;\">";
        // line 7
        echo $this->getAttribute((isset($context["Lang"]) ? $context["Lang"] : null), "get", array(0 => "order.field_goods_price"), "method");
        echo "</th>
        <th style=\"text-align: right; background-color: #dfe9f5; padding-right: 10px; font-weight: bold; border: 1px solid #dfe9f5;\">";
        // line 8
        echo $this->getAttribute((isset($context["Lang"]) ? $context["Lang"] : null), "get", array(0 => "order.field_goods_total"), "method");
        echo "</th>
    </tr>
    ";
        // line 10
        $context['_parent'] = (array) $context;
        $context['_seq'] = twig_ensure_traversable((isset($context["aGoods"]) ? $context["aGoods"] : null));
        foreach ($context['_seq'] as $context["_key"] => $context["item"]) {
            // line 11
            echo "        <tr>
            <td style=\"padding: 10px; font-size: 14px; text-align: left; border-bottom: 1px solid #dfe9f5;\">";
            // line 12
            echo $this->getAttribute((isset($context["item"]) ? $context["item"] : null), "title");
            echo "</td>
            <td style=\"font-size: 14px; text-align: right; border-bottom: 1px solid #dfe9f5;\">";
            // line 13
            echo $this->getAttribute($this->getAttribute($this->getAttribute($this->getAttribute((isset($context["item"]) ? $context["item"] : null), "object"), "fields"), "article"), "value");
            echo "</td>
            <td style=\"font-size: 14px; text-align: right; border-bottom: 1px solid #dfe9f5;\">";
            // line 14
            echo $this->getAttribute((isset($context["item"]) ? $context["item"] : null), "count");
            echo "</td>
            <td style=\"font-size: 14px; text-align: right; border-bottom: 1px solid #dfe9f5;\">";
            // line 15
            echo call_user_func_array($this->env->getFilter('price_format')->getCallable(), array($this->getAttribute((isset($context["item"]) ? $context["item"] : null), "price")));
            echo "</td>
            <td style=\"font-size: 14px; text-align: right; padding-right: 10px; border-bottom: 1px solid #dfe9f5;\">";
            // line 16
            echo call_user_func_array($this->env->getFilter('price_format')->getCallable(), array($this->getAttribute((isset($context["item"]) ? $context["item"] : null), "total")));
            echo "</td>
        </tr>
    ";
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['_key'], $context['item'], $context['_parent'], $context['loop']);
        $context = array_intersect_key($context, $_parent) + $_parent;
        // line 19
        echo "    <tr>
        <td colspan=\"5\" style=\"font-size: 18px; text-align: right;\">";
        // line 20
        echo $this->getAttribute((isset($context["Lang"]) ? $context["Lang"] : null), "get", array(0 => "order.field_goods_total"), "method");
        echo " <b>";
        echo call_user_func_array($this->env->getFilter('price_format')->getCallable(), array((isset($context["totalPrice"]) ? $context["totalPrice"] : null)));
        echo "</b> ";
        echo $this->getAttribute((isset($context["Lang"]) ? $context["Lang"] : null), "get", array(0 => "order.current_currency"), "method");
        echo "</td>
    </tr>
</table>";
    }

    public function getTemplateName()
    {
        return "admin.goods.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  84 => 20,  81 => 19,  72 => 16,  68 => 15,  64 => 14,  60 => 13,  56 => 12,  53 => 11,  49 => 10,  44 => 8,  40 => 7,  36 => 6,  32 => 5,  28 => 4,  19 => 1,);
    }
}
