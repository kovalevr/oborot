<?php

/* adaptive-menu.twig */
class __TwigTemplate_357677efc33088fab976c214c20f8795 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        // line 1
        echo "<div class=\"b-sidebar-menu\">
    <ul class=\"sidebar-menu__level-1\">
        ";
        // line 3
        $context['_parent'] = (array) $context;
        $context['_seq'] = twig_ensure_traversable((isset($context["items"]) ? $context["items"] : null));
        $context['loop'] = array(
          'parent' => $context['_parent'],
          'index0' => 0,
          'index'  => 1,
          'first'  => true,
        );
        if (is_array($context['_seq']) || (is_object($context['_seq']) && $context['_seq'] instanceof Countable)) {
            $length = count($context['_seq']);
            $context['loop']['revindex0'] = $length - 1;
            $context['loop']['revindex'] = $length;
            $context['loop']['length'] = $length;
            $context['loop']['last'] = 1 === $length;
        }
        foreach ($context['_seq'] as $context["_key"] => $context["item"]) {
            // line 4
            echo "            ";
            if ($this->getAttribute((isset($context["item"]) ? $context["item"] : null), "show")) {
                // line 5
                echo "                <li class=\"sidebar-menu__item-1";
                if ($this->getAttribute((isset($context["item"]) ? $context["item"] : null), "selected")) {
                    echo " sidebar-menu__item-1--on";
                }
                if ($this->getAttribute((isset($context["loop"]) ? $context["loop"] : null), "last")) {
                    echo " sidebar-menu__item-1--last";
                }
                if ((twig_length_filter($this->env, $this->getAttribute((isset($context["item"]) ? $context["item"] : null), "items")) > 0)) {
                    echo " sidebar-menu__item-1--haschild";
                }
                if ($this->getAttribute((isset($context["icon"]) ? $context["icon"] : null), $this->getAttribute((isset($context["item"]) ? $context["item"] : null), "id", array(), "array"), array(), "array", true, true)) {
                    echo " sidebar-menu__item-1--hasicon";
                }
                echo " js-sidebarmenu-item\">
                    <span class=\"js-sidebarmenu-achor-wrap\">
                        <div class=\"sidebar-menu__dropicon js-sidebarmenu-achor\"></div>
                        <a href=\"";
                // line 8
                echo $this->getAttribute((isset($context["item"]) ? $context["item"] : null), "href");
                echo "\">
                            ";
                // line 9
                if ($this->getAttribute((isset($context["icon"]) ? $context["icon"] : null), $this->getAttribute((isset($context["item"]) ? $context["item"] : null), "id", array(), "array"), array(), "array", true, true)) {
                    // line 10
                    echo "                                <em style=\"background-image: url(";
                    echo $this->getAttribute((isset($context["icon"]) ? $context["icon"] : null), $this->getAttribute((isset($context["item"]) ? $context["item"] : null), "id", array(), "array"), array(), "array");
                    echo ");\"></em>
                            ";
                } else {
                    // line 12
                    echo "                                <ins></ins>
                            ";
                }
                // line 14
                echo "                            ";
                echo $this->getAttribute((isset($context["item"]) ? $context["item"] : null), "title");
                echo "
                        </a>
                    </span>
                    ";
                // line 17
                if ((twig_length_filter($this->env, $this->getAttribute((isset($context["item"]) ? $context["item"] : null), "items")) > 0)) {
                    // line 18
                    echo "                        <ul class=\"sidebar-menu__level-2 js-sidebarmenu-content\">
                            ";
                    // line 19
                    $context['_parent'] = (array) $context;
                    $context['_seq'] = twig_ensure_traversable($this->getAttribute((isset($context["item"]) ? $context["item"] : null), "items"));
                    $context['loop'] = array(
                      'parent' => $context['_parent'],
                      'index0' => 0,
                      'index'  => 1,
                      'first'  => true,
                    );
                    if (is_array($context['_seq']) || (is_object($context['_seq']) && $context['_seq'] instanceof Countable)) {
                        $length = count($context['_seq']);
                        $context['loop']['revindex0'] = $length - 1;
                        $context['loop']['revindex'] = $length;
                        $context['loop']['length'] = $length;
                        $context['loop']['last'] = 1 === $length;
                    }
                    foreach ($context['_seq'] as $context["_key"] => $context["item2"]) {
                        // line 20
                        echo "                                ";
                        if ($this->getAttribute((isset($context["item2"]) ? $context["item2"] : null), "show")) {
                            // line 21
                            echo "                                    <li class=\"sidebar-menu__item-2";
                            if ($this->getAttribute((isset($context["item2"]) ? $context["item2"] : null), "selected")) {
                                echo " sidebar-menu__item-2--on";
                            }
                            if ($this->getAttribute((isset($context["loop"]) ? $context["loop"] : null), "last")) {
                                echo " sidebar-menu__item-2--last";
                            }
                            if ((twig_length_filter($this->env, $this->getAttribute((isset($context["item2"]) ? $context["item2"] : null), "items")) > 0)) {
                                echo " sidebar-menu__item-2--haschild";
                            }
                            echo " js-sidebarmenu-item\">
                                        <span class=\"js-sidebarmenu-achor-wrap\">
                                            <div class=\"sidebar-menu__dropicon js-sidebarmenu-achor\"></div>
                                            <a href=\"";
                            // line 24
                            echo $this->getAttribute((isset($context["item2"]) ? $context["item2"] : null), "href");
                            echo "\">
                                                ";
                            // line 25
                            echo $this->getAttribute((isset($context["item2"]) ? $context["item2"] : null), "title");
                            echo "
                                            </a>
                                        </span>
                                        ";
                            // line 28
                            if ((twig_length_filter($this->env, $this->getAttribute((isset($context["item2"]) ? $context["item2"] : null), "items")) > 0)) {
                                // line 29
                                echo "                                            <ul class=\"sidebar-menu__level-3 js-sidebarmenu-content\">
                                                ";
                                // line 30
                                $context['_parent'] = (array) $context;
                                $context['_seq'] = twig_ensure_traversable($this->getAttribute((isset($context["item2"]) ? $context["item2"] : null), "items"));
                                $context['loop'] = array(
                                  'parent' => $context['_parent'],
                                  'index0' => 0,
                                  'index'  => 1,
                                  'first'  => true,
                                );
                                if (is_array($context['_seq']) || (is_object($context['_seq']) && $context['_seq'] instanceof Countable)) {
                                    $length = count($context['_seq']);
                                    $context['loop']['revindex0'] = $length - 1;
                                    $context['loop']['revindex'] = $length;
                                    $context['loop']['length'] = $length;
                                    $context['loop']['last'] = 1 === $length;
                                }
                                foreach ($context['_seq'] as $context["_key"] => $context["item3"]) {
                                    // line 31
                                    echo "                                                    ";
                                    if ($this->getAttribute((isset($context["item3"]) ? $context["item3"] : null), "show")) {
                                        // line 32
                                        echo "                                                        <li class=\"sidebar-menu__item-3";
                                        if ($this->getAttribute((isset($context["item3"]) ? $context["item3"] : null), "selected")) {
                                            echo " sidebar-menu__item-3--on";
                                        }
                                        if ($this->getAttribute((isset($context["loop"]) ? $context["loop"] : null), "last")) {
                                            echo " sidebar-menu__item-3--last";
                                        }
                                        echo "\">
                                                            <span>
                                                                <a href=\"";
                                        // line 34
                                        echo $this->getAttribute((isset($context["item3"]) ? $context["item3"] : null), "href");
                                        echo "\">";
                                        echo $this->getAttribute((isset($context["item3"]) ? $context["item3"] : null), "title");
                                        echo "</a>
                                                            </span>
                                                        </li>
                                                    ";
                                    }
                                    // line 38
                                    echo "                                                ";
                                    ++$context['loop']['index0'];
                                    ++$context['loop']['index'];
                                    $context['loop']['first'] = false;
                                    if (isset($context['loop']['length'])) {
                                        --$context['loop']['revindex0'];
                                        --$context['loop']['revindex'];
                                        $context['loop']['last'] = 0 === $context['loop']['revindex0'];
                                    }
                                }
                                $_parent = $context['_parent'];
                                unset($context['_seq'], $context['_iterated'], $context['_key'], $context['item3'], $context['_parent'], $context['loop']);
                                $context = array_intersect_key($context, $_parent) + $_parent;
                                // line 39
                                echo "                                            </ul>
                                        ";
                            }
                            // line 41
                            echo "                                    </li>
                                ";
                        }
                        // line 43
                        echo "                            ";
                        ++$context['loop']['index0'];
                        ++$context['loop']['index'];
                        $context['loop']['first'] = false;
                        if (isset($context['loop']['length'])) {
                            --$context['loop']['revindex0'];
                            --$context['loop']['revindex'];
                            $context['loop']['last'] = 0 === $context['loop']['revindex0'];
                        }
                    }
                    $_parent = $context['_parent'];
                    unset($context['_seq'], $context['_iterated'], $context['_key'], $context['item2'], $context['_parent'], $context['loop']);
                    $context = array_intersect_key($context, $_parent) + $_parent;
                    // line 44
                    echo "                        </ul>
                    ";
                }
                // line 46
                echo "                </li>
            ";
            }
            // line 48
            echo "        ";
            ++$context['loop']['index0'];
            ++$context['loop']['index'];
            $context['loop']['first'] = false;
            if (isset($context['loop']['length'])) {
                --$context['loop']['revindex0'];
                --$context['loop']['revindex'];
                $context['loop']['last'] = 0 === $context['loop']['revindex0'];
            }
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['_key'], $context['item'], $context['_parent'], $context['loop']);
        $context = array_intersect_key($context, $_parent) + $_parent;
        // line 49
        echo "    </ul>
</div>";
    }

    public function getTemplateName()
    {
        return "adaptive-menu.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  237 => 49,  223 => 48,  219 => 46,  215 => 44,  201 => 43,  197 => 41,  193 => 39,  179 => 38,  170 => 34,  159 => 32,  156 => 31,  139 => 30,  136 => 29,  134 => 28,  128 => 25,  124 => 24,  109 => 21,  106 => 20,  89 => 19,  86 => 18,  84 => 17,  77 => 14,  73 => 12,  67 => 10,  61 => 8,  43 => 5,  40 => 4,  23 => 3,  65 => 9,  53 => 9,  41 => 7,  39 => 6,  36 => 5,  32 => 3,  30 => 2,  19 => 1,);
    }
}
