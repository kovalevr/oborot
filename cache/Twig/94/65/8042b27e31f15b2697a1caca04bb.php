<?php

/* ListItems.twig */
class __TwigTemplate_94658042b27e31f15b2697a1caca04bb extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        // line 1
        if (array_key_exists("aObjectList", $context)) {
            // line 2
            echo "    ";
            $context['_parent'] = (array) $context;
            $context['_seq'] = twig_ensure_traversable((isset($context["aObjectList"]) ? $context["aObjectList"] : null));
            foreach ($context['_seq'] as $context["iKey"] => $context["aObject"]) {
                // line 3
                echo "        ";
                if ($this->getAttribute((isset($context["aObject"]) ? $context["aObject"] : null), "show_detail", array(), "any", true, true)) {
                    // line 4
                    echo "            ";
                    $context["show_detail"] = $this->getAttribute((isset($context["aObject"]) ? $context["aObject"] : null), "show_detail");
                    // line 5
                    echo "        ";
                }
                // line 6
                echo "        <div class=\"catalogbox__item js_ecommerce_viewlist js_catalogbox_item\" data-ecommerce='";
                echo $this->getAttribute((isset($context["aObject"]) ? $context["aObject"] : null), "ecommerce");
                echo "' >
            <div class=\"catalogbox__imgbox\">
                <div class=\"catalogbox__img\">";
                // line 8
                if ($this->getAttribute($this->getAttribute($this->getAttribute($this->getAttribute($this->getAttribute($this->getAttribute((isset($context["aObject"]) ? $context["aObject"] : null), "fields", array(), "any", false, true), "gallery", array(), "any", false, true), "first_img", array(), "any", false, true), "images_data", array(), "any", false, true), "small", array(), "array", false, true), "file", array(), "any", true, true)) {
                    echo "<img alt=\"";
                    echo twig_escape_filter($this->env, $this->getAttribute($this->getAttribute($this->getAttribute($this->getAttribute((isset($context["aObject"]) ? $context["aObject"] : null), "fields"), "gallery"), "first_img"), "alt_title"));
                    echo "\" src=\"";
                    echo $this->getAttribute($this->getAttribute($this->getAttribute($this->getAttribute($this->getAttribute($this->getAttribute((isset($context["aObject"]) ? $context["aObject"] : null), "fields"), "gallery"), "first_img"), "images_data"), "small", array(), "array"), "file");
                    echo "\">";
                } else {
                    echo "<img alt=\"\" src=\"";
                    echo $this->getAttribute((isset($context["Design"]) ? $context["Design"] : null), "get", array(0 => "modules.catalogbox.imgbox", 1 => "nophoto_img"), "method");
                    echo "\">";
                }
                echo "</div>
                ";
                // line 9
                if ((isset($context["show_detail"]) ? $context["show_detail"] : null)) {
                    echo "<a href=\"[";
                    if ((isset($context["useMainSection"]) ? $context["useMainSection"] : null)) {
                        echo $this->getAttribute((isset($context["aObject"]) ? $context["aObject"] : null), "main_section");
                    } else {
                        echo (isset($context["section"]) ? $context["section"] : null);
                    }
                    echo "][CatalogViewer?";
                    if ($this->getAttribute((isset($context["aObject"]) ? $context["aObject"] : null), "alias")) {
                        echo "goods-alias=";
                        echo $this->getAttribute((isset($context["aObject"]) ? $context["aObject"] : null), "alias");
                    } else {
                        echo "item=";
                        echo $this->getAttribute((isset($context["aObject"]) ? $context["aObject"] : null), "id");
                    }
                    echo "]\" class=\"catalogbox__imglink js_ecommerce_link\" title=\"";
                    echo twig_escape_filter($this->env, $this->getAttribute($this->getAttribute($this->getAttribute($this->getAttribute((isset($context["aObject"]) ? $context["aObject"] : null), "fields"), "gallery"), "first_img"), "title"));
                    if ($this->getAttribute($this->getAttribute($this->getAttribute($this->getAttribute((isset($context["aObject"]) ? $context["aObject"] : null), "fields"), "gallery"), "first_img"), "description")) {
                        echo " ";
                        echo twig_escape_filter($this->env, $this->getAttribute($this->getAttribute($this->getAttribute($this->getAttribute((isset($context["aObject"]) ? $context["aObject"] : null), "fields"), "gallery"), "first_img"), "description"));
                    }
                    echo "\">";
                }
                if ((isset($context["show_detail"]) ? $context["show_detail"] : null)) {
                    echo "</a>";
                }
                // line 10
                echo "                <div class=\"catalogbox__helper\"></div>
                <div class=\"catalogbox__salebox\">
                    ";
                // line 12
                if ($this->getAttribute($this->getAttribute($this->getAttribute((isset($context["aObject"]) ? $context["aObject"] : null), "fields"), "discount"), "value")) {
                    // line 13
                    echo "                        <div class=\"catalogbox__saleitem\"><img alt=\"\" src=\"";
                    echo $this->getAttribute((isset($context["Design"]) ? $context["Design"] : null), "get", array(0 => "modules.catalogbox.btnbox.salebox", 1 => "image_sale"), "method");
                    echo "\"></div>
                    ";
                }
                // line 15
                echo "                    ";
                if ($this->getAttribute($this->getAttribute($this->getAttribute((isset($context["aObject"]) ? $context["aObject"] : null), "fields"), "hit"), "value")) {
                    // line 16
                    echo "                        <div class=\"catalogbox__saleitem\"><img alt=\"\" src=\"";
                    echo $this->getAttribute((isset($context["Design"]) ? $context["Design"] : null), "get", array(0 => "modules.catalogbox.btnbox.salebox", 1 => "image_hit"), "method");
                    echo "\"></div>
                    ";
                }
                // line 18
                echo "                    ";
                if ($this->getAttribute($this->getAttribute($this->getAttribute((isset($context["aObject"]) ? $context["aObject"] : null), "fields"), "new"), "value")) {
                    // line 19
                    echo "                        <div class=\"catalogbox__saleitem\"><img alt=\"\" src=\"";
                    echo $this->getAttribute((isset($context["Design"]) ? $context["Design"] : null), "get", array(0 => "modules.catalogbox.btnbox.salebox", 1 => "image_new"), "method");
                    echo "\"></div>
                    ";
                }
                // line 21
                echo "                </div>
            </div>
            <div class=\"catalogbox__content\">
                <p class=\"catalogbox__title\">";
                // line 24
                if ((isset($context["show_detail"]) ? $context["show_detail"] : null)) {
                    echo "<a class=\"js_ecommerce_link\" href=\"[";
                    if ((isset($context["useMainSection"]) ? $context["useMainSection"] : null)) {
                        echo $this->getAttribute((isset($context["aObject"]) ? $context["aObject"] : null), "main_section");
                    } else {
                        echo (isset($context["section"]) ? $context["section"] : null);
                    }
                    echo "][CatalogViewer?";
                    if ($this->getAttribute((isset($context["aObject"]) ? $context["aObject"] : null), "alias")) {
                        echo "goods-alias=";
                        echo $this->getAttribute((isset($context["aObject"]) ? $context["aObject"] : null), "alias");
                    } else {
                        echo "item=";
                        echo $this->getAttribute((isset($context["aObject"]) ? $context["aObject"] : null), "id");
                    }
                    echo "]\">";
                }
                echo $this->getAttribute((isset($context["aObject"]) ? $context["aObject"] : null), "title");
                if ((isset($context["show_detail"]) ? $context["show_detail"] : null)) {
                    echo "</a>";
                }
                echo "</p>
                ";
                // line 25
                echo $this->getAttribute($this->getAttribute((isset($context["aObject"]) ? $context["aObject"] : null), "Rating"), "html");
                echo "
                ";
                // line 26
                if ($this->getAttribute($this->getAttribute($this->getAttribute((isset($context["aObject"]) ? $context["aObject"] : null), "fields"), "article"), "value")) {
                    echo "<p class=\"catalogbox__artical\">";
                    if (($this->getAttribute($this->getAttribute($this->getAttribute($this->getAttribute((isset($context["aObject"]) ? $context["aObject"] : null), "fields"), "article"), "attrs"), "show_title") == 1)) {
                        echo $this->getAttribute($this->getAttribute($this->getAttribute((isset($context["aObject"]) ? $context["aObject"] : null), "fields"), "article"), "title");
                    }
                    echo " ";
                    echo $this->getAttribute($this->getAttribute($this->getAttribute((isset($context["aObject"]) ? $context["aObject"] : null), "fields"), "article"), "html");
                    echo "</p>";
                }
                // line 27
                echo "                ";
                if ($this->getAttribute($this->getAttribute($this->getAttribute((isset($context["aObject"]) ? $context["aObject"] : null), "fields"), "announce"), "value")) {
                    echo "<div class=\"catalogbox__param__";
                    echo $this->getAttribute($this->getAttribute($this->getAttribute((isset($context["aObject"]) ? $context["aObject"] : null), "fields"), "announce"), "name");
                    echo "\">";
                    echo $this->getAttribute($this->getAttribute($this->getAttribute((isset($context["aObject"]) ? $context["aObject"] : null), "fields"), "announce"), "value");
                    echo "</div>";
                }
                // line 28
                echo "                ";
                $context['_parent'] = (array) $context;
                $context['_seq'] = twig_ensure_traversable($this->getAttribute((isset($context["aObject"]) ? $context["aObject"] : null), "fields"));
                foreach ($context['_seq'] as $context["_key"] => $context["field"]) {
                    // line 29
                    echo "                    ";
                    if ((($this->getAttribute($this->getAttribute((isset($context["field"]) ? $context["field"] : null), "attrs"), "show_in_params") && $this->getAttribute((isset($context["field"]) ? $context["field"] : null), "value")) && $this->getAttribute((isset($context["field"]) ? $context["field"] : null), "html"))) {
                        // line 30
                        echo "                        ";
                        if ((($this->getAttribute((isset($context["field"]) ? $context["field"] : null), "type") == "multicollection") && twig_length_filter($this->env, $this->getAttribute((isset($context["field"]) ? $context["field"] : null), "value")))) {
                            // line 31
                            echo "                            <div class=\"catalogbox__param catalogbox__param__";
                            echo $this->getAttribute((isset($context["field"]) ? $context["field"] : null), "name");
                            echo "\"><span>";
                            echo $this->getAttribute((isset($context["field"]) ? $context["field"] : null), "title");
                            echo ":</span>
                                ";
                            // line 32
                            $context["aFirstMulti"] = twig_first($this->env, $this->getAttribute((isset($context["field"]) ? $context["field"] : null), "item"));
                            // line 33
                            echo "                                    ";
                            echo $this->getAttribute((isset($context["aFirstMulti"]) ? $context["aFirstMulti"] : null), "html");
                            echo "
                            </div>
                        ";
                        } else {
                            // line 36
                            echo "                            <div class=\"catalogbox__param catalogbox__param__";
                            echo $this->getAttribute((isset($context["field"]) ? $context["field"] : null), "name");
                            echo "\">";
                            if (($this->getAttribute($this->getAttribute((isset($context["field"]) ? $context["field"] : null), "attrs"), "show_title") == 1)) {
                                echo "<span>";
                                echo $this->getAttribute((isset($context["field"]) ? $context["field"] : null), "title");
                                echo ":</span>";
                            }
                            // line 37
                            echo "                                ";
                            if ($this->getAttribute((isset($context["field"]) ? $context["field"] : null), "htmlAlt")) {
                                echo $this->getAttribute((isset($context["field"]) ? $context["field"] : null), "htmlAlt");
                                echo " ";
                            } else {
                                echo $this->getAttribute((isset($context["field"]) ? $context["field"] : null), "html");
                            }
                            // line 38
                            echo "                            </div>
                        ";
                        }
                        // line 40
                        echo "                    ";
                    }
                    // line 41
                    echo "                ";
                }
                $_parent = $context['_parent'];
                unset($context['_seq'], $context['_iterated'], $context['_key'], $context['field'], $context['_parent'], $context['loop']);
                $context = array_intersect_key($context, $_parent) + $_parent;
                // line 42
                echo "                <p>";
                if ($this->getAttribute($this->getAttribute($this->getAttribute((isset($context["aObject"]) ? $context["aObject"] : null), "fields"), "old_price"), "value")) {
                    echo "<span class=\"catalogbox__oldprice\"><span>";
                    echo call_user_func_array($this->env->getFilter('price_format')->getCallable(), array($this->getAttribute($this->getAttribute($this->getAttribute((isset($context["aObject"]) ? $context["aObject"] : null), "fields"), "old_price"), "value")));
                    echo "
                            ";
                    // line 43
                    echo $this->getAttribute($this->getAttribute($this->getAttribute((isset($context["aObject"]) ? $context["aObject"] : null), "fields"), "old_price"), "measure");
                    if ($this->getAttribute($this->getAttribute($this->getAttribute((isset($context["aObject"]) ? $context["aObject"] : null), "fields"), "measure"), "html")) {
                        echo "/";
                        echo $this->getAttribute($this->getAttribute($this->getAttribute((isset($context["aObject"]) ? $context["aObject"] : null), "fields"), "measure"), "html");
                    }
                    // line 44
                    echo "                        </span></span>";
                }
                // line 45
                echo "                    ";
                if ($this->getAttribute($this->getAttribute($this->getAttribute((isset($context["aObject"]) ? $context["aObject"] : null), "fields"), "price"), "value")) {
                    // line 46
                    echo "                <p class=\"catalogbox__price\">";
                    if (($this->getAttribute($this->getAttribute($this->getAttribute($this->getAttribute((isset($context["aObject"]) ? $context["aObject"] : null), "fields"), "price"), "attrs"), "show_title") == 1)) {
                        echo $this->getAttribute($this->getAttribute($this->getAttribute((isset($context["aObject"]) ? $context["aObject"] : null), "fields"), "price"), "title");
                        echo ":";
                    }
                    // line 47
                    echo "                            <span class=\"catalogbox__price\">
                                <span>";
                    // line 48
                    echo call_user_func_array($this->env->getFilter('price_format')->getCallable(), array($this->getAttribute($this->getAttribute($this->getAttribute((isset($context["aObject"]) ? $context["aObject"] : null), "fields"), "price"), "value")));
                    echo "</span>
                            </span>
                    ";
                    // line 50
                    echo $this->getAttribute($this->getAttribute($this->getAttribute((isset($context["aObject"]) ? $context["aObject"] : null), "fields"), "price"), "measure");
                    if ($this->getAttribute($this->getAttribute($this->getAttribute((isset($context["aObject"]) ? $context["aObject"] : null), "fields"), "measure"), "html")) {
                        echo "/";
                        echo $this->getAttribute($this->getAttribute($this->getAttribute((isset($context["aObject"]) ? $context["aObject"] : null), "fields"), "measure"), "html");
                    }
                    // line 51
                    echo "                </p>
                ";
                }
                // line 53
                echo "                ";
                if ((!(isset($context["hideBuy1lvlGoods"]) ? $context["hideBuy1lvlGoods"] : null))) {
                    // line 54
                    echo "                <div class=\"catalogbox__shcar\">
                    <div class=\"catalogbox__btnbox\">
                    ";
                    // line 56
                    if ($this->getAttribute($this->getAttribute($this->getAttribute((isset($context["aObject"]) ? $context["aObject"] : null), "fields"), "fastbuy"), "value")) {
                        // line 57
                        echo "                        <div class=\"catalogbox__buynow\"><a class=\"js-callback\" data-ajaxform=\"1\" data-js_max_width=\"600\" data-width-type=\"px\" data-idobj=\"";
                        echo $this->getAttribute((isset($context["aObject"]) ? $context["aObject"] : null), "id");
                        echo "\" data-module=\"Cart\" data-cmd=\"checkout\" href=\"#\">";
                        if (($this->getAttribute($this->getAttribute($this->getAttribute($this->getAttribute((isset($context["aObject"]) ? $context["aObject"] : null), "fields"), "fastbuy"), "attrs"), "show_title") == 1)) {
                            echo $this->getAttribute($this->getAttribute($this->getAttribute((isset($context["aObject"]) ? $context["aObject"] : null), "fields"), "fastbuy"), "title");
                        }
                        echo "</a></div>
                    ";
                    }
                    // line 59
                    echo "                    </div>
                </div>
                <div class=\"g-clear\"></div>
                <div class=\"catalogbox__shcar\">
                    <div class=\"catalogbox__btnbox\">
                        ";
                    // line 64
                    if (($this->getAttribute($this->getAttribute((isset($context["aObject"]) ? $context["aObject"] : null), "fields"), "countbuy") && $this->getAttribute($this->getAttribute($this->getAttribute((isset($context["aObject"]) ? $context["aObject"] : null), "fields"), "countbuy"), "value"))) {
                        // line 65
                        echo "                            <div class=\"js_catalogbox_inputbox catalogbox__inputbox\"><div class=\"catalogbox__minus js_catalogbox_minus\">-</div><input class=\"js_count\" type=\"text\" data-id=\"";
                        echo $this->getAttribute((isset($context["aObject"]) ? $context["aObject"] : null), "id");
                        echo "\" value=\"1\"><div class=\"catalogbox__plus js_catalogbox_plus\">+</div></div>
                        ";
                    }
                    // line 67
                    echo "                        ";
                    if ($this->getAttribute($this->getAttribute($this->getAttribute((isset($context["aObject"]) ? $context["aObject"] : null), "fields"), "buy"), "value")) {
                        // line 68
                        echo "                            <div class=\"catalogbox__btn\"><a data-id=\"";
                        echo $this->getAttribute((isset($context["aObject"]) ? $context["aObject"] : null), "id");
                        echo "\" ";
                        if ((isset($context["useCart"]) ? $context["useCart"] : null)) {
                            // line 69
                            echo "onclick=\"return false;\" ";
                        }
                        echo "href=\"";
                        if ((!(isset($context["useCart"]) ? $context["useCart"] : null))) {
                            echo "[";
                            echo (isset($context["form_section"]) ? $context["form_section"] : null);
                            echo "][From?objectId=";
                            echo $this->getAttribute((isset($context["aObject"]) ? $context["aObject"] : null), "id");
                            echo "]";
                        } else {
                            echo "#tocart";
                        }
                        echo "\" class=\"js-btnBuy btnBuy\">";
                        echo $this->getAttribute((isset($context["Lang"]) ? $context["Lang"] : null), "get", array(0 => $this->getAttribute($this->getAttribute($this->getAttribute((isset($context["aObject"]) ? $context["aObject"] : null), "fields"), "buy"), "title")), "method");
                        echo "</a></div>
                        ";
                    }
                    // line 71
                    echo "
                    </div>
                </div>
                ";
                }
                // line 75
                echo "            </div>
        </div>
    ";
            }
            $_parent = $context['_parent'];
            unset($context['_seq'], $context['_iterated'], $context['iKey'], $context['aObject'], $context['_parent'], $context['loop']);
            $context = array_intersect_key($context, $_parent) + $_parent;
        }
    }

    public function getTemplateName()
    {
        return "ListItems.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  330 => 75,  324 => 71,  306 => 69,  301 => 68,  298 => 67,  292 => 65,  290 => 64,  283 => 59,  273 => 57,  271 => 56,  267 => 54,  264 => 53,  260 => 51,  254 => 50,  249 => 48,  246 => 47,  240 => 46,  237 => 45,  234 => 44,  228 => 43,  221 => 42,  215 => 41,  212 => 40,  208 => 38,  200 => 37,  191 => 36,  184 => 33,  182 => 32,  175 => 31,  172 => 30,  169 => 29,  164 => 28,  155 => 27,  145 => 26,  141 => 25,  117 => 24,  112 => 21,  106 => 19,  103 => 18,  97 => 16,  94 => 15,  88 => 13,  86 => 12,  82 => 10,  55 => 9,  41 => 8,  35 => 6,  29 => 4,  26 => 3,  79 => 22,  75 => 21,  71 => 20,  63 => 15,  58 => 14,  52 => 12,  49 => 11,  36 => 9,  32 => 5,  27 => 6,  34 => 7,  30 => 4,  28 => 3,  21 => 2,  19 => 1,);
    }
}
