<?php

/* topMenu.twig */
class __TwigTemplate_622fa87d0685c6f8c221961667da6e5b extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        // line 1
        echo "<div class=\"b-header";
        echo $this->getAttribute((isset($context["Adaptive"]) ? $context["Adaptive"] : null), "write", array(0 => " hide-on-tablet hide-on-mobile"), "method");
        echo "\"";
        echo $this->getAttribute((isset($context["Design"]) ? $context["Design"] : null), "write", array(0 => " sklayout=\"head\""), "method");
        echo ">
     <div class=\"b-sevice\"";
        // line 2
        echo $this->getAttribute((isset($context["Design"]) ? $context["Design"] : null), "write", array(0 => " sktag=\"menu.top\""), "method");
        echo ">
        <ul class=\"sevice__level-1\"";
        // line 3
        echo $this->getAttribute((isset($context["Design"]) ? $context["Design"] : null), "write", array(0 => " sktag=\"menu.top.level1\""), "method");
        echo ">
        ";
        // line 4
        $context['_parent'] = (array) $context;
        $context['_seq'] = twig_ensure_traversable((isset($context["items"]) ? $context["items"] : null));
        $context['loop'] = array(
          'parent' => $context['_parent'],
          'index0' => 0,
          'index'  => 1,
          'first'  => true,
        );
        if (is_array($context['_seq']) || (is_object($context['_seq']) && $context['_seq'] instanceof Countable)) {
            $length = count($context['_seq']);
            $context['loop']['revindex0'] = $length - 1;
            $context['loop']['revindex'] = $length;
            $context['loop']['length'] = $length;
            $context['loop']['last'] = 1 === $length;
        }
        foreach ($context['_seq'] as $context["_key"] => $context["item"]) {
            // line 5
            echo "        ";
            if ($this->getAttribute((isset($context["item"]) ? $context["item"] : null), "show")) {
                // line 6
                echo "            <li class=\"sevice__item-1 sevice__item-id-";
                echo $this->getAttribute((isset($context["item"]) ? $context["item"] : null), "id", array(), "array");
                if ($this->getAttribute((isset($context["item"]) ? $context["item"] : null), "selected")) {
                    echo " sevice__item-1--on";
                }
                if ($this->getAttribute((isset($context["loop"]) ? $context["loop"] : null), "last")) {
                    echo " sevice__item-1--last";
                }
                if ((twig_length_filter($this->env, $this->getAttribute((isset($context["item"]) ? $context["item"] : null), "items")) > 0)) {
                    echo " sevice__item-1--haschild";
                }
                echo "\">
                <span>
                    <a href=\"";
                // line 8
                echo $this->getAttribute((isset($context["item"]) ? $context["item"] : null), "href");
                echo "\">
                        ";
                // line 9
                if ($this->getAttribute((isset($context["icon"]) ? $context["icon"] : null), $this->getAttribute((isset($context["item"]) ? $context["item"] : null), "id", array(), "array"), array(), "array", true, true)) {
                    // line 10
                    echo "                            <em style=\"background-image: url(";
                    echo $this->getAttribute((isset($context["icon"]) ? $context["icon"] : null), $this->getAttribute((isset($context["item"]) ? $context["item"] : null), "id", array(), "array"), array(), "array");
                    echo ");\"></em>
                        ";
                } else {
                    // line 12
                    echo "                            <ins></ins>
                        ";
                }
                // line 14
                echo "                        ";
                echo $this->getAttribute((isset($context["item"]) ? $context["item"] : null), "title");
                echo "
                    </a>
                </span>
                ";
                // line 17
                if ((twig_length_filter($this->env, $this->getAttribute((isset($context["item"]) ? $context["item"] : null), "items")) > 0)) {
                    // line 18
                    echo "                <ul class=\"sevice__level-2\"";
                    echo $this->getAttribute((isset($context["Design"]) ? $context["Design"] : null), "write", array(0 => " sktag=\"menu.top.level2\""), "method");
                    echo ">
                    ";
                    // line 19
                    $context['_parent'] = (array) $context;
                    $context['_seq'] = twig_ensure_traversable($this->getAttribute((isset($context["item"]) ? $context["item"] : null), "items"));
                    $context['loop'] = array(
                      'parent' => $context['_parent'],
                      'index0' => 0,
                      'index'  => 1,
                      'first'  => true,
                    );
                    if (is_array($context['_seq']) || (is_object($context['_seq']) && $context['_seq'] instanceof Countable)) {
                        $length = count($context['_seq']);
                        $context['loop']['revindex0'] = $length - 1;
                        $context['loop']['revindex'] = $length;
                        $context['loop']['length'] = $length;
                        $context['loop']['last'] = 1 === $length;
                    }
                    foreach ($context['_seq'] as $context["_key"] => $context["item2"]) {
                        // line 20
                        echo "                    ";
                        if ($this->getAttribute((isset($context["item2"]) ? $context["item2"] : null), "show")) {
                            // line 21
                            echo "                        <li class=\"sevice__item-2";
                            if ((twig_length_filter($this->env, $this->getAttribute((isset($context["item2"]) ? $context["item2"] : null), "items")) > 0)) {
                                echo " sevice__item-2--haschild";
                            }
                            if ($this->getAttribute((isset($context["item2"]) ? $context["item2"] : null), "selected")) {
                                echo " sevice__item-2--on";
                            }
                            if ($this->getAttribute((isset($context["loop"]) ? $context["loop"] : null), "last")) {
                                echo " last2";
                            }
                            echo "\">
                            <span>
                                <a href=\"";
                            // line 23
                            echo $this->getAttribute((isset($context["item2"]) ? $context["item2"] : null), "href");
                            echo "\">
                                    ";
                            // line 24
                            echo $this->getAttribute((isset($context["item2"]) ? $context["item2"] : null), "title");
                            echo " 
                                    <ins></ins>
                                </a>
                            </span>
                            ";
                            // line 28
                            if ((twig_length_filter($this->env, $this->getAttribute((isset($context["item2"]) ? $context["item2"] : null), "items")) > 0)) {
                                // line 29
                                echo "                            <ul class=\"sevice__level-3\"";
                                echo $this->getAttribute((isset($context["Design"]) ? $context["Design"] : null), "write", array(0 => " sktag=\"menu.top.level3\""), "method");
                                echo ">
                                ";
                                // line 30
                                $context['_parent'] = (array) $context;
                                $context['_seq'] = twig_ensure_traversable($this->getAttribute((isset($context["item2"]) ? $context["item2"] : null), "items"));
                                $context['loop'] = array(
                                  'parent' => $context['_parent'],
                                  'index0' => 0,
                                  'index'  => 1,
                                  'first'  => true,
                                );
                                if (is_array($context['_seq']) || (is_object($context['_seq']) && $context['_seq'] instanceof Countable)) {
                                    $length = count($context['_seq']);
                                    $context['loop']['revindex0'] = $length - 1;
                                    $context['loop']['revindex'] = $length;
                                    $context['loop']['length'] = $length;
                                    $context['loop']['last'] = 1 === $length;
                                }
                                foreach ($context['_seq'] as $context["_key"] => $context["item3"]) {
                                    // line 31
                                    echo "                                    ";
                                    if ($this->getAttribute((isset($context["item3"]) ? $context["item3"] : null), "show")) {
                                        // line 32
                                        echo "                                        <li class=\"sevice__item-3";
                                        if ($this->getAttribute((isset($context["item3"]) ? $context["item3"] : null), "selected")) {
                                            echo " sevice__item-3--on";
                                        }
                                        if ($this->getAttribute((isset($context["loop"]) ? $context["loop"] : null), "last")) {
                                            echo " last3";
                                        }
                                        echo "\">
                                            <span>
                                                <a href=\"";
                                        // line 34
                                        echo $this->getAttribute((isset($context["item3"]) ? $context["item3"] : null), "href");
                                        echo "\">";
                                        echo $this->getAttribute((isset($context["item3"]) ? $context["item3"] : null), "title");
                                        echo "</a>
                                            </span>
                                        </li>
                                    ";
                                    }
                                    // line 38
                                    echo "                                ";
                                    ++$context['loop']['index0'];
                                    ++$context['loop']['index'];
                                    $context['loop']['first'] = false;
                                    if (isset($context['loop']['length'])) {
                                        --$context['loop']['revindex0'];
                                        --$context['loop']['revindex'];
                                        $context['loop']['last'] = 0 === $context['loop']['revindex0'];
                                    }
                                }
                                $_parent = $context['_parent'];
                                unset($context['_seq'], $context['_iterated'], $context['_key'], $context['item3'], $context['_parent'], $context['loop']);
                                $context = array_intersect_key($context, $_parent) + $_parent;
                                // line 39
                                echo "                            </ul>
                            ";
                            }
                            // line 41
                            echo "                        </li>
                    ";
                        }
                        // line 43
                        echo "                    ";
                        ++$context['loop']['index0'];
                        ++$context['loop']['index'];
                        $context['loop']['first'] = false;
                        if (isset($context['loop']['length'])) {
                            --$context['loop']['revindex0'];
                            --$context['loop']['revindex'];
                            $context['loop']['last'] = 0 === $context['loop']['revindex0'];
                        }
                    }
                    $_parent = $context['_parent'];
                    unset($context['_seq'], $context['_iterated'], $context['_key'], $context['item2'], $context['_parent'], $context['loop']);
                    $context = array_intersect_key($context, $_parent) + $_parent;
                    // line 44
                    echo "                </ul>
                ";
                }
                // line 46
                echo "            </li>
        ";
            }
            // line 48
            echo "        ";
            ++$context['loop']['index0'];
            ++$context['loop']['index'];
            $context['loop']['first'] = false;
            if (isset($context['loop']['length'])) {
                --$context['loop']['revindex0'];
                --$context['loop']['revindex'];
                $context['loop']['last'] = 0 === $context['loop']['revindex0'];
            }
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['_key'], $context['item'], $context['_parent'], $context['loop']);
        $context = array_intersect_key($context, $_parent) + $_parent;
        // line 49
        echo "        </ul>
    </div>
</div>";
    }

    public function getTemplateName()
    {
        return "topMenu.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  249 => 49,  235 => 48,  231 => 46,  227 => 44,  213 => 43,  205 => 39,  191 => 38,  171 => 32,  168 => 31,  151 => 30,  144 => 28,  133 => 23,  94 => 18,  92 => 17,  85 => 14,  81 => 12,  75 => 10,  54 => 6,  51 => 5,  60 => 14,  34 => 4,  161 => 41,  137 => 24,  114 => 30,  96 => 27,  83 => 26,  74 => 20,  69 => 8,  42 => 13,  38 => 11,  160 => 45,  154 => 44,  150 => 36,  140 => 39,  132 => 33,  126 => 34,  117 => 30,  102 => 26,  88 => 22,  68 => 17,  59 => 13,  49 => 17,  35 => 5,  22 => 2,  129 => 26,  121 => 24,  115 => 21,  97 => 25,  90 => 16,  82 => 15,  79 => 14,  71 => 12,  47 => 8,  45 => 9,  28 => 3,  26 => 2,  72 => 16,  64 => 13,  37 => 7,  33 => 6,  29 => 4,  25 => 3,  310 => 74,  304 => 70,  296 => 67,  278 => 65,  273 => 64,  270 => 63,  260 => 61,  258 => 60,  254 => 58,  248 => 56,  246 => 55,  242 => 53,  238 => 51,  232 => 50,  228 => 49,  225 => 48,  218 => 44,  209 => 41,  203 => 41,  200 => 40,  194 => 39,  190 => 37,  182 => 34,  173 => 35,  165 => 33,  146 => 29,  142 => 30,  130 => 29,  125 => 26,  119 => 21,  116 => 20,  110 => 21,  107 => 18,  101 => 29,  99 => 19,  95 => 24,  80 => 25,  66 => 16,  58 => 12,  55 => 12,  52 => 4,  48 => 10,  27 => 4,  24 => 2,  21 => 2,  237 => 49,  223 => 47,  219 => 46,  215 => 43,  201 => 43,  197 => 41,  193 => 39,  179 => 38,  170 => 34,  159 => 32,  156 => 32,  139 => 30,  136 => 37,  134 => 28,  128 => 25,  124 => 24,  109 => 28,  106 => 27,  89 => 19,  86 => 18,  84 => 17,  77 => 19,  73 => 9,  67 => 18,  61 => 12,  43 => 8,  40 => 4,  23 => 4,  65 => 22,  53 => 9,  41 => 7,  39 => 3,  36 => 10,  32 => 4,  30 => 3,  19 => 1,);
    }
}
