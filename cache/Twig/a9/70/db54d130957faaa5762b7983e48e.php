<?php

/* checkout.twig */
class __TwigTemplate_a970db54d130957faaa5762b7983e48e extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        // line 1
        echo "<div class=\"b-cart js-cart-content\">
    ";
        // line 2
        if ($this->getAttribute((isset($context["order"]) ? $context["order"] : null), "getCount", array(), "method")) {
            // line 3
            echo "        <div class=\"js_cart_content cart__content\">
            <table class=\"cart__table\">
                <tr>
                    <th>";
            // line 6
            echo $this->getAttribute((isset($context["Lang"]) ? $context["Lang"] : null), "get", array(0 => "order.title"), "method");
            echo "</th>
                    ";
            // line 7
            if (((isset($context["fastBuy"]) ? $context["fastBuy"] : null) != 1)) {
                echo "<th>";
                echo $this->getAttribute((isset($context["Lang"]) ? $context["Lang"] : null), "get", array(0 => "order.photo"), "method");
                echo "</th>";
            }
            // line 8
            echo "                    <th>";
            echo $this->getAttribute((isset($context["Lang"]) ? $context["Lang"] : null), "get", array(0 => "order.article"), "method");
            echo "</th>
                    <th>";
            // line 9
            echo $this->getAttribute((isset($context["Lang"]) ? $context["Lang"] : null), "get", array(0 => "order.price"), "method");
            echo ", ";
            echo $this->getAttribute((isset($context["Lang"]) ? $context["Lang"] : null), "get", array(0 => "order.current_currency"), "method");
            echo "</th>
                    <th>";
            // line 10
            echo $this->getAttribute((isset($context["Lang"]) ? $context["Lang"] : null), "get", array(0 => "order.count"), "method");
            echo "</th>
                    <th>";
            // line 11
            echo $this->getAttribute((isset($context["Lang"]) ? $context["Lang"] : null), "get", array(0 => "order.sum"), "method");
            echo ", ";
            echo $this->getAttribute((isset($context["Lang"]) ? $context["Lang"] : null), "get", array(0 => "order.current_currency"), "method");
            echo "</th>
                </tr>
                ";
            // line 13
            $context['_parent'] = (array) $context;
            $context['_seq'] = twig_ensure_traversable($this->getAttribute((isset($context["order"]) ? $context["order"] : null), "getItems", array(), "method"));
            foreach ($context['_seq'] as $context["_key"] => $context["item"]) {
                // line 14
                echo "                    <tr class=\"cart__row js-cart__row\">
                        <td><strong>";
                // line 15
                echo $this->getAttribute((isset($context["item"]) ? $context["item"] : null), "title");
                echo "</strong></td>
                        ";
                // line 16
                if (((isset($context["fastBuy"]) ? $context["fastBuy"] : null) != 1)) {
                    // line 17
                    echo "                        <td>
                            <span class=\"cabbox__imagebox\">
                            ";
                    // line 19
                    if ($this->getAttribute($this->getAttribute($this->getAttribute((isset($context["item"]) ? $context["item"] : null), "image", array(), "any", false, true), "big", array(), "any", false, true), "file", array(), "any", true, true)) {
                        // line 20
                        echo "                                <a class=\"js-gallery_resize\" href=\"";
                        echo $this->getAttribute($this->getAttribute($this->getAttribute((isset($context["item"]) ? $context["item"] : null), "image"), "big"), "file");
                        echo "\">
                                    <div class=\"cart__objectimg\">
                                        <img alt=\"\" src=\"/images/lk.photo.png\">
                                    </div>
                                </a>
                            ";
                    } else {
                        // line 26
                        echo "                                <div class=\"cart__objectimg\">
                                    <img alt=\"\" src=\"/images/lk.no_photo.png\">
                                </div>
                            ";
                    }
                    // line 30
                    echo "                            </span>
                        </td>
                        ";
                }
                // line 33
                echo "                        <td class=\"cart__center\">";
                echo $this->getAttribute((isset($context["item"]) ? $context["item"] : null), "article");
                echo "</td>
                        <td class=\"cart__center\"><span class=\"item_price\">";
                // line 34
                echo call_user_func_array($this->env->getFilter('price_format')->getCallable(), array($this->getAttribute((isset($context["item"]) ? $context["item"] : null), "price")));
                echo "</span></td>
                        <td class=\"cart__center\">";
                // line 35
                echo $this->getAttribute((isset($context["item"]) ? $context["item"] : null), "count");
                echo "</td>
                        <td class=\"cart__right\"><span class=\"item_total js-item_total\">";
                // line 36
                echo call_user_func_array($this->env->getFilter('price_format')->getCallable(), array($this->getAttribute((isset($context["item"]) ? $context["item"] : null), "total")));
                echo " </span></td>
                    </tr>
                ";
            }
            $_parent = $context['_parent'];
            unset($context['_seq'], $context['_iterated'], $context['_key'], $context['item'], $context['_parent'], $context['loop']);
            $context = array_intersect_key($context, $_parent) + $_parent;
            // line 39
            echo "                <tr class=\"cart__totalrow\">
                    <td class=\"cart__total\" colspan=\"";
            // line 40
            if (((isset($context["fastBuy"]) ? $context["fastBuy"] : null) != 1)) {
                echo "5";
            } else {
                echo "4";
            }
            echo "\"><b>";
            echo $this->getAttribute((isset($context["Lang"]) ? $context["Lang"] : null), "get", array(0 => "order.total"), "method");
            echo ":</b></td>
                    <td class=\"cart__right\"><span class=\"total\"><strong>";
            // line 41
            echo call_user_func_array($this->env->getFilter('price_format')->getCallable(), array($this->getAttribute((isset($context["order"]) ? $context["order"] : null), "getTotalPrice", array(), "method")));
            echo " ";
            echo $this->getAttribute((isset($context["Lang"]) ? $context["Lang"] : null), "get", array(0 => "order.current_currency"), "method");
            echo "</strong></span></td>
                </tr>
            </table>
        </div>
    ";
        }
        // line 46
        echo "</div>
";
        // line 47
        echo (isset($context["form"]) ? $context["form"] : null);
        echo "
";
        // line 48
        echo (isset($context["error"]) ? $context["error"] : null);
        echo "
";
    }

    public function getTemplateName()
    {
        return "checkout.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  153 => 48,  149 => 47,  146 => 46,  136 => 41,  126 => 40,  123 => 39,  114 => 36,  106 => 34,  96 => 30,  90 => 26,  80 => 20,  78 => 19,  74 => 17,  72 => 16,  65 => 14,  61 => 13,  54 => 11,  39 => 8,  33 => 7,  29 => 6,  24 => 3,  22 => 2,  119 => 25,  113 => 24,  110 => 35,  101 => 33,  99 => 20,  95 => 18,  84 => 16,  79 => 15,  73 => 13,  70 => 12,  68 => 15,  50 => 10,  47 => 9,  44 => 9,  35 => 6,  32 => 5,  30 => 4,  26 => 3,  21 => 2,  19 => 1,);
    }
}
