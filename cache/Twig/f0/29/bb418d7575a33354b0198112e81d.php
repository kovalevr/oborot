<?php

/* purchase.twig */
class __TwigTemplate_f029bb418d7575a33354b0198112e81d extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        // line 1
        if ((isset($context["AllowEcommerceSendData"]) ? $context["AllowEcommerceSendData"] : null)) {
            // line 2
            echo "
    <script type=\"text/javascript\">
        ";
            // line 4
            if ((isset($context["bIsFastBuy"]) ? $context["bIsFastBuy"] : null)) {
                // line 5
                echo "            ecommerce.sendDataPurchaseFastBuy( ";
                echo (isset($context["ecommerce_objects"]) ? $context["ecommerce_objects"] : null);
                echo ", ";
                echo (isset($context["order_id"]) ? $context["order_id"] : null);
                echo " );
        ";
            } else {
                // line 7
                echo "            ecommerce.sendDataPurchase( ";
                echo (isset($context["ecommerce_objects"]) ? $context["ecommerce_objects"] : null);
                echo ", ";
                echo (isset($context["order_id"]) ? $context["order_id"] : null);
                echo " );
        ";
            }
            // line 9
            echo "    </script>

";
        }
    }

    public function getTemplateName()
    {
        return "purchase.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  43 => 9,  35 => 7,  27 => 5,  25 => 4,  21 => 2,  19 => 1,);
    }
}
