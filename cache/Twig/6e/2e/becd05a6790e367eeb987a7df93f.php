<?php

/* mail.twig */
class __TwigTemplate_6e2ebecd05a6790e367eeb987a7df93f extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        // line 1
        echo "<p>
    <span style=\"float: right;\">";
        // line 2
        echo $this->getAttribute((isset($context["Lang"]) ? $context["Lang"] : null), "get", array(0 => "order.field_date"), "method");
        echo ": <b>";
        echo (isset($context["date"]) ? $context["date"] : null);
        echo "</b></span>
    ";
        // line 3
        echo $this->getAttribute((isset($context["Lang"]) ? $context["Lang"] : null), "get", array(0 => "order.order_number"), "method");
        echo " <b>";
        echo (isset($context["orderId"]) ? $context["orderId"] : null);
        echo "</b>.
</p>
";
        // line 5
        if ((!(isset($context["bAdmin"]) ? $context["bAdmin"] : null))) {
            // line 6
            echo "    <table cellpadding=\"0\" cellspacing=\"0\" style=\"border-top: 1px solid #ccc; border-bottom: 1px solid #ccc; width: 100%;\">
        ";
            // line 7
            $context['_parent'] = (array) $context;
            $context['_seq'] = twig_ensure_traversable((isset($context["items"]) ? $context["items"] : null));
            foreach ($context['_seq'] as $context["_key"] => $context["item"]) {
                // line 8
                echo "            ";
                if ($this->getAttribute((isset($context["item"]) ? $context["item"] : null), "value")) {
                    // line 9
                    echo "                <tr>
                    <td style=\" width: 150px; padding: 5px 10px; font-size: 14px; vertical-align: top;\">
                        <b>";
                    // line 11
                    echo $this->getAttribute((isset($context["item"]) ? $context["item"] : null), "title");
                    echo ":</b>
                    </td>
                    <td style=\"padding: 10px; font-size: 14px; vertical-align: top;\">";
                    // line 13
                    echo $this->getAttribute((isset($context["item"]) ? $context["item"] : null), "value");
                    echo "</td>
                </tr>
            ";
                }
                // line 16
                echo "        ";
            }
            $_parent = $context['_parent'];
            unset($context['_seq'], $context['_iterated'], $context['_key'], $context['item'], $context['_parent'], $context['loop']);
            $context = array_intersect_key($context, $_parent) + $_parent;
            // line 17
            echo "    </table>
";
        }
        // line 19
        echo "<p>&nbsp;</p>
<table class=\"shopping_cart\" cellpadding=\"0\" cellspacing=\"0\" class=\"shopping_cart\" style=\"border-top: 1px solid #ccc; width: 100%; border-collapse: collapse;\">
    <tr>
        <th style=\"padding: 10px; text-align: left; background-color: #CCCCCC; font-weight: bold; border: 1px solid #ccc;\">";
        // line 22
        echo $this->getAttribute((isset($context["Lang"]) ? $context["Lang"] : null), "get", array(0 => "order.field_goods_title"), "method");
        echo "</th>
        <th style=\"padding: 10px; text-align: left; background-color: #CCCCCC; font-weight: bold; border: 1px solid #ccc;\">";
        // line 23
        echo $this->getAttribute((isset($context["Lang"]) ? $context["Lang"] : null), "get", array(0 => "order.field_goods_article"), "method");
        echo "</th>
        <th style=\"text-align: left; background-color: #CCCCCC; font-weight: bold; border: 1px solid #ccc;\">";
        // line 24
        echo $this->getAttribute((isset($context["Lang"]) ? $context["Lang"] : null), "get", array(0 => "order.field_goods_count"), "method");
        echo "</th>
        <th style=\"text-align: left; background-color: #CCCCCC; font-weight: bold; border: 1px solid #ccc;\">";
        // line 25
        echo $this->getAttribute((isset($context["Lang"]) ? $context["Lang"] : null), "get", array(0 => "order.field_goods_price"), "method");
        echo "</th>
    </tr>
    ";
        // line 27
        $context['_parent'] = (array) $context;
        $context['_seq'] = twig_ensure_traversable((isset($context["aGoods"]) ? $context["aGoods"] : null));
        foreach ($context['_seq'] as $context["_key"] => $context["item"]) {
            // line 28
            echo "    <tr>
        <td style=\"padding: 10px; font-size: 14px; text-align: left; font-weight: bold; border-bottom: 1px solid #cccccc;\">
            ";
            // line 30
            if ($this->getAttribute((isset($context["item"]) ? $context["item"] : null), "show_detail")) {
                echo "<a href=\"";
                echo (isset($context["webrootpath"]) ? $context["webrootpath"] : null);
                echo $this->getAttribute((isset($context["item"]) ? $context["item"] : null), "url");
                echo "\" target=\"_blank\">";
            }
            echo $this->getAttribute((isset($context["item"]) ? $context["item"] : null), "title");
            if ($this->getAttribute((isset($context["item"]) ? $context["item"] : null), "show_detail")) {
                echo "</a>";
            }
            // line 31
            echo "        </td>
        <td style=\"font-size: 14px; text-align: left; font-weight: bold; border-bottom: 1px solid #cccccc;\">";
            // line 32
            echo $this->getAttribute((isset($context["item"]) ? $context["item"] : null), "article");
            echo "</td>
        <td style=\"font-size: 14px; text-align: left; font-weight: bold; border-bottom: 1px solid #cccccc;\">";
            // line 33
            echo $this->getAttribute((isset($context["item"]) ? $context["item"] : null), "count");
            echo "</td>
        <td style=\"font-size: 14px; text-align: left; font-weight: bold; border-bottom: 1px solid #cccccc;\">";
            // line 34
            echo call_user_func_array($this->env->getFilter('price_format')->getCallable(), array($this->getAttribute((isset($context["item"]) ? $context["item"] : null), "total")));
            echo "</td>
    </tr>
    ";
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['_key'], $context['item'], $context['_parent'], $context['loop']);
        $context = array_intersect_key($context, $_parent) + $_parent;
        // line 37
        echo "    <tr>
        <td colspan=\"4\" style=\"padding: 10px; font-size: 20px; text-align: right;\">";
        // line 38
        echo $this->getAttribute((isset($context["Lang"]) ? $context["Lang"] : null), "get", array(0 => "order.total"), "method");
        echo " <b>";
        echo call_user_func_array($this->env->getFilter('price_format')->getCallable(), array((isset($context["totalPrice"]) ? $context["totalPrice"] : null)));
        echo "</b> ";
        echo $this->getAttribute((isset($context["Lang"]) ? $context["Lang"] : null), "get", array(0 => "order.current_currency"), "method");
        echo "</td>
    </tr>
</table>";
    }

    public function getTemplateName()
    {
        return "mail.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  136 => 38,  133 => 37,  124 => 34,  120 => 33,  116 => 32,  113 => 31,  102 => 30,  98 => 28,  94 => 27,  89 => 25,  85 => 24,  81 => 23,  77 => 22,  72 => 19,  68 => 17,  62 => 16,  56 => 13,  51 => 11,  47 => 9,  44 => 8,  40 => 7,  37 => 6,  35 => 5,  28 => 3,  22 => 2,  19 => 1,);
    }
}
