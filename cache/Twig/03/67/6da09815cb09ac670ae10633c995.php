<?php

/* /el/file.twig */
class __TwigTemplate_03676da09815cb09ac670ae10633c995 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        // line 1
        echo "<div class=\"form__col-";
        echo $this->getAttribute((isset($context["oField"]) ? $context["oField"] : null), "getClassVal", array(), "method");
        echo "\">
    <div class=\"form__item form__item--label-";
        // line 2
        echo $this->getAttribute((isset($context["oField"]) ? $context["oField"] : null), "getVal", array(0 => "label_position"), "method");
        echo "\">
        ";
        // line 3
        if (($this->getAttribute((isset($context["oField"]) ? $context["oField"] : null), "getVal", array(0 => "group"), "method") == "0")) {
            // line 4
            echo "            ";
            if (($this->getAttribute((isset($context["oField"]) ? $context["oField"] : null), "getVal", array(0 => "label_position"), "method") != "right")) {
                // line 5
                echo "                <div class=\"form__label\">";
                echo $this->getAttribute((isset($context["oField"]) ? $context["oField"] : null), "getVal", array(0 => "param_title"), "method");
                if (($this->getAttribute((isset($context["oField"]) ? $context["oField"] : null), "getVal", array(0 => "param_required"), "method") == 1)) {
                    echo " <ins class=\"form__mark\">*</ins>";
                }
                echo "</div>
            ";
            }
            // line 7
            echo "        ";
        }
        // line 8
        echo "        <div class=\"form__input form__input--file js-inputfile\">
            <input class=\"file_upload js-inputfile-input\" data-hash=\"";
        // line 9
        echo (isset($context["formHash"]) ? $context["formHash"] : null);
        echo "\" data-name=\"";
        echo $this->getAttribute((isset($context["oField"]) ? $context["oField"] : null), "getVal", array(0 => "param_title"), "method");
        echo "\" data-multiple-caption=\"Файлов выбрано {count}\" type=\"file\" name=\"";
        echo $this->getAttribute((isset($context["oField"]) ? $context["oField"] : null), "getVal", array(0 => "param_name"), "method");
        echo "\" id=\"file-";
        echo $this->getAttribute((isset($context["oField"]) ? $context["oField"] : null), "getVal", array(0 => "param_name"), "method");
        echo "_";
        echo (isset($context["formHash"]) ? $context["formHash"] : null);
        echo "\"  ";
        echo $this->getAttribute((isset($context["oField"]) ? $context["oField"] : null), "getVal", array(0 => "param_man_params"), "method");
        echo " />
            <label class=\"js-inputfile-label\" for=\"file-";
        // line 10
        echo $this->getAttribute((isset($context["oField"]) ? $context["oField"] : null), "getVal", array(0 => "param_name"), "method");
        echo "_";
        echo (isset($context["formHash"]) ? $context["formHash"] : null);
        echo "\"><span>&nbsp;</span><strong>Выберите файл</strong></label>
        </div>
        ";
        // line 12
        if (($this->getAttribute((isset($context["oField"]) ? $context["oField"] : null), "getVal", array(0 => "label_position"), "method") == "right")) {
            // line 13
            echo "            <div class=\"form__label\">";
            echo $this->getAttribute((isset($context["oField"]) ? $context["oField"] : null), "getVal", array(0 => "param_title"), "method");
            if (($this->getAttribute((isset($context["oField"]) ? $context["oField"] : null), "getVal", array(0 => "param_required"), "method") == 1)) {
                echo " <ins class=\"form__mark\">*</ins>";
            }
            echo "</div>
        ";
        }
        // line 15
        echo "        ";
        if (($this->getAttribute((isset($context["oField"]) ? $context["oField"] : null), "getVal", array(0 => "param_description"), "method") != "")) {
            echo "<div class=\"form__info\">";
            echo $this->getAttribute((isset($context["oField"]) ? $context["oField"] : null), "getVal", array(0 => "param_description"), "method");
            echo "</div>";
        }
        // line 16
        echo "    </div>
</div>

";
    }

    public function getTemplateName()
    {
        return "/el/file.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  80 => 15,  71 => 13,  104 => 20,  98 => 19,  95 => 18,  86 => 16,  84 => 15,  60 => 10,  85 => 15,  75 => 12,  48 => 9,  45 => 8,  42 => 7,  33 => 5,  30 => 4,  24 => 2,  346 => 108,  341 => 105,  339 => 104,  334 => 102,  327 => 101,  320 => 100,  314 => 99,  310 => 98,  306 => 97,  302 => 96,  298 => 95,  294 => 93,  289 => 90,  283 => 88,  277 => 86,  275 => 85,  271 => 83,  266 => 80,  260 => 78,  254 => 76,  252 => 75,  249 => 74,  241 => 72,  239 => 71,  236 => 70,  234 => 69,  225 => 63,  219 => 62,  213 => 59,  209 => 58,  204 => 56,  201 => 55,  195 => 52,  192 => 51,  190 => 50,  187 => 49,  180 => 45,  170 => 40,  165 => 38,  158 => 36,  152 => 32,  149 => 31,  147 => 30,  143 => 28,  135 => 23,  129 => 22,  124 => 20,  120 => 18,  118 => 17,  115 => 16,  101 => 18,  97 => 14,  76 => 13,  66 => 10,  64 => 11,  62 => 10,  34 => 5,  28 => 3,  26 => 2,  102 => 24,  99 => 23,  96 => 22,  94 => 17,  87 => 16,  83 => 14,  78 => 14,  73 => 12,  69 => 12,  63 => 8,  59 => 7,  55 => 6,  51 => 5,  47 => 4,  39 => 3,  36 => 2,  19 => 1,);
    }
}
