<?php

/* gallery.twig */
class __TwigTemplate_63c0b3a218b3dc51134ce59009319fe9 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        // line 1
        echo "<div class=\"b-category\"";
        echo $this->getAttribute((isset($context["Design"]) ? $context["Design"] : null), "write", array(0 => " sktag=\"modules.category.gal\""), "method");
        echo ">

    ";
        // line 3
        $context['_parent'] = (array) $context;
        $context['_seq'] = twig_ensure_traversable((isset($context["list"]) ? $context["list"] : null));
        foreach ($context['_seq'] as $context["_key"] => $context["oItem"]) {
            // line 4
            echo "    <div class=\"category__item\">
        <div class=\"category__imgbox\">
            <a href=\"";
            // line 6
            echo $this->getAttribute((isset($context["oItem"]) ? $context["oItem"] : null), "href");
            echo "\">
                <img alt=\"";
            // line 7
            if ($this->getAttribute($this->getAttribute((isset($context["oItem"]) ? $context["oItem"] : null), "img"), "alt_title")) {
                echo $this->getAttribute($this->getAttribute((isset($context["oItem"]) ? $context["oItem"] : null), "img"), "alt_title");
            }
            echo "\" src=\"";
            if ($this->getAttribute($this->getAttribute($this->getAttribute($this->getAttribute((isset($context["oItem"]) ? $context["oItem"] : null), "img"), "images_data"), "preview"), "file")) {
                echo $this->getAttribute($this->getAttribute($this->getAttribute($this->getAttribute((isset($context["oItem"]) ? $context["oItem"] : null), "img"), "images_data"), "preview"), "file");
            } else {
                echo $this->getAttribute((isset($context["Design"]) ? $context["Design"] : null), "get", array(0 => "modules.category.gal", 1 => "noimage"), "method");
            }
            echo "\" />
            </a>
        </div>

        <div class=\"category__title\"><a href=\"";
            // line 11
            echo $this->getAttribute((isset($context["oItem"]) ? $context["oItem"] : null), "href");
            echo "\">";
            echo $this->getAttribute((isset($context["oItem"]) ? $context["oItem"] : null), "title");
            echo "</a></div>
        ";
            // line 12
            if ($this->getAttribute((isset($context["oItem"]) ? $context["oItem"] : null), "description")) {
                echo "<div class=\"category__text b-editor b-editor-nobot\">";
                echo $this->getAttribute((isset($context["oItem"]) ? $context["oItem"] : null), "description");
                echo "</div>";
            }
            // line 13
            echo "        
    </div>
    ";
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['_key'], $context['oItem'], $context['_parent'], $context['loop']);
        $context = array_intersect_key($context, $_parent) + $_parent;
        // line 16
        echo "
</div>
";
    }

    public function getTemplateName()
    {
        return "gallery.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  72 => 16,  64 => 13,  37 => 7,  33 => 6,  29 => 4,  25 => 3,  310 => 74,  304 => 70,  296 => 67,  278 => 65,  273 => 64,  270 => 63,  260 => 61,  258 => 60,  254 => 58,  248 => 56,  246 => 55,  242 => 53,  238 => 51,  232 => 50,  228 => 49,  225 => 48,  218 => 44,  209 => 42,  203 => 41,  200 => 40,  194 => 39,  190 => 37,  182 => 36,  173 => 35,  165 => 33,  146 => 31,  142 => 30,  130 => 29,  125 => 26,  119 => 24,  116 => 23,  110 => 21,  107 => 20,  101 => 18,  99 => 17,  95 => 15,  80 => 14,  66 => 13,  58 => 12,  55 => 10,  52 => 11,  48 => 8,  27 => 4,  24 => 3,  21 => 2,  237 => 49,  223 => 47,  219 => 46,  215 => 43,  201 => 43,  197 => 41,  193 => 39,  179 => 38,  170 => 34,  159 => 32,  156 => 32,  139 => 30,  136 => 29,  134 => 28,  128 => 25,  124 => 24,  109 => 21,  106 => 20,  89 => 19,  86 => 18,  84 => 17,  77 => 14,  73 => 12,  67 => 10,  61 => 12,  43 => 5,  40 => 4,  23 => 3,  65 => 9,  53 => 9,  41 => 7,  39 => 6,  36 => 5,  32 => 3,  30 => 2,  19 => 1,);
    }
}
