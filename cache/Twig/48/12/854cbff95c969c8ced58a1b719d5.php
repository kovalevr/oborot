<?php

/* AuthFormMiniHead.twig */
class __TwigTemplate_4812854cbff95c969c8ced58a1b719d5 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        // line 1
        echo "<div class=\"b-authmain-head";
        if ((isset($context["designMode"]) ? $context["designMode"] : null)) {
            echo " g-ramaborder js-designDrag-right";
        }
        echo "\"";
        if ((isset($context["designMode"]) ? $context["designMode"] : null)) {
            echo " sktag=\"modules.auth.authhead\"";
        }
        echo ">
    ";
        // line 2
        if ((isset($context["designMode"]) ? $context["designMode"] : null)) {
            // line 3
            echo "        <div class=\"b-desbtn\"><span>Auth</span><ins></ins></div>
    ";
        }
        // line 5
        echo "
    ";
        // line 6
        if ((isset($context["current_user"]) ? $context["current_user"] : null)) {
            // line 7
            echo "        <a href=\"";
            echo (isset($context["profile_url"]) ? $context["profile_url"] : null);
            echo "\">";
            echo $this->getAttribute((isset($context["Lang"]) ? $context["Lang"] : null), "get", array(0 => "auth.profile"), "method");
            echo "</a><span>/</span><a href=\"";
            echo (isset($context["auth_url"]) ? $context["auth_url"] : null);
            echo "?cmd=logout\">";
            echo $this->getAttribute((isset($context["Lang"]) ? $context["Lang"] : null), "get", array(0 => "auth.logout"), "method");
            echo "</a>
    ";
        } else {
            // line 9
            echo "        <a href=\"";
            echo (isset($context["auth_url"]) ? $context["auth_url"] : null);
            echo "\">";
            echo $this->getAttribute((isset($context["Lang"]) ? $context["Lang"] : null), "get", array(0 => "auth.login_head"), "method");
            echo "</a><span>/</span><a href=\"";
            echo (isset($context["auth_url"]) ? $context["auth_url"] : null);
            echo "?cmd=register\">";
            echo $this->getAttribute((isset($context["Lang"]) ? $context["Lang"] : null), "get", array(0 => "auth.reg_head"), "method");
            echo "</a>
    ";
        }
        // line 11
        echo "</div>
";
    }

    public function getTemplateName()
    {
        return "AuthFormMiniHead.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  65 => 11,  53 => 9,  41 => 7,  39 => 6,  36 => 5,  32 => 3,  30 => 2,  19 => 1,);
    }
}
