<?php

/* MicroDataReviews.twig */
class __TwigTemplate_a80779033099f78e370ab8f1bcc43fcd extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        // line 1
        $context['_parent'] = (array) $context;
        $context['_seq'] = twig_ensure_traversable((isset($context["items"]) ? $context["items"] : null));
        $context['loop'] = array(
          'parent' => $context['_parent'],
          'index0' => 0,
          'index'  => 1,
          'first'  => true,
        );
        if (is_array($context['_seq']) || (is_object($context['_seq']) && $context['_seq'] instanceof Countable)) {
            $length = count($context['_seq']);
            $context['loop']['revindex0'] = $length - 1;
            $context['loop']['revindex'] = $length;
            $context['loop']['length'] = $length;
            $context['loop']['last'] = 1 === $length;
        }
        foreach ($context['_seq'] as $context["_key"] => $context["aItem"]) {
            // line 2
            echo "
    <div ";
            // line 3
            if ((isset($context["bIsReview4Good"]) ? $context["bIsReview4Good"] : null)) {
                echo "itemprop=\"review\"";
            }
            echo " itemscope itemtype=\"";
            echo $this->getAttribute((isset($context["Site"]) ? $context["Site"] : null), "getWebProtocol", array(), "method");
            echo "schema.org/Review\" style=\"display:none\">
        <a itemprop=\"url\" href=\"";
            // line 4
            echo twig_escape_filter($this->env, (isset($context["url"]) ? $context["url"] : null));
            echo "\" ></a>
        <div itemprop=\"author\" itemscope itemtype=\"";
            // line 5
            echo $this->getAttribute((isset($context["Site"]) ? $context["Site"] : null), "getWebProtocol", array(), "method");
            echo "schema.org/Person\">
            <div itemprop=\"name\">";
            // line 6
            echo $this->getAttribute((isset($context["aItem"]) ? $context["aItem"] : null), "name");
            echo "</div>
            <div itemprop=\"email\">";
            // line 7
            echo $this->getAttribute((isset($context["aItem"]) ? $context["aItem"] : null), "email");
            echo "</div>
            <div itemprop=\"address\">";
            // line 8
            echo $this->getAttribute((isset($context["aItem"]) ? $context["aItem"] : null), "city");
            echo "</div>
        </div>

        <div itemprop=\"datePublished\">";
            // line 11
            echo twig_date_format_filter($this->env, $this->getAttribute((isset($context["aItem"]) ? $context["aItem"] : null), "date_time"), "Y-m-d");
            echo "</div>
        <div itemprop=\"reviewBody\">";
            // line 12
            echo $this->getAttribute((isset($context["aItem"]) ? $context["aItem"] : null), "content");
            echo "</div>

        <div itemprop=\"reviewRating\" itemscope itemtype=\"";
            // line 14
            echo $this->getAttribute((isset($context["Site"]) ? $context["Site"] : null), "getWebProtocol", array(), "method");
            echo "schema.org/AggregateRating\">
            <span itemprop=\"worstRating\">0</span>
            <span itemprop=\"ratingValue\">";
            // line 16
            echo $this->getAttribute((isset($context["aItem"]) ? $context["aItem"] : null), "rating");
            echo "</span>
            <span itemprop=\"bestRating\">";
            // line 17
            echo $this->getAttribute((isset($context["aItem"]) ? $context["aItem"] : null), "rating");
            echo "</span>
            <span itemprop=\"ratingCount\">1</span>
        </div>

        ";
            // line 21
            if ((!(isset($context["bIsReview4Good"]) ? $context["bIsReview4Good"] : null))) {
                // line 22
                echo "            ";
                $this->env->loadTemplate("MicroDataOrganization.twig")->display($context);
                // line 23
                echo "        ";
            }
            // line 24
            echo "
    </div>
";
            ++$context['loop']['index0'];
            ++$context['loop']['index'];
            $context['loop']['first'] = false;
            if (isset($context['loop']['length'])) {
                --$context['loop']['revindex0'];
                --$context['loop']['revindex'];
                $context['loop']['last'] = 0 === $context['loop']['revindex0'];
            }
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['_key'], $context['aItem'], $context['_parent'], $context['loop']);
        $context = array_intersect_key($context, $_parent) + $_parent;
    }

    public function getTemplateName()
    {
        return "MicroDataReviews.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  102 => 24,  99 => 23,  96 => 22,  94 => 21,  87 => 17,  83 => 16,  78 => 14,  73 => 12,  69 => 11,  63 => 8,  59 => 7,  55 => 6,  51 => 5,  47 => 4,  39 => 3,  36 => 2,  19 => 1,);
    }
}
