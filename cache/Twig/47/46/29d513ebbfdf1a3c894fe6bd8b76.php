<?php

/* log_template.twig */
class __TwigTemplate_474629d513ebbfdf1a3c894fe6bd8b76 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        // line 1
        echo "<div style=\"margin-left: 30px;\">
    ";
        // line 2
        if ($this->getAttribute((isset($context["log"]) ? $context["log"] : null), "copy_file", array(), "any", true, true)) {
            // line 3
            echo "        <p>";
            echo $this->getAttribute((isset($context["Lang"]) ? $context["Lang"] : null), "get", array(0 => "import.copy_file"), "method");
            echo ": ";
            echo $this->getAttribute((isset($context["log"]) ? $context["log"] : null), "copy_file");
            echo "</p>
    ";
        }
        // line 5
        echo "    <p>";
        echo $this->getAttribute((isset($context["Lang"]) ? $context["Lang"] : null), "get", array(0 => "import.status"), "method");
        echo ": ";
        echo $this->getAttribute((isset($context["log"]) ? $context["log"] : null), "status");
        echo "</p>
    <p>";
        // line 6
        echo $this->getAttribute((isset($context["Lang"]) ? $context["Lang"] : null), "get", array(0 => "import.start"), "method");
        echo ": ";
        echo $this->getAttribute((isset($context["log"]) ? $context["log"] : null), "start");
        echo "</p>
    ";
        // line 7
        if ($this->getAttribute((isset($context["log"]) ? $context["log"] : null), "finish")) {
            echo "<p>";
            echo $this->getAttribute((isset($context["Lang"]) ? $context["Lang"] : null), "get", array(0 => "import.finish"), "method");
            echo ": ";
            echo $this->getAttribute((isset($context["log"]) ? $context["log"] : null), "finish");
            echo "</p>";
        }
        // line 8
        echo "
    <hr>

    ";
        // line 11
        if ($this->getAttribute((isset($context["log"]) ? $context["log"] : null), "new", array(), "any", true, true)) {
            // line 12
            echo "        <div>
            <p>";
            // line 13
            echo $this->getAttribute((isset($context["Lang"]) ? $context["Lang"] : null), "get", array(0 => "import.added"), "method");
            echo ": ";
            echo $this->getAttribute((isset($context["log"]) ? $context["log"] : null), "new");
            echo " </p>
        </div>
    ";
        }
        // line 16
        echo "    ";
        if ($this->getAttribute((isset($context["log"]) ? $context["log"] : null), "update", array(), "any", true, true)) {
            // line 17
            echo "        <div>
            <p>";
            // line 18
            echo $this->getAttribute((isset($context["Lang"]) ? $context["Lang"] : null), "get", array(0 => "import.update"), "method");
            echo ": ";
            echo $this->getAttribute((isset($context["log"]) ? $context["log"] : null), "update");
            echo "</p>
        </div>
    ";
        }
        // line 21
        echo "    ";
        if ($this->getAttribute((isset($context["log"]) ? $context["log"] : null), "skip", array(), "any", true, true)) {
            // line 22
            echo "        <div>
            <p><b>";
            // line 23
            echo $this->getAttribute((isset($context["Lang"]) ? $context["Lang"] : null), "get", array(0 => "import.skip"), "method");
            echo ": ";
            echo $this->getAttribute((isset($context["log"]) ? $context["log"] : null), "skip");
            echo "</b></p>
        </div>
    ";
        }
        // line 26
        echo "    ";
        if ($this->getAttribute((isset($context["log"]) ? $context["log"] : null), "not_activity", array(), "any", true, true)) {
            // line 27
            echo "        <div>
            <p><b>";
            // line 28
            echo $this->getAttribute((isset($context["Lang"]) ? $context["Lang"] : null), "get", array(0 => "import.not_activity"), "method");
            echo ": ";
            echo $this->getAttribute((isset($context["log"]) ? $context["log"] : null), "not_activity");
            echo "</b></p>
        </div>
    ";
        }
        // line 31
        echo "    ";
        if ($this->getAttribute((isset($context["log"]) ? $context["log"] : null), "delete", array(), "any", true, true)) {
            // line 32
            echo "        <div>
            <p>";
            // line 33
            echo $this->getAttribute((isset($context["Lang"]) ? $context["Lang"] : null), "get", array(0 => "import.delete"), "method");
            echo ": ";
            echo $this->getAttribute((isset($context["log"]) ? $context["log"] : null), "delete");
            echo " </p>
        </div>
    ";
        }
        // line 36
        echo "    ";
        if ($this->getAttribute((isset($context["log"]) ? $context["log"] : null), "error", array(), "any", true, true)) {
            // line 37
            echo "        <div>
            <p><b>";
            // line 38
            echo $this->getAttribute((isset($context["Lang"]) ? $context["Lang"] : null), "get", array(0 => "import.error"), "method");
            echo ": ";
            echo $this->getAttribute((isset($context["log"]) ? $context["log"] : null), "error");
            echo "</b></p>
        </div>
    ";
        }
        // line 41
        echo "    ";
        if ($this->getAttribute((isset($context["log"]) ? $context["log"] : null), "add_photo", array(), "any", true, true)) {
            // line 42
            echo "        <div>
            <p>";
            // line 43
            echo $this->getAttribute((isset($context["Lang"]) ? $context["Lang"] : null), "get", array(0 => "import.add_photo"), "method");
            echo ": ";
            echo $this->getAttribute((isset($context["log"]) ? $context["log"] : null), "add_photo");
            echo " </p>
        </div>
    ";
        }
        // line 46
        echo "    ";
        if ($this->getAttribute((isset($context["log"]) ? $context["log"] : null), "no_section", array(), "any", true, true)) {
            // line 47
            echo "        <div>
            <p><b>";
            // line 48
            echo $this->getAttribute((isset($context["Lang"]) ? $context["Lang"] : null), "get", array(0 => "import.no_section"), "method");
            echo ":</b></p>
            <table>
                ";
            // line 50
            $context['_parent'] = (array) $context;
            $context['_seq'] = twig_ensure_traversable($this->getAttribute((isset($context["log"]) ? $context["log"] : null), "no_section"));
            foreach ($context['_seq'] as $context["_key"] => $context["item"]) {
                // line 51
                echo "                    <tr>
                        <td>";
                // line 52
                echo (isset($context["item"]) ? $context["item"] : null);
                echo "</td>
                    </tr>
                ";
            }
            $_parent = $context['_parent'];
            unset($context['_seq'], $context['_iterated'], $context['_key'], $context['item'], $context['_parent'], $context['loop']);
            $context = array_intersect_key($context, $_parent) + $_parent;
            // line 55
            echo "            </table>
        </div>
    ";
        }
        // line 58
        echo "    ";
        if ($this->getAttribute((isset($context["log"]) ? $context["log"] : null), "create_section", array(), "any", true, true)) {
            // line 59
            echo "        <div>
            <p><b>";
            // line 60
            echo $this->getAttribute((isset($context["Lang"]) ? $context["Lang"] : null), "get", array(0 => "import.create_section"), "method");
            echo ":</b></p>
            <table>
                ";
            // line 62
            $context['_parent'] = (array) $context;
            $context['_seq'] = twig_ensure_traversable($this->getAttribute((isset($context["log"]) ? $context["log"] : null), "create_section_list"));
            foreach ($context['_seq'] as $context["_key"] => $context["item"]) {
                // line 63
                echo "                    <tr>
                        <td>";
                // line 64
                echo (isset($context["item"]) ? $context["item"] : null);
                echo "</td>
                    </tr>
                ";
            }
            $_parent = $context['_parent'];
            unset($context['_seq'], $context['_iterated'], $context['_key'], $context['item'], $context['_parent'], $context['loop']);
            $context = array_intersect_key($context, $_parent) + $_parent;
            // line 67
            echo "            </table>
        </div>
    ";
        }
        // line 70
        echo "
    <hr>

    ";
        // line 73
        if ($this->getAttribute((isset($context["log"]) ? $context["log"] : null), "new_list", array(), "any", true, true)) {
            // line 74
            echo "    <div style=\"margin-top: 15px;\">
        <p><b>";
            // line 75
            echo $this->getAttribute((isset($context["Lang"]) ? $context["Lang"] : null), "get", array(0 => "import.added"), "method");
            echo ":</b></p>
        <table>
            ";
            // line 77
            $context['_parent'] = (array) $context;
            $context['_seq'] = twig_ensure_traversable($this->getAttribute((isset($context["log"]) ? $context["log"] : null), "new_list"));
            foreach ($context['_seq'] as $context["_key"] => $context["item"]) {
                // line 78
                echo "                <tr>
                    <td>";
                // line 79
                echo (isset($context["item"]) ? $context["item"] : null);
                echo "</td>
                </tr>
            ";
            }
            $_parent = $context['_parent'];
            unset($context['_seq'], $context['_iterated'], $context['_key'], $context['item'], $context['_parent'], $context['loop']);
            $context = array_intersect_key($context, $_parent) + $_parent;
            // line 82
            echo "        </table>
    </div>
    ";
        }
        // line 85
        echo "
    ";
        // line 86
        if ($this->getAttribute((isset($context["log"]) ? $context["log"] : null), "update_list", array(), "any", true, true)) {
            // line 87
            echo "    <div style=\"margin-top: 15px;\">
        <p><b>";
            // line 88
            echo $this->getAttribute((isset($context["Lang"]) ? $context["Lang"] : null), "get", array(0 => "import.update"), "method");
            echo ":</b></p>
        <table>
            ";
            // line 90
            $context['_parent'] = (array) $context;
            $context['_seq'] = twig_ensure_traversable($this->getAttribute((isset($context["log"]) ? $context["log"] : null), "update_list"));
            foreach ($context['_seq'] as $context["_key"] => $context["item"]) {
                // line 91
                echo "                <tr>
                    <td>";
                // line 92
                echo (isset($context["item"]) ? $context["item"] : null);
                echo "</td>
                </tr>
            ";
            }
            $_parent = $context['_parent'];
            unset($context['_seq'], $context['_iterated'], $context['_key'], $context['item'], $context['_parent'], $context['loop']);
            $context = array_intersect_key($context, $_parent) + $_parent;
            // line 95
            echo "        </table>
    </div>
    ";
        }
        // line 98
        echo "
    ";
        // line 99
        if ($this->getAttribute((isset($context["log"]) ? $context["log"] : null), "skip_list", array(), "any", true, true)) {
            // line 100
            echo "    <div style=\"margin-top: 15px;\">
        <p><b>";
            // line 101
            echo $this->getAttribute((isset($context["Lang"]) ? $context["Lang"] : null), "get", array(0 => "import.skip"), "method");
            echo ":</b></p>
        <table>
            ";
            // line 103
            $context['_parent'] = (array) $context;
            $context['_seq'] = twig_ensure_traversable($this->getAttribute((isset($context["log"]) ? $context["log"] : null), "skip_list"));
            foreach ($context['_seq'] as $context["_key"] => $context["item"]) {
                // line 104
                echo "                <tr>
                    <td>";
                // line 105
                echo (isset($context["item"]) ? $context["item"] : null);
                echo "</td>
                </tr>
            ";
            }
            $_parent = $context['_parent'];
            unset($context['_seq'], $context['_iterated'], $context['_key'], $context['item'], $context['_parent'], $context['loop']);
            $context = array_intersect_key($context, $_parent) + $_parent;
            // line 108
            echo "        </table>
    </div>
    ";
        }
        // line 111
        echo "
    ";
        // line 112
        if ($this->getAttribute((isset($context["log"]) ? $context["log"] : null), "not_activity_list", array(), "any", true, true)) {
            // line 113
            echo "        <div style=\"margin-top: 15px;\">
            <p><b>";
            // line 114
            echo $this->getAttribute((isset($context["Lang"]) ? $context["Lang"] : null), "get", array(0 => "import.not_activity_list"), "method");
            echo ":</b></p>
            <table>
                ";
            // line 116
            $context['_parent'] = (array) $context;
            $context['_seq'] = twig_ensure_traversable($this->getAttribute((isset($context["log"]) ? $context["log"] : null), "not_activity_list"));
            foreach ($context['_seq'] as $context["_key"] => $context["item"]) {
                // line 117
                echo "                    <tr>
                        <td>";
                // line 118
                echo (isset($context["item"]) ? $context["item"] : null);
                echo "</td>
                    </tr>
                ";
            }
            $_parent = $context['_parent'];
            unset($context['_seq'], $context['_iterated'], $context['_key'], $context['item'], $context['_parent'], $context['loop']);
            $context = array_intersect_key($context, $_parent) + $_parent;
            // line 121
            echo "            </table>
        </div>
    ";
        }
        // line 124
        echo "
    ";
        // line 125
        if ($this->getAttribute((isset($context["log"]) ? $context["log"] : null), "delete_list", array(), "any", true, true)) {
            // line 126
            echo "    <div style=\"margin-top: 15px;\">
        <p><b>";
            // line 127
            echo $this->getAttribute((isset($context["Lang"]) ? $context["Lang"] : null), "get", array(0 => "import.delete"), "method");
            echo ":</b></p>
        <table>
            ";
            // line 129
            $context['_parent'] = (array) $context;
            $context['_seq'] = twig_ensure_traversable($this->getAttribute((isset($context["log"]) ? $context["log"] : null), "delete_list"));
            foreach ($context['_seq'] as $context["_key"] => $context["item"]) {
                // line 130
                echo "                <tr>
                    <td>";
                // line 131
                echo (isset($context["item"]) ? $context["item"] : null);
                echo "</td>
                </tr>
            ";
            }
            $_parent = $context['_parent'];
            unset($context['_seq'], $context['_iterated'], $context['_key'], $context['item'], $context['_parent'], $context['loop']);
            $context = array_intersect_key($context, $_parent) + $_parent;
            // line 134
            echo "        </table>
    </div>
    ";
        }
        // line 137
        echo "
    ";
        // line 138
        if ($this->getAttribute((isset($context["log"]) ? $context["log"] : null), "error_list", array(), "any", true, true)) {
            // line 139
            echo "        <div style=\"margin-top: 15px;\">
            <p><b>";
            // line 140
            echo $this->getAttribute((isset($context["Lang"]) ? $context["Lang"] : null), "get", array(0 => "import.error_list"), "method");
            echo ":</b></p>
            <table>
                ";
            // line 142
            $context['_parent'] = (array) $context;
            $context['_seq'] = twig_ensure_traversable($this->getAttribute((isset($context["log"]) ? $context["log"] : null), "error_list"));
            foreach ($context['_seq'] as $context["_key"] => $context["item"]) {
                // line 143
                echo "                    <tr>
                        <td>";
                // line 144
                echo (isset($context["item"]) ? $context["item"] : null);
                echo "</td>
                    </tr>
                ";
            }
            $_parent = $context['_parent'];
            unset($context['_seq'], $context['_iterated'], $context['_key'], $context['item'], $context['_parent'], $context['loop']);
            $context = array_intersect_key($context, $_parent) + $_parent;
            // line 147
            echo "            </table>
        </div>
    ";
        }
        // line 150
        echo "
</div>";
    }

    public function getTemplateName()
    {
        return "log_template.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  425 => 150,  420 => 147,  411 => 144,  408 => 143,  404 => 142,  399 => 140,  396 => 139,  394 => 138,  391 => 137,  386 => 134,  377 => 131,  374 => 130,  370 => 129,  365 => 127,  362 => 126,  360 => 125,  357 => 124,  352 => 121,  343 => 118,  340 => 117,  336 => 116,  331 => 114,  328 => 113,  326 => 112,  323 => 111,  318 => 108,  309 => 105,  306 => 104,  302 => 103,  297 => 101,  294 => 100,  292 => 99,  289 => 98,  284 => 95,  275 => 92,  272 => 91,  268 => 90,  263 => 88,  260 => 87,  258 => 86,  255 => 85,  250 => 82,  241 => 79,  238 => 78,  234 => 77,  229 => 75,  226 => 74,  224 => 73,  219 => 70,  214 => 67,  205 => 64,  202 => 63,  198 => 62,  193 => 60,  190 => 59,  187 => 58,  182 => 55,  173 => 52,  170 => 51,  166 => 50,  161 => 48,  158 => 47,  155 => 46,  147 => 43,  144 => 42,  141 => 41,  133 => 38,  130 => 37,  127 => 36,  119 => 33,  116 => 32,  113 => 31,  105 => 28,  102 => 27,  99 => 26,  91 => 23,  88 => 22,  85 => 21,  77 => 18,  74 => 17,  71 => 16,  63 => 13,  60 => 12,  58 => 11,  53 => 8,  45 => 7,  39 => 6,  32 => 5,  24 => 3,  22 => 2,  19 => 1,);
    }
}
