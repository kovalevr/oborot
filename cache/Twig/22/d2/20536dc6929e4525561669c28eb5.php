<?php

/* SimpleList.twig */
class __TwigTemplate_22d220536dc6929e4525561669c28eb5 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        // line 1
        $this->env->loadTemplate("FilterList.twig")->display($context);
        // line 2
        echo "<div class=\"b-catalogbox b-catalogbox-list ";
        if (((isset($context["show_shadows"]) ? $context["show_shadows"] : null) == "enabled")) {
            echo "b-catalogbox-boxshadow";
        }
        echo " js_goods_container\">
    ";
        // line 3
        $this->env->loadTemplate("ListItems.twig")->display($context);
        // line 4
        echo "</div>

";
        // line 7
        $this->env->loadTemplate("paginator.twig")->display($context);
    }

    public function getTemplateName()
    {
        return "SimpleList.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  34 => 7,  30 => 4,  28 => 3,  21 => 2,  19 => 1,);
    }
}
