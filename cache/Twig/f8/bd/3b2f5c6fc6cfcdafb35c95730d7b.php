<?php

/* done.twig */
class __TwigTemplate_f8bd3b2f5c6fc6cfcdafb35c95730d7b extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        // line 1
        if ((isset($context["ecommerce"]) ? $context["ecommerce"] : null)) {
            echo (isset($context["ecommerce"]) ? $context["ecommerce"] : null);
        }
        // line 2
        if (array_key_exists("paymentForm", $context)) {
            // line 3
            echo $this->getAttribute((isset($context["Lang"]) ? $context["Lang"] : null), "get", array(0 => "order.message_pay"), "method");
            echo "
<p>";
            // line 4
            echo (isset($context["paymentForm"]) ? $context["paymentForm"] : null);
            echo "</p>
";
        } else {
            // line 6
            echo "<p>";
            echo $this->getAttribute((isset($context["Lang"]) ? $context["Lang"] : null), "get", array(0 => "order.message_pay2"), "method");
            echo "</p>
<a href=\"[";
            // line 7
            echo (isset($context["mainSection"]) ? $context["mainSection"] : null);
            echo "]\">";
            echo $this->getAttribute((isset($context["Lang"]) ? $context["Lang"] : null), "get", array(0 => "order.to_main"), "method");
            echo "</a>
";
        }
    }

    public function getTemplateName()
    {
        return "done.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  39 => 7,  34 => 6,  29 => 4,  25 => 3,  23 => 2,  19 => 1,);
    }
}
