<?php

/* SimpleDetail.twig */
class __TwigTemplate_f721101adb1ee72cf7822d3be9ce1d2a extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        // line 1
        echo "<div class=\"b-catalogbox b-catalogbox-detal\">
    ";
        // line 2
        if (array_key_exists("aObject", $context)) {
            // line 3
            echo "        ";
            $this->env->loadTemplate("MicroData.twig")->display($context);
            // line 4
            echo "        <div class=\"catalogbox__item js_ecommerce_detailPage js_catalogbox_item\" data-ecommerce='";
            echo $this->getAttribute((isset($context["aObject"]) ? $context["aObject"] : null), "ecommerce");
            echo "' >
            <div class=\"catalogbox__leftcol\">
                <div class=\"catalogbox__galbox\">
                    ";
            // line 7
            if ((twig_length_filter($this->env, $this->getAttribute($this->getAttribute($this->getAttribute($this->getAttribute((isset($context["aObject"]) ? $context["aObject"] : null), "fields"), "gallery"), "gallery"), "images")) > 0)) {
                // line 8
                echo "                        <div class=\"js-catalog-detail-fotorama\" data-thumbwidth=\"100\" data-thumbwidth=\"80\">
                            ";
                // line 9
                if ($this->getAttribute($this->getAttribute((isset($context["aObject"]) ? $context["aObject"] : null), "fields", array(), "any", false, true), "gallery", array(), "any", true, true)) {
                    // line 10
                    echo "                                ";
                    $context['_parent'] = (array) $context;
                    $context['_seq'] = twig_ensure_traversable($this->getAttribute($this->getAttribute($this->getAttribute($this->getAttribute((isset($context["aObject"]) ? $context["aObject"] : null), "fields"), "gallery"), "gallery"), "images"));
                    foreach ($context['_seq'] as $context["iKey"] => $context["aImage"]) {
                        // line 11
                        echo "                                    <img data-full=\"";
                        echo $this->getAttribute($this->getAttribute($this->getAttribute((isset($context["aImage"]) ? $context["aImage"] : null), "images_data"), "big"), "file");
                        echo "\" src=\"";
                        echo $this->getAttribute($this->getAttribute($this->getAttribute((isset($context["aImage"]) ? $context["aImage"] : null), "images_data"), "medium", array(), "array"), "file");
                        echo "\" alt=\"";
                        echo twig_escape_filter($this->env, $this->getAttribute((isset($context["aImage"]) ? $context["aImage"] : null), "alt_title"));
                        echo "\"  title=\"";
                        echo twig_escape_filter($this->env, $this->getAttribute((isset($context["aImage"]) ? $context["aImage"] : null), "title"));
                        echo "\" />
                                ";
                    }
                    $_parent = $context['_parent'];
                    unset($context['_seq'], $context['_iterated'], $context['iKey'], $context['aImage'], $context['_parent'], $context['loop']);
                    $context = array_intersect_key($context, $_parent) + $_parent;
                    // line 13
                    echo "                            ";
                }
                // line 14
                echo "                        </div>
                    ";
            } else {
                // line 16
                echo "                        <img alt=\"\" src=\"";
                echo $this->getAttribute((isset($context["Design"]) ? $context["Design"] : null), "get", array(0 => "modules.catalogbox.detal", 1 => "nophoto_img"), "method");
                echo "\" />
                    ";
            }
            // line 18
            echo "                     <div class=\"catalogbox__salebox\">
                        ";
            // line 19
            if ($this->getAttribute($this->getAttribute($this->getAttribute((isset($context["aObject"]) ? $context["aObject"] : null), "fields"), "discount"), "value")) {
                // line 20
                echo "                            <div class=\"catalogbox__saleitem\"><img alt=\"\" src=\"";
                echo $this->getAttribute((isset($context["Design"]) ? $context["Design"] : null), "get", array(0 => "modules.catalogbox.btnbox.salebox", 1 => "image_sale"), "method");
                echo "\"></div>
                        ";
            }
            // line 22
            echo "                        ";
            if ($this->getAttribute($this->getAttribute($this->getAttribute((isset($context["aObject"]) ? $context["aObject"] : null), "fields"), "hit"), "value")) {
                // line 23
                echo "                            <div class=\"catalogbox__saleitem\"><img alt=\"\" src=\"";
                echo $this->getAttribute((isset($context["Design"]) ? $context["Design"] : null), "get", array(0 => "modules.catalogbox.btnbox.salebox", 1 => "image_hit"), "method");
                echo "\"></div>
                        ";
            }
            // line 25
            echo "                        ";
            if ($this->getAttribute($this->getAttribute($this->getAttribute((isset($context["aObject"]) ? $context["aObject"] : null), "fields"), "new"), "value")) {
                // line 26
                echo "                            <div class=\"catalogbox__saleitem\"><img alt=\"\" src=\"";
                echo $this->getAttribute((isset($context["Design"]) ? $context["Design"] : null), "get", array(0 => "modules.catalogbox.btnbox.salebox", 1 => "image_new"), "method");
                echo "\"></div>
                        ";
            }
            // line 28
            echo "                    </div>
                </div>
            </div>
            <div class=\"catalogbox__content\">
                ";
            // line 32
            echo $this->getAttribute($this->getAttribute((isset($context["aObject"]) ? $context["aObject"] : null), "Rating"), "html");
            echo "
                ";
            // line 33
            if ($this->getAttribute($this->getAttribute($this->getAttribute((isset($context["aObject"]) ? $context["aObject"] : null), "fields"), "article"), "value")) {
                echo "<p class=\"catalogbox__artical\">
                    ";
                // line 34
                echo $this->getAttribute($this->getAttribute($this->getAttribute((isset($context["aObject"]) ? $context["aObject"] : null), "fields"), "article"), "title");
                echo " ";
                echo $this->getAttribute($this->getAttribute($this->getAttribute((isset($context["aObject"]) ? $context["aObject"] : null), "fields"), "article"), "html");
                echo "</p>";
            }
            // line 35
            echo "                ";
            if ($this->getAttribute($this->getAttribute($this->getAttribute((isset($context["aObject"]) ? $context["aObject"] : null), "fields"), "announce"), "value")) {
                echo "<div
                        class=\"catalogbox__param__";
                // line 36
                echo $this->getAttribute($this->getAttribute($this->getAttribute((isset($context["aObject"]) ? $context["aObject"] : null), "fields"), "announce"), "name");
                echo "\">";
                echo $this->getAttribute($this->getAttribute($this->getAttribute((isset($context["aObject"]) ? $context["aObject"] : null), "fields"), "announce"), "value");
                echo "</div>";
            }
            // line 37
            echo "                ";
            $context['_parent'] = (array) $context;
            $context['_seq'] = twig_ensure_traversable($this->getAttribute((isset($context["aObject"]) ? $context["aObject"] : null), "fields"));
            foreach ($context['_seq'] as $context["_key"] => $context["field"]) {
                // line 38
                echo "                    ";
                if ((($this->getAttribute($this->getAttribute((isset($context["field"]) ? $context["field"] : null), "attrs"), "show_in_params") && $this->getAttribute((isset($context["field"]) ? $context["field"] : null), "value")) && $this->getAttribute((isset($context["field"]) ? $context["field"] : null), "html"))) {
                    // line 39
                    echo "                        ";
                    if ((($this->getAttribute((isset($context["field"]) ? $context["field"] : null), "type") == "multicollection") && twig_length_filter($this->env, $this->getAttribute((isset($context["field"]) ? $context["field"] : null), "value")))) {
                        // line 40
                        echo "                            <div class=\"catalogbox__param catalogbox__param__";
                        echo $this->getAttribute((isset($context["field"]) ? $context["field"] : null), "name");
                        echo "\"><span>";
                        echo $this->getAttribute((isset($context["field"]) ? $context["field"] : null), "title");
                        echo ":</span>
                                    ";
                        // line 41
                        echo $this->getAttribute((isset($context["field"]) ? $context["field"] : null), "html");
                        echo "
                            </div>
                        ";
                    } else {
                        // line 44
                        echo "                            <div class=\"catalogbox__param catalogbox__param__";
                        echo $this->getAttribute((isset($context["field"]) ? $context["field"] : null), "name");
                        echo "\">
                                ";
                        // line 45
                        if (($this->getAttribute($this->getAttribute((isset($context["field"]) ? $context["field"] : null), "attrs"), "show_title") == 1)) {
                            // line 46
                            echo "                                    <span>";
                            echo $this->getAttribute((isset($context["field"]) ? $context["field"] : null), "title");
                            echo ":</span>
                                ";
                        }
                        // line 48
                        echo "                                ";
                        if ($this->getAttribute((isset($context["field"]) ? $context["field"] : null), "htmlAlt")) {
                            echo $this->getAttribute((isset($context["field"]) ? $context["field"] : null), "htmlAlt");
                            echo " ";
                        } else {
                            echo $this->getAttribute((isset($context["field"]) ? $context["field"] : null), "html");
                        }
                        // line 49
                        echo "                            </div>
                        ";
                    }
                    // line 51
                    echo "                    ";
                }
                // line 52
                echo "                ";
            }
            $_parent = $context['_parent'];
            unset($context['_seq'], $context['_iterated'], $context['_key'], $context['field'], $context['_parent'], $context['loop']);
            $context = array_intersect_key($context, $_parent) + $_parent;
            // line 53
            echo "                ";
            if ($this->getAttribute($this->getAttribute($this->getAttribute((isset($context["aObject"]) ? $context["aObject"] : null), "fields"), "old_price"), "value")) {
                echo "<p class=\"catalogbox__oldprice\">";
                echo $this->getAttribute($this->getAttribute($this->getAttribute((isset($context["aObject"]) ? $context["aObject"] : null), "fields"), "old_price"), "title");
                echo ":
                    <span>";
                // line 54
                echo call_user_func_array($this->env->getFilter('price_format')->getCallable(), array($this->getAttribute($this->getAttribute($this->getAttribute((isset($context["aObject"]) ? $context["aObject"] : null), "fields"), "old_price"), "value")));
                echo "
                        ";
                // line 55
                echo $this->getAttribute($this->getAttribute($this->getAttribute((isset($context["aObject"]) ? $context["aObject"] : null), "fields"), "old_price"), "measure");
                if ($this->getAttribute($this->getAttribute($this->getAttribute((isset($context["aObject"]) ? $context["aObject"] : null), "fields"), "measure"), "html")) {
                    echo "/";
                    echo $this->getAttribute($this->getAttribute($this->getAttribute((isset($context["aObject"]) ? $context["aObject"] : null), "fields"), "measure"), "html");
                }
                // line 56
                echo "                    </span>
                    </p>";
            }
            // line 58
            echo "                ";
            if ($this->getAttribute($this->getAttribute($this->getAttribute((isset($context["aObject"]) ? $context["aObject"] : null), "fields"), "price"), "value")) {
                // line 59
                echo "                    <p class=\"catalogbox__price\">";
                echo $this->getAttribute($this->getAttribute($this->getAttribute((isset($context["aObject"]) ? $context["aObject"] : null), "fields"), "price"), "title");
                echo ":
                        <span> ";
                // line 60
                echo call_user_func_array($this->env->getFilter('price_format')->getCallable(), array($this->getAttribute($this->getAttribute($this->getAttribute((isset($context["aObject"]) ? $context["aObject"] : null), "fields"), "price"), "value")));
                echo "</span>
                        ";
                // line 61
                echo $this->getAttribute($this->getAttribute($this->getAttribute((isset($context["aObject"]) ? $context["aObject"] : null), "fields"), "price"), "measure");
                if ($this->getAttribute($this->getAttribute($this->getAttribute((isset($context["aObject"]) ? $context["aObject"] : null), "fields"), "measure"), "html")) {
                    echo "/";
                    echo $this->getAttribute($this->getAttribute($this->getAttribute((isset($context["aObject"]) ? $context["aObject"] : null), "fields"), "measure"), "html");
                }
                // line 62
                echo "                    </p>
                ";
            }
            // line 64
            echo "                ";
            if (((!(isset($context["isMainObject"]) ? $context["isMainObject"] : null)) || (!(isset($context["hideBuy1lvlGoods"]) ? $context["hideBuy1lvlGoods"] : null)))) {
                // line 65
                echo "                <div class=\"catalogbox__shcar\">
                    <div class=\"catalogbox__btnbox\">
                        ";
                // line 67
                if ($this->getAttribute($this->getAttribute($this->getAttribute((isset($context["aObject"]) ? $context["aObject"] : null), "fields"), "countbuy"), "value")) {
                    // line 68
                    echo "                            <div class=\"js_catalogbox_inputbox catalogbox__inputbox\">
                                <div class=\"catalogbox__minus js_catalogbox_minus\">-</div>
                                <input class=\"js_count\" type=\"text\" data-id=\"";
                    // line 70
                    echo $this->getAttribute((isset($context["aObject"]) ? $context["aObject"] : null), "id");
                    echo "\" value=\"1\">
                                <div class=\"catalogbox__plus js_catalogbox_plus\">+</div>
                            </div>
                        ";
                }
                // line 74
                echo "                        ";
                if ($this->getAttribute($this->getAttribute($this->getAttribute((isset($context["aObject"]) ? $context["aObject"] : null), "fields"), "orderbuy"), "value")) {
                    // line 75
                    echo "                            <button>";
                    echo $this->getAttribute((isset($context["Lang"]) ? $context["Lang"] : null), "get", array(0 => "catalog.order"), "method");
                    echo "</button>
                        ";
                }
                // line 77
                echo "                    </div>
                    ";
                // line 78
                if ($this->getAttribute($this->getAttribute($this->getAttribute((isset($context["aObject"]) ? $context["aObject"] : null), "fields"), "fastbuy"), "value")) {
                    // line 79
                    echo "                        <div class=\"catalogbox__buynow\"><a class=\"js-callback\" data-ajaxform=\"1\" data-js_max_width=\"600\" data-width-type=\"px\" data-idobj=\"";
                    echo $this->getAttribute((isset($context["aObject"]) ? $context["aObject"] : null), "id");
                    echo "\" data-module=\"Cart\" data-cmd=\"checkout\" href=\"#\">";
                    echo $this->getAttribute($this->getAttribute($this->getAttribute((isset($context["aObject"]) ? $context["aObject"] : null), "fields"), "fastbuy"), "title");
                    echo "</a></div>
                    ";
                }
                // line 81
                echo "                    ";
                if ($this->getAttribute($this->getAttribute($this->getAttribute((isset($context["aObject"]) ? $context["aObject"] : null), "fields"), "buy"), "value")) {
                    // line 82
                    echo "                        <div class=\"catalogbox__btn\"><a ";
                    if ((isset($context["useCart"]) ? $context["useCart"] : null)) {
                        echo "onclick=\"return false;\"";
                    }
                    echo " data-id=\"";
                    echo $this->getAttribute((isset($context["aObject"]) ? $context["aObject"] : null), "id");
                    echo "\" class=\"js-btnBuy btnBuy\"
                                                           href=\"";
                    // line 83
                    if ((!(isset($context["useCart"]) ? $context["useCart"] : null))) {
                        echo "[";
                        echo (isset($context["form_section"]) ? $context["form_section"] : null);
                        echo "][From?objectId=";
                        echo $this->getAttribute((isset($context["aObject"]) ? $context["aObject"] : null), "id");
                        echo "]";
                    } else {
                        echo "#tocart";
                    }
                    echo "\">";
                    echo $this->getAttribute((isset($context["Lang"]) ? $context["Lang"] : null), "get", array(0 => $this->getAttribute($this->getAttribute($this->getAttribute((isset($context["aObject"]) ? $context["aObject"] : null), "fields"), "buy"), "title")), "method");
                    echo "</a>
                        </div>
                    ";
                }
                // line 86
                echo "                </div>
                ";
            }
            // line 88
            echo "            </div>
        </div>

        ";
            // line 91
            if ($this->getAttribute($this->getAttribute((isset($context["aObject"]) ? $context["aObject"] : null), "fields", array(), "any", false, true), "add_gallery", array(), "any", true, true)) {
                // line 92
                echo "            ";
                $context["gallery"] = $this->getAttribute($this->getAttribute((isset($context["aObject"]) ? $context["aObject"] : null), "fields"), "add_gallery");
                // line 93
                echo "            ";
                if ($this->getAttribute($this->getAttribute((isset($context["gallery"]) ? $context["gallery"] : null), "gallery"), "images")) {
                    // line 94
                    echo "                ";
                    if ((!$this->getAttribute($this->getAttribute((isset($context["gallery"]) ? $context["gallery"] : null), "attrs"), "show_in_tab"))) {
                        // line 95
                        echo "                    ";
                        if ($this->getAttribute((isset($context["gallery"]) ? $context["gallery"] : null), "widget")) {
                            // line 96
                            echo "                        <h2>";
                            echo twig_escape_filter($this->env, $this->getAttribute((isset($context["gallery"]) ? $context["gallery"] : null), "title"));
                            echo "</h2>
                        ";
                            // line 97
                            if (($this->getAttribute((isset($context["gallery"]) ? $context["gallery"] : null), "widget") == "fotorama")) {
                                // line 98
                                echo "                            ";
                                $this->env->loadTemplate("Fotorama.twig")->display($context);
                                // line 99
                                echo "                        ";
                            } elseif (($this->getAttribute((isset($context["gallery"]) ? $context["gallery"] : null), "widget") == "tile")) {
                                // line 100
                                echo "                            ";
                                $this->env->loadTemplate("Tile.twig")->display($context);
                                // line 101
                                echo "                        ";
                            }
                            // line 102
                            echo "                    ";
                        }
                        // line 103
                        echo "                ";
                    }
                    // line 104
                    echo "            ";
                }
                // line 105
                echo "        ";
            }
            // line 106
            echo "
        ";
            // line 107
            if (array_key_exists("aTabs", $context)) {
                // line 108
                echo "        <div id=\"js_tabs\" class=\"b-tab js-tabs\">
            ";
                // line 109
                if ((twig_length_filter($this->env, (isset($context["aTabs"]) ? $context["aTabs"] : null)) > 1)) {
                    // line 110
                    echo "                <ul>
                ";
                    // line 111
                    $context['_parent'] = (array) $context;
                    $context['_seq'] = twig_ensure_traversable((isset($context["aTabs"]) ? $context["aTabs"] : null));
                    foreach ($context['_seq'] as $context["_key"] => $context["tab"]) {
                        // line 112
                        echo "                    <li><a ";
                        if ($this->getAttribute((isset($context["tab"]) ? $context["tab"] : null), "active")) {
                            echo "class=\"js-selected-tab\"";
                        }
                        echo " href=\"#tabs-";
                        echo $this->getAttribute((isset($context["tab"]) ? $context["tab"] : null), "name");
                        echo "\">";
                        echo $this->getAttribute((isset($context["tab"]) ? $context["tab"] : null), "title");
                        echo "</a></li>
                ";
                    }
                    $_parent = $context['_parent'];
                    unset($context['_seq'], $context['_iterated'], $context['_key'], $context['tab'], $context['_parent'], $context['loop']);
                    $context = array_intersect_key($context, $_parent) + $_parent;
                    // line 114
                    echo "                </ul>
            ";
                } else {
                    // line 116
                    echo "                    <ul>
                        ";
                    // line 117
                    $context['_parent'] = (array) $context;
                    $context['_seq'] = twig_ensure_traversable((isset($context["aTabs"]) ? $context["aTabs"] : null));
                    foreach ($context['_seq'] as $context["_key"] => $context["tab"]) {
                        // line 118
                        echo "                            <li><h3 ";
                        if ($this->getAttribute((isset($context["tab"]) ? $context["tab"] : null), "active")) {
                            echo "class=\"js-selected-tab\"";
                        }
                        echo ">";
                        echo $this->getAttribute((isset($context["tab"]) ? $context["tab"] : null), "title");
                        echo "</h3></li>
                        ";
                    }
                    $_parent = $context['_parent'];
                    unset($context['_seq'], $context['_iterated'], $context['_key'], $context['tab'], $context['_parent'], $context['loop']);
                    $context = array_intersect_key($context, $_parent) + $_parent;
                    // line 120
                    echo "                    </ul>
                ";
                }
                // line 122
                echo "            ";
                $context['_parent'] = (array) $context;
                $context['_seq'] = twig_ensure_traversable((isset($context["aTabs"]) ? $context["aTabs"] : null));
                $context['loop'] = array(
                  'parent' => $context['_parent'],
                  'index0' => 0,
                  'index'  => 1,
                  'first'  => true,
                );
                if (is_array($context['_seq']) || (is_object($context['_seq']) && $context['_seq'] instanceof Countable)) {
                    $length = count($context['_seq']);
                    $context['loop']['revindex0'] = $length - 1;
                    $context['loop']['revindex'] = $length;
                    $context['loop']['length'] = $length;
                    $context['loop']['last'] = 1 === $length;
                }
                foreach ($context['_seq'] as $context["_key"] => $context["tab"]) {
                    // line 123
                    echo "                <div id=\"tabs-";
                    echo $this->getAttribute((isset($context["tab"]) ? $context["tab"] : null), "name");
                    echo "\">
                    ";
                    // line 124
                    if (($this->getAttribute((isset($context["tab"]) ? $context["tab"] : null), "name") == "add_gallery")) {
                        // line 125
                        echo "                        ";
                        $context["gallery"] = (isset($context["tab"]) ? $context["tab"] : null);
                        // line 126
                        echo "                        ";
                        if ($this->getAttribute((isset($context["gallery"]) ? $context["gallery"] : null), "widget")) {
                            // line 127
                            echo "                            ";
                            if (($this->getAttribute((isset($context["gallery"]) ? $context["gallery"] : null), "widget") == "fotorama")) {
                                // line 128
                                echo "                                ";
                                $this->env->loadTemplate("Fotorama.twig")->display($context);
                                // line 129
                                echo "                            ";
                            } elseif (($this->getAttribute((isset($context["gallery"]) ? $context["gallery"] : null), "widget") == "tile")) {
                                // line 130
                                echo "                               ";
                                $this->env->loadTemplate("Tile.twig")->display($context);
                                // line 131
                                echo "                            ";
                            }
                            // line 132
                            echo "                        ";
                        }
                        // line 133
                        echo "                    ";
                    } else {
                        // line 134
                        echo "                        ";
                        echo $this->getAttribute((isset($context["tab"]) ? $context["tab"] : null), "html");
                        echo "
                    ";
                    }
                    // line 136
                    echo "                </div>
            ";
                    ++$context['loop']['index0'];
                    ++$context['loop']['index'];
                    $context['loop']['first'] = false;
                    if (isset($context['loop']['length'])) {
                        --$context['loop']['revindex0'];
                        --$context['loop']['revindex'];
                        $context['loop']['last'] = 0 === $context['loop']['revindex0'];
                    }
                }
                $_parent = $context['_parent'];
                unset($context['_seq'], $context['_iterated'], $context['_key'], $context['tab'], $context['_parent'], $context['loop']);
                $context = array_intersect_key($context, $_parent) + $_parent;
                // line 138
                echo "        </div>
        ";
            }
            // line 140
            echo "    ";
        }
        // line 141
        echo "</div>
";
        // line 142
        if (array_key_exists("ModificationsItems", $context)) {
            // line 143
            echo "    ";
            $template = $this->env->resolveTemplate(twig_join_filter(array(0 => "ModificationsItems.twig")));
            $template->display($context);
        }
        // line 145
        echo "
";
        // line 146
        if ((isset($context["RecentlyViewed"]) ? $context["RecentlyViewed"] : null)) {
            // line 147
            echo "    ";
            echo (isset($context["RecentlyViewed"]) ? $context["RecentlyViewed"] : null);
            echo "
";
        }
        // line 149
        echo "
";
        // line 150
        if (array_key_exists("includedItems", $context)) {
            // line 151
            echo "    ";
            $template = $this->env->resolveTemplate(twig_join_filter(array(0 => "IncludedItems.twig")));
            $template->display($context);
        }
        // line 153
        if (array_key_exists("relatedItems", $context)) {
            // line 154
            echo "    ";
            $template = $this->env->resolveTemplate(twig_join_filter(array(0 => "RelatedItems.twig")));
            $template->display($context);
        }
        // line 156
        echo "
";
        // line 157
        if (array_key_exists("nearItems", $context)) {
            // line 158
            echo "    <div class=\"b-catslider\">
        <a class=\"catslider__back\"
           href=\"[";
            // line 160
            echo $this->getAttribute((isset($context["nearItems"]) ? $context["nearItems"] : null), "section");
            echo "][CatalogViewer?";
            if ($this->getAttribute($this->getAttribute((isset($context["nearItems"]) ? $context["nearItems"] : null), "prev"), "alias")) {
                echo "goods-alias=";
                echo $this->getAttribute($this->getAttribute((isset($context["nearItems"]) ? $context["nearItems"] : null), "prev"), "alias");
            } else {
                echo "item=";
                echo $this->getAttribute($this->getAttribute((isset($context["nearItems"]) ? $context["nearItems"] : null), "prev"), "id");
            }
            echo "]\">";
            echo $this->getAttribute((isset($context["Lang"]) ? $context["Lang"] : null), "get", array(0 => "Page.page_prev"), "method");
            echo "<ins></ins></a>
        <a class=\"catslider__mid\" href=\"[";
            // line 161
            echo $this->getAttribute((isset($context["nearItems"]) ? $context["nearItems"] : null), "section");
            echo "]\">";
            echo $this->getAttribute((isset($context["Lang"]) ? $context["Lang"] : null), "get", array(0 => "catalog.back_to_products"), "method");
            echo "</a>
        <a class=\"catslider__next\"
           href=\"[";
            // line 163
            echo $this->getAttribute((isset($context["nearItems"]) ? $context["nearItems"] : null), "section");
            echo "][CatalogViewer?";
            if ($this->getAttribute($this->getAttribute((isset($context["nearItems"]) ? $context["nearItems"] : null), "next"), "alias")) {
                echo "goods-alias=";
                echo $this->getAttribute($this->getAttribute((isset($context["nearItems"]) ? $context["nearItems"] : null), "next"), "alias");
            } else {
                echo "item=";
                echo $this->getAttribute($this->getAttribute((isset($context["nearItems"]) ? $context["nearItems"] : null), "next"), "id");
            }
            echo "]\">";
            echo $this->getAttribute((isset($context["Lang"]) ? $context["Lang"] : null), "get", array(0 => "Page.page_next"), "method");
            echo "<ins></ins></a>
    </div>
";
        }
    }

    public function getTemplateName()
    {
        return "SimpleDetail.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  567 => 163,  560 => 161,  546 => 160,  542 => 158,  540 => 157,  537 => 156,  532 => 154,  530 => 153,  525 => 151,  523 => 150,  520 => 149,  514 => 147,  512 => 146,  509 => 145,  504 => 143,  502 => 142,  499 => 141,  496 => 140,  492 => 138,  477 => 136,  471 => 134,  468 => 133,  465 => 132,  462 => 131,  459 => 130,  456 => 129,  453 => 128,  450 => 127,  447 => 126,  444 => 125,  442 => 124,  437 => 123,  419 => 122,  415 => 120,  402 => 118,  398 => 117,  395 => 116,  391 => 114,  376 => 112,  372 => 111,  369 => 110,  367 => 109,  364 => 108,  362 => 107,  359 => 106,  356 => 105,  353 => 104,  350 => 103,  347 => 102,  344 => 101,  338 => 99,  335 => 98,  333 => 97,  328 => 96,  325 => 95,  322 => 94,  319 => 93,  316 => 92,  309 => 88,  305 => 86,  280 => 82,  269 => 79,  267 => 78,  264 => 77,  248 => 70,  238 => 65,  221 => 60,  216 => 59,  203 => 55,  199 => 54,  179 => 49,  123 => 35,  117 => 34,  109 => 32,  79 => 20,  74 => 18,  61 => 13,  46 => 11,  27 => 4,  22 => 2,  212 => 56,  197 => 53,  194 => 52,  189 => 50,  174 => 47,  169 => 45,  166 => 44,  163 => 45,  146 => 39,  140 => 37,  134 => 37,  122 => 34,  113 => 33,  108 => 30,  106 => 29,  103 => 28,  100 => 27,  65 => 19,  57 => 16,  54 => 15,  44 => 13,  41 => 10,  272 => 98,  263 => 94,  258 => 75,  255 => 74,  247 => 88,  244 => 68,  242 => 67,  235 => 64,  231 => 62,  228 => 80,  226 => 79,  222 => 77,  214 => 74,  208 => 72,  206 => 71,  188 => 63,  186 => 52,  183 => 51,  176 => 59,  171 => 48,  168 => 57,  153 => 53,  151 => 41,  148 => 40,  145 => 40,  142 => 39,  139 => 38,  136 => 47,  133 => 46,  130 => 45,  128 => 36,  125 => 43,  119 => 41,  116 => 32,  110 => 38,  107 => 37,  92 => 31,  88 => 23,  82 => 27,  77 => 19,  72 => 23,  68 => 16,  52 => 14,  49 => 13,  43 => 10,  40 => 9,  37 => 8,  35 => 7,  32 => 6,  29 => 5,  21 => 2,  80 => 22,  71 => 13,  104 => 20,  98 => 19,  95 => 18,  86 => 16,  84 => 15,  60 => 17,  85 => 22,  75 => 24,  48 => 9,  45 => 8,  42 => 7,  33 => 5,  30 => 6,  24 => 3,  346 => 108,  341 => 100,  339 => 104,  334 => 102,  327 => 101,  320 => 100,  314 => 91,  310 => 98,  306 => 97,  302 => 96,  298 => 95,  294 => 93,  289 => 83,  283 => 88,  277 => 81,  275 => 99,  271 => 83,  266 => 95,  260 => 93,  254 => 76,  252 => 90,  249 => 89,  241 => 72,  239 => 85,  236 => 70,  234 => 69,  225 => 61,  219 => 62,  213 => 58,  209 => 56,  204 => 56,  201 => 69,  195 => 66,  192 => 53,  190 => 50,  187 => 49,  180 => 45,  170 => 40,  165 => 46,  158 => 44,  152 => 41,  149 => 31,  147 => 30,  143 => 38,  135 => 23,  129 => 22,  124 => 20,  120 => 18,  118 => 17,  115 => 16,  101 => 35,  97 => 26,  76 => 13,  66 => 10,  64 => 14,  62 => 18,  34 => 7,  28 => 3,  26 => 4,  102 => 24,  99 => 34,  96 => 33,  94 => 25,  87 => 16,  83 => 23,  78 => 14,  73 => 12,  69 => 12,  63 => 20,  59 => 18,  55 => 6,  51 => 5,  47 => 4,  39 => 9,  36 => 8,  19 => 1,);
    }
}
