<?php

/* view.twig */
class __TwigTemplate_0957a5ec18c4a1c18bd567880424fe09 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        // line 1
        if ((isset($context["count"]) ? $context["count"] : null)) {
            // line 2
            echo "<div class=\"message\">
    <a href=\"#out.left.tools=Messages;out.tabs=tools_Messages\">";
            // line 3
            echo (isset($context["count"]) ? $context["count"] : null);
            echo " ";
            echo (isset($context["text"]) ? $context["text"] : null);
            echo "</a>
</div>
";
        } else {
            // line 6
            echo "<div class=\"message\">
    <a href=\"#out.left.tools=Messages;out.tabs=tools_Messages\">";
            // line 7
            echo $this->getAttribute((isset($context["Lang"]) ? $context["Lang"] : null), "get", array(0 => "Messages.Messages.Cms.tab_name"), "method");
            echo "</a>
</div>
";
        }
    }

    public function getTemplateName()
    {
        return "view.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  35 => 7,  32 => 6,  24 => 3,  21 => 2,  19 => 1,);
    }
}
