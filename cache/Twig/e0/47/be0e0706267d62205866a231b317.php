<?php

/* FilterList.twig */
class __TwigTemplate_e047be0e0706267d62205866a231b317 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        // line 1
        if (array_key_exists("filter", $context)) {
            // line 2
            echo "<!--noindex-->
    <div class=\"b-catfilter\">
        <form method=\"get\" action=\"?\">
            <div class=\"catfilter__leftside\">
                <div class=\"catfilter__item\" style=\"font-weight: bold;\">";
            // line 6
            echo $this->getAttribute((isset($context["Lang"]) ? $context["Lang"] : null), "get", array(0 => "catalog.filter_order_by"), "method");
            echo ":</div>
                <div class=\"catfilter__item\">
                    ";
            // line 8
            $context['_parent'] = (array) $context;
            $context['_seq'] = twig_ensure_traversable($this->getAttribute((isset($context["filter"]) ? $context["filter"] : null), "aFields"));
            foreach ($context['_seq'] as $context["_key"] => $context["oField"]) {
                // line 9
                echo "                    <a class=\"catfilter__title";
                echo $this->getAttribute((isset($context["oField"]) ? $context["oField"] : null), "sel");
                echo " js_sort_control\" rel=\"nofollow\" href=\"#\" curval=\"";
                echo $this->getAttribute((isset($context["oField"]) ? $context["oField"] : null), "name");
                echo "\">";
                echo $this->getAttribute((isset($context["oField"]) ? $context["oField"] : null), "title");
                echo "</a>
                    ";
            }
            $_parent = $context['_parent'];
            unset($context['_seq'], $context['_iterated'], $context['_key'], $context['oField'], $context['_parent'], $context['loop']);
            $context = array_intersect_key($context, $_parent) + $_parent;
            // line 11
            echo "                    ";
            if (((isset($context["sortState"]) ? $context["sortState"] : null) && ((isset($context["sortState"]) ? $context["sortState"] : null) != (isset($context["defSortState"]) ? $context["defSortState"] : null)))) {
                // line 12
                echo "                    <a class=\"catfilter__title js_sort_control\" rel=\"nofollow\" href=\"#\" curval=\"\">";
                echo $this->getAttribute((isset($context["Lang"]) ? $context["Lang"] : null), "get", array(0 => "catalog.filter_order_default"), "method");
                echo "</a>
                    ";
            }
            // line 14
            echo "                    <input type=\"hidden\" name=\"sort\" value=\"";
            echo twig_escape_filter($this->env, (isset($context["sortState"]) ? $context["sortState"] : null));
            echo "\">
                    <input type=\"hidden\" name=\"way\" value=\"";
            // line 15
            echo twig_escape_filter($this->env, (isset($context["sortWay"]) ? $context["sortWay"] : null));
            echo "\">
                </div>
            </div>

            <div class=\"catfilter__rightside\">
                <div class=\"catfilter__item\"><a class=\"catfilter__galpic catfilter__galpic__on js_view_control\" href=\"#\" rel=\"nofollow\" curval=\"gallery\"><ins></ins>";
            // line 20
            echo $this->getAttribute((isset($context["Lang"]) ? $context["Lang"] : null), "get", array(0 => "catalog.view_gallery"), "method");
            echo "</a></div>
                <div class=\"catfilter__item\"><a class=\"catfilter__listpic catfilter__listpic__on js_view_control\" href=\"#\" rel=\"nofollow\" curval=\"list\"><ins></ins>";
            // line 21
            echo $this->getAttribute((isset($context["Lang"]) ? $context["Lang"] : null), "get", array(0 => "catalog.view_list"), "method");
            echo "</a></div>
                <input type=\"hidden\" name=\"view\" value=\"";
            // line 22
            echo twig_escape_filter($this->env, (isset($context["viewState"]) ? $context["viewState"] : null));
            echo "\">
            </div>
        </form>
    </div>
<!--/noindex-->
";
        }
    }

    public function getTemplateName()
    {
        return "FilterList.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  79 => 22,  75 => 21,  71 => 20,  63 => 15,  58 => 14,  52 => 12,  49 => 11,  36 => 9,  32 => 8,  27 => 6,  34 => 7,  30 => 4,  28 => 3,  21 => 2,  19 => 1,);
    }
}
