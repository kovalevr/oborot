<?php

/* banner.twig */
class __TwigTemplate_7cf269dcbb5c716bf5fd770fc5933032 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        // line 1
        echo "<div class=\"b-picture fotorama-container js-fotorama-container\">

    ";
        // line 4
        echo "    <style type=\"text/css\">

        .b-picture > .picture__fotorama:not(.fotorama) {
            padding-bottom: calc(";
        // line 7
        echo $this->getAttribute((isset($context["aDimensionsFirstImage"]) ? $context["aDimensionsFirstImage"] : null), "height");
        echo " * 100% / ";
        echo $this->getAttribute((isset($context["aDimensionsFirstImage"]) ? $context["aDimensionsFirstImage"] : null), "width");
        echo ");
        }

        ";
        // line 10
        if ($this->getAttribute((isset($context["configArray"]) ? $context["configArray"] : null), "maxHeight")) {
            // line 11
            echo "
            .fotorama-container{
                max-height: ";
            // line 13
            echo $this->getAttribute((isset($context["configArray"]) ? $context["configArray"] : null), "maxHeight");
            echo "px;
            }

        ";
        }
        // line 17
        echo "
    </style>


    <div class=\"picture__fotorama js-fotorama-slider fotorama-picture\"  data-ratioFirstImage='";
        // line 21
        echo $this->getAttribute((isset($context["aDimensionsFirstImage"]) ? $context["aDimensionsFirstImage"] : null), "ratio");
        echo "' data-config='";
        echo (isset($context["config"]) ? $context["config"] : null);
        echo "' data-minheight='";
        echo (isset($context["aMinHeight"]) ? $context["aMinHeight"] : null);
        echo "'";
        echo $this->getAttribute((isset($context["Design"]) ? $context["Design"] : null), "write", array(0 => " sktag=\"modules.slider\""), "method");
        echo ">
        ";
        // line 22
        $context['_parent'] = (array) $context;
        $context['_seq'] = twig_ensure_traversable((isset($context["banner"]) ? $context["banner"] : null));
        foreach ($context['_seq'] as $context["_key"] => $context["item"]) {
            // line 23
            echo "        <div class=\"picture__item\" data-img=\"";
            echo $this->getAttribute((isset($context["item"]) ? $context["item"] : null), "img");
            echo "\">
            ";
            // line 24
            if ($this->getAttribute((isset($context["item"]) ? $context["item"] : null), "slide_link")) {
                echo "<a class=\"picture__link\" href=\"";
                echo $this->getAttribute((isset($context["item"]) ? $context["item"] : null), "slide_link");
                echo "\" target=\"_blank\" rel=\"nofollow noopener\"></a>";
            }
            // line 25
            echo "            <div class=\"picture__inner\">
                <div class=\"picture__text picture__text1\" style=\"";
            // line 26
            if ($this->getAttribute((isset($context["item"]) ? $context["item"] : null), "text1_h")) {
                echo "left: ";
                echo $this->getAttribute((isset($context["item"]) ? $context["item"] : null), "text1_h");
                echo "px;";
            }
            if ($this->getAttribute((isset($context["item"]) ? $context["item"] : null), "text1_v")) {
                echo "top: ";
                echo $this->getAttribute((isset($context["item"]) ? $context["item"] : null), "text1_v");
                echo "px;";
            }
            echo "\">
                    ";
            // line 27
            echo $this->getAttribute((isset($context["item"]) ? $context["item"] : null), "text1");
            echo "
                </div>
                <div class=\"picture__text picture__text2\" style=\"";
            // line 29
            if ($this->getAttribute((isset($context["item"]) ? $context["item"] : null), "text2_h")) {
                echo "left: ";
                echo $this->getAttribute((isset($context["item"]) ? $context["item"] : null), "text2_h");
                echo "px;";
            }
            if ($this->getAttribute((isset($context["item"]) ? $context["item"] : null), "text2_v")) {
                echo "top: ";
                echo $this->getAttribute((isset($context["item"]) ? $context["item"] : null), "text2_v");
                echo "px;";
            }
            echo "\">
                    ";
            // line 30
            echo $this->getAttribute((isset($context["item"]) ? $context["item"] : null), "text2");
            echo "
                </div>
                <div class=\"picture__text tpicture__text3\" style=\"";
            // line 32
            if ($this->getAttribute((isset($context["item"]) ? $context["item"] : null), "text3_h")) {
                echo "left: ";
                echo $this->getAttribute((isset($context["item"]) ? $context["item"] : null), "text3_h");
                echo "px;";
            }
            if ($this->getAttribute((isset($context["item"]) ? $context["item"] : null), "text3_v")) {
                echo "top: ";
                echo $this->getAttribute((isset($context["item"]) ? $context["item"] : null), "text3_v");
                echo "px;";
            }
            echo "\">
                    ";
            // line 33
            echo $this->getAttribute((isset($context["item"]) ? $context["item"] : null), "text3");
            echo "
                </div>
                <div class=\"picture__text picture__text4\" style=\"";
            // line 35
            if ($this->getAttribute((isset($context["item"]) ? $context["item"] : null), "text4_h")) {
                echo "left: ";
                echo $this->getAttribute((isset($context["item"]) ? $context["item"] : null), "text4_h");
                echo "px;";
            }
            if ($this->getAttribute((isset($context["item"]) ? $context["item"] : null), "text4_v")) {
                echo "top: ";
                echo $this->getAttribute((isset($context["item"]) ? $context["item"] : null), "text4_v");
                echo "px;";
            }
            echo "\">
                    ";
            // line 36
            echo $this->getAttribute((isset($context["item"]) ? $context["item"] : null), "text4");
            echo "
                </div>
            </div>
        </div>
        ";
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['_key'], $context['item'], $context['_parent'], $context['loop']);
        $context = array_intersect_key($context, $_parent) + $_parent;
        // line 41
        echo "    </div>
</div>
";
    }

    public function getTemplateName()
    {
        return "banner.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  161 => 41,  137 => 35,  114 => 30,  96 => 27,  83 => 26,  74 => 24,  69 => 23,  42 => 13,  38 => 11,  160 => 45,  154 => 44,  150 => 36,  140 => 39,  132 => 33,  126 => 34,  117 => 30,  102 => 26,  88 => 22,  68 => 17,  59 => 13,  49 => 17,  35 => 5,  22 => 2,  129 => 26,  121 => 24,  115 => 21,  97 => 25,  90 => 16,  82 => 15,  79 => 14,  71 => 12,  47 => 8,  45 => 9,  28 => 7,  26 => 3,  72 => 16,  64 => 13,  37 => 7,  33 => 6,  29 => 4,  25 => 3,  310 => 74,  304 => 70,  296 => 67,  278 => 65,  273 => 64,  270 => 63,  260 => 61,  258 => 60,  254 => 58,  248 => 56,  246 => 55,  242 => 53,  238 => 51,  232 => 50,  228 => 49,  225 => 48,  218 => 44,  209 => 42,  203 => 41,  200 => 40,  194 => 39,  190 => 37,  182 => 36,  173 => 35,  165 => 33,  146 => 40,  142 => 30,  130 => 29,  125 => 26,  119 => 32,  116 => 23,  110 => 21,  107 => 18,  101 => 29,  99 => 17,  95 => 24,  80 => 25,  66 => 16,  58 => 12,  55 => 21,  52 => 4,  48 => 8,  27 => 4,  24 => 2,  21 => 2,  237 => 49,  223 => 47,  219 => 46,  215 => 43,  201 => 43,  197 => 41,  193 => 39,  179 => 38,  170 => 34,  159 => 32,  156 => 32,  139 => 30,  136 => 37,  134 => 28,  128 => 25,  124 => 24,  109 => 28,  106 => 27,  89 => 19,  86 => 18,  84 => 17,  77 => 19,  73 => 18,  67 => 10,  61 => 12,  43 => 7,  40 => 4,  23 => 4,  65 => 22,  53 => 9,  41 => 7,  39 => 3,  36 => 10,  32 => 4,  30 => 2,  19 => 1,);
    }
}
