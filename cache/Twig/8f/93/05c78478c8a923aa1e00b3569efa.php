<?php

/* MicroData.twig */
class __TwigTemplate_8f9305c78478c8a923aa1e00b3569efa extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        // line 1
        echo "<div itemscope itemtype=\"";
        echo $this->getAttribute((isset($context["Site"]) ? $context["Site"] : null), "getWebProtocol", array(), "method");
        echo "schema.org/Product\" style=\"display: none\">

    <meta itemprop=\"name\" content=\"";
        // line 3
        echo twig_escape_filter($this->env, $this->getAttribute((isset($context["aObject"]) ? $context["aObject"] : null), "title"));
        echo "\">

    ";
        // line 5
        if ($this->getAttribute($this->getAttribute($this->getAttribute((isset($context["aObject"]) ? $context["aObject"] : null), "fields"), "announce"), "value")) {
            // line 6
            echo "        ";
            $context["description"] = $this->getAttribute($this->getAttribute($this->getAttribute((isset($context["aObject"]) ? $context["aObject"] : null), "fields"), "announce"), "value");
            // line 7
            echo "    ";
        } else {
            // line 8
            echo "        ";
            $context["description"] = (isset($context["seo_description"]) ? $context["seo_description"] : null);
            // line 9
            echo "    ";
        }
        // line 10
        echo "
    ";
        // line 11
        if ((isset($context["description"]) ? $context["description"] : null)) {
            // line 12
            echo "        <div itemprop=\"description\">
            ";
            // line 13
            echo (isset($context["description"]) ? $context["description"] : null);
            echo "
        </div>
    ";
        }
        // line 16
        echo "
    ";
        // line 17
        if ($this->getAttribute($this->getAttribute($this->getAttribute($this->getAttribute($this->getAttribute($this->getAttribute((isset($context["aObject"]) ? $context["aObject"] : null), "fields"), "gallery"), "first_img"), "images_data"), "big"), "file")) {
            // line 18
            echo "        <img alt=\"";
            echo $this->getAttribute($this->getAttribute($this->getAttribute($this->getAttribute((isset($context["aObject"]) ? $context["aObject"] : null), "fields"), "gallery"), "first_img"), "alt_title");
            echo "\" src=\"";
            echo $this->getAttribute($this->getAttribute($this->getAttribute($this->getAttribute($this->getAttribute($this->getAttribute((isset($context["aObject"]) ? $context["aObject"] : null), "fields"), "gallery"), "first_img"), "images_data"), "big"), "file");
            echo "\" itemprop=\"image\" />
    ";
        }
        // line 20
        echo "
    ";
        // line 21
        if ($this->getAttribute($this->getAttribute($this->getAttribute((isset($context["aObject"]) ? $context["aObject"] : null), "fields"), "price"), "value")) {
            // line 22
            echo "        <div itemprop=\"offers\" itemscope itemtype=\"";
            echo $this->getAttribute((isset($context["Site"]) ? $context["Site"] : null), "getWebProtocol", array(), "method");
            echo "schema.org/Offer\">
            <meta itemprop=\"price\" content=\"";
            // line 23
            echo twig_escape_filter($this->env, $this->getAttribute($this->getAttribute($this->getAttribute((isset($context["aObject"]) ? $context["aObject"] : null), "fields"), "price"), "value"));
            echo "\">
            <meta itemprop=\"priceCurrency\" content=\"";
            // line 24
            echo (isset($context["currency_type"]) ? $context["currency_type"] : null);
            echo "\">
        </div>
    ";
        }
        // line 27
        echo "
    ";
        // line 28
        if ($this->getAttribute($this->getAttribute((isset($context["aObject"]) ? $context["aObject"] : null), "Rating"), "html")) {
            // line 29
            echo "        <div itemprop=\"aggregateRating\" itemscope itemtype=\"";
            echo $this->getAttribute((isset($context["Site"]) ? $context["Site"] : null), "getWebProtocol", array(), "method");
            echo "schema.org/AggregateRating\">
            <span itemprop=\"worstRating\">0</span>
            ";
            // line 31
            if ($this->getAttribute($this->getAttribute((isset($context["aObject"]) ? $context["aObject"] : null), "Rating"), "avgValue")) {
                // line 32
                echo "                <span itemprop=\"ratingValue\">";
                echo $this->getAttribute($this->getAttribute((isset($context["aObject"]) ? $context["aObject"] : null), "Rating"), "avgValue");
                echo "</span>
            ";
            }
            // line 34
            echo "            <span itemprop=\"bestRating\">";
            echo $this->getAttribute($this->getAttribute((isset($context["aObject"]) ? $context["aObject"] : null), "Rating"), "max");
            echo "</span>
            ";
            // line 35
            if ($this->getAttribute($this->getAttribute((isset($context["aObject"]) ? $context["aObject"] : null), "Rating"), "countRates")) {
                // line 36
                echo "                <span itemprop=\"ratingCount\">";
                echo $this->getAttribute($this->getAttribute((isset($context["aObject"]) ? $context["aObject"] : null), "Rating"), "countRates");
                echo "</span>
            ";
            }
            // line 38
            echo "
        </div>
    ";
        }
        // line 41
        echo "
    ";
        // line 42
        if ((isset($context["reviews"]) ? $context["reviews"] : null)) {
            // line 43
            echo "        ";
            echo (isset($context["reviews"]) ? $context["reviews"] : null);
            echo "
    ";
        }
        // line 45
        echo "
</div>";
    }

    public function getTemplateName()
    {
        return "MicroData.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  131 => 42,  91 => 27,  81 => 23,  58 => 16,  38 => 8,  25 => 3,  567 => 163,  560 => 161,  546 => 160,  542 => 158,  540 => 157,  537 => 156,  532 => 154,  530 => 153,  525 => 151,  523 => 150,  520 => 149,  514 => 147,  512 => 146,  509 => 145,  504 => 143,  502 => 142,  499 => 141,  496 => 140,  492 => 138,  477 => 136,  471 => 134,  468 => 133,  465 => 132,  462 => 131,  459 => 130,  456 => 129,  453 => 128,  450 => 127,  447 => 126,  444 => 125,  442 => 124,  437 => 123,  419 => 122,  415 => 120,  402 => 118,  398 => 117,  395 => 116,  391 => 114,  376 => 112,  372 => 111,  369 => 110,  367 => 109,  364 => 108,  362 => 107,  359 => 106,  356 => 105,  353 => 104,  350 => 103,  347 => 102,  344 => 101,  338 => 99,  335 => 98,  333 => 97,  328 => 96,  325 => 95,  322 => 94,  319 => 93,  316 => 92,  309 => 88,  305 => 86,  280 => 82,  269 => 79,  267 => 78,  264 => 77,  248 => 70,  238 => 65,  221 => 60,  216 => 59,  203 => 55,  199 => 54,  179 => 49,  123 => 38,  117 => 36,  109 => 32,  79 => 20,  74 => 21,  61 => 17,  46 => 11,  27 => 4,  22 => 2,  212 => 56,  197 => 53,  194 => 52,  189 => 50,  174 => 47,  169 => 45,  166 => 44,  163 => 45,  146 => 39,  140 => 37,  134 => 37,  122 => 34,  113 => 33,  108 => 30,  106 => 29,  103 => 28,  100 => 27,  65 => 19,  57 => 16,  54 => 15,  44 => 10,  41 => 9,  272 => 98,  263 => 94,  258 => 75,  255 => 74,  247 => 88,  244 => 68,  242 => 67,  235 => 64,  231 => 62,  228 => 80,  226 => 79,  222 => 77,  214 => 74,  208 => 72,  206 => 71,  188 => 63,  186 => 52,  183 => 51,  176 => 59,  171 => 48,  168 => 57,  153 => 53,  151 => 41,  148 => 40,  145 => 40,  142 => 39,  139 => 45,  136 => 47,  133 => 43,  130 => 45,  128 => 41,  125 => 43,  119 => 41,  116 => 32,  110 => 34,  107 => 37,  92 => 31,  88 => 23,  82 => 27,  77 => 19,  72 => 23,  68 => 16,  52 => 13,  49 => 12,  43 => 10,  40 => 9,  37 => 8,  35 => 7,  32 => 6,  29 => 5,  21 => 2,  80 => 22,  71 => 20,  104 => 32,  98 => 19,  95 => 18,  86 => 16,  84 => 15,  60 => 17,  85 => 24,  75 => 24,  48 => 9,  45 => 8,  42 => 7,  33 => 5,  30 => 5,  24 => 3,  346 => 108,  341 => 100,  339 => 104,  334 => 102,  327 => 101,  320 => 100,  314 => 91,  310 => 98,  306 => 97,  302 => 96,  298 => 95,  294 => 93,  289 => 83,  283 => 88,  277 => 81,  275 => 99,  271 => 83,  266 => 95,  260 => 93,  254 => 76,  252 => 90,  249 => 89,  241 => 72,  239 => 85,  236 => 70,  234 => 69,  225 => 61,  219 => 62,  213 => 58,  209 => 56,  204 => 56,  201 => 69,  195 => 66,  192 => 53,  190 => 50,  187 => 49,  180 => 45,  170 => 40,  165 => 46,  158 => 44,  152 => 41,  149 => 31,  147 => 30,  143 => 38,  135 => 23,  129 => 22,  124 => 20,  120 => 18,  118 => 17,  115 => 35,  101 => 35,  97 => 26,  76 => 22,  66 => 10,  64 => 14,  62 => 18,  34 => 7,  28 => 3,  26 => 4,  102 => 31,  99 => 34,  96 => 29,  94 => 28,  87 => 16,  83 => 23,  78 => 14,  73 => 12,  69 => 12,  63 => 18,  59 => 18,  55 => 6,  51 => 5,  47 => 11,  39 => 9,  36 => 8,  19 => 1,);
    }
}
