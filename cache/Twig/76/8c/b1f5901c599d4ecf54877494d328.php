<?php

/* pathLine.twig */
class __TwigTemplate_768cb1f5901c599d4ecf54877494d328 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        // line 1
        echo "<div class=\"b-path\"";
        echo $this->getAttribute((isset($context["Design"]) ? $context["Design"] : null), "write", array(0 => " sktag=\"menu.path\""), "method");
        echo ">
    <ul itemscope itemtype=\"";
        // line 2
        echo $this->getAttribute((isset($context["Site"]) ? $context["Site"] : null), "getWebProtocol", array(), "method");
        echo "schema.org/BreadcrumbList\">
    ";
        // line 3
        $context["position"] = 1;
        // line 4
        echo "    ";
        if ((isset($context["main_page"]) ? $context["main_page"] : null)) {
            // line 5
            echo "        <li itemprop=\"itemListElement\" itemscope itemtype=\"";
            echo $this->getAttribute((isset($context["Site"]) ? $context["Site"] : null), "getWebProtocol", array(), "method");
            echo "schema.org/ListItem\"";
            echo $this->getAttribute((isset($context["Design"]) ? $context["Design"] : null), "write", array(0 => " sktag=\"menu.path.a\""), "method");
            echo ">
            <a itemprop=\"item\" href=\"";
            // line 6
            echo twig_escape_filter($this->env, $this->getAttribute((isset($context["main_page"]) ? $context["main_page"] : null), "href"));
            echo "\"><span itemprop=\"name\">";
            echo $this->getAttribute((isset($context["main_page"]) ? $context["main_page"] : null), "title");
            echo "</span></a>
            <meta itemprop=\"position\" content=\"";
            // line 7
            echo (isset($context["position"]) ? $context["position"] : null);
            echo "\" />
        </li>
        ";
            // line 9
            $context["position"] = ((isset($context["position"]) ? $context["position"] : null) + 1);
            // line 10
            echo "    ";
        }
        // line 11
        echo "    ";
        $context['_parent'] = (array) $context;
        $context['_seq'] = twig_ensure_traversable((isset($context["items"]) ? $context["items"] : null));
        foreach ($context['_seq'] as $context["_key"] => $context["item"]) {
            // line 12
            echo "        ";
            if ($this->getAttribute((isset($context["item"]) ? $context["item"] : null), "selected")) {
                // line 13
                echo "            <li itemprop=\"itemListElement\" itemscope itemtype=\"";
                echo $this->getAttribute((isset($context["Site"]) ? $context["Site"] : null), "getWebProtocol", array(), "method");
                echo "schema.org/ListItem\" ";
                echo $this->getAttribute((isset($context["Design"]) ? $context["Design"] : null), "write", array(0 => " sktag=\"menu.path.item\""), "method");
                echo ">
                <span itemprop=\"name\">";
                // line 14
                echo $this->getAttribute((isset($context["item"]) ? $context["item"] : null), "title");
                echo "</span>
                <meta itemprop=\"position\" content=\"";
                // line 15
                echo (isset($context["position"]) ? $context["position"] : null);
                echo "\" />
            </li>
        ";
            } else {
                // line 18
                echo "            <li itemprop=\"itemListElement\" itemscope itemtype=\"";
                echo $this->getAttribute((isset($context["Site"]) ? $context["Site"] : null), "getWebProtocol", array(), "method");
                echo "schema.org/ListItem\"";
                echo $this->getAttribute((isset($context["Design"]) ? $context["Design"] : null), "write", array(0 => " sktag=\"menu.path.a\""), "method");
                echo ">
                <a itemprop=\"item\" href=\"";
                // line 19
                echo twig_escape_filter($this->env, $this->getAttribute((isset($context["item"]) ? $context["item"] : null), "href"));
                echo "\"><span itemprop=\"name\">";
                echo $this->getAttribute((isset($context["item"]) ? $context["item"] : null), "title");
                echo "</span></a>
                <meta itemprop=\"position\" content=\"";
                // line 20
                echo (isset($context["position"]) ? $context["position"] : null);
                echo "\" />
            </li>
        ";
            }
            // line 23
            echo "        ";
            $context["position"] = ((isset($context["position"]) ? $context["position"] : null) + 1);
            // line 24
            echo "    ";
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['_key'], $context['item'], $context['_parent'], $context['loop']);
        $context = array_intersect_key($context, $_parent) + $_parent;
        // line 25
        echo "    </ul>
</div>
";
    }

    public function getTemplateName()
    {
        return "pathLine.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  109 => 25,  103 => 24,  100 => 23,  94 => 20,  88 => 19,  81 => 18,  75 => 15,  71 => 14,  64 => 13,  61 => 12,  56 => 11,  53 => 10,  51 => 9,  46 => 7,  40 => 6,  33 => 5,  30 => 4,  28 => 3,  24 => 2,  19 => 1,);
    }
}
