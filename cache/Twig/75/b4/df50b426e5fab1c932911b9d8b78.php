<?php

/* paginator_tabs.twig */
class __TwigTemplate_75b4df50b426e5fab1c932911b9d8b78 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        // line 1
        if (((twig_length_filter($this->env, $this->getAttribute((isset($context["aPages"]) ? $context["aPages"] : null), "items")) > 1) || ($this->getAttribute((isset($context["aPages"]) ? $context["aPages"] : null), "page") > 1))) {
            // line 2
            echo "
";
            // line 3
            if ($this->getAttribute((isset($context["aPages"]) ? $context["aPages"] : null), "parameters")) {
                // line 4
                echo "    ";
                $context["PageParams"] = ("&" . $this->getAttribute((isset($context["aPages"]) ? $context["aPages"] : null), "parameters"));
            } else {
                // line 6
                echo "    ";
                $context["PageParams"] = "";
            }
            // line 8
            echo "
    <div class=\"b-pageline\">

    ";
            // line 11
            if ($this->getAttribute((isset($context["aPages"]) ? $context["aPages"] : null), "firstPage")) {
                // line 12
                echo "        ";
                if ($this->getAttribute($this->getAttribute((isset($context["aPages"]) ? $context["aPages"] : null), "firstPage"), "isActive")) {
                    // line 13
                    echo "            <a class=\"pageline__back1\" href=\"[";
                    echo $this->getAttribute((isset($context["aPages"]) ? $context["aPages"] : null), "sectionId");
                    echo "][CatalogViewer?";
                    echo $this->getAttribute((isset($context["aPages"]) ? $context["aPages"] : null), "parameters");
                    echo "]\">";
                    echo $this->getAttribute($this->getAttribute((isset($context["aPages"]) ? $context["aPages"] : null), "firstPage"), "title");
                    echo "</a>
        ";
                }
                // line 15
                echo "    ";
            }
            // line 16
            echo "
    ";
            // line 17
            if ($this->getAttribute((isset($context["aPages"]) ? $context["aPages"] : null), "prevGroup")) {
                // line 18
                echo "        ";
                if ($this->getAttribute($this->getAttribute((isset($context["aPages"]) ? $context["aPages"] : null), "prevGroup"), "isActive")) {
                    // line 19
                    echo "            <a class=\"pageline__back1\" href=\"[";
                    echo $this->getAttribute((isset($context["aPages"]) ? $context["aPages"] : null), "sectionId");
                    echo "][CatalogViewer?";
                    echo $this->getAttribute((isset($context["aPages"]) ? $context["aPages"] : null), "parameters");
                    echo "&page=";
                    echo $this->getAttribute($this->getAttribute((isset($context["aPages"]) ? $context["aPages"] : null), "prevGroup"), "page");
                    echo "]\">";
                    echo $this->getAttribute($this->getAttribute((isset($context["aPages"]) ? $context["aPages"] : null), "prevGroup"), "title");
                    echo "</a>
        ";
                }
                // line 21
                echo "    ";
            }
            // line 22
            echo "
    ";
            // line 23
            if ($this->getAttribute((isset($context["aPages"]) ? $context["aPages"] : null), "prevPage")) {
                // line 24
                echo "        ";
                if ($this->getAttribute($this->getAttribute((isset($context["aPages"]) ? $context["aPages"] : null), "prevPage"), "isActive")) {
                    // line 25
                    echo "            <a class=\"pageline__back2\" href=\"[";
                    echo $this->getAttribute((isset($context["aPages"]) ? $context["aPages"] : null), "sectionId");
                    echo "][CatalogViewer?";
                    echo $this->getAttribute((isset($context["aPages"]) ? $context["aPages"] : null), "parameters");
                    echo "&page=";
                    echo $this->getAttribute($this->getAttribute((isset($context["aPages"]) ? $context["aPages"] : null), "prevPage"), "page");
                    echo "]\">";
                    echo $this->getAttribute($this->getAttribute((isset($context["aPages"]) ? $context["aPages"] : null), "prevPage"), "title");
                    echo "</a>
        ";
                }
                // line 27
                echo "    ";
            }
            // line 28
            echo "
    ";
            // line 29
            if ($this->getAttribute((isset($context["aPages"]) ? $context["aPages"] : null), "itemsIsActive")) {
                // line 30
                echo "        ";
                $context['_parent'] = (array) $context;
                $context['_seq'] = twig_ensure_traversable($this->getAttribute((isset($context["aPages"]) ? $context["aPages"] : null), "items"));
                foreach ($context['_seq'] as $context["_key"] => $context["page"]) {
                    // line 31
                    echo "            ";
                    if ($this->getAttribute((isset($context["page"]) ? $context["page"] : null), "isActive")) {
                        // line 32
                        echo "                <a class=\"pageline__on\" href=\"\">";
                        echo $this->getAttribute((isset($context["page"]) ? $context["page"] : null), "title");
                        echo "</a>
            ";
                    } else {
                        // line 34
                        echo "                <a href=\"[";
                        echo $this->getAttribute((isset($context["aPages"]) ? $context["aPages"] : null), "sectionId");
                        echo "][CatalogViewer?";
                        echo $this->getAttribute((isset($context["aPages"]) ? $context["aPages"] : null), "parameters");
                        echo "&page=";
                        echo $this->getAttribute((isset($context["page"]) ? $context["page"] : null), "page");
                        echo "]\">";
                        echo $this->getAttribute((isset($context["page"]) ? $context["page"] : null), "title");
                        echo "</a>
            ";
                    }
                    // line 36
                    echo "        ";
                }
                $_parent = $context['_parent'];
                unset($context['_seq'], $context['_iterated'], $context['_key'], $context['page'], $context['_parent'], $context['loop']);
                $context = array_intersect_key($context, $_parent) + $_parent;
                // line 37
                echo "    ";
            }
            // line 38
            echo "
    ";
            // line 39
            if ($this->getAttribute((isset($context["aPages"]) ? $context["aPages"] : null), "nextPage")) {
                // line 40
                echo "        ";
                if ($this->getAttribute($this->getAttribute((isset($context["aPages"]) ? $context["aPages"] : null), "nextPage"), "isActive")) {
                    // line 41
                    echo "            <a class=\"pageline__next2\" href=\"[";
                    echo $this->getAttribute((isset($context["aPages"]) ? $context["aPages"] : null), "sectionId");
                    echo "][CatalogViewer?";
                    echo $this->getAttribute((isset($context["aPages"]) ? $context["aPages"] : null), "parameters");
                    echo "&page=";
                    echo $this->getAttribute($this->getAttribute((isset($context["aPages"]) ? $context["aPages"] : null), "nextPage"), "page");
                    echo "]\">";
                    echo $this->getAttribute($this->getAttribute((isset($context["aPages"]) ? $context["aPages"] : null), "nextPage"), "title");
                    echo "</a>
        ";
                }
                // line 43
                echo "    ";
            }
            // line 44
            echo "
    ";
            // line 45
            if ($this->getAttribute((isset($context["aPages"]) ? $context["aPages"] : null), "nextGroup")) {
                // line 46
                echo "        ";
                if ($this->getAttribute($this->getAttribute((isset($context["aPages"]) ? $context["aPages"] : null), "nextGroup"), "isActive")) {
                    // line 47
                    echo "            <a class=\"pageline__next1\" href=\"[";
                    echo $this->getAttribute((isset($context["aPages"]) ? $context["aPages"] : null), "sectionId");
                    echo "][CatalogViewer?";
                    echo $this->getAttribute((isset($context["aPages"]) ? $context["aPages"] : null), "parameters");
                    echo "&page=";
                    echo $this->getAttribute($this->getAttribute((isset($context["aPages"]) ? $context["aPages"] : null), "nextGroup"), "page");
                    echo "]\">";
                    echo $this->getAttribute($this->getAttribute((isset($context["aPages"]) ? $context["aPages"] : null), "nextGroup"), "title");
                    echo "</a>
        ";
                }
                // line 49
                echo "    ";
            }
            // line 50
            echo "
    ";
            // line 51
            if ($this->getAttribute((isset($context["aPages"]) ? $context["aPages"] : null), "lastPage")) {
                // line 52
                echo "        ";
                if ($this->getAttribute($this->getAttribute((isset($context["aPages"]) ? $context["aPages"] : null), "lastPage"), "isActive")) {
                    // line 53
                    echo "            <a class=\"pageline__next1\" href=\"[";
                    echo $this->getAttribute((isset($context["aPages"]) ? $context["aPages"] : null), "sectionId");
                    echo "][CatalogViewer?";
                    echo $this->getAttribute((isset($context["aPages"]) ? $context["aPages"] : null), "parameters");
                    echo "&page=";
                    echo $this->getAttribute($this->getAttribute((isset($context["aPages"]) ? $context["aPages"] : null), "lastPage"), "page");
                    echo "]\">";
                    echo $this->getAttribute($this->getAttribute((isset($context["aPages"]) ? $context["aPages"] : null), "lastPage"), "title");
                    echo "</a>
        ";
                }
                // line 55
                echo "    ";
            }
            // line 56
            echo "
</div>
";
        }
    }

    public function getTemplateName()
    {
        return "paginator_tabs.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  212 => 56,  197 => 53,  194 => 52,  189 => 50,  174 => 47,  169 => 45,  166 => 44,  163 => 43,  146 => 39,  140 => 37,  134 => 36,  122 => 34,  113 => 31,  108 => 30,  106 => 29,  103 => 28,  100 => 27,  65 => 19,  57 => 16,  54 => 15,  44 => 13,  41 => 12,  272 => 98,  263 => 94,  258 => 92,  255 => 91,  247 => 88,  244 => 87,  242 => 86,  235 => 83,  231 => 81,  228 => 80,  226 => 79,  222 => 77,  214 => 74,  208 => 72,  206 => 71,  188 => 63,  186 => 49,  183 => 61,  176 => 59,  171 => 46,  168 => 57,  153 => 53,  151 => 41,  148 => 40,  145 => 50,  142 => 49,  139 => 48,  136 => 47,  133 => 46,  130 => 45,  128 => 44,  125 => 43,  119 => 41,  116 => 32,  110 => 38,  107 => 37,  92 => 31,  88 => 25,  82 => 27,  77 => 21,  72 => 23,  68 => 22,  52 => 14,  49 => 13,  43 => 10,  40 => 9,  37 => 8,  35 => 7,  32 => 6,  29 => 5,  21 => 2,  80 => 22,  71 => 13,  104 => 20,  98 => 19,  95 => 18,  86 => 16,  84 => 15,  60 => 17,  85 => 24,  75 => 24,  48 => 9,  45 => 8,  42 => 7,  33 => 5,  30 => 6,  24 => 3,  346 => 108,  341 => 105,  339 => 104,  334 => 102,  327 => 101,  320 => 100,  314 => 99,  310 => 98,  306 => 97,  302 => 96,  298 => 95,  294 => 93,  289 => 90,  283 => 88,  277 => 86,  275 => 99,  271 => 83,  266 => 95,  260 => 93,  254 => 76,  252 => 90,  249 => 89,  241 => 72,  239 => 85,  236 => 70,  234 => 69,  225 => 63,  219 => 62,  213 => 59,  209 => 55,  204 => 56,  201 => 69,  195 => 66,  192 => 51,  190 => 50,  187 => 49,  180 => 45,  170 => 40,  165 => 56,  158 => 54,  152 => 32,  149 => 31,  147 => 30,  143 => 38,  135 => 23,  129 => 22,  124 => 20,  120 => 18,  118 => 17,  115 => 16,  101 => 35,  97 => 14,  76 => 13,  66 => 10,  64 => 11,  62 => 18,  34 => 8,  28 => 3,  26 => 4,  102 => 24,  99 => 34,  96 => 33,  94 => 17,  87 => 16,  83 => 23,  78 => 14,  73 => 12,  69 => 12,  63 => 20,  59 => 18,  55 => 6,  51 => 5,  47 => 4,  39 => 11,  36 => 2,  19 => 1,);
    }
}
