<?php

/* /el/select.twig */
class __TwigTemplate_fe7aee7a52edbe12d342529e011017bf extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        // line 1
        $context["sClassField"] = $this->getAttribute((isset($context["oField"]) ? $context["oField"] : null), "getVal", array(0 => "label_position"), "method");
        // line 2
        echo "<div class=\"form__col-";
        echo $this->getAttribute((isset($context["oField"]) ? $context["oField"] : null), "getClassVal", array(), "method");
        echo "\">
    <div class=\"form__item form__item--label-";
        // line 3
        echo $this->getAttribute((isset($context["oField"]) ? $context["oField"] : null), "getVal", array(0 => "label_position"), "method");
        echo "\">
        ";
        // line 4
        if (($this->getAttribute((isset($context["oField"]) ? $context["oField"] : null), "getVal", array(0 => "group"), "method") == "0")) {
            // line 5
            echo "            ";
            if (($this->getAttribute((isset($context["oField"]) ? $context["oField"] : null), "getVal", array(0 => "label_position"), "method") != "right")) {
                // line 6
                echo "                <div class=\"form__label\">";
                echo $this->getAttribute((isset($context["oField"]) ? $context["oField"] : null), "getVal", array(0 => "param_title"), "method");
                if (($this->getAttribute((isset($context["oField"]) ? $context["oField"] : null), "getVal", array(0 => "param_required"), "method") == 1)) {
                    echo " <ins class=\"form__mark\">*</ins>";
                }
                echo "</div>
            ";
            }
            // line 8
            echo "        ";
        }
        // line 9
        echo "        <div class=\"form__input form__input--select\">
            <select data-hash=\"";
        // line 10
        echo (isset($context["formHash"]) ? $context["formHash"] : null);
        echo "\" data-name=\"";
        echo $this->getAttribute((isset($context["oField"]) ? $context["oField"] : null), "getVal", array(0 => "param_title"), "method");
        echo "\" class=\"js-form-select\" id=\"";
        echo $this->getAttribute((isset($context["oField"]) ? $context["oField"] : null), "getVal", array(0 => "param_name"), "method");
        echo "_";
        echo (isset($context["formHash"]) ? $context["formHash"] : null);
        echo "\" name=\"";
        echo $this->getAttribute((isset($context["oField"]) ? $context["oField"] : null), "getVal", array(0 => "param_name"), "method");
        echo "\"  ";
        echo $this->getAttribute((isset($context["oField"]) ? $context["oField"] : null), "getVal", array(0 => "param_man_params"), "method");
        echo " data-placeholder=\"";
        echo $this->getAttribute((isset($context["oField"]) ? $context["oField"] : null), "getVal", array(0 => "param_title"), "method");
        echo "\" ";
        echo $this->getAttribute((isset($context["oField"]) ? $context["oField"] : null), "getVal", array(0 => "param_man_params"), "method");
        echo ">
                ";
        // line 11
        $context["aItems"] = $this->getAttribute((isset($context["oField"]) ? $context["oField"] : null), "getViewItems", array(), "method");
        // line 12
        echo "                ";
        if (((isset($context["sClassField"]) ? $context["sClassField"] : null) == "none")) {
            // line 13
            echo "                    <option selected disabled>";
            echo $this->getAttribute((isset($context["oField"]) ? $context["oField"] : null), "getVal", array(0 => "param_title"), "method");
            echo "</option>
                ";
        }
        // line 15
        echo "                ";
        $context['_parent'] = (array) $context;
        $context['_seq'] = twig_ensure_traversable((isset($context["aItems"]) ? $context["aItems"] : null));
        foreach ($context['_seq'] as $context["_key"] => $context["aItem"]) {
            // line 16
            echo "                    <option value=\"";
            echo $this->getAttribute((isset($context["aItem"]) ? $context["aItem"] : null), "value");
            echo "\">";
            echo $this->getAttribute((isset($context["aItem"]) ? $context["aItem"] : null), "title");
            echo "</option>
                ";
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['_key'], $context['aItem'], $context['_parent'], $context['loop']);
        $context = array_intersect_key($context, $_parent) + $_parent;
        // line 18
        echo "            </select>
        </div>
        ";
        // line 20
        if (($this->getAttribute((isset($context["oField"]) ? $context["oField"] : null), "getVal", array(0 => "label_position"), "method") == "right")) {
            // line 21
            echo "            <div class=\"form__label\">";
            echo $this->getAttribute((isset($context["oField"]) ? $context["oField"] : null), "getVal", array(0 => "param_title"), "method");
            if (($this->getAttribute((isset($context["oField"]) ? $context["oField"] : null), "getVal", array(0 => "param_required"), "method") == 1)) {
                echo " <ins class=\"form__mark\">*</ins>";
            }
            echo "</div>
        ";
        }
        // line 23
        echo "    </div>
    ";
        // line 24
        if (($this->getAttribute((isset($context["oField"]) ? $context["oField"] : null), "getVal", array(0 => "param_description"), "method") != "")) {
            echo "<div class=\"form__info\">";
            echo $this->getAttribute((isset($context["oField"]) ? $context["oField"] : null), "getVal", array(0 => "param_description"), "method");
            echo "</div>";
        }
        // line 25
        echo "</div>";
    }

    public function getTemplateName()
    {
        return "/el/select.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  119 => 25,  113 => 24,  110 => 23,  101 => 21,  99 => 20,  95 => 18,  84 => 16,  79 => 15,  73 => 13,  70 => 12,  68 => 11,  50 => 10,  47 => 9,  44 => 8,  35 => 6,  32 => 5,  30 => 4,  26 => 3,  21 => 2,  19 => 1,);
    }
}
