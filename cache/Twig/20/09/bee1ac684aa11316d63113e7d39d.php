<?php

/* htaccess.twig */
class __TwigTemplate_2009bee1ac684aa11316d63113e7d39d extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        // line 1
        echo "# generated: ";
        echo twig_date_format_filter($this->env, "now", "d-m-Y H:i:s");
        echo "
# Special version

ErrorDocument 404 /404.php

Options +FollowSymLinks
Options -Indexes
rewriteEngine on

# блокируем все url начинающаяся с точкой(.git,.idea)
RedirectMatch 404 /\\..*\$

RewriteCond %{REQUEST_URI} robots\\.txt*
RewriteRule ^(.*)\$ index.php [L]

RewriteCond %{REQUEST_URI} /gateway/index\\.php*
RewriteRule ^(.*)\$ index.php [L]

RewriteCond %{REQUEST_FILENAME} !-f
RewriteCond %{REQUEST_FILENAME} !-d
RewriteCond %{REQUEST_URI} ![.][^\\/]*\$
RewriteRule ^(.*)\$ /index.php

RewriteCond %{REQUEST_FILENAME} !-f
RewriteCond %{REQUEST_FILENAME} !-d
RewriteCond %{REQUEST_URI} (.*)\\.php\$
RewriteRule ^(.*)\$ /index.php
";
    }

    public function getTemplateName()
    {
        return "htaccess.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  191 => 87,  186 => 83,  184 => 82,  182 => 81,  172 => 78,  168 => 77,  166 => 76,  154 => 66,  144 => 63,  140 => 62,  131 => 60,  127 => 59,  125 => 58,  104 => 39,  94 => 36,  90 => 35,  81 => 33,  77 => 32,  75 => 31,  62 => 20,  52 => 17,  48 => 16,  39 => 14,  35 => 13,  33 => 12,  21 => 2,  19 => 1,);
    }
}
