<?php

/* bottomMenu.twig */
class __TwigTemplate_51678c35c807e29dcd587d0b5f57abe5 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        // line 1
        echo "<p class=\"b-menufoot\">
    ";
        // line 2
        $context['_parent'] = (array) $context;
        $context['_seq'] = twig_ensure_traversable((isset($context["items"]) ? $context["items"] : null));
        $context['loop'] = array(
          'parent' => $context['_parent'],
          'index0' => 0,
          'index'  => 1,
          'first'  => true,
        );
        if (is_array($context['_seq']) || (is_object($context['_seq']) && $context['_seq'] instanceof Countable)) {
            $length = count($context['_seq']);
            $context['loop']['revindex0'] = $length - 1;
            $context['loop']['revindex'] = $length;
            $context['loop']['length'] = $length;
            $context['loop']['last'] = 1 === $length;
        }
        foreach ($context['_seq'] as $context["_key"] => $context["item"]) {
            // line 3
            echo "        <a href=\"";
            echo $this->getAttribute((isset($context["item"]) ? $context["item"] : null), "href");
            echo "\" class=\"";
            if ($this->getAttribute((isset($context["item"]) ? $context["item"] : null), "selected")) {
                echo " on";
            }
            echo "\"><ins></ins>";
            echo $this->getAttribute((isset($context["item"]) ? $context["item"] : null), "title");
            echo "</a>";
            if ((!$this->getAttribute((isset($context["loop"]) ? $context["loop"] : null), "last"))) {
                echo " |";
            }
            // line 4
            echo "    ";
            ++$context['loop']['index0'];
            ++$context['loop']['index'];
            $context['loop']['first'] = false;
            if (isset($context['loop']['length'])) {
                --$context['loop']['revindex0'];
                --$context['loop']['revindex'];
                $context['loop']['last'] = 0 === $context['loop']['revindex0'];
            }
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['_key'], $context['item'], $context['_parent'], $context['loop']);
        $context = array_intersect_key($context, $_parent) + $_parent;
        // line 5
        echo "</p>
";
    }

    public function getTemplateName()
    {
        return "bottomMenu.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  22 => 2,  129 => 26,  121 => 24,  115 => 21,  97 => 17,  90 => 16,  82 => 15,  79 => 14,  71 => 12,  47 => 10,  45 => 9,  28 => 4,  26 => 3,  72 => 16,  64 => 13,  37 => 7,  33 => 6,  29 => 4,  25 => 3,  310 => 74,  304 => 70,  296 => 67,  278 => 65,  273 => 64,  270 => 63,  260 => 61,  258 => 60,  254 => 58,  248 => 56,  246 => 55,  242 => 53,  238 => 51,  232 => 50,  228 => 49,  225 => 48,  218 => 44,  209 => 42,  203 => 41,  200 => 40,  194 => 39,  190 => 37,  182 => 36,  173 => 35,  165 => 33,  146 => 31,  142 => 30,  130 => 29,  125 => 26,  119 => 23,  116 => 23,  110 => 21,  107 => 18,  101 => 18,  99 => 17,  95 => 15,  80 => 14,  66 => 5,  58 => 12,  55 => 10,  52 => 4,  48 => 8,  27 => 4,  24 => 3,  21 => 2,  237 => 49,  223 => 47,  219 => 46,  215 => 43,  201 => 43,  197 => 41,  193 => 39,  179 => 38,  170 => 34,  159 => 32,  156 => 32,  139 => 30,  136 => 29,  134 => 28,  128 => 25,  124 => 24,  109 => 21,  106 => 20,  89 => 19,  86 => 18,  84 => 17,  77 => 14,  73 => 12,  67 => 10,  61 => 12,  43 => 5,  40 => 4,  23 => 3,  65 => 9,  53 => 9,  41 => 7,  39 => 3,  36 => 6,  32 => 3,  30 => 2,  19 => 1,);
    }
}
