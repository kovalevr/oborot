<?php

/* answer.twig */
class __TwigTemplate_fce7c10dbdff21e6c1bee40192b52055 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        // line 1
        echo "<div class=\"b-answer";
        if ((isset($context["popup_result_page"]) ? $context["popup_result_page"] : null)) {
            echo " b-answer--popup";
        }
        echo "\">
    ";
        // line 2
        if ((isset($context["SuccAnswer"]) ? $context["SuccAnswer"] : null)) {
            // line 3
            echo "        ";
            // line 4
            echo "        <p>";
            echo (isset($context["SuccAnswer"]) ? $context["SuccAnswer"] : null);
            echo "</p>
    ";
        } else {
            // line 6
            echo "
        ";
            // line 7
            if (((isset($context["successAjax"]) ? $context["successAjax"] : null) == 1)) {
                // line 8
                echo "
            ";
                // line 9
                if ((isset($context["popup_result_page"]) ? $context["popup_result_page"] : null)) {
                    // line 10
                    echo "                <p>";
                    echo $this->getAttribute((isset($context["Lang"]) ? $context["Lang"] : null), "get", array(0 => "forms.ans_success"), "method");
                    echo "</p>
            ";
                } else {
                    // line 12
                    echo "                <p>";
                    echo $this->getAttribute((isset($context["Lang"]) ? $context["Lang"] : null), "get", array(0 => "forms.ans_success"), "method");
                    echo "</p>
            ";
                }
                // line 14
                echo "
        ";
            }
            // line 16
            echo "
        ";
            // line 17
            if (((isset($context["success"]) ? $context["success"] : null) == 1)) {
                // line 18
                echo "            <p>";
                echo $this->getAttribute((isset($context["Lang"]) ? $context["Lang"] : null), "get", array(0 => "forms.ans_success"), "method");
                echo "</p>
        ";
            }
            // line 20
            echo "
        ";
            // line 21
            if (((isset($context["success_otziv"]) ? $context["success_otziv"] : null) == 1)) {
                // line 22
                echo "            <p>";
                echo $this->getAttribute((isset($context["Lang"]) ? $context["Lang"] : null), "get", array(0 => "forms.ans_success_otziv"), "method");
                echo "</p>
        ";
            }
            // line 24
            echo "
    ";
        }
        // line 26
        echo "
        ";
        // line 27
        if (((isset($context["success"]) ? $context["success"] : null) == 1)) {
            // line 28
            echo "            <p><a href=\"[";
            echo (isset($context["form_section"]) ? $context["form_section"] : null);
            echo "]\">";
            echo $this->getAttribute((isset($context["Lang"]) ? $context["Lang"] : null), "get", array(0 => "page.back"), "method");
            echo "</a></p>
        ";
        } elseif (((isset($context["successAjax"]) ? $context["successAjax"] : null) != 1)) {
            // line 30
            echo "            <p><a href=\"javascript: history.go(-1);\">";
            echo $this->getAttribute((isset($context["Lang"]) ? $context["Lang"] : null), "get", array(0 => "Forms.back"), "method");
            echo "</a></p>
        ";
        } elseif (((isset($context["successAjax"]) ? $context["successAjax"] : null) == 1)) {
            // line 32
            echo "            <p><a href=\"javascript: top.location.reload(false);\">";
            echo $this->getAttribute((isset($context["Lang"]) ? $context["Lang"] : null), "get", array(0 => "Forms.back"), "method");
            echo "</a></p>
        ";
        }
        // line 34
        echo "
";
        // line 35
        if ((isset($context["ecommerce"]) ? $context["ecommerce"] : null)) {
            echo (isset($context["ecommerce"]) ? $context["ecommerce"] : null);
        }
        // line 36
        if ((isset($context["reachGoals"]) ? $context["reachGoals"] : null)) {
            echo (isset($context["reachGoals"]) ? $context["reachGoals"] : null);
        }
        // line 37
        echo "
</div>";
    }

    public function getTemplateName()
    {
        return "answer.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  107 => 32,  101 => 30,  93 => 28,  91 => 27,  88 => 26,  84 => 24,  78 => 22,  76 => 21,  73 => 20,  67 => 18,  65 => 17,  58 => 14,  52 => 12,  46 => 10,  41 => 8,  39 => 7,  36 => 6,  30 => 4,  26 => 2,  136 => 38,  133 => 37,  124 => 37,  120 => 36,  116 => 35,  113 => 34,  102 => 30,  98 => 28,  94 => 27,  89 => 25,  85 => 24,  81 => 23,  77 => 22,  72 => 19,  68 => 17,  62 => 16,  56 => 13,  51 => 11,  47 => 9,  44 => 9,  40 => 7,  37 => 6,  35 => 5,  28 => 3,  22 => 2,  19 => 1,);
    }
}
