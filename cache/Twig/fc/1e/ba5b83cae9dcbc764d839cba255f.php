<?php

/* leftMenu.twig */
class __TwigTemplate_fc1eba5b83cae9dcbc764d839cba255f extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        // line 1
        echo "<div class=\"b-menu context\"";
        echo $this->getAttribute((isset($context["Design"]) ? $context["Design"] : null), "write", array(0 => " sktag=\"menu.left\""), "method");
        echo ">
    <ul class=\"menu__level-1\"";
        // line 2
        echo $this->getAttribute((isset($context["Design"]) ? $context["Design"] : null), "write", array(0 => " sktag=\"menu.left.level1\""), "method");
        echo ">
    ";
        // line 3
        $context['_parent'] = (array) $context;
        $context['_seq'] = twig_ensure_traversable((isset($context["items"]) ? $context["items"] : null));
        foreach ($context['_seq'] as $context["_key"] => $context["item"]) {
            // line 4
            echo "    ";
            if ($this->getAttribute((isset($context["item"]) ? $context["item"] : null), "show")) {
                // line 5
                echo "        <li class=\"menu__item-1";
                if ($this->getAttribute((isset($context["item"]) ? $context["item"] : null), "selected")) {
                    echo " menu__item-1--on";
                }
                echo "\">
            <span>
                <a href=\"";
                // line 7
                echo $this->getAttribute((isset($context["item"]) ? $context["item"] : null), "href");
                echo "\">
                    ";
                // line 8
                if ($this->getAttribute((isset($context["icon"]) ? $context["icon"] : null), $this->getAttribute((isset($context["item"]) ? $context["item"] : null), "id", array(), "array"), array(), "array", true, true)) {
                    // line 9
                    echo "                        <em style=\"background-image: url(";
                    echo $this->getAttribute((isset($context["icon"]) ? $context["icon"] : null), $this->getAttribute((isset($context["item"]) ? $context["item"] : null), "id", array(), "array"), array(), "array");
                    echo ");\"></em>
                    ";
                } else {
                    // line 11
                    echo "                        <ins></ins>
                    ";
                }
                // line 13
                echo "                    ";
                echo $this->getAttribute((isset($context["item"]) ? $context["item"] : null), "title");
                echo "
                </a>
            </span>
        ";
                // line 16
                if ((twig_length_filter($this->env, $this->getAttribute((isset($context["item"]) ? $context["item"] : null), "items")) > 0)) {
                    // line 17
                    echo "            <ul class=\"menu__level-2\"";
                    echo $this->getAttribute((isset($context["Design"]) ? $context["Design"] : null), "write", array(0 => " sktag=\"menu.left.level2\""), "method");
                    echo ">
                ";
                    // line 18
                    $context['_parent'] = (array) $context;
                    $context['_seq'] = twig_ensure_traversable($this->getAttribute((isset($context["item"]) ? $context["item"] : null), "items"));
                    foreach ($context['_seq'] as $context["_key"] => $context["item2"]) {
                        // line 19
                        echo "                ";
                        if ($this->getAttribute((isset($context["item2"]) ? $context["item2"] : null), "show")) {
                            // line 20
                            echo "                    <li class=\"menu__item-2";
                            if ($this->getAttribute((isset($context["item2"]) ? $context["item2"] : null), "selected")) {
                                echo " menu__item-2--on";
                            }
                            echo "\">
                        <span>
                            <a href=\"";
                            // line 22
                            echo $this->getAttribute((isset($context["item2"]) ? $context["item2"] : null), "href");
                            echo "\">";
                            echo $this->getAttribute((isset($context["item2"]) ? $context["item2"] : null), "title");
                            echo "</a>
                        </span>
                    ";
                            // line 24
                            if ((twig_length_filter($this->env, $this->getAttribute((isset($context["item2"]) ? $context["item2"] : null), "items")) > 0)) {
                                // line 25
                                echo "                        <ul class=\"menu__level-3\"";
                                echo $this->getAttribute((isset($context["Design"]) ? $context["Design"] : null), "write", array(0 => " sktag=\"menu.left.level3\""), "method");
                                echo ">
                        ";
                                // line 26
                                $context['_parent'] = (array) $context;
                                $context['_seq'] = twig_ensure_traversable($this->getAttribute((isset($context["item2"]) ? $context["item2"] : null), "items"));
                                foreach ($context['_seq'] as $context["_key"] => $context["item3"]) {
                                    // line 27
                                    echo "                        ";
                                    if ($this->getAttribute((isset($context["item3"]) ? $context["item3"] : null), "show")) {
                                        // line 28
                                        echo "                            <li class=\"menu__item-3";
                                        if ($this->getAttribute((isset($context["item3"]) ? $context["item3"] : null), "selected")) {
                                            echo " menu__item-3--on";
                                        }
                                        echo "\">
                                <span>
                                    <a href=\"";
                                        // line 30
                                        echo $this->getAttribute((isset($context["item3"]) ? $context["item3"] : null), "href");
                                        echo "\">";
                                        echo $this->getAttribute((isset($context["item3"]) ? $context["item3"] : null), "title");
                                        echo "</a>
                                </span>
                            </li>                    
                        ";
                                    }
                                    // line 34
                                    echo "                        ";
                                }
                                $_parent = $context['_parent'];
                                unset($context['_seq'], $context['_iterated'], $context['_key'], $context['item3'], $context['_parent'], $context['loop']);
                                $context = array_intersect_key($context, $_parent) + $_parent;
                                // line 35
                                echo "                        </ul>                    
                    ";
                            }
                            // line 37
                            echo "                    </li>
                ";
                        }
                        // line 39
                        echo "                ";
                    }
                    $_parent = $context['_parent'];
                    unset($context['_seq'], $context['_iterated'], $context['_key'], $context['item2'], $context['_parent'], $context['loop']);
                    $context = array_intersect_key($context, $_parent) + $_parent;
                    // line 40
                    echo "            </ul>
            ";
                }
                // line 42
                echo "        </li>
    ";
            }
            // line 44
            echo "    ";
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['_key'], $context['item'], $context['_parent'], $context['loop']);
        $context = array_intersect_key($context, $_parent) + $_parent;
        // line 45
        echo "    </ul>
</div>
";
    }

    public function getTemplateName()
    {
        return "leftMenu.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  160 => 45,  154 => 44,  150 => 42,  140 => 39,  132 => 35,  126 => 34,  117 => 30,  102 => 26,  88 => 22,  68 => 17,  59 => 13,  49 => 9,  35 => 5,  22 => 2,  129 => 26,  121 => 24,  115 => 21,  97 => 25,  90 => 16,  82 => 15,  79 => 14,  71 => 12,  47 => 8,  45 => 9,  28 => 3,  26 => 3,  72 => 16,  64 => 13,  37 => 7,  33 => 6,  29 => 4,  25 => 3,  310 => 74,  304 => 70,  296 => 67,  278 => 65,  273 => 64,  270 => 63,  260 => 61,  258 => 60,  254 => 58,  248 => 56,  246 => 55,  242 => 53,  238 => 51,  232 => 50,  228 => 49,  225 => 48,  218 => 44,  209 => 42,  203 => 41,  200 => 40,  194 => 39,  190 => 37,  182 => 36,  173 => 35,  165 => 33,  146 => 40,  142 => 30,  130 => 29,  125 => 26,  119 => 23,  116 => 23,  110 => 21,  107 => 18,  101 => 18,  99 => 17,  95 => 24,  80 => 20,  66 => 16,  58 => 12,  55 => 11,  52 => 4,  48 => 8,  27 => 4,  24 => 2,  21 => 2,  237 => 49,  223 => 47,  219 => 46,  215 => 43,  201 => 43,  197 => 41,  193 => 39,  179 => 38,  170 => 34,  159 => 32,  156 => 32,  139 => 30,  136 => 37,  134 => 28,  128 => 25,  124 => 24,  109 => 28,  106 => 27,  89 => 19,  86 => 18,  84 => 17,  77 => 19,  73 => 18,  67 => 10,  61 => 12,  43 => 7,  40 => 4,  23 => 3,  65 => 9,  53 => 9,  41 => 7,  39 => 3,  36 => 6,  32 => 4,  30 => 2,  19 => 1,);
    }
}
