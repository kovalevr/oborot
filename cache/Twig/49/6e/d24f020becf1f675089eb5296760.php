<?php

/* mini_form.twig */
class __TwigTemplate_496ed24f020becf1f675089eb5296760 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        // line 1
        echo "<div class=\"b-search\"";
        echo $this->getAttribute((isset($context["Design"]) ? $context["Design"] : null), "write", array(0 => " sktag=\"modules.search\""), "method");
        echo ">
    <form method=\"GET\" class=\"js_mini_form_search\" action=\"[";
        // line 2
        echo (isset($context["search_section"]) ? $context["search_section"] : null);
        echo "]\">
        <button type=\"submit\"></button>
        <div class=\"search_inputbox\"><div><input class=\"";
        // line 4
        if ((isset($context["bHidePlaceHolder"]) ? $context["bHidePlaceHolder"] : null)) {
            echo "js_hide_placeholder";
        }
        echo "\" placeholder=\"";
        echo $this->getAttribute((isset($context["Lang"]) ? $context["Lang"] : null), "get", array(0 => "search.placeholder_mini_form"), "method");
        echo "\" value=\"\" name=\"search_text\" id=\"search_text\" /></div></div>
    </form>
</div>
";
    }

    public function getTemplateName()
    {
        return "mini_form.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  60 => 14,  34 => 6,  161 => 41,  137 => 35,  114 => 30,  96 => 27,  83 => 26,  74 => 20,  69 => 23,  42 => 13,  38 => 11,  160 => 45,  154 => 44,  150 => 36,  140 => 39,  132 => 33,  126 => 34,  117 => 30,  102 => 26,  88 => 22,  68 => 17,  59 => 13,  49 => 17,  35 => 5,  22 => 2,  129 => 26,  121 => 24,  115 => 21,  97 => 25,  90 => 16,  82 => 15,  79 => 14,  71 => 12,  47 => 8,  45 => 9,  28 => 3,  26 => 3,  72 => 16,  64 => 13,  37 => 7,  33 => 6,  29 => 4,  25 => 3,  310 => 74,  304 => 70,  296 => 67,  278 => 65,  273 => 64,  270 => 63,  260 => 61,  258 => 60,  254 => 58,  248 => 56,  246 => 55,  242 => 53,  238 => 51,  232 => 50,  228 => 49,  225 => 48,  218 => 44,  209 => 42,  203 => 41,  200 => 40,  194 => 39,  190 => 37,  182 => 36,  173 => 35,  165 => 33,  146 => 40,  142 => 30,  130 => 29,  125 => 26,  119 => 32,  116 => 23,  110 => 21,  107 => 18,  101 => 29,  99 => 17,  95 => 24,  80 => 25,  66 => 16,  58 => 12,  55 => 12,  52 => 4,  48 => 10,  27 => 4,  24 => 2,  21 => 2,  237 => 49,  223 => 47,  219 => 46,  215 => 43,  201 => 43,  197 => 41,  193 => 39,  179 => 38,  170 => 34,  159 => 32,  156 => 32,  139 => 30,  136 => 37,  134 => 28,  128 => 25,  124 => 24,  109 => 28,  106 => 27,  89 => 19,  86 => 18,  84 => 17,  77 => 19,  73 => 18,  67 => 18,  61 => 12,  43 => 8,  40 => 4,  23 => 4,  65 => 22,  53 => 9,  41 => 7,  39 => 3,  36 => 10,  32 => 4,  30 => 4,  19 => 1,);
    }
}
