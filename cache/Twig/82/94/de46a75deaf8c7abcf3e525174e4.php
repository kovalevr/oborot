<?php

/* form.twig */
class __TwigTemplate_8294de46a75deaf8c7abcf3e525174e4 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        // line 1
        echo "<div id=\"js-";
        echo (isset($context["form_name"]) ? $context["form_name"] : null);
        echo "\" class=\"b-form js-form\"";
        echo $this->getAttribute((isset($context["Design"]) ? $context["Design"] : null), "write", array(0 => " sktag=\"modules.forms\""), "method");
        echo ">
    ";
        // line 2
        if ($this->getAttribute((isset($context["oForm"]) ? $context["oForm"] : null), "doShowHeader", array(), "method")) {
            // line 3
            echo "        <h2>";
            echo $this->getAttribute((isset($context["oForm"]) ? $context["oForm"] : null), "getTitle", array(), "method");
            echo "</h2>
    ";
        }
        // line 5
        echo "    <form id=\"form_";
        echo (isset($context["formHash"]) ? $context["formHash"] : null);
        echo "\" data-hash=\"";
        echo (isset($context["formHash"]) ? $context["formHash"] : null);
        echo "\" method=\"post\" data-form_name=\"";
        echo $this->getAttribute((isset($context["oForm"]) ? $context["oForm"] : null), "getName", array(), "method");
        echo "\" ";
        if ((isset($context["card"]) ? $context["card"] : null)) {
            echo "action=\"";
            echo (isset($context["section_path"]) ? $context["section_path"] : null);
            echo "checkout/\"";
        } else {
            echo "action=\"";
            echo (isset($context["form_section_path"]) ? $context["form_section_path"] : null);
            echo "response/\"";
        }
        echo " enctype=\"multipart/form-data\"";
        if ((isset($context["ajaxForm"]) ? $context["ajaxForm"] : null)) {
            echo " data-ajaxForm=\"1\"";
        }
        echo " ";
        if ((isset($context["popupResultPage"]) ? $context["popupResultPage"] : null)) {
            echo " data-popup_result_page=\"1\"";
        }
        echo " >
        <div class=\"form__cols-wrap\">
            ";
        // line 8
        echo "                ";
        // line 9
        echo "                ";
        // line 10
        echo "            ";
        if ((isset($context["sError"]) ? $context["sError"] : null)) {
            echo "<div>";
            echo (isset($context["sError"]) ? $context["sError"] : null);
            echo "</div> ";
        }
        // line 11
        echo "            ";
        $context["aFieldList"] = $this->getAttribute((isset($context["oForm"]) ? $context["oForm"] : null), "getFields", array(), "method");
        // line 12
        echo "            ";
        $context['_parent'] = (array) $context;
        $context['_seq'] = twig_ensure_traversable((isset($context["aFieldList"]) ? $context["aFieldList"] : null));
        $context['loop'] = array(
          'parent' => $context['_parent'],
          'index0' => 0,
          'index'  => 1,
          'first'  => true,
        );
        if (is_array($context['_seq']) || (is_object($context['_seq']) && $context['_seq'] instanceof Countable)) {
            $length = count($context['_seq']);
            $context['loop']['revindex0'] = $length - 1;
            $context['loop']['revindex'] = $length;
            $context['loop']['length'] = $length;
            $context['loop']['last'] = 1 === $length;
        }
        foreach ($context['_seq'] as $context["_key"] => $context["oField"]) {
            // line 13
            echo "                ";
            $context["sType"] = $this->getAttribute((isset($context["oField"]) ? $context["oField"] : null), "getType", array(), "method");
            // line 14
            echo "                ";
            $template = $this->env->resolveTemplate(twig_join_filter(array(0 => "/el/", 1 => (isset($context["sType"]) ? $context["sType"] : null), 2 => ".twig")));
            $template->display($context);
            // line 15
            echo "            ";
            ++$context['loop']['index0'];
            ++$context['loop']['index'];
            $context['loop']['first'] = false;
            if (isset($context['loop']['length'])) {
                --$context['loop']['revindex0'];
                --$context['loop']['revindex'];
                $context['loop']['last'] = 0 === $context['loop']['revindex0'];
            }
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['_key'], $context['oField'], $context['_parent'], $context['loop']);
        $context = array_intersect_key($context, $_parent) + $_parent;
        // line 16
        echo "
            ";
        // line 17
        if ($this->getAttribute((isset($context["oForm"]) ? $context["oForm"] : null), "getFormCaptcha", array(), "method")) {
            // line 18
            echo "                <div class=\"form__col-1\">
                    <div class=\"form__item form__item--captha form__item--label-top\">
                        <div class=\"form__label\">";
            // line 20
            echo $this->getAttribute((isset($context["Lang"]) ? $context["Lang"] : null), "get", array(0 => "forms.captcha"), "method");
            echo "</div>
                        <div class=\"form__input form__input--input\">
                            <img alt=\"\" src=\"/ajax/captcha.php?h=";
            // line 22
            echo (isset($context["formHash"]) ? $context["formHash"] : null);
            echo "&v=";
            echo (isset($context["iRandVal"]) ? $context["iRandVal"] : null);
            echo "\" class=\"form__captha-img img_captcha\" />
                            <input data-name=\"";
            // line 23
            echo $this->getAttribute((isset($context["Lang"]) ? $context["Lang"] : null), "get", array(0 => "forms.captcha_title"), "method");
            echo "\" type=\"text\" value=\"\" name=\"captcha\" id=\"captcha\" maxlength=\"4\" />
                        </div>
                    </div>
                </div>
            ";
        }
        // line 28
        echo "
            <!--Согласие на обработку данных-->
            ";
        // line 30
        $context["agreedData"] = $this->getAttribute((isset($context["oForm"]) ? $context["oForm"] : null), "getAgreedData", array(), "method");
        // line 31
        echo "            ";
        if ((isset($context["agreedData"]) ? $context["agreedData"] : null)) {
            // line 32
            echo "                <div class=\"form__col-1\">
                    <div class=\"form__item form__item--checkboxes form__item--agree\">
                        <div class=\"form__input-wrap\">
                            <div class=\"form__input form__input--checkbox\">
                                <input data-name=\"";
            // line 36
            echo $this->getAttribute((isset($context["Lang"]) ? $context["Lang"] : null), "get", array(0 => "forms.personal_data_accept"), "method");
            echo "\" type=\"checkbox\" id=\"check_ok_";
            echo (isset($context["formHash"]) ? $context["formHash"] : null);
            echo "\" name=\"agreed\" value=\"1\" />
                                <div class=\"form__checkbox\">
                                    <label class=\"form__checkbox-trigger fa fa-check\" for=\"check_ok_";
            // line 38
            echo (isset($context["formHash"]) ? $context["formHash"] : null);
            echo "\"></label>
                                </div>
                                <label class=\"form__checkbox-label\" for=\"check_ok_";
            // line 40
            echo (isset($context["formHash"]) ? $context["formHash"] : null);
            echo "\"><a href=\"#agreed_text\" class=\"js-agreed-readmore\">";
            echo $this->getAttribute((isset($context["agreedData"]) ? $context["agreedData"] : null), "agreed_title");
            echo "</a></label>
                            </div>
                        </div>
                    </div>
                    <div style=\"display: none;\" class=\"js-agreed_text\">
                        <div id=\"agreed_text\" class=\"agreed_text\">";
            // line 45
            echo $this->getAttribute((isset($context["agreedData"]) ? $context["agreedData"] : null), "agreed_text");
            echo "</div>
                    </div>
                </div>
            ";
        }
        // line 49
        echo "
            ";
        // line 50
        if ($this->getAttribute((isset($context["oForm"]) ? $context["oForm"] : null), "doShowPhraseRequiredFields", array(), "method")) {
            // line 51
            echo "                <div class=\"form__col-1\">
                    <p class=\"form__info\"><ins class=\"form__mark2\">*</ins> - ";
            // line 52
            echo $this->getAttribute((isset($context["Lang"]) ? $context["Lang"] : null), "get", array(0 => "forms.required"), "method");
            echo "</p>
                </div>
            ";
        }
        // line 55
        echo "
        <div class=\"form__col-1\" id=\"js-error_block_";
        // line 56
        echo (isset($context["formHash"]) ? $context["formHash"] : null);
        echo "\" style=\"display:none\">
            <div class=\"form__errors\">
                <div class=\"form__label\">";
        // line 58
        echo $this->getAttribute((isset($context["Lang"]) ? $context["Lang"] : null), "get", array(0 => "forms.err_empty_fields"), "method");
        echo "</div>
                <ul id=\"js-error_required_";
        // line 59
        echo (isset($context["formHash"]) ? $context["formHash"] : null);
        echo "\">

                </ul>
                <div class=\"form__label\" id=\"js-error_valid_title_";
        // line 62
        echo (isset($context["formHash"]) ? $context["formHash"] : null);
        echo "\">";
        echo $this->getAttribute((isset($context["Lang"]) ? $context["Lang"] : null), "get", array(0 => "forms.err_validation_fields"), "method");
        echo "</div>
                <ul id=\"js-error_valid_";
        // line 63
        echo (isset($context["formHash"]) ? $context["formHash"] : null);
        echo "\">

                </ul>
            </div>
        </div>

            ";
        // line 69
        if ((isset($context["card"]) ? $context["card"] : null)) {
            // line 70
            echo "                <div class=\"form__col-1 form__align_right\">
                    ";
            // line 71
            if (((isset($context["fastBuy"]) ? $context["fastBuy"] : null) != "1")) {
                // line 72
                echo "                        <a href=\"";
                echo (isset($context["section_path"]) ? $context["section_path"] : null);
                echo "\">";
                echo $this->getAttribute((isset($context["Lang"]) ? $context["Lang"] : null), "get", array(0 => "order.back_to_cart"), "method");
                echo "</a>
                    ";
            }
            // line 74
            echo "                    <button type=\"submit\" class=\"b-btnbox b-btnboxfull b-btnboxfull3\">
                        ";
            // line 75
            if (($this->getAttribute((isset($context["Lang"]) ? $context["Lang"] : null), "get", array(0 => $this->getAttribute((isset($context["oForm"]) ? $context["oForm"] : null), "getFormParam", array(0 => "form_button"), "method")), "method") && trim($this->getAttribute((isset($context["Lang"]) ? $context["Lang"] : null), "get", array(0 => $this->getAttribute((isset($context["oForm"]) ? $context["oForm"] : null), "getFormParam", array(0 => "form_button"), "method")), "method")))) {
                // line 76
                echo "                            ";
                echo $this->getAttribute((isset($context["Lang"]) ? $context["Lang"] : null), "get", array(0 => $this->getAttribute((isset($context["oForm"]) ? $context["oForm"] : null), "getFormParam", array(0 => "form_button"), "method")), "method");
                echo "
                        ";
            } else {
                // line 78
                echo "                            ";
                echo $this->getAttribute((isset($context["Lang"]) ? $context["Lang"] : null), "get", array(0 => "order.checkout"), "method");
                echo "
                        ";
            }
            // line 80
            echo "                    </button>
                </div>
            ";
        } else {
            // line 83
            echo "                <div class=\"form__col-1\">
                    <button type=\"submit\" class=\"b-btnbox\">
                        ";
            // line 85
            if (($this->getAttribute((isset($context["Lang"]) ? $context["Lang"] : null), "get", array(0 => $this->getAttribute((isset($context["oForm"]) ? $context["oForm"] : null), "getFormParam", array(0 => "form_button"), "method")), "method") && trim($this->getAttribute((isset($context["Lang"]) ? $context["Lang"] : null), "get", array(0 => $this->getAttribute((isset($context["oForm"]) ? $context["oForm"] : null), "getFormParam", array(0 => "form_button"), "method")), "method")))) {
                // line 86
                echo "                            ";
                echo $this->getAttribute((isset($context["Lang"]) ? $context["Lang"] : null), "get", array(0 => $this->getAttribute((isset($context["oForm"]) ? $context["oForm"] : null), "getFormParam", array(0 => "form_button"), "method")), "method");
                echo "
                        ";
            } else {
                // line 88
                echo "                            ";
                echo $this->getAttribute((isset($context["Lang"]) ? $context["Lang"] : null), "get", array(0 => "forms.send"), "method");
                echo "
                        ";
            }
            // line 90
            echo "                    </button>
                </div>
            ";
        }
        // line 93
        echo "
            <input type=\"hidden\" name=\"cmd\" value='send' />
            <input type=\"hidden\" name=\"form_id\" id=\"form_id\" value=\"";
        // line 95
        echo $this->getAttribute((isset($context["oForm"]) ? $context["oForm"] : null), "getId", array(), "method");
        echo "\" />
            <input type=\"hidden\" name=\"label\" value=\"";
        // line 96
        echo (isset($context["label"]) ? $context["label"] : null);
        echo "\" />
            <input type=\"hidden\" name=\"section\" value=\"";
        // line 97
        echo (isset($context["section"]) ? $context["section"] : null);
        echo "\" />
            <input type=\"hidden\" class=\"_rules\" value='";
        // line 98
        echo $this->getAttribute((isset($context["oForm"]) ? $context["oForm"] : null), "getRules", array(), "method");
        echo "' />
            ";
        // line 99
        if ((isset($context["fastBuy"]) ? $context["fastBuy"] : null)) {
            echo "<input type=\"hidden\" name=\"fastBuy\" value=\"";
            echo (isset($context["fastBuy"]) ? $context["fastBuy"] : null);
            echo "\" /> ";
        }
        // line 100
        echo "            ";
        if ((isset($context["parent"]) ? $context["parent"] : null)) {
            echo "<input type=\"hidden\" name=\"parent\" value=\"";
            echo (isset($context["parent"]) ? $context["parent"] : null);
            echo "\" />";
        }
        // line 101
        echo "            ";
        if ((isset($context["parent_class"]) ? $context["parent_class"] : null)) {
            echo "<input type=\"hidden\" name=\"parent_class\" value=\"";
            echo (isset($context["parent_class"]) ? $context["parent_class"] : null);
            echo "\" />";
        }
        // line 102
        echo "            ";
        echo (isset($context["HiddenCaptchaInput"]) ? $context["HiddenCaptchaInput"] : null);
        echo "
                    ";
        // line 104
        echo "                        ";
        // line 105
        echo "        </div>
    </form>
</div>
";
        // line 108
        $this->env->loadTemplate("reachgoals.twig")->display($context);
    }

    public function getTemplateName()
    {
        return "form.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  346 => 108,  341 => 105,  339 => 104,  334 => 102,  327 => 101,  320 => 100,  314 => 99,  310 => 98,  306 => 97,  302 => 96,  298 => 95,  294 => 93,  289 => 90,  283 => 88,  277 => 86,  275 => 85,  271 => 83,  266 => 80,  260 => 78,  254 => 76,  252 => 75,  249 => 74,  241 => 72,  239 => 71,  236 => 70,  234 => 69,  225 => 63,  219 => 62,  213 => 59,  209 => 58,  204 => 56,  201 => 55,  195 => 52,  192 => 51,  190 => 50,  187 => 49,  180 => 45,  170 => 40,  165 => 38,  158 => 36,  152 => 32,  149 => 31,  147 => 30,  143 => 28,  135 => 23,  129 => 22,  124 => 20,  120 => 18,  118 => 17,  115 => 16,  101 => 15,  97 => 14,  76 => 12,  66 => 10,  64 => 9,  62 => 8,  34 => 5,  28 => 3,  26 => 2,  102 => 24,  99 => 23,  96 => 22,  94 => 13,  87 => 17,  83 => 16,  78 => 14,  73 => 11,  69 => 11,  63 => 8,  59 => 7,  55 => 6,  51 => 5,  47 => 4,  39 => 3,  36 => 2,  19 => 1,);
    }
}
