<?php

/* list2.twig */
class __TwigTemplate_7d21cadab7c9bf1e78e300a5ca6c63c5 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        // line 1
        echo "<div class=\"b-cart\">
    ";
        // line 2
        if (twig_length_filter($this->env, $this->getAttribute((isset($context["order"]) ? $context["order"] : null), "getItems", array(), "method"))) {
            // line 3
            echo "        <div class=\"js_cart_content cart__content\">
            <table class=\"cart__table\">
                <tr>
                    <th>";
            // line 6
            echo $this->getAttribute((isset($context["Lang"]) ? $context["Lang"] : null), "get", array(0 => "order.photo"), "method");
            echo "</th>
                    <th>";
            // line 7
            echo $this->getAttribute((isset($context["Lang"]) ? $context["Lang"] : null), "get", array(0 => "order.title"), "method");
            echo "</th>
                    <th></th>
                </tr>
                ";
            // line 10
            $context['_parent'] = (array) $context;
            $context['_seq'] = twig_ensure_traversable($this->getAttribute((isset($context["order"]) ? $context["order"] : null), "getItems", array(), "method"));
            foreach ($context['_seq'] as $context["_key"] => $context["item"]) {
                // line 11
                echo "                    <tr class=\"cart__row js-cart__row\" data-id=\"";
                echo $this->getAttribute((isset($context["item"]) ? $context["item"] : null), "id_goods");
                echo "\">
                        <td class=\"cart__center\">
                            <span class=\"cabbox__phonebox\">
                            ";
                // line 14
                if ($this->getAttribute($this->getAttribute($this->getAttribute((isset($context["item"]) ? $context["item"] : null), "image", array(), "any", false, true), "mini", array(), "any", false, true), "file", array(), "any", true, true)) {
                    // line 15
                    echo "                                <img src=\"";
                    echo $this->getAttribute($this->getAttribute($this->getAttribute((isset($context["item"]) ? $context["item"] : null), "image"), "mini"), "file");
                    echo "\" alt=\"\"/>
                            ";
                } else {
                    // line 17
                    echo "                                    <img alt=\"\" src=\"/images/lk.photo.png\">
                            ";
                }
                // line 19
                echo "                            </span>
                        </td>
                        <td>
                            <p><a href=\"";
                // line 22
                echo $this->getAttribute((isset($context["item"]) ? $context["item"] : null), "url");
                echo "\"><strong>";
                echo $this->getAttribute((isset($context["item"]) ? $context["item"] : null), "title");
                echo "</strong></a></p>
                            ";
                // line 23
                if (($this->getAttribute((isset($context["item"]) ? $context["item"] : null), "not_available") == 1)) {
                    // line 24
                    echo "                                <div class=\"js_good_is_not_available\">
                                    ";
                    // line 25
                    echo $this->getAttribute((isset($context["Lang"]) ? $context["Lang"] : null), "get", array(0 => "order.good_not_available"), "method");
                    echo "
                                </div>
                            ";
                }
                // line 28
                echo "                            <p>";
                echo $this->getAttribute((isset($context["Lang"]) ? $context["Lang"] : null), "get", array(0 => "order.article"), "method");
                echo ": ";
                echo $this->getAttribute((isset($context["item"]) ? $context["item"] : null), "article");
                echo "</p>
                            <p>";
                // line 29
                echo $this->getAttribute((isset($context["Lang"]) ? $context["Lang"] : null), "get", array(0 => "order.price"), "method");
                echo ", ";
                echo $this->getAttribute((isset($context["Lang"]) ? $context["Lang"] : null), "get", array(0 => "order.current_currency"), "method");
                echo ": <span class=\"item_price\">";
                echo call_user_func_array($this->env->getFilter('price_format')->getCallable(), array($this->getAttribute((isset($context["item"]) ? $context["item"] : null), "price")));
                echo " </span></p>
                            <div class=\"b-catalogbox b-catalogbox-cart\">
                                <div class=\"js_catalogbox_inputbox catalogbox__inputbox\">
                                    <div class=\"catalogbox__minus js_cart_minus\" data-id=\"";
                // line 32
                echo $this->getAttribute((isset($context["item"]) ? $context["item"] : null), "id_goods");
                echo "\"></div>
                                    <input data-id=\"";
                // line 33
                echo $this->getAttribute((isset($context["item"]) ? $context["item"] : null), "id_goods");
                echo "\" class=\"js_cart_amount\" type=\"text\" value=\"";
                echo $this->getAttribute((isset($context["item"]) ? $context["item"] : null), "count");
                echo "\"/>
                                    <div class=\"catalogbox__plus js_cart_plus\" data-id=\"";
                // line 34
                echo $this->getAttribute((isset($context["item"]) ? $context["item"] : null), "id_goods");
                echo "\"></div>
                                </div>
                            </div>
                            <p>";
                // line 37
                echo $this->getAttribute((isset($context["Lang"]) ? $context["Lang"] : null), "get", array(0 => "order.sum"), "method");
                echo ", ";
                echo $this->getAttribute((isset($context["Lang"]) ? $context["Lang"] : null), "get", array(0 => "order.current_currency"), "method");
                echo ": <span class=\"item_total js-item_total\"><strong>";
                echo call_user_func_array($this->env->getFilter('price_format')->getCallable(), array($this->getAttribute((isset($context["item"]) ? $context["item"] : null), "total")));
                echo "</strong></span></p>
                        </td>

                        <td class=\"cart__center cart__remove\">
                            <a data-id=\"";
                // line 41
                echo $this->getAttribute((isset($context["item"]) ? $context["item"] : null), "id_goods");
                echo "\" class=\"js_cart_remove\" href=\"#\">";
                echo $this->getAttribute((isset($context["Lang"]) ? $context["Lang"] : null), "get", array(0 => "order.delete"), "method");
                echo "</a>
                        </td>
                    </tr>
                ";
            }
            $_parent = $context['_parent'];
            unset($context['_seq'], $context['_iterated'], $context['_key'], $context['item'], $context['_parent'], $context['loop']);
            $context = array_intersect_key($context, $_parent) + $_parent;
            // line 45
            echo "                <tr class=\"cart__totalrow\">
                    <td></td>
                    <td class=\"cart__total\" colspan=\"2\"><b>";
            // line 47
            echo $this->getAttribute((isset($context["Lang"]) ? $context["Lang"] : null), "get", array(0 => "order.payable"), "method");
            echo ":</b> <span class=\"js_cart_total\"><span class=\"total\">";
            echo call_user_func_array($this->env->getFilter('price_format')->getCallable(), array($this->getAttribute((isset($context["order"]) ? $context["order"] : null), "getTotalPrice", array(), "method")));
            echo "</span></span></td>
                    
                    
                </tr>
            </table>
            <div class=\"cart__buttons\">
                <div class=\"cart__back\">
                    <a class=\"b-btnbox b-btnboxfull b-btnboxfull4\" href=\"[";
            // line 54
            echo (isset($context["mainSection"]) ? $context["mainSection"] : null);
            echo "]\">";
            echo $this->getAttribute((isset($context["Lang"]) ? $context["Lang"] : null), "get", array(0 => "order.back"), "method");
            echo "</a>
                </div>
                <div class=\"cart__confirm\">
                    <a class=\"b-btnbox b-btnboxfull\" href=\"[";
            // line 57
            echo (isset($context["sectionId"]) ? $context["sectionId"] : null);
            echo "][Cart?action=checkout]\">";
            echo $this->getAttribute((isset($context["Lang"]) ? $context["Lang"] : null), "get", array(0 => "order.checkout"), "method");
            echo "</a>
                </div>
                <div class=\"js_cart_reset cart__reset\">
                    <a class=\"b-btnbox b-btnboxfull b-btnboxfull4\" href=\"#\">";
            // line 60
            echo $this->getAttribute((isset($context["Lang"]) ? $context["Lang"] : null), "get", array(0 => "order.clear"), "method");
            echo "</a>
                </div>
            </div>
        </div>
    ";
        }
        // line 65
        echo "    <div class=\"js_cart_empty ";
        if (twig_length_filter($this->env, $this->getAttribute((isset($context["order"]) ? $context["order"] : null), "getItems", array(), "method"))) {
            echo " cart__empty-hidden";
        }
        echo "\">
        <p>";
        // line 66
        echo $this->getAttribute((isset($context["Lang"]) ? $context["Lang"] : null), "get", array(0 => "order.cart_empty"), "method");
        echo "</p>
    </div>
</div>
<div id=\"js_translate_msg_count_gt_zero\" style=\"display: none;\">
    ";
        // line 70
        echo $this->getAttribute((isset($context["Lang"]) ? $context["Lang"] : null), "get", array(0 => "order.msg_count_gt_zero"), "method");
        echo "
</div>
<div id=\"js_translate_msg_dell_all\" style=\"display: none;\">
    ";
        // line 73
        echo $this->getAttribute((isset($context["Lang"]) ? $context["Lang"] : null), "get", array(0 => "order.msg_dell_all"), "method");
        echo "
</div>
";
    }

    public function getTemplateName()
    {
        return "list2.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  200 => 73,  194 => 70,  187 => 66,  180 => 65,  172 => 60,  164 => 57,  156 => 54,  144 => 47,  140 => 45,  128 => 41,  117 => 37,  111 => 34,  105 => 33,  101 => 32,  91 => 29,  84 => 28,  78 => 25,  75 => 24,  73 => 23,  67 => 22,  62 => 19,  58 => 17,  52 => 15,  50 => 14,  43 => 11,  39 => 10,  33 => 7,  29 => 6,  24 => 3,  22 => 2,  19 => 1,);
    }
}
