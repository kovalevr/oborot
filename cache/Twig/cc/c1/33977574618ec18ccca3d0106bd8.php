<?php

/* robots.twig */
class __TwigTemplate_ccc133977574618ec18ccca3d0106bd8 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        // line 1
        if ((isset($context["domain_exist"]) ? $context["domain_exist"] : null)) {
            // line 2
            echo "User-Agent: *
Disallow: /admin/
Disallow: /search/
Disallow: /profile/
Disallow: /pda/
Disallow: /cart/
Disallow: /auth/
Disallow: /ajax/
Disallow: */?
Disallow: /assets/
";
            // line 12
            if (array_key_exists("pattern", $context)) {
                // line 13
                $context['_parent'] = (array) $context;
                $context['_seq'] = twig_ensure_traversable($this->getAttribute((isset($context["pattern"]) ? $context["pattern"] : null), "disallow"));
                foreach ($context['_seq'] as $context["_key"] => $context["params"]) {
                    // line 14
                    echo "Disallow: ";
                    echo (isset($context["params"]) ? $context["params"] : null);
                    echo "
";
                }
                $_parent = $context['_parent'];
                unset($context['_seq'], $context['_iterated'], $context['_key'], $context['params'], $context['_parent'], $context['loop']);
                $context = array_intersect_key($context, $_parent) + $_parent;
                // line 16
                $context['_parent'] = (array) $context;
                $context['_seq'] = twig_ensure_traversable($this->getAttribute((isset($context["pattern"]) ? $context["pattern"] : null), "allow"));
                foreach ($context['_seq'] as $context["_key"] => $context["params"]) {
                    // line 17
                    echo "Allow: ";
                    echo (isset($context["params"]) ? $context["params"] : null);
                    echo "
";
                }
                $_parent = $context['_parent'];
                unset($context['_seq'], $context['_iterated'], $context['_key'], $context['params'], $context['_parent'], $context['loop']);
                $context = array_intersect_key($context, $_parent) + $_parent;
            }
            // line 20
            echo "
User-agent: GoogleBot
Disallow: /admin/
Disallow: /search/
Disallow: /profile/
Disallow: /pda/
Disallow: /cart/
Disallow: /auth/
Disallow: /ajax/
Disallow: */?
Disallow: /assets/
";
            // line 31
            if (array_key_exists("pattern", $context)) {
                // line 32
                $context['_parent'] = (array) $context;
                $context['_seq'] = twig_ensure_traversable($this->getAttribute((isset($context["pattern"]) ? $context["pattern"] : null), "disallow"));
                foreach ($context['_seq'] as $context["_key"] => $context["params"]) {
                    // line 33
                    echo "Disallow: ";
                    echo (isset($context["params"]) ? $context["params"] : null);
                    echo "
";
                }
                $_parent = $context['_parent'];
                unset($context['_seq'], $context['_iterated'], $context['_key'], $context['params'], $context['_parent'], $context['loop']);
                $context = array_intersect_key($context, $_parent) + $_parent;
                // line 35
                $context['_parent'] = (array) $context;
                $context['_seq'] = twig_ensure_traversable($this->getAttribute((isset($context["pattern"]) ? $context["pattern"] : null), "allow"));
                foreach ($context['_seq'] as $context["_key"] => $context["params"]) {
                    // line 36
                    echo "Allow: ";
                    echo (isset($context["params"]) ? $context["params"] : null);
                    echo "
";
                }
                $_parent = $context['_parent'];
                unset($context['_seq'], $context['_iterated'], $context['_key'], $context['params'], $context['_parent'], $context['loop']);
                $context = array_intersect_key($context, $_parent) + $_parent;
            }
            // line 39
            echo "Allow: /assets/*.js
Allow: /assets/*.css
Allow: *.js
Allow: *.css
Allow: *.png
Allow: *.jpg
Allow: *.jpeg
Allow: *.gif

User-agent: Yandex
Disallow: /admin/
Disallow: /search/
Disallow: /profile/
Disallow: /pda/
Disallow: /cart/
Disallow: /auth/
Disallow: /ajax/
Disallow: */?
Disallow: /assets/
";
            // line 58
            if (array_key_exists("pattern", $context)) {
                // line 59
                $context['_parent'] = (array) $context;
                $context['_seq'] = twig_ensure_traversable($this->getAttribute((isset($context["pattern"]) ? $context["pattern"] : null), "disallow"));
                foreach ($context['_seq'] as $context["_key"] => $context["params"]) {
                    // line 60
                    echo "Disallow: ";
                    echo (isset($context["params"]) ? $context["params"] : null);
                    echo "
";
                }
                $_parent = $context['_parent'];
                unset($context['_seq'], $context['_iterated'], $context['_key'], $context['params'], $context['_parent'], $context['loop']);
                $context = array_intersect_key($context, $_parent) + $_parent;
                // line 62
                $context['_parent'] = (array) $context;
                $context['_seq'] = twig_ensure_traversable($this->getAttribute((isset($context["pattern"]) ? $context["pattern"] : null), "allow"));
                foreach ($context['_seq'] as $context["_key"] => $context["params"]) {
                    // line 63
                    echo "Allow: ";
                    echo (isset($context["params"]) ? $context["params"] : null);
                    echo "
";
                }
                $_parent = $context['_parent'];
                unset($context['_seq'], $context['_iterated'], $context['_key'], $context['params'], $context['_parent'], $context['loop']);
                $context = array_intersect_key($context, $_parent) + $_parent;
            }
            // line 66
            echo "Allow: /assets/*.js
Allow: /assets/*.css
Allow: *.js
Allow: *.css
Allow: *.png
Allow: *.jpg
Allow: *.jpeg
Allow: *.gif
Clean-Param: utm_source&utm_medium&utm_campaign

";
            // line 76
            if ((isset($context["system_service"]) ? $context["system_service"] : null)) {
                // line 77
                $context['_parent'] = (array) $context;
                $context['_seq'] = twig_ensure_traversable((isset($context["system_service"]) ? $context["system_service"] : null));
                foreach ($context['_seq'] as $context["_key"] => $context["item"]) {
                    // line 78
                    echo "Disallow: ";
                    echo (isset($context["item"]) ? $context["item"] : null);
                    echo "
";
                }
                $_parent = $context['_parent'];
                unset($context['_seq'], $context['_iterated'], $context['_key'], $context['item'], $context['_parent'], $context['loop']);
                $context = array_intersect_key($context, $_parent) + $_parent;
            }
            // line 81
            if (array_key_exists("pattern", $context)) {
                // line 82
                if ((!(isset($context["bOnlyRules"]) ? $context["bOnlyRules"] : null))) {
                    // line 83
                    $this->env->loadTemplate("robots_host.twig")->display($context);
                }
            }
        } else {
            // line 87
            echo "User-Agent: *
Disallow: /
";
        }
    }

    public function getTemplateName()
    {
        return "robots.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  191 => 87,  186 => 83,  184 => 82,  182 => 81,  172 => 78,  168 => 77,  166 => 76,  154 => 66,  144 => 63,  140 => 62,  131 => 60,  127 => 59,  125 => 58,  104 => 39,  94 => 36,  90 => 35,  81 => 33,  77 => 32,  75 => 31,  62 => 20,  52 => 17,  48 => 16,  39 => 14,  35 => 13,  33 => 12,  21 => 2,  19 => 1,);
    }
}
