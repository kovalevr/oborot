<?php

/* view.twig */
class __TwigTemplate_4b597ecda9175de2e4da487fef20902f extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        // line 1
        if (((isset($context["revert"]) ? $context["revert"] : null) == 0)) {
            // line 2
            echo "
    ";
            // line 3
            if (array_key_exists("oForm", $context)) {
                // line 4
                echo "        ";
                $this->env->loadTemplate("form.twig")->display($context);
                // line 5
                echo "    ";
            }
            // line 6
            echo "
    ";
            // line 7
            if (array_key_exists("back_link", $context)) {
                // line 8
                echo "        ";
                if (array_key_exists("msg", $context)) {
                    // line 9
                    echo "            <div class=\"b-msgbox\">
                ";
                    // line 10
                    echo (isset($context["msg"]) ? $context["msg"] : null);
                    echo "
            </div>
        ";
                }
                // line 13
                echo "        <p>
            <a href=\"\">";
                // line 14
                echo $this->getAttribute((isset($context["Lang"]) ? $context["Lang"] : null), "get", array(0 => "page.back"), "method");
                echo "</a>
        </p>
    ";
            }
        }
        // line 18
        echo "
<!-- Вывод с фото -->
<div class=\"b-guestbox b-guestbox_photo\"";
        // line 20
        echo $this->getAttribute((isset($context["Design"]) ? $context["Design"] : null), "write", array(0 => " sktag=\"modules.guestbox\""), "method");
        echo ">
    <div class=\"guestbox__msgtext\"></div>
    ";
        // line 22
        $context['_parent'] = (array) $context;
        $context['_seq'] = twig_ensure_traversable((isset($context["items"]) ? $context["items"] : null));
        foreach ($context['_seq'] as $context["_key"] => $context["aItem"]) {
            // line 23
            echo "    <div class=\"guestbox__item\">
        ";
            // line 24
            if ((isset($context["showGallery"]) ? $context["showGallery"] : null)) {
                // line 25
                echo "            <div class=\"guestbox__photo\">
                ";
                // line 26
                if (($this->getAttribute((isset($context["aItem"]) ? $context["aItem"] : null), "photo_gallery") && $this->getAttribute($this->getAttribute((isset($context["aItem"]) ? $context["aItem"] : null), "photo_gallery"), "detail"))) {
                    // line 27
                    echo "                    <img src=\"";
                    echo $this->getAttribute($this->getAttribute($this->getAttribute((isset($context["aItem"]) ? $context["aItem"] : null), "photo_gallery"), "detail"), "file");
                    echo "\" alt=\"\">
                ";
                } else {
                    // line 29
                    echo "                    <img src=\"/images/categ.nophoto2.png\" alt=\"\">
                ";
                }
                // line 31
                echo "            </div>
        ";
            }
            // line 33
            echo "        <div class=\"guestbox__leftside\">
            ";
            // line 34
            if ($this->getAttribute((isset($context["aItem"]) ? $context["aItem"] : null), "name")) {
                // line 35
                echo "                <div class=\"guestbox__title\">";
                echo $this->getAttribute((isset($context["aItem"]) ? $context["aItem"] : null), "name");
                echo " </div>
            ";
            }
            // line 37
            echo "            ";
            if ($this->getAttribute((isset($context["aItem"]) ? $context["aItem"] : null), "city")) {
                // line 38
                echo "                <div class=\"guestbox__city\">";
                echo $this->getAttribute((isset($context["aItem"]) ? $context["aItem"] : null), "city");
                echo " </div>
            ";
            }
            // line 40
            echo "            ";
            if ($this->getAttribute((isset($context["aItem"]) ? $context["aItem"] : null), "company")) {
                // line 41
                echo "                <div class=\"guestbox__city\">";
                echo $this->getAttribute((isset($context["aItem"]) ? $context["aItem"] : null), "company");
                echo " </div>
            ";
            }
            // line 43
            echo "
            ";
            // line 44
            if (((isset($context["show_rating"]) ? $context["show_rating"] : null) && $this->getAttribute((isset($context["aItem"]) ? $context["aItem"] : null), "rating"))) {
                // line 45
                echo "                ";
                if (($this->getAttribute((isset($context["aItem"]) ? $context["aItem"] : null), "rating") > 5)) {
                    // line 46
                    echo "                    ";
                    $context["rating"] = 5;
                    // line 47
                    echo "                ";
                } else {
                    // line 48
                    echo "                    ";
                    $context["rating"] = $this->getAttribute((isset($context["aItem"]) ? $context["aItem"] : null), "rating");
                    // line 49
                    echo "                ";
                }
                // line 50
                echo "                ";
                $context["ext"] = (5 - (isset($context["rating"]) ? $context["rating"] : null));
                // line 51
                echo "                <div class=\"b-ratbox\">
                    ";
                // line 52
                if (((isset($context["rating"]) ? $context["rating"] : null) > 0)) {
                    // line 53
                    echo "                        ";
                    $context['_parent'] = (array) $context;
                    $context['_seq'] = twig_ensure_traversable(range(1, (isset($context["rating"]) ? $context["rating"] : null)));
                    foreach ($context['_seq'] as $context["_key"] => $context["i"]) {
                        // line 54
                        echo "                            <div class=\"ratbox__item ratbox__itemon\"></div>
                        ";
                    }
                    $_parent = $context['_parent'];
                    unset($context['_seq'], $context['_iterated'], $context['_key'], $context['i'], $context['_parent'], $context['loop']);
                    $context = array_intersect_key($context, $_parent) + $_parent;
                    // line 56
                    echo "                    ";
                }
                // line 57
                echo "                    ";
                if (((isset($context["ext"]) ? $context["ext"] : null) > 0)) {
                    // line 58
                    echo "                        ";
                    $context['_parent'] = (array) $context;
                    $context['_seq'] = twig_ensure_traversable(range(1, (isset($context["ext"]) ? $context["ext"] : null)));
                    foreach ($context['_seq'] as $context["_key"] => $context["i"]) {
                        // line 59
                        echo "                            <div class=\"ratbox__item\"></div>
                        ";
                    }
                    $_parent = $context['_parent'];
                    unset($context['_seq'], $context['_iterated'], $context['_key'], $context['i'], $context['_parent'], $context['loop']);
                    $context = array_intersect_key($context, $_parent) + $_parent;
                    // line 61
                    echo "                    ";
                }
                // line 62
                echo "                    ";
                // line 63
                echo "                </div>
            ";
            }
            // line 65
            echo "        </div>
        <a name=\"";
            // line 66
            echo $this->getAttribute((isset($context["aItem"]) ? $context["aItem"] : null), "id");
            echo "\"></a>
        <div class=\"guestbox__rightside\">
            <div class=\"b-editor\">
            ";
            // line 69
            echo $this->getAttribute((isset($context["aItem"]) ? $context["aItem"] : null), "content");
            echo "
            </div>
            ";
            // line 71
            if ($this->getAttribute((isset($context["aItem"]) ? $context["aItem"] : null), "date_time")) {
                // line 72
                echo "                <div class=\"guestbox__date\">";
                echo $this->getAttribute((isset($context["aItem"]) ? $context["aItem"] : null), "date_time");
                echo " </div>
            ";
            }
            // line 74
            echo "        </div>
    </div>
    ";
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['_key'], $context['aItem'], $context['_parent'], $context['loop']);
        $context = array_intersect_key($context, $_parent) + $_parent;
        // line 77
        echo "</div>

";
        // line 79
        if ((!(isset($context["bIsReview4Good"]) ? $context["bIsReview4Good"] : null))) {
            // line 80
            echo "    ";
            $this->env->loadTemplate("MicroDataReviews.twig")->display($context);
            // line 81
            echo "    ";
            $this->env->loadTemplate("paginator.twig")->display($context);
        } else {
            // line 83
            echo "    ";
            $this->env->loadTemplate("paginator_tabs.twig")->display($context);
        }
        // line 85
        echo "
";
        // line 86
        if (((isset($context["revert"]) ? $context["revert"] : null) == 1)) {
            // line 87
            echo "
    ";
            // line 88
            if (array_key_exists("oForm", $context)) {
                // line 89
                echo "        ";
                $this->env->loadTemplate("form.twig")->display($context);
                // line 90
                echo "    ";
            }
            // line 91
            echo "
    ";
            // line 92
            if (array_key_exists("back_link", $context)) {
                // line 93
                echo "        ";
                if (array_key_exists("msg", $context)) {
                    // line 94
                    echo "            <div class=\"b-msgbox\">
                ";
                    // line 95
                    echo (isset($context["msg"]) ? $context["msg"] : null);
                    echo "
            </div>
        ";
                }
                // line 98
                echo "        <p>
            <a href=\"\">";
                // line 99
                echo $this->getAttribute((isset($context["Lang"]) ? $context["Lang"] : null), "get", array(0 => "page.back"), "method");
                echo "</a>
        </p>
    ";
            }
        }
    }

    public function getTemplateName()
    {
        return "view.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  272 => 98,  263 => 94,  258 => 92,  255 => 91,  247 => 88,  244 => 87,  242 => 86,  235 => 83,  231 => 81,  228 => 80,  226 => 79,  222 => 77,  214 => 74,  208 => 72,  206 => 71,  188 => 63,  186 => 62,  183 => 61,  176 => 59,  171 => 58,  168 => 57,  153 => 53,  151 => 52,  148 => 51,  145 => 50,  142 => 49,  139 => 48,  136 => 47,  133 => 46,  130 => 45,  128 => 44,  125 => 43,  119 => 41,  116 => 40,  110 => 38,  107 => 37,  92 => 31,  88 => 29,  82 => 27,  77 => 25,  72 => 23,  68 => 22,  52 => 14,  49 => 13,  43 => 10,  40 => 9,  37 => 8,  35 => 7,  32 => 6,  29 => 5,  21 => 2,  80 => 26,  71 => 13,  104 => 20,  98 => 19,  95 => 18,  86 => 16,  84 => 15,  60 => 10,  85 => 15,  75 => 24,  48 => 9,  45 => 8,  42 => 7,  33 => 5,  30 => 4,  24 => 3,  346 => 108,  341 => 105,  339 => 104,  334 => 102,  327 => 101,  320 => 100,  314 => 99,  310 => 98,  306 => 97,  302 => 96,  298 => 95,  294 => 93,  289 => 90,  283 => 88,  277 => 86,  275 => 99,  271 => 83,  266 => 95,  260 => 93,  254 => 76,  252 => 90,  249 => 89,  241 => 72,  239 => 85,  236 => 70,  234 => 69,  225 => 63,  219 => 62,  213 => 59,  209 => 58,  204 => 56,  201 => 69,  195 => 66,  192 => 65,  190 => 50,  187 => 49,  180 => 45,  170 => 40,  165 => 56,  158 => 54,  152 => 32,  149 => 31,  147 => 30,  143 => 28,  135 => 23,  129 => 22,  124 => 20,  120 => 18,  118 => 17,  115 => 16,  101 => 35,  97 => 14,  76 => 13,  66 => 10,  64 => 11,  62 => 10,  34 => 5,  28 => 3,  26 => 4,  102 => 24,  99 => 34,  96 => 33,  94 => 17,  87 => 16,  83 => 14,  78 => 14,  73 => 12,  69 => 12,  63 => 20,  59 => 18,  55 => 6,  51 => 5,  47 => 4,  39 => 3,  36 => 2,  19 => 1,);
    }
}
