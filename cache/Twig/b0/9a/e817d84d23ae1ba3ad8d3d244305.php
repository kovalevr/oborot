<?php

/* OnMain.gallery.twig */
class __TwigTemplate_b09ae817d84d23ae1ba3ad8d3d244305 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        // line 1
        if (array_key_exists("aObjectList", $context)) {
            // line 2
            echo "    ";
            if ((!twig_test_empty((isset($context["aObjectList"]) ? $context["aObjectList"] : null)))) {
                // line 3
                echo "        ";
                if (((isset($context["titleOnMain"]) ? $context["titleOnMain"] : null) || $this->getAttribute((isset($context["Design"]) ? $context["Design"] : null), "modeIsActive", array(), "method"))) {
                    // line 4
                    echo "            <h2";
                    if ($this->getAttribute((isset($context["Design"]) ? $context["Design"] : null), "modeIsActive", array(), "method")) {
                        echo " sktag=\"editor.h2\" skeditor=\"";
                        echo (isset($context["moduleGroup"]) ? $context["moduleGroup"] : null);
                        echo "/titleOnMain\"";
                    }
                    echo ">";
                    echo (isset($context["titleOnMain"]) ? $context["titleOnMain"] : null);
                    echo "</h2>
        ";
                }
                // line 6
                echo "        <div class=\"b-catalogbox b-catalogbox-gal";
                echo $this->getAttribute((isset($context["Adaptive"]) ? $context["Adaptive"] : null), "write", array(0 => " b-catalogbox-carousel"), "method");
                if (((isset($context["show_shadows"]) ? $context["show_shadows"] : null) == "enabled")) {
                    echo " b-catalogbox-boxshadow";
                }
                echo " js_goods_container\">
            <div class=\"js-catalogbox_gal_onmain\">
                ";
                // line 8
                $context['_parent'] = (array) $context;
                $context['_seq'] = twig_ensure_traversable((isset($context["aObjectList"]) ? $context["aObjectList"] : null));
                foreach ($context['_seq'] as $context["iKey"] => $context["aObject"]) {
                    // line 9
                    echo "                    ";
                    if ($this->getAttribute((isset($context["aObject"]) ? $context["aObject"] : null), "show_detail", array(), "any", true, true)) {
                        // line 10
                        echo "                        ";
                        $context["show_detail"] = $this->getAttribute((isset($context["aObject"]) ? $context["aObject"] : null), "show_detail");
                        // line 11
                        echo "                    ";
                    }
                    // line 12
                    echo "                    <div class=\"catalogbox__item js_ecommerce_viewlist js_catalogbox_item\" data-ecommerce='";
                    echo $this->getAttribute((isset($context["aObject"]) ? $context["aObject"] : null), "ecommerce");
                    echo "' >
                        <div class=\"catalogbox__imgbox\"><div class=\"catalogbox__img\">";
                    // line 13
                    if ($this->getAttribute($this->getAttribute($this->getAttribute($this->getAttribute($this->getAttribute($this->getAttribute((isset($context["aObject"]) ? $context["aObject"] : null), "fields", array(), "any", false, true), "gallery", array(), "any", false, true), "first_img", array(), "any", false, true), "images_data", array(), "any", false, true), "small", array(), "array", false, true), "file", array(), "any", true, true)) {
                        echo "<img alt=\"";
                        echo twig_escape_filter($this->env, $this->getAttribute($this->getAttribute($this->getAttribute($this->getAttribute((isset($context["aObject"]) ? $context["aObject"] : null), "fields"), "gallery"), "first_img"), "alt_title"));
                        echo "\" src=\"";
                        echo $this->getAttribute($this->getAttribute($this->getAttribute($this->getAttribute($this->getAttribute($this->getAttribute((isset($context["aObject"]) ? $context["aObject"] : null), "fields"), "gallery"), "first_img"), "images_data"), "small", array(), "array"), "file");
                        echo "\">";
                    } else {
                        echo "<img alt=\"\" src=\"";
                        echo $this->getAttribute((isset($context["Design"]) ? $context["Design"] : null), "get", array(0 => "modules.catalogbox.imgbox", 1 => "nophoto_img"), "method");
                        echo "\">";
                    }
                    echo "</div>
                            ";
                    // line 14
                    if ((isset($context["show_detail"]) ? $context["show_detail"] : null)) {
                        echo "<a href=\"";
                        echo $this->getAttribute((isset($context["aObject"]) ? $context["aObject"] : null), "url");
                        echo "\" class=\"catalogbox__imglink js_ecommerce_link\" title=\"";
                        echo twig_escape_filter($this->env, $this->getAttribute($this->getAttribute($this->getAttribute($this->getAttribute((isset($context["aObject"]) ? $context["aObject"] : null), "fields"), "gallery"), "first_img"), "title"));
                        if ($this->getAttribute($this->getAttribute($this->getAttribute($this->getAttribute((isset($context["aObject"]) ? $context["aObject"] : null), "fields"), "gallery"), "first_img"), "description")) {
                            echo " ";
                            echo twig_escape_filter($this->env, $this->getAttribute($this->getAttribute($this->getAttribute($this->getAttribute((isset($context["aObject"]) ? $context["aObject"] : null), "fields"), "gallery"), "first_img"), "description"));
                        }
                        echo "\">";
                    }
                    if ((isset($context["show_detail"]) ? $context["show_detail"] : null)) {
                        echo "</a>";
                    }
                    // line 15
                    echo "                            <div class=\"catalogbox__helper\"></div>
                            <div class=\"catalogbox__salebox\">
                                ";
                    // line 17
                    if ($this->getAttribute($this->getAttribute($this->getAttribute((isset($context["aObject"]) ? $context["aObject"] : null), "fields"), "discount"), "value")) {
                        // line 18
                        echo "                                <div class=\"catalogbox__saleitem\"><img alt=\"\" src=\"";
                        echo $this->getAttribute((isset($context["Design"]) ? $context["Design"] : null), "get", array(0 => "modules.catalogbox.btnbox.salebox", 1 => "image_sale"), "method");
                        echo "\"></div>
                                ";
                    }
                    // line 20
                    echo "                                ";
                    if ($this->getAttribute($this->getAttribute($this->getAttribute((isset($context["aObject"]) ? $context["aObject"] : null), "fields"), "hit"), "value")) {
                        // line 21
                        echo "                                <div class=\"catalogbox__saleitem\"><img alt=\"\" src=\"";
                        echo $this->getAttribute((isset($context["Design"]) ? $context["Design"] : null), "get", array(0 => "modules.catalogbox.btnbox.salebox", 1 => "image_hit"), "method");
                        echo "\"></div>
                                ";
                    }
                    // line 23
                    echo "                                ";
                    if ($this->getAttribute($this->getAttribute($this->getAttribute((isset($context["aObject"]) ? $context["aObject"] : null), "fields"), "new"), "value")) {
                        // line 24
                        echo "                                <div class=\"catalogbox__saleitem\"><img alt=\"\" src=\"";
                        echo $this->getAttribute((isset($context["Design"]) ? $context["Design"] : null), "get", array(0 => "modules.catalogbox.btnbox.salebox", 1 => "image_new"), "method");
                        echo "\"></div>
                                ";
                    }
                    // line 26
                    echo "                            </div>
                        </div>
                        <div class=\"catalogbox__content\">
                            <p class=\"catalogbox__title\">";
                    // line 29
                    if ((isset($context["show_detail"]) ? $context["show_detail"] : null)) {
                        echo "<a class=\"js_ecommerce_link\" href=\"";
                        echo $this->getAttribute((isset($context["aObject"]) ? $context["aObject"] : null), "url");
                        echo "\">";
                    }
                    echo $this->getAttribute((isset($context["aObject"]) ? $context["aObject"] : null), "title");
                    if ((isset($context["show_detail"]) ? $context["show_detail"] : null)) {
                        echo "</a>";
                    }
                    echo "</p>
                            ";
                    // line 30
                    echo $this->getAttribute($this->getAttribute((isset($context["aObject"]) ? $context["aObject"] : null), "Rating"), "html");
                    echo "
                            ";
                    // line 31
                    if ($this->getAttribute($this->getAttribute($this->getAttribute((isset($context["aObject"]) ? $context["aObject"] : null), "fields"), "article"), "value")) {
                        echo "<p class=\"catalogbox__artical\">";
                        if (($this->getAttribute($this->getAttribute($this->getAttribute($this->getAttribute((isset($context["aObject"]) ? $context["aObject"] : null), "fields"), "article"), "attrs"), "show_title") == 1)) {
                            echo $this->getAttribute($this->getAttribute($this->getAttribute((isset($context["aObject"]) ? $context["aObject"] : null), "fields"), "article"), "title");
                        }
                        echo " ";
                        echo $this->getAttribute($this->getAttribute($this->getAttribute((isset($context["aObject"]) ? $context["aObject"] : null), "fields"), "article"), "html");
                        echo "</p>";
                    }
                    // line 32
                    echo "                            ";
                    if ($this->getAttribute($this->getAttribute($this->getAttribute((isset($context["aObject"]) ? $context["aObject"] : null), "fields"), "announce_HOLD"), "value")) {
                        echo "<div class=\"catalogbox__param__";
                        echo $this->getAttribute($this->getAttribute($this->getAttribute((isset($context["aObject"]) ? $context["aObject"] : null), "fields"), "announce_HOLD"), "name");
                        echo "\">";
                        echo $this->getAttribute($this->getAttribute($this->getAttribute((isset($context["aObject"]) ? $context["aObject"] : null), "fields"), "announce"), "value");
                        echo "</div>";
                    }
                    // line 33
                    echo "                            ";
                    $context['_parent'] = (array) $context;
                    $context['_seq'] = twig_ensure_traversable($this->getAttribute((isset($context["aObject"]) ? $context["aObject"] : null), "fields"));
                    foreach ($context['_seq'] as $context["_key"] => $context["field"]) {
                        // line 34
                        echo "                                ";
                        if ((($this->getAttribute($this->getAttribute((isset($context["field"]) ? $context["field"] : null), "attrs"), "show_in_params") && $this->getAttribute((isset($context["field"]) ? $context["field"] : null), "value")) && $this->getAttribute((isset($context["field"]) ? $context["field"] : null), "html"))) {
                            // line 35
                            echo "                                    <div class=\"catalogbox__param catalogbox__param__";
                            echo $this->getAttribute((isset($context["field"]) ? $context["field"] : null), "name");
                            echo "\">";
                            if (($this->getAttribute($this->getAttribute((isset($context["field"]) ? $context["field"] : null), "attrs"), "show_title") == 1)) {
                                echo "<span>";
                                echo $this->getAttribute((isset($context["field"]) ? $context["field"] : null), "title");
                                echo ":</span>";
                            }
                            // line 36
                            echo "                                        ";
                            if ($this->getAttribute((isset($context["field"]) ? $context["field"] : null), "htmlAlt")) {
                                echo $this->getAttribute((isset($context["field"]) ? $context["field"] : null), "htmlAlt");
                                echo " ";
                            } else {
                                echo $this->getAttribute((isset($context["field"]) ? $context["field"] : null), "html");
                            }
                            // line 37
                            echo "                                    </div>
                                ";
                        }
                        // line 39
                        echo "                            ";
                    }
                    $_parent = $context['_parent'];
                    unset($context['_seq'], $context['_iterated'], $context['_key'], $context['field'], $context['_parent'], $context['loop']);
                    $context = array_intersect_key($context, $_parent) + $_parent;
                    // line 40
                    echo "
                            ";
                    // line 41
                    if ($this->getAttribute($this->getAttribute($this->getAttribute((isset($context["aObject"]) ? $context["aObject"] : null), "fields"), "old_price"), "value")) {
                        echo "<p class=\"catalogbox__oldprice\"><span>";
                        echo call_user_func_array($this->env->getFilter('price_format')->getCallable(), array($this->getAttribute($this->getAttribute($this->getAttribute((isset($context["aObject"]) ? $context["aObject"] : null), "fields"), "old_price"), "value")));
                        echo "
                                    ";
                        // line 42
                        echo $this->getAttribute($this->getAttribute($this->getAttribute((isset($context["aObject"]) ? $context["aObject"] : null), "fields"), "old_price"), "measure");
                        if ($this->getAttribute($this->getAttribute($this->getAttribute((isset($context["aObject"]) ? $context["aObject"] : null), "fields"), "measure"), "html")) {
                            echo "/";
                            echo $this->getAttribute($this->getAttribute($this->getAttribute((isset($context["aObject"]) ? $context["aObject"] : null), "fields"), "measure"), "html");
                        }
                        // line 43
                        echo "                            </span></p>";
                    }
                    // line 44
                    echo "                        </div>
                        <div class=\"catalogbox__shcar\">
                            <div class=\"catalogbox__pricebox\">
                                ";
                    // line 47
                    if ($this->getAttribute($this->getAttribute($this->getAttribute((isset($context["aObject"]) ? $context["aObject"] : null), "fields"), "price"), "value")) {
                        // line 48
                        echo "                                    <span class=\"catalogbox__price\">
                                        <span>";
                        // line 49
                        echo call_user_func_array($this->env->getFilter('price_format')->getCallable(), array($this->getAttribute($this->getAttribute($this->getAttribute((isset($context["aObject"]) ? $context["aObject"] : null), "fields"), "price"), "value")));
                        echo "</span>
                                        ";
                        // line 50
                        echo $this->getAttribute($this->getAttribute($this->getAttribute((isset($context["aObject"]) ? $context["aObject"] : null), "fields"), "price"), "measure");
                        if ($this->getAttribute($this->getAttribute($this->getAttribute((isset($context["aObject"]) ? $context["aObject"] : null), "fields"), "measure"), "html")) {
                            echo "/";
                            echo $this->getAttribute($this->getAttribute($this->getAttribute((isset($context["aObject"]) ? $context["aObject"] : null), "fields"), "measure"), "html");
                        }
                        // line 51
                        echo "                                    </span>
                                ";
                    }
                    // line 53
                    echo "                            </div>
                            <div class=\"catalogbox__btnbox\">
                                ";
                    // line 55
                    if ($this->getAttribute($this->getAttribute($this->getAttribute((isset($context["aObject"]) ? $context["aObject"] : null), "fields"), "countbuy"), "value")) {
                        // line 56
                        echo "                                    <div class=\"js_catalogbox_inputbox catalogbox__inputbox\"><div class=\"catalogbox__minus js_catalogbox_minus\">-</div><input class=\"js_count\" type=\"text\" data-id=\"";
                        echo $this->getAttribute((isset($context["aObject"]) ? $context["aObject"] : null), "id");
                        echo "\" value=\"1\"><div class=\"catalogbox__plus js_catalogbox_plus\">+</div></div>
                                ";
                    }
                    // line 58
                    echo "                            </div>
                            <div class=\"g-clear\"></div>
                            ";
                    // line 60
                    if ($this->getAttribute($this->getAttribute($this->getAttribute((isset($context["aObject"]) ? $context["aObject"] : null), "fields"), "fastbuy"), "value")) {
                        // line 61
                        echo "                                <div class=\"catalogbox__buynow\"><a href=\"#\" class=\"js-callback\" data-ajaxform=\"1\" data-js_max_width=\"600\" data-width-type=\"px\" data-idobj=\"";
                        echo $this->getAttribute((isset($context["aObject"]) ? $context["aObject"] : null), "id");
                        echo "\" data-module=\"Cart\" data-cmd=\"checkout\">";
                        if (($this->getAttribute($this->getAttribute($this->getAttribute($this->getAttribute((isset($context["aObject"]) ? $context["aObject"] : null), "fields"), "fastbuy"), "attrs"), "show_title") == 1)) {
                            echo $this->getAttribute($this->getAttribute($this->getAttribute((isset($context["aObject"]) ? $context["aObject"] : null), "fields"), "fastbuy"), "title");
                        }
                        echo "</a></div>
                            ";
                    }
                    // line 63
                    echo "                            ";
                    if ($this->getAttribute($this->getAttribute($this->getAttribute((isset($context["aObject"]) ? $context["aObject"] : null), "fields"), "buy"), "value")) {
                        // line 64
                        echo "                                <div class=\"catalogbox__btn\"><a data-id=\"";
                        echo $this->getAttribute((isset($context["aObject"]) ? $context["aObject"] : null), "id");
                        echo "\" ";
                        if ((isset($context["useCart"]) ? $context["useCart"] : null)) {
                            // line 65
                            echo "onclick=\"return false;\"";
                        }
                        echo " href=\"";
                        if ((!(isset($context["useCart"]) ? $context["useCart"] : null))) {
                            echo "[";
                            echo (isset($context["form_section"]) ? $context["form_section"] : null);
                            echo "][From?objectId=";
                            echo $this->getAttribute((isset($context["aObject"]) ? $context["aObject"] : null), "id");
                            echo "]";
                        } else {
                            echo "#tocart";
                        }
                        echo "\" class=\"js-btnBuy btnBuy\">";
                        echo $this->getAttribute((isset($context["Lang"]) ? $context["Lang"] : null), "get", array(0 => $this->getAttribute($this->getAttribute($this->getAttribute((isset($context["aObject"]) ? $context["aObject"] : null), "fields"), "buy"), "title")), "method");
                        echo "</a></div>
                            ";
                    }
                    // line 67
                    echo "                        </div>
                    </div>
                ";
                }
                $_parent = $context['_parent'];
                unset($context['_seq'], $context['_iterated'], $context['iKey'], $context['aObject'], $context['_parent'], $context['loop']);
                $context = array_intersect_key($context, $_parent) + $_parent;
                // line 70
                echo "            </div>
        </div>
";
            }
        }
        // line 74
        echo "

<div class=\"b-block\">
    <div class=\"b-block__item\"></div>
</div>
";
    }

    public function getTemplateName()
    {
        return "OnMain.gallery.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  310 => 74,  304 => 70,  296 => 67,  278 => 65,  273 => 64,  270 => 63,  260 => 61,  258 => 60,  254 => 58,  248 => 56,  246 => 55,  242 => 53,  238 => 51,  232 => 50,  228 => 49,  225 => 48,  218 => 44,  209 => 42,  203 => 41,  200 => 40,  194 => 39,  190 => 37,  182 => 36,  173 => 35,  165 => 33,  146 => 31,  142 => 30,  130 => 29,  125 => 26,  119 => 24,  116 => 23,  110 => 21,  107 => 20,  101 => 18,  99 => 17,  95 => 15,  80 => 14,  66 => 13,  58 => 11,  55 => 10,  52 => 9,  48 => 8,  27 => 4,  24 => 3,  21 => 2,  237 => 49,  223 => 47,  219 => 46,  215 => 43,  201 => 43,  197 => 41,  193 => 39,  179 => 38,  170 => 34,  159 => 32,  156 => 32,  139 => 30,  136 => 29,  134 => 28,  128 => 25,  124 => 24,  109 => 21,  106 => 20,  89 => 19,  86 => 18,  84 => 17,  77 => 14,  73 => 12,  67 => 10,  61 => 12,  43 => 5,  40 => 4,  23 => 3,  65 => 9,  53 => 9,  41 => 7,  39 => 6,  36 => 5,  32 => 3,  30 => 2,  19 => 1,);
    }
}
