<?php

/* list_on_main.twig */
class __TwigTemplate_30026a6f115e8c53f4ce7ccb55662775 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        // line 1
        if (array_key_exists("aArticlesList", $context)) {
            // line 2
            echo "    <div class=\"b-article b-article--list\"";
            echo $this->getAttribute((isset($context["Design"]) ? $context["Design"] : null), "write", array(0 => " sktag=\"modules.news\""), "method");
            echo ">
        ";
            // line 3
            if (((isset($context["titleOnMain"]) ? $context["titleOnMain"] : null) || $this->getAttribute((isset($context["Design"]) ? $context["Design"] : null), "modeIsActive", array(), "method"))) {
                // line 4
                echo "            <div class=\"article__alltitle\" ";
                echo $this->getAttribute((isset($context["Design"]) ? $context["Design"] : null), "write", array(0 => " sktag=\"editor.h2\" skeditor=\"articles/titleOnMain\""), "method");
                echo ">";
                echo (isset($context["titleOnMain"]) ? $context["titleOnMain"] : null);
                echo "</div>
        ";
            }
            // line 6
            echo "        ";
            $context['_parent'] = (array) $context;
            $context['_seq'] = twig_ensure_traversable((isset($context["aArticlesList"]) ? $context["aArticlesList"] : null));
            foreach ($context['_seq'] as $context["iKey"] => $context["aArticles"]) {
                // line 7
                echo "            <div class=\"article__item\">
                <div class=\"article__title\">
                    ";
                // line 9
                if (($this->getAttribute((isset($context["aArticles"]) ? $context["aArticles"] : null), "full_text") || $this->getAttribute((isset($context["aArticles"]) ? $context["aArticles"] : null), "hyperlink"))) {
                    // line 10
                    echo "                        <a class=\"article__link\"";
                    echo $this->getAttribute((isset($context["Design"]) ? $context["Design"] : null), "write", array(0 => " sktag=\"modules.news.normal\""), "method");
                    echo " href=\"";
                    if ($this->getAttribute((isset($context["aArticles"]) ? $context["aArticles"] : null), "hyperlink")) {
                        echo $this->getAttribute((isset($context["aArticles"]) ? $context["aArticles"] : null), "hyperlink");
                    } else {
                        echo "[";
                        echo $this->getAttribute((isset($context["aArticles"]) ? $context["aArticles"] : null), "parent_section");
                        echo "][Articles?";
                        if ($this->getAttribute((isset($context["aArticles"]) ? $context["aArticles"] : null), "articles_alias")) {
                            echo "articles_alias=";
                            echo $this->getAttribute((isset($context["aArticles"]) ? $context["aArticles"] : null), "articles_alias");
                        } else {
                            echo "articles_id=";
                            echo $this->getAttribute((isset($context["aArticles"]) ? $context["aArticles"] : null), "id");
                        }
                        echo "]";
                    }
                    echo "\">";
                    echo $this->getAttribute((isset($context["aArticles"]) ? $context["aArticles"] : null), "title");
                    echo "</a>
                    ";
                } else {
                    // line 12
                    echo "                        <span class=\"article__link\"";
                    echo $this->getAttribute((isset($context["Design"]) ? $context["Design"] : null), "write", array(0 => " sktag=\"modules.news.normal\""), "method");
                    echo ">";
                    echo $this->getAttribute((isset($context["aArticles"]) ? $context["aArticles"] : null), "title");
                    echo "</span>
                    ";
                }
                // line 14
                echo "                </div>
                ";
                // line 15
                if ((!(isset($context["hideDate"]) ? $context["hideDate"] : null))) {
                    echo "<p class=\"article__date\"";
                    echo $this->getAttribute((isset($context["Design"]) ? $context["Design"] : null), "write", array(0 => " sktag=\"modules.news.date\""), "method");
                    echo ">";
                    echo $this->getAttribute((isset($context["aArticles"]) ? $context["aArticles"] : null), "publication_date");
                    echo "</p>";
                }
                // line 16
                echo "                <div class=\"b-editor\"";
                echo $this->getAttribute((isset($context["Design"]) ? $context["Design"] : null), "write", array(0 => " sktag=\"editor\""), "method");
                echo ">";
                echo $this->getAttribute((isset($context["aArticles"]) ? $context["aArticles"] : null), "announce");
                echo " </div>
                ";
                // line 17
                if ($this->getAttribute((isset($context["aArticles"]) ? $context["aArticles"] : null), "author")) {
                    echo "<p class=\"article__date\"";
                    echo $this->getAttribute((isset($context["Design"]) ? $context["Design"] : null), "write", array(0 => " sktag=\"modules.news.date\""), "method");
                    echo ">";
                    echo $this->getAttribute((isset($context["Lang"]) ? $context["Lang"] : null), "get", array(0 => "Articles.author"), "method");
                    echo ": ";
                    echo $this->getAttribute((isset($context["aArticles"]) ? $context["aArticles"] : null), "author");
                    echo "</p>";
                }
                // line 18
                echo "                <div class=\"g-clear\"></div>
            </div>
        ";
            }
            $_parent = $context['_parent'];
            unset($context['_seq'], $context['_iterated'], $context['iKey'], $context['aArticles'], $context['_parent'], $context['loop']);
            $context = array_intersect_key($context, $_parent) + $_parent;
            // line 21
            echo "    </div>

    ";
            // line 23
            if ((isset($context["section_all"]) ? $context["section_all"] : null)) {
                // line 24
                echo "        <a class=\"b-article-blog\" href=\"[";
                echo (isset($context["section_all"]) ? $context["section_all"] : null);
                echo "]\">";
                echo $this->getAttribute((isset($context["Lang"]) ? $context["Lang"] : null), "get", array(0 => "Articles.all_section_link"), "method");
                echo "</a>
    ";
            }
            // line 26
            echo "
";
        }
    }

    public function getTemplateName()
    {
        return "list_on_main.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  129 => 26,  121 => 24,  115 => 21,  97 => 17,  90 => 16,  82 => 15,  79 => 14,  71 => 12,  47 => 10,  45 => 9,  28 => 4,  26 => 3,  72 => 16,  64 => 13,  37 => 7,  33 => 6,  29 => 4,  25 => 3,  310 => 74,  304 => 70,  296 => 67,  278 => 65,  273 => 64,  270 => 63,  260 => 61,  258 => 60,  254 => 58,  248 => 56,  246 => 55,  242 => 53,  238 => 51,  232 => 50,  228 => 49,  225 => 48,  218 => 44,  209 => 42,  203 => 41,  200 => 40,  194 => 39,  190 => 37,  182 => 36,  173 => 35,  165 => 33,  146 => 31,  142 => 30,  130 => 29,  125 => 26,  119 => 23,  116 => 23,  110 => 21,  107 => 18,  101 => 18,  99 => 17,  95 => 15,  80 => 14,  66 => 13,  58 => 12,  55 => 10,  52 => 11,  48 => 8,  27 => 4,  24 => 3,  21 => 2,  237 => 49,  223 => 47,  219 => 46,  215 => 43,  201 => 43,  197 => 41,  193 => 39,  179 => 38,  170 => 34,  159 => 32,  156 => 32,  139 => 30,  136 => 29,  134 => 28,  128 => 25,  124 => 24,  109 => 21,  106 => 20,  89 => 19,  86 => 18,  84 => 17,  77 => 14,  73 => 12,  67 => 10,  61 => 12,  43 => 5,  40 => 4,  23 => 3,  65 => 9,  53 => 9,  41 => 7,  39 => 6,  36 => 6,  32 => 3,  30 => 2,  19 => 1,);
    }
}
