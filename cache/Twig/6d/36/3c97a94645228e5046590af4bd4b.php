<?php

/* sitemap.twig */
class __TwigTemplate_6d363c97a94645228e5046590af4bd4b extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        // line 1
        echo "<?xml version=\"1.0\" encoding=\"UTF-8\"?>
<urlset xmlns=\"http://www.sitemaps.org/schemas/sitemap/0.9\">
    ";
        // line 3
        $context['_parent'] = (array) $context;
        $context['_seq'] = twig_ensure_traversable((isset($context["items"]) ? $context["items"] : null));
        foreach ($context['_seq'] as $context["_key"] => $context["item"]) {
            // line 4
            echo "    <url>
        <loc>";
            // line 5
            echo $this->getAttribute((isset($context["item"]) ? $context["item"] : null), "url");
            echo "</loc>
        <lastmod>";
            // line 6
            echo $this->getAttribute((isset($context["item"]) ? $context["item"] : null), "modify_date");
            echo "</lastmod>
        <changefreq>";
            // line 7
            echo $this->getAttribute((isset($context["item"]) ? $context["item"] : null), "frequency");
            echo "</changefreq>
        <priority>";
            // line 8
            echo $this->getAttribute((isset($context["item"]) ? $context["item"] : null), "priority");
            echo "</priority>
    </url>
    ";
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['_key'], $context['item'], $context['_parent'], $context['loop']);
        $context = array_intersect_key($context, $_parent) + $_parent;
        // line 11
        echo "</urlset>
";
    }

    public function getTemplateName()
    {
        return "sitemap.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  51 => 11,  42 => 8,  38 => 7,  34 => 6,  30 => 5,  27 => 4,  23 => 3,  19 => 1,);
    }
}
