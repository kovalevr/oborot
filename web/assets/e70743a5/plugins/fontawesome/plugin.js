/*
 Author	: Michael Janea (https://facebook.com/mbjanea)
 Version	: 1.2
 */
function RGBToHex(color)
{
	color = color.replace(/\s/g,"");
	var aRGB = color.match(/^rgb\((\d{1,3}[%]?),(\d{1,3}[%]?),(\d{1,3}[%]?)\)$/i);
	if(aRGB) {
		color = '';
		for (var i=1;  i<=3; i++) color += Math.round((aRGB[i][aRGB[i].length-1]=="%"?2.55:1)*parseInt(aRGB[i])).toString(16).replace(/^(.)$/,'0$1');
	} else color = color.replace(/^#?([\da-f])([\da-f])([\da-f])$/i, '$1$1$2$2$3$3');
	return '#'+color;
}


CKEDITOR.plugins.add('fontawesome', {
	requires: 'widget',
	icons: 'fontawesome',
	init: function(editor) {
		editor.widgets.add('FontAwesome', {
			button: 'Insert Font Awesome',
			template: '<span class="" style=""></span>',
			dialog: 'fontawesomeDialog',
			allowedContent: 'span(!fa){style}',
			upcast: function(element) {
				return element.name == 'span' && element.hasClass('fa')
			},
			init: function() {
				this.setData('class', this.element.getAttribute('class'));
				if (this.element.getAttribute('class')=='cke_widget_element'){
					if (this.element.getStyle('color')=='')
						this.setData('color', '#000');
					if (this.element.getStyle('size')=='')
						this.setData('size', '20px');
				} else {

					this.setData('color', RGBToHex(this.element.getStyle('color')));
					this.setData('size', this.element.getStyle('font-size'));
				}
			},
			data: function() {
				var istayl = '';
				this.element.setAttribute('class', this.data.class);
				istayl += this.data.color != '' ? 'color:' + this.data.color + ';' : '';
				istayl += this.data.size != '' ? 'font-size:' + parseInt(this.data.size) + 'px;' : '';
				istayl != '' ? this.element.setAttribute('style', istayl) : '';
				istayl == '' ? this.element.removeAttribute('style') : ''
			}
		});
		CKEDITOR.dialog.add('fontawesomeDialog', this.path + 'dialogs/fontawesome.js');
		CKEDITOR.document.appendStyleSheet(CKEDITOR.plugins.getPath('fontawesome') + 'font-awesome/css/font-awesome.min.css')
	}
});