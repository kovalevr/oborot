$(function() {

    //select
    $('.js-form-select').each(function (){
        var theme = 'default'
        var options = {
            minimumResultsForSearch: Infinity,
            placeholder: $(this).data('placeholder'),
            theme: theme
        }
        $(this).select2(options);

    })

});// ready