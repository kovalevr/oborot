var bFormSubmitAllow = [];

$(function(){

    /* применяем правила валидации к полям */
    $('form').each( function() {
        var formId = this.id;
        if ( /form_(.+?)/ , formId ) { // отсекли левые формы типа поиска
            updateFromValidator( formId );
        }
    });

    /* релоад каптчи */
    $(document).on( 'click', '.img_captcha', function() {
        return reloadImg( this );
    });

    function isMobile(){
        return window.innerWidth < $('#js-adaptive-min-form-width').val();
    }


        if (!isMobile()){
            $( ".js_calendar" ).each(function( index ) {
                $(this).attr('type','text');
                $(this).addClass("js_init_datepicker");
                $(this).removeClass("js_calendar");

                /* calendar - инициализация календарика */
                if ( typeof $.datepicker !== 'undefined' ) {
                    $.datepicker.setDefaults({
                        dateFormat: 'dd.mm.yy'
                    });

                    $('.js_init_datepicker').datepicker({
                        nextText: '',
                        prevText: ''
                    });

                    $('body').on('click', '.js_ui-datepicker-trigger', function() {
                        $( '.js_init_datepicker', $(this).parent('div') ).focus();
                    });
                }
            });
        }

    jQuery.validator.addMethod( 'date', function( value ) {

        if ( value == '' ) return true;

        const regex = /^(\d{1}|\d{2})[-|.|/](\d{1}|\d{2})[-|.|/](\d{2}|\d{4})$/gm;
        var match = regex.exec(value);

        if ( match === null )
            return false;

        if( parseInt(match[3]) < 1000 || parseInt(match[3]) > 9999 ) return false;
        if( parseInt(match[2]) < 1 || parseInt(match[2]) > 12 ) return false;
        if( parseInt(match[1]) < 1 || parseInt(match[1]) > 31 ) return false;
        var tmpDate = new Date( match[3], parseInt(match[2]) - 1, match[1], 12 );

        return !/Invalid|NaN/.test(tmpDate);
    });

    $.validator.addMethod('filesize', function(value, element, param) {
        if ($(element).attr('type') != "file"){
            return true;
        }
        if (element.files.length > 0){
            return this.optional(element) || (element.files[0].size <= param[0])
        }
        return true;
    });

    // $('.agreed_readmore').fancybox();
    $('.js-agreed-readmore').click(function (event) {
        event.preventDefault();
        $('.js-agreed_text').toggle();
    });
});


function updateFromValidator ( formId ) {

    /*Добавим в валидатор метод который валидирует группу галочек*/
    $.validator.addMethod('require_checkbox_group', function(value) {
        if (typeof(value)=='undefined')
            return false;
        else
            return true;
    });

    var form = $( '#' + formId );
    var sRules = $('._rules', form).val();

    if ( sRules ) {

        sRules = $.parseJSON( sRules );

        /*Обойдем ВСЕ группы галочек которые обязательны и добавим им метод для валидации*/
        $('.js_required_group').each( function() {
            var field_name = $(this).find('input')[0].name;
            sRules.rules[field_name] = {'require_checkbox_group':true};
        });

        bFormSubmitAllow[ formId ] = false;

        sRules.errorPlacement = function( error, element ) {
            element.closest(".form__item").addClass('form__item--error');

        };

        sRules.unhighlight = function(element, error, valid, _orig) {
            $(element).closest(".form__item").removeClass('form__item--error');
        };

        /*Назначим обработчик который соберет список ошибок*/
        sRules.invalidHandler = function(event, validator) {

            /*Cброс подсветки полей с предыдущей валидации*/
            $('.form__item').removeClass('form__item--error');


            var errors = validator.numberOfInvalids();
            if (errors) {

                var form_hash = $(validator.currentForm).data('hash');
                // var form_hash = $(validator.errorList[0].element).data('hash');

                /*Сброс блока с ошибками*/
                $('#js-error_required_'+form_hash).empty();
                $('#js-error_valid_'+form_hash).empty();

                var aRequired = [];
                var aValid = [];

                for (var i = 0; i < validator.errorList.length; i++) {

                    var error_content = validator.errorList[i];

                    var error_text='"'+$(error_content.element).data('name')+'" '+error_content.message;

                    if (error_content.method=='required' || error_content.method=='require_checkbox_group'){
                        //в блок обязательных
                        aRequired.push("<li>"+error_text+"</li>");
                    } else {
                        //в блок прочего
                        aValid.push("<li>"+error_text+"</li>");
                    }
                }

                for (var i = 0; i < aRequired.length; i++) {
                    $( '#js-error_required_'+form_hash ).append( aRequired[i] );
                }

                if (aValid.length) {
                    $("#js-error_valid_title_"+form_hash).show();
                    $("#js-error_valid_"+form_hash).show();
                    for (var i = 0; i < aValid.length; i++) {
                        $('#js-error_valid_' + form_hash).append(aValid[i]);
                    }
                } else {
                    $("#js-error_valid_title_"+form_hash).hide();
                    $("#js-error_valid_"+form_hash).hide();
                }

                $("#js-error_block_"+form_hash).show();
            }

        };


        $( form ).unbind( 'submit' );

        var oValidator = $(form).validate(sRules);

        // Для обновления размеров fancybox окна после jquery валидации
        if ( $( form ).data( 'ajaxform' ) == 1 ) {

            oValidator.showErrors = function (errorMap, errorList) {
                var errors = errorMap;
                if ( errors ) {
                    var validator = this;
                    validator.findByName('captcha')[0].value = '';
                    // Add items to error list and map
                    $.extend( this.errorMap, errors );
                    this.errorList = $.map( this.errorMap, function( message, name ) {
                        return {
                            message: message,
                            element: validator.findByName( name )[ 0 ]
                        };
                    } );

                    // Remove items from success list
                    this.successList = $.grep( this.successList, function( element ) {
                        return !( element.name in errors );
                    } );
                }
                if ( this.settings.showErrors ) {
                    this.settings.showErrors.call( this, this.errorMap, this.errorList );
                } else {
                    this.defaultShowErrors();
                }
            };
        }

        function formSubmit(){

            $('.js-agreed_text').css('display','none');

            if ($( form ).data( 'ajaxform' ) != 1) {
                if (bFormSubmitAllow[formId]) return true;
            }

           // if ( oValidator.errorList.length != 0 )
             //   return false;

            if ( $( ".img_captcha", $(form) ).size() ) {

                var sCode = $( "input[name=captcha]", $(form) ).val();
                var sHash = $(form).attr('id');
                if ( sHash )
                    sHash = sHash.substr(5);

                $.post( '/ajax/ajax.php',{
                        moduleName: 'Forms',
                        cmd: 'captcha_ajax',
                        code: sCode,
                        hash: sHash,
                        language: $('#current_language').val()
                    },
                    function ( mCaptchaResponse ) {

                        if ( !mCaptchaResponse ) {
                            alert( 'Error: message not sent.' );
                            return false;
                        }

                        var oResponse = $.parseJSON( mCaptchaResponse );
                        var sResponse = oResponse.data.out;

                        if ( sResponse == '1' ) {

                            if ( oValidator.errorList.length != 0 )
                                return false;

                            bFormSubmitAllow[ formId ] = true;

                            if ( $( form ).data( 'ajaxform' ) == 1 ) {

                                setTimeout(function(){
                                    if (oValidator.errorList.length==0)
                                        sendAjaxForm(form);
                                },500);

                            } else {

                                $( form ).submit();
                            }
                        } else {

                            reloadCaptchaByForm( form );
                            $('#captcha').val('');

                            setTimeout(function(){
                                oValidator.showErrors({"captcha": sResponse});
                            },500);

                        }

                        return true;
                    });

                return false;

            } else {

                bFormSubmitAllow[ formId ] = true;

                if ( $( form ).data( 'ajaxform' ) == 1 ) {

                    setTimeout(function(){
                        if (oValidator.errorList.length==0)
                            sendAjaxForm(form);
                    },500);
                    return false;

                } else {

                    $( form ).submit();
                }

            }

            return false;
        }

        $( form ).submit( function() {
            return formSubmit();
        })
    }
}

function sendAjaxForm( form ) {

    $.ajax({
        url: '/ajax/ajax.php' + '?cmd=send' + '&moduleName=Forms' + '&ajaxForm=1'+ '&language=' + $('#current_language').val() + '&sectionId=' + $("#current_section").val(),
        type: 'POST',
        data: new FormData(form[0]), // Добавить форму с файлами
        cache: false,
        dataType: 'json',
        processData: false,
        contentType: false,
        success: function ( mFormResponse ) {
            if ( mFormResponse ) {
                var oResponse = mFormResponse;

                if ( $( form ).data('popup_result_page') == '1' && !isMobile() && $.fancybox.getInstance() === false ){
                    //Всплывающая результирующая
                    $.fancybox.open(oResponse.html,{
                        dataType : 'html',
                        afterClose: function(){
                            //Чистим поля формы
                            form[0].reset();
                            var hash = $( form ).data('hash');
                            $("#js-error_block_"+hash).html('');
                        }
                    });

                } else {

                    var fancyboxDiv = $( form ).parents( '.js-form' );

                    // Чистка контейнера формы
                    fancyboxDiv.hide();
                    fancyboxDiv.html( '' );


                    // Чистка таблицы товаров заказа
                    $('.js-cart-content').remove();

                    // Выводим текст результирующей
                    fancyboxDiv.html( oResponse.html );
                    fancyboxDiv.show();
                }

                setTimeout( function(){
                    $.fancybox.close();
                    reloadCaptchaByForm(form);
                }, 2000);
            }
        }
    });
    return false;
}

function maskInit(el){
    $('input', el).each(function(){
        switch ($(this).data('mask')){
            case "phone":
                $(this).inputmask("mask", {"mask": "+7(999) 999-9999", "placeholder": "+7(___) ___-____"});
                break;
        }
    });
}

function toggleShowPlaceholder(el) {
    $('input, textarea', el).each(function(){

        // обрабатываем только раз
        if ( $(this).data('js_placeholder') )
            return;

        if ($(this).prop('placeholder') && $(this).data('hide-placeholder')) {

            $(this).data('placeholder', $(this).prop('placeholder'));

            this.onmouseenter = function() {
                $(this).prop('placeholder', '');
            };

            this.onfocus = this.onmouseenter;

            this.onmouseleave = function() {
                if ($(this).is(':focus'))
                    return;
                $(this).prop('placeholder', $(this).data('placeholder'));
            };

            this.onblur = function() {
                // синтаксис именно такой для jq > 1.9
                if ($(this).filter(':hover').size())
                    return;
                $(this).prop('placeholder', $(this).data('placeholder'));
            };

        }

        // флаг "обработано"
        $(this).data('js_placeholder', 1);

    });
}

$(document).ready(function(){
    maskInit();
    toggleShowPlaceholder();
});
