$(document).ready(function(){

    setTimeout(function(){
        $(".js_good_is_not_available").hide();
    },15000);

    // Общая функция посылки команд для работы с корзиной и обновления изменённых параметров в заказе
    function sendParam(params, callback) {

        params.moduleName = 'Cart';
        params.language = $('#current_language').val();

        $.post('/ajax/ajax.php', params, function(jsonData) {

            var aData = jQuery.parseJSON(jsonData);

            // Обновить общую стоимость последней изменённой позиции в соответствии с данными от сервера
            if (params.id != undefined && aData.lastItem.id_goods == params.id) {

                var oItemRow = $('.js-cart__row[data-id=' + params.id + ']');

                oItemRow.find('.js_cart_amount').val(aData.lastItem.count);
                oItemRow.data("count_goods_before_recount", aData.lastItem.count);
                oItemRow.find('.js-item_total').text(aData.lastItem.total);
            }

            $('.total').text(aData.total);

            // Если указана ф-ция обратного вызова, то выполнить её
            if (typeof callback === 'function')
                callback(aData);

            // Обновить мини-корзину, если присутствует
            if (window.updateMiniCart !== undefined)
                updateMiniCart(jsonData);
        });
    }

    // Удаление каталожной позиции
    $('body').on('click', '.js_cart_remove', function() {

        var me = this;
        var id_good = $(this).attr('data-id');

        var tableRow = $(this).closest(".js-cart__row"),
            oldCount = tableRow.data("count_goods_before_recount");

        sendParam({cmd: 'removeItem', id: id_good}, function(aData) {

            // Удалить строку каталожной позиции
            $(me).parents('.js-cart__row').remove();

            // Если больше нет заказанных позиций, то очистить корзину
            if (!aData.count) {
                $('.js_cart_content').hide();
                $('.js_cart_empty').removeClass('cart__empty-hidden');
            }

            ecommerce.sendDataChangeCart( tableRow.data("ecommerce"), oldCount, 0 );

        });

        return false;
    });

    // Уменьшение количества каталожной позиции
    $('body').on('click', ".js_cart_minus", function(){

        var me = this;
        var id_good = $(me).attr('data-id');

        var row = $('.js_cart_amount[data-id=' + id_good + ']');
        var count = parseInt(row.val());

        // если не число - поставить 1
        if ( isNaN(count) )
            count = 1;

        // меньше 1 быть не может
        if (count > 1)
            count--;
        else
            count = 1;

        var tableRow = $(this).closest(".js-cart__row"),
            oldCount = tableRow.data("count_goods_before_recount");

        sendParam({cmd: 'recountItem', id: id_good, count: count}, function(){
            ecommerce.sendDataChangeCart( tableRow.data("ecommerce"), oldCount, count );
        });
    });

    // Увеличение количества каталожной позиции
    $('body').on('click', ".js_cart_plus", function() {

        var me = this;
        var id_good = $(me).attr('data-id');

        var row = $('.js_cart_amount[data-id=' + id_good + ']');
        var count = parseInt(row.val());

        // проверка на валидность
        if (isNaN(count))
            count = 0;

        // увеличть на 1 (если меньше 0 - привести к 1)
        if (count >= 0)
            count++;
        else
            count = 1;

        var tableRow = $(this).closest(".js-cart__row"),
            oldCount = tableRow.data("count_goods_before_recount");

        sendParam({cmd: 'recountItem', id: id_good, count: count}, function(){
            ecommerce.sendDataChangeCart( tableRow.data("ecommerce"), oldCount, count );
        });


    });

    // Ручное изменение количества товара
    $('.js_cart_amount')
        .keypress(function(e){
            if (e.keyCode==13){
                $(this).blur();
            }
        })
        .blur(function() {

            var me = this;
            var id_good = $(me).attr('data-id');

            var countValue = parseInt($(me).val());
            if ( isNaN(countValue) )
                countValue = 1;

            if (countValue > 0 && countValue == $(me).val()){

                var tableRow = $(this).closest(".js-cart__row"),
                    oldCount = tableRow.data("count_goods_before_recount");

                sendParam({cmd: 'recountItem', id: id_good, count: countValue}, function(){
                    ecommerce.sendDataChangeCart( tableRow.data("ecommerce"), oldCount, countValue );
                });

            } else
                alert($('#js_translate_msg_count_gt_zero').html());
        })
    ;

    // Очистка корзины
    $('body').on('click', '.js_cart_reset', function(){

        if (!confirm($('#js_translate_msg_dell_all').html()))
            return false;

        var aEcommerceObjects = [];
        $(".js-cart__row").each(function(index, item){
            var temp = $(item).data('ecommerce');
            if ( temp ) {
                temp.quantity = $(".js_cart_amount", $(item)).val();
                aEcommerceObjects.push(temp);
            }
        });

        sendParam({cmd: 'unsetAll'}, function(aData){

            $('.js_cart_content').hide();
            $('.js_cart_empty').removeClass('cart__empty-hidden');

            ecommerce.sendDataClearCart( aEcommerceObjects );
        });

        return false;
    });

    // $('.js_anchor_agreed_readmore').fancybox();
    /** показ и скрытие данных по обработке персональных данных*/
    $('body').on('click', '.js_anchor_agreed_readmore', function () {
        if ($('.js-agreed_text').css('display') == 'block')
            $('.js-agreed_text').css('display','');
        else
            $('.js-agreed_text').css('display','none');
    });

    $('body').on( 'click', '.fast_agreed_readmore', function(){

        var agreed_container = $('.js_agreed_readmore');

        if (agreed_container.hasClass('open')){
            agreed_container
                .removeClass('open')
                .hide()
            ;
        }else{
            agreed_container
                .addClass('open')
                .show()
            ;
        }

        return false;

    });
});