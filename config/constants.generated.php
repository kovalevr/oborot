<?php
/**
 * Этот файл генерируется автоматически!
 * @date 09-10-2017 06:49:52
 * @project Skewer
 * @package Site
 */

define('USECLUSTERBUILD', false);
define('INCLUSTER', 1);

define('BUILDNAME', 'canape');

define('BUILDNUMBER', '0029m1');

define('ROOTPATH', '/var/www/testing-1c/');

define('RELEASEPATH', '/var/www/testing-1c/skewer/');

define('APPKEY', 'c811daf594b90aa2d1a189cb439986ad59db9a97');

define('CLUSTERGATEWAY', 'http://sms.sktest.ru/gateway/index.php');
