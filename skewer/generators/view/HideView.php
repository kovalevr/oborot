<?php

namespace skewer\generators\view;

class HideView extends PrototypeView {

    protected $sType = 'string hidden';
    /**
     * @inheritDoc
     */
    public function getCode()
    {
        return '<?=($'.$this->sName.')?"<li hidden>".$aNameField["'.$this->sName.'"].":".$'.$this->sName.'."</li>":""?>'."\n";
    }
}

?>

