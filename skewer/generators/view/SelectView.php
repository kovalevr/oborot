<?php

namespace skewer\generators\view;


class SelectView extends PrototypeView {

    protected $sType = 'string';

    public function getCodeDetail()
    {
        $mCardId = $this->aField['link_id'];
        if ($mCardId) {
            return str_replace('%s',$this->sName,"// Обработка поля справочника - %s \n".
                '       $aDictSelect = Dict::getValues('.$mCardId.',$aFieldDict[\'%s\'],true);'."\n".
                '       $aFieldDict[\'%s\'] = (isset($aDictSelect[\'title\']))?$aDictSelect[\'title\']:\'\';');
        }
    }
}

?>