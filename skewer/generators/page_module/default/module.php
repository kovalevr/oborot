<?php
/**
 * This is the template for generating a module class file.
 * @var $this yii\web\View
 * @var $generator \skewer\generators\page_module\Generator
 */
use skewer\generators\page_module\Api;
    $className = $generator->moduleName;
    $nameDict = $generator->nameDict;
    $fullClassName = $generator->getModulePath();
    $ns = 'skewer\build\Page\\'.$className;
    echo "<?php\n";
?>

namespace <?= $ns ?>;
<? foreach (Api::getUses($nameDict) as $sUse):?>
use <?=$sUse?>;
<?endforeach;?>
/**
 *  Class Module
 * @package skewer\build\Page\<?= $className."\n"?>
 */
class Module extends site_module\page\ModulePrototype {

    public $listTemplate = 'list.php';
    public $detailTemplate = 'detail_page.php';

    public $nameDict = "<?= $nameDict ?>";
    public $onPage = 10;
    

    public function init() {
        $this->setParser(parserPHP);
        return true;
    }


    /**
    * Выводит список элементов из справочника
    * @param int $page номер страницы
    * @return int
    */
    public function actionIndex( $page=1) {
    
        if (!$this->onPage) return psComplete;
        $iCount = 0;
        
        // Получаем список элементов словаря
        
        $sNameTable = Dict::getDictTableName($this->nameDict);
        $aListElement = Query::SelectFrom($sNameTable)
                                ->setCounterRef($iCount)
                                ->limit($this->onPage,($page-1)*$this->onPage)
                                ->order('priority')
                                ->getAll();
        //пагинатор
        $this->getPageLine($page, $iCount, $this->sectionId(), [], array( 'onPage' => $this->onPage));
        
        $this->setTemplate($this->listTemplate);
        $this->setData('aListElement',$aListElement);
        $this->setData('sectionId',$this->sectionId());
        
        return psComplete;
        
    }

    /**
    * Выводит список элементов по id
    */
    public function actionViewById() {
        $id = $this->get('dict_id');
        $oDict = Dict::getValues($this->nameDict,$id);
        return $this->showOne($oDict);
    }
    
    /**
    * Выводит список элементов по alias
    */
    public function actionViewByAlias() {
        $alias = $this->get('dict_alias');
        $oDict = Dict::getValByString($this->nameDict,$alias,false,'alias');
        return $this->showOne($oDict);
    }

    /**
    * Выводит элемент
    * @param \skewer\base\orm\ActiveRecord $oDict
    * @return int
    */
    public function showOne( $oDict ) {

        $aNameField = [];
        $aFieldDict = $oDict->getData();
        Page::setTitle(false);
        // добавляем элемент в pathline
        Page::setAddPathItem( $aFieldDict['title'] );

        foreach ($aFieldDict as $sFieldName=>$sValue)
            $aNameField[$sFieldName] = $oDict->getModel()->getFiled($sFieldName)->getTitle();

<? foreach (Api::getArrayPrototypeView($nameDict) as $oField) {
    $sCode = $oField->getCodeDetail();
    if ($sCode)
        echo "        ".$sCode."\n\n";
    }
?>


        foreach ($aFieldDict as $sName=>$sValue)
            $this->setData($sName,$sValue);
        
        $this->setData('aNameField',$aNameField);
        $this->setData('aFieldDict',$aFieldDict);
        $this->setTemplate($this->detailTemplate);
        return psComplete;

    }

}