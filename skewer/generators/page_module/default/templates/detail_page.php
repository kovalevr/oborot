<?php
/**
 * 
 * This is the template for generating detail page file.
 * @var $this \yii\web\View
 * @var $generator \skewer\generators\page_module\Generator 
 * 
 */
use skewer\generators\page_module\Api;
    $className = $generator->moduleName;
    $aDictField = Api::getArrayPrototypeView($generator->nameDict);
    echo "<?php\n";
?>
    /**
    * Structure: <?=$className."\n"?>
    * @var array() $aNameField - наименования строк
    * @var int $id - not displayed
<? foreach ($aDictField as $oField):?>
    <?=$oField->getComment();?>
<? endforeach;?>
    */
?>
<h1><? echo "<?= "?> $title ?></h1>

<ul>
<? foreach ($aDictField as $oField) {
        if (!in_array($oField->sName,Api::$aNotShow))
            echo $oField->getCode();
   }
?>

    <p class="dict__linkback">
        <a rel="nofollow" href="#" onclick="history.go(-1);return false;">
            <? echo "<?= "?> \Yii::t('page', 'back') ?>
        </a>
    </p>
</ul>