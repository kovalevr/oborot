<?php
/**
 *
 * This is the template for generating list page file.
 * @var $generator \skewer\generators\page_module\Generator 
 */

$className = $generator->moduleName;
echo "<?php";
?>

/**
 * @var array() $aListElement - массив элементов справочника
 * @var int $sectionId
 */
?>

<div class=" ">
    <ul>
        <? echo "<? "?> foreach ($aListElement as $aValue):?>
        <? echo "<? "?>if (isset($aValue['alias'])&&$aValue['alias'])
            $hrefParam = "dict_alias={$aValue['alias']}";
        else
            $hrefParam = "dict_id={$aValue['id']}";
        $href = "[{$sectionId}][<?=$className?>?".$hrefParam."]"; ?>
            <li class="b-dict-<? echo "<?= "?>$aValue['id']?>">
                <a href="<? echo "<?="?> $href ?>"><? echo "<?= "?>$aValue['title']?> </a>
            </li>
        <? echo "<? "?> endforeach;?>
    </ul>
    <? echo "<? "?> include(BUILDPATH . 'common/templates/paginator.php'); ?>
</div>