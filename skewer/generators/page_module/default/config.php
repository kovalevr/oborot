<?php
/**
 * This is the template for generating config file.
 * @var $this yii\web\View
 * @var $generator \skewer\generators\page_module\Generator
 */

$className = $generator->moduleName;

$fullClassName = $generator->getModulePath();
$ns = 'skewer\build\Page\\'.$className;
$sLayer = skewer\base\site\Layer::PAGE;

echo "<?php\n";
?>

/* main */
use skewer\base\site\Layer;


$aConfig['name']     = "<?= $className ?>";
$aConfig['version']  = '1.0';
$aConfig['title']    = "<?= $className ?>";
$aConfig['description']  = 'Модуль из словаря системы';
$aConfig['revision'] = '0001';
$aConfig['layer']     = "<?= $sLayer?>";
$aConfig['useNamespace'] = true;

return $aConfig;
