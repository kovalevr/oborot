<?php

namespace skewer\components\forms;


use skewer\base\site_module\Parser;
use skewer\components\targets;
use skewer\components\ecommerce;
use yii\base\UserException;

class Api{

    public static $sRedirectUri = null;
    public static $sAnswerText  = null;

    /**
     * Вернет html результирующей страницы успешной отправки
     * @param Entity $oForm
     * @param $bAjaxForm - форма отправляется через ajax?
     * @param $iSectionId - id раздела
     * @param array $aParams
     * @return string
     */
    public static function buildSuccessAnswer(Entity $oForm, $bAjaxForm, $iSectionId, $aParams = []){

        // Текст для форм со сторонней результирующей редактируется в разделе на который идёт перенаправление
        if ( !$bAjaxForm and $oForm->hasExternalResultPage() and $oForm->getFormRedirect(true) )
            return '';

        $aData = [];

        // Строим js-скрипт ричголов
        $aData['reachGoals'] = targets\Api::buildScriptTargetsInForm( $oForm );

        // Альтернативный текст ответа пользователю
        if ( $sFormAnswer = $oForm->getFormSuccAnswer(true) )
            $aData['SuccAnswer'] = $sFormAnswer;

        $sData = $bAjaxForm ? 'successAjax' : 'success';
        $aData[$sData] =  1;

        if ( $oForm->hasPopupResultPage() )
            $aData['popup_result_page'] = true;

        //Передача e-commerce данных после отправка формы "Купить в один клик"
        if ( $oForm->getFormParam('form_name') == Entity::NAME_FORM_ONE_CLICK ) {
            $aData['ecommerce'] = ecommerce\Api::buildScriptPurchase( $oForm->getResultHandlers(), $iSectionId, true );
        }

        $aData += $aParams;

        return Parser::parseTwig( 'answer.twig', $aData, __DIR__ . '/templates' );

    }

    /**
     * Вернет html результирующей страницы неуспешной отправки
     * @param string $sErrorMessage - код ошибки
     * @return string
     */
    public static function buildErrorAnswer( $sErrorMessage ){
        $aData[$sErrorMessage] = 1;
        return Parser::parseTwig( 'error.twig', $aData, __DIR__ . '/templates' );
    }

    public static function getViewTypesBeFieldTypeId($iFieldTypeId){
        if (!isset( FieldTable::$aTypes[$iFieldTypeId]))
            throw new UserException('Invalid field type');

        $sClassName = '\\skewer\\components\\forms\\field\\'.ucfirst(FieldTable::$aTypes[$iFieldTypeId]);

        $aParams = [
            0=>'---'
        ];

        if (method_exists($sClassName,'getViewTypes'))
            $aParams = $sClassName::getViewTypes();

        return $aParams;
    }
}