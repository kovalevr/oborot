<?php

namespace skewer\components\forms\field;


class Checkbox extends Prototype {

    public $type = 'checkbox';
    public $clsSpec = 'form__checkbox';

    protected $sTpl = 'checkbox.twig';

    public static function getViewTypes(){
        return [
            '1'=>'Тип 1',
            '2'=>'Тип 2',
            '3'=>'Тип 3',
            '4'=>'Тип 4',
            '5'=>'Тип 5'
        ];
    }

} 
