<?php

namespace skewer\components\forms\field;


class CheckboxGroup extends Select {

    public $type = 'checkboxGroup';
    public $clsSpec = 'form__checkbox_group';

    protected $sTpl = 'checkbox_group.twig';

    public static function getViewTypes(){
        return [
            '1'=>'Тип 1',
            '2'=>'Тип 2',
            '3'=>'Тип 3',
            '4'=>'Тип 4',
            '5'=>'Тип 5'
        ];
    }

} 
