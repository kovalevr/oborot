<?php

namespace skewer\components\forms\field;


class Rating extends Radio {

    public $type = 'radio';
    public $clsSpec = 'form__radio';

    protected $sTpl = 'rating.twig';

    public static function getViewTypes(){
        return [
            '1'=>'Звезды'
        ];
    }

} 