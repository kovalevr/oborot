<?php

namespace skewer\components\forms\field;


class Select extends Prototype {

    public $type = 'select';

    protected $sTpl = 'select.twig';
} 