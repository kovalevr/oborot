<?php

namespace skewer\components\forms\field;


class Radio extends Select {

    public $type = 'radio';
    public $clsSpec = 'form__radio';

    protected $sTpl = 'radio.twig';

    public static function getViewTypes(){
        return [
            '1'=>'Тип 1',
            '2'=>'Тип 2',
            '3'=>'Тип 3',
            '4'=>'Тип 4'
        ];
    }

} 