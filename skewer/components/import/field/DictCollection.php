<?php

namespace skewer\components\import\field;

use skewer\components\import;
use skewer\components\catalog;
use skewer\helpers\Mailer;
use skewer\helpers\Transliterate;


/**
 * Обработчик поля типа коллекция
 */
class DictCollection extends Dict {

    /** @var bool Создавать новые */
    protected $create = false;
    /** @var bool Активировать новый элемент коллекции */
    protected $active_new = false;
    /** @var bool Отправлять уведомление о добавлении элементов коллекции */
    protected $send_mail = true;

    protected static $parameters = [
        'create' => [
            'title' => 'field_dict_create',
            'datatype' => 'i',
            'viewtype' => 'check',
            'default' => 0
        ],
        'active_new' => [
            'title' => 'field_el_col_new',
            'datatype' => 'i',
            'viewtype' => 'check',
            'default' => 0
        ],
        'send_mail' => [
            'title' => 'field_col_send_mail',
            'datatype' => 'i',
            'viewtype' => 'check',
            'default' => 1
        ]

    ];

    /**
     * Отдает значение на сохранение в запись товара
     * @return mixed
     */
    public function getValue() {

        $val = implode(',', $this->values);

        if ($this->sCardDictId) {

            //ищем в справочнике
            $aElement = catalog\Dict::getValByTitle($this->sCardDictId, $val, true);

            if ($aElement) {
                return $aElement['id'];
            }

            //создадим, если надо
            if ($this->create and $val != ''){

                $alias = Transliterate::generateAlias($val);
                $oTableDict = catalog\Dict::getTableDict($this->sCardDictId);
                $oItem = $oTableDict->getNewRow();

                $oItem->setData([
                    'title' => $val,
                    'alias' => $alias,
                    'active' => $this->active_new,
                    'on_main' => '0'
                ]);
                $oItem->save();

                if ($oItem->id) {
                    $aCollection = (catalog\Card::getCollection($this->sCardDictId));
                    if ($this->send_mail) {
                        $sLetter = str_replace(
                                            ["[title_element]","[title_collection]"],
                                            [$val,($aCollection)?$aCollection->title:$this->sCardDictId],
                                            \Yii::t('collections',"letter_admin"));
                        Mailer::sendMailAdmin(\Yii::t('collections',"title_mail_new_element"),$sLetter);
                    }
                    return $oItem->id;
                }
            }
        }

        return '';
    }

}