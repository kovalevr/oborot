<?php

namespace skewer\components\import\field;

use skewer\base\log\Logger;
use skewer\components\gallery\Album;
use skewer\components\gallery\Format;
use skewer\components\gallery\Profile;
use skewer\components\gallery\Photo;
use skewer\components\import;

/**
 * Обработчик поля типа галерея
 */
class Gallery extends Prototype {

    /** @var string Разделитель фоток */
    protected $delimiter = ',';

    /** @var bool Пересоздавать фото в альбоме */
    protected $recreate = false;

    /** @var bool Искать по названию */
    protected $find = false;

    /** @var [] кроп */
    protected $crop = [];

    /** @var int Профиль */
    protected $profile = 0;

    /** @var array Изображения */
    protected $photos = [];

    /** @var int Альбом */
    protected $album = 0;

    /** @var array Разрешенные форматы файлов */
    protected $allowFormatFile = [];

    protected $imagedir = 'import';

    protected static $parameters = [
        'delimiter' => [
            'title' => 'field_gallery_delimiter',
            'datatype' => 's',
            'viewtype' => 'string',
            'default' => ','
        ],
        'find' => [
            'title' => 'field_gallery_find',
            'datatype' => 'i',
            'viewtype' => 'check',
            'default' => '0'
        ],
        'recreate' => [
            'title' => 'field_gallery_recreate',
            'datatype' => 's',
            'viewtype' => 'check',
            'default' => '0'
        ],
        'imagedir' => [
            'title' => 'field_gallery_image_dir',
            'datatype' => 's',
            'viewtype' => 'string',
            'default'  => 'import',
        ]
    ];

    public function init(){

        $this->profile = Profile::getDefaultId(Profile::TYPE_CATALOG);

        $this->crop = Format::getCrop4Catalog();

        $this->allowFormatFile = \Yii::$app->getParam(['upload','allow','images']);
    }


    /**
     * @inheritdoc
     */
    public function beforeExecute(){
        $this->photos = [];
    }


    /**
     * Отдает значение на сохранение в запись товара
     * @return mixed
     */
    public function getValue(){

        $this->album = $this->getGoodsRow()->getData()[$this->fieldName];

        //если не нужна перезапись и фотки уже есть
        if (!$this->recreate && Photo::getCountByAlbum( $this->album ))
            return $this->album;

        $sVal = implode( $this->delimiter, $this->values );

        if ($sVal){

            //создадим альбом
            if (!$this->album)
                $this->album = Album::create4Catalog();

            if ( $this->imagedir )
                $sImageDir = ROOTPATH . $this->imagedir . '/';

            $sImageDir = str_replace( '//', '/', $sImageDir);

            //собираем фотки
            if ($this->find){
                //ищем по названию
                $aFiles = glob($sImageDir . $sVal . '.*');
                if ($aFiles)
                    //проверим на форматы
                    foreach( $aFiles as $sFileName ){
                        $ext = strtolower(substr($sFileName, strrpos($sFileName, '.') + 1));
                        if (in_array($ext, $this->allowFormatFile))
                            $this->photos[] = $sFileName;
                    }
            }else{
                $this->photos = explode( $this->delimiter, $sVal );
                $this->photos = array_map(function( $s ) use ($sImageDir){
                        //где-то этот путь должен быть сохранен
                        return $sImageDir . $s;
                    },
                $this->photos);
            }

        }

        return $this->album;

    }


    /**
     * @inheritdoc
     */
    public function afterSave(){

        $oGoodsRow = $this->getGoodsRow();
        if (!$oGoodsRow)
            return;

        /** Добавляем фотки здесь, так как при добавлении товар уже должен быть создан */
        if ($this->photos){
            //чистим старые
            Photo::removeFromAlbum( $this->album );
            foreach( $this->photos as $sPhoto ){
                $bAbb = Photo::addPhotoInAlbum( $sPhoto, $this->album, $this->crop, $this->profile );
                if ($bAbb){
                    $this->logger->incParam( 'add_photo' );
                }
            }
        }

    }

}