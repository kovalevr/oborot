<?php

namespace skewer\components\import\field;

use skewer\components\import;

/**
 * Обработчик поля типа деньги
 */
class Money extends Prototype {

    /**
     * Отдает значение на сохранение в запись товара
     * @return mixed
     */
    public function getValue(){

        $fSum = 0;

        foreach ($this->values as $value)
            $fSum += self::parseToFloat($value);

        return (float) $fSum;
    }


    /**
     * Преобразует строку во float значение
     * @param $sValue
     * @return float
     */
    protected function parseToFloat($sValue){

        $sValue = preg_replace('/[^0-9,\.]/', '', $sValue);
        $sValue = rtrim($sValue,'.');

        $iPosDot = strpos($sValue, '.'); // Позиция точки
        $iPosComma = strpos($sValue, ','); // Позиция запятой

        if ($iPosDot && $iPosComma){  // 2 различных разделителя
            //
            $sThousandDelimiter =  $sValue[min($iPosDot,$iPosComma)];
            $sValue = str_replace($sThousandDelimiter,'',$sValue);
            
        } elseif ($iPosDot or $iPosComma){ // 1 разделитель
            $iPosDelimiter = $iPosDot ? $iPosDot : $iPosComma;
            $delimiter = $sValue[$iPosDelimiter];
            if (substr_count($sValue,$delimiter) > 1)
                $sValue = str_replace($delimiter,'',$sValue);

        }

        $sValue = str_replace(',','.',$sValue);

        return (float) $sValue;
        
    }

}