<?php

namespace skewer\components\search;

use skewer\base\ui\ARSaveException;
use skewer\components\search\models\SearchIndex;
use skewer\components\seo\Service;
use skewer\components\seo;
use skewer\base\site\Layer;
use yii\helpers\HtmlPurifier;

/**
 * Класс прототип для поисковых механизмов контентных сущностей
 */
abstract class Prototype {

    /**
     * @var string псевдоним поискового движка,
     *      который передается основным обработчиком при итерации
     *      поисковых записей.
     *      В большинстве случаев совпадает с ответом метода getName()
     */
    protected $sIncomingName = '';

    /**
     * отдает имя идентификатора ресурса для работы с поисковым индексом
     * @return string
     */
    abstract public function getName();

    /**
     * Отдает название модуля
     * @return string
     */
    public function getModuleTitle() {
        if ( !\Yii::$app->register->moduleExists($this->getName(), Layer::PAGE) )
            return '-';
        return \Yii::$app->register->getModuleConfig($this->getName(), Layer::PAGE)->getTitle();
    }

    /**
     * обновляет конкретную запись поисковой таблицы
     * * устанавливает status в 1
     * * обновляет поисковый текст
     * * обновляет заголовок
     * * обновляет url
     * * задает раздел
     * * выставляет остальные параметры на свое усмотрение
     * * сохраняет запись. если вернет false, то посиковая запись будет стерта из базы
     * @param $oSearchRow
     * @return boolean если возвращает false - запись будет удалена вышестоящим методом
     */
    abstract protected function update(SearchIndex $oSearchRow);

    /**
     *  воссоздает полный список пустых записей для сущности, отдает количество добавленных
     * @return int
     */
    abstract public function restore();

    /**
     * сбрасывает статус для всех записей с идентификатором модуля, отдает количество измененных
     * @return int
     */
    public function resetAll(){
        return SearchIndex::updateAll(['status'=>0],['class_name'=>$this->getName(),'status'=>1]);
    }

    /**
     * сбрасывает по id объекта в 0
     * @param int $id
     * @return int
     */
    public function resetToId($id){
        return SearchIndex::updateAll(['status'=>0],['class_name'=>$this->getName(),'object_id'=>$id]);
    }

    /**
     * удаляет все записи из поискового индекса, отдает количество измененных
     * @return int
     */
    public function deleteAll(){
        $res = SearchIndex::deleteAll(['class_name'=>$this->getName()]);
        Service::updateSiteMap();
        return $res;
    }

    /**
     * отдает число неактивных записей
     * @return int
     */
    public function getInactiveCount(){
        return SearchIndex::find()->where(['class_name'=>$this->getName(),'status'=>0])->count();
    }

    /**
     * обновляет/добавляет запись в поисковой таблице по id конкретной записи ресурса
     * выбирает / создает если нет поисковую запись по id и идентификатору ресурса
     * @param $iId
     * @param bool $bAddSitemapTask нужно ли ставить задачу на обновление sitemap?
     * @return bool
     * @throws ARSaveException
     */
    public function updateByObjectId($iId, $bAddSitemapTask = true){

        // найти запись
        $oSearchRow = SearchIndex::find()
            ->where(['class_name'=>$this->getName(),'object_id'=>$iId])
            ->one();

        // ... или создать новую
        if (!$oSearchRow){
            $oSearchRow = new SearchIndex;
            $oSearchRow->class_name = $this->getName();
            $oSearchRow->object_id = (int)$iId;
            $oSearchRow->save();
        }

        // выполнить конкретное действие по обновлению

        $res = $this->update($oSearchRow);
       
        // если метод вернул false, а запись есть в базе (задан id) - убрать из использования
//        var_dump($oSearchRow);
        if ( $res === false and $oSearchRow->id ) {
            $oSearchRow->use_in_search = 0; // не использовать при поиске
            $oSearchRow->use_in_sitemap = 0;// не использовать в sitemap
            $oSearchRow->status = 1;            // обработана
            $oSearchRow->save();
//            var_dump($oSearchRow);
        }

        if ( $oSearchRow->hasErrors() )
            throw new ARSaveException($oSearchRow);
        
        // если стоит флаг - поставить задачу на обновление sitemap
        if ($bAddSitemapTask)
            seo\Api::setUpdateSitemapFlag();

        return $res;

    }

    /**
     * удаляет запись по id и идентификатору ресурса
     * @param $iId
     * @return bool
     */
    public function deleteByObjectId($iId){
        $res = (boolean) SearchIndex::deleteAll(['class_name'=>$this->getName(),'object_id'=>$iId]);
        Service::updateSiteMap();
        return $res;
    }

    /**
     * Returns the fully qualified name of this class.
     * @return string the fully qualified name of this class.
     */
    public static function className() {
        return get_called_class();
    }

    /**
     * Задает имя под которым поисковая записб зарегистрирована в таблице
     * @param string $sName
     */
    public function provideName( $sName ) {
        $this->sIncomingName = $sName;
    }

    /**
     * Расширенная функция удаления тегов
     * Умеет стирать также теги типа style, script
     * @param $sText
     * @return string
     */
    protected function stripTags($sText) {
        return strip_tags( HtmlPurifier::process( $sText ) );
    }

    /**
     * Добавление задачи на обновление карты сайта
     */
    protected function updateSiteMap() {
        Service::updateSiteMap();
	}

    /**
     * Заполняет объект поисковой записи seo данными
     * @param SearchIndex $oSearchRow - поисковая запись
     * @param seo\SeoPrototype $oSeo - seo -компонент
     */
	protected function fillSearchRowSeoData( SearchIndex &$oSearchRow, seo\SeoPrototype $oSeo){

        // загружаем seo данные в компонент
        $oSeo->initSeoData();

        if ( $oSeo->none_index )
            $oSearchRow->use_in_sitemap = false;

        if ( $oSeo->none_search )
            $oSearchRow->use_in_search = false;

        $oSearchRow->priority = ($oSeo->priority)
            ? $oSeo->priority
            : $oSeo->calculatePriority();

        $oSearchRow->frequency = ($oSeo->frequency)
            ? $oSeo->frequency
            : $oSeo->calculateFrequency();

    }

}