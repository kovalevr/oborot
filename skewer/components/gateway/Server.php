<?php

namespace skewer\components\gateway;

use ErrorException;
use Exception;
use ReflectionClass;
use ReflectionException;
use ReflectionMethod;
use skewer\base\log\Logger;
use skewer\components\config\UpdateException;
use yii\web\ServerErrorHttpException;

/**
 * Класс, отвечающий за прием удаленных обращений
 */
class Server extends Prototype {

    /**
     * @const int Тип валидации на выполнение - по принадлежности классу
     */
    const ValidateTypeConcreteClass = 0x03;

    /**
     * @const int Тип валидации на выполнение - по родительскому классу
     */
    const ValidateTypeParentClass = 0x04;

    /**
     * @const int Тип валидации на выполнение - по соответствию интерфейсу
     */
    const ValidateTypeInterface = 0x05;

    /**
     * Текущий тип валидации метода класса на выполнение
     * @var int
     */
    private $iValidateType = 0x04;

    /**
     * Значение валидации класса в зависимости от настроек
     * @var string
     */
    protected $sValidateValue = '';

    /**
     * Обработчик пришедших заголовков
     * @var null|callable|callback
     */
    protected $mHeaderHandler = null;

    /**
     * Массив ответов по результатам выполнения запросов имеет следующий вид:
     * ['response'] = <результат работы метода>
     * ['error']    = экземпляр, сгенерированного исключения
     * @var array
     */
    protected $aResponse = array();

    /** @var bool Флаг успешной расшифровки пришедшего пакета */
    private $bPackageDecrypted = false;

    /**
     * Флаг критической ошибки произошедшей на сайте, выкинет исключение на СМС
     * @var bool
     */
    public static $bHaveCriticalError = false;

    /**
     * @param int $iStreamMode тип пакетов
     */
    public function __construct($iStreamMode = 0x00) {

        set_error_handler( array($this, 'error') );
        set_exception_handler( array($this, 'exception') );
        register_shutdown_function( array($this, 'fatalError') );

        $this->iStreamType = (int)$iStreamMode;
        return true;
    }

    /**
     * Указывает класс, методы потомков которого можно выполнять средствами протокола
     * @param string $sClassName
     */
    public function addParentClass($sClassName) {

        $this->sValidateValue = $sClassName;
        $this->iValidateType  = self::ValidateTypeParentClass;

    }// func

    /**
     * Указывает класс, методы которого можно выполнять средствами протокола
     * @param string $sClassName
     */
    public function addClass($sClassName) {

        $this->sValidateValue = $sClassName;
        $this->iValidateType  = self::ValidateTypeConcreteClass;

    }// func

    /**
     * Указывает интерфейс к которому должны пренадлежать классы, методы которых можно выполнять
     * @param string $sInterfaceName
     */
    public function addInterface($sInterfaceName) {

        $this->sValidateValue = $sInterfaceName;
        $this->iValidateType  = self::ValidateTypeInterface;

    }// func

    /**
     * Указыввает функцию обратного вызова для обработки пришедшего заголовка
     * @param callback|callable $mCalledMethod
     * @return bool
     */
    public function onLoadHeaderHandler($mCalledMethod) {

        if(!is_callable($mCalledMethod)) return false;

        $this->mHeaderHandler = $mCalledMethod;

        return true;
    }// func

    /**
     * Проверяет наличие строки запроса в массиве POST и возвращает ее либо
     * генерирует исключение типа gateway\Exception
     * @return string
     * @throws Exception
     */
    protected function getRequest() {

        if (!count($_POST)) throw new Exception('Request error: Empty Request!');

        if (!isSet($_POST['_gateway_request']) OR empty($_POST['_gateway_request']))
            throw new Exception('Request error: Wrong Request!');

        return $_POST['_gateway_request'];
    }// func

    protected function parseRequest($sRequest) {

        if (!function_exists('json_decode')) throw new Exception('Request error: JSON library not found!');

        $aPackage = json_decode(stripslashes($sRequest), true);

        if(!count($aPackage) OR
            !isSet($aPackage['Header']) OR
            !count($aPackage['Header']) OR
            !isSet($aPackage['Data'])
        )
            throw new Exception('Request error: Package has wrong format!');

        $this->aHeader = $aPackage['Header'];

        if(!isSet($this->aHeader['Client']) OR
            !$this->aHeader['Client'] OR
            !isSet($this->aHeader['Certificate']) OR
            !$this->aHeader['Certificate']
        )
            throw new Exception('Request error: Package has wrong Header (Not found required fields)!');

        call_user_func_array($this->mHeaderHandler, array(&$this, $this->aHeader));

        $aPackage['Data'] = $this->decryptData($aPackage['Data'], $aPackage['Header']['crypted']);

        $this->bPackageDecrypted = true;

        /** @todo Добавить проверку сертификата */

        //if(isSet($aPackage['Data']['Actions']))
        $this->aActions = (isSet($aPackage['Data']['Actions']) && count($aPackage['Data']['Actions']))? $aPackage['Data']['Actions']: null;

        $this->aFiles   = (isSet($aPackage['Data']['Files']) && count($aPackage['Data']['Files']))? $aPackage['Data']['Files']: null;

        if (!$this->aActions AND !$this->aFiles) throw new Exception('Request error: No actions & Files!');

    }// func

    /**
     * Проверяет правильность указания класса и метода для выполнения Согласно настройкам сервера. Их наличие и доступность.
     * Если метод является валидным с точки зрения настроек сервера, то происходит его выполнение.
     * @param array|callable|callback $aAction Вызываемые методы
     * @throws ExecuteException
     * @return bool Возвращает true если метод разрешен для выполнения либо false в случае ошибки
     */
    protected function executeAction($aAction) {

        /* Есть ли в массиве не менее двух параметров */
        if(!is_array($aAction) OR count($aAction) < 2) return false;

        /* Запись о классе есть и она не пустая */
        if (!isSet($aAction['Class']) OR empty($aAction['Class']))
            throw new ExecuteException('Error checking: Class [' . $aAction['Class'] . '] not received!');

        $sClass = $aAction['Class'];

        // todo пофиксить когда переведется все на namespace включая sms
        if ( preg_match( '/^(\w+)(Page|Tool|Adm)(Service)?$/i', $sClass, $aMatch ) ) {

            $sName = $aMatch[1];
            $sLayer = $aMatch[2];

            $sClass = 'skewer\\build\\'.$sLayer.'\\'.$sName.'\\Service';
        }

        /* Запись о методе есть и она не пустая */
        if (!isSet($aAction['Method']) OR empty($aAction['Method']))
            throw new ExecuteException('Error checking: Method [' . $aAction['Method'] . '] not received!');

        $sMethod = $aAction['Method'];


        $aParameters = array();

        /* Запись о параметрах есть и она не пустая */
        if(isSet($aAction['Parameters']) OR count($aAction['Parameters']))
            $aParameters = $aAction['Parameters'];

        /* В зависимости от типа валидации */
        switch($this->iValidateType) {

            /* Заглушка */
            case self::ValidateTypeConcreteClass:

                throw new ExecuteException('Error checking: Mode [concrete class] is not implemented!');
                break;

            /* По родительскому классу */
            case self::ValidateTypeParentClass:

                /* Пытаемся получить описание класса */
                $oCalledClass = new ReflectionClass($sClass);

                if (!($oCalledClass instanceof ReflectionClass)) throw new ExecuteException('Error checking: Class [' . $sClass . '] not found!');

                /* Запрашиваем родителя класса и проверяем на соответствие условию */
                if ($oCalledClass->getParentClass()->name != $this->sValidateValue)
                    throw new ExecuteException('Security error: Class [' . $sClass . '] not accessible!');

                break;

            /* Заглушка */
            case self::ValidateTypeInterface:

                throw new ExecuteException('Error checking: Mode [Interface] is not implemented!');
                break;

        }// case of validate type

        /* Проверяем наличие метода в классе */

        /* Пытаемся получить описание метода */
        $oCalledMethod = new ReflectionMethod($sClass, $sMethod);

        /* Метод найден */
        if (!($oCalledMethod instanceof ReflectionMethod)) throw new ExecuteException('Error checking: Method [' . $sMethod . '] in class [' . $sClass . '] not found!');

        /* И он публичный */
        if (!$oCalledMethod->isPublic()) throw new ExecuteException('Error checking: Method [' . $sMethod . '] in class [' . $sClass . '] not accessible!');

        /* Пытаемся выполнить */
        $mResponse = $oCalledMethod->invokeArgs(new $sClass(), $aParameters);

        return $mResponse;
    }// func

    /**
     * Последовательно выполнить все методы по запросу
     * @throws Exception
     * @return bool
     */
    protected function doActions() {

        if (!count($this->aActions)) throw new Exception('Execute error: Queue of actions is empty!');

        foreach ($this->aActions as $aAction) {
            try {

                $aResponse = array(
                    'response' => $this->executeAction($aAction),
                    'error'    => null,
                );
                $this->aResponse[] = $aResponse;

            } catch (ExecuteException $e) { /* До выполнения дело не дошло, отвалились по недопустимости вызова */

                $this->aResponse[] = array(
                    'response' => null,
                    'error'=> $e->getMessage(),
                );

                /*Передадим флаг критичности ошибки*/
                $this->aResponse[0]['critical_error'] = (int)self::$bHaveCriticalError;

            } catch (ReflectionException $e) { /* Пытались выполнить - что-то пошло не так */

                $this->aResponse[] = array(
                    'response' => null,
                    'error'=> $e->getMessage(),
                );
            }
        }// each method

        return true;
    }// func

    /**
     * @todo Реализовать обработку
     * Должен обрабатывать действия после загрузки файлов
     * @return bool
     */
    protected function doFiles() {


        if (!count($this->aFiles)) return false;

        return true;
    }// func

    /**
     * Собирает ответ с результатами обработки запроса. Шифорвание происходит в зависимости от режима
     * @param array $aHeader Массив заголовков, отправляемых в ответе
     * @param array $aResponse Массив ответов отработки запроса
     * @return string Возвращает строку ответа либо генерирует исключение
     * типа gateway\Exception в случае ошибки
     * @throws Exception
     */
    protected function doResponse($aHeader, $aResponse) {

        $aPackage['Header'] = $aHeader;

        $aResponse = json_encode($aResponse);
        $sClientHost = isset($this->aHeader['Client']) ? $this->aHeader['Client'] : '';
        $aPackage['Header']['Certificate'] = $this->makeCertificate($sClientHost, $aResponse);

        if ( $this->packageIsDecrypted() )
            $aResponse = $this->encryptData($aResponse, $aPackage['Header']['crypted']);

        if (empty($aResponse)) throw new Exception('Response error: Assembled response is empty!');

        $aPackage['Data'] = $aResponse;

        return json_encode($aPackage);
    }// func

    /**
     *  Слушаем на предмет запросов, выполняем, собираем ответ, отдаем
     * @throws Exception
     */
    public function handler() {

        try {

            $sRequest = $this->getRequest();
            $this->parseRequest($sRequest);

            $this->doFiles();
            $this->doActions();

            $aHeader = array(
                'Status'  => 200,
                'Version' => $this->aHeader['Version'],
            );

            echo $this->doResponse($aHeader, $this->aResponse);

        } catch (Exception $e) {

            $aHeader = array(
                'Status'  => 500,
                'Version' => $this->aHeader['Version'],
            );

            $aErrorResponse = array('error' => $e->getMessage());
            echo $t = $this->doResponse($aHeader, $aErrorResponse);
        }


    }// func

    /**
     * Внутренний обработчик перехваченных ошибок
     * @param $errNo
     * @param $errStr
     * @param $errFile
     * @param $errLine
     * @throws ServerErrorHttpException
     */
    public static function error($errNo, $errStr, $errFile, $errLine) {
        throw new ServerErrorHttpException(sprintf(
            '"%s(%s)" in %s:%d',
            $errStr,
            $errNo,
            $errFile,
            $errLine
        ), $errNo);
    }

    /**
     * Внутренний обработчик перехваченных фатальных ошибок
     * @throws ServerErrorHttpException
     */
    public function fatalError() {
        if(!is_null($e = error_get_last())){
            self::handleException(
                new ServerErrorHttpException(
                    sprintf(
                        '"%s(%s)" in %s:%d',
                        $e['message'],
                        $e['type'],
                        $e['file'],
                        $e['line']
                    )
                )
            );
        }
    }

    /**
     * Внутренний обработчик неперехваченных исключений
     * @param \Exception $e
     * @throws ErrorException
     * @throws Exception
     */
    public function exception(\Exception $e) {

        self::handleException($e);
    }

    public function handleException(\Exception $e) {

        Logger::error((string)$e);

        echo self::makeErrorPackge( $e->getMessage(), $e->getCode() );
        exit;

    }

    /**
     * Генерирует посылку с описанием ошибки
     * @param $sMessage
     * @param int $iCode
     */
    private function makeErrorPackge( $sMessage, $iCode=0 ) {

        //header($_SERVER['SERVER_PROTOCOL'] . ' 500 Internal Server Error', true, 500);

        $aHeader = array(
            'Status'  => 500,
            'Version' => isset($this->aHeader['Version']) ? $this->aHeader['Version'] : '1.0',
        );

        $aErrorResponse = array(
            'error' => $sMessage,
            'code' => $iCode
        );

        echo $t = $this->doResponse($aHeader, $aErrorResponse);

    }

    /**
     * Отдает флаг успешной расшифровки пришедшего пакета
     * @return boolean
     */
    public function packageIsDecrypted() {
        return $this->bPackageDecrypted;
    }


}// class
