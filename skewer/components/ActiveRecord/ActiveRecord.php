<?php
/**
 * Created by PhpStorm.
 * User: na
 * Date: 31.05.2017
 * Time: 9:23
 */
namespace skewer\components\ActiveRecord;

use skewer\base\log\Logger;

/**
 * Класс прослойка для БД.
 * Нужен чтобы писать в лог CRUD операции с конкретной таблицей
 * Class ActiveRecord
 * @package skewer\components\ActiveRecord
 */

class ActiveRecord extends \yii\db\ActiveRecord{

    /**
     * Флаг писать логи?
     * @var bool
     */
    private static $bWriteLogs = true;

    /**
     * Флаг логировать ли обновление записи
     * @var bool
     */
    protected static $bLogUpdate = true;

    /**
     * Флаг логированть ли создание записи
     * @var bool
     */
    protected static $bLogCreate = true;

    /**
     * Флаг логировать ли удаление записи
     * @var bool
     */
    protected static $bLogDelete = true;

    public function afterSave($insert, $changedAttributes) {

        $sCurrentTable = static::tableName();

        if ($insert)
            $sTitle = 'Добавление записи';
        else
            $sTitle = 'Изменение записи';

        if ((($insert && static::$bLogCreate) || static::$bLogUpdate) && self::$bWriteLogs)
            Logger::addToLog($sTitle,json_encode($this->getAttributes()),'DB ('.$sCurrentTable.')',4,Logger::logUsers);

        return parent::afterSave($insert,$changedAttributes);
    }

    public function afterDelete() {

        if (static::$bLogDelete) {
            $sCurrentTable = static::tableName();

            $sTitle = 'Удаление записи';

            if (self::$bWriteLogs)
                Logger::addToLog($sTitle, json_encode($this->getAttributes()), 'DB (' . $sCurrentTable . ')', 4, Logger::logUsers);
        }
    }

    public static function enableLogs(){
        self::$bWriteLogs = true;
    }

    public static function disableLogs(){
        self::$bWriteLogs = false;
    }

}