<?php

namespace skewer\components\design;

use skewer\base\orm\Query;
use skewer\base\section\Page;
use skewer\build\Adm\Slider\Banner;
use skewer\build\Adm\Slider\Slide;
use yii\helpers\ArrayHelper;


/**
 * Прототип для переключателя форм
 */
abstract class TplSwitchForm extends TplSwitchPrototype {

    /**
     * Отдает тип переключателя шаблонов
     * @return string
     */
    protected function getType() {
        return 'form';
    }


}