<?php

namespace skewer\components\fonts;

use yii\helpers\FileHelper;
use yii\web\AssetBundle;
use yii\helpers\Url;

class Asset extends AssetBundle {
    public $sourcePath = '@skewer/components/fonts/web/';

    public $css = [
        'css/fonts.css',
    ];

    public function publish($am)
    {

        if ($this->sourcePath !== null && !isset($this->basePath, $this->baseUrl)) {
            list ($this->basePath, $this->baseUrl) = $am->publish($this->sourcePath, $this->publishOptions);
        }

        if (!file_exists($this->basePath))
            mkdir($this->basePath);

        if (!file_exists($this->basePath.'/css'))
            mkdir($this->basePath.'/css');
        
        if (isset($this->basePath, $this->baseUrl) && ($converter = $am->getConverter()) !== null) {
            foreach ($this->css as $i => $css) {
                if (Url::isRelative($css)) {
                    $this->css[$i] = Api::convert($css,$this->basePath);
                }
            }
        }
    }
}