<?php

namespace skewer\components\fonts;

use skewer\base\site_module\Parser;
use skewer\base\SysVar;
use skewer\components\design\model\Params;
use Symfony\Component\Yaml\Yaml;

class Api {

    const NAME_SYSVAR_ACTIVE = 'Fonts.data';
    const NAME_SYSVAR_CSS = 'Fonts.css';

    const PATH_YML = 'components/fonts/files/fonts.yml';
    const PATH_FONTS = 'components/fonts/web/fonts/';

    public static $aFormatFile4Css = ['eot','woff','ttf'];

    /**
     * Получение список семейств шрифтов с их активностью
     * Обновление списка в админке
     * @return array
     */
    public static function addFromYmlIntoJson() {
        
        $aFontsJson = self::getJsonSysVar();
        $aFontsYml = self::getFontsYaml();
        
        $aJsonFamilyKeys = array_keys($aFontsJson);
        $aYmlFamilyKeys = array_keys($aFontsYml);

        //проверка отсутствия шрифта в массиве, полученном из Json строки
        foreach ($aFontsYml as $sFamilyKeyYml => $aFonts) {
            if (!in_array($sFamilyKeyYml,$aJsonFamilyKeys))
                $aFontsJson[$sFamilyKeyYml] = [];
        }

        //проверка отсутствия шрифта в массиве, полученном из Yml строки
        foreach ($aFontsJson as $sFamilyKeyJson => $aActiveFonts) {
            if (!in_array($sFamilyKeyJson,$aYmlFamilyKeys)) {
                //удаление кода не существующего шрифта из css файла
                foreach ($aActiveFonts as $sFontActive => $aDataFont) {
                    $sCssGenerate = self::getTempleteCss($aDataFont);
                    self::deleteCss($sCssGenerate);
                }
                //удаление из массива json не существующего шрифта
                unset($aFontsJson[$sFamilyKeyJson]);
            }
        }

        self::setFontsJson($aFontsJson);
    }

    /**
     * Получение список семейств шрифтов с их активностью
     * @return array
     */
    public static function getFamilyFonts() {
        
        $aFonts = self::getJsonSysVar();
        $aResult = [];
        if ($aFonts) {
            $i = 0;
            foreach ($aFonts as $sNameFamily => $aActive) {
                $aResult[$i]['name'] = $sNameFamily;
                $aResult[$i++]['active'] = ($aActive)?1:0;
            }
        }
        
        return $aResult;
    }

    /**
     * Получение списка шрифтов 
     * @param $sFamilyName - семейство шрифтов
     * @return array
     */
    public static function getCustomizeFonts($sFamilyName) {

        $aYMLDoc = self::getFontsYaml();
        $aActiveFonts = self::getJsonSysVar();
        $aActiveFonts = array_keys($aActiveFonts[$sFamilyName]);
        $aCustomizeFonts = [];
        if (isset($aYMLDoc[$sFamilyName])&&(isset($aYMLDoc[$sFamilyName]['types']))) {
            foreach ($aYMLDoc[$sFamilyName]['types'] as &$aValue) {
                $aValue['active'] = (in_array($aValue['name'],$aActiveFonts))?1:0;
                $aValue['parent'] = $sFamilyName;
                $aValue['folder'] = $aYMLDoc[$sFamilyName]['folder'];
            }
            $aCustomizeFonts = $aYMLDoc[$sFamilyName]['types'];
        }

        return $aCustomizeFonts;
    }

    /**
     * Изменение активности
     * @param $aCustomizeFont
     * @param bool $bDelete
     * @return bool
     */
    public static function changeActive($aCustomizeFont, $bActive = false) {
        
        $aFamilyFonts = self::getJsonSysVar();
        if ($bActive) {
            $aFamilyFonts[$aCustomizeFont['parent']][$aCustomizeFont['name']] = $aCustomizeFont;
        } else {
            unset($aFamilyFonts[$aCustomizeFont['parent']][$aCustomizeFont['name']]);
        }

        return self::setFontsJson($aFamilyFonts);
    }

    /**
     * Формирование Json по Yaml
     * @param string $sNameFamily - семейство шрифтов
     * @param string $aActiveFonts - активные шрифты - описание полное
     * @return bool
     */
    public static function setJsonSysVar($sNameFamily = '',$aActiveFonts ='') {

        $aYMLDoc = self::getFontsYaml();
        $aFamilyName = array_keys($aYMLDoc);
        $aResult = [];
        foreach ($aFamilyName as $sName) {
            $aResult[$sName] = ($sNameFamily == $sName) ? $aActiveFonts : [];
        }

        return self::setFontsJson($aResult);
    }

    /**
     * Формирование css файла с подключением активных шрифтов
     * @param $sInsert
     * @param $aFamilyFont
     * @return bool|string
     */
    public static function insertCss($sInsert,$aFamilyFont) {

        foreach (self::$aFormatFile4Css as $sFormat) {
            $sFileName = $aFamilyFont['filename'] . ".$sFormat";
            $sPath = RELEASEPATH . self::PATH_FONTS . $aFamilyFont['folder'] . "/$sFileName";
            if (!file_exists($sPath))
                return \Yii::t('fonts','not_file')."$sFileName";
        }

        $sCssData = SysVar::get(self::NAME_SYSVAR_CSS);
        if (!SysVar::set(self::NAME_SYSVAR_CSS, $sCssData.$sInsert . PHP_EOL))
            return \Yii::t('fonts','not_add_css');
        else
            return true;
    }

    /**
     * удаление файла
     * @return mixed
     */    
    public static function delCssSysVar() {
        return SysVar::del(self::NAME_SYSVAR_CSS);
    }

    /**
     * Очистка кода для неактивного шрифта
     * @param $sDelete
     * @return bool|string
     */
    public static function deleteCss($sDelete) {

        $sFile = SysVar::get(self::NAME_SYSVAR_CSS);
        $sReplace = str_replace($sDelete, '', $sFile);
        if (!SysVar::set(self::NAME_SYSVAR_CSS, $sReplace))
            return \Yii::t('fonts','not_add_css');
        else
            return true;
    }


    public static function getActiveFonts() {
        $aFamilyFonts = self::getJsonSysVar();
        $aActiveFonts = [];
        foreach ($aFamilyFonts as $aValue)
            $aActiveFonts = array_merge($aActiveFonts,$aValue);
        return $aActiveFonts;
    }

    public static function getTempleteCss($aFamilyFont) {
        $sTemplate = Parser::parseTwig('fonts.twig',$aFamilyFont,
            BUILDPATH . 'Tool/Fonts/templates');
        return $sTemplate;
    }
    /**
     * Получение данных о шрифтах из yaml файла
     * @return array
     */
    private static function getFontsYaml() {
        $aYMLDoc = Yaml::parse(RELEASEPATH . self::PATH_YML);
        $aYMLDoc = is_array($aYMLDoc) ? $aYMLDoc : [];
        return $aYMLDoc;
    }

    /**
     * Формирование json
     * @param $aFutureJson
     * @return bool
     */
    private static function setFontsJson($aFutureJson) {
        $sJson = json_encode($aFutureJson);
        return SysVar::set(self::NAME_SYSVAR_ACTIVE,$sJson);

    }

    /**
     * Получение json описания активных шрифтов
     * @return mixed
     */
    private static function getJsonSysVar() {
        $sJsonFormat = SysVar::get(self::NAME_SYSVAR_ACTIVE);
        return json_decode($sJsonFormat, true);
    }


    /**
     * Проверяет наобходимо ли пересобрать файл с целями
     * и отправляет на пересоздание
     * @param $asset
     * @param $basePath
     * @return string
     */
    public static function convert($asset, $basePath){

        $pos = strrpos($asset, '.');

        if ($pos !== false) {

            $ext = substr($asset, $pos + 1);

            if ($ext=='css'){

                $fileName = substr($asset,0,strlen($asset)-4);
                $newFileName = $fileName . '.compile.css';
                $fullFileName = $basePath . '/' . $newFileName;

                // перестраиваем если файла нет или нужно обновить его
                if ( !is_file($fullFileName) or \Yii::$app->assetManager->forceCopy ){
                    self::createScript($fullFileName);
                }

                return $newFileName;

            }
        }
        return $asset;
    }

    /**
     * Пересоздает файл с целями
     * @param $fullFileName
     */
    public static function createScript($fullFileName){

        $sData = SysVar::get(self::NAME_SYSVAR_CSS);
        $h = fopen($fullFileName, 'w');
        fwrite($h, $sData);
        fclose($h);
    }

    /**
     * Получить используемый шрифт
     * @return Params | null
     */
    public static function getUsedFont(){
        return Params::findOne(['name' => 'base.userbase.fontfamily.font-family']);
    }


}