<?php
/**
 * Created by PhpStorm.
 * User: na
 * Date: 06.06.2016
 * Time: 10:44
 */
namespace skewer\components\targets;

use skewer\components\forms\Entity;
use skewer\components\targets\models\Targets;
use skewer\components\targets\models\TargetSelectors;
use skewer\components\targets\types\Prototype;
use yii\helpers\ArrayHelper;

class Api{

    /**
     * Проверяет наобходимо ли пересобрать файл с целями
     * и отправляет на пересоздание
     * @param $asset
     * @param $basePath
     * @return string
     */
    public static function convert($asset, $basePath){

        $pos = strrpos($asset, '.');

        if ($pos !== false) {

            if (!file_exists($basePath))
                mkdir($basePath);

            $ext = substr($asset, $pos + 1);

            if ($ext=='js'){

                if (!file_exists($basePath.'/'.$ext))
                    mkdir($basePath.'/'.$ext);

                $fileName = substr($asset,0,strlen($asset)-4);
                $newFileName = $fileName . '.compile.js';
                $fullFileName = $basePath . '/' . $newFileName;

                // перестраиваем если файла нет или нужно обновить его
                if ( !is_file($fullFileName) or \Yii::$app->assetManager->forceCopy ){
                    self::createScript($fullFileName);
                }

                return $newFileName;

            }
        }
        return $asset;
    }

    /**
     * Пересоздает файл с целями
     * @param $fullFileName
     */
    public static function createScript($fullFileName){

        $aSelectors = TargetSelectors::find()
            ->asArray()
            ->all();

        $aTargets = Targets::find()
            ->all();

        $aTargets = ArrayHelper::index($aTargets,'name');

        $aOut['items'] = [];

        foreach ($aSelectors as &$item){
            /** @var Prototype $oType */
            $oType = Creator::getObject(ucfirst($item['type']),true);

            if (!isset($aOut['items'][$item['selector']]))
                $aOut['items'][$item['selector']] = [];

            if ($item['name']){
                $aOut['items'][$item['selector']][] = $oType::getTarget($aTargets[$item['name']]);
            }
        }

        $sData = \Yii::$app->getView()->renderFile(__DIR__. '/templates/targets.php',$aOut);
        $h = fopen($fullFileName, 'w');
        fwrite($h, $sData);
        fclose($h);
    }

    public static function checkDuplicate($sName){

        $aTarget = Targets::find()
            ->where(['name'=>$sName])
            ->one();

        return !is_null($aTarget);

    }

    /**
     * Вернет скрипт ричголов, подключаемый к формам
     * @param Entity $oForm - объект формы
     * @return string
     */
    public static function buildScriptTargetsInForm( Entity $oForm ){

        $aData = [];

        $sTarget = $oForm->getTarget();
        if ( $sTarget and Yandex::isActive() ) {
            $aData['yandexReachGoal'] = array(
                'target' => $sTarget
            );
        }

        $gTarget = $oForm->getTargetGoogle();

        /** @var Targets $oTarget */
        $aTargetAttrs = ($oTarget = Targets::findOne(['name'=>$gTarget])) ? $oTarget->getAttributes() : [];
        if ( $gTarget  ) {
            $aData['googleReachGoal'] = array(
                'target' => $gTarget,
                'category' => $aTargetAttrs['category']
            );
        }

        $sHtml = \Yii::$app->getView()->renderFile(__DIR__. '/templates/sendFormTargets.php', $aData);

        return $sHtml;
    }

}