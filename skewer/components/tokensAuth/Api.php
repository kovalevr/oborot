<?php
/**
 * Created by PhpStorm.
 * User: na
 * Date: 27.10.2016
 * Time: 15:46
 */
namespace skewer\components\tokensAuth;

use skewer\base\log\Logger;

class Api{


    private static $aQueryData;

    public static function checkUpdates(){

        self::$aQueryData = array(
            'cmd'=>'getVersion'
        );

        Response::$aData = json_decode(Gateway::getData(UrlHelper::getTokensUrl('get-version'),self::$aQueryData),true);

        if (isset(Response::$aData['content']) && Response::$aData['content']!=Config::$sVersion){
            echo 'Вы используете устаревшую версию sys.php';
            exit;
        }
    }

    public static function getSysVersion(){
        echo Config::$sVersion;
        exit;
    }

    /**
     * Подтягивание формы авторизации и ее отрисовка
     */
    public static function getToken(){

        $sReturnLink = Request::getValByKey('return_link','none');
        $_SESSION['need_access_to'] = str_replace('%23','#',$sReturnLink);

        self::$aQueryData = array(
            'cmd'=>'getToken',
            'redirect_link'=>'http://'.str_replace('www.','',$_SERVER['HTTP_HOST']).$_SERVER['SCRIPT_NAME'],
            'public_key'=>DB::getPublicKey(),
            'service_name'=>Config::$sServiceName,
            'site_type'=>'Canape3'
        );

        Response::$aData['mode'] = 'redirect';
        Response::$aData['redirect_link'] = UrlHelper::getTokensUrl('get-token');
        Response::$aData['params'] = self::$aQueryData;

    }

    /**
     * Получение от сервиса токенов уникального ключа, запись в SysVar и редирект на состояние отрисовки формы
     */
    public static function getKey(){
        self::$aQueryData = array(
            'cmd'=>'getKey',
            'site_url'=>str_replace('www.','',$_SERVER['HTTP_HOST']),
            'site_type'=>'Canape3'
        );

        Response::$aData['mode'] = 'redirect';
        Response::$aData['redirect_link'] = UrlHelper::getTokensUrl('get-key');
        Response::$aData['params'] = self::$aQueryData;

        Response::execute();

    }

    /**
     * Проверка токена через сервис токенов
     */
    public static function checkToken(){

        self::$aQueryData['cmd'] = 'checkToken';
        self::$aQueryData['token'] = Request::getValByKey('token','');
        self::$aQueryData['app_key'] = hash('sha512',DB::getAppKey().str_replace('www.','',$_SERVER['HTTP_HOST']));
        self::$aQueryData['user_ip'] = $_SERVER['REMOTE_ADDR'];
        self::$aQueryData['site_name'] = $_SERVER['HTTP_HOST'];

        Response::$aData = json_decode(Gateway::getData(UrlHelper::getTokensUrl('check-token'),self::$aQueryData),true);

        if (Response::$aData['content']=='1'){

            $iSessionId = Session::setSession(Response::$aData['auth_mode']);

            /*Запросим данные о последнем логине этого пользователя*/
            if (isset(Response::$aData['username']))
                $sUserName = Response::$aData['username'];

            $sUserEmail = '';
            if (isset(Response::$aData['email']))
                $sUserEmail = Response::$aData['email'];

            if (isset(Response::$aData['auth_mode']))
                $sAuthMode = Response::$aData['auth_mode'];

            if (isset($sUserName) && isset($sAuthMode) && $sUserName && $sAuthMode) {

                if ($iSessionId)
                    Logger::addNoticeReport('Вход через CanapeId', json_encode([
                        'username'=>$sUserName,
                        'email'=>$sUserEmail,
                        'auth_mode'=>$sAuthMode
                    ]), Logger::logUsers, 'Auth');

                self::getLastLoginData($sUserName);

                \Yii::$app->session->set('current_canape_id_login', [
                            'username'=>$sUserName,
                            'email'=>$sUserEmail,
                            'auth_mode'=>$sAuthMode
                        ]
                );
            }

            self::$aQueryData = array(
                'cmd'=>'setKill',
                'session_id'=>$iSessionId,
                'app_key'=>hash('sha512',DB::getAppKey().str_replace('www.','',$_SERVER['HTTP_HOST'])),
                'token'=>Request::getValByKey('token',''),
                'target_url'=>'http://'.str_replace('www.','',$_SERVER['HTTP_HOST'])
            );

            /*Отдадим сервису токенов ид сессии*/
            Response::$aData = json_decode(Gateway::getData(UrlHelper::getTokensUrl('set-kill'),self::$aQueryData),true);

            Response::$aData=[
                'mode'=>'redirect',
                'redirect_link'=>'/admin/'
            ];

            if (isset($_SESSION['need_access_to']) && $_SESSION['need_access_to']!=='none')
                Response::$aData['redirect_link'] = $_SESSION['need_access_to'];

        } else {

            self::getToken();
        }
    }

    public static function getLastLoginData($sUserName){

        self::$aQueryData = array(
            'cmd'=>'getLastLoginData',
            'app_key'=>hash('sha512',DB::getAppKey().str_replace('www.','',$_SERVER['HTTP_HOST'])),
            'token'=>Request::getValByKey('token',''),
            'site_name'=>$_SERVER['HTTP_HOST'],
            'username'=>$sUserName
        );

        /*Отдадим сервису токенов ид сессии*/
        Response::$aData = json_decode(Gateway::getData(UrlHelper::getTokensUrl('get-last-login-data'),self::$aQueryData),true);

        if (is_null(Response::$aData) || Response::$aData['time'] == 'firstVisit' ){
            \Yii::$app->session->set('last_canape_id_login', 'firstVisit');
        } else {
            if (Response::$aData)
                \Yii::$app->session->set('last_canape_id_login', Response::$aData);
        }
    }

    public static function setKey(){

        if (DB::getAppKey()=='no_key'){
            $aRequest = Request::getRequest();
            DB::setAppKey($aRequest['app_key']);
            DB::setPublicKey($aRequest['public_key']);
        }

        Api::getToken();
    }

    /**
     * Удаление сессии по ее ID
     */
    public static function killSession(){
        Session::unsetSession(Request::getValByKey('session_id',''));
        Response::$aData['content'] = 1;
    }

}