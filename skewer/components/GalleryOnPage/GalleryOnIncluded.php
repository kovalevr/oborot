<?php
/**
 * Created by PhpStorm.
 * User: na
 * Date: 20.03.2017
 * Time: 11:57
 */
namespace skewer\components\GalleryOnPage;

/**
 * Class GalleryOnIncluded
 * @package skewer\components\GalleryOnPage
 */
class GalleryOnIncluded extends FilePrototype{

    /**
     * @return string
     */
    public function getEntityName(){
        return 'Included';
    }
}