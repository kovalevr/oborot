<?php

namespace skewer\components\GalleryOnPage;


use yii\base\Exception;

class Api {

    /**
     * Список настраиваемых параметров
     * @var array
     */
    private static $aParams = [
        'items',
        'margin',
        'dots',
        'nav',
        'autoWidth',
        'responsive',
        'slideBy',
        'loop',
        'shadow'
    ];

    /**
     * Валидирует набор пришедших данных
     * @param $aData
     * @throws Exception
     */
    public static function validateData($aData){

        foreach (self::$aParams as $param) {
            if ($param === 'slideBy') continue;
            if (!isset($aData[$param]))
                throw new Exception('Not set param!');
        }

        if ($aData['items']<1)
            throw new Exception(\Yii::t('GalleryOnPage','items_fail'));

        if ($aData['margin']<0)
            throw new Exception(\Yii::t('GalleryOnPage','margin_fail'));

        if (is_null(json_decode($aData['responsive'])))
            throw new Exception(\Yii::t('GalleryOnPage','adaptive_fail'));
    }

    /**
     * Отдает настройки по имени режима
     * @param $sEntity
     * @param bool $bInJSON
     * @return string
     */
    public static function getSettingsByEntity($sEntity,$bInJSON = false){

        $sClassName = 'skewer\components\GalleryOnPage\GalleryOn'.$sEntity;
        /** @var \skewer\components\GalleryOnPage\Prototype $oGallery */
        $oGallery = new $sClassName();

        $aOutData = $oGallery->getSettings();

        /*Приведение по типам*/
        $aOutData['navText'] = false;
        $aOutData['margin'] = (int)$aOutData['margin'];
        $aOutData['items'] = (int)$aOutData['items'];

        /*Если НЕ работает модуль режима адаптивности, скроем параметр адаптивности*/
        $oInstaller = new \skewer\components\config\installer\Api();
        if (!$oInstaller->isInstalled('AdaptiveMode', \skewer\base\site\Layer::PAGE))
            unset($aOutData['responsive']);

        if ($bInJSON)
            return json_encode($aOutData);
        else
            return $aOutData;

    }

    public static function convertResponsive($aData){

        $aResponsiveOut = [];

        $aResponsiveIn = json_decode($aData,true);

        foreach ($aResponsiveIn as $key=>&$value){
            $aResponsiveOut[$key] = [
                'items'=>$value
            ];
        }

        return json_encode($aResponsiveOut);
    }


    public static function unConvertResponsive($aData){

        $aResponsiveOut = [];

        $aResponsiveIn = json_decode($aData,true);

        foreach ($aResponsiveIn as $key=>&$value)
            $aResponsiveOut[$key] = $value['items'];

        return json_encode($aResponsiveOut);
    }
}
