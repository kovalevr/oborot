<?php
/**
 * Created by PhpStorm.
 * User: na
 * Date: 20.03.2017
 * Time: 11:57
 */
namespace skewer\components\GalleryOnPage;
/**
 * Class GalleryOnRelated
 * @package skewer\components\GalleryOnPage
 */
class GalleryOnRelated extends FilePrototype{
    /**
     * @return string
     */
    public function getEntityName(){
        return 'Related';
    }

}