<?php
/**
 * Created by PhpStorm.
 * User: na
 * Date: 20.03.2017
 * Time: 11:56
 */
namespace skewer\components\GalleryOnPage;

/**
 * Class Prototype
 * @package skewer\components\GalleryOnPage
 */
abstract class Prototype {

    /**
     * @return mixed
     */
    abstract public function getSettings();

    /**
     * @return mixed
     */
    abstract protected function getDefaultValues();
}