<?php
/**
 * Created by PhpStorm.
 * User: na
 * Date: 20.03.2017
 * Time: 11:57
 */
namespace skewer\components\GalleryOnPage;

use skewer\base\SysVar;

/**
 * Class GalleryOnPage
 * @package skewer\components\GalleryOnPage
 */
class GalleryOnPage extends Prototype{

    /**
     * @return array|mixed
     */
    public function getSettings(){

        $sData = SysVar::get('GalleryOnPageSettings');

        if (is_null($sData)) return $this->getDefaultValues();

        return json_decode($sData,true);

    }

    /**
     * @return array
     */
    public function getDefaultValues(){
        return [
            'items'=>3,
            'slideBy'=>'page',
            'margin'=> 5,//TODO значение установлено наугад
            'nav'=>true,
            'dots'=>false,
            'autoWidth'=>true,
            'responsive'=>[
                0 =>    ['items'=> 1],
                768 =>  ['items' => 2],
                980 =>  ['items' => 3],
                1240 => ['items' => 4]
            ],
            'loop'=>true,
            'shadow'=>false
        ];
    }
}