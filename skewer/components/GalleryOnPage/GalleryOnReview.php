<?php
/**
 * Created by PhpStorm.
 * User: na
 * Date: 20.03.2017
 * Time: 11:57
 */
namespace skewer\components\GalleryOnPage;
/**
 * Class GalleryOnReview
 * @package skewer\components\GalleryOnPage
 */
class GalleryOnReview extends FilePrototype{
    /**
     * @return string
     */
    public function getEntityName(){
        return 'Review';
    }

    protected function getDefaultValues(){
        return [
            'items'=>3,
            'slideBy'=>1,
            'margin'=> 20,
            'nav'=>true,
            'dots'=>false,
            'autoWidth'=>false,
            'loop'=>true,
            'responsive'=>[
                0 =>    ['items'=> 1],
                640 =>  ['items'=> 2],
                768 =>  ['items' => 2],
                980 =>  ['items' => 3],
                1240 => ['items' => 3]
            ],
        ];
    }
}