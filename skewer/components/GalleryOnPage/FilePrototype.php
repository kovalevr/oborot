<?php
/**
 * Created by PhpStorm.
 * User: na
 * Date: 20.03.2017
 * Time: 11:56
 */
namespace skewer\components\GalleryOnPage;

/**
 * Class FilePrototype
 * @package skewer\components\GalleryOnPage
 */
class FilePrototype extends Prototype{

    /**
     * Отдает имя режима
     * @return string
     */
    public function getEntityName(){
        return 'FilePrototype';
    }

    /**
     * Отдает настройки для режима
     * @return array
     */
    public function getSettings(){

        $mData = $this->getFileData();

        if (!$mData) return $this->getDefaultValues();

        if (!isset($mData[$this->getEntityName()]) || !$mData[$this->getEntityName()])
            return $this->getDefaultValues();

        return $mData[$this->getEntityName()];

    }

    /**
     * Отдает данные из файла с настройками
     * @return bool|mixed
     */
    public function getFileData(){

        $sFilePath = WEBPATH.'files/gallery_on_page/gallery_settings.txt';

        if (!file_exists($sFilePath)) return false;

        $sData = file_get_contents($sFilePath);

        $aData = json_decode($sData,true);

        if (is_null($aData)) return false;

        return $aData;
    }

    /**
     * Дефолтные значения
     * @return array
     */
    protected function getDefaultValues(){
        return [
            'items'=>3,
            'slideBy'=>'page',
            'margin'=> 20,
            'nav'=>true,
            'dots'=>false,
            'autoWidth'=>false,
            'responsive'=>[
                0 =>    ['items'=> 1],
                768 =>  ['items' => 2],
                980 =>  ['items' => 3],
                1240 => ['items' => 4]
            ],
            'loop'=>false,
            'shadow'=>false
        ];
    }
}