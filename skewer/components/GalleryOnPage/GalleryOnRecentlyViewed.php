<?php

namespace skewer\components\GalleryOnPage;

/**
 * Class GalleryOnRecentlyViewed
 * @package skewer\components\GalleryOnPage
 */
class GalleryOnRecentlyViewed extends FilePrototype{

    /**
     * @return string
     */
    public function getEntityName(){
        return 'RecentlyViewed';
    }
}