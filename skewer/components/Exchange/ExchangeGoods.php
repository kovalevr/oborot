<?php

namespace skewer\components\Exchange;

use skewer\base\queue;
use skewer\base\queue\ar\Task;
use skewer\base\site_module\Request;
use skewer\base\SysVar;
use skewer\components\import\Api;
use skewer\components\import\ar;
use skewer\components\import\Config;
use yii\helpers\ArrayHelper;
use yii\helpers\FileHelper;
use skewer\components\import;

/**
 * Class ExchangeGoods
 * @link http://v8.1c.ru/edi/edi_stnd/131/ - Протокол обмена между системой "1С:Предприятие" и сайтом
 * @package skewer\components\Exchange
 */
class ExchangeGoods extends ExchangePrototype {

    /**
     * Прием файлов от 1с
     */
    protected function cmdFile(){

        if ( $bRes = $this->loadFileImport() ){
            $sResponse = "success";
        } else {
            $sResponse = "failure";
        }

        $this->sendResponse( $sResponse );
    }


    /**
     * Импорт товаров и цен
     */
   protected function cmdImport(){

       $aRes = $this->importGoodsAndPrices();

       if ( $aRes === false ){
           $sResponse = "failure\n" . iconv('utf-8', 'windows-1251', 'Ошибка запуска задачи');
       } elseif ( $aRes['repeat'] ){
           $sResponse = "progress\n" . iconv('utf-8', 'windows-1251', $aRes['message'] );
       } else{
           $sResponse = "success";
       }

       $this->sendResponse( $sResponse );

   }

    /**
     * Импорт товаров и цен
     * @return array | bool
     */
    private function importGoodsAndPrices(){

        $iCountIteration = SysVar::get('1c.counterIteration', 0);

        // Инициализация параметров до выполнения импорта
        if ( $iCountIteration == 0 )
            $this->startImport();

        SysVar::set('1c.counterIteration',++$iCountIteration);

        // Сообщение статуса выполнения операции для 1с
        $sMessageStatus = '';

        // Флаг необходимости повторить этот же запрос
        $bNeedRepeatRequest = false;

        // Результат работы метода
        $aOut = array( 'repeat' => &$bNeedRepeatRequest, 'message' => &$sMessageStatus );

        // флаг, указывающий на то, что импорт товаров и предложений выполнен
        $bImportCompleted = SysVar::get('1c.bImportCompleted', false);

        $sFileName = Request::getStr('filename');

        if ( strpos($sFileName, 'import') !== false ){
            $sMode = 'import';
        } elseif ( strpos($sFileName, 'offers') !== false ){
            $sMode = 'offers';
        } else {
            return $aOut;
        }

        $aCurrentTask = [];

        if ( !$bImportCompleted ){

            // 1. Проверка наличия необходимых шаблонов импорта
            /** @var ar\ImportTemplateRow $oImportTemplate */
            if ( !$oImportTemplate = self::getImportTemplateFor1c($sMode) )
                return $aOut;

            // 2. Установка путей к файлам импорта
            $sFullNameImportFile = $this->getFullNameCurrentFile($sFileName);
            self::replacePaths( $oImportTemplate, $sFullNameImportFile );

            $aCurrentTask = ( $sMode == 'import' )
                ? self::settingTaskImportGood()
                : self::settingTaskImportOffers()
            ;

        } else {
            $aCurrentTask = self::settingTaskDeleteFiles();
        }

        try{
            $aRes = queue\Task::runTask($aCurrentTask['configTask'], 0, true);
        }catch(\Exception $e){
            return false;
        }

        if ( in_array($aRes['status'], [queue\Task::stFrozen, queue\Task::stWait]) ){
            $sMessageStatus = ArrayHelper::getValue($aCurrentTask, 'header', 'Importing in Progress');
            $bNeedRepeatRequest = true;
        } else {

            if ( $bImportCompleted ){
                $this->finishImport();
            } elseif ( $sMode == 'offers' ){

                    $aMatch = [];
                    preg_match("{offers(\d*).xml}", $sFileName, $aMatch);

                    $iIndex = (int)(isset($aMatch[1])? $aMatch[1] : 0);

                    $iIndexLastOffersFile = count(FileHelper::findFiles( $this->getDirCurrentExchange(), ['only' => ['offers*.xml']] )) - 1;

                    // Если загружен последний файл offers, то импорт товаров/предложений считаем оконченным
                    if ( $iIndex == $iIndexLastOffersFile ){
                        SysVar::set('1c.bImportCompleted', true);
                        $bNeedRepeatRequest = true;
                    }

            }

        }

        return $aOut;

    }

    /** Старт импорта. Инициализация параметров */
    private function startImport(){
        SysVar::set('1c.bImportCompleted', false);
    }

    /** Конец импорта. Восстановление параметров */
    private function finishImport(){
        // сбрасываем счетчик итераций
        SysVar::set('1c.counterIteration',0);
    }

    /**
     * Получить шаблон импорта для обмена с 1с
     * @param string $type режим обмена
     * @return bool|\skewer\base\orm\ActiveRecord
     */
    private static function getImportTemplateFor1c( $type ){

        if ( $type == 'import' ){
            $sParamName = '1c.id_import_template_goods';
        } elseif ( $type == 'offers' ){
            $sParamName = '1c.id_import_template_prices';
        } else {
            return false;
        }

        if ( !($iId = SysVar::get($sParamName)) )
            return false;

        return  ar\ImportTemplate::find( $iId );

    }

    /**
     * Установка путей к файлам импорта(к самому файлу импорта + к директории с изображениями)
     * @param ar\ImportTemplateRow $oImportTemplate - шаблон импорта
     * @param string $sFullNameImportFile - полный путь к файлу импорта
     */
    private static function replacePaths(ar\ImportTemplateRow $oImportTemplate, $sFullNameImportFile ){

        // Подмена пути к директории с изображениями
        $oConfig = new Config( $oImportTemplate );
        $oConfig->setFieldsParam( ['params_gallery:imagedir' => str_replace(ROOTPATH, '', dirname($sFullNameImportFile)) ] );
        $oImportTemplate->settings = $oConfig->getJsonData();

        // Подмена пути к файлу обмена
        $oImportTemplate->source = str_replace(ROOTPATH, '', $sFullNameImportFile);
        $oImportTemplate->type = Api::Type_Path;
        $oImportTemplate->save();

    }

    /**
     * Конфигурация задачи "Импорт товаров"
     * @return array|bool
     */
    public static function settingTaskImportGood(){

        /** @var ar\ImportTemplateRow $oImportTpl*/
        if ( !$oImportTpl = self::getImportTemplateFor1c( 'import' ) )
            return false;

        return [
            'header'     => 'Импорт товаров в CMS',
            'configTask' => import\Task::getConfigTask($oImportTpl)
        ];
    }

    /**
     * Конфигурация задачи "Импорт предложений"
     * @return array|bool
     */
    public static function settingTaskImportOffers(){

        /** @var ar\ImportTemplateRow $oImportTpl*/
        if ( !$oImportTpl = self::getImportTemplateFor1c( 'offers' ) )
            return false;

        return [
            'header'      => 'Импорт предложений в CMS',
            'configTask'  => import\Task::getConfigTask($oImportTpl)
        ];
    }

    /**
     * Конфигурация задачи "Удаление файлов 1с"
     * @return array
     */
    public static function settingTaskDeleteFiles(){
        return [
           'header'     => 'Удаление файлов обмена',
           'configTask' => DeleteFileTask::getConfig()
        ];
    }

}