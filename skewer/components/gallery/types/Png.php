<?php
/**
 * Created by PhpStorm.
 * User: na
 * Date: 12.05.2017
 * Time: 14:37
 */

namespace skewer\components\gallery\types;

use skewer\components\gallery\Config;
use skewer\helpers\Image;

class Png extends Jpg{

    public function getCleanTpl($aParams){

        $oImg = parent::getCleanTpl($aParams);

        $oImg = $this->addTransparent($aParams,$oImg);

        return $oImg;
    }

    public function addTransparent($aParams, $rOut){
        
        imagealphablending($rOut, false);
        imagesavealpha($rOut, true);
        /*закрасим возможно прозрачным цветом*/
        $red = imagecolorallocatealpha($rOut,Image::$aColor['r'], Image::$aColor['g'], Image::$aColor['b'], 127);
        imagefill($rOut, 0, 0, $red);

        return $rOut;
    }

    public function createImg($oImg,$sFileName)
    {
        imagepng($oImg, $sFileName ? $sFileName : null);
    }

    public function createGD($sFileName)
    {
        $oTmp = imagecreatefrompng($sFileName);
        imagecolortransparent ( $oTmp , imagecolorallocate ( $oTmp , 0, 0 , 0 ) );

        // image in the form of black))
        imagealphablending($oTmp, false);

        // of transparency is preserved)
        imagesavealpha($oTmp, true);

        return $oTmp;
    }

    public static function getNumType(){
        return '3';
    }

    public function applyWaterMark($oImage,$aParams){

        if (!is_file($aParams['sPossibleFileName'])) return parent::applyWaterMark($oImage,$aParams);

        $rWatermarkImage = imagecreatefrompng($aParams['sPossibleFileName']);

        switch($aParams['iAlign']) {

            case Config::alignWatermarkTopLeft:

                $iX = $aParams['iMargin'];
                $iY = $aParams['iMargin'];

                break;
            case Config::alignWatermarkTopRight:

                $iX = $aParams['iCurrentWidth'] - $aParams['iWMWidth'] - $aParams['iMargin'];
                $iY = $aParams['iMargin'];

                break;
            case Config::alignWatermarkBottomLeft:

                $iX = $aParams['iMargin'];
                $iY = $aParams['iCurrentHeight'] - $aParams['iWMHeight'] - $aParams['iMargin'];

                break;

            default:
            case Config::alignWatermarkBottomRight:

                $iX = $aParams['iCurrentWidth'] - $aParams['iWMWidth'] - $aParams['iMargin'];
                $iY = $aParams['iCurrentHeight'] - $aParams['iWMHeight'] - $aParams['iMargin'];

                break;
            case Config::alignWatermarkCenter:

                $iX = ( $aParams['iCurrentWidth'] - $aParams['iWMWidth'] ) / 2;
                $iY = ( $aParams['iCurrentHeight'] - $aParams['iWMHeight'] ) / 2;

                break;

        }

        imagealphablending($oImage, true);
        imagecopy($oImage, $rWatermarkImage, (int)$iX, (int)$iY, 0, 0, $aParams['iWMWidth'], $aParams['iWMHeight']);
        imagealphablending($oImage, false);

        return $oImage;
    }

}