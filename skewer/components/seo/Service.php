<?php

namespace skewer\components\seo;


use skewer\base\queue as QM;
use skewer\base\section\Tree;
use skewer\base\site\Server;
use skewer\base\SysVar;
use skewer\build\Tool\RobotsTxt;
use skewer\components\search;
use skewer\base\site\ServicePrototype;
use skewer\base\site_module\Parser;
use skewer\base\section\models\TreeSection;
use yii\base\UserException;
use yii\helpers\ArrayHelper;
use skewer\components\search\models\SearchIndex;


/**
 * Сервис для работы с СЕО компонентами
 */
class Service extends ServicePrototype {

    /**
     * Флаг о том что был изменен алиас при сохранении.
     * @var bool
     */
    public static $bAliasChanged = false;

    /** @var array() Cлужебные слова */
    protected static $aFunctionSections = [
        '/design/',
        '/admin/',
        '/download/',
        '/cron/',
        '/contentgenerator/',
        '/gateway/',
        '/local/',
        '/assets/',
        '/files/',
        '/images/',
        '/sitemap_files/'
    ];

    /**
     * Очищаем поисковый индекс
     * todo перенести в поиск
     */
    public static function rebuildSearchIndex(){
        SearchIndex::deleteAll();

        $aResourseList = search\Api::getResourceList();

        foreach($aResourseList as $name => $item){
            /** @var search\Prototype $oEngine */
            $oEngine = new $item();
            $oEngine->provideName( $name );
            $oEngine->restore();
        }
    }

    /**
     * Формат например aData = [
     *                  'alias'=>'/test',
     *                  'class_name'=>'CatalogViewer',
     *                  'object_id'=>5
     *                  ]
     * @param array $aData
     * @return array
     */
     public static function findUrlCollisions($aData){

         /*Поиск по конкретной ссылке*/
         $sQuery =
             "
            SELECT href
            FROM search_index
            WHERE href='{$aData['alias']}'";

         /*Тут происходит исключение из выборки того объекта который проверям*/
         if (isset($aData['object_id']) && isset($aData['class_name']))
             $sQuery .= " AND (object_id!='{$aData['object_id']}' OR class_name!='{$aData['class_name']}')";

         $sQuery .= " GROUP BY href";


         $aCollisions = \Yii::$app->db->createCommand($sQuery)->queryAll();

         // служебные слова
         if (isset($aData['alias']) and  in_array($aData['alias'],self::$aFunctionSections)) {
             $aCollisions[]['href'] = $aData['alias'];
         }

        return $aCollisions;
    }

    /**
     * @param $sAlias
     * @param $iParent
     * @param $iId
     * @param $sEntity
     * @return bool true - есть коллизия / false - нет коллизии
     */
    public static function checkCollision($sAlias,$iParent,$iId,$sEntity){

        $sParentPath = Tree::getSectionAliasPath($iParent);

        $aData = [
            'alias'=>$sParentPath.$sAlias.'/',
            'class_name'=>$sEntity
        ];

        if ($iId)
            $aData['object_id']=$iId;

        $aCollisions = self::findUrlCollisions($aData);

        if (!empty($aCollisions)){
            self::$bAliasChanged = true;
            return true;
        }

        return false;

    }

    /**
     * Формирует уникальный алиас
     * @param $sAlias
     * @param $iId
     * @param $iParentId
     * @param $sEntity
     * @return bool|null|string
     * @throws UserException
     */
    public static function generateAlias($sAlias, $iId, $iParentId, $sEntity){

        if (!$sAlias)
            $sAlias = date('d-m-Y-H-i');

        // Счетчик итераций
        $iIteration = 1;

        // Мин.длина alias
        $iMinLengthAlias = 1;

        // Максимальная длина ссылки(alias_section + alias_entity)
        $iMaxLengthFullAlias = SearchIndex::getTableSchema()->getColumn('href')->size;

        // Флаг, указывающий что к alias'у ранее добавлялась цифра
        $bIsAddNumber = false;

        $sParentPath = Tree::getSectionAliasPath($iParentId);

        $iTmpLengthFullPath = mb_strlen( $sParentPath . $sAlias . '/');

        if ( $iTmpLengthFullPath > $iMaxLengthFullAlias ){

            if ( mb_strlen($sAlias) - ($iTmpLengthFullPath - $iMaxLengthFullAlias) < $iMinLengthAlias )
                throw new UserException( \Yii::t('tree', 'error_can_not_create_alias') );

            $sAlias = substr( $sAlias, 0,  mb_strlen($sAlias) - ($iTmpLengthFullPath - $iMaxLengthFullAlias));

        }

        // Работаем пока есть коллизия
        while( self::checkCollision($sAlias, $iParentId, $iId, $sEntity) ){

            if ( $iIteration >= 100 )
                throw new UserException( \Yii::t('tree', 'error_can_not_create_alias') );

            self::$bAliasChanged = true;

            $iLengthAlias = mb_strlen($sAlias);

            $iLengthFullPath = mb_strlen($sParentPath . $sAlias . '/');

            if ( $bIsAddNumber ){
                $iPrevIteration = $iIteration - 1;
                $sTmpTail = "-{$iPrevIteration}";
                $sAlias = substr($sAlias, 0, mb_strlen($sAlias) - mb_strlen($sTmpTail));
            }

            if ( $iLengthFullPath == $iMaxLengthFullAlias || ($iLengthFullPath == ($iMaxLengthFullAlias - 1)) ) {

                if ( !$bIsAddNumber ){

                    // Укорачиваем alias на 1 символ
                    if ( $iLengthAlias <= $iMinLengthAlias )
                        throw new UserException( \Yii::t('tree', 'error_can_not_create_alias') );

                    $sAlias = substr( $sAlias, 0, $iLengthAlias - 1 );

                } else {

                    // Если $iIteration увеличило количество разрядов (10->100, 100->1000)
                    if ( mb_strlen((string)$iIteration) > mb_strlen((string)($iIteration - 1)) ){

                        if ( mb_strlen($sAlias) <= $iMinLengthAlias )
                            throw new UserException( \Yii::t('tree', 'error_can_not_create_alias') );

                        $sAlias = substr( $sAlias, 0, mb_strlen($sAlias) - 1 );

                    }

                    // Добавляем в конец alias '-цифра'
                    $sAlias .= '-' . $iIteration;
                    $iIteration++;
                    $bIsAddNumber = true;
                }

            } else {

                // Добавляем в конец alias '-цифра' */
                $sAlias .= '-' . $iIteration;
                $iIteration++;
                $bIsAddNumber = true;
            }

        }

        return $sAlias;

    }

    /**
     * Обновляет поисковый индекс
     * @param $iTask
     * @throws \Exception
     * @return array
     */
    public static function makeSearchIndex( $iTask = 0 ){
        return QM\Task::runTask( SearchTask::getConfig(), $iTask, true );
    }

    /**
     * Постановка задачи на обновление search index
     * @static
     * @return bool
     */
    public static function updateSearchIndex() {
        return QM\Api::addTask( SearchTask::getConfig() );
    }


    /**
     * Постановка задачи на обновление sitemap.xml
     * @static
     * @return bool
     */
    public static function updateSiteMap() {
        return QM\Api::addTask( SitemapTask::getConfig() );
    }


    /**
     * Обновляет карту сайта
     * @param $iTask
     * @throws \Exception
     * @return array
     */
    public static function makeSiteMap( $iTask = false){
        return QM\Task::runTask( SitemapTask::getConfig(), $iTask, true );
    }


    public static function setNewDomainToSiteMap(){

        self::updateSiteMap();

        return true;
    }


    public static function updateRobotsTxt($sDomain){

        $out = self::getContentRobotsTxtFile( $sDomain );

        // -- save - rewrite file

        $filename = WEBPATH."robots.txt";

        if (file_exists($filename) && !is_writable($filename))
            throw new \Exception('Can\'t write robots.txt');

        if (!$handle = fopen($filename, 'w+'))   return false;

        if (fwrite($handle, $out) === FALSE)    return false;

        fclose($handle);

        return true;

    }

    /**
     * Получить содержимое robots.txt
     * (!!!контент, записываемый в файл при его перестроении, но не контент считанный из файла)
     * @param string - домен
     * @return string
     */
    public static function getContentRobotsTxtFile( $sDomain ){

        $aConfigPaths = \Yii::$app->getParam(['parser', 'default', 'paths']);
        if (is_array($aConfigPaths) && isset($aConfigPaths[0])) {
            $aConfigPaths = $aConfigPaths[0];
        }

        if ( ( RobotsTxt\Api::getSysVar('content_ovveriden', '0') === '1' ) ){

            $sRobotsRules = RobotsTxt\Api::getSysVar('robots_content', '');
            $sRobotsRules = $sRobotsRules . "\r\n" . Parser::parseTwig('robots_host.twig', [
                'site_url'  => WEBPROTOCOL . $sDomain,
                'site_host' => SysVar::get('enableHTTPS') ? WEBPROTOCOL . $sDomain : $sDomain
            ], $aConfigPaths);

            $sRobotsContent = $sRobotsRules;

        } else {

            $sRobotsContent = self::generateDefaultContentRobotsTxtFile( $sDomain ) ;

        }

        return $sRobotsContent;

    }

    /**
     * Пути системных разделов
     * @return array
     */
    private static function getSystemPaths()
    {
        $aPaths = [];
        $aServices = [];
        foreach(['search', 'card', 'auth', 'profile'] as $key)
            $aServices[$key] = \Yii::$app->sections->getValues($key);

        if ($aServices)
            $aPaths = TreeSection::find()
                ->where(['id' => $aServices])
                ->asArray()
                ->all();

        return ArrayHelper::map($aPaths, 'alias_path', 'alias_path');
    }

    /**
     * Сгенирирует содержимое (по умолчанию) файла robots.txt
     * @param string $sDomain  - домен сайта
     * @param bool $bOnlyRules - вернуть только правила (disallow/allow)
     * @return string
     */
    public static function generateDefaultContentRobotsTxtFile($sDomain, $bOnlyRules = false){

        // набор предустановленных в конфиге путей для парсинга
        $aConfigPaths = \Yii::$app->getParam(['parser', 'default', 'paths']);
        if (is_array($aConfigPaths) && isset($aConfigPaths[0])) {
            $aConfigPaths = $aConfigPaths[0];
        }

        $bExistDomain = false;

        if ($sDomain)
            $bExistDomain = true;

        if (!Server::isProduction())
            $bExistDomain = false;

        $aData = [
            'domain_exist' => $bExistDomain,
            'site_host'    => SysVar::get('enableHTTPS') ? WEBPROTOCOL . $sDomain : $sDomain,
            'site_url'     => WEBPROTOCOL . $sDomain,
            'pattern'      => Api::getRobotsPattern(),
            'system_service' => self::getSystemPaths(),
            'bOnlyRules'   => $bOnlyRules
        ];

        $sOut = Parser::parseTwig('robots.twig', $aData, $aConfigPaths);

        return $sOut;

    }

}
