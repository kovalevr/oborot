<?php
/**
 * Created by PhpStorm.
 * User: Александр
 * Date: 26.05.2016
 * Time: 10:04
 */

namespace skewer\components\catalog\field;


class Html extends Prototype{

    /**
     * @inheritdoc
     */
    protected function build($value, $rowId, $aParams) {
        return [
            'value' => $value,
            'html' => \skewer\helpers\Html::hasContent($value) ? $value : ''
        ];
    }
}