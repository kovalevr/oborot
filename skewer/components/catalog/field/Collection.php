<?php

namespace skewer\components\catalog\field;

use skewer\base\section\models\ParamsAr;
use skewer\components\catalog\Section;
use yii\base\UserException;
use yii\helpers\ArrayHelper;
use skewer\components\catalog\Parser;

class Collection extends Prototype {

    protected $subSection = 0;


    protected function build( $value, $rowId, $aParams ) {

        $item = $this->getSubDataValue( $value );
        $itemTitle = ArrayHelper::getValue( $item, 'title', '' );
        $itemAltTitle = ArrayHelper::getValue( $item, 'alt_title', '' );
        $itemAlias = ArrayHelper::getValue( $item, 'alias', '' );

        $iSectionId = $this->subSection;

        $href = Parser::buildUrl($iSectionId, $value, $itemAlias);

        $out = $href ? ' <a href="'. $href .'">' . $itemTitle . '</a>' : $itemTitle;
        $sTitle = ($itemAltTitle)?$itemAltTitle:$itemTitle;
        $outAlt = $href ? ' <a href="'. $href .'">' . $sTitle . '</a>' : $itemTitle;

        return [
            'value' => $value,
            'item' => $item,
            'html' => $out,
            'htmlAlt' => $outAlt
        ];
    }


    protected function load() {

        // todo перенести это в кэш
        // fixme не решена проблема с одноименными полями в разных карточках

        $this->subSection = Section::get4CollectionField( $this->name );

    }

    /**
     * @param $oField
     * @throws UserException
     */
    public static function validateFieldDelete($oField){

        $iLinkId = $oField->link_id;

        $iCountUses = ParamsAr::find()
            ->where([
                'value'=>$iLinkId.':'.$oField->name,
                'name'=>'collectionField',
            ])
            ->count();

        if ($iCountUses)
            throw new UserException(\Yii::t( 'catalog', 'field_collection_in_use'));

    }

}