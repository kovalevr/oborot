<?php

namespace skewer\components\catalog\field;


class Money extends Prototype {

    protected function build( $value, $rowId, $aParams ) {
        $out = ($value == (int)$value) ? (int)$value : $value;
        // todo здесь если потребуется поменять тип разделителя
        return [
            'value' => $out,
            'value_full' => $value,
            'editor' => ['minValue' => 0],
            'html' => $out
        ];
    }
}