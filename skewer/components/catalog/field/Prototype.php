<?php

namespace skewer\components\catalog\field;

use skewer\base\ft;
use skewer\base\log\Logger;
use skewer\components\catalog\Filters;
use skewer\components\catalog\model\EntityTable;
use skewer\components\catalog\model\FieldTable;
use yii\helpers\ArrayHelper;
use skewer\base\ft\Editor;
use skewer\components\catalog\Card;

/**
 * Прототип для типизированного объекта парсинга поля сущности
 * Class Prototype
 * @package skewer\components\catalog\field
 */
abstract class Prototype {

    protected $type = '';

    protected $name = '';

    protected $title = '';

    protected $card = '';

    protected $attr = [];

    protected $widget = '';


    protected $subEntity = '';

    protected $subData = []; // todo перенести в Cache


    /** @var ft\model\Field */
    protected $ftField = null;


    public function getName() {
        return $this->name;
    }


    /**
     * Создание поля для парсера на основе ft-поля модели
     * @param ft\model\Field $field
     * @return Prototype
     */
    public static function init( $field ) {

        $sEditorName = $field->getEditorName();

        // для php 7.0
        if ( in_array($sEditorName, ['string']) )
            $sEditorName .= 'Field';

        $class = __NAMESPACE__ . '\\' . ucfirst($sEditorName);

        $obj = class_exists( $class ) ? new $class() : new StringField();

        $obj->ftField = $field;
        $obj->type = $field->getEditorName();
        $obj->name = $field->getName();
        $obj->title = $field->getTitle();
        $obj->card = $field->getModel()->getName();
        $obj->attr = $field->getAttrs();
        $obj->widget = $field->getWidgetName();

        $obj->load();
        $obj->loadSubData();

        return $obj;
    }


    protected function load() {

    }


    protected function loadSubData() {

        $oModel = ft\Cache::get( $this->card );

        if ( !$oModel )
            return;

        if ( $oRel = $oModel->getOneFieldRelation( $this->name ) ) {

            $this->subEntity = $oRel->getEntityName();

            $query = ft\Cache::getMagicTable( $this->subEntity )->find()->asArray();
            $idEntity = EntityTable::getByName($this->subEntity)->id;
            // Сортировка значений справочника мультисписка
            if ($this->type == Editor::MULTISELECT)
                $query->order(Card::FIELD_SORT);

            while ( $row = $query->each() ) {
                $this->subData[$row['id']] = $row;
                if ($this->type == Editor::SELECT)
                    $this->subData[ $row['id'] ] = $this->getRecursionData($this->subData[ $row['id'] ], $idEntity,1,[$idEntity]);
            }

        }
    }

    /**
     * Рекурсивная функция получения наследованных сущностей
     * @param $aSubData - данные для разбора на наличие сущностей
     * @param $idEntity - id сущности
     * @param int $countDeep - количество спусков
     * @param array $aDeepValidId - массив проходящих сущностей
     * @return mixed
     * @throws \Exception
     */
    private function getRecursionData($aSubData, $idEntity, $countDeep = 1, $aDeepValidId) {
        foreach ($aSubData as $sNameField =>$sValue) {
            $aFieldData = FieldTable::findOne(['name' => $sNameField, 'entity' => $idEntity]);
            if ($aFieldData && ($aFieldData->editor == Filters::TYPE_SELECT) && $aFieldData->link_id) {
                $sLinkId = $aFieldData->link_id;
                $sNameSubEntity = ft\Cache::get($sLinkId)->getName();
                $aSubTable = ft\Cache::getMagicTable($sNameSubEntity)->find()->where(['id' => $sValue])->asArray()->getOne();
                //логирование циклов
                if (in_array($sLinkId,$aDeepValidId)) {
                    Logger::addToLog(\Yii::t('dict','err_dict'),\Yii::t('dict','err_dict_cycle'), get_class()."::getRecursionData()",3,Logger::logUsers);
                    return $aSubData;
                }

                $aDeepValidId[] = $sLinkId;
                $aSubData[$sNameField] = (($aSubTable)&&($countDeep < dictInDict))
                    ? $this->getRecursionData($aSubTable, $sLinkId, ++$countDeep,$aDeepValidId) : ($aSubTable);
            }
        }
        return $aSubData;
    }


    protected function getSubDataValue( $key ) {
        return ArrayHelper::getValue( $this->subData, $key, false );
    }


    /**
     * Классовая функция преобразования значения
     * @param string $value значение поля из записи
     * @param int $rowId идентификатор записи
     * @param array $aParams Дополнительные параметры
     * @return mixed
     */
    abstract protected function build( $value, $rowId, $aParams );


    /**
     * Функция преобразования значения
     * @param string $value значение поля из записи
     * @param int $rowId идентификатор записи
     * @param array $aParams Дополнительные параметры
     * @return mixed
     */
    public function parse( $value, $rowId, $aParams = [] ) {

        $out = $this->build( $value, $rowId, $aParams );

        $out['attrs'] = $this->attr;
        $out['name'] = $this->name;
        $out['card'] = $this->card;
        $out['title'] = $this->title;
        $out['type'] = $this->type;
        $out['widget'] = $this->widget;

        // ед измерения
        if ( isSet($out['attrs']['measure']) && $out['attrs']['measure'] && $out['html'] ) {
            $out['measure'] = $out['attrs']['measure'];
            $out['html'] .= ' ' . $out['attrs']['measure'];
        }

        return $out;
    }

    /**
     * Действие, выполняемое после парсинга товара
     * @param array $aGoodData  - данные товара
     * @param array $aFieldData - данные поля
     * @return null
     */
    public function afterParseGood( $aGoodData, $aFieldData ){
        return null;
    }
}