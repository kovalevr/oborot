<?php

namespace skewer\components\catalog\field;


class Datetime extends Prototype {

    protected function build( $value, $rowId, $aParams ) {
        return [
            'value' => ( (int)$value ) ? $value : '',
            'html' => $value ? date( 'd.m.Y H:i', strtotime( $value ) ) : ''
        ];
    }
}