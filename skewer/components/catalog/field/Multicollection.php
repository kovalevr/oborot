<?php

namespace skewer\components\catalog\field;

use skewer\base\section\models\ParamsAr;
use skewer\components\catalog\Parser;
use skewer\components\catalog\Section;
use yii\base\UserException;


class Multicollection extends Collection {

	protected $subSection = 0;

    /**
     * @param string $values
     * @param int $rowId
     * @param array $aParams
     * @return array
     */
	protected function build( $values, $rowId, $aParams ) {

		if (is_string($values) and empty($values))
			$values = $this->ftField->getLinkRow( $rowId );

		if (!is_array($values))
			$values = explode( ',', $values );

        $items = [];
		foreach ($values as $value) {
            $items[] = $this->getSubDataValue($value);
		}

		$aOut = [];
		foreach ($items as &$item){

			$iSectionId = $this->subSection;

			$href = Parser::buildUrl($iSectionId, $rowId, $item['alias']);
			$sAltTitle =(isset($item['alt_title'])&&$item['alt_title'])?$item['alt_title']:$item['title'];
			$item['html'] = $href ? ' <a href="'. $href .'">' . $sAltTitle . '</a>' : $sAltTitle;
			$aOut[] = $href ? ' <a href="'. $href .'">' . $sAltTitle . '</a>' : $sAltTitle;
		}

		return [
            'value' => implode(',',$values),
            'item'  => $items,
            'html'  => implode(',',$aOut)
        ];

	}

}