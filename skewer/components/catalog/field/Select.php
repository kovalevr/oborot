<?php

namespace skewer\components\catalog\field;

use yii\helpers\ArrayHelper;


class Select extends Prototype {


    protected function build( $value, $rowId, $aParams ) {

        $item = $this->getSubDataValue( $value );

        return [
            'value' => $value,
            'item' => $item,
            'html' => ArrayHelper::getValue( $item, 'title', '' )
        ];
    }

}