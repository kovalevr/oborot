<?php

namespace skewer\components\catalog\model;

use skewer\base\orm\ActiveRecord;
use skewer\base\SysVar;


/**
 * Запись атрибута для поля сущности
 * Class FieldAttrRow
 * @package skewer\components\catalog\model
 */
class FieldAttrRow extends ActiveRecord {

    public $id = 0;
    public $field = 0;
    public $tpl = 0;
    public $value = '';


    /**
     * Отдает имя таблицы
     * @return string
     */
    public function getTableName() {
        return 'c_field_attr';
    }

    public function preSave(){
        \Yii::$app->router->updateModificationDateSite();
    }


    public function preDelete(){
        \Yii::$app->router->updateModificationDateSite();
    }
    

}