<?php

namespace skewer\components\catalog;

use skewer\base\SysVar;
use skewer\build\Catalog\Goods\model\GoodsEditor;
use skewer\build\Catalog\Goods\Search;
use skewer\base\ft;
use skewer\base\orm\Query;
use skewer\base\orm\ActiveRecord;
use skewer\components\catalog;
use skewer\components\import\field\Section as ImportSection;
use skewer\helpers\Transliterate;
use \yii\base\ModelEvent;
use skewer\build\Catalog\Goods\model\ModGoodsEditor;
use skewer\components\gallery\Album;
use skewer\components\rating\Rating;
use skewer\build\Page\CatalogViewer;

/**
 * класс для работы с товаром, состоящим из нескольких карточек
 * @package skewer\build\libs\Catalog\model
 */
class GoodsRow extends GoodsRowPrototype {


    /**
     * Создание нового товара
     * @param $card
     * @return GoodsRow
     * @throws ft\Exception
     */
    public static function create( $card ) {

        $oGoodsRow = new self();

        $sBaseCard = Card::getBaseCard( $card );

        // base row
        $oBaseModel = ft\Cache::get( $sBaseCard );
        $oBaseRow = ActiveRecord::getByFTModel( $oBaseModel );

        $oGoodsRow->setBaseRow( $oBaseRow );
        $oGoodsRow->setBaseCardName( $sBaseCard );

        // ext row
        $oExtModel = ft\Cache::get( $card );
        $oExtRow = ActiveRecord::getByFTModel( $oExtModel );

        $oGoodsRow->setExtRow( $oExtRow );
        $oGoodsRow->setExtCardName( $card );

        return $oGoodsRow;
    }


    /**
     * Получить объект товара по id
     * @param $id
     * @param string $card
     * @return GoodsRow|bool
     * @throws \Exception
     * @throws ft\Exception
     * todo обработка карточки
     */
    public static function get($id, /** @noinspection PhpUnusedParameterInspection */
                               $card = '' ) {

        $oGoodsRow = new self();

        if ( ! ( $row = catalog\model\GoodsTable::get( $id ) ) )
            return false;

        $oModel = ft\Cache::get( $row['base_card_name'] );

        if ( !$oModel )
            throw new \Exception('Не найдена модель для карточки.');

        $oBaseRow = Query::SelectFrom( $oModel->getTableName(), $oModel )
            ->where( $oModel->getPrimaryKey(), $id )
            ->getOne();

        $oGoodsRow->setRowId( $id );
        $oGoodsRow->setBaseRow( $oBaseRow );
        $oGoodsRow->setBaseCardName( $row['base_card_name'] );

        // ext row
        $oExtModel = ft\Cache::get( $row['ext_card_name'] );
        $oExtRow = Query::SelectFrom( $oExtModel->getTableName(), $oExtModel )
            ->where( $oBaseRow->primaryKey(), $oBaseRow->getPrimaryKeyValue() )
            ->getOne();

        $oGoodsRow->setExtRow( $oExtRow );
        $oGoodsRow->setExtCardName( $row['ext_card_name'] );

        $oGoodsRow->setMainRowId( $row['parent'] );

        $oGoodsRow->fillLinkFields();

        return $oGoodsRow;
    }


    public static function getByAlias( $alias, $card = '' ) {

        if ( !($oModel = ft\Cache::get( $card )) )
            throw new \Exception('Не найдена модель для карточки.');

        if ( !($oBaseRow = Card::getItemRow( $card, ['alias' => $alias] )) )
            return false;

        return self::get( $oBaseRow->getPrimaryKeyValue(), $card );
    }


    public static function getByFields( $aFields, $card = '' ) {

        if ( !($oModel = ft\Cache::get( $card )) )
            throw new \Exception('Не найдена модель для карточки.');

        if ( !($oBaseRow = Card::getItemRow( $card, $aFields )) )
            return false;

        return self::get( $oBaseRow->getPrimaryKeyValue(), $card );
    }


    /**
     * Изменение значений полей товара
     * @param array $aData
     */
    public function setData( $aData ) {

        //Если нет алиаса но есть title
        if (isset($aData['title']) && !isset($aData['alias']))
            $aData['alias'] = $aData['title'];

        // генерация и сохранение алиаса
        if ( $this->oBaseRow->getModel()->hasField( 'alias' ) && isset($aData['alias']) )
            $this->checkAlias( $aData );


        // перебираем все пришедшие данные
        foreach ( $aData as $sKey => $mVal ) {

            if ( $this->oBaseRow->getModel()->hasField($sKey) )
                $this->oBaseRow->$sKey = $mVal;

            if ( $this->oExtRow and $this->oExtRow->getModel()->hasField($sKey) )
                $this->oExtRow->$sKey = $mVal;

        }

    }


    /**
     * Получение значений полей товара
     * Данные базвовой как есть,
     * @return array
     */
    public function getData() {

        $aData = array();

        if ( $this->hasBaseRow() )
            $aData = $this->getBaseRow()->getData();

        if ( $this->hasExtRow() ) {
            foreach ( $this->getExtRow()->getData() as $sKey => $mVal ) {
                $aData[ $sKey ] = $mVal;
            }
        }

        return $aData;
    }


    /**
     * Список разделов для показа
     * @return array|bool
     */
    public function getViewSection() {

        return Section::getList4Goods( $this->oBaseRow->getPrimaryKeyValue(), $this->getBaseCardId() );
    }


    /**
     * Актуализация связей товара с разделами и возвращает основной раздел
     * @param array $aSectionList
     * @return bool|int
     */
    public function setViewSection( $aSectionList = array() ) {
        Section::save4Goods( $this->getRowId(), $this->getBaseCardId(), $this->getExtCardId(), $aSectionList );
        return $this->getMainSection();
    }


    /**
     * Получение/установка основного раздела для товара
     * @return bool|int
     */
    public function getMainSection() {
        return Section::getMain4Goods( $this->getRowId(), $this->getBaseCardId() );
    }


    /**
     * Обновление главного раздела для товара
     * @param $iSectionId
     * @return bool
     */
    public function setMainSection( $iSectionId ) {
        return catalog\model\GoodsTable::setMainSection( $this->getRowId(), $iSectionId );
    }


    /**
     * Сохранение товара
     * @return bool
     */
    public function save() {

        $bNew = !$this->iRowId;

        if ( !parent::save() )
            return false;

        if ( $bNew ) {
            $this->createLink();
        } else {

            if ($this->wasUpdated()) 
                $this->setUpdDate();

            if (SysVar::get('catalog.goods_modifications') && $this->isMainRow()) {
                // для модификаций сохраняем не уникальные поля
                $data = [];
                $values = $this->getData();
                unSet( $values['id'] );
                foreach ( $this->getFields() as $oFtField ) {
                    $sFieldName = $oFtField->getName();
                    if ( !$oFtField->getAttr( 'is_uniq' ) && isSet($values[$sFieldName]) )
                        $data[$sFieldName] = $values[$sFieldName];
                }

                if ( count($data) ) {
                    $list = catalog\GoodsSelector::getModificationList( $this->getRowId() )->getArray( $count );
                    foreach ( $list as $row ) {
                        $goods = self::get( $row['id'] );

                        /*Если для модификации уже задан алиас, используем его*/
                        if (isset($row['alias'])) $data['alias'] = $row['alias'];

                        $goods->setData( $data );
                        $goods->save();
                        // обновление поискового индекса
                        $oSearch = new Search();
                        $oSearch->updateByObjectId( $row['id'] );
                        // todo прокинуть сообщение об ошибке $goods->getErrorList()
                    }

                }
            }


        }

        return true;
    }


    /**
     * Удаление товара
     */
    public function delete() {

        if ( !$this->iRowId )
            return false;

        if ( !parent::delete() )
            return false;

        // Удаление модификаций
        if ($this->isMainRow())
            ModGoodsEditor::multipleDeleteAll($this->iRowId);

        /* Удаление галерей товара */
        $aData = $this->getData();
        foreach ($this->getFields() as $sFieldName => $oField)
            // Если товар не модификация или у модификации своя галерея (ps модификация может иметь одну галерею с товаром)
            if ($this->isMainRow() or $oField->getAttr('is_uniq'))
                if ($oField->getEditorName() == ft\Editor::GALLERY) {
                    $iAlbumId = isset($aData[$sFieldName]) ? $aData[$sFieldName] : 0;
                    $iAlbumId and Album::removeAlbum($iAlbumId, $error);
                }

        // Удаление рейтинга к товару
        (new Rating(CatalogViewer\Module::getNameModule()))->removeRating($this->iRowId);

        catalog\model\SemanticTable::remove( $this->iRowId, $this->getBaseCardId() );
        $this->setViewSection( array() );
        $this->removeLink();

        return true;
    }


    private function setUpdDate() {
        catalog\model\GoodsTable::setChangeDate( $this->iRowId, $this->getBaseCardId() );
    }


    private function createLink() {
        catalog\model\GoodsTable::add(
            $this->iRowId,
            $this->getBaseCardId(),
            $this->getBaseCardName(),
            $this->getExtCardId(),
            $this->getExtCardName(),
            $this->isMainRow() ? $this->getRowId() : $this->getMainRowId()
        );
    }


    private function removeLink() {
        catalog\model\GoodsTable::remove( $this->iRowId, $this->getBaseCardId() );
    }


    protected function checkAlias( &$aData ) {

        $sAlias = isSet($aData['alias']) ? $aData['alias'] : $this->getBaseRow()->getVal( 'alias' );
        $sTitle = isSet($aData['title']) ? $aData['title'] : $this->getBaseRow()->getVal( 'title' );

        if ( $this->iRowId ) {
            $sOldTitle = $this->getBaseRow()->getVal( 'title' );
            $sOldAlias = $this->getBaseRow()->getVal( 'alias' );
            // Alias должен создаваться заново при следующих условиях:
            // 1. title изменен
            // 2. alias задан и не изменен при редактировании и не изменен при сохранении
            // 3. старый title при транслитерации совпадает с alias
            if ( $sOldTitle != $sTitle && $sOldAlias == $sAlias ) {
                $oldAlias = Transliterate::generateAlias( $sOldTitle );
                if ( $sOldAlias == $oldAlias || ( strpos($sOldAlias,$oldAlias)!==false ) )
                    $sAlias = $sTitle;
            }
        }

        if ( !$sAlias )
            $sAlias = $sTitle;

        $sAlias = Transliterate::generateAlias( $sAlias );

        if (isset($aData[GoodsEditor::mainLinkField]))
            /*Стандартное сохранение*/
            $sAlias = \skewer\components\seo\Service::generateAlias($sAlias,$aData['id'],$aData[GoodsEditor::mainLinkField],'CatalogViewer');
        elseif (ImportSection::$iCurrentSection)
            /*Сохранение через импорт*/
            $sAlias = \skewer\components\seo\Service::generateAlias($sAlias,0,ImportSection::$iCurrentSection,'CatalogViewer');
        elseif (isset($aData[GoodsEditor::categoryField])) {
            /*Не импортный товар и не установлен основной раздел*/
            /*Выдернем категорию в которую он пишется*/
            $aCategories = explode(',',$aData[GoodsEditor::categoryField]);
            $sAlias = \skewer\components\seo\Service::generateAlias($sAlias,0,$aCategories[0],'CatalogViewer');
        }

        $aData['alias'] = $this->getUniqAlias( $sAlias );
    }

    protected function getUniqAlias( $sAlias ) {

        $flag = (bool)Query::SelectFrom( $this->getBaseRow()->getModel()->getTableName() )
            ->where( 'alias', $sAlias )
            ->andWhere( 'id!=?', $this->iRowId )
            ->getCount()
        ;

        if ( !$flag )
            return $sAlias;

        preg_match( '/^(\S+)(-\d+)?$/Uis', $sAlias, $res );
        $sTplAlias = isSet( $res[1] ) ? $res[1] : $sAlias;
        $iCnt = isSet( $res[2] ) ? -(int)$res[2] : 0;
        while ( substr( $sTplAlias, -1 ) == '-' )
            $sTplAlias = substr( $sTplAlias, 0 , strlen($sTplAlias) - 1 );

        do {

            $iCnt++;
            $sAlias = $sTplAlias . '-' . $iCnt;

            $flag = (bool)Query::SelectFrom( $this->getBaseRow()->getModel()->getTableName() )
                ->where( 'alias', $sAlias )
                ->andWhere( 'id!=?', $this->iRowId )
                ->getCount()
            ;

        } while ( $flag );


        return $sAlias;
    }


    /**
     * Получение значений для сложно связанных полей
     */
    protected function fillLinkFields() {

        foreach( $this->getFields() as $oField ) {

            $aRel = $oField->getRelationList();

            if ( count($aRel) )
                foreach ( $aRel as $oRel )
                    if ( $oRel->getType() == ft\Relation::MANY_TO_MANY ) {
                        $sField = $oField->getName();
                        $this->setData( [$sField => $oField->getLinkRow( $this->getRowId() )] );
                    }
        }

    }

    /**
     * Уладение всех связей товаров с разделом $iSectionId
     * @param ModelEvent $event
     */
    public static function removeSection( ModelEvent $event ) {
        model\SectionTable::unlink( $event->sender->id );
        model\GoodsTable::removeSection( $event->sender->id );
    }

    /**
     * Класс для сборки списка автивных поисковых движков
     * @param \skewer\components\search\GetEngineEvent $event
     */
    public static function getSearchEngine(\skewer\components\search\GetEngineEvent $event ) {
        $event->addSearchEngine( Search::className() );
    }

}