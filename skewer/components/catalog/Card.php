<?php

namespace skewer\components\catalog;


use skewer\components\catalog\model\EntityTable;
use skewer\components\catalog\model\FieldRow;
use yii\helpers\ArrayHelper;
use skewer\base\ft;


/**
 * API для работы с карточками, словарями
 * Class Card
 * @package skewer\components\catalog
 * todo перенести методы работы с полями, группами в entity
 */
class Card extends Entity {

    /** дефолтная базовая карточка для каталога */
    const DEF_BASE_CARD = 'base_card';

    /** Имя модуля для карточки */
    const ModuleName = 'Catalog';

    const DEF_GOODS_MODULE = 'Catalog';

    const DEF_DICT_LAYER = 'Tool';
    
    const DEF_COLLECTION_MODULE = 'Collection';

    /** Системное имя поля для сортировки записей */
    const FIELD_SORT = 'priority';


    /**
     * Все справочники
     * @param $nameLayer - имя слоя {Catalog,Tool}
     * @return model\EntityRow[]
     */
    public static function getDictionaries($nameLayer) {
        return model\EntityTable::find()
            ->where( 'type', self::TypeDictionary )
            ->andWhere( 'module', $nameLayer )
            ->getAll();
    }


    /**
     * Все коллекции каталога
     * @return model\EntityRow[]
     */
    public static function getCollections() {
        return model\EntityTable::find()
            ->where( 'type', self::TypeDictionary )
            ->andWhere( 'module', self::DEF_COLLECTION_MODULE )
            ->getAll();
    }

    /**
     * Отдает запись коллекции по имени
     * @param string $sName
     * @return model\EntityRow|bool
     */
    public static function getCollection( $sName ) {
        return model\EntityTable::find()
            ->where( 'type', self::TypeDictionary )
            ->andWhere( 'module', self::DEF_COLLECTION_MODULE )
            ->andWhere('name', $sName)
            ->getOne();
    }

    /**
     * Все товарные карточки
     * @param bool $bWithBase Флаг выборки базовых карточек
     * @return model\EntityRow[]
     */
    public static function getGoodsCards( $bWithBase = false ) {

        $aTypes = [self::TypeExtended];

        if ( $bWithBase )
            $aTypes[] = self::TypeBasic;

        return model\EntityTable::find()
            ->where( 'type', $aTypes )
            ->getAll();
    }


    /**
     * Список справочников с id в качестве ключей
     * @var $nameLayer - имя слоя {Catalog|Tool}
     * @return array
     */
    public static function getDictAsArray($nameLayer) {
        $aOut = [];
        foreach ( self::getDictionaries($nameLayer) as $oEntity )
            $aOut[$oEntity->id] = $oEntity->title;
        return $aOut;
    }

    /**
     * Список справочников с системными именами в качестве ключей
     * @var $nameLayer - имя слоя {Catalog|Tool}
     * @return array
     */
    public static function getDictArrayWithName($nameLayer) {
        $aOut = [];
        foreach ( self::getDictionaries($nameLayer) as $oEntity )
            $aOut[$oEntity->name] = $oEntity->title;
        return $aOut;
    }
    
    
    /**
     * Получение справочника по названию и имени модуля
     * @param $sTitle
     * @param $sModuleName
     * @return array
     */
    public static function getDictByTitle($sTitle,$sModuleName) {

        return EntityTable::find()
            ->where('title',$sTitle)
            ->andWhere('module',$sModuleName)
            ->andWhere('type',self::TypeDictionary)
            ->getOne();
    }

    /**
     * Получение id справочника по системному имени и имени модуля
     * @param $sName
     * @param $sModuleName
     * @return array
     */
    public static function getDictIdByName($sName,$sModuleName) {

        $aDict =  EntityTable::find()
            ->where('name',$sName)
            ->andWhere('module',$sModuleName)
            ->andWhere('type',self::TypeDictionary)
            ->asArray()
            ->getOne();
        return ($aDict)?$aDict['id']:'';

    }
    

    /**
     * Список коллекций каталога
     * @return array
     */
    public static function getCollectionList() {
        $aOut = [];
        foreach ( self::getCollections() as $oEntity )
            $aOut[$oEntity->id] = $oEntity->title;
        return $aOut;
    }


    /**
     * Список товарных карточек
     * @param bool $bKeyIsName Флаг использования в качестве ключа название
     * @return array
     */
    public static function getGoodsCardList( $bKeyIsName = false ) {
        $aOut = [];
        foreach ( self::getGoodsCards() as $oEntity )
            $aOut[$bKeyIsName ? $oEntity->name : $oEntity->id] = $oEntity->title;
        return $aOut;
    }


    /**
     * Получить title карточки
     * @param string $name Имя карточки
     * @return string
     */
    public static function getTitle( $name ) {

        if ( is_numeric($name) )
            $card = model\EntityTable::findOne(['id' => $name]);
        else
            $card = model\EntityTable::findOne(['name' => $name]);

        return $card ? ArrayHelper::getValue( $card, 'title', '' ) : '';
    }


    /**
     * Получиь техническое имя по id
     * @param int $id Идентификатор карточки
     * @return string
     */
    public static function getName( $id ) {
        $card = model\EntityTable::find()->where( 'id', $id )->fields( 'name' )->asArray()->getOne();
        return isset($card['name']) ? $card['name'] : '';
    }


    /**
     * Имя базовой карточки для $card
     * @param int|string $card Идентификатор карточки
     * @return string
     */
    public static function getBaseCard( $card ) {

        $oExtCard = self::get( $card );

        if ( !$oExtCard )
            return '';
            //fixme throw new Exception( "Не найдена карточка [$sExtCardName]" );

        if ( !$oExtCard->isExtended() )
            return $card;
            //fixme throw new Exception( "Сущность [$sExtCardName] не является расширяющей" );

        $oBaseCard = self::get( $oExtCard->parent );

        if ( !$oBaseCard )
            return '';
            //fixme throw new Exception( "Не найдена карточка c id [".$oExtCard->parent."]" );

        return $oBaseCard->name;
    }



    /**
     * @param $data
     * @param $nameModule
     * @return model\EntityRow
     * @throws \Exception
     * used Dictionary\Module::actionDictionaryAdd
     */
    public static function addDictionary( $data, $nameModule) {

        $oCard = Card::get();

        $oCard->setData( $data );

        $oCard->type = self::TypeDictionary;
        $oCard->module = $nameModule;
        $oCard->save();

        // добавляем поле название
        self::setField( $oCard->id, 'title', \Yii::t('dict', 'title') );

        // добавляем поле сортировки
        self::setField( $oCard->id, self::FIELD_SORT, self::FIELD_SORT, ft\Editor::INTEGER);

        // генерим кеш
        $oCard->updCache();

        return $oCard;
    }

    /**
     * Создать коллекцию
     * @param $data Данные поля
     * @param $iProfileId Id профиля галереи
     * @return model\EntityRow
     * @throws \Exception
     */
    public static function addCollection( $data, $iProfileId ) {

        $oCard = Card::get();

        $oCard->setData( $data );

        $oCard->type = self::TypeDictionary;
        $oCard->module = self::DEF_COLLECTION_MODULE;
        $oCard->save();
        
        $iSeoGroup = self::setGroup( 'seo', 'SEO параметры' );
        $iControlsGroup = self::setGroup( 'controls', 'Элементы управления' );

        // добавляем поле название
        self::setField( $oCard->id, 'title', \Yii::t('collections', 'field_title') );
        self::setField( $oCard->id, 'alias', \Yii::t('collections', 'field_alias') );
        $row = self::setField( $oCard->id, 'gallery', \Yii::t('collections', 'field_gallery'), ft\Editor::GALLERY );
        $row->link_id = $iProfileId;
        $row->save();
        self::setField( $oCard->id, 'info', \Yii::t('collections', 'field_info'), ft\Editor::WYSWYG );

        self::setField( $oCard->id, 'active', \Yii::t('collections', 'field_active'), ft\Editor::CHECK, $iControlsGroup->id );
        self::setField( $oCard->id, 'on_main', \Yii::t('collections', 'field_on_main'), ft\Editor::CHECK, $iControlsGroup->id );
        self::setField( $oCard->id, 'last_modified_date', \Yii::t('collections', 'fields_last_modified_date'), ft\Editor::DATETIME, $iControlsGroup->id );

        // генерим кеш
        $oCard->updCache();

        return $oCard;
    }



    /**
     * Получение объекта поля
     * @param null|int $id
     * @return model\FieldRow
     */
    public static function getField( $id = null ) {

        if ( !$id )
            return model\FieldTable::getNewRow();

        return model\FieldTable::find( $id );
    }


    /**
     * Добавление нового поля для сущности
     * @param int $card Идентификатор карточки
     * @param string $name Техническое имя поля
     * @param string $title Заголовок поля
     * @param string $editor Тип редактора для поля
     * @param int $group Идентификатор группы поля
     * @return model\FieldRow
     */
    public static function setField( $card, $name, $title, $editor = ft\Editor::STRING, $group = 0 ) {

        $oField = model\FieldTable::getNewRow();

        $oField->setData([
            'name' => $name,
            'title' => $title,
            'group' => $group,
            'editor' => $editor,
            'entity' => $card
        ]);

        $oField->save();

        return $oField;
    }


    /**
     * Добавление новой группы полей для сущности
     * @param string $name Техническое имя поля
     * @param string $title Заголовок поля
     * @return model\FieldRow
     */
    public static function setGroup( $name, $title ) {

        $oGroup = model\FieldGroupTable::getNewRow();
        
        // Высчитать позицию последней группы
        $aLastPos = \Yii::$app->getDb()->createCommand('
                SELECT MAX(`position`)
                FROM ' . model\FieldGroupTable::getTableName()
        )->query()->read();

        $oGroup->setData([
            'name'     => $name,
            'title'    => $title,
            'position' => (int)reset($aLastPos) + 1,
        ]);

        $oGroup->save();

        return $oGroup;
    }


    /**
     * Получение объекта поля по имени и идетификатору карточки
     * @param int $card идентификатор карточки
     * @param string $name имя поля
     * @return model\FieldRow
     */
    public static function getFieldByName( $card, $name ) {
        return model\FieldTable::findOne( [ 'entity' => $card, 'name' => $name ] );
    }


    /**
     * Получение списка всех полей связанных с коллекциями
     * @return array
     */
    public static function getCollectionFields() {

        $coll = self::getCollectionList();

        $out = [];

        if ( !count($coll) )
            return $out;

        $fields = model\FieldTable::find()->where( 'link_id', array_keys( $coll ) )->getAll();

        foreach ( $fields as $field )
            $out[$field->link_id.':'.$field->name] = $coll[$field->link_id] . ' (' . $field->title . ')';

        return $out;
    }


    /**
     * Получение карточки для поля
     * @param $name
     * @return int
     */
    public static function get4Field( $name ) {

        $query = model\FieldTable::find()->where( ['name' => $name] );

        /** @var FieldRow $row */
        if ( $row = $query->each() )
            return $row->entity;

        return false;
    }


    /**
     * Получение списка групп карточки
     * @return model\FieldGroupRow[]
     */
    public static function getGroups() {
        return model\FieldGroupTable::find()->order( 'position' )->getAll();
    }


    /**
     * Получение списка заголовоков групп
     * @param array $aGroupsTypes Ссылка на типы групп
     * @return array
     */
    public static function getGroupList(&$aGroupsTypes = []) {

        $aOut = [0 => \Yii::t('catalog' ,'base_group')];

        /** @var model\FieldGroupRow $oGroup */
        $query = model\FieldGroupTable::find()
            ->order('position')
        ;

        while ( $oGroup = $query->each() ) {
            $aOut[$oGroup->id]         = $oGroup->title;
            $aGroupsTypes[$oGroup->id] = (int)$oGroup->group_type;
        }

        return $aOut;
    }

    /**
     * Получение объекта группы поля
     * @param null|int $id идентификатор группы
     * @return model\FieldGroupRow
     */
    public static function getGroup( $id = null ) {

        if ( !$id )
            return model\FieldGroupTable::getNewRow();

        return model\FieldGroupTable::find( $id );
    }


    /**
     * Получение объекта группы поля по имени и идетификатору карточки
     * @param string $name имя группы
     * @return model\FieldGroupRow
     */
    public static function getGroupByName( $name ) {
        return model\FieldGroupTable::findOne( [ 'name' => $name ] );
    }

    /**
     * Отдает флаг скрытости детальной
     * @return bool
     */
    public static function isDetailHidden($iSectionId){

        $sCard = Section::getDefCard($iSectionId);

        return self::isDetailHiddenByCard($sCard);
    }

    public static function isDetailHiddenByCard($sCard){
        $oCard = self::get($sCard);

        if (!$oCard) return false;

        return (bool)$oCard->hide_detail;
    }
}