<?php

namespace skewer\components\catalog;

use skewer\base\orm\Query;
use skewer\base\ft;
use skewer\base\SysVar;
use skewer\base\Twig;
use skewer\build\Catalog\Goods\SeoGood;
use skewer\build\Catalog\Goods\SeoGoodModifications;
use skewer\components\gallery\models\Photos;
use yii\helpers\ArrayHelper;
use skewer\components\catalog\model\GoodsTable;
use skewer\components\catalog\model\SectionTable;
use skewer\components\catalog\model\SemanticTable;
use skewer\base\section\Tree;

/**
 * Класс для выборки товарных позиций для вывода на клиентской части
 * Class GoodsSelector
 * @package skewer\build\libs\Catalog
 */
class GoodsSelector extends SelectorPrototype {

    /** @var bool Флаг фильтрации по полям */
    private $bApplyAttrFilter = false;

    /** @var bool Флаг факта использования фильтрации */
    private $bFilterUsed = false;

    /** @var bool Флаг необходимости использовать группировку для товаров */
    private $useGrouping = false;

    /**
     * Отдает алиас товара по его ID
     * @param $iGoodId
     * @return ft\model\Field
     * @throws \Exception
     */
    public static function getGoodsAlias($iGoodId){

        $aAlias = Query::SelectFrom( 'co_base_card' )
                    ->fields('alias')
                    ->where('id',$iGoodId)
                    ->asArray()
                    ->getOne();
        if (!$aAlias) return false;

        return $aAlias['alias'];

    }

    /**
     * Получить распарсенную каталожную позицию
     * @param int $id Id каталожной позиции
     * @param string $baseCard
     * @param bool $bAllFields Парсить все активные поля карточки(true) или только поля для детальной страницы(false)?
     * @param int $iCurrentSection - id текущего раздела. Если =0, вернет товар без seo - данных
     * @return array|bool
     */
    public static function get( $id, $baseCard = Card::DEF_BASE_CARD, $bAllFields = false, $iCurrentSection = 0 ) {

        if ( is_numeric($id) ) {
            $oGoodsRow = GoodsRow::get( $id, $baseCard );
        } else {
            $oGoodsRow = GoodsRow::getByAlias( $id, $baseCard );
        }

        if (!$oGoodsRow){
            return false;
        }

        $aGoodParams = model\GoodsTable::get( $oGoodsRow->getRowId() );

        $aGoodData =  Parser::get( $oGoodsRow->getFields(), $bAllFields ? ['active'] : ['active', 'show_in_detail'] )
            ->parseGood( $aGoodParams, $oGoodsRow->getData(), false );

        //  Если передан $iCurrentSection, то добавляем seo данные
        if ($iCurrentSection){
            $sCard = ArrayHelper::getValue($aGoodData, 'card', '');
            $aGoodData = self::addSeoDataInGood($aGoodData, $sCard, $iCurrentSection);
        }

        return $aGoodData;

    }

    /**
     * Добавляет seo - данные товара в массив
     * @param $aData - данные товара
     * @param $mCard - карточка товара
     * @param $iSectionId - id текущего раздела
     * @return array
     * @throws \Exception
     */
    private static function addSeoDataInGood($aData, $mCard, $iSectionId){

        if (!$mCard)
            throw new \Exception("Не передана карточка");

        $sCard = is_numeric($mCard)? Card::getName($mCard) : $mCard;

        if ($aData['id'] != $aData['main_obj_id']){
            $oSeo = new SeoGoodModifications($aData['id'], $iSectionId, $aData);
        } else {
            $oSeo = new SeoGood($aData['id'], $iSectionId, $aData);
            $oSeo->setExtraAlias($sCard);
        }

        // Перекрываем alt_title и title изображениям фотогалерей
        foreach ($aData['fields'] as $aField) {
            if ( $aField['type'] == ft\Editor::GALLERY ){
                if ( ArrayHelper::getValue($aField, 'gallery.images') ){

                    /** @var Photos $image */
                    foreach ($aField['gallery']['images'] as $image) {
                        if ( !$image->alt_title )
                            $image->alt_title =  $oSeo->parseField('altTitle', ['sectionId' => $iSectionId, 'label_number_photo' => $image->priority ]);

                        if ( !$image->title )
                            $image->title = $oSeo->parseField('nameImage', ['sectionId' => $iSectionId, 'label_number_photo' => $image->priority]);

                        $oSeo->clearReplaceLabels();
                    }
                }
            }
        }

        return $aData;
    }


    /**
     * Получить товар следующий за заданным
     * @param int $iObjectId Ид объекта
     * @param int $iSectionId Ид раздела
     * @param array $aFilter Фильтр для сортировки
     * @param bool $bShortData отдаст только основные данные без парсингов
     * @return array|bool
     */
    public static function getPrev($iObjectId, $iSectionId, /** @noinspection PhpUnusedParameterInspection */
                                   $aFilter = array(), $bShortData = false ) {

        if ( !($row = model\SectionTable::getPrev( $iObjectId, $iSectionId )) )
            return false;

        if ($bShortData) return $row;

        if ( !($oGoodsRow = GoodsRow::get( $row['goods_id'], $row['goods_card'] )) )
            return false;

        $aGoodParams = model\GoodsTable::get( $oGoodsRow->getRowId() );

        return Parser::get( $oGoodsRow->getFields() )
            ->parseGood( $aGoodParams, $oGoodsRow->getData(), true )
            ;

    }


    /**
     * Получить товар идущий перед заданным
     * @param int $iObjectId Ид объекта
     * @param int $iSectionId Ид раздела
     * @param array $aFilter Фильтр для сортировки
     * @param bool $bShortData отдаст только основные данные без парсингов
     * @return array|bool
     */
    public static function getNext($iObjectId, $iSectionId, /** @noinspection PhpUnusedParameterInspection */
                                   $aFilter = array() , $bShortData = false ) {

        if ( !($row = model\SectionTable::getNext( $iObjectId, $iSectionId )) )
            return false;

        if ($bShortData) return $row;

        if ( !($oGoodsRow = GoodsRow::get( $row['goods_id'], $row['goods_card'] )) )
            return false;

        $aGoodParams = model\GoodsTable::get( $oGoodsRow->getRowId() );

        return Parser::get( $oGoodsRow->getFields() )
            ->parseGood( $aGoodParams, $oGoodsRow->getData(), true )
            ;

    }


    /**
     * Список товарных позиций для коллекции
     * @param $brand
     * @param $field
     * @param $value
     * @return GoodsSelector
     */
    public static function getList4Collection( /** @noinspection PhpUnusedParameterInspection */
        $brand, $field, $value ) {

        $oGoods = new self();

        $oGoods->selectCard( Card::get4Field( $field ) );
        $oGoods->initParser( ['active','show_in_list'] );
        $oGoods->cacheFields();


        // выборка по таблицам
        $sFieldLine = 'co_' . $oGoods->sBaseCard . '.*, base_id';

        $oGoods->oQuery = Query::SelectFrom( 'co_' . $oGoods->sBaseCard )
            ->join( 'inner', GoodsTable::getTableName(), GoodsTable::getTableName(), 'co_' . $oGoods->sBaseCard . '.id=base_id' )
            ->on( 'base_id=parent' )
        ;

        if ( $oGoods->bUseExtCard ) {
            $oGoods->oQuery->join( 'inner', 'ce_' . $oGoods->sExtCard, 'ext_card', 'ext_card.id=base_id' );
            $sFieldLine .= ', ext_card.*';
        }


        if ($oGoods->aCardFields[$field]->getOption('editor')['name'] == ft\Editor::MULTICOLLECTION) {
            $oGoods->oQuery->join( 'inner', 'cr_' . $oGoods->sBaseCard .'__'.$field, 'mlt_collection', 'mlt_collection.__inner=base_id')
                ->on('mlt_collection.__external = ?', $value);
        } else {
            if ( isSet( $oGoods->aCardFields[ $field ] ) )
                $oGoods->oQuery->where( $field, $value );
            else
                $oGoods->oQuery->where( 'id', 0 );
        }


        $oGoods->oQuery->fields( $sFieldLine )->asArray();

        $oGoods->bSorted = false;

        return $oGoods;

    }


    /**
     * Список товарных позиций для разделов
     * @param int|array $section Ид или список разедлов
     * @param array $aAttr - атрибуты выбираемых полей (active, show_in_list ..)
     * @param bool $bShowModification - выводить товары модификации ?
     * @return GoodsSelector
     */
    public static function getList4Section($section, $aAttr = [ 'active','show_in_list' ], $bShowModification = false ) {

        $oGoods = new self();

        $oGoods->selectCard( self::card4Section( $section ) );
        $oGoods->initParser( $aAttr );
        $oGoods->cacheFields();

        // Выборка по таблицам. Поле section из таблицы c_goods используется для определения наличия раздела у товара
        // при получении списка для добавления сопутствующих товаров и товаров в комплекте
        $sFieldLine = 'co_' . $oGoods->sBaseCard . '.*, base_id, section';

        if ( !$bShowModification ){

            $oGoods->oQuery = Query::SelectFrom( 'co_' . $oGoods->sBaseCard )
                ->join( 'inner', GoodsTable::getTableName(), GoodsTable::getTableName(), 'co_' . $oGoods->sBaseCard . '.id=base_id' )
                ->on( 'base_id=parent' )
                ->join( 'inner', SectionTable::getTableName(), '', 'co_' . $oGoods->sBaseCard . '.id=goods_id' )
                ->on( 'section_id', $section )
            ;

        } else {

            $oGoods->oQuery = Query::SelectFrom( 'co_' . $oGoods->sBaseCard )
                ->join( 'inner', GoodsTable::getTableName(), GoodsTable::getTableName(), 'co_' . $oGoods->sBaseCard . '.id=base_id' )
                ->join( 'inner', SectionTable::getTableName(), '', GoodsTable::getTableName() . '.parent=goods_id' )
                ->on( 'section_id', $section )
            ;

        }

        if ( $oGoods->bUseExtCard ) {
            $oGoods->oQuery->join( 'inner', 'ce_' . $oGoods->sExtCard, 'ext_card', 'ext_card.id=base_id' );
            $sFieldLine .= ', ext_card.*';
        }

        // отсекаем дубли одного товара в нескольких разделах
        if ( is_array($section) && count($section) > 1 )
            $oGoods->useGrouping = true;

        $oGoods->oQuery->fields( $sFieldLine )->asArray();

        $oGoods->bSorted = true;

        return $oGoods;
    }


    /**
     * Выборка позиций для вывода общего списка по базовой карточки
     * @param int|string $card Базовая или расширенная карточка
     * @param bool $bOnlyFirstLevel флаг выбора только товаров первого уровне (без модификаций)
     * @return GoodsSelector
     * @throws Exception
     */
    public static function getList( $card = Card::DEF_BASE_CARD, $bOnlyFirstLevel = true ) {

        $oGoods = new self();

        $oGoods->selectCard( $card );
        $oGoods->initParser( ['active','show_in_list'] );
        $oGoods->cacheFields();

        // Выборка по таблицам. Поле section из таблицы c_goods используется для определения наличия раздела у товара
        // при получении списка для добавления сопутствующих товаров и товаров в комплекте
        $sFieldLine = 'co_' . $oGoods->sBaseCard . '.*, base_id, section';

        $oGoods->oQuery = Query::SelectFrom( 'co_' . $oGoods->sBaseCard )
            ->join( 'inner', GoodsTable::getTableName(), GoodsTable::getTableName(), 'co_' . $oGoods->sBaseCard . '.id=base_id' )
        ;

        if ( $bOnlyFirstLevel )
            $oGoods->oQuery->on( 'base_id=parent' );

        if ( $oGoods->bUseExtCard ) {
            $oGoods->oQuery->join( 'inner', 'ce_' . $oGoods->sExtCard, 'ext_card', 'ext_card.id=base_id' );
            $sFieldLine .= ', ext_card.*';
        }

        $oGoods->oQuery->fields( $sFieldLine )->asArray();

        return $oGoods;
    }

    public static function getListByIds( $aIds, $card = Card::DEF_BASE_CARD, $bOnlyFirstLevel = true ) {

    	if ( count($aIds) == 0 )
    		return [];

        $oGoods = new self();

        $oGoods->selectCard( $card );
        $oGoods->initParser( ['active','show_in_list'] );
        $oGoods->cacheFields();

        // Выборка по таблицам. Поле section из таблицы c_goods используется для определения наличия раздела у товара
        // при получении списка для добавления сопутствующих товаров и товаров в комплекте
        $sFieldLine = 'co_' . $oGoods->sBaseCard . '.*, base_id, section';

        $oGoods->oQuery = Query::SelectFrom( 'co_' . $oGoods->sBaseCard )
            ->join( 'inner', GoodsTable::getTableName(), GoodsTable::getTableName(), 'co_' . $oGoods->sBaseCard . '.id=base_id' )
        ;

        if ( $bOnlyFirstLevel )
            $oGoods->oQuery->on( 'base_id=parent' );

        if ( $oGoods->bUseExtCard ) {
            $oGoods->oQuery->join( 'inner', 'ce_' . $oGoods->sExtCard, 'ext_card', 'ext_card.id=base_id' );
            $sFieldLine .= ', ext_card.*';
        }

        $oGoods->oQuery->where('id',$aIds);

        $oGoods->oQuery->fields( $sFieldLine )->asArray();

        return $oGoods;
    }


    /**
     * Список сопутствующих товаров
     * @param int $iObjectId Id товара
     * @return GoodsSelector
     */
    public static function getRelatedList( $iObjectId ) {

        $oGoods = new self();

        $row = model\GoodsTable::get( $iObjectId );

        $oGoods->selectCard( $row['base_card_name'] );
        $oGoods->initParser( ['active','show_in_list'] );

        $oGoods->oQuery = Query::SelectFrom( 'co_' . $oGoods->sBaseCard, $oGoods->sBaseCard )
            ->fields( 'co_' . $oGoods->sBaseCard . '.*, section,priority' )
            ->join( 'inner', GoodsTable::getTableName(), GoodsTable::getTableName(), 'co_' . $oGoods->sBaseCard . '.id=base_id' )
            ->on( 'base_id=parent' )
            ->join( 'inner', SemanticTable::getTableName(), 'tbl_semantic', 'co_' . $oGoods->sBaseCard . '.id=parent_id' )
            ->on( 'semantic=?', Semantic::TYPE_RELATED )
            ->on( 'child_id=?', $iObjectId )
            ->asArray()
        ;

        $oGoods->bSorted = true;

        return $oGoods;
    }

    public static function getRelatedList4ObjectRand( $iObjectId, $iCurSectionId ) {

        //Выборка разделов из которых выводим сопутствующие

        $oGoods = new self();

        $row = model\GoodsTable::get( $iObjectId );

        $oGoods->selectCard( $row['base_card_name'] );

        $aSections = RelatedSections::getRelationsByPageId($iCurSectionId);

        if (empty($aSections)) return false;

        $iCountGoods = Query::SelectFrom( 'co_'.$oGoods->sBaseCard )
            ->fields('id')
            ->join( 'inner', SectionTable::getTableName(), '', 'co_'.$oGoods->sBaseCard.'.id=goods_id' )
            ->on( 'section_id', $aSections )
            ->where('active','1')
            ->where('id!=?',$iObjectId)
            ->where('section_id',$aSections)
            ->getCount('id');

        /*Вот тут вот*/
        $iNeedCount = SysVar::get('catalog.random_related_count');

        $iRandCount = 100;

        $iOffset = rand(0,$iCountGoods-$iRandCount);

        if ($iOffset<0) $iOffset = 0;

        $aGoods = Query::SelectFrom( 'co_base_card' )
            ->fields('id')
            ->join( 'inner', SectionTable::getTableName(), '', 'co_base_card.id=goods_id' )
            ->on( 'section_id', $aSections )
            ->where('active','1')
            ->where('id!=?',$iObjectId)
            ->where('section_id',$aSections)
            ->limit($iRandCount,$iOffset)
            ->asArray()
            ->getAll();
        ;

        $aGoods = ArrayHelper::getColumn($aGoods,'id');

        if (count($aGoods)<$iNeedCount) $iNeedCount = count($aGoods);

	    $aKeys = ( count($aGoods) > 0) ? array_rand($aGoods,$iNeedCount) : [];
	    $aGoodsIds = [];

	    if (is_int($aKeys)) { // Если один ключ
		    $aGoodsIds[] = $aGoods[$aKeys];
	    } else {
		    foreach ($aKeys as $key ){
			    $aGoodsIds[] = $aGoods[$key];
		    }
	    }

        return self::getListByIds($aGoodsIds);
    }
    /**
     * Список товаров в комплекте
     * @param int $iObjectId Id товара
     * @return GoodsSelector
     */
    public static function getIncludedList( $iObjectId ) {

        $oGoods = new self();

        $row = model\GoodsTable::get( $iObjectId );

        $oGoods->selectCard( $row['base_card_name'] );
        $oGoods->initParser( ['active','show_in_list'] );

        $oGoods->oQuery = Query::SelectFrom( 'co_' . $oGoods->sBaseCard, $oGoods->sBaseCard )
            ->fields( 'co_' . $oGoods->sBaseCard . '.*, section' )
            ->join( 'inner', GoodsTable::getTableName(), GoodsTable::getTableName(), 'co_' . $oGoods->sBaseCard . '.id=base_id' )
            ->on( 'base_id=parent' )
            ->join( 'inner', SemanticTable::getTableName(), 'tbl_semantic', 'co_' . $oGoods->sBaseCard . '.id=parent_id' )
            ->on( 'semantic=?', Semantic::TYPE_INCLUDE )
            ->on( 'child_id=?', $iObjectId )
            ->asArray()
        ;

        $oGoods->bSorted = true;

        return $oGoods;
    }


    /**
     * Получение списка аналогов для товара
     * @param int $iObjectId Id объекта для которого запрашиваются модификации
     * @param int $iExcludeId Id Объекта для исключения из списка модификаций. По умолчанию = id родительского объекта
     * @return GoodsSelector
     */
    public static function getModificationList( $iObjectId, $iExcludeId = 0 ) {

        $aRow = model\GoodsTable::get( $iObjectId );
        $iExcludeId = $iExcludeId ?: $aRow['parent'];

        $oGoods = new self();

        $oGoods->selectCard( $aRow['ext_card_name'] );
        $oGoods->initParser( ['active','show_in_list'] );

        $oGoods->oQuery = Query::SelectFrom( 'co_' . $oGoods->sBaseCard, $oGoods->sBaseCard )
            ->fields( 'co_' . $oGoods->sBaseCard . '.*,ext_card.*' )
            ->order( 'title' )
            ->order( 'co_'.$oGoods->sBaseCard.'.id' )
            ->join( 'inner', GoodsTable::getTableName(), GoodsTable::getTableName(), 'co_' . $oGoods->sBaseCard . '.id=base_id' )
            ->on( 'parent=?', (int)$iObjectId )
            ->on( 'base_id<>?', $iExcludeId )
            ->join( 'inner', 'ce_' . $oGoods->sExtCard, 'ext_card', 'ext_card.id=base_id' )
            ->asArray()
        ;

        $oGoods->bSorted = false;

        return $oGoods;

    }


    /**
     * Фильтрация списка каталожных позиций
     * @return bool
     */
    public function applyFilter() {

        $this->bApplyAttrFilter = true;

        $aFilter = Cache\Api::get( 'Catalog.Conditions' );

        if ( $aFilter )
            foreach ( $aFilter as $field => $conditions ) {

                $oField = $this->aCardFields[$field];
                $oRel = $oField->getModel()->getOneFieldRelation( $field );

                if ( $oRel and $oRel->getType() == ft\Relation::MANY_TO_MANY ) {

                    $this->oQuery
                        ->join( 'inner', $oField->getLinkTableName(), $oField->getLinkTableName(), 'co_' . $this->sBaseCard . '.id=`'.$oField->getLinkTableName().'`.' . ft\Relation::INNER_FIELD )
                        ->on( sprintf('%s.%s', $oField->getLinkTableName(), ft\Relation::EXTERNAL_FIELD), $conditions[1] )
                    ;

                    $this->useGrouping = true;

                } else
                    $this->oQuery->andWhere( $conditions[0], $conditions[1] );

                $this->bFilterUsed = true;
            }

        return $this->bFilterUsed;
    }



    /**
     * Парсинг набора товаров
     * @return array
     */
    public function parse() {

        // установка базовой сортировки если не задано поле
        if ( $this->bSorted )
            $this->oQuery->order( 'priority' );

        if ( $this->useGrouping )
            $this->oQuery->groupBy( 'co_' . $this->sBaseCard . '.id' );

        // Включить в выборку поля из таблицы описания товара, нужные для парсера
        $this->oQuery->fields(GoodsTable::getTableName() . '.*');

        $aGoodsList = $this->oQuery->asArray()->getAll();

        $aItems = [];
        foreach ( $aGoodsList as &$aGood )
            $aItems[] = $this->oParser->parseGood( $aGood, $aGood, false);

        // Добавляем seo - данные
        if ($this->bWithSeo && ($iSectionId = $this->getInnerParam('iSectionId'))){
            foreach ($aItems as &$aItem){
                $sCard = ArrayHelper::getValue($aItem, "card", '');
                $aItem = self::addSeoDataInGood($aItem, $sCard, $iSectionId);
            }
        }

        return $aItems;
    }


    /**
     * Итератор выборки с парсером (используется в YandexExport)
     * @return bool|array
     */
    public function parseEach() {

        // Инициализация итератора
        if ( !$this->oQuery->getBEachIterator() ) {

            // Включить в выборку поля из таблицы описания товара, нужные для парсера
            $this->oQuery
                ->fields(GoodsTable::getTableName() . '.*')
                ->asArray();

            // установка базовой сортировки если не задано поле
            if ( $this->bSorted )
                $this->oQuery->order( 'priority' );
        }

        if ( !($aGood = $this->oQuery->each()) )
            return false;

        return $this->oParser->parseGood( $aGood, $aGood );
    }


    public function getArray( &$count ) {

        // установка базовой сортировки если не задано поле
        if ( $this->bSorted )
            $this->oQuery->order( 'priority' );

        $goods = $this->oQuery->setCounterRef( $count )->asArray()->getAll();

        foreach ($goods as &$good)
        {
            $good['price'] =  Twig::priceFormat($good['price'], 0);
        }

        return $goods;
    }

    /**
     * Возвращает объект запросника
     * @return \skewer\base\orm\state\StateSelect
     * fixme используется в FullCustomList ( ->getWithoutIncluded ->getWithoutRelated )
     */
    public function getQuery() {

        // установка базовой сортировки если не задано поле
        if ( $this->bSorted )
            $this->oQuery->order( 'priority' );

        return $this->oQuery;
    }


    /**
     * Возвращает карточку для построения списка товаров для разделов
     * @param int $section идентификатор раздела с товарами
     * @return int
     * todo это функция в апи разделов должна быть
     */
    public static function card4Section( $section ) {

        // получаем список всех карточек товаров для раздела
        $cards = Section::getCardList( $section );

        if ( count( $cards ) == 1 ) {
            // если одна, то берем ее (вывод по одной расширенной карточке)
            $card = array_shift( $cards );
        } elseif ( count( $cards ) > 1 ) {
            // если больше одной, то берем базовую (вывод только по базовой)
            $card = Card::getBaseCard( $cards[0] );
        } else {
            // карточки нет - либо ошибка, либо раздел пуст - выводим по дефолтной базовой
            // todo проработать этот сценарий
            $card = Card::DEF_BASE_CARD;
        }

        return $card;
    }


    /**
     * Задание условий на сортировку поля товара
     * @param string $sFieldName
     * @param string $sWay
     * @param bool $bCheckField Проверить поле на доступность сортировки
     * @return $this
     * fixme ref
     */
    public function sort( $sFieldName, $sWay = 'ASC', $bCheckField = true ) {

        // todo разобраться с типами
        // нужна ли проверка на существование поля здесь ?
        //        if ( $sFieldName && count($this->aCardFields) ) {
        //            if ( isSet( $this->aCardFields[$sFieldName] ) ) {
        //                // todo ...
        //                $sDataType = $this->aCardFields[$sFieldName]->getDatatype();
        //                if ( $sDataType == 'int' || $sDataType == 'float' ) {
        //                    $sWay = ( $sWay == 'ASC' ) ? 'ASC_NUMERIC' : 'DESC_NUMERIC';
        //                }
        //            } else {
        //                throw new Exception( 'Ошибка: поле для сортировки "' . $sFieldName . '" не найдено' );
        //            }
        //        }


        if ( !$sFieldName )
            return $this;

        if ($bCheckField) {

            // fixme
            if (!count($this->aCardFields)) {
                $this->oQuery->order($sFieldName, $sWay);
                $this->bSorted = false;

                return $this;
            }

            /** @var ft\model\Field $field */
            if (!($field = ArrayHelper::getValue($this->aCardFields, $sFieldName, false)))
                return $this;

            // fixme Attr::SHOW_IN_LIST Attr::SHOW_IN_SORTPANEL
            if (!$field->getAttr('show_in_list') || !$field->getAttr('show_in_sortpanel'))
                return $this;

            // hack не обрабатываем пока связи ><
            if ($rel = $field->getFirstRelation())
                if ($rel->getType() == ft\Relation::MANY_TO_MANY)
                    return $this;
        }

        $this->oQuery->order( $sFieldName, $sWay );
        $this->bSorted = false;

        return $this;
    }

    /** Только товары из видимых разделов */
    public function onlyVisibleSections() {

        $this->oQuery
            ->where('section', Tree::getVisibleSections())
        ;

        return $this;
    }

    /**
     * Добавить в выборку поле, показывающее доступность базового раздела товара, которое будет содержать:
     * * 0, если раздел невидим, является ссылкой или отсутствует
     * * 1, иначе
     */
    public function addAvailableSectionField() {

        $this->oQuery
            ->field("(`section` IN (". join(',', array_keys(Tree::getVisibleSections())) .")) AS 'available_section'")
        ;

        return $this;
    }
}
