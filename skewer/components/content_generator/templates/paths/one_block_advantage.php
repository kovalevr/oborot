<div class="gc-plus">
    <div class="plus__item">
        <div class="plus__imgbox"><span class="fa fa-asterisk"></span></div>

        <div class="plus__text">
            <p>Lorem ipsum dolor sit amet consectetur adipiscing elit</p>
        </div>
    </div>

    <div class="plus__item">
        <div class="plus__imgbox"><span class="fa fa-search"></span></div>

        <div class="plus__text">
            <p>Lorem ipsum dolor sit amet consectetur adipiscing elit</p>
        </div>
    </div>

    <div class="plus__item">
        <div class="plus__imgbox"><span class="fa fa-user"></span></div>

        <div class="plus__text">
            <p>Lorem ipsum dolor sit amet consectetur adipiscing elit</p>
        </div>
    </div>

    <div class="plus__item">
        <div class="plus__imgbox"><span class="fa fa-eur"></span></div>

        <div class="plus__text">
            <p>Lorem ipsum dolor sit amet consectetur adipiscing elit</p>
        </div>
    </div>

    <div class="plus__item">
        <div class="plus__imgbox"><span class="fa fa-home"></span></div>

        <div class="plus__text">
            <p>Lorem ipsum dolor sit amet consectetur adipiscing elit</p>
        </div>
    </div>

    <div class="plus__item">
        <div class="plus__imgbox"><span class="fa fa-qrcode"></span></div>

        <div class="plus__text">
            <p>Lorem ipsum dolor sit amet consectetur adipiscing elit</p>
        </div>
    </div>
</div>
