<div class="gc-promo">
    <div class="promo__nav">
        <div class="promo__imgbox"><span class="fa fa-phone"></span></div>
        <h2><?=Yii::t('content_generator','title_callback_h2')?></h2>
        <div class="promo__buttom"><a href="#"><?=Yii::t('content_generator','title_order')?></a></div>
    </div>
    <div class="promo__content">
        <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat.</p>
    </div>
</div>
