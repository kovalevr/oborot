<?php

namespace skewer\components\ext\field;

/**
 * Поле типа "Файл"
 */
class File extends Prototype {

    function getView() {
        return 'file';
    }

}