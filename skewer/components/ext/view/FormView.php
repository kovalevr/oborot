<?php
/**
 * Created by PhpStorm.
 * User: Александр
 * Date: 29.07.2016
 * Time: 13:38
 */

namespace skewer\components\ext\view;


use skewer\base\ui;


abstract class FormView extends Prototype{

    /** @var ui\builder\FormBuilder объект построителя интерфейсов */
    protected $_form;

    public function __construct(array $config = []) {

        parent::__construct($config);

        $this->_form = ui\StateBuilder::newEdit();

    }

    /**
     * Отдает объект построитель интерфейса
     * @return ui\state\BaseInterface
     */
    function getInterface() {
        return $this->_form->getForm();
    }

}