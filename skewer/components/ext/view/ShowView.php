<?php
/**
 * Created by PhpStorm.
 * User: holod
 * Date: 28.02.2017
 * Time: 18:25
 */

namespace skewer\components\ext\view;

use skewer\base\ui;
use skewer\components\ext;

abstract class ShowView extends Prototype
{
    /** @var skewer\base\ui\builder\FormBuilder объект построителя интерфейсов */
    protected $_form;

    public function __construct(array $config = []) {

        parent::__construct($config);

        $this->_form = ui\StateBuilder::newShow();

    }

    /**
     * Отдает объект построитель интерфейса
     * @return ui\state\BaseInterface
     */
    function getInterface() {
        return $this->_form->getForm();
    }
}