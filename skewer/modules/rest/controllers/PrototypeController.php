<?php

namespace skewer\modules\rest\controllers;

use skewer\base\section\Parameters;
use skewer\base\section\params\ListSelector;
use skewer\base\site\Site;
use skewer\components\gallery\Photo;

/**
 * Прототип restApi контроллеров
 * Class PrototypeController
 * @package skewer\modules\rest\controllers
 */
class PrototypeController extends \yii\rest\Controller {

    /** Текущая версия restApi контроллеров */
    const VERSION = '1.0';

    /** Ошибка проверки ключа */
    const ERR_KEY = 'not_valid_key';
    /** Неизвестная ошибка */
    const ERR_OTHER = 'unknown';

    /** Текстовые сообщения ошибок для пользователя. Общие для всех контроллеров */
    private static $aErrorMess = [
        self::ERR_KEY                 => 'Пустой либо неверный ключ защиты',
        UsersController::ERR_AUTH     => 'Указанная связка пользователя и пароля не существует либо пользователь не активен или забанен',
        UsersController::ERR_NOAUTH   => 'Для проведения операции необходимо авторизоваться',
        UsersController::ERR_DATA     => 'Данные заполнены не верно',
        UsersController::ERR_REGLOGIN => 'Пользователь с таким логином уже существует',
    ];

    /**
     * Проверить ключ доступа при запросе на изменение данных на сайте
     * @param \yii\base\InlineAction $action
     * @return bool
     * @throws \yii\web\BadRequestHttpException
     */
    final public function beforeAction($action) {

        // Если данные передаются методом POST, то проверить ключ доступа
        if ($aData = \Yii::$app->request->post()) {

            $aParams = Parameters::getList()
                ->name('key_rest')
                ->group('.')
                ->level(ListSelector::alEdit)
                ->get();
            $sKeyServer = $aParams ? reset($aParams)->value : '';

            $sKeyClient = \Yii::$app->request->post('key_rest', '');
            $sKeyServer = $sKeyServer ?: $sKeyClient;

            if (strcasecmp($sKeyClient, $sKeyServer) !== 0)
                $action->actionMethod = 'actionErrorkey';
        }

        // Добавить в заголовок информацию о текущей версии
        header('X_Rest_Api_Version: '. self::VERSION);

        return parent::beforeAction($action);
    }

    /** Вывод текущей версии контроллеров */
    final public function actionVersion() {

        return ['version' => self::VERSION];
    }

    /** Вывод сообщения об ошибке проверки ключа */
    final public function actionErrorkey() {

        return $this->showError(self::ERR_KEY);
    }

    /** Вывод сообщения об успешной операции */
    final protected function showSuccess() {

        return ['success' => 1];
    }

    /** Вывод ошибки */
    final protected function showError($sErrorCode, $sErrorMess = '') {

        $sErrorMess = $sErrorMess ?: @self::$aErrorMess[$sErrorCode] ?: '';
        return [
            'error' => $sErrorCode,
            'error_text_ru' => $sErrorMess,
        ];
    }

    /**
     * Установить данные постраничника в заголовок ответа
     * @param int $iCountTotal Всего позиций
     * @param int $iCountPage Всего страниц
     * @param int $iCurrentPage Текущая страница
     * @param int $iOnPage Число на странице
     */
    final protected function setPagination($iCountTotal, $iCountPage, $iCurrentPage, $iOnPage) {
        header('X_Pagination_Total_Count: ' . $iCountTotal);
        header('X_Pagination_Page_Count: ' . $iCountPage);
        header('X_Pagination_Current_Page: ' . $iCurrentPage);
        header('X_Pagination_Per_Page: ' . $iOnPage);
    }

    /** Получить массив изображений из альбома/альбомов */
    protected function getImages($mAlbumId, $bGetOne = true) {
        $aResult = [];

        if ( $aImages = Photo::getFromAlbum($mAlbumId) )
            foreach ($aImages as &$paImage) {
                foreach ($paImage['images_data'] as $sFormatName => $aFormatData)
                    $aResult[ $paImage['album_id'] ][$sFormatName] = Site::httpDomain() . $aFormatData['file'];
            }

        if ($bGetOne)
            return $aResult ? reset($aResult) : '';

        return $aResult ? : '';
    }
}