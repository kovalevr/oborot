<?php
namespace skewer\helpers;
use skewer\base\section\Tree;
use skewer\base\site\Page;
use skewer\base\site\Site;

/**
 *
 * @class Paginator
 *
 * @author ArmiT, $Author$
 * @version $Revision$
 * @date $Date$
 * @project Skewer
 * @package Build
 *
 * @example
 * $aURLParams['NewsModule'] array('onpage'=>100);
 * $aParams['onPage'] = 100;
 * $aParams['onGroup'] = 10;
 * $aParams['useGroups'] = true;
 * $aParams['useEdges'] = true;
 * $aParams['useItems'] = true;
 * Paginator::getPageLine(1, 100, 5, $aURLParams, $aParams);
 */
class Paginator {

    /**
     * Флаг запрещающий в код вставлять canonical пагинации
     * @var bool
     */
    public static $bHideCanonicalPagination = false;

    /**
     * Построение пагинационных страниц
     * @param int $iPage - номер страницы
     * @param int $iCount - общее количество
     * @param int $iSectionId - номер секции
     * @param array $aURLParams
     * @param array $aParams  - $aParams['onGroup'] - кол-во страниц пагинации
     * @return array $aPages
     */
    static public function getPageLine($iPage, $iCount, $iSectionId, $aURLParams = array(), $aParams = array()) {

        if ( $iPage<=0 )
            $iPage = 1;

        // init default settings
        $iOnPage = (isSet($aParams['onPage'])) ? $aParams['onPage'] : 10;
        $iOnGroup = (isSet($aParams['onGroup'])) ? $aParams['onGroup'] : 10;
        $bUseEdges = (isSet($aParams['useEdges'])) ? $aParams['useEdges'] : true;
        $bUseItems = (isSet($aParams['useItems'])) ? $aParams['useItems'] : true;
        $bUsePages = (isSet($aParams['usePages'])) ? $aParams['usePages'] : true;
        $bUseGroups = (isSet($aParams['useGroups'])) ? $aParams['useGroups'] : true;
        //$bUseShortKeys = (isSet($aParams['useShortKeys']))? $aParams['useShortKeys']: true;

        $aPages['goodId'] = (isSet($aParams['goodId'])) ? $aParams['goodId'] : '';

        if (!$iOnPage) return false;
        if (!count($aURLParams)) return false;

        reset($aURLParams);
        $aGet = array();
        list($sModuleName, $aURLParams) = each($aURLParams);

        if (count($aURLParams))
            foreach ($aURLParams as $sKey => $sValue)
                $aGet[] = $sKey . '=' . urlencode($sValue);

        $aPages['page'] = $iPage;
        $aPages['module'] = $sModuleName;
        $aPages['sectionId'] = $iSectionId;
        $aPages['parameters'] = (count($aGet)) ? implode('&', $aGet) : '';

        $iPagesCount = ceil($iCount / $iOnPage);
        if ($iPagesCount>1) {
            $oPage = Page::getRootModule();
            $sCanonical = Site::httpDomain() . Tree::getSectionAliasPath($iSectionId);
            if (!self::$bHideCanonicalPagination)
                $oPage->setData('canonical_pagination', $sCanonical);

        }
        //центральная активная страница
        $iCenterGroup = ceil($iOnGroup/2)+1;
        //первая страница
        $iCountFirst = ($iPage>$iCenterGroup)?$iPage-$iCenterGroup:0;
        $iCountFirst =(($iCount < $iCountFirst + $iOnGroup)&&($iPage>$iCenterGroup)) ? $iCount-$iOnGroup+2: $iCountFirst;


        if ($bUseItems)
            if ($iPagesCount) {
                $aPages['itemsIsActive'] = true;

                for ($i = $iCountFirst; $i < $iCountFirst + $iOnGroup; ++$i) {

                    if ($i == $iPagesCount) break;

                    $aItem['title'] = $i + 1;
                    $aItem['page'] = $i + 1;
                    $aItem['isActive'] = (($i + 1) == $iPage) ? true : false;
                    $aPages['items'][] = $aItem;
                }// each item
            }// if pages count

        if ($bUsePages) {

            $aPages['prevPage']['isActive'] = ($iPage > 1) ? true : false;
            $aPages['prevPage']['page'] = ($iPage > 1) ? $iPage - 1 : $iPage;
            $aPages['prevPage']['title'] = '<';

            $aPages['nextPage']['isActive'] = ($iPage < $iPagesCount) ? true : false;
            $aPages['nextPage']['page'] = ($iPage < $iPagesCount) ? $iPage + 1 : $iPage;
            $aPages['nextPage']['title'] = '>';
        }// use previos/next pageLinks

        if ($bUseEdges) {

            $aPages['firstPage']['isActive'] = ($iPage > 1) ? true : false;
            $aPages['firstPage']['page'] = 1;
            $aPages['firstPage']['title'] = \Yii::t('page', 'page_first');


            $aPages['lastPage']['isActive'] = ($iPage < $iPagesCount) ? true : false;
            $aPages['lastPage']['page'] = $iPagesCount;
            $aPages['lastPage']['title'] = \Yii::t('page', 'page_last');

        }// use edges Links

        return $aPages;
    }// func

}// class
