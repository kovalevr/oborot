<?php

namespace skewer\base\section;


/**
 * Класс для работы с разделами
 * Class Api
 * @package skewer\base\section
 */
class Api {

    /**
     * Список вложенных разделов в формате alias=>id
     * @var array
     */
    private static $aAliasSubList = [];

    /**
     * Разделитель пути
     * @var string
     */
    public static $sDelimiter = '/';

    /**
     * По псевдониму и базовому разделу
     * @param $sAlias
     * @param int $iBaseId
     * @return int
     */
    public static function getIdByAlias( $sAlias, $iBaseId = 0 ) {

        $sAlias = mb_strtolower(trim($sAlias), 'utf-8');

        if (!isset(static::$aAliasSubList[$iBaseId])){
            static::$aAliasSubList[$iBaseId] = static::getAliasSubList( $iBaseId );
        }

        return (isset(static::$aAliasSubList[$iBaseId][$sAlias])) ? static::$aAliasSubList[$iBaseId][$sAlias] : 0;

    }


    /**
     * Список вложенных разделов в формате alias=>id
     * @param $id
     * @param string $label
     * @return array
     */
    public static function getAliasSubList( $id, $label = '' ) {

        $out = array();

        $tree = Tree::getSubSections( $id );

        foreach ( $tree as $item ) {
            $currentLabel = ($label)
                ? $label . $item->title . self::$sDelimiter
                : $item->title . self::$sDelimiter;
            $out[mb_strtolower(trim($currentLabel, self::$sDelimiter), 'utf-8')] = $item->id;
            $out = array_merge( $out, self::getAliasSubList( $item->id, $currentLabel ) );
        }

        return $out;
    }


    /**
     * Добавление раздела
     * @param $iParent
     * @param $alias
     * @param $iTemplate
     * @return bool|int
     */
    public static function addSection( $iParent, $alias, $iTemplate ){

//        $oTree = new \Tree();
//
//        $iSection = $oTree->addSection( $iParent, $alias, $iTemplate );

        $oSection = Tree::addSection( $iParent, $alias, $iTemplate );

        if ( $oSection ) {

            $alias = mb_strtolower(trim($alias), 'utf-8');

            /** Добавляем во внутренние массивы */
            if (isset(static::$aAliasSubList[$iParent]))
                static::$aAliasSubList[$iParent][$alias] = $oSection->id;

            foreach( static::$aAliasSubList as &$aAliasList ){
                foreach( $aAliasList as $sAlias => $iKey ){
                    if ($iKey == $iParent){
                        $aAliasList[$sAlias. self::$sDelimiter .$alias] = $oSection->id;
                    }
                }
            }
        }

        return $oSection->id;
    }

}