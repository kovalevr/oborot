<?php

namespace skewer\base\orm;

use skewer\base\log\Logger;
use skewer\base\orm\service\ActiveRecordPrototype;
use skewer\base\ft;
use yii\base\Exception;


/**
 * Прототип для сущностей типа ActiveRecord
 * Class ActiveRecord
 * User: ilya
 * Date: 24.03.14
 * @package skewer\build\Component\Query
 */
class ActiveRecord extends ActiveRecordPrototype {

    /**
     * Флаг писать логи?
     * @var bool
     */
    public static $bWriteLogs = true;

    /**
     * Флаг логировать ли обновление записи
     * @var bool
     */
    protected static $bLogUpdate = true;

    /**
     * Флаг логированть ли создание записи
     * @var bool
     */
    protected static $bLogCreate = true;

    /**
     * Флаг логировать ли удаление записи
     * @var bool
     */
    protected static $bLogDelete = true;

    /**
     * Сохранение состояния записи
     * @return bool
     * todo exception нет примари кея
     */
    public function save() {

        $this->preSave();

        $sPKey = $this->primaryKey();

        // запустить валидаторы
        if ( !$this->executeValidators() )
            return false;

        $aData = $this->getData();

        // удаление значений фиктивных полей
        if ( $oModel = $this->getModel() )
            foreach( $oModel->getFileds() as $oField )
                if ( $oField->isFictitious() )
                    unset( $aData[$oField->getName()] );

        $oQuery = Query::InsertInto( $this->getTableName() );

        foreach( $aData as $sFieldName => $sValue )
            $oQuery->set( $sFieldName, $sValue );

        $oQuery->onDuplicateKeyUpdate();

        foreach( $aData as $sFieldName => $sValue )
                $oQuery->set( $sFieldName, $sValue );

        //echo $oQuery->getQuery();
        $res = $oQuery->get();
        $this->setWasUpdated($res);

        $this->afterSave(!(bool)$aData[$this->primaryKey()],$aData);

        if ( $this->compositePK() ) {

            // сохранение связанных полей
            $this->saveLinkedFields(); // todo это не будет работать для составного ключа

            return true;
        } else {
            if ( $res && ( !$this->$sPKey || $this->$sPKey=='NULL' ) )
                $this->$sPKey = $res;

            // сохранение связанных полей
            $this->saveLinkedFields();

            return (int)$this->$sPKey;
        }

    }


    /**
     * Удаление записи
     * @return bool
     */
    public function delete() {

        $this->preDelete();

        $sPKey = $this->primaryKey();
        if ( !isSet($this->$sPKey) )
            return false; // todo exception нет примари кея

        $res = Query::DeleteFrom( $this->getTableName() )->where( $sPKey, $this->$sPKey )->get();

        $this->afterDelete($this);
        return (bool)$res;
    }


    public function preSave() {}


    public function preDelete() {}


    /**
     * Создание новой записи по описанию полей
     * @param string $sTableName Имя таблицы
     * @param array $aFieldList Список полей для добавления в запись
     * @param array $aFieldDesc Описания полей
     * @return ActiveRecord
     */
    public static function init( $sTableName, $aFieldList, $aFieldDesc = array() ) {

        $oRow = new self();

        $oRow->setTableName( $sTableName );

        foreach( $aFieldList as $sField ) {

            $oRow->addField( $sField, isSet($aFieldDesc[$sField]) ? $aFieldDesc[$sField] : array() );

        }

        return $oRow;
    }


    /**
     * Создание новой записи по FT модели
     * @param ft\Model $oModel
     * @return ActiveRecord
     */
    public static function getByFTModel( ft\Model $oModel ) {

        $oRow = new static();

        $oRow->setModel( $oModel );
        $oRow->setTableName( $oModel->getTableName() );

        foreach( $oModel->getFileds() as $oField ) {

            $oRow->addField( $oField->getName(), array( 'default' => $oField->getDefault() ) );

        }

        return $oRow;
    }


    /**
     * Заполнение полей по массивы данных
     * @param array $aData
     * @return bool
     */
    public function load( $aData = [] ) {

        if ( empty( $aData ) )
            return false;

        foreach ( $aData as $sFieldName => $mValue ) {
            if ( isSet($this->$sFieldName) )
                $this->$sFieldName = $mValue;
            // todo модификация значения для поля со связью ><
        }

        return true;
    }


    /**
     * Задать набор значений
     * @param array $aData
     */
    public function setData( $aData = [] ) {

        if ( $this->load( $aData ) )
            $this->leadValues();

    }


    /**
     * Получеть набор значений как массив
     * @param bool $bByModel флаг вывода только полей модели
     * @return array
     */
    public function getData( $bByModel = true ) {

        $aData = array();
        $aFieldList = $this->getFieldDesc();

        if ( !empty($aFieldList) && $bByModel ) {

            foreach ( $aFieldList as $sFieldName => $aFieldDesc ) {
                if ( isSet($this->$sFieldName) )
                    $aData[$sFieldName] = $this->$sFieldName;
            }

        } else {
            foreach ( $this as $sFieldName => $aFieldDesc) {
                $aData[$sFieldName] = $this->$sFieldName;
            }
        }

        // todo приведение типов

        return $aData;
    }


    /**
     * Возвращает значение поля
     * @param string $sFieldName Имя поля
     * @param mixed $mDefValue Значение если не существует поле
     * @return mixed
     */
    public function getVal( $sFieldName, $mDefValue = '' ) {
        return isSet( $this->$sFieldName ) ? $this->$sFieldName : $mDefValue;
    }


    /**
     * Задает значение для поля
     * @param string $sFieldName Имя поля
     * @param mixed $mValue Значение поля
     * @param bool $bOnlyExist
     */
    public function setVal( $sFieldName, $mValue, $bOnlyExist = true ) {
        // todo валидация значения
        if ( isSet($this->$sFieldName) || !$bOnlyExist )
            $this->$sFieldName = $mValue;
    }


    /**
     * Применяет набор валидаторов к заданному набору данных
     * @return bool
     */
    protected function executeValidators() {

        $oModel = $this->getModel();

        if ( is_null($oModel) )
            return true;

        // валидация
        $bIsValid = true;
        $aErrorList = array();

        foreach ( $oModel->getFileds() as $oField ) {

            if ( !$oField->getAttr('active') )
                continue;

            foreach ( $oField->getValidatorList( $this ) as $oValidator ) {

                if ( !$oValidator->isValid() ) {
                    $bIsValid = false;
                    $aErrorList[ $oField->getName() ] = $oValidator->getErrorText();
                    break;
                }

            }

        }

        // очистка списка ошибок
        $this->clearErrors();

        // если ошибки есть
        if ( !$bIsValid ) {
            $this->setErrorList( $aErrorList );
            return false;
        }

        return true;

    }


    /**
     * Перевод объекта в строку
     * @return string
     */
    public function __toString() {
        $sKeyName = $this->primaryKey();
        return (string)$this->$sKeyName;
    }


    /**
     * Выполняет приводит к нужному типу и проверку данных
     * @return bool
     */
    private function leadValues() {

        $oModel = $this->getModel();

        if ( is_null($oModel) )
            return true;

        foreach ( $oModel->getFileds() as $sFieldName => $oField ) {

            if ( $oField->isFictitious() )
                continue;

            $this->$sFieldName = $this->getValidValue( $this->$sFieldName, $oField );

        }

        return true;
    }

    /**
     * Отдает валидное значение для заданного поля
     * @param mixed $mValue тукущее значение поля
     * @param ft\model\Field $oField описание поля
     * @return mixed
     */
    private function getValidValue( $mValue, ft\model\Field $oField ) {

        switch ( $oField->getDatatype() ) {
            case 'int':
                $mValue = (int)$mValue;
                break;
            case 'bool':
                $mValue = (bool)$mValue;
                break;
            case 'float':
                $mValue = (float)$mValue;
                break;
        }

        return $mValue;

    }

    public function afterSave($insert, $aData) {

        $sCurrentTable = $this->getTableName();

        if ($insert)
            $sTitle = 'Добавление записи';
        else
            $sTitle = 'Изменение записи';

        if ((($insert && static::$bLogCreate) || static::$bLogUpdate) && self::$bWriteLogs)
            Logger::addToLog($sTitle,json_encode($aData),'DB ('.$sCurrentTable.')',4,Logger::logUsers);

    }

    public function afterDelete($aData) {

        if (static::$bLogDelete) {
            $sCurrentTable = $this->getTableName();

            $sTitle = 'Удаление записи';

            if (self::$bWriteLogs)
                Logger::addToLog($sTitle, json_encode($aData), 'DB (' . $sCurrentTable . ')', 4, Logger::logUsers);
        }
    }


    public static function enableLogs(){
        self::$bWriteLogs = true;
    }

    public static function disableLogs(){
        self::$bWriteLogs = false;
    }

} 