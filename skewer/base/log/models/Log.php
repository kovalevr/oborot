<?php

namespace skewer\base\log\models;

use skewer\base\log\Logger;
use skewer\components\ActiveRecord\ActiveRecord;
use skewer\components\auth\CurrentAdmin;
use skewer\components\auth\models\Users;
use Yii;
use yii\data\ActiveDataProvider;
use yii\db\Query;

/**
 * This is the model class for table "log".
 *
 * @property integer $id
 * @property string $event_time
 * @property integer $event_type
 * @property integer $log_type
 * @property string $title
 * @property string $module
 * @property string $initiator
 * @property string $ip
 * @property string $proxy_ip
 * @property string $external_id
 * @property string $description
 * @method static Log findOne($condition)
 */
class Log extends ActiveRecord
{

    protected static $bLogUpdate = false;

    /**
     * Флаг логированть ли создание записи
     * @var bool
     */
    protected static $bLogCreate = false;

    /**
     * Флаг логировать ли удаление записи
     * @var bool
     */
    protected static $bLogDelete = false;

    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'log';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['event_time'], 'safe'],
            [['event_type', 'title', 'ip' ], 'required'],
            [['event_type', 'log_type'], 'integer'],
            [['description'], 'string'],
            [['title', 'module','initiator'], 'string', 'max' => 255],
            [['ip', 'proxy_ip'], 'string', 'max' => 255],
            [['external_id'], 'string', 'max' => 20]
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id'            => 'field_id',
            'event_time'    => 'field_event_time',
            'event_type'    => 'field_event_type',
            'log_type'      => 'field_log_type',
            'title'         => 'field_title',
            'module'        => 'field_module',
            'initiator'     => 'field_initiator',
            'ip'            => 'field_ip',
            'proxy_ip'      => 'field_proxy_ip',
            'external_id'   => 'field_external_id',
            'description'   => 'field_description',
        ];
    }


    public static function getItemsList($aInputData){

        // маска входного фильтра, он же массив данных для подстановки в запрос

        $f = [ // filter mask
            'limit.start'=> 0,
            'limit.count'=> 100,
            'order.field'=> 'id',
            'order.way'  => 'DESC',
            'login'      => false,
            'module'     => null,
            'event_type' => null,
            'log_type'   => null,
            'event_time.sign'  => null,
            'event_time.value' => null,
        ];


        // покрываем маску фильтра значениями из входного массива

        if(is_array($aInputData))
            foreach($aInputData as $k0=>$val0)
                if(is_array($val0))
                    foreach($val0 as $k1=>$val1)
                        $f[$k0.'.'.$k1]=$val1;
                else
                    $f[$k0]=$val0;

        /*Базовая выборка*/
        $q = (new Query())
            ->select("`log`.*, `log`.initiator as login")
            ->from('log');

        /*Добавляем логин*/
        if ($f['login']){
            /*Вытаскиваем ID пользователя по его логину*/
            $oUser = Users::find()
                ->where(['login'=>$f['login']])
                ->one();

            if (!is_null($oUser))
                /*Ищем по пользователю который существует*/
                $q->andWhere(['initiator'=>$oUser['login']]);
            else{
                //Определение по пользователю CanapeId
                if (isset($aInputData['login']) && strpos($aInputData['login'],'canape-id')!==false){
                    $q->andWhere(['initiator'=>$aInputData['login']]);
                } else {
                    /*Этого пользователя не существует, но выведем записи которые ему соответствуют*/
                    $q->andWhere(['initiator'=>$oUser['login']]);
                }
            }

        }

        /*Добавляем название модуля*/
        if ($f['module']){
            $q->andWhere(['module'=>$f['module']]);
        }

        /*Добавляем уровень события*/
        if ($f['event_type']){
            $q->andWhere(['event_type'=>$f['event_type']]);
        }

        /*Добавляем тип журнала*/
        if ($f['log_type']){
            $q->andWhere(['log_type'=>$f['log_type']]);
        }

        /*Добавляем временные рамки*/
        if($f['event_time.sign'] and $f['event_time.value']){

            if(($f['event_time.sign']=='BETWEEN') and is_array($f['event_time.value']))
                $timeQ = [
                    $f['event_time.sign'],
                       'event_time',
                    $f['event_time.value'][0],
                    $f['event_time.value'][1]
                ];
            else
                $timeQ = [
                    $f['event_time.sign'],
                       'event_time',
                    $f['event_time.value']
                ];

            $q->andWhere($timeQ);
        }

        // если не сисадмин - вырезаем системные записи из выборки
        if( !CurrentAdmin::isSystemMode() ){
            $q->andWhere("log_type != ".Logger::logSystem);
        }

        /*Дополняем сортировкой*/
        $q->orderBy($f['order.field'] . ' ' . $f['order.way']);

        // Выполняем запрос и собираем выходной массив

        $provider = new ActiveDataProvider([
            'query'      => $q,
            'pagination' => [
                'page'      => floor($f['limit.start'] / $f['limit.count']),
                'pageSize'  => $f['limit.count'],
            ],
        ]);

        return [
            'items' => $provider->getModels(),
            'count' => $provider->getTotalCount()
        ];

    }

}
