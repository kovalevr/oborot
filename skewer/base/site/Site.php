<?php

namespace skewer\base\site;

use skewer\base\section\Parameters;
use skewer\components\auth\CurrentAdmin;


/**
 * Класс, отвечающий за переменные, относящиеся к сайту
 */
class Site {

    /**
     * Отдает название сайта
     * @return string
     */
    public static function getSiteTitle() {

        return Parameters::getValByName(
            \Yii::$app->sections->languageRoot(),
            Parameters::settings, 'site_name', true
        );
    }

    /**
     * Отдает email админа
     * @return string
     */
    public static function getAdminEmail(){
        return Parameters::getValByName( \Yii::$app->sections->root(), Parameters::settings, 'email', false );
    }

    /**
     * Отдает email для отправки
     * @return string
     */
    public static function getNoReplyEmail(){
        return Parameters::getValByName( \Yii::$app->sections->root(), Parameters::settings, 'send_email', false );
    }


    /**
     * Отдает текущую версию сборки в виде 3.19.3(dev)
     *      при BUILDNUMBER = 0019m3(dev)
     * @return string
     */
    public static function getCmsVersion() {

        $sVersion = \Yii::$app->version;
        if ($sVersion == '1.0')
            $sVersion = BUILDNUMBER;
        $sVersion = ltrim($sVersion, '0');
        $sVersion = preg_replace('/^(\d+)m(\d+.*)/', '$1.$2', $sVersion);

        // режим файлового окружения сайта
        if ( CurrentAdmin::isSystemMode() ) {
            if (defined('SITE_MODE')) {
                $sMode = SITE_MODE;
            } else {
                if (INCLUSTER) {
                    if (USECLUSTERBUILD)
                        $sMode = \Yii::t('app', 'site_mode_cluster');
                    else
                        $sMode = \Yii::t('app', 'site_mode_cluster_own_files');
                } else {
                    $sMode = \Yii::t('app', 'site_mode_hosting');
                }
            }
            if ($sMode)
                $sMode = '/' . $sMode;
        } else {
            $sMode = '';
        }

        // тип конфигурации сайта
        $sType = \Yii::t('app', 'site_type_'.Type::getAlias());

        $sOut = sprintf('3.%s [%s%s]', $sVersion, $sType, $sMode);

        if ( CurrentAdmin::isSystemMode() ) {
            if (YII_ENV_DEV)
                $sOut .= ' {dev}';
            if (YII_DEBUG)
                $sOut .= ' {debug}';
        }

        return $sOut;
    }

    /**
     * Отдает основной домен для площадки <br />
     * Example: "www.domain.com"
     * @return string
     */
    public static function domain() {
        return \skewer\build\Tool\Domains\Api::getCurrentDomain();
    }

    /**
     * Отдает основной домен для площадки <br />
     * Example: "http://www.domain.com"
     * @return string
     */
    public static function httpDomain() {
        return WEBPROTOCOL . \skewer\build\Tool\Domains\Api::getCurrentDomain();
    }

    /**
     * Отадет домен с http и / в конце <br />
     * Example: "http://www.domain.com/"
     * @return string
     */
    public static function httpDomainSlash() {
        return self::httpDomain().'/';
    }

    /**
     * Вернёт протокол доступа к сайту: http:// или https://
     * @return string
     */
    public static function getWebProtocol(){
        return WEBPROTOCOL;
    }

    /**
     * Отдает директорию релиза без директории skewer на конце
     * @return string
     */
    public static function getReleaseRootPath() {
        return substr(RELEASEPATH, 0, strlen(RELEASEPATH)-7);
    }

    /**
     * Формирование url 
     * для модулей из обрасти Tool в админке
     * @param $sNameModule
     * @param string $sParam
     * @return string
     */
    public static function admToolUrl($sNameModule,$sParam = '') {
        $sLink = Site::httpDomainSlash() . "admin/#out.left.tools=$sNameModule;out.tabs=tools_$sNameModule;";
        if ($sParam)
            $sLink .= "init_tab=tools_$sNameModule;init_param=$sParam";
        return $sLink;
    }

}
