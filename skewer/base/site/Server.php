<?php

namespace skewer\base\site;
use skewer\components\gateway;

/**
 * Класс для работы с серверными данными
 */
class Server {

    /**
     * Отдает true если сервер, на котором работает данный сайт - nginx
     * Пока это только заготовка метода
     * @return bool
     */
    public static function isNginx() {
        if ( !isset($_SERVER['SERVER_SOFTWARE']) )
            return false;
        return (strpos($_SERVER['SERVER_SOFTWARE'],'nginx')!==false);
    }

    /**
     * Отдает true если сервер, на котором работает данный сайт - apache
     * Пока это только заготовка метода
     * @return bool
     */
    public static function isApache() {
        if ( !isset($_SERVER['SERVER_SOFTWARE']) )
            return false;
        return (strpos($_SERVER['SERVER_SOFTWARE'],'Apache')!==false);
    }

    /**
     * Возворащает состояние сервера (продакшн/тест)
     * @return bool
     * @throws gateway\Exception
     */
    public static function isProduction() {

        if (!INCLUSTER) return true;

        $oClient = gateway\Api::createClient();

        $bResultStatus = false;

        /** @noinspection PhpUnusedParameterInspection */
        $oClient->addMethod('HostTools', 'isProductionServer', array(), function ($mResult, $mError) use (&$bResultStatus) {

            $bResultStatus = $mResult;

        });

        if (!$oClient->doRequest()) return false;

        return $bResultStatus;
    }

}