<?php

namespace skewer\base\ft;

use skewer\components\catalog;
use Exception;
use skewer\base\orm\MagicTable;

/**
 * Класс для работы с кэшем ft
 * ver 2.00
 */

class Cache {

    /** @var array[] Список ft-моделей в виде массивов*/
    private static $aModelList = array();

    /**
     * Проверяет существует ли сущность
     * @static
     * @param string $sEntityName имя сущности
     * @return bool
     */
    public static function exists( $sEntityName ) {
        return isSet( self::$aModelList[$sEntityName] );
    }

    /**
     * Отдает объект-описание сущности
     * @static
     * @param int|string $sEntityName имя сущности
     * @throws Exception
     * @return Model
     */
    static public function get( $sEntityName ) {

        if ( !isSet( self::$aModelList[$sEntityName] ) ) {

            // запросник на сущность

            /** @var catalog\model\EntityRow $oEntityRow */
            if ( is_numeric( $sEntityName ) ) {

                $oEntityRow = catalog\model\EntityTable::find( $sEntityName );
            }
            else {

                $sValue = $sEntityName;
                if (strpos($sEntityName, 'c_') === 0)
                    $sValue = substr( $sEntityName, 2 );

                $oEntityRow = catalog\model\EntityTable::find()
                    ->where('name',$sValue)
                    ->getOne();

            }

            if ( empty($oEntityRow) )
                throw new Exception("Не найдена модель для сущности [$sEntityName]");

            $oModel = $oEntityRow->getModelFromCache();

            self::set( $sEntityName, $oModel );

            return clone $oModel;

        } else {

            $oConverter = new converter\Arr();

            return $oConverter->dataToFtModel( self::$aModelList[$sEntityName] );

        }

    }

    /**
     * Записать в кэш описание модели
     * @static
     * @param $sEntityName
     * @param Model $oModel
     */
    static public function set( $sEntityName, Model $oModel ) {

        $oConverter = new converter\Arr();

        self::$aModelList[$sEntityName] = $oConverter->ftModelToData( $oModel );

    }

    /**
     * Загрузка системный сущностей
     * @param $sEntityName
     * @param $sNameSpace
     */
    public static function loadSystemEntity ( $sEntityName, $sNameSpace ) {

        self::set( $sEntityName, self::get($sNameSpace.'\\'.$sEntityName) );

    }

    /**
     * Получение класса для работы с сущностью
     * @param string $sModelName Имя сущности
     * @return \skewer\base\orm\MagicTable
     */
    static public function getMagicTable( $sModelName ) {

        $oModel = self::get( $sModelName );

        // todo запроксировать объекты через хранилище

        $oTable = MagicTable::init( $oModel );

        return $oTable;
    }

}
