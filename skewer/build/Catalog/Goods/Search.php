<?php


namespace skewer\build\Catalog\Goods;

use skewer\base\orm\Query;
use skewer\components\search\models\SearchIndex;
use skewer\build\Page\CatalogViewer\Module as PageModule;
use skewer\components\search\Prototype;
use skewer\components\catalog;
use skewer\base\section\Parameters;
use skewer\base\section\Tree;
use skewer\base\SysVar;
use skewer\components\catalog\Parser;

class Search extends Prototype{

    const CLASS_NAME = 'CatalogViewer';

    /**
     * отдает имя идентификатора ресурса для работы с поисковым индексом
     * @return string
     */
    public function getName() {
        return self::CLASS_NAME;
    }

    /**
     * @inheritdoc
     */
    protected function update(SearchIndex $oSearchRow) {
        $oSearchRow->class_name = $this->getName();

        if (!$oSearchRow->object_id)
            return false;

        // todo здесь нузно знать с какой базовой карточкой мы работаем
        // если появится множество базовых карточек, то можно применить подход с
        //      вычислением базовой карточки как в коллекциях
        $iBaseCard = 1;

        $oGoods = catalog\Card::getItemRow( $iBaseCard, ['id' => $oSearchRow->object_id] );

        /*Если нет товаров - выходим*/
        if (!$oGoods) return false;

        // Если отключены модификации, то не включать в индекс и карту сайта модификации товара
        if (!SysVar::get('catalog.goods_modifications')) {
            
            $bIsModifications = (bool)catalog\model\GoodsTable::find()
                ->where('base_id', $oSearchRow->object_id)
                ->where('parent !=?', $oSearchRow->object_id)
                ->asArray()
                ->getOne();

            $oSearchRow->use_in_search = false;
            $oSearchRow->use_in_sitemap = false;

            if ($bIsModifications) return $oSearchRow->save();
        }

        // Если у товара слетел базовый раздел (например удалён), то здесь произведётся попытка установить новый
        $iSectionId = catalog\Section::getMain4Goods( $oSearchRow->object_id );

        if (!$iSectionId)
            return false;

        $oSearchRow->section_id = $iSectionId;

        // проверка существования раздела и реального url у него
        $oSection = Tree::getSection( $oSearchRow->section_id );
        if ( !$oSection || !$oSection->hasRealUrl() )
            return false;

        /* У длинных артикулов заиндексировать первые 3 символа для возможности поиска по ним */
        $sArticleSearch = $oGoods->article;
        if ($aArticleParts = explode(' ', $oGoods->article)) {
            if (mb_strlen($aArticleParts[0]) > 3)
                $sArticleSearch = mb_substr($aArticleParts[0], 0, 3) . ' ' . $sArticleSearch;
        }

        // todo пересмотреть формирование текста - должно быть по галочке
        $sText = trim(
            "$sArticleSearch".$this->getAllSearchText($oSearchRow->object_id)
        );

        /** условия для удаления товара из поискового индекса */
        if ( !isSet($oGoods->active) || !$oGoods->active )
            return false;

        /**
         * @todo добавляться должны несколько записей ???
         */
        $oSearchRow->language      = Parameters::getLanguage($iSectionId);
        $oSearchRow->search_text   = $this->stripTags($sText);
        $oSearchRow->search_title  = $this->stripTags($oGoods->title);
        $oSearchRow->href          = Parser::buildUrl($iSectionId, $oSearchRow->object_id, $oGoods->alias);
        $oSearchRow->status        = 1;
        $oSearchRow->use_in_search = true;
        $oSearchRow->use_in_sitemap = true;

        if (catalog\Card::isDetailHidden($oSearchRow->section_id)) {
            $oSearchRow->use_in_sitemap = false;
        }

        $oSeoComponent = new SeoGood($oSearchRow->object_id, $oSection->id);
        $oSeoComponent->loadDataEntity();
        $this->fillSearchRowSeoData( $oSearchRow, $oSeoComponent );

        $oSearchRow->modify_date = catalog\model\GoodsTable::getChangeDate($oSearchRow->object_id,$iBaseCard);

        return $oSearchRow->save();

    }

    /**
     *  Склеивание строк для поискового индекса
     * @param int $idGoods
     * @return string $aSearchData
     */
    private function getAllSearchText($idGoods) {

        $oGoodsOne = catalog\GoodsRow::get($idGoods);
        $aFields = $oGoodsOne->getFields();
        $aSearchData = '';

        if ($oGoodsOne) {
            $aGoods = $oGoodsOne->getData();
            foreach ($aFields as $sName => $aField) {
                $aAttr = $aField->getAttrs();
                if (isset($aAttr['show_in_search'])&&$aAttr["show_in_search"] && !is_array($aGoods[$sName])) {
                    $aSearchData .= ($aGoods[$sName])?" ".$this->stripTags($aGoods[$sName]):'';
                }
            }
        }
        return $aSearchData;
    }

    /**
     *  воссоздает полный список пустых записей для сущности, отдает количество добавленных
     */
    public function restore() {
        $sql = "INSERT INTO search_index(`status`,`class_name`,`object_id`)  SELECT '0','{$this->getName()}',base_id  FROM c_goods WHERE 1";
        Query::SQL($sql);
    }

    public static function rebuildSearchByCardName($sCardName,$sValue){

        $sNameCard = ( $sCardName == catalog\Card::DEF_BASE_CARD )?  "co_$sCardName": "ce_$sCardName";
        \Yii::$app->db->createCommand("UPDATE search_index
                                        SET
                                        use_in_sitemap = '$sValue'
                                        where object_id in
                                        (
                                          select id from
                                          $sNameCard
                                        )")->execute();
    }

}