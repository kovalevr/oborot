<?php
/**
 * Created by PhpStorm.
 * User: holod
 * Date: 10.01.2017
 * Time: 18:35
 */

namespace skewer\build\Catalog\Goods\view;

use skewer\build\Catalog\Goods\model\GoodsEditor;
use skewer\components\ext\view\FormView;

class LoadSection extends FormView
{
    public $sMainLinkField;
    public $aSectionList;
    public $iSection;
    /**
     * Выполняет сборку интерфейса
     * @return void
     */
    function build() {
        $this->_form
            ->fieldSelect($this->sMainLinkField,  '', $this->aSectionList, [], false)
            ->setValue([
                GoodsEditor::mainLinkField => $this->iSection
            ]);

    }
}