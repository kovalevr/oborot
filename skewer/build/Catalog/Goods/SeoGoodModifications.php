<?php
namespace skewer\build\Catalog\Goods;


use skewer\base\site\Site;
use skewer\components\catalog;
use skewer\components\seo\Template;

class SeoGoodModifications extends SeoGood{

    /**
     * id базового товара
     * @var int
     */
    protected $iBaseGoodId = null;

    /**
     * @inheritdoc
     */
    public static function getAlias(){
        return 'catalogDetail2Layer';
    }

    /**
     * Установить id базового товара
     * @param $iBaseGoodId
     */
    public function setBaseGoodId($iBaseGoodId){
        $this->iBaseGoodId = $iBaseGoodId;
    }

    /**
     * Получить id базового товара
     * @return int | null
     */
    public function getBaseGoodId(){
        return $this->iBaseGoodId;
    }

    /**
     * Метод вернет $iPosition запись сущности
     * Позиция записи вычисляется относительно отсортированного списка товаров модификаций базового товара
     * !!! Перед вызовом необходимо установить id базового товара [[ setBaseGoodId ]]
     * @param $iPosition - позиция записи. Отсчет ведется с нуля
     * @return array | bool - массив с данными Ar сущности и SEO данными. false - если запись не найдена
     */
    public function getRecordWithinEntityByPosition($iPosition){

        $aResult = catalog\GoodsSelector::getModificationList($this->iBaseGoodId)
            ->limit(1,$iPosition + 1)
            ->parse();

        if (!isset($aResult[0]))
            return false;

        $aGood = $aResult[0];

        $this->setDataEntity($aGood);
        $this->setExtraAlias($aGood['card']);

        $aRow = array_merge($aGood,[
            'url' => catalog\Parser::buildUrl($this->iSectionId, $aGood['id'], $aGood['alias']),
            'seo' => $this->parseSeoData( ['sectionId' => $this->iSectionId] )
        ]);

        return $aRow;

    }

    /**
     * @inheritdoc
     */
    public function getIndividualTemplate4Section(){
        return Template::getByAliases(parent::getAlias(), $this->iSectionId);
    }

}