<?php

namespace skewer\build\Catalog\Dictionary;

use skewer\base\site_module\Context;
use skewer\build\Catalog\CardEditor;
use skewer\build\Catalog\LeftList\ModuleInterface;
use skewer\components\catalog;
use skewer\base\ui;
use skewer\base\ft;

/**
 * Модуль для редактирования записей в каталоге
 * Class Module
 * @package skewer\build\Catalog\Dictionary
 */
class Module extends \skewer\build\Tool\Dictionary\Module implements ModuleInterface{


    public function __construct(Context $oContext ) {
        parent::__construct( $oContext );
        $oContext->setModuleName('Dictionary');
        $oContext->setModuleWebDir('/skewer/build/Tool/Dictionary');
        $oContext->setModuleDir(RELEASEPATH.'build/Tool/Dictionary');
    }

} 