<?php

namespace skewer\build\Catalog\Settings;

use skewer\base\site\Layer;
use skewer\build\Page\RecentlyViewed;
use skewer\build\Catalog\LeftList\ModulePrototype;
use skewer\base\SysVar;
use yii\base\UserException;
use skewer\components\catalog;
use skewer\components\seo\Service;

/**
 * Модуль настройки каталога
 */
class Module extends ModulePrototype {

    public function actionInit() {

        $goodsInclude = SysVar::get('catalog.goods_include');
        $goodsRelated = SysVar::get('catalog.goods_related');
        $goodsRecentlyViewed = SysVar::get('catalog.goods_recentlyViewed');
        $bHidePriceFractional = SysVar::get('catalog.hide_price_fractional');
        $iCountShowGoods = SysVar::get('catalog.countShowGoods');
        $iParametricSearch = SysVar::get('catalog.parametricSearch');
        $iShadowParamFilter = SysVar::get('catalog.shadow_param_filter');

        $iGuestBookShow = SysVar::get('catalog.guest_book_show');
        $sCurrencyType = SysVar::get('catalog.currency_type');
        $iShowRating = SysVar::get('catalog.show_rating');
        $iRandomRelated = SysVar::get('catalog.random_related');
        $iRandomRelatedCount = SysVar::get('catalog.random_related_count');
        $iHideReviewForm = SysVar::get('catalog.hide_review_form');

        if ( ($goodsModifications = SysVar::get('catalog.goods_modifications')) == null )
            $goodsModifications = 0;

        $aData = [
            'sGoodsInclude' => $goodsInclude,
            'sGoodsRelated' => $goodsRelated,
            'sGoodsModifications' => $goodsModifications,
            'bHidePriceFractional' => $bHidePriceFractional,
            'iCountShowGoods' => $iCountShowGoods,
            'iParametricSearch' => $iParametricSearch,
            'iShadowParamFilter' => $iShadowParamFilter,
            'iGuestBookShow' => $iGuestBookShow,
            'sCurrencyType' => $sCurrencyType,
            'iShowRating' => $iShowRating,
            'iRandomRelated' => $iRandomRelated,
            'iRandomRelatedCount' => $iRandomRelatedCount,
            'iHideReviewForm'=>$iHideReviewForm
        ];

        if ( \Yii::$app->register->moduleExists( RecentlyViewed\Module::getNameModule(), Layer::PAGE ) ) {
            $aData['bRecentlyViewedInstalled'] = true;
            $aData['bGoodsRecentlyViewed'] = $goodsRecentlyViewed;
        }

        $this->render(new view\Index($aData));
    }

    public function actionSave(){

        $countShowGoods = $this->getInDataValInt('countShowGoods');

        if ($countShowGoods <= 0)
            throw new UserException(\Yii::t('catalog', 'countShowGoodsError'));

        if ($this->getInDataValInt('random_related_count') <=0 )
            throw new UserException(\Yii::t('catalog', 'countShowGoodsError'));

        // Если были включены или выключены аналоги(модификации), то обновить поисковой индекс и карту сайта
        if ((int)$this->getInDataVal('goods_modifications') != (int)SysVar::get('catalog.goods_modifications')) {

            // ! Этот параметр нужно установить раньше обновления поискового индекса товара, т. к. используется там
            SysVar::set('catalog.goods_modifications', (int)$this->getInDataVal('goods_modifications'));

            catalog\Api::deactiveModificationsFromSearch();
            Service::makeSearchIndex();
            Service::updateSiteMap();
        }

        if ( ( $bGoodsRecentlyViewed =  $this->getInDataVal('bGoodsRecentlyViewed', null)) !== null )
            SysVar::set('catalog.goods_recentlyViewed', (int) $bGoodsRecentlyViewed );

        SysVar::set('catalog.goods_include',(int)$this->getInDataVal('goods_include'));
        SysVar::set('catalog.goods_related', (int)$this->getInDataVal('goods_related'));
        SysVar::set('catalog.hide_price_fractional', (int)$this->getInDataVal('hide_price_fractional'));
        SysVar::set('catalog.parametricSearch', (int)$this->getInDataVal('parametric_search'));
//        SysVar::set('catalog.hide2lvlGoodsLinks', (int)$this->getInDataVal('hide2lvlGoodsLinks'));
//        SysVar::set('catalog.hideBuy1lvlGoods', (int)$this->getInDataVal('hideBuy1lvlGoods'));
        SysVar::set('catalog.shadow_param_filter', (int)$this->getInDataVal('shadow_param_filter'));

        SysVar::set('catalog.countShowGoods', $countShowGoods);
        SysVar::set('catalog.guest_book_show', (int)$this->getInDataVal('guest_book_show'));
        SysVar::set('catalog.currency_type', $this->getInDataVal('currency_type'));
        SysVar::set('catalog.show_rating', (int)$this->getInDataVal('show_rating'));
        SysVar::set('catalog.random_related', (int)$this->getInDataValInt('random_related'));
        SysVar::set('catalog.random_related_count', (int)$this->getInDataValInt('random_related_count'));
        SysVar::set('catalog.hide_review_form', (int)$this->getInDataValInt('hide_review_form'));
        \Yii::$app->router->updateModificationDateSite();

        $this->actionInit();
    }


}