<?php

namespace skewer\build\Page\CategoryViewer;

use skewer\base\log\Logger;
use skewer\components\config\InstallPrototype;

class Install extends InstallPrototype {

    public function init() {
        return true;
    }

    public function install() {

        $sGroupParam = 'CategoryViewer';

        $iMainTplId = \Yii::$app->sections->tplNew();

        $this->addParameter( $iMainTplId, 'object', 'CategoryViewer', '', $sGroupParam );
        $this->addParameter( $iMainTplId, '.title', 'categoryViewer.title', '', $sGroupParam );
        $this->addParameter( $iMainTplId, 'layout', 'content', '', $sGroupParam );

        return true;
    }

    public function uninstall() {

        $sGroupParam = 'CategoryViewer';

        $iMainTplId = \Yii::$app->sections->tplNew();

        $this->removeParameter( $iMainTplId, 'object', $sGroupParam );
        $this->removeParameter( $iMainTplId, '.title', $sGroupParam );
        $this->removeParameter( $iMainTplId, 'layout', $sGroupParam );

        return true;
    }
}