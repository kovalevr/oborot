<?php

namespace skewer\build\Page\CategoryViewer;


use skewer\base\section\models\TreeSection;
use skewer\base\section\Parameters;
use skewer\base\SysVar;
use skewer\components\gallery\Photo;
use yii\helpers\ArrayHelper;
use skewer\base\section\Tree;
use skewer\base\site_module;

class Module extends site_module\page\ModulePrototype {

    /** @var int нужно выводить подразделы */
    public $category_parent = 0;

    /** @var int раздел из которого выводить. 0 - текущий */
    public $category_from = 0;

    /**
     * @inheritdoc
     */
    public function autoInitAsset() {
        return false;
    }

    public function execute() {

        if ( !$this->category_parent )
            return psComplete;

        $iFromSection = $this->category_from ? $this->category_from : $this->sectionId();

        /** @var array[] $aSections полный набор разделов */
        $aSections = Tree::getSubSections( $iFromSection, true );

        foreach ($aSections as $sKey => $aSection) {
            if (!ArrayHelper::getValue($aSection, 'visible', 0)){
                unset($aSections[$sKey]);
            }
        }

        if ( empty( $aSections ) )
            return psComplete;


        /** @var int[] $aSectionList полный набор идентификаторов разделов */
        $aSectionList = array_keys($aSections);

        if ( !$aSectionList )
            return psComplete;

        // ресурсы подключаются только если нужен вывод
        // при переводе на php шаблонны должно быть перенесено в них
        Asset::register(\Yii::$app->view);

        /** @var string $sParamGroup Группа параметров модуля */
        $sParamGroup = $this->getConfigParam('param_group');

        $aShowValList = Parameters::getList( $aSectionList )->group( $sParamGroup)->name('category_show')->asArray()->get();
        $aShowValList = ArrayHelper::map( $aShowValList, 'parent', 'value' );
        $aImgList = Parameters::getList( $aSectionList )->group( $sParamGroup)->name('category_img')->asArray()->get();
        $aImgList = ArrayHelper::map( $aImgList, 'parent', 'value' );
        $sTextSectionList = Parameters::getList( $aSectionList )->group( $sParamGroup)->name('category_description')->asArray()->get();
        $sTextSectionList = ArrayHelper::map( $sTextSectionList, 'parent', 'show_val' );
        $aAltTitleList = Parameters::getList( $aSectionList )->group('title')->name('altTitle')->asArray()->get();
        $aAltTitleList = ArrayHelper::map( $aAltTitleList, 'parent', 'value' );

        $aOutList = array();
        // перебираем все разделы
        foreach ( $aSections as $aSect ) {

            $iSectId = (int)$aSect['id'];

            // проверяем флаг активности
            if ( !isset($aShowValList[$iSectId]) or !(int)$aShowValList[$iSectId] )
                continue;

            if (isset($aAltTitleList[$iSectId]) && $aAltTitleList[$iSectId])
                    $aSect['title']  = $aAltTitleList[$iSectId];

            $aSect['description'] = isset($sTextSectionList[$iSectId]) ? $sTextSectionList[$iSectId] : '';

            // добавляем изображение, если есть
            if (isset($aImgList[$iSectId]) && $aPhoto = Photo::getFromAlbum($aImgList[$iSectId], true, 1)) {
                $aSect['img'] = $aPhoto[0]['attributes'];
            } else {
                $aSect['img'] = '';
            }

            $aSect['href'] = ($aSect['link']) ? $aSect['link'] : '['.$iSectId.']';

            $aOutList[] = $aSect;
        }

        \Yii::$app->router->setLastModifiedDate(TreeSection::getMaxLastModifyDate());

        $this->setData( 'list', $aOutList );

        if ( SysVar::get('Page.type_list_of_sections') == 'under_image' )
            $this->setTemplate( 'gallery.twig' );
        else
            $this->setTemplate( 'gallery2.twig' );

        return psComplete;
    }
}