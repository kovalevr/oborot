<?php

namespace skewer\build\Page\MainBanner;

use skewer\build\Adm\Slider;
use skewer\base\site_module;
use yii\helpers\ArrayHelper;
use yii\helpers\Json;


/**
 * Модуль вывода банера в шапке
 */
class Module extends site_module\page\ModulePrototype {

    public function init() {

        $this->setParser(parserTwig);

    }


    public function execute() {

        /* получаем список доступных баннеров */
        $aBanners = Slider\Banner::getBanner4Section( $this->sectionId() );

        /* выбираем баннер */
        $iBannerCount = count($aBanners);

        if( !$iBannerCount )
            return psBreak;

        // todo написать ротацию на сессии
        $iBannerId = rand(0,$iBannerCount-1);

        $aCurrentBanner = $aBanners[$iBannerId];
        $iBannerId = $aBanners[$iBannerId]['id'];

        \Yii::$app->router->setLastModifiedDate($aCurrentBanner['last_modified_date']);

        /* получаем список слайдов выбранного банера */
        $aSlides = Slider\Slide::getSlides4Banner( $iBannerId );

        $aBannerTools = Slider\Banner::getAllTools( $aCurrentBanner );

        $this->setData('configArray', $aBannerTools);
        $this->setData('config', Json::htmlEncode($aBannerTools));
        $this->setData('aMinHeight', Json::htmlEncode(ArrayHelper::getValue($aBannerTools, 'height_limits', '')));

        $this->setData('banner', $aSlides);
        $this->setData('aDimensionsFirstImage', Slider\Slide::getDimensionsFirstImage($aSlides));

        $this->setTemplate('banner.twig');

        return psComplete;
    }

}
