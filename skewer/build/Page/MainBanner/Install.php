<?php

namespace skewer\build\Page\MainBanner;
use skewer\components\config\InstallPrototype;

/**
 *
 * @class MainBannerInstall
 *
 * @author kolesnikov, $Author: $
 * @version $Revision: $
 * @date $Date: $
 * @package kernel
 */
class Install extends InstallPrototype {
    public function init() {
        return true;
    }// func

    public function install() {
        return true;
    }// func

    public function uninstall() {
        return true;
    }// func
}
