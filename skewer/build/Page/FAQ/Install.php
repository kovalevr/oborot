<?php

namespace skewer\build\Page\FAQ;

use skewer\components\config\InstallPrototype;
use skewer\components\forms;

class Install extends InstallPrototype {

    public function init() {

        return true;
    }// func

    public function install() {

        if (forms\Table::getByName('form_faq'))
            return true;

        /* Создание формы */
        $oForm = forms\Table::getNewRow();

        $oForm->form_name         = 'form_faq';
        $oForm->form_title        = $this->lang('form_title');
        $oForm->form_handler_type = forms\Entity::TYPE_TOMETHOD;
        $oForm->form_sys          = 1;
        $oForm->form_active       = 1;
        $oForm->form_captcha      = 1;
        $oForm->save();

        /* Создание полей формы */

        forms\FieldTable::getNewRow([
            'form_id'               => $oForm->form_id,
            'param_name'            => 'name',
            'param_title'           => 'faq.name',
            'param_type'            => 1, // @todo Сделать константой
            'param_required'        => 0,
            'param_maxlength'       => 255,
            'width_factor'          => 1,
            'param_validation_type' => 'text', // @todo Сделать константой
            'param_priority'        => 1,
            'label_position'        => 'top', // @todo Сделать константой
            'new_line'              => 1,
        ])->save();

        forms\FieldTable::getNewRow([
            'form_id'               => $oForm->form_id,
            'param_name'            => 'email',
            'param_title'           => 'faq.email',
            'param_type'            => 1, // @todo Сделать константой
            'param_required'        => 1,
            'param_maxlength'       => 255,
            'width_factor'          => 1,
            'param_validation_type' => 'email', // @todo Сделать константой
            'param_priority'        => 2,
            'label_position'        => 'top', // @todo Сделать константой
            'new_line'              => 0,
        ])->save();

        forms\FieldTable::getNewRow([
            'form_id'               => $oForm->form_id,
            'param_name'            => 'city',
            'param_title'           => 'faq.city',
            'param_type'            => 1, // @todo Сделать константой
            'param_required'        => 0,
            'param_maxlength'       => 255,
            'width_factor'          => 1,
            'param_validation_type' => 'text', // @todo Сделать константой
            'param_priority'        => 3,
            'label_position'        => 'top', // @todo Сделать константой
            'new_line'              => 0,
        ])->save();

        forms\FieldTable::getNewRow([
            'form_id'               => $oForm->form_id,
            'param_name'            => 'content',
            'param_title'           => 'faq.content',
            'param_type'            => 2, // @todo Сделать константой
            'param_required'        => 1,
            'param_maxlength'       => 500,
            'width_factor'          => 1,
            'param_validation_type' => 'text', // @todo Сделать константой
            'param_priority'        => 4,
            'label_position'        => 'top', // @todo Сделать константой
            'new_line'              => 1,
        ])->save();

        return true;
    }// func

    public function uninstall() {
        return true;
    }// func

}//class