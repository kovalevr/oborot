<?php

namespace skewer\build\Page\CatalogFilter;


use skewer\components\catalog\Attr;
use skewer\base\orm\Query;
use skewer\base\site\RobotsInterface;

class Robots implements RobotsInterface{

    public static function getRobotsDisallowPatterns(){

        /**
         * собираем список полей, выводимых в фильтре
         * @todo наверное можно по-другому
         */
        $oQuery = Query::SQL('
             SELECT field.`name`
             FROM `c_field` as `field` LEFT JOIN `c_field_attr` AS attr
             ON attr.`field`=`field`.`id`
             WHERE attr.`tpl`=:tpl
             AND attr.`value`=1;', ['tpl' => Attr::SHOW_IN_FILTER])
        ;

        $aResult = array();

        while ($aRow = $oQuery->fetchArray()){
            $aResult[] = '/*' . $aRow['name'] . '=';
        }

        return $aResult;
    }

    public static function getRobotsAllowPatterns(){
        return false;
    }
}