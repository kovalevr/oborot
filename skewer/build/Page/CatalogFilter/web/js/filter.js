$(function(){

    $('.js_slider').each(function() {

        var
            me = this,
            oInpMin = $( 'input[name$="[min]"]', $( me ).parents( '.js_filter_inputwrap')),
            oInpMax = $( 'input[name$="[max]"]', $( me ).parents( '.js_filter_inputwrap')),
            v_min = parseInt( oInpMin.val() ),
            v_max = parseInt( oInpMax.val() ),
            l_min = parseInt( oInpMin.attr( 'def' ) ),
            l_max = parseInt( oInpMax.attr( 'def' ) );

        $( me ).slider({
            range: true,
            min: l_min,
            max: l_max,
            values: [ v_min, v_max ],
            slide: function( event, ui ) {
                oInpMin.val( ui.values[ 0 ] );
                oInpMax.val( ui.values[ 1 ] );
            }
        });

        oInpMin.change( function() {

            var
                cur_min = parseInt( oInpMin.val() ),
                cur_max = parseInt( oInpMax.val() );

            if ( cur_min > cur_max ) {
                cur_min = cur_max - 1;
                oInpMin.val( cur_min );
            }


            $( me ).slider( { values: [ cur_min, cur_max ] } );

        });

        oInpMax.change( function() {

            var
                cur_min = parseInt( oInpMin.val() ),
                cur_max = parseInt( oInpMax.val() );

            if ( cur_min > cur_max ) {
                cur_max = cur_min + 1;
                oInpMax.val( cur_max );
            }


            $( me ).slider( { values: [ cur_min, cur_max ] } );

        });

    });


    $('#form_cat_filter').submit( function() {

        var view = skCatFilter.getParam( 'view' );
        var sort = skCatFilter.getParam( 'sort' );
        var way = skCatFilter.getParam( 'way' );

        if ( view != undefined )
            $('#form_cat_filter').append('<input type="hidden" name="view" value="'+view+'">');

        if ( sort != undefined )
            $('#form_cat_filter').append('<input type="hidden" name="sort" value="'+sort+'">');

        if ( way != undefined )
            $('#form_cat_filter').append('<input type="hidden" name="way" value="'+way+'">');

        return true;
    });

    $('.js_clear_catalog_filter').click( function() {
        $( '#form_cat_filter input.js_fl_str' ).val( '' );
        $( '#form_cat_filter option' ).prop( 'selected', false );
        $( '#form_cat_filter input[type=checkbox]' ).prop( 'checked', false );
        $( '#form_cat_filter input.js_fl_num' ).each( function() {
            $( this ).val( $( this ).attr( 'def' ) );
        });
        $( '#form_cat_filter' ).submit();
        return false;
    });

});
