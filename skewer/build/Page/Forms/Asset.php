<?php

namespace skewer\build\Page\Forms;


use yii\web\AssetBundle;
use yii\web\View;

class Asset extends AssetBundle {
    
    public $sourcePath = '@skewer/build/Page/Forms/web/';

    public $css = [
//        'css/form_custom.css',
        'css/forms.css',
        'css/jquery-ui-datepicker-theme.css',
        'css/select2.css',
    ];

    public $js = [
        'js/jquery.inputmask.min.js',
        'js/formValidator.js',
        'js/jquery.validate.min.js',
        'js/select2.full.min.js',
        'js/jquery.custom-file-input.js',
        'js/form-init.js'
    ];

    public $jsOptions = [
        'position'=>View::POS_HEAD
    ];

    public $depends = [
        'skewer\libs\jquery\Asset',
        'skewer\libs\datepicker\Asset',
    ];

    public function init(){

        $this->js[] = 'js/message_' . \Yii::$app->language . '.js';

        parent::init();

    }

}