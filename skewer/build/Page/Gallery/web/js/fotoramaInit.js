$(function() {

    var fotoramaContainer = $('.js-fotorama');

    fotoramaContainer.fotorama({
        nav: "thumbs",
        width: "100%",
        allowfullscreen: true,
        loop: true,
        arrows: true,
        keyboard: true,
        maxheight: 600
    });

});
