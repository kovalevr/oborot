$(function() {

    // gallery tile cols init
    var jqGalleryTile = $('.js-gallery-tile'),
        backendOptions = jqGalleryTile.data('config');

    jqGalleryTile.justifiedGallery( getConfig(backendOptions) );

    jqGalleryTile.justifiedGallery().on('jg.resize', function () {
        $(this).justifiedGallery( getConfig(backendOptions) );
    });

    /**
     * Конфиг для инициализации библиотеки
     * @param {Object} backendOptions - опции принятые с сервера
     */
    function getConfig( backendOptions ){

        return {
            margins: 5,
            rowHeight: adaptive.isMobile() ? 130 : backendOptions.rowHeight,
            maxRowHeight: adaptive.isMobile() ? 130 : backendOptions.maxRowHeight,
            captions: false,
            cssAnimation: true,

            thumbnailPath: function (currentPath, width, height, image) {

                /** width, height - размеры текущей превьюшки(размеры высчитываются самой библиотекой)*/
                var jqImage = $(image),
                    oImageData = jqImage.data('images'),
                    path = currentPath,
                    diffHeight = [],
                    formats = [],
                    /** @var Наиболее релевантный формат(с наименьшими потерями качества) для выделенного под изображение места */
                    relevantFormat,
                    orientation = 'horizontal'
                ;

                if ( oImageData[currentPath].hasOwnProperty('original') ){
                    if ( oImageData[currentPath]['original']['height'] >= oImageData[currentPath]['original']['width'] )
                        orientation = 'vertical';
                }

                for (var format in oImageData[currentPath]) {

                    var side = 'width',
                        sideValue;

                    if (orientation == 'horizontal'){
                        side = 'width';
                        sideValue = width;
                    } else {
                        side = 'height';
                        sideValue = height;
                    }

                    var diff = oImageData[currentPath][format][side] - sideValue;
                    if (diff > 0) {
                        formats.push(format);
                        diffHeight.push(diff);
                    }
                }

                // Раскомментировать для отладки
                // console.log(image);
                // console.log(formats, diffHeight);
                // console.log('selected format - ' + relevantFormat);
                // console.log('selected image' + path);
                //
                // for ( var format in oImageData[currentPath] ){
                //     console.log(oImageData[currentPath][format]['file']);
                // }

                if (!diffHeight.length)
                    return currentPath;

                relevantFormat = formats[diffHeight.indexOf(Math.min.apply(null, diffHeight))];
                path = oImageData[currentPath][relevantFormat]['file'];

                return path;
            }
        };
    }


});// ready