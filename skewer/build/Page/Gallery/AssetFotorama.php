<?php

namespace skewer\build\Page\Gallery;

use yii\web\AssetBundle;
use yii\web\View;

class AssetFotorama extends AssetBundle {

    public $sourcePath = '@skewer/build/Page/Gallery/web/';

    public $js = [
        'js/fotoramaInit.js',
    ];

    public $depends = [
        'skewer\libs\fotorama\Asset',
    ];

    public $jsOptions = [
        'position'=> View::POS_HEAD
    ];

}