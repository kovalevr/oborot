<!--gallery plugin used http://miromannino.github.io/Justified-Gallery/-->
<?php

use skewer\build\Page\Gallery\AssetJustifiedGallery;
use skewer\components\design\Design;
use yii\helpers\ArrayHelper;
use yii\helpers\Html;

/**
 * @var $_objectId
 * @var $description
 * @var $images
 * @var $openAlbum
 * @var $aAlbums
 * @var string $justifiedGalleryConfig
 */

AssetJustifiedGallery::register(\Yii::$app->view);
?>


<div class="b-galbox b-galbox--inline" <? if (Design::modeIsActive()): ?>sktag="modules.gallery" sklabel="<?=$_objectId?>" <?endif;?> >
    <? if (!empty($description)): ?>
        <div class="galbox__contentbox"><?= Html::encode($description)?></div>
    <?endif;?>

    <? if (!empty($images)): ?>

        <div class="galbox__items">
            <div class="js-gallery-tile" data-config='<?=$justifiedGalleryConfig?>'>
            <? foreach ($images as $aImage):?>
                <?
                    $sVerticalPhoto =  ArrayHelper::getValue($aImage, 'images_data.preview_ver.file');
                    if ( $sVerticalPhoto && file_exists(WEBPATH . $sVerticalPhoto) ){
                        $sSelectedFormat = 'preview_ver';
                        $sSrc = $sVerticalPhoto;
                    } else{
                        $sSrc = ArrayHelper::getValue($aImage, 'images_data.preview.file');
                        $sSelectedFormat = 'preview';
                    }
                ?>
                <? if ($openAlbum) $sGalId = 1; else $sGalId=$aImage['album_id'];?>
                <a data-fancybox="<?=$sGalId?>" data-fancybox-group="gallery" class="js-gallery-link js-gallery_resize" href="<?=ArrayHelper::getValue($aImage, 'images_data.med.file', '')?>" title="<?=Html::encode(ArrayHelper::getValue($aImage, 'title', ''))?><?=Html::encode(ArrayHelper::getValue($aImage, 'description', ''))?>">
                    <img data-images='<?= json_encode(ArrayHelper::index($aImage, "{$sSelectedFormat}.file")); ?>' class="js-gallery-pic"
                        src="<?=$sSrc?>"
                        alt="<?=Html::encode(ArrayHelper::getValue($aImage, 'alt_title', ''))?>">
                </a>
            <? endforeach;?>
            </div>
        </div>

    <? else: ?>
        <p></p>
    <?endif;?>



    <div class="g-clear"></div>

    <? if (!$openAlbum):?>
        <p class="galbox__linkback"><a href="javascript: history.go(-1);" rel="nofollow"><?= \Yii::t('page', 'back') ?></a></p>
    <? endif; ?>
</div>
