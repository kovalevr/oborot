$(function() {
    if ($('.js-reviews-carousel').length) {
        $('.js-reviews-carousel').each(function() {
            var carouselOptions = $(this).data('carouselOptions');
            $(this).owlCarousel(carouselOptions);
        });
    };
});//ready