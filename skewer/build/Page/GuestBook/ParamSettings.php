<?php

namespace skewer\build\Page\GuestBook;

/**
 * Класс, содержащий редактируемые для текущего модуля параметры, используемые в админском модуле "Настройка параметров"
 * (skewer\build\Adm\ParamSettings)
 * Class ParamSettings
 * @package skewer\build\Page\ParamSettings
 */

class ParamSettings extends \skewer\build\Adm\ParamSettings\Prototype {

    public static $aGroups = [
        'Review' => 'review.groups_Review',
    ];

    public static $iGroupSortIndex = 30;

    public static $aTypeReviews = [
        'list' => [
            'title' => 'Review.field_in_column',
            'file'  => 'main.twig',
        ],
        'carousel' => [
            'title' => 'Review.field_carousel',
            'file'  => 'carousel.twig',
        ],
    ];
    /**Отображение отзывов списком*/
    const REVIEW_LIST = 'list';
    /**Отображение отзывов каруселью*/
    const REVIEW_CAROUSEL = 'carousel';

    /** @inheritdoc */
    public function getList() {

        return [
            [
                'name'   => 'titleOnMain',
                'group'  => 'Review',
                'title'  => 'news.title_on_main',
            ],
            [
                'name'    => 'onPage',
                'group'   => 'Review',
                'title'   => 'review.show_on_page',
                'editor'  => 'int',
                'settings' => [
                    'minValue' => 0,
                    'allowDecimals' => false
                ],
            ],
            [
                'name'    => 'onPageContent',
                'group'   => 'Review',
                'title'   => 'review.on_page',
                'editor'  => 'int',
                'default' => '3',
                'settings' => [
                    'minValue' => 1,
                    'allowDecimals' => false
                ],
            ],
            [
                'name'   => 'maxLen',
                'group'  => 'Review',
                'title'  => 'review.maxLen',
                'editor' => 'int',
                'settings' => [
                    'minValue' => 0,
                    'allowDecimals' => false
                ],
            ],
            [
                'name'   => 'rating',
                'group'  => 'Review',
                'title'  => 'review.settings_rating',
                'editor' => 'check',
                'default' => 0
            ],
            [
                'name'   => 'section_id',
                'group'  => 'Review',
                'title'  => 'review.section_id_editor',
                'editor' => 'string',
            ],
            [
                'name'   => 'showListOrCarousel',
                'group'  => 'Review',
                'title'  => 'review.field_type_show',
                'editor'  => 'select',
                'options' => self::getReviewsOnMain(),
                'default' => 'list',
            ],
            [
                'name'   => 'HideGalleryReview',
                'group'  => 'Review',
                'title'  => 'review.field_hide_gallery',
                'editor'  => 'check',
                'default' => 0
            ],
        ];
    }

    /** @inheritdoc */
    public function saveData() {
    }

    public function getInstallationParam(){
        return [];
    }
    
    /** Возвращает список доступных отображений */
    private static function getReviewsOnMain() {

        static $aReviews;
        if (!$aReviews)
            $aReviews = \yii\helpers\ArrayHelper::getColumn(self::$aTypeReviews, 'title');

        return $aReviews ? : [];
    }

}