<?php

namespace skewer\build\Page\GuestBook;

use skewer\base\section\params\Type;
use skewer\build\Tool\Review\Api;
use skewer\components\config\InstallPrototype;
use skewer\components\forms;

class Install extends InstallPrototype {

    public function init() {

        return true;
    }// func

    public function install() {

        if (forms\Table::getByName(Api::NAME_FORM_REVIEW))
            return true;

        /* Создание формы */
        $oForm = forms\Table::getNewRow();

        $oForm->form_name         = Api::NAME_FORM_REVIEW;
        $oForm->form_title        = $this->lang('form_title');
        $oForm->form_handler_type = forms\Entity::TYPE_TOMETHOD;
        $oForm->form_sys          = 1;
        $oForm->form_active       = 1;
        $oForm->form_captcha      = 1;
        $oForm->form_succ_answer = \Yii::t('review', 'send_msg');
        $oForm->form_answer = 1;
        $oForm->save();

        /* Создание полей формы */

        forms\FieldTable::getNewRow([
            'form_id'               => $oForm->form_id,
            'param_name'            => 'name',
            'param_title'           => 'review.field_name',
            'param_type'            => Type::paramString,
            'param_required'        => 1,
            'param_maxlength'       => 255,
            'width_factor'          => 1,
            'param_validation_type' => 'text', // @todo Сделать константой
            'param_priority'        => 1,
            'label_position'        => 'top', // @todo Сделать константой
            'new_line'              => 1,
        ])->save();

        forms\FieldTable::getNewRow([
            'form_id'               => $oForm->form_id,
            'param_name'            => 'email',
            'param_title'           => 'review.field_email',
            'param_type'            => Type::paramString,
            'param_required'        => 1,
            'param_maxlength'       => 255,
            'width_factor'          => 1,
            'param_validation_type' => 'email', // @todo Сделать константой
            'param_priority'        => 2,
            'label_position'        => 'top', // @todo Сделать константой
            'new_line'              => 0,
        ])->save();

        forms\FieldTable::getNewRow([
            'form_id'               => $oForm->form_id,
            'param_name'            => 'city',
            'param_title'           => 'review.field_city',
            'param_type'            => Type::paramString,
            'param_required'        => 0,
            'param_maxlength'       => 255,
            'width_factor'          => 1,
            'param_validation_type' => 'text', // @todo Сделать константой
            'param_priority'        => 3,
            'label_position'        => 'top', // @todo Сделать константой
            'new_line'              => 0,
        ])->save();

        forms\FieldTable::getNewRow([
            'form_id'               => $oForm->form_id,
            'param_name'            => 'content',
            'param_title'           => 'review.field_comment',
            'param_type'            => Type::paramText,
            'param_required'        => 1,
            'param_maxlength'       => 500,
            'width_factor'          => 1,
            'param_validation_type' => 'text', // @todo Сделать константой
            'param_priority'        => 4,
            'label_position'        => 'top', // @todo Сделать константой
            'new_line'              => 1,
        ])->save();

        forms\FieldTable::getNewRow([
            'form_id'               => $oForm->form_id,
            'param_name'            => 'rating',
            'param_title'           => 'review.field_rating',
            'param_type'            => Type::paramHiddenField,
            'param_required'        => 0,
            'param_maxlength'       => 255,
            'width_factor'          => 1,
            'param_validation_type' => 'text', // @todo Сделать константой
            'param_priority'        => 5,
            'label_position'        => 'top', // @todo Сделать константой
            'new_line'              => 1,
        ])->save();
        forms\FieldTable::getNewRow([
            'form_id'               => $oForm->form_id,
            'param_name'            => 'company',
            'param_title'           => 'review.field_company',
            'param_type'            => Type::paramString,
            'param_required'        => 0,
            'param_maxlength'       => 255,
            'width_factor'          => 1,
            'param_validation_type' => 'text', // @todo Сделать константой
            'param_priority'        => 5,
            'label_position'        => 'top', // @todo Сделать константой
            'new_line'              => 1,
        ])->save();
        forms\FieldTable::getNewRow([
            'form_id'               => $oForm->form_id,
            'param_name'            => 'photo_gallery',
            'param_title'           => 'review.field_photo_gallery',
            'param_type'            => Type::paramFile,
            'param_required'        => 0,
            'param_maxlength'       => 255,
            'width_factor'          => 1,
            'param_validation_type' => 'text', // @todo Сделать константой
            'param_priority'        => 5,
            'label_position'        => 'top', // @todo Сделать константой
            'new_line'              => 0,
        ])->save();

        return true;
    }// func

    public function uninstall() {
        return true;
    }// func

}//class
