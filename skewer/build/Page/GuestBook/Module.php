<?php

namespace skewer\build\Page\GuestBook;

use skewer\base\section\Parameters;
use skewer\base\SysVar;
use skewer\build\Adm\GuestBook\ar;
use skewer\base\section\Tree;
use skewer\base\site_module;
use skewer\build\Page\CatalogViewer\State\DetailPage;
use skewer\build\Tool\LeftList\Group;
use skewer\components\catalog\GoodsSelector;
use skewer\components\forms;
use skewer\build\Tool\Review\Api;
use skewer\components\gallery\Album;
use skewer\components\gallery\Format;
use skewer\components\gallery\Photo;
use skewer\components\gallery\Profile;
use skewer\components\GalleryOnPage\Api as GalOnPageApi;
use \skewer\components\seo;
use skewer\helpers\Files;
use skewer\helpers\Image;
use yii\helpers\ArrayHelper;

/**
 * Пользовательский модуль отзывов
 * Class Module
 * @package skewer\build\Page\GuestBook
 */
class Module extends site_module\page\ModulePrototype {

    public $hideTitle;
    public $altTitle = '';
    /** @var null Имя таба */
    public $sTabName = null;
    /** @var int Номер страницы из модуля CatalogViewer - DetailPage */
    public $iPage = 0;

    public $objectId;
    public $className = '';
    public $actionForm = '';

    public $showList = false;

    public $titleOnMain= '';
    public $maxLen = 500;
    public $section_id;

    public $onPage = 10;

    public $revert = 0;

    public $hide_form = 0;

    public $iCountItems = 0;

    /** @var bool Отдать только микроразметку? */
    public $bOnlyMicrodata = false;

    /** @var int Использовать голосование в форме? Если значение <0 то параметр будет браться из глобальной языковой метки Review */
    public $rating = -1;

    /** @var int Показать звёзды голосования в списке отзывов? Если значение <0 то параметр = параметру rating */
    public $rating_list = -1;

    public function init() {

        $this->onPage = abs($this->onPage);
        $this->maxLen = abs($this->maxLen);

        $this->setParser(parserTwig);

        // Если параметр не задан строго для раздела, то взять глобальную настройку парамера rating, использующуюся в модуле "Настройки параметров"
        if ($this->rating < 0) {
            if ($oParamRating = Parameters::getByName($this->sectionId(), 'Review', 'rating', true, true))
                $this->rating = $oParamRating->value;
            else
                $this->rating = 0;
        }
        $this->rating_list = ($this->rating_list < 0) ? $this->rating : $this->rating_list;
    }

    protected function dateToText($sDate) {

        $arr = explode(' ', $sDate);
        $res = array();
        $res['date'] = $arr[0];
        $res['time'] = $arr[1];
        list($res['year'], $res['month'], $res['day']) = explode('-', $arr[0]);
        list($res['hour'], $res['min'], $res['sec']) = explode(':', $arr[1]);

        //$sNewDate = $res['day'] . ' ' . \Yii::t('app', 'month_' . $res['month']) . ' ' . $res['year'];
        $sNewDate = $res['day'] . '.' . $res['month'] . '.' . $res['year'];
        return $sNewDate;
    }

    /** Вывод формы / обработка данных формы */
    private function showForm() {

        if (!$oForm = forms\Table::getByName(Api::NAME_FORM_REVIEW))
            return;

        $oForm    = new forms\Entity($oForm, $this->getPost());
        $sLabel   = $this->oContext->getLabel();
        $formHash = $oForm->getHash((int)\Yii::$app->request->post('section', $this->sectionId()), $sLabel);

        if ( ($oForm->getState($sLabel) == forms\Entity::STATE_SEND_FORM) and $oForm->validate($formHash) ) {
            // Отправить форму
            $idAlbum = $this->addImage($oForm);
            $this->sendComment($idAlbum);

            $aData = $oForm->getData();

            if (isset($aData['parent_class'])
                && $aData['parent_class'] == 'GoodsReviews'
                && (isset($aData['parent']))
            ){
                $sGoodsAlias = GoodsSelector::getGoodsAlias($aData['parent']);

                /*Если обрабатываем отзыв с каталога*/
                $sRedirectUrl = Tree::getSectionAliasPath($this->sectionId()).$sGoodsAlias.DIRECTORY_SEPARATOR.'#tabs-reviews';
            } else {
                /*Стандартный отзыв*/
                $sRedirectUrl = Tree::getSectionAliasPath($this->sectionId());
            }

            /*Установим флаг о отправке и редиректим на себя же*/
            \Yii::$app->session->set('guestbook_send','1');
            header('Location: '.$sRedirectUrl);
            exit;

        } elseif(\Yii::$app->session->get('guestbook_send',0)=='1') {
            /*Снимаем флаг отправки и выводим сообщение*/
            \Yii::$app->session->set('guestbook_send','0');
            $ajaxForm = $this->getInt('ajaxForm');
            $sAnswer = forms\Api::buildSuccessAnswer($oForm, $ajaxForm, $this->sectionId(), ['form_section' => $this->sectionId()]);
            $sAnswer = ($sAnswer)?:\Yii::t('review', 'send_msg');

            if ( !$ajaxForm ){

                // Базовая результирующая или сторонняя с пустым адресом результирующей -> редирект на /response
                if ( $oForm->hasBaseResultPage() || ($oForm->hasExternalResultPage() && !$oForm->getFormRedirect(true)) ){
                    $this->setData('msg', $sAnswer);
                    $this->setData('back_link', 1);

                    // Сторонняя результирующая -> редирект на другую страницу
                } elseif ( $oForm->hasExternalResultPage() ){
                    \Yii::$app->session->setFlash('form_source',$oForm->getId());
                    \Yii::$app->getResponse()->redirect($oForm->getFormRedirect( true ),'301');
                }
                return psComplete;

            }

            $this->setData('msg', $sAnswer);
            $this->setData('back_link', 1);

        } else {
            // Показать форму
            //вывод системной формы
            $aParamForm = [
                'oForm' => $oForm,
                'HiddenCaptchaInput' =>forms\Entity::getHiddenCaptchaInput(),
                'formHash' => $formHash,
                'popupResultPage' => (int)$oForm->hasPopupResultPage(),
                'ajaxForm' => $oForm->hasPopupResultPage(),
                'label' => $sLabel,
                'section' => $this->sectionId(),
                'parent' => $this->objectId ? $this->objectId : $this->sectionId(),
                'parent_class' => $this->className
            ];

            $sFormTemplate = site_module\Parser::parseTwig('form.twig',$aParamForm,
                BUILDPATH . 'Page/Forms/templates');
            $this->setData('form', $sFormTemplate);
            $this->setData('oForm', $oForm);
        }
    }
    /**
     * Добавление изображения, отправленного из отзыва
     * @var object $oForm
     * @return int
     */
    private function addImage($oForm) {

        $aFields = $oForm->getFields(true);
        if (isset($aFields['photo_gallery'])) {
            $sNameImg = $aFields['photo_gallery']->param_title;

            if ( !ArrayHelper::getValue($_FILES, 'photo_gallery.tmp_name', '') )
                return 0;

            $aImageInfo = getimagesize($_FILES['photo_gallery']['tmp_name']);
            $sTypeImage = $aImageInfo[2];
            $aAllowImageTypes = array_keys(Image::getAllowImageTypes());

            if ( !in_array($sTypeImage,$aAllowImageTypes) )
                return 0;

            $aDate = $oForm->getData();
            if (isset($aDate['photo_gallery'])) {
                \skewer\build\Adm\Gallery\Api::crateTempDir();
                $allPath = ROOTPATH.'web/'.\skewer\build\Adm\Gallery\Api::$sTempPath.$sNameImg;
                file_put_contents($allPath,$aDate['photo_gallery']);
                //добавление альбома
                $idAlbum = Album::setAlbum([
                    'owner'      => 'section',  // владелец
                    'section_id' => $this->sectionId(), // родительский раздел
                    'profile_id' => Profile::getDefaultId(Profile::TYPE_REVIEWS) // Профиль форматов
                ]);
                $idProfile = Profile::getDefaultId(Profile::TYPE_REVIEWS);
                $aCrop = Format::getCropTypeProfile(Profile::TYPE_REVIEWS);
                $bAdd = Photo::addPhotoInAlbum($allPath,$idAlbum,$aCrop,$idProfile);
                Files::remove($allPath);
                return $idAlbum;
            }

        }
        return 0;
    }

    /** Отправить отзыв в БД */
    private function sendComment($idAlbum=0) {

        $aData = $this->getPost();
        $aData['photo_gallery'] = $idAlbum;

        foreach($aData as &$psValue)
            $psValue = strip_tags($psValue);

        $oRow = new ar\GuestBookRow();
        $oRow->setData($aData);
        $oRow->date_time = date( 'Y-m-d H:i:s' );

        $oRow->status =  ar\GuestBook::statusNew;

        if (!$oRow->save())
            return false;

        Api::sendMailToClient( $oRow );
        Api::sendMailToAdmin( $oRow );

        return true;
    }

    public function execute() {

        // номер текущей страницы
        $iPage = ($this->iPage) ? $this->iPage : $this->getInt('page', 1);

        \Yii::$app->router->setLastModifiedDate(ar\GuestBook::getMaxLastModifyDate());

        /**
         * Вывод списка для правой/левой колонки
         */
        $sCarousel = Parameters::getValByName(\Yii::$app->sections->languageRoot(),'Review','showListOrCarousel',true);

        $this->setData('show_rating', $this->rating_list);
        $this->setData('showGallery',$this->showGallery());
        //Отзывы на главной и список на других страницах
        if ( $this->showList&&$sCarousel) {
            $onPageContent = Parameters::getValByName(\Yii::$app->sections->languageRoot(),'Review','onPageContent',true);
            $this->onPage = ($this->sectionId() != \Yii::$app->sections->main())?abs($onPageContent):abs($this->onPage);

            if ( isset(ParamSettings::$aTypeReviews[$sCarousel])) {

                if ($this->zone != Group::CONTENT)
                    $sCarousel = ParamSettings::REVIEW_LIST;
                if ($this->onPage) {
                    $aData = $this->getReviews($this->onPage,$sCarousel);
                    $this->setData('title', \Yii::t('review', $this->titleOnMain));
                    $this->setData('items', $aData);
                    $this->setData('maxLen',$this->maxLen);
                    if ($this->section_id){
                        $this->setData('section_id', $this->section_id);
                    }
                    $sGalSettings = GalOnPageApi::getSettingsByEntity('Review',false);
                    $sJsonGalSettings = $this->setNewCountItemsInJson($sGalSettings,count($aData));
                    $this->setData('gallerySettings_review', $sJsonGalSettings);
                }
                $zone = ($this->zone==Group::CONTENT)?1:0;
                $this->setData('zone', $zone);
                $this->setTemplate(ParamSettings::$aTypeReviews[$sCarousel]['file']);
            }

        } else {

            $count = 0;
            $aData = ar\GuestBook::find()
                ->where('status', ar\GuestBook::statusApproved)
                ->where('parent', $this->objectId ? $this->objectId : $this->sectionId())
                ->where('parent_class', $this->className)
                ->limit($this->onPage, ($iPage - 1) * $this->onPage)
                ->setCounterRef($count)
                ->order('date_time', 'DESC')
                ->asArray()
                ->getAll();

            // расставить отформатированииые даты в записях
            foreach ($aData as &$aItem) { // todo перенести в Row
                $photos = (isset($aItem['photo_gallery']))?Photo::getFromAlbum($aItem['photo_gallery'],true):'';
                $oFirstImage = isset($photos[0]) ? $photos[0] : false;
                if ($oFirstImage){
                    $aItem['photo_gallery'] = $oFirstImage->images_data;
                }
                $aItem['date_time'] = $this->dateToText($aItem['date_time']);
            }

            $this->iCountItems = count($aData);

            // задать данные для парсинга
            $this->setData('items', $aData);


            $this->setData('aOrganization',[
                'address' => Parameters::getValByName(\Yii::$app->sections->root(), seo\Api::GROUP_PARAM_MICRODATA, 'OrganizationAddress'),
                'name'    => Parameters::getValByName(\Yii::$app->sections->root(), seo\Api::GROUP_PARAM_MICRODATA, 'OrganizationName'),
                'phone'   => Parameters::getValByName(\Yii::$app->sections->root(), seo\Api::GROUP_PARAM_MICRODATA, 'OrganizationPhone')
            ]);
            $bIsReview4Good = $this->objectId;
            $this->setData('bIsReview4Good',$bIsReview4Good);

            // задать шаблон
            if ($this->bOnlyMicrodata && $bIsReview4Good)
                $this->setTemplate('MicroDataReviews.twig');
            elseif ($this->sectionId() != \Yii::$app->sections->main())
                $this->setTemplate('view.twig');
            $this->setData('revert',$this->revert);

            if (( ( ($this->getLabel() == DetailPage::LABEL_GOODSREVIEWS ) && !$this->bOnlyMicrodata ) || $this->getLabel() == 'content' ) && !$this->hide_form)
                $this->showForm();

            $aURLParams = array();
            // параметры для построение пагинации для табов
            if (!is_null($this->sTabName)){
                $aURLParams = [
                    'goods-alias' => GoodsSelector::getGoodsAlias($this->objectId),
                    'tab'=>$this->sTabName
                ];
            }
            // генерируем пагинацию только когда есть данные
            if (count($aData)){
                $this->getPageLine($iPage, $count, $this->sectionId(), $aURLParams, array('onPage' => $this->onPage), 'aPages');
            }

        }

        return psComplete;
    }

    private function showGallery(){
        $bHideGallery = Parameters::getValByName(\Yii::$app->sections->languageRoot(),'Review','HideGalleryReview',true);
        return (!$bHideGallery);
    }

    /**
     * Обновление количества позиций
     * @param $aGalOnPage
     * @param $countItems
     * @return string
     */
    private function setNewCountItemsInJson($aGalOnPage,$countItems)
    {
        if (isset($aGalOnPage['items'])&&$aGalOnPage['items']>=$countItems) {
            $aGalOnPage['items'] = $countItems;
            $aGalOnPage['loop'] = false;
            if (isset($aGalOnPage['responsive']))
                foreach ($aGalOnPage['responsive'] as $iKey => $aItems)
                    if ($aItems['items']>$countItems)
                        unset($aGalOnPage['responsive'][$iKey]);
        }

        return json_encode($aGalOnPage);
    }

    /**
     * Получение отзывов
     * @param integer $onPage
     * @param string $sType {'list','carousel'}
     * @return array $aReviews
     */
    private function getReviews($onPage,$sType) {

        $aReviews = ar\GuestBook::find()
            ->where('status', ar\GuestBook::statusApproved);

        if ($this->sectionId() == \Yii::$app->sections->main())
            $aReviews->where('on_main', 1);
        /**
         * Если указан раздел - тащим из раздела, если нет - по всему сайту
         */
        $this->section_id = (int)$this->section_id;
        if ($this->section_id) {
            $aReviews->where('parent', $this->section_id)
                ->where('parent_class', '');
        } else {
            $aSections = Tree::getAllSubsection(\Yii::$app->sections->languageRoot());
            if ($aSections) {
                $aReviews->whereRaw("(parent_class != '' OR parent IN (" . implode(', ', $aSections) . '))');
            }
        }

        $aReviews = $aReviews->order('date_time', 'DESC')
            ->limit($onPage)
            ->asArray()
            ->getAll();


        foreach ($aReviews as &$aItem) {
            $photos = ($aItem['photo_gallery'])?Photo::getFromAlbum($aItem['photo_gallery']):'';
            $oFirstImage = isset($photos[0]) ? $photos[0] : false;
            if ($oFirstImage)
                $aItem['photo_gallery'] = $oFirstImage->images_data;
        }

        if (!empty($aReviews)) {
            $aReviews = $this->getLinkWithAnchor($aReviews);
        }
        return $aReviews;
    }


    /**
     *  Получение ссылки на отзывы
     *  @var array() $aData массив отзывов
     *  @return array() $aData
     */
    private function getLinkWithAnchor(&$aData) {

        $aSectionTree = [];
        $onPageContent = Parameters::getValByName(\Yii::$app->sections->languageRoot(),'Review','onPageContent',true);
        foreach ($aData as &$aValue) {

            $aSectionTree[$aValue['parent']] = (isset($aSectionTree[$aValue['parent']]))?$aSectionTree[$aValue['parent']]:Tree::getSectionAliasPath($aValue['parent']);
            $onPage[$aValue['parent']] = (isset($onPage[$aValue['parent']]))?$onPage[$aValue['parent']]:(($onPageContent)?:Parameters::getValByName($aValue['parent'],'content','onPage',true));

            $iPage = $this->getByIdNumberPage($aValue['date_time'],$aValue['parent'],$onPage[$aValue['parent']]);

            $aValue['link'] = ($iPage!=1)?$aSectionTree[$aValue['parent']]."?page=".$iPage."#".$aValue['id']:$aSectionTree[$aValue['parent']]."#".$aValue['id'];
        }
        return $aData;
    }

    /**
     *  Получение номера страницы
     * @var $sData - дата создания новости
     * @var $idPage - id родительского раздела отзыва
     * @var $onPage - количество отзывов на одной странице без пагинации
     * @return $iPage - страница пагинации
     */
    private function getByIdNumberPage($sData,$idPage,$onPage) {

        $sWhere = "`date_time`>'".$sData."'";
        $sCount = ar\GuestBook::find()
            ->where('status', ar\GuestBook::statusApproved)
            ->where('parent', $idPage)
            ->whereRaw($sWhere)
            ->getCount();

        $iPage = ceil(($sCount+1)/$onPage);

        return $iPage;
    }

}