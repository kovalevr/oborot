<?php

namespace skewer\build\Page\ReserveOnline;

use Yii;
use yii\helpers\FileHelper;
use yii\web\AssetBundle;
use yii\web\View;

class Asset extends AssetBundle {

    public $sourcePath = '@skewer/build/Page/ReserveOnline/web/';

    public $css = [
        'css/reserve_online.css',
    ];

    public $js = [
        'js/reserve_online.js'
    ];

    public $jsOptions = [
        'position' => View::POS_END
    ];

    public $depends = [
        'skewer\libs\jquery\Asset',
        'skewer\libs\datepicker\Asset'
    ];
}
