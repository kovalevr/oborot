<?php

$aLanguage = [];

$aLanguage['ru']['room_reservation']  = 'Бронирование номеров';
$aLanguage['ru']['make_reserve_now']  = 'Получите гарантированное заселение прямо сейчас!';
$aLanguage['ru']['arrival_date']      = 'Дата заезда';
$aLanguage['ru']['departure_date']    = 'Дата выезда';
$aLanguage['ru']['show_availability'] = 'Показать наличие';

// ********************************************************************************************************************
// ***************************************************** ENGLISH ******************************************************
// ********************************************************************************************************************

$aLanguage['en']['room_reservation']  = 'Room reservation';
$aLanguage['en']['make_reserve_now']  = 'Book a room now!';
$aLanguage['en']['arrival_date']      = 'Arrival date';
$aLanguage['en']['departure_date']    = 'Departure date';
$aLanguage['en']['show_availability'] = 'Show availability';

// ********************************************************************************************************************
// ***************************************************** GERMAN *******************************************************
// ********************************************************************************************************************

$aLanguage['de']['room_reservation']  = 'Zimmerreservierung';
$aLanguage['de']['make_reserve_now']  = 'Sichern Sie sich schon jetzt Ihr Wunschhotel!';
$aLanguage['de']['arrival_date']      = 'Anreise';
$aLanguage['de']['departure_date']    = 'Abreise';
$aLanguage['de']['show_availability'] = 'verfügbare Hotelzimmer anzeigen';

return $aLanguage;