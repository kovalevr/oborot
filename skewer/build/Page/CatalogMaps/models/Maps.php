<?php

namespace skewer\build\Page\CatalogMaps\models;

use skewer\components\ActiveRecord\ActiveRecord;
use Yii;
use yii\helpers\StringHelper;

/**
 * This is the model class for table "maps".
 *
 * @property integer $id
 * @property string $center
 * @property string $zoom
 */
class Maps extends ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'maps';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['center', 'zoom'], 'required'],
            [['center', 'zoom'], 'string', 'max' => 255]
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('maps', 'ID'),
            'center' => Yii::t('maps', 'Center'),
            'zoom' => Yii::t('maps', 'Zoom'),
        ];
    }


    public static function getSettingsMapById( $iMapId ){

        if ( !($oMap = Maps::findOne( $iMapId )) )
            return [];

        $aCenter = StringHelper::explode($oMap->center, ',', true, true);

        return array(
            'center' => array(
                'lat' => $aCenter[0],
                'lng' => $aCenter[1]
            ),
            'zoom' =>  $oMap->zoom
        );

        return $aLocalSettingsMaps;

    }


    public static function getNewOrExist( $iRecordId ){

        if ( $oRecord = self::findOne($iRecordId) )
            return $oRecord;
        else
            return new self();

    }


}
