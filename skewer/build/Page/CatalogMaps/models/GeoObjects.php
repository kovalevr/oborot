<?php

namespace skewer\build\Page\CatalogMaps\models;

use skewer\components\ActiveRecord\ActiveRecord;
use Yii;

/**
 * This is the model class for table "geoObjects".
 *
 * @property integer $id
 * @property integer $map_id
 * @property string $latitude
 * @property string $longitude
 * @property string $address
 */
class GeoObjects extends ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'geoObjects';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['latitude', 'longitude', 'map_id', 'address'], 'required'],
            [['latitude'],  'double', 'min'=> -90, 'max' => 90],
            [['longitude'], 'double', 'min'=> -180, 'max' => 180]
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('maps', 'ID'),
            'map_id' => Yii::t('maps', 'map_id'),
            'latitude' => Yii::t('maps', 'latitude'),
            'longitude' => Yii::t('maps', 'longitude'),
            'address'   => Yii::t('maps', 'address'),
        ];
    }

    /**
     * Вернёт карту геообъекта
     * @param $iGeoObjectId int
     * @return array
     */
    public static function getMapByGeoObjectId( $iGeoObjectId ){

        return self::find()
            ->select("maps.*")
            ->where(['geoObjects.id' => $iGeoObjectId])
            ->innerJoin( Maps::tableName(), 'geoObjects.map_id = maps.id' )
            ->asArray()
            ->one();

    }

}
