<?php
/**
 * @var $this \yii\base\View
 *  @var array $aGood
 *  @var bool $reedMore
 */
use skewer\base\ft\Editor;
use yii\helpers\ArrayHelper;

?>
<? foreach ($aGood['fields'] as $aField): ?>

    <?php
        if ( $aField['type'] == Editor::MAP_SINGLE_MARKER ){
            $sHtml = ArrayHelper::getValue($aField, 'address');
        } else {
            $sHtml = ArrayHelper::getValue($aField, 'html');
        }
    ?>
    <? if ( ArrayHelper::getValue($aField, 'attrs.show_in_map') && $sHtml  ): ?>
        <div><? if ( ArrayHelper::getValue($aField, 'attrs.show_title_in_map') ) :?><strong><?=ArrayHelper::getValue($aField, 'title')?>:</strong> <?endif;?><?=$sHtml?></div>
    <? endif; ?>
<? endforeach; ?>

<? if ($reedMore): ?>
    <a href="<?=$aGood['url']?>"><?=Yii::t('page','readmore');?></a>
<? endif; ?>
