<?php

use \skewer\base\section\Parameters;
use \skewer\components\design\DesignManager;
use \skewer\base\SysVar;
use \yii\helpers\ArrayHelper;
use \skewer\components\gallery\Photo;
use \skewer\components\seo;

/* @var $news \skewer\build\Adm\News\models\News */
?>
<?

    $sAddress = Parameters::getValByName(Yii::$app->sections->root(), seo\Api::GROUP_PARAM_MICRODATA, 'OrganizationAddress');
    $sPhone   = Parameters::getValByName(Yii::$app->sections->root(), seo\Api::GROUP_PARAM_MICRODATA, 'OrganizationPhone');
    $sName    = Parameters::getValByName(Yii::$app->sections->root(), seo\Api::GROUP_PARAM_MICRODATA, 'OrganizationName');


?>
<div itemscope itemtype="<?=WEBPROTOCOL?>schema.org/NewsArticle" style="display: none">
    <? $aLogo = Photo::getLogoInfo(); ?>
    <div itemprop="headline"><?= $news->title ?></div>
    <div itemprop="author"><?= Yii::$app->request->absoluteUrl ?></div>
    <div itemprop="datePublished"><?= $news->publication_date ?></div>
    <div itemprop="description"><?= $news->announce ?></div>

    <div itemprop="publisher" itemscope itemtype="<?=WEBPROTOCOL?>schema.org/Organization">
        <div itemprop="name"><?= ($sName)? $sName : '' ?></div>
        <div itemprop="address"><?= ($sAddress)? $sAddress : '' ?></div>
        <div itemprop="telephone"><?= ($sPhone)? $sPhone : '' ?></div>

        <div itemprop="logo" itemscope itemtype="<?=WEBPROTOCOL?>schema.org/ImageObject">
            <img alt="" itemprop="url" src="<?=$aLogo['src']?>" />
            <img alt="" itemprop="contentUrl" src="<?=$aLogo['src']?>" />
        </div>
    </div>

    <?php

        $aImage = $aLogo;

        if (SysVar::get('News.galleryStatus') & \skewer\build\Adm\News\ShowType::PREVIEW){
            $photos = \skewer\components\gallery\Photo::getFromAlbum($news->gallery);
            $oFirstImage = isset($photos[0]) ? $photos[0] : false;
            if ($oFirstImage){
                $aImage = [
                    'src'    => ArrayHelper::getValue($oFirstImage->images_data, 'big.file'),
                    'width'  => ArrayHelper::getValue($oFirstImage->images_data, 'big.width'),
                    'height' => ArrayHelper::getValue($oFirstImage->images_data, 'big.height')
                ];
            }
        }

    ?>

    <div itemprop="image" itemscope itemtype="<?=WEBPROTOCOL?>schema.org/ImageObject">
        <meta itemprop="width" content="<?=$aImage['width']?>" />
        <meta itemprop="height" content="<?=$aImage['height']?>" />
        <a itemprop="url" href="<?=$aImage['src']?>"></a>
        <a itemprop="contentUrl" href="<?=$aImage['src']?>"></a>
    </div>

    <meta itemprop="dateModified" content="<?= ((new DateTime($news->last_modified_date))->getTimestamp() > 0)? $news->last_modified_date : $news->publication_date ?>" />
    <?
        $hrefParam = ($news->news_alias)?"news_alias={$news->news_alias}":"news_id={$news->id}";
        $href = ($news->hyperlink)?$news->hyperlink:("[{$news->parent_section}][News?".$hrefParam."]");
    ?>
    <a itemprop="mainEntityOfPage" href="<?= $href ?>"></a>


</div>