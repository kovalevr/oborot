<?php

use skewer\components\design\Design;
use skewer\base\SysVar;
use skewer\components\gallery\Photo;
use skewer\components\gallery\models\Photos;

/**
 * @var $this yii\web\View
 * @var $item \skewer\build\Adm\News\models\News
 * @var $dataProvider yii\data\ActiveDataProvider
 * @var string $titleOnMain
 * @var bool $showDetailLink
 * @var string $onMainShowType
 * @var string $zone
 * @var string $sFormatImage
 * @var bool $hideDate
 */

if ( !$dataProvider->getCount() )
    return;

if ( $zone == 'content' ){

    if ( $onMainShowType == 'column' )
        $sAddClass = ' b-news--main';
    else
        $sAddClass = ' b-news--list';

} else{
    $sAddClass = '';
}

?>

<div class="b-news <?= $sAddClass ?>"<?= Design::write(' sktag="modules.news"') ?>>
    <? if (($titleOnMain || Design::modeIsActive())):?>
        <div class="news__alltitle" <?= Design::write(' sktag="editor.h2" skeditor="news/titleOnMain"') ?>><?= $titleOnMain ?></div>
    <? endif ?>
    <? foreach($dataProvider->getModels() as $item): ?>
    <?
        $hrefParam = ($item->news_alias)?"news_alias={$item->news_alias}":"news_id={$item->id}";
        $href = ($item->hyperlink)?$item->hyperlink:("[{$item->parent_section}][News?".$hrefParam."]");
    ?>
    <div class="news__item">
        <? if (SysVar::get('News.galleryStatus') & \skewer\build\Adm\News\ShowType::PREVIEW):?>
            <div class="news__imgbox">
                <? if ($item->full_text || $item->hyperlink):?><a href="<?= $href ?>"><?endif;?>
                    <? /** @var Photos $oFirstImage */
                    $oFirstImage = ($aPhotos = Photo::getFromAlbum($item->gallery, false, 1)) ? $aPhotos[0] : false;
                    $sFirstImgPath = ($oFirstImage && isset($oFirstImage->images_data[$sFormatImage]['file'])) ? $oFirstImage->images_data[$sFormatImage]['file'] : false;

                    if ($sFirstImgPath):?>
                        <img src="<?=$sFirstImgPath?>" title="<?= htmlspecialchars($oFirstImage->title,ENT_QUOTES) ?>" alt="<?= htmlspecialchars($oFirstImage->alt_title,ENT_QUOTES) ?>">
                    <? else: ?>
                        <? $oBandle = \skewer\build\Page\News\Asset::register($this); ?>
                        <? if (isset($defImg)): ?>
                            <img src="<?= $defImg ?>">
                        <? else: ?>
                            <img src="<?=$oBandle->baseUrl.'/images/news.noimg_on_main.gif'?>">
                        <? endif;?>
                    <? endif;?>
                    <? if ($item->full_text || $item->hyperlink):?></a><?endif;?>
            </div>
        <? endif; ?>

        <div class="news__wrap">
           <div class="news__title">
               <? if ($item->full_text || $item->hyperlink):?>
                   <a class="news__link"<?= Design::write(' sktag="modules.news.normal"') ?> href="<?= $href ?>"><?= $item->title ?></a>
               <? else: ?>
                   <span class="news-title"<?= Design::write(' sktag="modules.news.normal"') ?>><?= $item->title ?></span>
               <? endif ?>
           </div>
           <? if (!$hideDate): ?>
            <p class="news__date"<?= Design::write(' sktag="modules.news.date"') ?>><?= Yii::$app->getFormatter()->asDate($item->publication_date,'php:d.m.Y') ?></p>
        <? endif; ?>
           <div class="b-editor"<?= Design::write(' sktag="editor"') ?>>
               <?= $item->announce ?>
           </div>
           <div class="g-clear"></div>
           <? if ($showDetailLink && ($item->full_text || $item->hyperlink)): ?>
               <p class="news__linkback">
                   <a href="<?= $href ?>"><?= \Yii::t('page', 'readmore') ?></a>
               </p>
           <? endif ?>
        </div>
    </div>
    <? endforeach ?>
</div>
<? if ((isset($section_all))): ?>
    <a class="b-news-blog" href="[<?=$section_all ?>]"><?= \Yii::t('News', 'all_section_link') ?></a>
<? endif ?>
