<?php
/* @var $this yii\web\View */
/* @var $news \skewer\build\Adm\News\models\News */
/* @var $hideDate bool */
use skewer\base\SysVar;
use skewer\components\design\Design;
use skewer\components\gallery\Album;
use yii\helpers\ArrayHelper;
use skewer\libs\fotorama;
use yii\helpers\Html;

if (SysVar::get('News.galleryStatus') & \skewer\build\Adm\News\ShowType::DETAIL) {
    fotorama\Asset::register(\Yii::$app->view);
    $photos = \skewer\components\gallery\Photo::getFromAlbum($news->gallery, true);
} else
    $photos = [];



?>
<div class="b-news b-news--detal"<?= Design::write(' sktag="modules.news"') ?>>
    <? if (isset($news)): ?>
        <?= $this->render('MicroData', ['news' => $news]) ?>
        <? if (!$hideDate): ?>
            <p class="news__date"<?= Design::write(' sktag="modules.news.date"') ?>><?= Yii::$app->getFormatter()->asDate($news->publication_date,'php:d.m.Y') ?></p>
        <? endif; ?>
        <div class="b-editor"<?= Design::write(' sktag="editor"') ?>>
            <?= $news->full_text ?>
        </div>

        <? if ($photos) : ?>
            <?php if (isset($photos[0]['images_data']['mini']) && isset($photos[0]['images_data']['mini']['width'])){?>

                <? list($iMaxWidth, $iMaxHeight) = Album::getDimensions4Fotorama($news->gallery, 'big', $photos, true); ?>

                <div class="fotorama" data-nav="thumbs" data-width="100%" <? if ($iMaxWidth): ?>data-max-width="<?=$iMaxWidth?>px" <? endif; ?> <? if ($iMaxHeight): ?>data-height="<?=$iMaxHeight?>px" <? endif; ?>data-thumbheight="<?=$photos[0]['images_data']['mini']['height']?>" data-thumbwidth="<?=$photos[0]['images_data']['mini']['width']?>" data-keyboard="true" data-allowfullscreen="true">
                    <? foreach ($photos as $photo):
                        $sBigPath = ArrayHelper::getValue($photo->images_data, 'big.file', '');
                        $sMiniPath = ArrayHelper::getValue($photo->images_data, 'mini.file', '');
                    ?>
                        <?php if ($sBigPath && $sMiniPath) {?>
                            <a href="<?= $sBigPath ?>" data-thumb="<?= $sMiniPath ?>" alt="<?= Html::encode(ArrayHelper::getValue($photo, 'alt_title', ''))?>" title="<?=Html::encode(ArrayHelper::getValue($photo, 'title', ''))?><?=Html::encode(ArrayHelper::getValue($photo, 'description', ''))?>" >
                                <img src="<?= $sMiniPath ?>" />
                            </a>
                        <?php } ?>
                    <? endforeach; ?>
                </div>
            <?php } ?>
        <? endif; ?>

        <p class="news__linkback"><a rel="nofollow" href="#" onclick="history.go(-1);return false;">
                <?= \Yii::t('page', 'back') ?>
            </a>
        </p>
        <? if ($news->source_link): ?><p><a rel="nofollow" target="_blank" href="<?=$news->source_link?>" ><?= Yii::t('news','field_source_link')?></a></p> <?endif; ?>
    <? endif ?>
</div>
