$(document).ready(function() {

    $('a.single_3').fancybox({
        openEffect: 'fade',
        nextEffect: 'elastic',
        prevEffect: 'elastic',
        helpers : {
            title : {
                type : 'inside'
            }
        },
        arrows    : true,
        nextClick : true,
        mouseWheel: true,
        closeBtn: true,
        beforeShow: function () {
            var imgAlt = $(this.element).find("img").attr("alt");
            var dataAlt = $(this.element).data("alt");
            if (imgAlt) {
                $(".fancybox-image").attr("alt", imgAlt);
            } else if (dataAlt) {
                $(".fancybox-image").attr("alt", dataAlt);
            }
        }
    });



});

$( window ).load(function() {
//carousel owl
    if ($('.js-gallery-owlcarousel').length) {


        $('.js-gallery-owlcarousel').each(function(index, el) {
            var carouselOptions = $(this).data('carouselOptions');

            if (typeof(carouselOptions.responsive)!='undefined')
                carouselOptions.responsive = $.parseJSON(carouselOptions.responsive);

            $(this).on('initialized.owl.carousel resized.owl.carousel resized.owl.carousel refreshed.owl.carousel dragged.owl.carousel translated.owl.carousel changed.owl.carousel', function( event ){
            // $(this).on('refreshed.owl.carousel changed.owl.carousel', function( event ){

                var eventItemIndex = event.item.index;
                var eventItemCount = event.item.count;
                var eventPageSize = event.page.size;

                if (carouselOptions.loop == true && carouselOptions.shadow ) {
                    $(this).addClass('owl-carousel-shadow-right owl-carousel-shadow-left')
                } else {
                    //show right shadow
                    if ( eventItemIndex !== eventItemCount - eventPageSize && carouselOptions.shadow ) {
                        $(this).addClass('owl-carousel-shadow-right');
                    } else {
                        $(this).removeClass('owl-carousel-shadow-right');
                    };
                    //show left shadow
                    if ( eventItemIndex > 0  && carouselOptions.shadow) {
                        $(this).addClass('owl-carousel-shadow-left');
                    } else {
                        $(this).removeClass('owl-carousel-shadow-left');
                    };
                };

            }).owlCarousel(carouselOptions);

            $(this).trigger('refresh.owl.carousel');

        });
    };
});
