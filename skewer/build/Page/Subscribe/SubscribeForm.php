<?php

namespace skewer\build\Page\Subscribe;


use skewer\base\log\Logger;
use skewer\base\orm;
use skewer\build\Page\Subscribe\ar\SubscribeUser;
use skewer\build\Page\Subscribe\ar\SubscribeUserRow;

class SubscribeForm  extends orm\FormRecord{

    public $person = '';
    public $email = '';
    public $city = '';
    public $cmd = 'subscribe';
    public $confirm = '';

    public function rules() {
        return array(
            array( array('email'), 'required', 'msg'=>\Yii::t('subscribe', 'required_fields' ) ),
            array( array('email'), 'check', 'method'=>'checkLogin', 'msg'=>\Yii::t('subscribe', 'duplicate_email' ) ),
            array( array('email'), 'check', 'method'=>'checkEmail', 'msg'=>\Yii::t('subscribe', 'no_valid' ) )

        );
    }

    public function getLabels() {
        return array(
            //'person' => \Yii::t('subscribe', 'person' ),
            'email' => \Yii::t('subscribe', 'email' ),
            //'city' => \Yii::t('subscribe', 'city' )
        );
    }

    public function getEditors() {
        return array(
            'cmd' => 'hidden'
        );
    }


    public function checkEmail(){
        if ( !filter_var( $this->email, FILTER_VALIDATE_EMAIL ) )
            return false;

        return true;
    }

    /**
     * проверка на совпадение email
     * @return bool
     */
    public function checkLogin() {
        $oItem =  SubscribeUser::find()->where( 'email', $this->email )->get();

        if ( $oItem ) {
            $this->setFieldError( 'email', \Yii::t('subscribe', 'duplicate_email') );
            return false;
        }
        return true;
    }

    public function save(){

        /**
         * @var SubscribeUserRow $oRow
         */
        $oRow = SubscribeUser::getNewRow();

        $oRow->city = $this->city;
        $oRow->person = $this->person;
        $oRow->email = $this->email;
        $oRow->confirm = $this->confirm;

        return $oRow->save();
    }

    /**
     * Отдает текст ошибки для поля
     * @param $sFieldName
     * @return bool
     */
    public function getErrorByFieldName($sFieldName)
    {
        $aErrors = $this->getErrorList();
        if (!isset($aErrors[$sFieldName])) return false;

        return $aErrors[$sFieldName];
    }

    /**
     * Отдает количество ошибок на форме
     * @return int
     */
    public function getCountErrors(){
        $aErrors = $this->getErrorList();
        return count($aErrors);
    }

    /**
     * Проверка значения полей на валидность
     * @return bool
     */
    public function isValid() {

        if($this->cptch_country) {
            Logger::dump('Попытка отправки спама через форму. В поле cptch_country введено: '.$this->cptch_country);
            return false;
        }

        $aRules = $this->rules();

        $aFields = $this->getFields();

        $bFlagHasErors = false;
        foreach ( $aFields as $sFieldName => $oField ) {

            if ( $sErrMsg = $oField->validate( $this, $aRules ) ) {
                $this->setFieldError( $sFieldName, $sErrMsg );
                $bFlagHasErors = true;
            }

        }
        return !$bFlagHasErors;
    }

} 