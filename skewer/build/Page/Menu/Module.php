<?php

namespace skewer\build\Page\Menu;

use skewer\base\section\Parameters;
use skewer\base\section\Tree;
use skewer\base\section;
use skewer\base\site_module;
use skewer\base\SysVar;
use yii\helpers\ArrayHelper;


class Module extends site_module\page\ModulePrototype {

    public $templateFile = 'leftMenu.twig';
    public $parentSection = 3;
    public $customSections = '';
    public $openAll = '';

    /*
     * Набор модулей, контент коготрых будет проброшен внутрь модуля
     */
    /** @var string Набор пробрасываемых модулей */
    public $subModules = '';

    private static $icons = [];

	
	public function execute() {

	    // собираем набор модулей, которые должны отдать контент текущему
	    foreach ( explode(',', $this->subModules) as $sSubModuleName ) {
            $sSubModuleName = trim($sSubModuleName);
            if ( $sSubModuleName ) {

                // запрашиваем процесс из этой метки
                $oProcess = \Yii::$app->processList->getProcess('out.'.$sSubModuleName, psAll);

                if ( !$oProcess )
                    continue;

                // ждем, если есть незаконченные
                if ( $oProcess->getStatus() !== psComplete )
                    return psWait;

                // рендерим его
                $oProcess->render();

                // добавляем данные на вывод
                $this->setData($sSubModuleName, $oProcess->getOuterText());
            }
        }

        $to = $this->sectionId();
        $iMainSection = \Yii::$app->sections->main();
        $from = $this->parentSection;

        $mode = 'normal';
        if($this->openAll==1) $mode = 'openAll';
        if($this->openAll==2) $mode = 'openSecond';
        if($this->customSections) $mode = 'custom';

        $items = array();

        switch($mode){
            case 'normal':
                $items = Tree::getUserSectionTree( $from, $to, 1 );
                break;
            case 'openAll':
                $items = Tree::getUserSectionTree( $from, $to );
                break;
            case 'openSecond':
                $items = Tree::getUserSectionTree( $from, $to, 2 );
                break;

            case 'custom':

                $list = explode( ',', $this->customSections );
                $sections = Tree::getCachedSection();
                $items = [];
                foreach ( $list as $id )
                    if ( isSet( $sections[$id] ) ) {
                        $section = $sections[$id];
                        $section['show'] = in_array($section['visible'], section\Visible::$aShowInMenu);
                        $section['href'] = $section['link'] ?: '['.$id.']';
                        $section['selected'] = ( $id == $to );
                        $items[] = $section;
                    }

                break;

        } // switch

        $this->setData('items', $items);

        if ( SysVar::get('Menu.ShowIcons'))
            $this->setData('icon', self::getIcons());

        if ( $iMainSection != $to ) $this->setData('hideMenu', 1);
        $this->setTemplate($this->templateFile);

        return psComplete;
    }


    /**
     * Иконки для разделов
     * @return array
     */
    private static function getIcons(){

        if (!self::$icons){
            self::$icons = Parameters::getList()
                ->asArray()
                ->fields(['value', 'parent'])
                ->group(Parameters::settings)
                ->name('category_icon')
                ->get();

            self::$icons = ArrayHelper::map(self::$icons, 'parent', 'value');

            self::$icons = array_filter(self::$icons, function( $s ){
               return (bool)$s;
            });
        }

        return self::$icons;

    }

}
