<?php

namespace skewer\build\Page\Articles;

use skewer\base\section\Page;
use skewer\base\section\Parameters;
use skewer\base\site_module\page\ModulePrototype;
use skewer\build\Design\Zones;
use skewer\base\SysVar;
use skewer\build\Page\Articles\Model\Articles;
use skewer\components\gallery\Photo;
use skewer\components\seo;
use skewer\base\site;
use yii\web\NotFoundHttpException;


/**
 * Модуль системы статей
 * Class Module
 * @package skewer\build\Page\Articles
 */
class Module extends ModulePrototype {

    public $parentSections;
    public $onPage = 10;
    public $listTemplate = 'list.twig';
    public $detailTemplate = 'detail_page.twig';
    public $titleOnMain = 'Статьи';
    public $showFuture;
    public $showOnMain;  // отменяет фильтр по разделам
    public $allArticles;
    public $sortArticles;
    public $showList;
    // public $usePageLine = 1;

    private $iCurrentSection;

    public $aParameterList = array(
        'order'    => 'DESC',
        'future'   => '',
        'byDate'   => '',
        'on_page'  => '',
        'on_main'  => '',
        'section'  => '',
        'active'  => '',
        'all_articles' => '',
        'show_list'=> '',
    );

    public $section_all = 0;

    public function init() {

        $this->onPage = abs($this->onPage);

        $this->setParser(parserTwig);
        $this->iCurrentSection = $this->sectionId();
        $this->aParameterList['on_page'] = $this->onPage;

        if ( $this->allArticles ) $this->aParameterList['all_articles'] = 1;
        if ( $this->showOnMain ) {
            $this->aParameterList['all_articles'] = 1;
            $this->aParameterList['on_main'] = 1;
        }

        if ( $this->iCurrentSection == \Yii::$app->sections->main() ) $this->aParameterList['on_main'] = 1;
        if ( $this->parentSections ) $this->aParameterList['section'] = $this->parentSections;
        else $this->aParameterList['section'] = $this->iCurrentSection;

        if ( $this->showFuture ) $this->aParameterList['future'] = 1;
        if ( $this->sortArticles ) $this->aParameterList['order'] = $this->sortArticles;

        if ( $this->showList ) $this->aParameterList['show_list'] = 1;

        return true;

    }// func

    protected function onCreate() {
        if ( $this->showList )
            $this->setUseRouting( false );
    }

    public function execute() {

        if (!$this->onPage) return psComplete;

        $iPage       = $this->getInt('page', 1);
        $iArticlesId     = $this->getInt('articles_id', 0);
        $sArticlesAlias  = $this->getStr('articles_alias', '');
        $sDateFilter = $this->getStr('date');

        if ( !empty($sDateFilter) ) {
            $sDateFilter = date('Y-m-d', strtotime($sDateFilter));
            $this->aParameterList['byDate'] = $sDateFilter;
        }

        /*
         * Если в запросе передается ID или её алиас, значит необходимо вывести конкретную статью;
         * если нет - выводим список, разбитый по страницам.
         */
        if ( ($iArticlesId || $sArticlesAlias) && !$this->showList ) {

            $this->setStatePage( Zones\Api::DETAIL_LAYOUT );

            /** @var Model\ArticlesRow $oArticlesRow */
            if ( $sArticlesAlias )
                $oArticlesRow = Model\Articles::getPublicByAliasAndSec( $sArticlesAlias,$this->sectionId() );
            else
                $oArticlesRow = Model\Articles::getPublicById( $iArticlesId );

            if ( !$oArticlesRow )
                throw new NotFoundHttpException();

            \Yii::$app->router->setLastModifiedDate($oArticlesRow->last_modified_date);

            $aRow = $oArticlesRow->getData();
            $aRow['publication_date'] = date( "d.m.Y", strtotime( $oArticlesRow->publication_date ) );

            $this->setData( 'hideDate', $this->hasHideDatePublication() );
            $this->setData( 'aArticlesItem', $aRow );
            
            $this->setData('logo',Photo::getLogoInfo());
            $this->setData('currentPage', \Yii::$app->request->getAbsoluteUrl());

            $this->setData('address', Parameters::getValByName(\Yii::$app->sections->root(), seo\Api::GROUP_PARAM_MICRODATA, 'OrganizationAddress'));
            $this->setData('phone',   Parameters::getValByName(\Yii::$app->sections->root(), seo\Api::GROUP_PARAM_MICRODATA, 'OrganizationPhone'));
            $this->setData('name',    Parameters::getValByName(\Yii::$app->sections->root(), seo\Api::GROUP_PARAM_MICRODATA, 'OrganizationName'));

            $this->setSEO( new \skewer\build\Adm\Articles\Seo(0, $this->sectionId(), $aRow) );

            // меняем заголовок
            site\Page::setTitle( $aRow['title'] );

            // добавляем элемент в pathline
            site\Page::setAddPathItem( $aRow['title'] );

            $this->setTemplate( $this->detailTemplate );

        } else {

            $iAllSection = 0;
            if ($this->showOnMain){
                $iAllSection = $this->section_all;
            }

            if ($iAllSection){
                $this->aParameterList['all_articles'] = 0;
                $this->aParameterList['section'] = $iAllSection;
            }


            // Получаем список новостей
            $iCount = 0;
            $aArticlesList = Model\Articles::getPublicList( $this->showList ? 1 : $iPage, $this->aParameterList, $iCount );

            \Yii::$app->router->setLastModifiedDate( Articles::getMaxLastModifyDate() );

            if ( $iCount ) {

                $this->setData( 'aArticlesList', $aArticlesList );

                if ($iAllSection){
                    $this->setData('section_all', $iAllSection);
                }

                $aURLParams = array();

                if( !empty($sDateFilter) )
                    $aURLParams['date'] = $sDateFilter;

                $this->getPageLine(
                    $iPage,
                    $iCount,
                    $this->iCurrentSection,
                    $aURLParams,
                    array( 'onPage' => $this->aParameterList['on_page'] )
                );

            }

            $this->setData( 'hideDate', $this->hasHideDatePublication() );
            $this->setData('titleOnMain',$this->titleOnMain);

            $this->setTemplate($this->listTemplate);

        }

        site\Page::reloadSEO();

        return psComplete;
    }// func

    public function setSEO(seo\SeoPrototype $oSeo) {

        $this->setEnvParam(seo\Api::SEO_COMPONENT, $oSeo );
        // OPENGRAPH статьи пока не поддерживают
        $this->setEnvParam(seo\Api::OPENGRAPH, '');
        site\Page::reloadSEO();
    }

    /**
     * Скрывать дату публикации ?
     * @return bool
     */
    public function hasHideDatePublication(){
        return (bool) SysVar::get('Articles.hasHideDatePublication', false);
    }

}// class
