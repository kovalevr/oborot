<?php

namespace skewer\build\Page\Articles;

use Yii;
use yii\helpers\FileHelper;
use yii\web\AssetBundle;
use yii\web\View;

class Asset extends AssetBundle {

    public $sourcePath = '@skewer/build/Page/Articles/web/';

    public $css = [
        'css/articles.css'
    ];

}
