<?php

namespace skewer\build\Page\Profile;


use skewer\base\orm;
use skewer\components\auth\models\Users;
use skewer\components\auth\CurrentUser;

class EditProfileForm extends orm\FormRecord {

    public $fio = '';
    public $postcode = '';
    public $address = '';
    public $contact_phone = '';

    public $cmd = 'info';

    public function __construct( $id = null ) {

        if ( !is_null( $id ) ) {

            $oUser = Users::findOne( CurrentUser::getId() );

            $this->fio = $oUser->name;
            $this->postcode = (isset($oUser->postcode))?$oUser->postcode:'';
            $this->address = (isset($oUser->address))?$oUser->address:'';
            $this->contact_phone = (isset($oUser->phone))?$oUser->phone:'';
        }

    }

    public function rules() {
        return array(
            //array( array('fio','address','contact_phone'), 'required', 'msg'=>\Yii::t('auth', 'err_empty_field' ) ),
        );
    }

    public function getLabels() {
        return array(
            'login' => \Yii::t('auth', 'login_mail' ),
            'fio' => \Yii::t('auth', 'fio' ),
            'postcode' => \Yii::t('auth', 'postcode' ),
            'address' => \Yii::t('auth', 'address' ),
            'contact_phone' => \Yii::t('auth', 'contact_phone' ),
        );
    }

    public function getEditors() {
        return array(
            'cmd' => 'hidden',
        );
    }

    public function save() {

        if ( $this->isValid() ) {
            
            $oUser = Users::findOne( CurrentUser::getId() );
            $oUser->name = $this->fio;
            $oUser->postcode = $this->postcode;
            $oUser->address = $this->address;
            $oUser->phone = $this->contact_phone;

            return $oUser->save();
        }

        return false;
    }

} 