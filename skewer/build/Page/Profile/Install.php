<?php

namespace skewer\build\Page\Profile;


use skewer\base\section\Tree;
use skewer\base\section\Visible;
use skewer\build\Page\Main\Seo;
use skewer\components\config\InstallPrototype;
use skewer\components\seo\Api;

class Install extends InstallPrototype {

    public function init() {
        return true;
    }// func

    public function install() {

        $this->createProfileSections();

        return true;
    }// func

    public function uninstall() {
        return true;
    }

    protected function createProfileSections()
    {
        $iNewPageSection = \Yii::$app->sections->tplNew();
        foreach (\Yii::$app->sections->getValues('tools') as $sLang => $iSection) {

            $oSection = Tree::addSection($iSection, \Yii::t('data/auth', 'profile_sections_title', [], $sLang), $iNewPageSection, 'profile', Visible::HIDDEN_FROM_MENU);
            $this->setParameter( $oSection->id, 'object', 'content', 'Profile' );
            $this->setParameter( $oSection->id, 'hide_right_column', '.', '1' );

            /*Закроем от индексации*/
            Api::set( Seo::getGroup(), $oSection->id, $oSection->id, [ 'none_index' => 1 ] );


            \Yii::$app->sections->setSection('profile', \Yii::t('site', 'profile', [], $sLang), $oSection->id, $sLang);

        }

    }// func

    public function getCommandsAfterInstall(){
        return [
            '\\skewer\\components\\config\\installer\\Service:rebuildSearchIndex',
            '\\skewer\\components\\config\\installer\\Service:resetActive',
            '\\skewer\\components\\config\\installer\\Service:makeSearchIndex',
            '\\skewer\\components\\config\\installer\\Service:makeSiteMap',
        ];
    }

}// class
