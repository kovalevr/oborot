<?php

/* main */
use skewer\base\site\Layer;

$aConfig['name']     = 'Copyright';
$aConfig['version']  = '1.0';
$aConfig['title']    = 'Копирайт';
$aConfig['description']  = 'Копирайт';
$aConfig['revision'] = '0001';
$aConfig['layer']     = Layer::PAGE;
$aConfig['useNamespace'] = true;

$aConfig['dependency'] = [
    ['Copyright', Layer::TOOL]
];

return $aConfig;
