<?php

namespace skewer\build\Page\Copyright;

use skewer\components\design\Design;
use yii\helpers\FileHelper;
use yii\web\AssetBundle;
use yii\web\View;

class Asset extends AssetBundle {

    public $sourcePath = '@skewer/build/Page/Copyright/web/';

    public $js = [
        'js/antiCopyright.js'
    ];

}
