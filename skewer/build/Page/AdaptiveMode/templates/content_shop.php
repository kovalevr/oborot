<?php
/**
 * @var string $asset_path
 * @var string $adaptive_menu_content
 */
?>

    <div class="l-sidebar hide-on-desktop show-on-tablet-l js-sidebar js-sidebar-hide"></div>
    <div class="l-sidebar-block l-sidebar-block--left hide-on-desktop show-on-tablet-l js-sidebar-block">
        <div class="b-sidebar">
            <div class="sidebar__close ">
                <div class="sidebar__close-btn js-sidebar-hide"></div>
            </div>
            <div class="sidebar__content">
                <?= $adaptive_menu_content ?>
            </div>
        </div>
    </div>

    <div class="l-sidebar js-sidebar-catalog js-sidebar-catalog-hide"></div>
    <div class="l-sidebar-block l-sidebar-block--left js-sidebar-catalog-block">
        <div class="b-sidebar-title">Каталог</div>
        <div class="b-sidebar">
            <div class="sidebar__close">
                <div class="sidebar__close-btn js-sidebar-catalog-hide"></div>
            </div>
            <div class="sidebar__content">
                <h2>Меню каталога</h2>
                <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Architecto ea id magnam qui quis, rerum vitae! Adipisci aspernatur consequuntur earum enim iure, labore numquam omnis qui ratione voluptates? Amet enim explicabo fugiat iste iure iusto laboriosam maiores nobis odit placeat.</p>
                <p>Consectetur eaque exercitationem explicabo incidunt, laboriosam mollitia nemo nulla obcaecati odit officia tenetur, voluptas! Accusamus aperiam architecto aut autem deserunt doloremque dolorum, ea eaque error facilis in iure iusto molestias natus neque nostrum quaerat qui recusandae repudiandae sequi unde voluptas.</p>
                <p>Accusantium autem consequatur eaque ex harum maxime nisi quasi voluptas? Assumenda, culpa dignissimos eos, esse exercitationem fuga itaque nisi non, omnis perferendis porro qui sed vero? Cum, deleniti dolore, dolorum eligendi fuga fugiat molestias praesentium quia quidem repellendus, repudiandae sint.</p>
                <p>Aliquid aut commodi consectetur culpa cumque delectus distinctio dolor error excepturi harum illo inventore laboriosam magni minus nemo nihil officia officiis omnis optio quos ratione recusandae sint sit, tempore temporibus! A autem dolor dolorem error incidunt non perspiciatis, praesentium veniam.</p>
                <p>Accusantium ducimus nesciunt non nostrum officiis omnis optio reiciendis tenetur. Earum eos impedit magni molestias non odio, provident! Assumenda cupiditate debitis deserunt dicta eius, in incidunt laboriosam modi molestias necessitatibus non pariatur perspiciatis possimus qui quos ratione sint, sunt tempore.</p>
                <p>Distinctio nulla recusandae suscipit. Ab ad at aut blanditiis cumque cupiditate deserunt distinctio eveniet impedit in itaque iure nulla officiis quis repellat repellendus repudiandae soluta, sunt ullam voluptatibus. Blanditiis dignissimos eum ex fugiat placeat quibusdam reiciendis similique tempora? Consequuntur, sapiente.</p>
                <p>Autem beatae dolorem earum impedit inventore iusto magni maxime, modi natus, possimus quae quidem quo similique! Deleniti dolor hic illo officiis quia quibusdam reiciendis rem! Aliquid autem consectetur cumque harum ipsa laborum nulla officiis quam quasi reprehenderit, sed vero, voluptate.</p>
                <p>Atque expedita natus repellat soluta sunt voluptas! Consequuntur esse eveniet, excepturi expedita, facilis iusto laudantium neque, nihil quibusdam sapiente sint sit veniam voluptatem. Deserunt dolore eius, labore minima modi mollitia, necessitatibus, quibusdam repellendus similique sunt unde voluptas voluptates. Nisi, odio.</p>
                <p>Facere, nemo recusandae? Beatae deserunt explicabo fugit harum iusto magni, nostrum quisquam repellat rerum sunt temporibus, tenetur velit? A, accusamus accusantium alias dignissimos dolores enim error ex facere itaque officia, provident quia quibusdam quos reiciendis suscipit temporibus ullam ut vero.</p>
                <p>Amet assumenda corporis debitis enim quasi qui repellendus! A asperiores aspernatur atque, beatae commodi consectetur distinctio doloribus eaque enim error esse eum expedita hic illo iste libero maxime molestias necessitatibus nesciunt nihil non officiis reiciendis repellendus similique sit tenetur totam?</p>
            </div>
        </div>
    </div>