$(function() {

    $('.js-sidebar-show').on('click', function(event) {
        event.preventDefault();
        showSidebar();
    });
    $('.js-sidebar-hide').on('click', function(event) {
        event.preventDefault();
        hideSidebar();
    });

    function showSidebar() {
        $('.js-sidebar').addClass('l-sidebar--open');
        $('.js-sidebar-block').addClass('l-sidebar-block--open');

        $('html, body').css('overflow', 'hidden');

    }

    function hideSidebar() {
        $('.js-sidebar').removeClass('l-sidebar--open');
        $('.js-sidebar-block').removeClass('l-sidebar-block--open');

        $('html, body').css('overflow', 'visible');
    }

    // Включение адаптивных слайдеров для каталогов на главной
        if ($('.js-catalogbox_gal_onmain').length) {

            $('.js-catalogbox_gal_onmain').each(function() {
                // var carouselOptions = $(this).data('carouselOptions');
                var carouselOptions = {
                    'items': 10,
                    'slideBy':'page',
                    'margin': 20,
                    'nav': true,
                    'navText': false,
                    'dots': false,
                    'autoWidth': false,
                    'responsive':{"0": {"items": 1}, "500": {"items": 1}, "768": {"items": 3}, "980": {"items": 3}, "1240": {"items": 3}},
                    'loop': true
                };
                $(this).addClass('owl-carousel owl-carousel-maintheme').owlCarousel(carouselOptions);

            });
        }

    // меню в сайдбаре
    $('.js-sidebarmenu-achor').on('click', function(event) {
        event.preventDefault();
        var $menuItem = $(this).parent('.js-sidebarmenu-achor-wrap').parent('.js-sidebarmenu-item');
        var $menuNextLvl = $(this).parents('.js-sidebarmenu-achor-wrap').next('.js-sidebarmenu-content');

        $menuItem.toggleClass('sidebar-menu__item--open');
        $menuNextLvl.stop(true, false).slideToggle('slow')
    });
});
