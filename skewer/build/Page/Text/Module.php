<?php

namespace skewer\build\Page\Text;

use skewer\base\section\Page;
use skewer\base\site_module;


/**
 * Публичный модуль добавления текста, обернутого в шаблон в зону вывода
 *
 * Все параметры из метки кроме системных (см. параметр $aSystemNames)
 * будут переданы на парсинг в шаблон
 */
class Module extends site_module\page\ModulePrototype {

    public $template = null;

    /**
     * Набор системных имен параметрв,
     * которые в парсинг не передаются
     * @var string[]
     */
    private static $aSystemNames = [
        'object',
        '.layout',
        '.title',
        'template'
    ];

    public function init() {

        $this->setParser(parserPHP);
        return true;

    }// func


    public function execute(){

        // достаем все параметры метки
        $aParameters = Page::getByGroup($this->getLabel());

        // перебираем их
        foreach ($aParameters as $aParam) {
            // системные параметры пропускаем
            if ( in_array($aParam['name'], self::$aSystemNames) )
                continue;
            // остальные добавляем в вывод
            $this->setData($aParam['name'], $aParam);
        }

        $this->setTemplate($this->template);

        return psComplete;

    }



} 