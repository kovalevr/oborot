<?php
    /**
     * @var $source array
     */
use skewer\components\design\Design;

?>


<? if (!empty($source['show_val'])): ?>
    <div class="b-editor"<? if (Design::modeIsActive()): ?> sktag="editor" skeditor="<?=$source['group']?>/source"<? endif; ?>>
        <?=$source['show_val']?>
    </div>
<? endif; ?>






