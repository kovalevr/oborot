<?php

    use \skewer\base\section\Parameters;
    use \skewer\base\section\Page;

    /**
    * @var $source string
    */

?>

<? if (!Page::getVal(Parameters::settings, 'headPersonRightshow') && !empty($source['show_val'])):?>
    <div class="b-picbox">
        <?= $source['show_val'] ?>
    </div>
<? endif; ?>