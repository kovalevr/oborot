<?php

namespace skewer\build\Page\Cart;

use skewer\base\log\Logger;
use skewer\base\orm\Query;
use skewer\build\Adm\Order as AdmOrder;
use skewer\base\section\Tree;
use skewer\build\Tool\Payments as Payments;

use skewer\build\Page;
use \skewer\build\Page\CatalogViewer;
use skewer\components\catalog\Card;
use skewer\components\catalog\GoodsRow;
use skewer\components\auth\models\Users;
use skewer\components\catalog\model\GoodsTable;
use skewer\components\forms\Entity;
use skewer\components\forms\Table;
use skewer\components\auth\Auth;
use skewer\components\auth\CurrentUser;
use skewer\components\ecommerce;
use skewer\base\site_module;
use skewer\base\site;
use yii\helpers\ArrayHelper;
use yii\web\NotFoundHttpException;

class Module extends site_module\page\ModulePrototype implements site_module\Ajax {

    /** @var int ID текущего раздела */
    public $sectionId;

    public $bIsFustBuy = false;

    /** @var string имя параметра для команды */
    private $sActionParamName = 'action';

    /**
     * Шаблон вывода списка
     * @var string
     */
    public $templateList = 'list.twig';

    private $bHaveUnavalGoods = false;

    /**
     * Инициализация
     * @return void
     */
    public function init() {
        $this->sectionId = $this->sectionId();
    }

    protected function getActionParamName() {
        return $this->sActionParamName;
    }

    public function getBaseActionName() {
        return 'list';
    }

    /**
     * Запуск модуля
     * @return int
     */
    public function execute() {

        $this->executeRequestCmd();

        $this->bIsFustBuy = $this->getStr('ajaxShow', 0);

        if ($this->getStr('cmd', '') && $this->bIsFustBuy){
            $this->sActionParamName = 'cmd';
        }

        return parent::execute();
    }

    /**
     * Список позиций заказа
     * @return int
     */
    public function actionList() {

        // подключен для js и css для кнопок "+" и "-"
        CatalogViewer\Asset::register(\Yii::$app->getView());

        $oOrder = Api::getOrder();

        $oOrder = $this->validateGoods($oOrder);

        $aItems = $oOrder->getItems();

        $aItems = ecommerce\Api::addEcommerceDataInGoodsOrder($aItems);

        foreach ($aItems as &$aObject)
            $aObject['show_detail'] = (int)!Card::isDetailHiddenByCard($aObject['card']);

        $oOrder->setItems($aItems);

        $this->setTemplate($this->templateList);
        $this->setData('order', $oOrder);
        $this->setData('sectionId', $this->sectionId);
        $this->setData('mainSection', \Yii::$app->sections->main());

        return psComplete;
    }

    /**
     * Форма оформления заказа
     * @return int
     * @throws NotFoundHttpException
     *
     */
    public function actionCheckout() {

        // Заказ в один клик?
        if ($this->bIsFustBuy){

            $this->sectionId = \Yii::$app->sections->getValue('cart');

            $oOrderCart = Api::getOrder($this->bIsFustBuy);
            $oOrderCart->unsetAll();
            Api::setOrder($oOrderCart, true);

            $iObj = $this->getInt('idObj');

            if (!$iObj) return 0;
            $count = $this->getInt('count',1);
            if (!$count) return 0;
            Api::setItem($iObj, $count, true);
            $this->setData('fastBuy',$this->bIsFustBuy);
        }

        $oOrderCart = Api::getOrder($this->bIsFustBuy);
        $ajaxForm = $this->getInt('ajaxForm');
        $sLabel = ($this->get('label'))?:$this->oContext->getLabel();
        if ($section = \Yii::$app->request->post('section'))
            $section = ($aSection = Tree::getSection($section, true)) ? $aSection['id'] : $this->sectionId();
        else
            $section = $this->sectionId();

        // Не выводим форму, если заказ пустой
        if (!$oOrderCart->getCount())
            throw new NotFoundHttpException();

        /*Проверим доступность товаров которые лежат в админке*/
        $oOrderCart = $this->validateGoods($oOrderCart);

        $this->setTemplate('checkout.twig');
        $this->setData('order', $oOrderCart);

        $sCmd = $this->get('cmd');
        $oFormRow = ($this->bIsFustBuy)?Table::getByName(Entity::NAME_FORM_ONE_CLICK):Table::getByName(Entity::NAME_FORM_ORDER);
        if ($oFormRow) {
            $iFormId = $oFormRow->getId();

            switch ($sCmd) {
                case 'send':
                    try {
                        $aData = $this->getPost();

                        /*Если какие то товары стали неактивными уйдем в предыдущее состояние*/
                        if ($this->bHaveUnavalGoods) {
                            /*Кладем пришедшие с формы данные во временный контейнер*/
                            \Yii::$app->session->set('CartFormContainer',$aData);
                            \Yii::$app->response->redirect('/cart');
                            \Yii::$app->end();
                        }

                        $oForm = Table::build($iFormId, $aData);
                        $formHash = $oForm->getHash((int)\Yii::$app->request->post('section', $section), $this->get('label') );

                        if (!$oForm->validate($formHash))
                            throw new \Exception($oForm->getError());

                        $oService = new AdmOrder\Service();
                        $id = $oService->saveOrder($oForm);
                        $sToken = '';
                        if ($id) {
                            $aOrder = AdmOrder\ar\Order::find()->where('id', $id)->asArray()->getOne();
                            if (isset($aOrder['token']))
                                $sToken = $aOrder['token'];
                        }

                        $sCanapeuuid = isset($aData['_canapeuuid']) ? $aData['_canapeuuid'] : '';

                        // Разрешаем отправлять ecommerce данные
                        \Yii::$app->session->setFlash('ecommerceSend', true);

                        \Yii::$app->getResponse()->redirect(\Yii::$app->router->rewriteURL("[$this->sectionId][Cart?action=done&token=$sToken&canapeuuid=$sCanapeuuid]"), '301')->send();
                    } catch (\Exception $e) {
                        Logger::dump($e->getMessage());//!!!
                        $sNewUrl = str_replace('response/', '', \Yii::$app->request->pathInfo);
                        \Yii::$app->response->redirect(site\Site::httpDomainSlash() . $sNewUrl)->send();
                    }
                    break;
                    default:

                        /*Если какие то товары стали неактивными уйдем в предыдущее состояние*/
                        if ($this->bHaveUnavalGoods) {
                            \Yii::$app->response->redirect('/cart');
                            \Yii::$app->end();
                        }

                        $aData = [];
                        /* Данные из личного кабинета */
                        if (($iUserId = CurrentUser::getId()) && (CurrentUser::getPolicyId() != Auth::getDefaultGroupId()))
                            $aData = Users::find()->where(['id' => $iUserId])->asArray()->one();

                        /*Если в контейнере есть данные которые пытались ввести в форму, подтянем их*/
                        if (!is_null(\Yii::$app->session->get('CartFormContainer'))) {
                            $aData = \Yii::$app->session->get('CartFormContainer');
                            \Yii::$app->session->set('CartFormContainer',null);
                        }

                        $aData['fastBuy'] = $this->bIsFustBuy;
                        $oForm = Table::build($iFormId, $aData);

                        $formHash = $oForm->getHash( (int)\Yii::$app->request->post('section', $section), $sLabel );
                        $aParamForm = [
                            'oForm' => $oForm,
                            'formHash' => $formHash,
                            'fastBuy' => $this->bIsFustBuy,
                            'card' => 1,
                            'label' => $sLabel,
                            'section_path' => Tree::getSectionAliasPath($this->sectionId, true),
                            'section' => $section
                        ];

                        if ($ajaxForm) {
                            $aParamForm['ajaxShow'] = 1;
                            $aParamForm['ajaxForm'] = 1;
                        }
                        $sFormTemplate = site_module\Parser::parseTwig('form.twig',$aParamForm,
                            BUILDPATH . 'Page/Forms/templates');
                        $this->setData('form', $sFormTemplate);

                        $sTitle = \Yii::t('order', 'title_checkout');
                        site\Page::setTitle($sTitle);
                        site\Page::setAddPathItem($sTitle);
            }
        } else
            $this->setData('error',\Yii::t('order','error_form'));

        return psComplete;
        
    }

    /**
     * Завершение заказа
     * @return int
     */
    public function actionDone() {

        // rename title and pathline
        $sTitle =\Yii::t('order', 'title_checkout' );

        site\Page::setTitle( $sTitle );

        site\Page::setAddPathItem( $sTitle );

        $sToken = $this->get('token','');

        if ($sToken){

            $aOrder =  AdmOrder\ar\Order::find()->where('token', $sToken)->order('id','DESC')->asArray()->getOne();

            if ($aOrder){

                $aGoods = AdmOrder\ar\Goods::find()->where('id_order', $aOrder['id'])->asArray()->getAll();

                $total = 0;

                foreach ($aGoods as $aItem)
                    $total += $aItem['total'];

                if ($total>0){

                    if ($aOrder['status'] == AdmOrder\model\Status::getIdByNew()) {

                        // Отправляем e-commerce данные только один раз
                        if ( \Yii::$app->session->getFlash('ecommerceSend', false) ){
                            $this->setData('ecommerce', ecommerce\Api::buildScriptPurchase($aOrder['id']) );
                        }

                        /**
                         * @var $oPaymentType AdmOrder\ar\TypePaymentRow
                         */
                        $oPaymentType = AdmOrder\ar\TypePayment::find( $aOrder['type_payment'] );

                        if ($oPaymentType && $oPaymentType->payment){
                            /**
                             * @var Payments\Payment $oPayment
                             */
                            $oPayment = Payments\Api::make( $oPaymentType->payment );

                            if ($oPayment){
                                $oPayment->setOrderId($aOrder['id']);
                                $oPayment->setSum($total);

                                $this->setData( 'paymentForm', $oPayment->getForm() );
                            }
                        }

                    }
                }
            }
        }

        $this->setTemplate( 'done.twig' );
        $this->setData('mainSection', \Yii::$app->sections->main());

        return psComplete;
    }

    /**
     * Добавление новой позиции в заказ
     * @return void
     */
    public function cmdSetItem() {

        $objectId = $this->getInt('objectId');
        $count = $this->getInt('count');

        $bErrorCount = false;

        $sGoods = GoodsRow::get($objectId);

        if ( $count and ($count > 0) )
            $bErrorCount = !Api::setItem($objectId, $count, false, $sGoods->getExtRow()->getModel()->getName());

        if ($sGoods) {
            $aFields = $sGoods->getData();
            $sTitle = $aFields['title'];
        } else
            $sTitle = '';
        $cartSectionLink =  Tree::getSectionAliasPath(\Yii::$app->sections->getValue('cart'));

        $sTemplate = site_module\Parser::parseTwig('fancy_panel.twig',['cartSectionLink'=>$cartSectionLink,'errorCount'=>$bErrorCount,'title'=>$sTitle],BUILDPATH.'Page/Cart/templates/');

        Api::sendJSON(false,
            ['sTemplate' => $sTemplate]
        );
    }

    /**
     * Удаление позиции заказа
     * @return void
     */
    public function cmdRemoveItem() {

        $id = $this->getInt('id');
        $oOrderCart = Api::getOrder();
        $oOrderCart->unsetItem($id);

        Api::setOrder($oOrderCart);
        Api::sendJSON();
    }

    /**
     * Пересчет количества позиции
     * @return void
     */
    public function cmdRecountItem() {

        $id = $this->getInt('id');
        $count = $this->getInt('count');

        if (!$count || $count<0) exit;

        $oOrderCart = Api::getOrder();

        if ($aItem = $oOrderCart->getItemById($id)) {

            $aItem['count'] = $count;
            $aItem['total'] = $count * Api::priceFormat($aItem['price']);
            $oOrderCart->setItem($aItem);
            Api::setOrder($oOrderCart);
        }

        Api::sendJSON();
    }

    /**
     * Удаление всех позиций
     * @return void
     */
    public function cmdUnsetAll() {

        $oOrderCart = Api::getOrder();
        $oOrderCart->unsetAll();

        Api::setOrder($oOrderCart);
        Api::sendJSON();
    }

    /**
     * Проверяет что все товары лежащие в корзине активны и существуют
     * @param $oOrderCart
     * @return mixed
     */
    private function validateGoods($oOrderCart){

        $aItems = $oOrderCart->getItems();

        $aGoodsIds = ArrayHelper::getColumn($aItems, 'id_goods');

        if ( !$aGoodsIds )
            return $oOrderCart;

        /*Попытаемся достать товары с такими Id при условии что они активны*/
        $aGoods = Query::SelectFrom( 'co_base_card' )
            ->fields('id')
            ->where('id',$aGoodsIds)
            ->where('active',1)
            ->asArray()
            ->getAll();

        $aExistsGoodsIds = ArrayHelper::getColumn($aGoods, 'id');

        /*Получим разницу. Собственно это IDs которые есть в заказе, но их уже нет на сайте*/
        $aDiffIds = array_diff($aGoodsIds,$aExistsGoodsIds);

        if (!empty($aDiffIds)) {
            foreach ($aItems as &$item) {
                if (array_search($item['id_goods'], $aDiffIds) !== false)
                    $item['not_available'] = 1;
            }

            $oOrderCart->setItems($aItems);

            $this->bHaveUnavalGoods = true;
        }

        return $oOrderCart;
    }

} 