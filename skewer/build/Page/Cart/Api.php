<?php

namespace skewer\build\Page\Cart;

use skewer\base\Twig;
use skewer\base\SysVar;

/**
 * Методы для работы с заказом корзины
 * Class Api
 * @package skewer\build\Page\Cart
 */
class Api {

    const SESSION_KEY      = '_cart_storage';
    const FAST_SESSION_KEY = '_fast_buy_storage';
    const PRICE_FIELDNAME  = 'price';

    /**
     * Форматирование вывода копеек
     * @param $mPrice
     * @return int|float
     */
    public static function priceFormat($mPrice) {

        return (SysVar::get('catalog.hide_price_fractional')) ? (int)$mPrice : (float)$mPrice;
    }

    /**
     * Форматирование вывода копеек для запросов через ajax
     * @param $mPrice
     * @return int|float
     */
    public static function priceFormatAjax($mPrice){

        return Twig::priceFormat($mPrice);
    }

    /**
     * Добавление торава в корзину
     * Если уже есть, количество складывается
     * @param $iObjectId int ID товара
     * @param $iCount int Количество
     * @param $bFast bool быстрый заказ
     * @return bool true в случае успеха и false если превышено максимальное число заказа
     */
    public static function setItem($iObjectId, $iCount, $bFast = false, $sCard = 'group1') {

        $oOrderCart = self::getOrder($bFast);
        $aCartItem  = $oOrderCart->getExistingOrNew($iObjectId);
        if (!$aCartItem) return true; // Ошибочно задан объект заказа

        // Если в заказ добавляется новая позиция и не позволяет предел, то отменить
        if ( !$aCartItem['count'] and (count($oOrderCart->getItems()) >= self::maxOrderSize()) )
            return false;

        $aCartItem['count'] += $iCount;
        $aCartItem['total'] = $aCartItem['count'] * self::priceFormat($aCartItem['price']);
        $aCartItem['card'] = $sCard;

        $oOrderCart->setItem($aCartItem);

        self::setOrder($oOrderCart, $bFast);
        return true;
    }

    /**
     * Сохраняет объект заказа в сессию
     * @param Order $oOrderCart Объект заказа
     * @param $bFast bool быстрый заказ
     */
    public static function setOrder(Order $oOrderCart, $bFast = false) {

        if ($bFast)
            $_SESSION[self::FAST_SESSION_KEY] = serialize($oOrderCart);
        else
            $_SESSION[self::SESSION_KEY] = serialize($oOrderCart);
    }

    /**
     * Очищает корзину заказов текущего пользователя
     * @param $bFast bool быстрый заказ. Если не указан, то очищает полностью
     */
    public static function clearOrder($bFast = null) {

        if ($bFast or $bFast === null)
            unset($_SESSION[self::FAST_SESSION_KEY]);

        if (!$bFast or $bFast === null)
            unset($_SESSION[self::SESSION_KEY]);
    }

    /**
     * Возвращает объект заказа
     * @param $bFast bool быстрый заказ
     * @return Order
     */
    public static function getOrder($bFast = false) {

        if ($bFast)
            return isset($_SESSION[self::FAST_SESSION_KEY]) ? unserialize($_SESSION[self::FAST_SESSION_KEY]) : new Order();
        else
            return isset($_SESSION[self::SESSION_KEY]) ? unserialize($_SESSION[self::SESSION_KEY]) : new Order();
    }

    /** Получить/установить максимальное число позиций в одном заказе */
    public static function maxOrderSize() {

        return func_num_args() ? SysVar::set('Order.order_max_size', (int)func_get_arg(0)) : SysVar::get('Order.order_max_size', 500);
    }

    /** Получить/установить максильное число отображаемых заказов в личном кабинете */
    public static function maxOrdersOnPage() {

        return  func_num_args() ? SysVar::set('Order.onpage_profile', (int)func_get_arg(0)) : SysVar::get('Order.onpage_profile', 10);
    }

    /**
     * Отсылает заказ в JSON
     * @param $bFast bool быстрый заказ
     * @param $aParamsAdded array Дополнительные параметры
     * @return void
     */
    public static function sendJSON($bFast = false, array $aParamsAdded = []) {

        $aOrderCart = self::getOrder($bFast);

        echo json_encode(
            [
                'lastItem' => $aOrderCart->getItemLast(),
                'count'    => $aOrderCart->getTotalCount(),
                'total'    => self::priceFormatAjax($aOrderCart->getTotalPrice()),
            ] + $aParamsAdded);
        exit;
    }
}