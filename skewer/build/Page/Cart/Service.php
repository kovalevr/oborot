<?php

namespace skewer\build\Page\Cart;

use skewer\build\Adm\Order\ar\TypeDelivery;
use skewer\base\site\ServicePrototype;
use skewer\build\Adm\Order\ar\TypePayment;

/**
 * Class Service
 * Удаленный запуск функция
 */
class Service extends ServicePrototype {

    /**
     * Получение всех типов доставки
     * @return array
     */
    public function getTypeDelivery(){

        $aTypeDelivery = TypeDelivery::find()->asArray()->getAll();
        $aResult = [];
        if ($aTypeDelivery)
            foreach ($aTypeDelivery as $aValue) {
                $aResult[$aValue['id']] = $aValue['title'];
            }
        return $aResult;
    }

    /**
     * Получение всех типов оплаты
     * @return array
     */
    public function getTypePayment(){

        $aTypeDelivery = TypePayment::find()->asArray()->getAll();
        $aResult = [];
        if ($aTypeDelivery)
            foreach ($aTypeDelivery as $aValue) {
                $aResult[$aValue['id']] = $aValue['title'];
            }
        return $aResult;
    }
}
