<?php

namespace skewer\build\Page\CatalogViewer\State;


use skewer\components\ecommerce;
use skewer\components\catalog;
use skewer\base\section\Page;
use skewer\build\Page\CatalogViewer;
use skewer\base\site;
use yii\helpers\ArrayHelper;


class ListOnMain extends Prototype {

    /** @var array Набор товарных позиций для вывода */
    private $aGoods = array();

    /** @var int Всего найдено товарных позиций */
    private $iAllCount = 0;

    /** @var int Кол-во позиций на страницу */
    private $iCount = 3;

    /** @var string Шаблон для вывода */
    private $sTpl = 'gallery';

    /** @var array Набор шаблонов для каталога */
    public static $aTemplates = [
        'list' => [
            'title' => 'Editor.type_catalog_list',
            'file'  => 'OnMain.list.twig',
        ],

        'gallery' => [
            'title' => 'Editor.type_catalog_gallery',
            'file'  => 'OnMain.gallery.twig',
        ],
    ];

    public function init() {

        \Yii::$app->router->setLastModifiedDate(catalog\model\GoodsTable::getMaxLastModifyDate());                

        $this->iCount = abs($this->getModuleField( 'onPage', $this->iCount ));

        $sTpl = Page::getVal( 'catalog', 'type_catalog_view' );

        if ($sTpl && $sTpl == 'list'){
            $this->sTpl = 'OnMain.list.twig';
        }

        // получение набора товаров
        if ( $this->iCount > 0 )
            $this->getGoods();

    }


    public function build() {

        // Рейтинг
        $this->addRating($this->aGoods);

        // парсинг
        $this->oModule->setData( 'aObjectList', $this->aGoods );
        $this->oModule->setData( 'titleOnMain', $this->getModuleField( 'titleOnMain' ) );
        $this->oModule->setData( 'form_section', $this->getModuleField( 'buyFormSection' ) );
        $this->oModule->setData( 'useCart', site\Type::isShop() );
        $this->oModule->setData( 'moduleGroup', $this->getModuleGroup() );

        // шаблон
        $this->sTpl = $this->getModuleField( 'listTemplate' );
        $this->sTpl = ArrayHelper::getValue( self::$aTemplates, $this->sTpl, self::$aTemplates['list'] )['file'];
        $this->oModule->setTemplate( $this->sTpl );
    }


    /**
     * Получение списка товарных позиций для текущей страницы
     * @return bool
     */
    private function getGoods() {

        $field = $this->getModuleField( 'onMain' );

        if ( !$this->iCount )
            return false;

        if ( !in_array( $field, ['hit', 'new', 'on_main'] ) )
            $field = 'on_main';

        $this->aGoods = catalog\GoodsSelector::getList( catalog\Card::DEF_BASE_CARD, true )
            ->onlyVisibleSections()
            ->condition( 'active', 1 )
            ->condition( $field, 1 )
            ->limit( $this->iCount, 1, $this->iAllCount )
            ->sortByRand()
            ->withSeo($this->getSection())
            ->parse()
        ;

        $this->aGoods = ecommerce\Api::addEcommerceDataInGoods($this->aGoods, $this->sectionId(), $this->getModuleField('titleOnMain', "" ));

        foreach ($this->aGoods as &$aObject)
            $aObject['show_detail'] = (int)!catalog\Card::isDetailHiddenByCard($aObject['card']);

        return true;
    }


}