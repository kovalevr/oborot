<?php

namespace skewer\build\Page\CatalogViewer\State;

use skewer\build\Design\Zones;
use skewer\build\Catalog\Goods\SeoGood;
use skewer\build\Catalog\Goods\SeoGoodModifications;
use skewer\build\Page\RecentlyViewed;
use skewer\components\catalog;
use skewer\build\Page\CatalogViewer;
use skewer\base\site;
use skewer\build\Tool\Review;
use skewer\base\ft;
use skewer\base\site_module\Context;
use skewer\base\SysVar;
use skewer\components\seo\Api;
use skewer\helpers\Paginator;
use yii\web\NotFoundHttpException;
use skewer\build\Page\GuestBook;
use skewer\components\GalleryOnPage\Api as GalOnPageApi;
use skewer\components\ecommerce;


class DetailPage extends Prototype {

    /** Метка процесса "Отзывы в товарах" */
    const LABEL_GOODSREVIEWS = 'GoodsReviewsModule';

    /** @var array Данные товарной позиции */
    private $aGood = [];

    /** @var string Шаблон вывода */
    private $sTpl = 'SimpleDetail.twig';

    /** @var string Псевдоним текущего товара */
    private $sGoodsAlias = '';

    /** @var int ID текущего товара */
    private $iGoodsId = 0;

    protected $bShowFilter = false;


    public function init() {

        /*Если установлено скрытие детальных, 404*/
        if (catalog\Card::isDetailHidden($this->sectionId())) throw new NotFoundHttpException();

        $this->oModule->setStatePage( Zones\Api::DETAIL_LAYOUT );

        $sBaseCardName = catalog\Card::DEF_BASE_CARD; // fixme переделать при нескольких базовых карточек

        $this->aGood = catalog\GoodsSelector::get( $this->sGoodsAlias ? $this->sGoodsAlias : $this->iGoodsId , $sBaseCardName, false, $this->sectionId() );

        \Yii::$app->router->setLastModifiedDate($this->aGood['last_modified_date']);

        // Не отдавать модификацию товара, если выключен показ модификаций
        if (!SysVar::get('catalog.goods_modifications') and ($this->aGood['id'] != $this->aGood['main_obj_id']))
            $this->aGood = null;

        // проверка на существование и активность
        if ( empty($this->aGood) || !$this->aGood['active'] )
            throw new NotFoundHttpException('Не найдена товарная позиция');

        $aGoods = ecommerce\Api::addEcommerceDataInGoods([$this->aGood], $this->sectionId(), 'Детальная страница');
        $this->aGood = reset($aGoods);

        // проверка на раздел
        $aSectionList = catalog\Section::getList4Goods( $this->aGood['main_obj_id'], $this->aGood['base_card_id'] );
        if ( !is_array($aSectionList) || !in_array( $this->getSection(), $aSectionList ) )
            throw new NotFoundHttpException('Не найдена товарная позиция');

        CatalogViewer\AssetDetail::register(\Yii::$app->view);

    }


    public function build() {

        $iObjectId = $this->aGood['id'];
        $this->oModule->setEnvParam('iCatalogObjectId', $iObjectId);

        $iMainObjId = (isSet( $this->aGood['main_obj_id'] ) and $this->aGood['main_obj_id']) ? $this->aGood['main_obj_id']: $iObjectId;

        /*Если это нормальный товар и не модификация, попробуем добавить "ближайшие товары" */
        if ( $iMainObjId == $iObjectId )
            $this->showNearItems( $iObjectId );


        // товары аналоги
        if ( SysVar::get('catalog.goods_modifications') )
            $this->showModificationsItems( $iMainObjId, $iObjectId );

        // Ранее просмотренные товары
        $this->showRecentlyViewed();

        // товары идущие в комплекте товаров
        if ( SysVar::get('catalog.goods_include') )
            $this->showIncludedItems( $iObjectId );

        // модуль сопутствующих товаров
        if ( SysVar::get('catalog.goods_related') )
            $this->showRelatedItems( $iObjectId );

        // Показать рейтинг. Разрешить в нём голосование, если отключены отзывы к товару. Иначе голосование осуществляется через отзывы
        $this->addRating($this->aGood, $this->showingReviews());

        // Вывод микроразметки отзывов
        if ($this->showingReviews())
            $this->oModule->setData('reviews',$this->getMicroDataReview());

        // отдаем данные на парсинг
        $this->oModule->setData( 'aObject', $this->aGood );
        $this->oModule->setData( 'aTabs', $this->buildTabs() );
        $this->oModule->setData( 'form_section', $this->getModuleField( 'buyFormSection' ) );
        $this->oModule->setData( 'useCart', site\Type::isShop() );
        $this->oModule->setData( 'isMainObject', $iMainObjId == $iObjectId );
        $this->oModule->setData( 'hide2lvlGoodsLinks', SysVar::get('catalog.hide2lvlGoodsLinks') );
        $this->oModule->setData( 'hideBuy1lvlGoods', SysVar::get('catalog.hideBuy1lvlGoods') );
        $this->oModule->setData('currency_type', SysVar::get('catalog.currency_type'));
        $this->oModule->setTemplate( $this->sTpl );

        $this->setSeo($this->aGood);
        site\Page::setTitle( $this->aGood['title'] );
        site\Page::setAddPathItem( $this->aGood['title'] );

    }


    /**
     * Поиск товара по идентификаторам
     * @param int $iGoodsId
     * @param string $sGoodsAlias
     * @return bool
     */
    public function findGoods( $iGoodsId, $sGoodsAlias = '' ) {

        $this->iGoodsId = $iGoodsId;
        $this->sGoodsAlias = $sGoodsAlias;

        return true;
    }

    public function setSeo($aGood){

        // Убрать статический контент
        $oPage = site\Page::getRootModule();

        if ( $aGood['main_obj_id'] != $aGood['id'] ){
            $oSeo = new SeoGoodModifications(0, $this->oModule->sectionId(), $aGood);
        } elseif (!empty($aGood['card'])){
            $oSeo = new SeoGood(0, $this->oModule->sectionId(), $aGood);
            $oSeo->setExtraAlias($aGood['card']);
        } else
            $oSeo = null;

        $this->oModule->setEnvParam(Api::SEO_COMPONENT, $oSeo );

        if (isset($aGood['canonical_url']))
            $oPage->setData('canonical_url', $aGood['canonical_url']);

        $oSeo->initSeoData();
        /** Вывод description в шаблон(для микроразметки) */
        $this->oModule->setData( 'seo_description', ($oSeo->description)? $oSeo->description : $oSeo->parseField( 'description', ['sectionId' => $oSeo->getSectionId()] ) );

        $this->oModule->setEnvParam( Api::OPENGRAPH,
            \Yii::$app->getView()->renderPhpFile(
                $this->oModule->getModuleDir() . DIRECTORY_SEPARATOR . $this->oModule->getTplDirectory() . DIRECTORY_SEPARATOR . 'OpenGraph.php',
                ['aGood' => $aGood, 'oSeoComponent' => $oSeo]
            )
        );

        site\Page::reloadSEO();

    }

    /**
     * Выводит модуль перехода на ближайшие элементы
     * @param $iObjectId
     * todo протащить поле для сотритовки и фильтр
     */
    protected function showNearItems( $iObjectId ) {

        $aData = array(
            'section' => $this->sectionId(),
            'next' => catalog\GoodsSelector::getNext( $iObjectId, $this->sectionId() ),
            'prev' => catalog\GoodsSelector::getPrev( $iObjectId, $this->sectionId() )
        );

        \Yii::$app->router->setLastModifiedDate(catalog\model\GoodsTable::getMaxLastModifyDate());

        if ($iObjectId!=$aData['next']['id'] && $iObjectId!=$aData['prev']['id'])
            $this->oModule->setData('nearItems', $aData );
    }


    /**
     * Вывод модуля сопутствующих товаров
     * @param $iObjectId
     */
    private function showRelatedItems( $iObjectId ) {

        $sTpl = $this->getModuleField( 'relatedTpl' );
        if ( !in_array( $sTpl, array( 'list', 'gallery', 'table' ) ) )
            $sTpl = 'gallery';

        $aObjectList = catalog\GoodsSelector::getRelatedList($iObjectId)
            ->onlyVisibleSections()
            ->condition( 'active', 1 )
            ->withSeo($this->getSection())
            ->parse()
        ;

        \Yii::$app->router->setLastModifiedDate(catalog\model\GoodsTable::getMaxLastModifyDate());
        if ((empty($aObjectList)) and (SysVar::get('catalog.random_related'))) {

            $aObjectList = catalog\GoodsSelector::getRelatedList4ObjectRand($iObjectId,$this->sectionId());

            if ($aObjectList)
                $aObjectList = $aObjectList->condition('active', 1)
                    ->withSeo($this->getSection())
                    ->parse();
            else return;

        }

        if ( count($aObjectList) ) {

            $aObjectList = ecommerce\Api::addEcommerceDataInGoods($aObjectList, $this->sectionId(), \Yii::t('catalog','related_product'));

            $aData['section'] = $this->sectionId();

            // Рейтинг
            $this->addRating($aObjectList);

            $this->oModule->setData( 'relatedTpl', $sTpl );
            $this->oModule->setData( 'relatedItems', $aData );
            $this->oModule->setData( 'aRelObjList', $aObjectList );

            $this->oModule->setData( 'gallerySettings_related', htmlentities(GalOnPageApi::getSettingsByEntity('Related',true), ENT_QUOTES, 'UTF-8'));
        }

    }


    /**
     * Вывод товаров из комплекта
     * @param $iObjectId
     */
    private function showIncludedItems( $iObjectId ) {

        $sTpl = $this->getModuleField( 'includedTpl' );
        if ( !in_array( $sTpl, array( 'list', 'gallery', 'table' ) ) )
            $sTpl = 'gallery';

        $aObjectList = catalog\GoodsSelector::getIncludedList($iObjectId)
            ->onlyVisibleSections()
            ->condition( 'active', 1 )
            ->withSeo($this->getSection())
            ->parse()
        ;

        \Yii::$app->router->setLastModifiedDate(catalog\model\GoodsTable::getMaxLastModifyDate());

        if ( count($aObjectList) ) {

            $aObjectList = ecommerce\Api::addEcommerceDataInGoods($aObjectList, $this->sectionId(), \Yii::t('catalog','included_product'));

            $aData['section'] = $this->sectionId();

            // Рейтинг
            $this->addRating($aObjectList);
            $this->oModule->setData('gallerySettings_included', htmlentities(GalOnPageApi::getSettingsByEntity('Included',true), ENT_QUOTES, 'UTF-8'));
            $this->oModule->setData( 'includedTpl', $sTpl );
            $this->oModule->setData( 'includedItems', $aData );
            $this->oModule->setData( 'aIncObjList', $aObjectList );
        }

    }


    /**
     * Вывод товаров аналогов
     * @param int $iObjectId Ид текущего товара
     * @param int $iExcludeId Id Объекта для исключения из списка модификаций. По умолчанию = id родительского объекта
     */
    private function showModificationsItems($iObjectId, $iExcludeId = 0 ) {

        $aObjectList = catalog\GoodsSelector::getModificationList( $iObjectId, $iExcludeId )
            ->condition( 'active', 1 )
            ->withSeo($this->getSection())
            ->parse()
        ;

        \Yii::$app->router->setLastModifiedDate(catalog\model\GoodsTable::getMaxLastModifyDate());

        if ( count($aObjectList) ) {

            $aObjectList = ecommerce\Api::addEcommerceDataInGoods($aObjectList, $this->sectionId(), 'Список модификаций');

            $aData['section'] = $this->sectionId();

            // Рейтинг
            $this->addRating($aObjectList);

            $this->oModule->setData( 'ModificationsItems', $aData );
            $this->oModule->setData( 'aAngObjList', $aObjectList );
        }

    }

    /**
     * Вернет микроразметку отзывов к выбранному товару
     * @return string
     */
    private function getMicroDataReview() {

        $oReviews = new Context( 'GoodsReviewsModule', GuestBook\Module::className(), ctModule );
        $oReviews->setParams([
            'className' => Review\Api::GoodReviews,
            'objectId' => $this->aGood['id'],
            'bOnlyMicrodata' => true
        ]);

        Paginator::$bHideCanonicalPagination = true;

        $this->oModule->setEnvParam('iCatalogObjectId', $this->aGood['id'] );

        $oReviewsProcess = $this->oModule->addChildProcess( $oReviews );
        $oReviewsProcess->execute();
        $oReviewsProcess->render();

        return $oReviewsProcess->getOuterText();
    }


    /**
     * Вывод отзывов к товару
     * @return array
     */
    private function showReviews() {

        $bHideForm = (bool)SysVar::get('catalog.hide_review_form');

        $oReviews = new Context( 'GoodsReviewsModule', GuestBook\Module::className(), ctModule );
        $oReviews->setParams([
            'className' => Review\Api::GoodReviews,
            'objectId' => $this->aGood['id'],
            'actionForm'=>'#tabs-reviews',
            'rating'      => (bool)SysVar::get('catalog.show_rating'), // Разрешить голосование из формы отзывов
            'rating_list' => -1, // Не отображать рейтинг в отзывах
            'sTabName'=>'reviews', //прокидываем имя таба к которому надо будет вернуться
            'iPage' => $this->getModule()->get('page',1), //номер страницы, полученных из урла по правилам роутинга
            'hide_form' => $bHideForm //прокидываем параметр скрытия формы отзывов
        ]);

        $oReviewsProcess = $this->oModule->addChildProcess( $oReviews );
        $oReviewsProcess->execute();
        $oReviewsProcess->render();
        
        if (!$oReviewsProcess->getContext()->oProcess->getModule()->iCountItems && $bHideForm)
            return [];

        return array(
            'name' => 'reviews',
            'title' => \Yii::t('catalog', 'reviews'),
            'html' =>  $oReviewsProcess->getOuterText()
        );
    }

    private function buildTabs() {

        $aTabs = array();
        //имя таба
        $sTabName = $this->getModule()->get('tab');

        foreach( $this->aGood['fields'] as $oField ){
            if ( !empty($oField['attrs']['show_in_tab']) && $oField['value'] && $oField['html']  ) {
                $aTabs[] = $oField;
            }
        }

        // модуль вывода отзывов
        if ( $this->showingReviews() ){
            $aReviewTab = $this->showReviews();
            if (!empty($aReviewTab))
                $aTabs[] = $aReviewTab;
        }

        //активные табы
        if (!is_null($sTabName)){
            foreach ($aTabs as &$tab){
                if ($tab['name'] === $sTabName) $tab['active'] = 1;
            }
        }

        return $aTabs;
    }

    /** Показывать отзывы к товару? */
    private function showingReviews() {

        return $this->oModule->showReviews or SysVar::get('catalog.guest_book_show');
    }

    /** Вывод блока "Ранее просмотренные товары" */
    private function showRecentlyViewed(){

        if ( !\Yii::$app->register->moduleExists( RecentlyViewed\Module::getNameModule(), site\Layer::PAGE) )
            return;

        $oContext = new Context( RecentlyViewed\Module::getNameModule(), RecentlyViewed\Module::className(), ctModule );
        $oContext->setParams([
            'iGoodsId' => $this->aGood['id'],
            'buyFormSection' => $this->getModuleField( 'buyFormSection' ),
            'sTpl'     => $this->getModuleField('recentlyViewedTpl'),
            'iOnPage'  => $this->getModuleField('recentlyViewedOnPage'),
            'bShow'     => (bool) SysVar::get('catalog.goods_recentlyViewed')
        ]);

        $oProcess = $this->oModule->addChildProcess( $oContext );
        $oProcess->execute();
        $oProcess->render();

        $this->oModule->setData('RecentlyViewed', $oProcess->getOuterText());

    }

}