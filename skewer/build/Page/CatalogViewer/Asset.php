<?php

namespace skewer\build\Page\CatalogViewer;

use yii\web\AssetBundle;
use yii\web\View;

class Asset extends AssetBundle {
    
    public $sourcePath = '@skewer/build/Page/CatalogViewer/web/';

    public $css = [
        'css/catalog.css',
        'css/filter.css',
        'css/tab.css',
        'css/catalog__param.css',
        'css/owl-carousel-collectiontheme.css',
        'css/owl-carousel-maintheme.css'
    ];

    public $js = [
        'js/catalog.js'
    ];

    public $jsOptions = [
        'position'=>View::POS_HEAD
    ];

    public $depends = [
        'skewer\libs\fotorama\Asset',
        'skewer\build\Page\Cart\Asset',
        'skewer\libs\owlcarousel\Asset',
        'skewer\libs\jquery\UiAsset'
    ];


}
