$(document).ready(function(){
    // $(window).load(function () {

    var oFotoramaConfig = {
        width: '100%',
        click: true,
        shadows: false,
        arrows: 'always',
        nav: 'thumbs',
        thumbwidth: 80,
        thumbheight: 55,
        allowfullscreen: true
    };


    // инициализация табов
    $(".js-tabs").tabs({
        create: function( event, ui ) {
            // Фоторама в табах
            if ( $('.js-catalog-fotorama').length ) {
                $('.js-catalog-fotorama').fotorama(oFotoramaConfig);
            }
        },

        //активация таба
        activate: function( event, ui ){

            //Карта в табе
            if ( ui.newPanel.attr('id').substr(5) == 'map' ){
                if ( window.initMap !== undefined )
                    initMap();
            }
        }
    });

    $(".js-selected-tab").click();
    if ($(".js-selected-tab").length>0)
        location.hash = "#js_tabs";

    var jqFotoramaContainer = $('.js-catalog-fotorama');

    // Фоторама вне таб
    if ( jqFotoramaContainer.length && !jqFotoramaContainer.closest('.js-tabs').length ){
        jqFotoramaContainer.fotorama(oFotoramaConfig);
    }


    // Фоторама основной галереи
    if ($('.js-catalog-detail-fotorama').length) {
        $('.js-catalog-detail-fotorama').fotorama(oFotoramaConfig);
    }
    //carousel owl
    if ($('.js-catalog_slider').length) {
        $('.js-catalog_slider').each(function() {
            var carouselOptions = $(this).data('carouselOptions');
            if (typeof(carouselOptions.responsive)!='undefined')
                carouselOptions.responsive = carouselOptions.responsive;

            $(this).addClass('owl-carousel owl-carousel-relatedtheme').owlCarousel(carouselOptions);
        });
    }

    //carousel owl
    if ($('.js-catalog_slider_recently').length) {
        $('.js-catalog_slider_recently').each(function() {
            var carouselOptions = $(this).data('carouselOptions');
            if (typeof(carouselOptions.responsive)!='undefined')
                carouselOptions.responsive = carouselOptions.responsive;
            $(this).addClass('owl-carousel owl-carousel-relatedtheme').owlCarousel(carouselOptions);
        });
    }


});
