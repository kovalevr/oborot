<?php

namespace skewer\build\Page\CatalogViewer;

use skewer\base\site\RobotsInterface;

class Robots implements RobotsInterface{

    public static function getRobotsDisallowPatterns(){
        return array(
            '/*view=',
            '/*sort=',
           // '/*?page=' //@todo нужно ли это убирать?
        );
    }

    public static function getRobotsAllowPatterns(){
        return false;
    }
} 