<?php

namespace skewer\build\Page\CatalogViewer;

use yii\web\AssetBundle;
use yii\web\View;

class AssetDetail extends AssetBundle {
    
    public $sourcePath = '@skewer/build/Page/CatalogViewer/web/';

    public $css = [
        'css/owl-carousel-catalogtheme.css',
        'css/owl-carousel-relatedtheme.css'
    ];

    public $js = [
        'js/detail.js'
    ];

    public $jsOptions = [
        'position'=>View::POS_HEAD
    ];

    public $depends = [
        'skewer\build\Page\CatalogViewer\Asset',
        'skewer\libs\fotorama\Asset',
        'skewer\libs\owlcarousel\Asset'
    ];


}
