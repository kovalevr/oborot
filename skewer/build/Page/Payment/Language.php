<?php

$aLanguage = array();

$aLanguage['ru']['tab_name'] = 'Системы оплат';
$aLanguage['ru']['success_text'] = 'Платеж совершен. Спасибо за ваш заказ!';
$aLanguage['ru']['fail_text'] = 'Заказ №{0, number}. Вы отказались от оплаты';

$aLanguage['en']['tab_name'] = 'Payment';
$aLanguage['en']['success_text'] = 'Payment is complete. Thank you for your order!';
$aLanguage['en']['fail_text'] = 'Order #{0, number}. You refused to pay';

return $aLanguage;