<?php

namespace skewer\build\Page\Main;

use skewer\base\section\Parameters;
use skewer\base\SysVar;
use skewer\libs\Compress\ChangeAssets;
use skewer\components\design\Design;
use yii\helpers\FileHelper;
use yii\web\AssetBundle;
use yii\web\View;

class Asset extends AssetBundle {

    public $sourcePath = '@skewer/build/Page/Main/web/';

    public $css = [
        'css/param.css',
        'css/main.css',
        'css/layout.css',
        //'css/superfast.css',
        'css/varcss.css',
        //'css/shcart.css',
        'css/typo.css'
    ];

    public $jsOptions = [
        'position'=>View::POS_HEAD
    ];

    public $depends = [
        'skewer\build\Page\Main\PrintAsset',
        'skewer\libs\jquery\Asset',
        'skewer\libs\datepicker\Asset',
        'skewer\libs\fancybox\Asset',
        'skewer\build\Page\Main\BaseAsset',
        'skewer\build\Page\Forms\Asset',
        'skewer\components\content_generator\Asset',
        'skewer\components\fonts\Asset'
    ];

    public static $cssIgnore = array(
//        'css/print.compile.css'
    );
    public static $jsPath = array();
    public static $jsHash = '';
    public static $cssPath= array();
    public static $cssHash = '';

    //название файлов игнора
    const CSSHASHIGNORE ='ignoreCSS';
    const PATHCOMPILEFILE ='/compile/';

    //Внешние файлы
    public static $aOutLinkJs = [];
    public static $aOutLinkCss = [];


    public function init()
    {

        if (Design::modeIsActive())
            array_unshift( $this->css, 'css/design.css' );

        parent::init();
        /** @noinspection PhpUnusedParameterInspection */
        $this->publishOptions['beforeCopy'] = function ($from, $to) {
            if (is_dir($from) && basename($from)=='images'){
                FileHelper::copyDirectory($from,WEBPATH.'images/');
            }
            return true;
        };

        //формирование одного js и одного css
        \Yii::$app->view->on(\yii\base\View::EVENT_END_PAGE, function() {

            //избавление от повторной обработки
            if (!self::$cssPath||!self::$jsPath) {

                $bCompress = Parameters::getValByName(\Yii::$app->sections->root(),'.',ChangeAssets::NAMEPARAM,true);
                if ($bCompress) {
                    //Получение правильных путей
                    foreach (\Yii::$app->view->jsFiles as $sAssetPath) {
                        $onlyPathJs = array_keys($sAssetPath);
                        foreach ($onlyPathJs as $jsPath) {
                            ChangeAssets::getOnlyPath($jsPath,'js');
                        }
                    }

                    $onlyPathCss = array_keys(\Yii::$app->view->cssFiles);
                    foreach ($onlyPathCss as $cssPath) {
                        ChangeAssets::getOnlyPath($cssPath,'css');
                    }
                    \Yii::$app->view->jsFiles = [];
                    \Yii::$app->view->cssFiles = [];
                    $directory = \Yii::$app->basePath.'/web/assets'.Asset::PATHCOMPILEFILE;
                    if (!file_exists($directory))
                        mkdir($directory);
                    $filenameJs = self::putData(self::$jsHash,'js',self::$jsPath);
                    //css
                    $filenameCss = self::putData(self::$cssHash,'css',self::$cssPath);

                    $newJsUrl = \Yii::$app->assetManager->baseUrl.Asset::PATHCOMPILEFILE.$filenameJs.'.js';
                    $newCssUrl = \Yii::$app->assetManager->baseUrl.Asset::PATHCOMPILEFILE.$filenameCss.'.css';

                    $newCssPrint = \Yii::$app->assetManager->baseUrl.'/'.SysVar::get('print_path');

                    \Yii::$app->view->registerJsFile($newJsUrl);
                    \Yii::$app->view->registerCssFile($newCssPrint,['media' => 'print']);
                    \Yii::$app->view->registerCssFile($newCssUrl);

                    if (self::$aOutLinkJs) {
                        array_unique(self::$aOutLinkJs);
                        foreach (self::$aOutLinkJs as $sLinkJs) {
                            \Yii::$app->view->registerJsFile($sLinkJs);
                        }
                    }
                    if (self::$aOutLinkCss) {
                        array_unique(self::$aOutLinkCss);
                        foreach(self::$aOutLinkCss as $sLinkCss)
                            \Yii::$app->view->registerCssFile($sLinkCss);
                    }
                }

                $this->unRegisterEmptyFiles();

            }

        });


    }

    /**
     * Удаление пустых файлов(css и js) ассетов из вывода.
     */
    protected function unRegisterEmptyFiles(){

        foreach (\Yii::$app->view->jsFiles as $iPosition => &$aJsFiles) {

            foreach ($aJsFiles as $sRelativePath => $sHtmlTag) {
                // Удалим времен.метку
                $sFileUrl = preg_replace('{\?v=(.)*}', '', $sRelativePath);
                $sFilePath = WEBPATH . $sFileUrl;

                if ( @filesize($sFilePath) === 0 )
                    unset(\Yii::$app->view->jsFiles[$iPosition][$sRelativePath]);

            }
        }

        foreach (\Yii::$app->view->cssFiles as $sRelativePath => $sHtmlTag) {

            // Удалим времен.метку
            $sFileUrl = preg_replace('{\?v=(.)*}', '', $sRelativePath);
            $sFilePath = WEBPATH . $sFileUrl;

            if ( @filesize($sFilePath) === 0 )
                unset(\Yii::$app->view->cssFiles[$sRelativePath]);

        }

    }

    public function putData($strForHash,$sExpansion,$aPath,$ignore = 0) {

        $filename = md5($strForHash);
        $newPath = \Yii::$app->basePath.'/web/assets'.Asset::PATHCOMPILEFILE.$filename.'.'.$sExpansion;
        if (!file_exists($newPath)) {
            $sContent = '';

            foreach ($aPath as $sPath) {
                if ($sExpansion =='css') {
                    $content = ChangeAssets::parseDataCss($sPath);
                    if (!stristr($sPath,'print.compile.css'))
//                        $sContent .= "\r\n/*$sPath*/\r\n".$content;
                        $sContent .= "\r\n/*".str_replace(\Yii::$app->basePath.'/web/','',$sPath)."*/\r\n".$content;
                    else {
                        $aPatternPath = "/assets\/([^\)]+)/i";
                        $aPathLong = array();
                        preg_match($aPatternPath,$sPath,$aPathLong);
                        SysVar::set('print_path',$aPathLong[1]);
                    }

                } else {
                    $content = ChangeAssets::jsMin($sPath);
//                        $sContent .= "\r\n/*$sPath*/\r\n".$content;
                    $sContent .= $content;
                }
            }
            file_put_contents($newPath, $sContent);
        }

        return $filename;
    }

}
