<?php

namespace skewer\build\Page\Main\templates\form\base;

use skewer\components\design\TplSwitchForm;

class Switcher extends TplSwitchForm {

    /**
     * Отдает имя название шаблона
     * @return string
     */
    public function getTitle() {
        return "Базовая";
    }

    /**
     * Отдает набор меток модулей, которые должны быть выведены в шапку
     */
    protected function getModulesList() {

    }

    /**
     * Задать набор настроек для модулей
     */
    public function setModuleSettings() {

    }

    /**
     * Задать настройки для типовых блоков
     */
    public function setBlocks() {

    }

    /**
     * Установить типовой контент
     */
    public function setContent() {
    }

}