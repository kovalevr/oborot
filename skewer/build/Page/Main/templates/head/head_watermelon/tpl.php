<?php

namespace skewer\build\Page\Main\templates\head\head_watermelon;

/**
 * @var int $mainId
 */
use skewer\base\section\Page;
use skewer\components\design\Design;

Asset::register($this);

?>

<div class="l-header">
    <div class="header__wrapper">

        <div class="b-pilot<?= Design::write(' js_dnd_wraper" sktag="page.head" sklayout="head')?>">
            <div class="b-logo <? if (Design::modeIsActive()): ?>g-ramaborder js-designDrag-<?= Design::get('page.head.logo','h_position')?><? endif ?>"<?= Design::write(' sktag="page.head.logo"') ?>>
                <a href="<?= '[' . $mainId . ']' ?>"><img alt="logo" src="<?= Design::getLogo() ?>"></a>
                <? if (Design::modeIsActive()): ?>
                    <div class="b-desbtn"><span>logo</span><ins></ins></div>
                <? endif ?>
            </div>

            <div class="pilot__1 hide-on-mobile <? if (Design::modeIsActive()): ?>g-ramaborder js-designDrag-<?= Design::get('page.head.pilot1','h_position')?>" sktag="page.head.pilot1" skeditor="headtext1/source<? endif ?>"><?= Page::getShowVal('headtext1', 'source') ?>
                <? if (Design::modeIsActive()): ?>
                    <div class="b-desbtn"><span>txt1</span><ins></ins></div>
                <? endif ?>
            </div>

            <div class="pilot__2 <? if (Design::modeIsActive()): ?>g-ramaborder js-designDrag-<?= Design::get('page.head.pilot2','h_position')?>" sktag="page.head.pilot2" skeditor="headtext2/source<? endif ?>"><?= Page::getShowVal('headtext2', 'source') ?>
                <? if (Design::modeIsActive()): ?>
                    <div class="b-desbtn"><span>txt2</span><ins></ins></div>
                <? endif ?>
            </div>

            <div class="pilot__3 <? if (Design::modeIsActive()): ?>g-ramaborder js-designDrag-<?= Design::get('page.head.pilot3','h_position')?>" sktag="page.head.pilot3" skeditor="headtext3/source<? endif ?>"><?= Page::getShowVal('headtext3', 'source') ?>
                <? if (Design::modeIsActive()): ?>
                    <div class="b-desbtn"><span>txt3</span><ins></ins></div>
                <? endif ?>
            </div>

            <div class="pilot__4 <? if (Design::modeIsActive()): ?>g-ramaborder js-designDrag-<?= Design::get('page.head.pilot4','h_position')?>" sktag="page.head.pilot4" skeditor="headtext4/source<? endif ?>"><?= Page::getShowVal('headtext4', 'source') ?>
                <? if (Design::modeIsActive()): ?>
                    <div class="b-desbtn"><span>txt4</span><ins></ins></div>
                <? endif ?>
            </div>

            <div class="pilot__5 <? if (Design::modeIsActive()): ?>g-ramaborder js-designDrag-<?= Design::get('page.head.pilot5','h_position')?>" sktag="page.head.pilot5" skeditor="headtext5/source<? endif ?>"><?= Page::getShowVal('headtext5', 'source') ?>
                <? if (Design::modeIsActive()): ?>
                    <div class="b-desbtn"><span>txt5</span><ins></ins></div>
                <? endif ?>
            </div>
        </div>

        <?= (isset($layout['head']))?$layout['head']:'' ?>

        <div class="header__left"></div>
        <div class="header__right"></div>
    </div>
</div>