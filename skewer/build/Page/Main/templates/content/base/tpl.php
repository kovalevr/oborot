<?php

use skewer\components\design\Design;

if ( !isset($hide_right_column) )
    $hide_right_column = 0;

/** @var bool $showRight Флаг отображения правой колонки */
$showRight = ($hide_right_column['value'] != 1 && isset($layout['right']) && $layout['right']);

?>

<? if (isset($layout['before_content'])):?>
    <div class="l-main l-main--before">
        <div class="main__wrapper">
        <?= (isset($layout['before_content']))?$layout['before_content']:'' ?>
        </div>
    </div>
<? endif ?>

<div class="l-main">
    <div class="main__wrapper">

        <div class="column <? if (!$showRight): ?>column_lc<? endif ?> <? if (!isset($layout['left'])): ?>column_cr<? endif ?>">
            <div class="column__center">
                <div class="column__center-indent"<?= Design::write(' sklayout="content"') ?>>
                    <?= (isset($layout['content']))?$layout['content']:'' ?>
                </div>
            </div>
            <? if (isset($layout['left'])):?>
                <div class="column__left"<?= Design::write(' sklayout="left"') ?>>
                    <div class="column__left-indent">
                        <?= $layout['left'] ?>
                    </div>
                </div>
            <? endif ?>

            <? if ($showRight): ?>
                <div class="column__right"<?= Design::write(' sklayout="right"') ?>>
                    <div class="column__right-indent"><?= $layout['right'] ?></div>
                </div>
            <? endif ?>
            <div class="column__center-bg">
                <div class="column__center-inside"></div>
            </div>
            <? if (isset($layout['left'])): ?>
                <div class="column__left-bg">
                    <div class="column__left-inside"></div>
                </div>
            <? endif ?>
            <? if ($showRight): ?>
                <div class="column__right-bg">
                    <div class="column__right-inside"></div>
                </div>
            <? endif ?>
        </div>
        <div class="main__left"></div>
        <div class="main__right"></div>
    </div>
</div>

<? if (isset($layout['after_content'])):?>
    <div class="l-main l-main--after">
        <div class="main__wrapper">
            <?= (isset($layout['after_content']))?$layout['after_content']:'' ?>
        </div>
    </div>
<? endif ?>