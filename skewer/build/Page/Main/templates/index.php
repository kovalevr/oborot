<?php

use skewer\components\design\Design;
use yii\helpers\ArrayHelper;
use skewer\helpers\Adaptive;

/**
 * @var $this \yii\web\View
 * @var string $SEOTitle
 * @var string $SEOKeywords
 * @var string $SEODescription
 * @var string $canonical_url
 * @var string $openGraph
 * @var array $page_class
 * @var int $sectionId
 * @var array $_params_
 * @var string $canonical_pagination
 * @var string $adaptive_parameters
 */

use skewer\components\seo;
use \yii\helpers\Html;
                                 
$pageBundle = \skewer\build\Page\Main\Asset::register($this);

if (Design::modeIsActive()){
    \skewer\build\Design\Frame\AssetDesign::register($this);
}

?>
<?php $this->beginPage() ?>
<!DOCTYPE html>
<html xmlns="http://www.w3.org/1999/xhtml" lang="<?= \Yii::$app->language ?>" class="g-no-js">
<head>
    <meta charset="utf-8" />

    <? if (Adaptive::modeIsActive()):?><meta name="viewport" content="width=device-width, initial-scale=1.0"><? endif; ?>

    <? // todo SEO переписать по Yii-ому #44105 ?>
    <title><?= seo\Api::prepareRawString($SEOTitle) ?></title>
    <? if ($openGraph):?> <?= $openGraph ?> <? endif; ?>
    <meta name="description" content="<?= seo\Api::prepareRawString($SEODescription) ?>" />
    <meta name="keywords" content="<?= seo\Api::prepareRawString($SEOKeywords) ?>" />
    <link rel="shortcut icon" href="<?= Design::getFavicon() ?>" type="<?= Design::getFavicon(true) ?>" />
    <? if (isSet($SEONonIndex) && $SEONonIndex): ?><meta name="robots" content="none"/><? endif ?>
    <? if (isSet($SEOAddMeta) && $SEOAddMeta): ?><?=Html::decode($SEOAddMeta) ?><? endif ?>

    <? if ($canonical_url): ?>
        <link rel="canonical" href="<?= $canonical_url ?>" />
    <? endif ?>
    <? if (isset($canonical_pagination)): ?>
        <link rel="canonical" href="<?=$canonical_pagination;?>">
    <? endif; ?>

    <? if (isset($aLangLinks)): ?>
        <? foreach ($aLangLinks as $links): ?>
            <meta rel="alternate" hreflang="<?=Html::decode($links['hreflang']) ?>" href="<?=Html::decode($links['href']) ?>" />
        <? endforeach ?>
    <? endif ?>

    <?php $this->head() ?>

    <? # дополнительный блок в заголовке ?>
    <? if (isSet($addHead['text']) && $addHead['text']): ?><?=Html::decode($addHead['text']) ?><? endif ?>

    <? # должен быть непосредственно перед закрывающим тегом </head> ?>
    <? if (isSet($gaCode['text']) && $gaCode['text']): ?><?=Html::decode($gaCode['text']) ?><? endif ?>

    <script>(function(e,t,n){var r=e.querySelectorAll("html")[0];r.className=r.className.replace(/(^|\s)g-no-js(\s|$)/,"$1g-js$2")})(document,window,0);</script>
</head>

    <?
    /** @var string $sBodyAttr доп атрибуты для тэга body */
    $sBodyAttr = '';

    if ( isset($page_class) and $page_class['value'] )
        $sBodyAttr .= sprintf(' class="%s"', $page_class['value']);

    if ( Design::modeIsActive() ) {
        $sBodyAttr .= ' sktag="page"';
        $sBodyAttr .= sprintf(' sectionid="%d"', $sectionId );
    }

    if (isset($specMenu_bodyFontSize))
        $sBodyAttr .= ' style="font-size: '.$specMenu_bodyFontSize.'px;"';

    ?>

    <body<?= $sBodyAttr ?>>
        <?php $this->beginBody() ?>
        <input type="hidden" id="current_language" value="<?= \Yii::$app->language ?>">
        <input type="hidden" id="current_section" value="<?= $sectionId ?>">
        <input type="hidden" id="js-adaptive-min-form-width" value="<?=$iMinWidthForForm?>">
        <div class="l-layout">
            <div class="layout__wrap">

                <?
                $headTpl = ArrayHelper::getValue($_params_, ['.layout', 'head_tpl', 0], 'base');
                echo $this->render('head/'.$headTpl.'/tpl', $_params_)
                ?>

                <?
                $contentTpl = ArrayHelper::getValue($_params_, ['.layout', 'content_tpl', 0], 'base');
                echo $this->render('content/'.$contentTpl.'/tpl', $_params_)
                ?>

            </div>
            <div class="layout__bgbox">
                <div class="layout__bgwrap">
                    <div class="layout__bgleft"></div>
                    <div class="layout__bgright"></div>
                </div>
            </div>
        </div>

        <?
        $footerTpl = ArrayHelper::getValue($_params_, ['.layout', 'footer_tpl', 0], 'base');
        echo $this->render('footer/'.$footerTpl.'/tpl', $_params_)
        ?>

        <?= (isset($countersCode['text']))?$countersCode['text']:'' ?>

        <div id="js-callbackForm" class="js-callbackForm b-callbackform-wrap" style="display: none;"></div>

        <?= $this->render('MicroData'); ?>
        <div class="js_adaptive_params<? if (!Adaptive::modeIsActive()): ?> g-nodisplay<? endif; ?>" data-adaptive_parameters='<?=$adaptive_parameters?>'></div>

        <?php
            // дополнительный css файл из диз режима
            // вызывается в самом конце, чтобы гарантированно был последним
            \skewer\build\Page\Main\AddCssAsset::register($this);
        ?>

        <?php $this->endBody() ?>

    </body>
</html>
<?php $this->endPage() ?>
