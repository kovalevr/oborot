<?php

use skewer\components\design\Design;
use skewer\base\section\Page;

/**
 * @var string $bottomMenu
 */

?>

<div class="l-footerbox"<?= Design::write(' sktag="page.footer"') ?>>
    <div class="footerbox__wrapper js_dnd_wraper">

        <div class="l-grid">
            <div class="grid__item1<? if (Design::modeIsActive()): ?> g-ramaborder js-designDrag-<?= Design::get('page.footer.grid1','h_position')?>" sktag="page.footer.grid1" skeditor="copyright/source<? endif ?>">
                <? if (Design::modeIsActive()): ?>
                    <div class="b-desbtn"><span>copyright</span><ins></ins></div>
                <? endif ?>
                <?= str_replace('[Year]', date('Y'), Page::getShowVal('copyright', 'source') ) ?>
            </div>
            <div class="grid__item2<? if (Design::modeIsActive()): ?> g-ramaborder js-designDrag-<?= Design::get('page.footer.grid2','h_position')?>" sktag="page.footer.grid2" skeditor="counters/source<? endif ?>">
                <? if (Design::modeIsActive()): ?>
                    <div class="b-desbtn"><span>counters</span><ins></ins></div>
                <? endif ?>
                <div class="b-counter">
                    <!--noindex--><?= Page::getShowVal('counters', 'source') ?><!--/noindex-->
                </div>
            </div>
            <div class="grid__item3<? if (Design::modeIsActive()): ?> g-ramaborder js-designDrag-<?= Design::get('page.footer.grid3','h_position')?>" sktag="page.footer.grid3" skeditor="contacts/source<? endif ?>">
                <? if (Design::modeIsActive()): ?>
                    <div class="b-desbtn"><span>contacts</span><ins></ins></div>
                <? endif ?>
                <?= Page::getShowVal('contacts', 'source') ?>
            </div>
            <div class="grid__item4<? if (Design::modeIsActive()): ?> g-ramaborder js-designDrag-<?= Design::get('page.footer.grid4','h_position')?>" sktag="page.footer.grid4" skeditor="footertext4/source<? endif ?>">
                <? if (Design::modeIsActive()): ?>
                    <div class="b-desbtn"><span>txt</span><ins></ins></div>
                <? endif ?>
                <?= Page::getShowVal('footertext4', 'source') ?>
            </div>
            <div class="grid__item5<? if (Design::modeIsActive()): ?> g-ramaborder js-designDrag-<?= Design::get('page.footer.grid5','h_position')?>" sktag="page.footer.grid5" skeditor="footertext5/source<? endif ?>">
                <? if (Design::modeIsActive()): ?>
                    <div class="b-desbtn"><span>txt2</span><ins></ins></div>
                <? endif ?>
                <?= Page::getShowVal('footertext5', 'source') ?>
            </div>
            <div class="grid__item6<? if (Design::modeIsActive()): ?> g-ramaborder js-designDrag-<?= Design::get('page.footer.grid6','h_position')?>" sktag="page.footer.grid6<? endif ?>">
                <? if (Design::modeIsActive()): ?>
                    <div class="b-desbtn"><span>service-menu</span><ins></ins></div>
                <? endif ?>
                <?= $bottomMenu ?>
            </div>
            <div class="grid__item7<? if (Design::modeIsActive()): ?> g-ramaborder js-designDrag-<?= Design::get('page.footer.grid7','h_position')?>" sktag="page.footer.grid7<? endif ?>">
                <? if (Design::modeIsActive()): ?>
                    <div class="b-desbtn"><span>copyright-dev</span><ins></ins></div>
                <? endif ?>
                <?= str_replace('[Year]', date('Y'),Page::getShowVal('copyright_dev', 'source')) ?>
            </div>
        </div>
        <div class="footerbox__left"></div>
        <div class="footerbox__right"></div>
    </div>
</div>

