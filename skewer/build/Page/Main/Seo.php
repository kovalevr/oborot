<?php
namespace skewer\build\Page\Main;

use skewer\base\section\Parameters;
use skewer\base\section\Tree;
use skewer\base\site\Site;
use skewer\build\Adm\Tree\Search;
use skewer\components\seo\Frequency;
use skewer\components\seo\SeoPrototype;
use skewer\helpers\Html;
use yii\helpers\ArrayHelper;
use skewer\build\Page\Text;

class Seo extends SeoPrototype{


    public static function getGroup(){
        return 'section';
    }

    /**
     * @inheritdoc
     */
    public static function getAlias(){
        return 'text';
    }

    /**
     * @inheritdoc
     */
    public function extractReplaceLabels( $aParams ){
        return [];
    }


    public function loadDataEntity(){
        $aData = Tree::getCachedSection($this->iSectionId);
        $aData['text'] = Text\Api::getTextContentFromZone($this->iSectionId);
        $this->aDataEntity = $aData;
    }

    /**
     * Пересчет значения приоритета(priority)
     * @return float|int
     */
    public function getPriority(){

        $fPriority = $fTemplatePriority = parent::getPriority();

        $oSection = Tree::getSection( $this->iSectionId );

        if ($oSection->parent == \Yii::$app->sections->templates())
            return $fTemplatePriority;

        $bIsLanding = ( \Yii::$app->sections->landingPageTpl() == Parameters::getTpl($this->iSectionId) );

        $sText = ArrayHelper::getValue($this->aDataEntity, 'text', '');

        if (!$bIsLanding && !Html::hasContent($sText))
            $fPriority -= 0.2;

        return $fPriority;
    }

    /**
     * Пересчет значения частоты(frequency)
     * @return string
     */
    public function calculateFrequency(){

        $sFrequency = $sTemplateFrequency = parent::calculateFrequency();

        $oSection = Tree::getSection( $this->iSectionId );

        if ($oSection->parent == \Yii::$app->sections->templates())
            return $sTemplateFrequency;

        $bIsLanding = ( \Yii::$app->sections->landingPageTpl() == Parameters::getTpl($this->iSectionId) );

        $sText = ArrayHelper::getValue($this->aDataEntity, 'text', '');

        if (!$bIsLanding && !Html::hasContent($sText))
            return Frequency::MONTHLY;

        return $sFrequency;
    }

    /**
     * @inheritdoc
     */
    protected function getSearchClassName(){
        return Search::className();
    }


    /**
     * Метод вернет данные раздела $this->iSectionId + seo данные
     * @param bool $iPosition - в методе данного класса не используется
     * @return array
     */
    public function getRecordWithinEntityByPosition($iPosition){

        $this->loadDataEntity();

        $aRow = [];
        $aRow['url']            = Tree::getSectionAliasPath($this->iSectionId);
        $aRow['h1']             = ($sAltTitle = Parameters::getValByName($this->iSectionId, 'title', 'altTitle'))? $sAltTitle : '';
        $aRow['staticContent']  = ($sTemp = Parameters::getShowValByName($this->iSectionId, 'staticContent', 'source'))? $sTemp : '';
        $aRow['staticContent2'] = ($sTemp = Parameters::getShowValByName($this->iSectionId, 'staticContent2', 'source'))? $sTemp : '';
        $aRow['seo']            = $this->parseSeoData( ['sectionId' => $this->iSectionId] );

        $aRow = array_merge($this->aDataEntity, $aRow);

        return $aRow;

    }

    /**
     * @inheritdoc
     */
    public function doExistRecord($sPath){

        $sTail = '';
        $iSectionId = Tree::getSectionByPath($sPath, $sTail);
        $sTail = trim($sTail, '/');

        return ($iSectionId && !$sTail )
            ? $iSectionId
            : false
        ;

    }


}