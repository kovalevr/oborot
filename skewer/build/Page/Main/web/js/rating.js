
/* Скрипт рейтингового голосования для компонента \skewer\components\rating\Rating */

$(function(){

    initRatings();
});

// Инициализация/обновление всех рейтингов на странице
function initRatings() {
    var iStarsCount = 5; // Число звёзд рейтинга

    $('.js-rating').each(function(){
        var oRating = $(this);
        var oStarDiv = oRating.children();
        var iStarHeight = oStarDiv.height();

        // Если данный рейтинг уже инициализирвоан, то пропустить
        if (oStarDiv.length > 1) return;

        // Установить бызовые свойства
        oStarDiv.css({
            float: 'left',
            'background-repeat' : 'repeat-y'
        });

        // Разрешить голосование
        if (oRating.data('allowrate')) {
            oStarDiv
                .css({cursor: 'pointer'})
                .mouseenter(function () {
                    $(this).parent().children().children().remove(); // Удалить не целые звёзды
                    $(this).prevAll().add(this).css('background-position', '0px ' + iStarHeight + 'px');
                    $(this).nextAll().css('background-position', '0px 0px');
                })
                .click(submitRating)
            ;
            oRating.mouseleave(setRating);
        }

        // Добавить недостающие звёзды
        for (i = 1; i < iStarsCount; i++)
            oRating.append(oStarDiv.clone(true));

        setRating(oRating);
    });
}

// Установить текущий рейтинг (that/this -- указатели на dom-элемент рейтинга)
function setRating(that) {
    that = (that.originalEvent == undefined) ? that : this;

    var iRating = $(that).data('rating');
    var oStarDiv = $(that).children().first();
    var iStarHeight = oStarDiv.height();
    var oStarHalf = oStarDiv.clone(false).css({padding: '0px', margin: '0px', 'background-position': '0px ' + iStarHeight + 'px'});

    var i = 0;
    $(that).children().each(function () {
        i++;
        if (i <= iRating) {

            // Добавить целую активнцю звезду
            $(this).css('background-position', '0px ' + iStarHeight + 'px');

        } else {

            // Добавить целую пассивную звезду
            $(this).css('background-position', '0px 0px');

            // Добавить не целую звезду
            if ( (i - 1 < iRating) && (iRating - i + 1 > 0) )  {

                iFrac = iRating - i + 1; // Дробная часть рейтинга
                iSizeAct = Math.round(iFrac*iStarHeight);
                iSizePas = iStarHeight - iSizeAct;

                // Очистить фон родителя
                $(this).css('background-position', oStarDiv.width() + 'px 0px');

                // Добавить не целую активную часть звёзды
                $(this).append(oStarHalf.clone(true).css('width', iSizeAct + 'px'));

                // Добавить не целую пассивную часть звёзды
                $(this).append(oStarHalf.clone(true).css({
                    'background-position': -iSizeAct + 'px 0px',
                    width: iSizePas + 'px'
                }));
            }
        }
    });
}

// Отправить рейтинг
function submitRating(e) {
    var iRating = $(this).prevAll().length + 1;
    var oRatingDiv = $(this).parent();

    $.post( '/ajax/ajax.php', {
            moduleName: oRatingDiv.data('modulename'),
            cmd: 'addRating',
            rating: parseInt(iRating),
            objId: parseInt(oRatingDiv.data('objectid')),
            rate_url: window.location.pathname + window.location.search
        },
        function (mResponse) {
            if (!mResponse) return false;
            var oResponse = $.parseJSON(mResponse);
            oResponse = $.parseJSON(oResponse.html);

            // Обновить результаты
            oRatingDivNew = $('.js-ajax_content', $(oResponse.Rating.html).wrapAll('<div>').parent());
            if (oRatingDivNew.length) {

                // Обновить рейтинги у всех копий объектов на странице
                var oRatingDivs = $('.js-rating[data-objectid = '+ oRatingDiv.data('objectid') +']');
                oRatingDivs.each(function(){
                    $(this).parents('.js-ajax_content').replaceWith(oRatingDivNew.prop('outerHTML'));
                });

                initRatings();

                if (oResponse.Answer)
                    $.fancybox.open(oResponse.Answer, {
                        autoSize: true,
                        autoCenter: true,
                        height: 20,
                        minHeight: 20,
                        afterLoad: function(){
                            // Закрыть через некоторое время
                            setTimeout("$.fancybox.close()", 3000);
                        }
                    });
            }
        }
    );
    return false;
}