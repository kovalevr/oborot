<?php

namespace skewer\build\Page\Auth;

use skewer\base\log\Logger;
use skewer\base\orm;
use skewer\build\Adm\Auth as AuthAdm;
use skewer\components\auth\Auth;
use skewer\components\auth\models\Users;

class RegForm extends orm\FormRecord {

    public $login = '';
    public $password = '';
    public $wpassword = '';

    public $accepts_the_offer = 0;
    public $captcha = '';

    public $cmd = 'register';

    public function rules() {
        return array(
            array( array('login','password','wpassword','captcha'), 'required', 'msg'=>\Yii::t('auth', 'err_empty_field' ) ),
            array( array('login'), 'email', 'msg'=>\Yii::t('auth', 'no_email_valid' ) ),
            array( array('password', 'wpassword'), 'minlength', 'length' => 6, 'msg'=>\Yii::t('auth', 'err_short_pass' ) ),
            array( array('wpassword'), 'compare', 'compareField'=>'password', 'msg'=>\Yii::t('auth', 'err_pass_not_mutch' ) ),
            array( array('login'), 'check', 'method'=>'checkLogin', 'msg'=>\Yii::t('auth', 'err_login_exsist' ) ),
            array( array('accepts_the_offer'), 'required', 'msg'=>\Yii::t('auth', 'err_empty_field' ) ),
            array( array('captcha'), 'captcha' ),
        );
    }

    public function getLabels() {
        return array(
            'login' => \Yii::t('auth', 'login_mail' ),
            'password' => \Yii::t('auth', 'password' ),
            'wpassword' => \Yii::t('auth', 'wpassword' ),
            'accepts_the_offer' => \Yii::t('forms','agreement_title'),
        );
    }

    public function getEditors() {
        return array(
            'password' => 'password',
            'wpassword' => 'password',
            'accepts_the_offer' => 'checkbox',
            'cmd' => 'hidden',
            'captcha' => 'captcha',
        );
    }


    /**
     * проверка на совпадение логина
     * @return bool
     */
    public function checkLogin() {

        $oItem =   Users::findOne( ["login"=>$this->login]);

        if ( $oItem ) {
            $this->setFieldError( 'login', \Yii::t('auth', 'alredy_taken') );
            return false;
        }

        return true;
    }


    /**
     * Сохранение нового пользователя
     * @return bool
     */
    public function saveUser() {

        if ( $this->password != $this->wpassword )
            return false;

        return Api::registerUser([
            'login' => $this->login,
            'pass'  => $this->password,
        ]);
    }

    /**
     * Отдает текст ошибки для поля
     * @param $sFieldName
     * @return bool
     */
    public function getErrorByFieldName($sFieldName)
    {
        $aErrors = $this->getErrorList();
        if (!isset($aErrors[$sFieldName])) return false;

        return $aErrors[$sFieldName];
    }

    /**
     * Отдает количество ошибок на форме
     * @return int
     */
    public function getCountErrors(){
        $aErrors = $this->getErrorList();
        return count($aErrors);
    }

    /**
     * Проверка значения полей на валидность
     * @return bool
     */
    public function isValid() {

        if($this->cptch_country) {
            Logger::dump('Попытка отправки спама через форму. В поле cptch_country введено: '.$this->cptch_country);
            return false;
        }

        $aRules = $this->rules();

        $aFields = $this->getFields();

        $bFlagHasErors = false;
        foreach ( $aFields as $sFieldName => $oField ) {

            if ( $sErrMsg = $oField->validate( $this, $aRules ) ) {
                $this->setFieldError( $sFieldName, $sErrMsg );
                $bFlagHasErors = true;
            }

        }
        return !$bFlagHasErors;
    }
} 