<?php

namespace skewer\build\Page\Auth;


use skewer\base\log\Logger;
use skewer\base\orm;

class RecoverForm extends orm\FormRecord {

    public $login = '';
    public $captcha = '';
    public $cmd = 'recover';

    public function rules() {
        return array(
            array( array('login','captcha'), 'required', 'msg'=>\Yii::t('auth', 'err_empty_field' ) ),
            array( array('captcha'), 'captcha' ),
        );
    }

    public function getLabels() {
        return array(
            'login' => \Yii::t('auth', 'your_email' ),
        );
    }

    public function getEditors() {
        return array(
            //'email' => 'password',
            'cmd' => 'hidden',
            'captcha' => 'captcha',
        );
    }


    /**
     * Отдает текст ошибки для поля
     * @param $sFieldName
     * @return bool
     */
    public function getErrorByFieldName($sFieldName)
    {
        $aErrors = $this->getErrorList();
        if (!isset($aErrors[$sFieldName])) return false;

        return $aErrors[$sFieldName];
    }

    /**
     * Отдает количество ошибок на форме
     * @return int
     */
    public function getCountErrors(){
        $aErrors = $this->getErrorList();
        return count($aErrors);
    }

    /**
     * Проверка значения полей на валидность
     * @return bool
     */
    public function isValid() {

        if($this->cptch_country) {
            Logger::dump('Попытка отправки спама через форму. В поле cptch_country введено: '.$this->cptch_country);
            return false;
        }

        $aRules = $this->rules();

        $aFields = $this->getFields();

        $bFlagHasErors = false;
        foreach ( $aFields as $sFieldName => $oField ) {

            if ( $sErrMsg = $oField->validate( $this, $aRules ) ) {
                $this->setFieldError( $sFieldName, $sErrMsg );
                $bFlagHasErors = true;
            }

        }
        return !$bFlagHasErors;
    }
} 