<?php

namespace skewer\build\Page\Auth;


use skewer\base\log\Logger;
use skewer\base\orm;

class NewPassForm extends orm\FormRecord {

    public $login = '';
    public $token = '';
    public $pass = '';
    public $wpass = '';
    public $cmd = 'saveNewPass';

    public function rules() {
        return array(
            array( array('pass'), 'required', 'msg'=>\Yii::t('auth', 'err_empty_field' ) ),
            array( array('wpass'), 'compare', 'compareField'=>'pass', 'msg'=>\Yii::t('auth', 'err_pass_not_mutch' ) ),
            array( array('pass', 'wpass'), 'minlength', 'length' => 6, 'msg'=>\Yii::t('auth', 'err_short_pass' ) ),
        );
    }

    public function getLabels() {
        return array(
            'pass' => \Yii::t('auth', 'new_pass' ),
            'wpass' => \Yii::t('auth', 'wpassword' ),
        );
    }

    public function getEditors() {
        return array(
            'pass' => 'password',
            'wpass' => 'password',
            'login' => 'hidden',
            'token' => 'hidden',
            'cmd' => 'hidden',
        );
    }

    /**
     * Отдает текст ошибки для поля
     * @param $sFieldName
     * @return bool
     */
    public function getErrorByFieldName($sFieldName)
    {
        $aErrors = $this->getErrorList();
        if (!isset($aErrors[$sFieldName])) return false;

        return $aErrors[$sFieldName];
    }

    /**
     * Отдает количество ошибок на форме
     * @return int
     */
    public function getCountErrors(){
        $aErrors = $this->getErrorList();
        return count($aErrors);
    }

    /**
     * Проверка значения полей на валидность
     * @return bool
     */
    public function isValid() {

        if($this->cptch_country) {
            Logger::dump('Попытка отправки спама через форму. В поле cptch_country введено: '.$this->cptch_country);
            return false;
        }

        $aRules = $this->rules();

        $aFields = $this->getFields();

        $bFlagHasErors = false;
        foreach ( $aFields as $sFieldName => $oField ) {

            if ( $sErrMsg = $oField->validate( $this, $aRules ) ) {
                $this->setFieldError( $sFieldName, $sErrMsg );
                $bFlagHasErors = true;
            }

        }
        return !$bFlagHasErors;
    }
} 