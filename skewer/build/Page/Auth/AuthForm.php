<?php

namespace skewer\build\Page\Auth;


use skewer\base\log\Logger;
use skewer\base\orm;
use skewer\components\auth\Auth;
use skewer\components\auth\CurrentUser;


class AuthForm extends orm\FormRecord {

    public $login = '';
    public $password = '';
    public $cmd = 'ShowAuthForm';

    public function rules() {
        return array(
            array( array('login','password'), 'required', 'msg'=>\Yii::t('auth', 'err_empty_field' ) )
        );
    }

    public function getLabels() {
        return array(
            'login' => \Yii::t('auth', 'login_mail' ),
            'password' => \Yii::t('auth', 'password' ),
        );
    }

    public function getEditors() {
        return array(
            'password' => 'password',
            'cmd' => 'hidden'
        );
    }

    public function auth() {

        $bRes = CurrentUser::login( $this->login, $this->password );

        if ( !$bRes )
            $this->addError( \Yii::t('auth', 'incorrect_login_or_pass' ) );

        return $bRes;
    }

    /**
     * Отдает текст ошибки для поля
     * @param $sFieldName
     * @return bool
     */
    public function getErrorByFieldName($sFieldName)
    {
        $aErrors = $this->getErrorList();
        if (!isset($aErrors[$sFieldName])) return false;

        return $aErrors[$sFieldName];
    }

    /**
     * Отдает количество ошибок на форме
     * @return int
     */
    public function getCountErrors(){
        $aErrors = $this->getErrorList();
        return count($aErrors);
    }

    /**
     * Проверка значения полей на валидность
     * @return bool
     */
    public function isValid() {

        if($this->cptch_country) {
            Logger::dump('Попытка отправки спама через форму. В поле cptch_country введено: '.$this->cptch_country);
            return false;
        }

        $aRules = $this->rules();

        $aFields = $this->getFields();

        $bFlagHasErors = false;
        foreach ( $aFields as $sFieldName => $oField ) {

            if ( $sErrMsg = $oField->validate( $this, $aRules ) ) {
                $this->setFieldError( $sFieldName, $sErrMsg );
                $bFlagHasErors = true;
            }

        }
        return !$bFlagHasErors;
    }
}