```css
```

tpl

```html
<div class="b-section">
    <div class="section__inner">
        <div class="section__content">
            <h2 class="section__header">{Заголовок секции}</h2>

            {:form}

        </div>
    </div>
</div>```

form

```html
        <div class="js-form b-form" sktag="modules.forms">
            <form id="form_{{formHash}}" method="post" action="{{form_section}}" enctype="multipart/form-data"{% if ajaxForm %} data-ajaxForm="1"{% endif %} {% if popup_result_page %}data-popup_result_page="1" {% endif %}>

            {% set aFieldList = oForm.getFields() %}
            {% for oField in aFieldList %}

            {% if oField.getType() == 'calendar' %}
            <div class="form__col-{{oField.getClassVal()}} form__label-{{oField.getVal('label_position')}} form__date">
                <img src="/images/calendar.jpg" class="js_ui-datepicker-trigger" alt="" />
                <label class="form__label">{{oField.getVal('param_title')}}{% if oField.getVal('param_required')==1 %} <ins class="form__mark">*</ins>{% endif %}</label>
                <div class="form__inputwrap">
                    <input type="text" class="js_init_datepicker" value="{{oField.getVal('param_default')}}" id="datepicker_{{oField.getVal('param_name')}}" name="{{oField.getVal('param_name')}}" cur_year="{{oField.getYear()}}" />
                </div>
            </div>
            {% elseif oField.getType() == 'checkbox' %}
            <div class="form__col-{{oField.getClassVal()}}{% if oField.getVal('label_position')=='left' %} form__label-left{% endif %} form__checkbox">
                <div class="form__inputwrap">
                    <input type="checkbox" name="{{oField.getVal('param_name')}}" id="{{oField.getVal('param_name')}}" value="{{oField.getVal('param_default')}}" {{oField.getVal('param_man_params')}} />
                </div>
                <label class="form__label" for="check_err">{{oField.getVal('param_title')}}{% if oField.getVal('param_required')==1 %} <ins class="form__mark">*</ins>{% endif %}</label>
                <p class="form__info">{{oField.getVal('param_description')}}</p>
            </div>
            {% elseif oField.getType() == 'delimiter' %}
            <div class="form__col-{{oField.getClassVal()}}">
                <p class="form__title">{{oField.getVal('param_default')}}</p>
            </div>
            {% elseif oField.getType() == 'file' %}
            <div class="form__col-{{oField.getClassVal()}} form__label-{{oField.getVal('label_position')}}">
                <label class="form__label">{{oField.getVal('param_title')}}{% if oField.getVal('param_required')==1 %} <ins class="form__mark">*</ins>{% endif %}</label>
                <div class="form__inputwrap">
                    <input type="file" name="{{oField.getVal('param_name')}}"  {{oField.getVal('param_man_params')}} />
                    
                </div>
                <p class="form__info">{{oField.getVal('param_description')}}</p>
            </div>
            {% elseif oField.getType() == 'input' %}
            <div class="form__col-{{oField.getClassVal()}} form__label-{{oField.getVal('label_position')}}">
                <label class="form__label">{{oField.getVal('param_title')}}{% if oField.getVal('param_required')==1 %}<ins class="form__mark">*</ins>{% endif %}</label>
                <div class="form__inputwrap">
                    <input type="text" name="{{oField.getVal('param_name')}}" id="{{oField.getVal('param_name')}}" value="{{oField.getVal('param_default')}}"  {{oField.getVal('param_man_params')}} />
                </div>
                <p class="form__info">{{oField.getVal('param_description')}}</p>
            </div>
            {% elseif oField.getType() == 'radio' %}
            {% set aItems = oField.getViewItems() %}
            <div class="form__col-{{oField.getClassVal()}} form__label-{{oField.getVal('label_position')}} form__radio">
                <label class="form__label" for="{{aItem.param_id}}">{{oField.getVal('param_title')}}{% if oField.getVal('param_required')==1 %} <ins class="form__mark">*</ins>{% endif %}</label>
                {% for aItem in aItems %}
                <div class="form__inputwrap">
                    <input type="radio" name="{{oField.getVal('param_name')}}" id="{{aItem.param_id}}"  {{aItem.checked}} value="{{aItem.value}}" {{oField.getVal('param_man_params')}} />
                    <label class="form__label" for="{{aItem.param_id}}">{{oField.getVal('param_title')}}</label>
                </div>
                {% endfor %}
                <p class="form__info">{{oField.getVal('param_description')}}</p>
            </div>
            {% elseif oField.getType() == 'select' %}
            <div class="form__col-{{oField.getClassVal()}} form__label-{{oField.getVal('label_position')}}">
                <label class="form__label">{{oField.getVal('param_title')}}{% if oField.getVal('param_required')==1 %} <ins class="form__mark">*</ins>{% endif %}</label>
                <div class="form__inputwrap">
                    <select name="{{oField.getVal('param_name')}}" id="{{oField.getVal('param_name')}}" {{oField.getVal('param_man_params')}}>
                    {% set aItems = oField.getViewItems() %}
                    <option selected disabled>{{oField.getVal('param_title')}}</option>
                    {% for aItem in aItems %}
                    <option value="{{aItem.value}}">{{aItem.title}}</option>
                    {% endfor %}
                    </select>
                </div>
                <p class="form__info">{{oField.getVal('param_description')}}</p>
            </div>
            {% elseif oField.getType() == 'textarea' %}
            <div class="form__col-{{oField.getClassVal()}} form__label-{{oField.getVal('label_position')}}">
                <label class="form__label">{{oField.getVal('param_title')}}{% if oField.getVal('param_required')==1 %} <ins class="form__mark">*</ins>{% endif %}</label>
                <div class="form__inputwrap">
                    <textarea name="{{oField.getVal('param_name')}}" id="{{oField.getVal('param_name')}}">{{oField.getVal('param_default')}}</textarea>
                </div>
                <p class="form__info">{{oField.getVal('param_description')}}</p>
            </div>
            {% endif %}
            {% endfor %}

            {% if oForm.getFormCaptcha() %}
            <div class="form__col-1 form__label-left form__captha">
                <label class="form__label form__captha_label">{{ Lang.get('Forms.captcha') }}</label>
                <div class="form__inputwrap">
                    <img alt="" src="/ajax/captcha.php?h={{formHash}}&v={{iRandVal}}" class="img_captcha" id="img_captcha" />
                    <input type="text" value="" name="captcha" id="captcha" maxlength="4" />
                </div>
            </div>
            {% endif %}
            {% set agreedData = oForm.getAgreedData() %}
            {% if agreedData %}
                <div class="form__col-1 form__checkbox" {% if agreedData.current_user %} style="display:none;" {% endif %}>
                    <div class="form__inputwrap">
                        <input type="checkbox" id="check_ok_{{formHash}}" name="agreed" value="1" {% if agreedData.current_user %}checked{% endif %}/>
                    </div>
                    <label class="form__label" for="check_ok_{{formHash}}"><a href="#agreed_text" class="js_anchor_agreed_readmore agreed_readmore">{{ agreedData.agreed_title }}</a>
             <ins class="form__mark">*</ins></label>
                    <div style="display: none;" class="js-agreed_text">
                <div id="agreed_text" class="agreed_text">{{ agreedData.agreed_text }}</div>
           </div>
                </div>
            {% endif %}
              
            <div class="form__col-1">
                <p class="form__info"><ins class="form__mark">*</ins> - {{ Lang.get('Forms.required') }}</p>
            </div>

            <div class="form__col-1 form__align_center">
                <button type="submit" class="b-btnbox">{{ Lang.get('Forms.send') }}</button>
            </div>

            <input type="hidden" name="cmd" value="send" /><input type="hidden" name="label" value="{{label}}" />
            <input type="hidden" name="form_id" id="form_id" value="{{oForm.getId()}}" />
            <input type="hidden" class="_rules" value='{{oForm.getRules()}}' />
            </form>
        </div>
        {% if reachGoals %}{{ reachGoals }}{% endif %}

```