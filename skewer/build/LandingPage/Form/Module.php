<?php

namespace skewer\build\LandingPage\Form;

use skewer\components\targets;
use skewer\base\section\Tree;
use skewer\base\site\Site;
use skewer\components\lp;
use skewer\components\forms;
use skewer\base\Twig;

/**
 * Модуль вывода формы на странице LandingPage
 * Class Module
 * @package skewer\build\Page\LandingPage\Form
 */
class Module extends lp\PagePrototype {

    //todo При ajaxForm=1 запрос на отправку формы пойдет в модуль Page/Forms и как результат поедет верстка
    public $AjaxForm = 0;

    public $FormId = 0;

    /**
     * Отдает полностью сформированный текст блока
     * @todo большую часть ф-ционала перенести в компонент, т. к. дублируется в модуле форм Page слоя
     * @return string
     */
    protected function getRenderedText() {

        $cmd = $this->getStr('cmd');

        /*Проверка на состояние "после отправки показать результирующую"*/
        if ( !is_null(\Yii::$app->session->get('forms_label') ) and
             !is_null(\Yii::$app->session->get('html_answer') ) and
             (\Yii::$app->session->get('forms_label') == $this->oContext->getLabel())
        ){
            $cmd = 'redirect';
        }

        // Формы со стандартными результирующими
        if ( $cmd == 'redirect' ){

            $sAnswer = \Yii::$app->session->get('html_answer');

            // Читаем flash сообщение; в след.запросе оно будет удалено
            \Yii::$app->session->getFlash('legal_redirect');
            \Yii::$app->session->set('forms_label', null);
            \Yii::$app->session->set('html_answer', null);
            return $sAnswer;

        } elseif ( $cmd == 'send' && ($this->getLabel() == $this->get('label')) ) {

            $ajaxForm = $this->getInt('ajaxForm', $this->AjaxForm);

            $iFormId    = $this->getInt( 'form_id' );
            $sLabel     = $this->get('label');

            try {

                /** @var forms\Entity $oForm */
                $oForm = forms\Table::build( $iFormId, $_POST );

                $formHash = $oForm->getHash( (int)\Yii::$app->request->post('section'), $sLabel );

                if ( !$oForm->validate( $formHash ) )
                    throw new \Exception( $oForm->getError() );

                switch( $oForm->getHandlerType() ){

                    case 'toMail':

                        $bRes = $oForm->send2Mail( 'letter.twig', $this->getModuleDir() . $this->getTplDirectory() );
                        break;

                    case 'toMethod':

                        $bRes = $oForm->send2Method();
                        break;

                    case 'toBase':

                        $oForm->send2Base( $this->sectionId() );
                        $bRes = $oForm->send2Mail( 'letter.twig', $this->getModuleDir() . $this->getTplDirectory() );
                        break;

                    default:
                        throw new \Exception('handler_not_found');# обработчик неизвестен - выходим
                }

            }
            catch( \Exception $e ) {
                $sErrorMessage = $e->getMessage();
                $sErrorAnswer  = forms\Api::buildErrorAnswer( $sErrorMessage );
                $sHtml = $this->renderContent($sErrorAnswer);
                return $sHtml;
            }

            $sSuccessAnswer = forms\Api::buildSuccessAnswer($oForm, $this->AjaxForm, $this->sectionId(), ['form_section' => $this->sectionId()]);
            $sHtml = $this->renderContent( $sSuccessAnswer );

            if ( !$ajaxForm ){

                // Базовая результирующая или сторонняя с пустым адресом результирующей -> редирект на /response
                if ( $oForm->hasBaseResultPage() || ($oForm->hasExternalResultPage() && !$oForm->getFormRedirect(true)) ){
                    \Yii::$app->session->set('html_answer', $sHtml);
                    \Yii::$app->session->set('forms_label', $this->get('label'));
                    \Yii::$app->session->setFlash( 'legal_redirect', 1 );
                    $sNewUrl = str_replace('response/', '', \Yii::$app->request->pathInfo);
                    \Yii::$app->response->redirect( Site::httpDomainSlash() . $sNewUrl . 'response/' )->send();
                    exit;

                    // Сторонняя результирующая -> редирект на другую страницу
                } elseif ( $oForm->hasExternalResultPage() ){
                    \Yii::$app->session->setFlash('form_source', $iFormId);
                    \Yii::$app->response->redirect($oForm->getFormRedirect( true ),'301')->send();
                    exit;
                }

            }

            return $sHtml;


        } else {
            return $this->actionShowForm();
        }

        return '';

    }

    /**
     * Отдает текст формы в виде html разметки
     * @return string
     * @throws \Exception
     */
    protected function actionShowForm() {

        if (!$this->FormId) return '';

        $aData = [];

        // Обращение к /response без отправки формы
        if ( !\Yii::$app->session->getFlash('legal_redirect') && Routing::patternUsed() ) {
            $sNewUrl = str_replace('response/', '', \Yii::$app->request->pathInfo);
            \Yii::$app->response->redirect( Site::httpDomainSlash() . $sNewUrl )->send();
            return '';
        } else {

            // Отлавливаем цели со сторонней формы
            if ( $iFormSourceId = \Yii::$app->session->getFlash('form_source', null, true) ){
                $oFormSource = forms\Table::build( $iFormSourceId );
                $sRichGoals = targets\Api::buildScriptTargetsInForm( $oFormSource );
                $aData['reachGoals'] = $sRichGoals;
            }

        }

        $oForm = forms\Table::build($this->FormId);
        \Yii::$app->router->setLastModifiedDate($oForm->getFormParam('last_modified_date'));
        
        $sLabel = $this->oContext->getLabel();

        $aData += [
            'oForm' => $oForm,
            'iRandVal' => rand(0, 1000),
            'form_section' => '#' . Tree::getSectionAlias( $this->sectionId() ),
            'formHash' => $oForm->getHash($this->sectionId(), $sLabel),
            'label' => $sLabel,
            'moduleWebPath' => $this->getModuleWebDir(),
            'section' => $this->sectionId(),
            'ajaxForm' => ($this->AjaxForm || $oForm->hasPopupResultPage()) ? 1 : 0 ,
            'popup_result_page' => $oForm->hasPopupResultPage()
        ];

        // шаблон формы
        $sFormText = lp\Tpl::getForSection( $this->sectionId(), 'form' );
        $sHtml = $this->renderContent( $sFormText, $aData );

        return $sHtml;
    }

    /**
     * Вернет html код блока lp "Формы"
     * @param string $sContent - контент, который заменяет метку {:form} в основном шаблоне
     * @param array $aData - данные для парсинга
     * @return string
     * */
    public function renderContent( $sContent, $aData = [] ){

        // текст шаблона
        $sTplText = lp\Tpl::getForSection( $this->sectionId() );

        // полный шаблон - вставляем форму в основной
        $aAllTpl = str_replace( '{:form}', $sContent, $sTplText );

        // парсим lp метки
        $sContent = lp\Parser::render(
            $aAllTpl,
            lp\Api::getDataRows( $this->sectionId() )
        );

        // парсим twig метки
        $sHtml = Twig::renderSource( $sContent, $aData );

        return $sHtml;
    }

}