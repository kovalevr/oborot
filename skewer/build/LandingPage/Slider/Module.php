<?php

namespace skewer\build\LandingPage\Slider;

use skewer\base\section\Parameters;
use skewer\build\Page\MainBanner\Asset;
use skewer\components\lp;
use skewer\build\Adm\Slider;
use skewer\base\Twig;
use yii\helpers\Json;

/**
 * Class Module
 * @package skewer\build\Page\LandingPage\Editor
 */
class Module extends lp\PagePrototype {

    /** Asset регистрируем вручную */
    public function autoInitAsset(){
        return false;
    }

    /**
     * Отдает полностью сформированный текст блока
     * @return string
     */
    protected function getRenderedText() {

        $sContent = lp\Api::getViewForSection( $this->sectionId() );

        $iBannerId = Parameters::getValByName( $this->sectionId(), 'object', 'currentBanner', false );

        $aBanner = Slider\Banner::find()
            ->where('id', $iBannerId)
            ->where('active', 1)
            ->asArray()->getOne();

        if ( !$aBanner )
            return '';

        \Yii::$app->router->setLastModifiedDate($aBanner['last_modified_date']);

        $aSlides = Slider\Slide::getSlides4Banner( $iBannerId );

        if (!$aSlides)
            return '';

        $aBannerTools = Slider\Banner::getAllTools( $aBanner );

        $aData = array(
            'banner' => $aSlides,
            'configArray' => $aBannerTools,
            'config' => json_encode($aBannerTools),
            'aDimensionsFirstImage' => Slider\Slide::getDimensionsFirstImage( $aSlides ),
            'aMinHeight', Json::htmlEncode($aBannerTools['height_limits'])
        );

        Asset::register( \Yii::$app->view );
        return Twig::renderSource( $sContent, $aData );
        //return psRendered;

    }

}