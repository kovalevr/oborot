$(document).ready(function() {

    $('.image_carousel').each(function(index, item){
        $('a.single_3', $(item)).each(function(indexLink, itemLink){
            $(itemLink).attr('data-fancybox', 'gallery_' + index );
        });
    });


    $('a.single_3').fancybox({
        slideShow: false,
        fullScreen: false,
        thumbs: false
    });


    $('.js-gal_slider').each(function(){

        $(this).carouFredSel({
            circular: false,
            auto 	: false,
            width : '100%',
            align : 'center',
            // Раскомментировать для использования пагинатора/булетов
            // pagination: $(this).siblings('.js_pagination'),
            prev	: {
                button	: $(this).siblings('.js-but_prev'),
                key		: "left",
                items: 1
            },
            next	: {
                button	: $(this).siblings('.js-but_next'),
                key		: "right",
                items: 1
            }

        });
    });

});
