<?php
/**
 * Created by PhpStorm.
 * User: Александр
 * Date: 22.01.2016
 * Time: 12:52
 */

namespace skewer\build\LandingPage\Gallery;

use yii\web\AssetBundle;
use yii\web\View;


class Asset extends AssetBundle {

    public $sourcePath = '@skewer/build/LandingPage/Gallery/web/';

    public $css = [
        'css/skin.css'
    ];

    public $js = [
        'js/init.js'
    ];

    public $jsOptions = [
        'position'=> View::POS_HEAD
    ];

    public $depends = [
        'skewer\libs\jquery\Asset',
        'skewer\libs\jquery\CarouFredSelAsset',
    ];


}