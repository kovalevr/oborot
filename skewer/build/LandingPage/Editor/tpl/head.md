```css
[block_class] .section__content {
    height: 200px;
    padding: 0;
}
[block_class] .b-absolute {
    position: relative;
    width: 100%;
}
[block_class] .b-absolute .absolute__item {
    position: absolute;
    padding: 0;
    z-index: 15;
}
[block_class] .b-absolute .absolute__1 {
    top: 20px;
    left: 10px;
    width: 200px;
}
[block_class] .b-absolute .absolute__2 {
    top: 20px;
    left: 120px;
    width: 300px;
}
[block_class] .b-absolute .absolute__3 {
    top: 10px;
    right: 20px;
    width: 200px;
}
```

```html
<div class="class b-section">
    <div class="section__inner">
        <div class="section__content">
            <div class="b-absolute">
                <div class="absolute__item absolute__1">
                    <div class="b-editor">
                        {wyswyg|Левый блок}
                    </div>
                </div>
                <div class="absolute__item absolute__2">
                    <div class="b-editor">
                       {wyswyg|Средний блок}
                    </div>
                </div>
                <div class="absolute__item absolute__3">
                    <div class="b-editor">
                        {wyswyg|Правый блок}
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
```