<?php

$aLanguage = array();

$aLanguage ['ru']['tab_name'] = 'Страница сайта';
$aLanguage ['ru']['back_to_top'] = 'Наверх';

$aLanguage ['en']['tab_name'] = 'Site page';
$aLanguage ['en']['back_to_top'] = 'Back to top';

return $aLanguage;