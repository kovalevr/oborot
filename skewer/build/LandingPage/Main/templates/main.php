<?php

use skewer\components\design\Design;

/**
 * @var $this \yii\web\View
 * @var string $SEOTitle
 * @var string $SEOKeywords
 * @var string $SEODescription
 * @var string $canonical_url
 *
 * @var string[] $blockList
 * @var string $countersCode
 * @var string $CopyRightModule
 */

use \yii\helpers\Html;

\skewer\libs\jquery\ScrollToAsset::register(\Yii::$app->view);

?>
<?php $this->beginPage() ?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.1//EN" "http://www.w3.org/TR/xhtml11/DTD/xhtml11.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="<?= \Yii::$app->language ?>" >
<head>
    <meta charset="utf-8"  content="text/html" />
    <title><?= Html::decode($SEOTitle) ?></title>
    <? if ($openGraph):?> <?= $openGraph ?> <? endif; ?>
    <meta name="description" content="<?= Html::decode($SEODescription) ?>" />
    <meta name="keywords" content="<?=Html::decode($SEOKeywords) ?>" />
    <link rel="shortcut icon" href="<?= Design::getFavicon() ?>" type="<?= Design::getFavicon(true) ?>" />

    <? if (isSet($SEONonIndex) && $SEONonIndex): ?><meta name="robots" content="none"/><? endif ?>
    <? if (isSet($SEOAddMeta) && $SEOAddMeta): ?><?=Html::decode($SEOAddMeta) ?><? endif ?>

    <? if (isSet($gaCode['text']) && $gaCode['text']): ?><?=Html::decode($gaCode['text']) ?><? endif ?>
    <?php $this->head() ?>
</head>
<body>
<?php $this->beginBody() ?>
<input type="hidden" id="current_language" value="<?= \Yii::$app->language ?>">
<input type="hidden" id="js-adaptive-min-form-width" value="<?=$iMinWidthForForm?>">
    <div class="l-container">
        <a class="js_page_top page_top"><?= \Yii::t('page', 'back_to_top') ?></a>
        <? if (isset($css)): ?>
            <style type="text/css">
                <?= $css ?>
            </style>
        <? endif ?>
        <? if (isset($menu) && is_array($menu)): ?>
            <div class="sevice-stopper"></div>
            <div class="b-sevice js-sevice">
                <ul class="level-1">
                    <? foreach($menu as $item): ?>
                        <?php if ($item['visible'] && !empty($item['link'])): ?>
                            <li class="item-1 <?=(isset($item['selected']))?"on-1":"" ?> <?= (isset($item['last']))?"last":"" ?>">
                                <a href="<?=$item['link'] ?>"><span data-link="<?= $item['alias']?>"><ins></ins><?= $item['title'] ?></span></a>
                            </li>
                        <?php else: ?>
                            <li class="item-1 <?=(isset($item['selected']))?"on-1":"" ?> <?= (isset($item['last']))?"last":"" ?>">
                                <span data-link="<?= $item['alias']?>"><ins></ins><?= $item['title'] ?></span>
                            </li>
                        <? endif ?>
                    <? endforeach ?>
                </ul>
            </div>
        <? endif ?>
        <?php foreach($blockList as $block): ?>
            <?= $block ?>
        <? endforeach ?>
    </div>

    <?= isset($CopyRightModule)? $CopyRightModule : '' ?>
    <?= ($countersCode && isset($countersCode['text']))? $countersCode['text'] : "" ?>

    <div id="js-callbackForm" style="display: none;"></div>
<?php $this->endBody() ?>
<script type="text/javascript">
    var menu_height = $(".b-sevice").height();
    $('.sevice-stopper').height(menu_height);

    $(function() {

        $(window).scroll(function() {
            if($(this).scrollTop() != 0) {
                $('.js_page_top').fadeIn();
            } else {
                $('.js_page_top').fadeOut();
            }
        });

        $('body').on('click', '.js_page_top', function() {
            $('body,html').animate({scrollTop:0},800);
        });

        $('.js-sevice span').click(function() {
            heightval = $(this).parents(".js-sevice").height();
            str = $(this).attr('data-link');
            if ( str )
                $.scrollTo('#'+str, 500, {offset: -heightval});
        });

    });
</script>
</body>
</html>
<?php $this->endPage() ?>
