<?php

namespace skewer\build\Tool\HttpsMode;

use skewer\build\Tool;
use skewer\base\SysVar;

use skewer\components\seo\Service as ServiceSeo;
use skewer\build\Tool\Domains;

/**
 * Модуль переключения сайта в работу на протоке https
 */
class Module extends Tool\LeftList\ModulePrototype {

    /**
     * Вызывается в случае отсутствия явного обработчика
     */
    protected function actionInit() {

        // включение редиректа на https
        $this->render(new Tool\HttpsMode\view\Index([
            "bActiveRedirect" => (bool)SysVar::get('Redirect.toHttps'),
            "bActiveHTTPS" => (bool)SysVar::get('enableHTTPS'),
        ]));
    }

    /** Отключить статистику */
    protected function actionStop() {

        SysVar::set('Redirect.toHttps', 0);
        $this->updateExportFiles();

        $this->actionInit();
    }

    /** Включить статистику */
    protected function actionStart() {

        SysVar::set('Redirect.toHttps', 1);
        $this->updateExportFiles();

        $this->actionInit();
    }

    /** отключение редиректа на https для sitemap`а */
    protected function actionStopHttps() {

        SysVar::set('enableHTTPS', 0);
        $this->updateExportFiles();

        $this->actionInit();
    }

    /** Редирект на https для sitemap`а */
    protected function actionStartHttps() {

        SysVar::set('enableHTTPS', 1);
        $this->updateExportFiles();

        $this->actionInit();
    }

    /** Обновить файлы, содержащие ссылки на ресурсы сайта */
    private function updateExportFiles() {
        $this->addMessage('<b>Необходимо обновить страницу, потом перестроить SITEMAP и robots.txt </b>', '', -1);
    }
}