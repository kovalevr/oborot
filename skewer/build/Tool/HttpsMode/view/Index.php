<?php

/**
 * Created by PhpStorm.
 * User: holod
 * Date: 18.01.2017
 * Time: 12:42
 */

namespace skewer\build\Tool\HttpsMode\view;

use skewer\components\ext\view\FormView;

class Index extends FormView
{
    public $bActiveRedirect;
    public $bActiveHTTPS;
    /**
     * Выполняет сборку интерфейса
     * @return void
     */
    function build() {
        $this->_form
            ->field('https_enable', '', 'show', [
                'hideLabel' => true,
                'fieldStyle' => 'font-size: 14px; float: none; text-align: center;',
            ])
        ;

        $this->_form
            ->field('https_status', '', 'show', [
                'hideLabel' => true,
                'fieldStyle' => 'font-size: 14px; float: none; text-align: center;',
            ])
            ->setValue([
                'https_enable' => ($this->bActiveHTTPS) ? \Yii::t('HttpsMode', 'https_on') : \Yii::t('HttpsMode', 'https_off'),
                'https_status' => ($this->bActiveRedirect) ? \Yii::t('HttpsMode', 'redirect_on') : \Yii::t('HttpsMode', 'redirect_off'),
            ]);

        // Кнопка включения протокола https
        if ($this->bActiveHTTPS)
            $this->_form->button('stopHttps', \Yii::t('HttpsMode', 'button_https_off'), 'icon-stop', 'init', ['disabled'=>$this->bActiveRedirect]);
        else
            $this->_form->button('startHttps', \Yii::t('HttpsMode', 'button_https_on'), 'icon-visible');

        $this->_form->buttonSeparator();

        // Кнопка включения редиректа на https
        if ($this->bActiveRedirect)
            $this->_form->button('stop', \Yii::t('HttpsMode', 'button_redirect_off'), 'icon-stop');
        else
            $this->_form->button('start', \Yii::t('HttpsMode', 'button_redirect_on'), 'icon-visible', 'init', ['disabled'=>!$this->bActiveHTTPS]);

    }
}