<?php

$aLanguage = array();

$aLanguage ['ru']['HttpsMode.Tool.tab_name'] = 'HTTPS соединение';

$aLanguage ['ru']['button_redirect_on'] = 'Включить редирект на HTTPS';
$aLanguage ['ru']['button_redirect_off'] = 'Выключить редирект на HTTPS';
$aLanguage ['ru']['redirect_on'] = 'Включен редирект на HTTPS';
$aLanguage ['ru']['redirect_off'] = 'Выключен редирект на HTTPS';

$aLanguage ['ru']['button_https_on'] = 'Включить протокол HTTPS';
$aLanguage ['ru']['button_https_off'] = 'Выключить протокол HTTPS';
$aLanguage ['ru']['https_on'] = 'Включен протокол HTTPS';
$aLanguage ['ru']['https_off'] = 'Выключен протокол HTTPS';

// ********************************************************************************************************************
// ***************************************************** ENGLISH ******************************************************
// ********************************************************************************************************************

$aLanguage ['en']['HttpsMode.Tool.tab_name'] = 'HTTPS mode';

$aLanguage ['en']['button_redirect_on']     = 'Disable redirects to HTTPS';
$aLanguage ['en']['button_redirect_off']    = 'Enable redirects to HTTPS';
$aLanguage ['en']['redirect_on']  = 'Redirects to HTTPS enabled';
$aLanguage ['en']['redirect_off'] = 'Redirects to HTTPS disabled';
$aLanguage ['en']['button_https_on']     = 'Enable protocol HTTPS';
$aLanguage ['en']['button_https_off']    = 'Disable protocol HTTPS';
$aLanguage ['en']['https_on']  = 'Enabled protocol HTTPS';
$aLanguage ['en']['https_off'] = 'Disabled protocol HTTPS';

return $aLanguage;