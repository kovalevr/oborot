<?php

namespace skewer\build\Tool\Messages;


use skewer\build\Tool;
use skewer\base\ui;
use skewer\components\ext;
use skewer\base\site_module\Parser;

class Module extends Tool\LeftList\ModulePrototype {

    /**
     * Перед запуском
     * @return bool|void
     */
    protected function preExecute() {}

    /**
     * Первичное состояние
     * @return void
     */
    protected function actionInit() {
        $this->actionList();
    }

    /**
     * Список сообщений
     * @return void
     */
    protected function actionList() {
        $this->render(new Tool\Messages\view\Index([
            "aMessages" => Api::getMessages()
        ]));
    }

    /**
     * Показывает сообщение
     * @return void
     */
    protected function actionMsgShow() {

        $oForm = new ext\UserFileView('Message');
        $this->addLibClass('MessageView');

        $oForm->addDockedItem(array(
            'text' => \Yii::t('messages', 'back'),
            'action' => 'list',
            'iconCls' => 'icon-cancel'
        ));

        $this->setCmd('load');

        $data = $this->get('data');
        $msgId = (isset($data['id'])) ? $data['id'] : false;
        $message = Api::getMessageById($msgId);
        $body = Parser::parseTwig('message.twig', $message, BUILDPATH.'Tool/Messages/templates/');

        if ($message['new']) {
            Api::setMessageRead($msgId);
        }

        $this->setData('message', $body);

        $this->fireJSEvent( 'reloadMessageBar' );

        $this->setInterface($oForm);
    }

    /**
     * Удаляет сообщение
     * @return void
     */
    protected function actionMsgDelete(){

        $data = $this->get('data');
        $msgId = (isset($data['id'])) ? $data['id'] : false;

        Api::delMessage($msgId);
        $this->actionList();
    }
}