<?php

namespace skewer\build\Tool\FormOrders;



use skewer\base\section\models\ParamsAr;
use skewer\components\forms\Entity;
use skewer\components\forms\Table;
use yii\helpers\ArrayHelper;

class Api {

    /**
     * Набор статусов для заказов из форм
     * @return array
     */
    public static function getStatusList() {
        return array(
            'new' => \Yii::t('forms', 'status_new' ),
            'ready' => \Yii::t('forms', 'status_ready' ),
        );
    }


    /**
     * Виджет на вывод статуса в списке
     * @param $oItem
     * @param $sField
     * @return mixed
     */
    public static function getWidget4Status( $oItem, $sField  ) {

        $aStatusList = self::getStatusList();

        $sVal = isSet( $oItem[$sField] ) ? $oItem[$sField] : $sField;

        return isSet( $aStatusList[$sVal] ) ? $aStatusList[$sVal] : $sVal;
    }

    /**
     * Отдает список форм в разделе///только те которые добавляют данные в БД
     * @param $iSectionId
     * @return array
     */
    public static function getFormsForSection($iSectionId){

        $aItems = Table::get4Section($iSectionId);

        $aTmpItems = [];

        foreach ($aItems as $key=>$item){
            if ($item->form_handler_type==Entity::TYPE_TOBASE)
                $aTmpItems[$key] = $item;
        }

        return $aTmpItems;
    }

    public static function getFormIdsForSection($aForms){

        $aOut = [];

        foreach ($aForms as $form)
            $aOut[] = $form->form_id;

        return $aOut;
    }

} 