<?php
/**
 * Created by PhpStorm.
 * User: holod
 * Date: 16.01.2017
 * Time: 14:37
 */

namespace skewer\build\Tool\FormOrders\view;

use skewer\components\ext\view\ListView;

class ListForm extends ListView
{
    public $aFieldList;
    public $aItems;
    public $bNotUseOneForm;

    /**
     * @inheritdoc
     */
    function build() {
        $this->_list->field('id', 'id', 'string');
        foreach ( $this->aFieldList as $oFieldRow ) $this->_list->field($oFieldRow->param_name, $oFieldRow->param_title, 'string', array('listColumns' => array('flex' => 1)));
        $this->_list
            ->field('__add_date', \Yii::t('forms', 'add_date'), 'string')
            ->field('__status', \Yii::t('forms', 'status'), 'string')
            ->widget( '__status', 'skewer\\build\\Tool\\FormOrders\\Api', 'getWidget4Status' )
            ->setValue( $this->aItems )
            ->buttonRowUpdate( 'Edit' )
            ->buttonRowDelete( 'Delete' )
            ->buttonAddNew('edit')
        ;
        if ($this->bNotUseOneForm) $this->_list->buttonBack('Init');
    }
}