<?php
/**
 * Created by PhpStorm.
 * User: holod
 * Date: 16.01.2017
 * Time: 16:31
 */

namespace skewer\build\Tool\FormOrders\view;

use skewer\components\ext\view\FormView;

class Error extends FormView
{
    public $sErrorMsg;
    /**
     * Выполняет сборку интерфейса
     * @return void
     */
    function build() {
        $this->_form->getForm()->setTitle("");
        $this->_form->headText($this->sErrorMsg);
    }
}