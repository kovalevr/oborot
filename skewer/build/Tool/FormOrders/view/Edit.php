<?php
/**
 * Created by PhpStorm.
 * User: holod
 * Date: 16.01.2017
 * Time: 15:23
 */

namespace skewer\build\Tool\FormOrders\view;

use skewer\components\ext\view\FormView;
use skewer\components\forms\FieldRow;

class Edit extends FormView
{
    /** @var FieldRow[] */
    public $aFieldList;
    public $aStatusList;
    public $aItems;
    public $bCanDelete;

    /**
     * Выполняет сборку интерфейса
     * @return void
     */
    function build() {
        $this->_form->field('id', 'id', 'hide');

        foreach ( $this->aFieldList as $oFieldRow ) {
            $sCurType = $oFieldRow->getType4ExtJS();
            switch ( $sCurType ) {
                case 'select':
                    $def = $oFieldRow->parseDefaultAsList();
                    $sFirst = ($def)?array_shift($def):'';
                    if ($sFirst&&(preg_match( '/^dictionary\(([^&]*)\)$/i', $sFirst, $aItemAll ))) {
                        $sNameDict = (isset($aItemAll[1]))?$aItemAll[1]:'';
                        $this->_form->fieldSelect( $oFieldRow->param_name, $oFieldRow->param_title, $oFieldRow->parseDictionary($sNameDict) );
                    } else 
                        $this->_form->fieldSelect( $oFieldRow->param_name, $oFieldRow->param_title, $oFieldRow->parseDefaultAsList() );
                    break;
                case 'radio':
                    $this->_form->fieldSelect( $oFieldRow->param_name, $oFieldRow->param_title, $oFieldRow->parseDefaultAsList() );
                    break;
                case 'multiselect':
                    $this->_form->fieldMultiSelect( $oFieldRow->param_name, $oFieldRow->param_title, $oFieldRow->parseDefaultAsList() );
                    break;
                default:
                    /*Если поле "Файл" Пропустим его*/
                    if ($oFieldRow->param_type=='6') continue;
                    $this->_form->field($oFieldRow->param_name, $oFieldRow->param_title, $sCurType);
            }
        }
        $this->_form
            ->field('__add_date', \Yii::t('forms', 'add_date'), 'string', array('disabled' => true))
            ->field('__section', \Yii::t('forms', 'section'), 'string', array('disabled' => true))
            ->fieldSelect( '__status', \Yii::t('forms', 'status'), $this->aStatusList )
            ->setValue($this->aItems)
            ->buttonSave('save')
            ->buttonCancel('list');

        if ($this->bCanDelete) {
            $this->_form
                ->buttonSeparator( '->' )
                ->buttonDelete();
        }
    }
}