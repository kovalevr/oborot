<?php

/**
 * Created by PhpStorm.
 * User: holod
 * Date: 16.01.2017
 * Time: 13:51
 */

namespace skewer\build\Tool\FormOrders\view;

use skewer\components\ext\view\ListView;

class ShowForms extends ListView
{
    public $aItems;
    /**
     * @inheritdoc
     */
    function build() {
        $this->_list
            ->field('form_title', \Yii::t('forms', 'form_title'), 'string', array('listColumns' => array('flex' => 1)))
            ->widget( 'form_handler_type', 'skewer\\build\\Component\\Forms\\Table', 'getTypeTitle' )
            ->setValue( $this->aItems )
            ->buttonRowUpdate( 'List' )
        ;
    }
}