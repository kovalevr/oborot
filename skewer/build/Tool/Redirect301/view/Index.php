<?php

/**
 * Created by PhpStorm.
 * User: holod
 * Date: 20.01.2017
 * Time: 13:08
 */

namespace skewer\build\Tool\Redirect301\view;

use skewer\components\ext\view\ListView;

class Index extends ListView
{
    public $aRedirects;

    /**
     * @inheritdoc
     */
    function build() {
        $this->_list
            ->field('old_url', \Yii::t('redirect301', 'old_url'), 'string', ['listColumns' => ['flex' => 1]])
            ->field('new_url', \Yii::t('redirect301', 'new_url'), 'string', ['listColumns' => ['flex' => 1]])
            ->buttonRowUpdate('editForm')
            ->buttonRowDelete()
            ->buttonAddNew('addForm')
            ->button('testAll',\Yii::t('redirect301','test'))
            ->button('importForm',\Yii::t('redirect301','import'))
            ->button('exportForm',\Yii::t('redirect301','export'))
            ->setValue($this->aRedirects)
            ->enableDragAndDrop('sortRedirects');
    }
}