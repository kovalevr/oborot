<?php
/**
 * Created by PhpStorm.
 * User: holod
 * Date: 02.03.2017
 * Time: 15:36
 */

namespace skewer\build\Tool\Import\view;

use skewer\build\Tool\Import\Module;

class HeadSettingsForm extends ImportForm
{
    public $sGroup;
    public $aCardList;
    public $aProviderTypeList;
    public $aTypeList;
    public $aCodingList;
    public $aData;
    public $id;

    /**
     * Выполняет сборку интерфейса
     * @return void
     */
    function build() {
        $this->_form
            ->field('id', 'ID', 'hide', ['groupTitle' => $this->sGroup])
            ->field('title', \Yii::t('import', 'field_title'), 'string', ['groupTitle' => $this->sGroup])
            ->fieldSelect( 'card', \Yii::t('import', 'field_card'), $this->aCardList, ['groupTitle' => $this->sGroup], false )

            ->fieldSelect( 'provider_type', \Yii::t('import', 'field_provider_type'), $this->aProviderTypeList, ['groupTitle' => $this->sGroup], false )

            ->fieldSelect( 'type', \Yii::t( 'import', 'field_type' ), $this->aTypeList, ['customField' => 'ImportType', 'groupTitle' => $this->sGroup], false )

            ->field('source_file', \Yii::t('import', 'field_source'), 'file', ['groupTitle' => $this->sGroup])
            ->field('source_str', \Yii::t('import', 'field_source'), 'string', ['groupTitle' => $this->sGroup])

            ->fieldSelect( 'coding', \Yii::t( 'import', 'field_coding' ), $this->aCodingList, ['groupTitle' => $this->sGroup], false )
            ->field('use_dict_cache', \Yii::t( 'import', 'use_dict_cache'), 'check', ['groupTitle' => $this->sGroup])
            ->field('use_goods_hash', \Yii::t( 'import', 'use_goods_hash'), 'check', ['groupTitle' => $this->sGroup])

            ->addLib('ImportType')
            ->setValue( $this->aData );

        if ($this->id) {
            $this->_form->buttonSave();
            $this->addStateButton('headSettings');
        }
        else {
            $this->_form
                ->buttonSave( 'save' )
                ->buttonCancel( 'list' );
        }

        /** @var Module $oModule */
        $oModule = $this->_module;

        if ( $oModule->isSys() ){
            $this->_form->buttonSeparator('->');
            $this->_form->button('clearQueue', \Yii::t('import', 'button_label_clearQueue'), 'icon-delete' );
        }

        $this->_form->setTrackChanges( false );
    }
}