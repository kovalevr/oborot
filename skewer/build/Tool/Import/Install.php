<?php

namespace skewer\build\Tool\Import;


use skewer\components\auth\models\GroupPolicy;
use skewer\components\catalog\Attr;
use skewer\components\catalog\Card;
use skewer\components\import\ar\ImportTemplate;
use skewer\components\import\ar\Log;
use skewer\base\section\Visible;
use skewer\base\section\Tree;

use skewer\base\site\Type;
use skewer\components\auth\Policy;
use skewer\components\config\InstallPrototype;
use skewer\base\section\models\TreeSection;
use skewer\components\import\Task;

/**
 * Class Install
 * @package skewer\build\Tool\Import
 */
class Install extends InstallPrototype {

    public function init() {
        return true;
    }// func

    public function install() {

        if (!Type::hasCatalogModule())
            $this->fail('Нельзя установить импорт на некаталожный сайт!');

        Log::rebuildTable();
        ImportTemplate::rebuildTable();

        $iSectionId = Tree::getSectionByAlias( 'Tool_Import', \Yii::$app->sections->library() );
        if (!$iSectionId){

            $oSection = new TreeSection();
            $oSection->parent = \Yii::$app->sections->library();
            $oSection->alias = 'Tool_Import';
            $oSection->title = 'Импорт';
            $oSection->visible = Visible::VISIBLE;
            $oSection->type = 1;
            $oSection->save();

            $iSectionId = $oSection->id;

        }

        if (!$iSectionId)
            $this->fail('Не удалось создать папку для модуля!');

        $aPolicyAdmin = GroupPolicy::find()->where(['alias' => 'admin'])->asArray()->all();

        if ($aPolicyAdmin)
            foreach ($aPolicyAdmin as $aPolicy)
                Policy::addModule( $aPolicy['id'], 'Import', 'Импорт и обновление товаров');
        // todo добавить перевод из словаря

        //Добавим поле в базовую карточку для записи хэшей
        $aFieldData = [
            'id'=>0,
            'title'=>Task::$sHashFieldName,
            'name'=>Task::$sHashFieldName,
            'group'=>0,
            'editor'=>'string',
            'widget'=>'',
            'validator'=>'',
            'def_value'=>'',
            'attr_'.Attr::SHOW_IN_LIST =>0,
            'attr_'.Attr::SHOW_IN_DETAIL =>0,
            'attr_'.Attr::SHOW_IN_SORTPANEL =>0,
            'attr_'.Attr::ACTIVE =>0,
            'attr_'.Attr::MEASURE =>0,
            'attr_'.Attr::SHOW_IN_PARAMS =>0,
            'attr_'.Attr::SHOW_IN_TAB =>0,
            'attr_'.Attr::SHOW_IN_FILTER =>0,
            'attr_'.Attr::IS_UNIQ =>0,
            'attr_'.Attr::SHOW_IN_TABLE =>0,
            'attr_'.Attr::SHOW_IN_CART =>0,
            'attr_'.Attr::SHOW_TITLE =>0,
            'attr_'.Attr::SHOW_IN_MAP =>0,
        ];

        $oField = Card::getField(0);
        $oCard  = Card::get(1);

        $oField->setData( $aFieldData );
        $oField->entity = 1;
        $oField->save();

        foreach ( $aFieldData as $sKey => $sValue )
            if ( !isSet( $oField->$sKey ) && strpos($sKey,'attr_') === 0 )
                $oField->setAttr( substr($sKey, 5), $sValue );

        $oCard->updCache();

    }// func

    public function uninstall() {

        $oCard   = Card::get(1);

        $oField = Card::getFieldByName( 1, Task::$sHashFieldName );

        $oField->delete();

        $oCard->updCache();

        return true;
    }// func

}//class