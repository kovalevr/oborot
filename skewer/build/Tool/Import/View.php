<?php

namespace skewer\build\Tool\Import;


use skewer\build\Catalog\CardEditor\Module as CardModule;
use skewer\components\import\Api;
use skewer\components\import\ar\ImportTemplateRow;
use skewer\components\import\Config;
use skewer\base\queue\Task;
use skewer\base\ui;
use yii\helpers\ArrayHelper;
use skewer\components\catalog;

/**
 * Класс для построений форм настройки
 */
class View{

    /**
     * Дополнение формы полями для нужного провайдера данных
     * @param ui\builder\FormBuilder $oForm
     * @param ImportTemplateRow $oTemplate
     */
    public static function getProviderForm( ui\builder\FormBuilder &$oForm, ImportTemplateRow $oTemplate ){

        $oConfig = new Config( $oTemplate );

        /** Если удаленный источник и нет локального файла - скачаем для примера */
        if ( $oConfig->getParam( 'type' ) == Api::Type_Url && (!$oConfig->getParam('file') || !file_exists( $oConfig->getParam('file'))) ){
            $oConfig->setParam( 'file', Api::uploadFile( $oConfig->getParam( 'source' )));
            //  и запомним для будущих примеров
            $oTemplate->settings = json_encode( $oConfig->getData() );
            $oTemplate->save();
        }

        $oProvider = Api::getProvider( $oConfig );

        $aParameters = $oProvider->getParameters();

        $aData = $oConfig->getData();

        /** Создаем поля для редактирования параметров */
        $oForm->field('id', 'ID', 'hide');
        foreach( $aParameters as $key=>$aVal ){

            switch ($aVal['viewtype']){
                case 'select':
                    $aList = [];
                    if (isset($aVal['method'])){
                        $aList = $oProvider->$aVal['method']();
                    }
                    $oForm->fieldSelect( $key, \Yii::t('import', $aVal['title'] ), $aList, [], false );
                    break;

                default:
                    $oForm->field($key, \Yii::t('import', $aVal['title']), $aVal['viewtype'], isset($aVal['params'])?$aVal['params']:[]);
                    break;
            }

            /** Значение полей по умолчанию */
            if (!isset($aData[$key]))
                $aData[$key] = $aVal['default'];
        }

        /** Пример данных из файла */
        $sExapmles = $oProvider->getExample();
        if ($sExapmles){
            $oForm->field('example', \Yii::t('import', 'example'), 'show', ['labelAlign' => 'top']);
            $aData['example'] = $sExapmles;
        }

        $oForm->setValue( $aData );

    }

    /**
     * Дополнение формы связями полей
     * @param ui\builder\FormBuilder $oForm
     * @param ImportTemplateRow $oTemplate
     */
    public static function getFieldsForm( ui\builder\FormBuilder &$oForm, ImportTemplateRow $oTemplate ){

        $oConfig = new Config( $oTemplate );

        /** Если удаленный источник и нет локального файла - скачаем для примера */
        if ( $oConfig->getParam( 'type' ) == Api::Type_Url && (!$oConfig->getParam('file') || !file_exists( $oConfig->getParam('file'))) ){
            $oConfig->setParam( 'file', Api::uploadFile( $oConfig->getParam( 'source' )));
            //  и запомним для будущих примеров
            $oTemplate->settings = json_encode( $oConfig->getData() );
            $oTemplate->save();
        }

        $oProvider = Api::getProvider( $oConfig );

        $aData = [];
        $aData['id'] = $oTemplate->id;
        $aFields = $oConfig->getParam('fields');
        if (is_array($aFields)){
            foreach($aFields as $aVal){

                if ($aVal['type']){
                    /** чтобы не путать незаданное поле с полем, которому соответствует 0 */
                    if ($aVal['importFields'] !== '')
                        $aData['field_'.$aVal['name']] = $aVal['importFields'];
                    
                    $aData['type_'.$aVal['name']] = $aVal['type'];
                }
            }
        }

        /** Создаем поля для редактирования параметров */
        $oForm->field('id', 'ID', 'hide');

        /** Пример данных из файла */
        $sExapmles = $oProvider->getExample();
        if ($sExapmles){
            $oForm->field('example', \Yii::t('import', 'example'), 'show', ['labelAlign' => 'top']);
            $aData['example'] = $sExapmles;
        }

        /** Соответствие полей */
        $aInfoRow = $oProvider->getInfoRow();
        if ($aInfoRow){
            $aFields = Api::getFieldList( $oTemplate->card );

            /** Убираем сео поля */
            $aSeoFields = catalog\Api::getSeoFields();
            $aFields = array_diff_key($aFields, ArrayHelper::index($aSeoFields, function($a){ return $a;}));

            unset($aFields['id']);

            /** Хак на раздел */
            $aFields = array_merge( ['section' => \Yii::t( 'import', 'section')], $aFields);

            $aHiddenFields = CardModule::getHiddenFields();

            $j=0;
            foreach( $aFields as $i=>$sValue ){

                if (array_search($sValue,$aHiddenFields)===false) {
                    $j++;
                    $sGroup = (string)($j);

                    $oForm->field('show_' . $i, \Yii::t("import", "field"), 'show', ['groupTitle' => $sGroup]);
                    $aData['show_' . $i] = $sValue;

                    $oForm->fieldMultiSelect('field_' . $i, \Yii::t("import", "field_type_card"), $aInfoRow, [], ['forceSelection' => false, 'groupTitle' => $sGroup]);

                    $oForm->fieldSelect("type_" . $i, \Yii::t("import", "field_type"), Api::getFieldTypeList(), ['forceSelection' => false, 'groupTitle' => $sGroup], false);
                }
            }
        }

        $oForm->setValue( $aData );

    }


    /**
     * Дополнение формы настройками полей
     * @param ui\builder\FormBuilder $oForm
     * @param ImportTemplateRow $oTemplate
     */
    public static function getFieldsSettingsForm( ui\builder\FormBuilder &$oForm, ImportTemplateRow $oTemplate ){

        $oConfig = new Config( $oTemplate );

        $aData = [];
        $aData['id'] = $oTemplate->id;

        /** Создаем поля для редактирования параметров */
        $oForm->field('id', 'ID', 'hide');

        $aCardFields = Api::getFieldList( $oTemplate->card );

        /** Убираем сео поля */
        $aSeoFields = catalog\Api::getSeoFields();
        $aCardFields = array_diff_key($aCardFields, ArrayHelper::index($aSeoFields, function($a){ return $a;}));

        /** Хак на раздел */
        $aCardFields = array_merge( ['section' => \Yii::t( 'import', 'section')], $aCardFields);

        $aFields = $oConfig->getParam('fields');
        if (is_array($aFields)){
            foreach($aFields as $aVal){
                if ( !isset($aVal['type']) or !$aVal['type'] or !isset($aCardFields[$aVal['name']]) )
                    continue;

                $sClassName = 'skewer\\components\\import\\field\\' . $aVal['type'];
                if (!class_exists($sClassName))
                    continue;

                /** @noinspection PhpUndefinedMethodInspection */
                $aParams = $sClassName::getParameters();
                if (!$aParams)
                    continue;

                $sGroup = $aCardFields[$aVal['name']];

                foreach($aParams as $k => $value){

                    if ($value['viewtype'] == 'select'){

                        $aList = [];
                        if (isset($value['method'])){
                            $aList = call_user_func([$sClassName, $value['method']]);

                            /** Если значения по умолчанию нет в списке, выделяем первый */
                            if (!isset($aList[$value['default']]) && count($aList))
                                $value['default'] = key($aList);
                        }

                        $oForm->fieldSelect( 'params_' . $aVal['name'] . ':' . $k, \Yii::t("import", $value['title']), $aList, ['groupTitle' => $sGroup], false );
                    }else{
                        $oForm->field('params_' . $aVal['name'] . ':' . $k, \Yii::t("import", $value['title']), $value['viewtype'], ['groupTitle' => $sGroup]);
                    }

                    $aData['params_' . $aVal['name'] . ':' . $k] = (isset($aVal['params'][$k])) ? $aVal['params'][$k] : $value['default'];
                }

            }
        }

        $oForm->setValue( $aData );

    }


    /**
     * Статус
     * @param $item
     * @return string
     */
    public static function getStatus( $item ){

        $aList = \skewer\base\queue\Api::getStatusList();

        // вместо заморожена, писать в работе
        if ($item['status'] == Task::stFrozen)
            $item['status'] = Task::stProcess;

        return (isset($aList[$item['status']])) ? $aList[$item['status']] : '';

    }

}