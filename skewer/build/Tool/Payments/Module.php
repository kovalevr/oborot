<?php

namespace skewer\build\Tool\Payments;

use skewer\base\ui;
use skewer\build\Tool;
use skewer\build\Adm\Order\ar\TypePayment;


/**
 * Class Module
 * @package skewer\build\Tool\Payments
 */
class Module extends Tool\LeftList\ModulePrototype  {

    /** @var string Заглушка для пароля */
    private $pwdFake = '--------';

    protected function actionInit(){
        $this->actionList();
    }

    protected function actionList() {
        $aItems = Api::getPaymentsList();

        $this->render(new Tool\Payments\view\Index([
            "aItems" => $aItems
        ]));
        return psComplete;
    }

    protected function actionEdit(){
        $sType = $this->getInDataVal( 'type' );

        $aPaymentsFields = Api::getFields4PaymentType( $sType );

        if (!$aPaymentsFields){
            $this->actionList();
        }

        $aFields = array( 'active', 'test_life' );

        foreach ($aPaymentsFields as $aField){
            $aFields[] = $aField[0];
        }

        $aItems = Api::getParams( $sType, $aFields );

        if (!$aItems){
            $aItems = array();
        }

        $aItems['type'] = $sType;

        $this->render(new Tool\Payments\view\Edit([
            "aPaymentsFields" => $aPaymentsFields,
            "sType" => $sType,
            "aItems" => $aItems
        ]));
    }

    /**
     * Сохранение активности из списка
     * @return int
     */
    protected function actionSaveActive(){
        $aType = $this->getInDataVal( 'type' );

        if (!$aType OR !Api::existType($aType)) {
            return $this->actionList();
        }

        $oParam = ar\Params::getParam( $aType, 'active');
        $oParam->value = $this->getInDataVal('value');
        $oParam->save();

        // Сбросить деактивируемый тип оплаты в таблице типов оплат заказов
        if ( !$oParam->value )
            TypePayment::update()
                ->set('payment', '')
                ->where('payment', $aType)
                ->get();

        return $this->actionList();
    }

    protected function actionSave() {
        $aType = $this->getInDataVal('type');

        if (!$aType OR !Api::existType($aType))
            return $this->actionList();

        /**
         * @todo нехорошо
         */
        $aData = $this->getInData();
        unset($aData['type']);

        if ($aData){
            foreach( $aData as $sKey => $sVal){
                $oParam = ar\Params::getParam( $aType, $sKey);
                $oParam->value = $sVal;
                $oParam->save();
            }
        }
        return $this->actionList();
    }
}