<?php

$aLanguage = array();

$aLanguage['ru']['tab_name'] = 'Системы оплат';
$aLanguage['ru']['title'] = 'Название';
$aLanguage['ru']['active'] = 'Активность';
$aLanguage['ru']['test_life'] = 'Тестовый режим';
$aLanguage['ru']['robokassa'] = 'Робокасса';
$aLanguage['ru']['payanyway'] = 'Payanyway';
$aLanguage['ru']['test'] = 'Демо-оплата';

$aLanguage['ru']['Robokassa_login_field'] = 'Идентификатор магазина';
$aLanguage['ru']['Robokassa_password1_field'] = 'Пароль 1';
$aLanguage['ru']['Robokassa_password2_field'] = 'Пароль 2';
$aLanguage['ru']['Payanyway_login_field'] = 'Логин';
$aLanguage['ru']['Payanyway_password_field'] = 'Пароль';
$aLanguage['ru']['Payanyway_invoice_field'] = 'Номер счета';
$aLanguage['ru']['Payanyway_code_field'] = 'Код проверки целостности данных';
$aLanguage['ru']['PayPal_clientId_field'] = 'Id клиента';
$aLanguage['ru']['PayPal_clientSecret_field'] = 'Секретный ключ';
$aLanguage['ru']['PayPal_currency_field'] = 'Трехзначный код валюты';
$aLanguage['ru']['PayPal_VAT'] = 'Ставка НДС';
$aLanguage['ru']['PayPal_VAT_0'] = 'НДС 0%';
$aLanguage['ru']['PayPal_VAT_10'] = 'НДС 10%';
$aLanguage['ru']['PayPal_VAT_18'] = 'НДС 18%';
$aLanguage['ru']['PayPal_VAT_NO'] = 'НДС не облагается';
$aLanguage['ru']['PayPal_VAT_10_1'] = ' НДС с рассч. ставкой 10%';
$aLanguage['ru']['PayPal_VAT_18_1'] = 'НДС с рассч. ставкой 18%';
$aLanguage['ru']['paypal'] = 'PayPal';

$aLanguage['ru']['yandexkassa'] = 'ЯндексКасса';
$aLanguage['ru']['YandexKassa_shopId_field'] = 'Идентификатор Контрагента';
$aLanguage['ru']['YandexKassa_scid_field'] = 'Номер витрины Контрагента';
$aLanguage['ru']['YandexKassa_shopPassword_field'] = 'Пароль магазина';

$aLanguage['ru']['taxSystem_field'] = 'Система налогообложения магазина';
$aLanguage['ru']['taxSystem_1'] = 'общая СН';
$aLanguage['ru']['taxSystem_2'] = 'упрощенная СН (доходы)';
$aLanguage['ru']['taxSystem_3'] = 'упрощенная СН (доходы минус расходы)';
$aLanguage['ru']['taxSystem_4'] = 'единый налог на временный доход';
$aLanguage['ru']['taxSystem_5'] = 'единый сельскохозяйственный налог';
$aLanguage['ru']['taxSystem_6'] = 'патентная СН';

$aLanguage['ru']['tax_field'] = 'Ставка НДС';
$aLanguage['ru']['tax_none'] = 'без НДС';
$aLanguage['ru']['tax_vat0'] = 'НДС по ставке 0%';
$aLanguage['ru']['tax_vat10'] = 'НДС чека по ставке 10%';
$aLanguage['ru']['tax_vat18'] = 'НДС чека по ставке 18%';
$aLanguage['ru']['tax_vat110'] = 'НДС чека по расчетной ставке 10/110';
$aLanguage['ru']['tax_vat118'] = 'НДС чека по расчетной ставке 18/118';

$aLanguage['ru']['YandexKassa_send_client'] = 'Отправка чека клиенту';
$aLanguage['ru']['YK_send_phone'] = 'на телефон';
$aLanguage['ru']['YK_send_email'] = 'на email';


$aLanguage['ru']['tinkoff'] = 'Тинькоф';
$aLanguage['ru']['Tinkoff_terminalKey_field'] = 'Ключ терминала';

$aLanguage['ru']['Tinkoff_password_field'] = 'Пароль';
$aLanguage['ru']['payment_canceled'] = 'Платеж отменен';
$aLanguage['ru']['paid'] = 'Оплачено';
$aLanguage['ru']['not_paid'] = 'Деньги задержаны, но не списаны';
$aLanguage['ru']['connection_error'] = 'Не смог соединиться с системой оплаты, обратитесь к администратору';



$aLanguage['ru']['button_label'] = 'Оплатить';

$aLanguage['en']['tab_name'] = 'Payment systems';
$aLanguage['en']['title'] = 'Name';
$aLanguage['en']['active'] = 'Active';
$aLanguage['en']['test_life'] = 'Test mode';
$aLanguage['en']['robokassa'] = 'Robokassa';
$aLanguage['en']['payanyway'] = 'Payanyway';
$aLanguage['en']['test'] = 'Demo-payment';

$aLanguage['en']['Robokassa_login_field'] = 'Scout shop';
$aLanguage['en']['Robokassa_password1_field'] = 'Password 1';
$aLanguage['en']['Robokassa_password2_field'] = 'Password 2';
$aLanguage['en']['Payanyway_login_field'] = 'Login';
$aLanguage['en']['Payanyway_password_field'] = 'Password';
$aLanguage['en']['Payanyway_invoice_field'] = 'Account number';
$aLanguage['en']['Payanyway_code_field'] = 'Code of data integrity verification';
$aLanguage['en']['PayPal_VAT'] = 'VAT rate';
$aLanguage['en']['PayPal_VAT_0'] = 'VAT 0%';
$aLanguage['en']['PayPal_VAT_10'] = 'VAT 10%';
$aLanguage['en']['PayPal_VAT_18'] = 'VAT 18%';
$aLanguage['en']['PayPal_VAT_NO'] = 'not VAT';
$aLanguage['en']['PayPal_VAT_10_1'] = 'VAT rate with 10%';
$aLanguage['en']['PayPal_VAT_18_1'] = 'VAT rate with 18%';
$aLanguage['en']['PayPal_clientId_field'] = 'Client Id';
$aLanguage['en']['PayPal_clientSecret_field'] = 'Client Secret';
$aLanguage['en']['PayPal_currency_field'] = '3-letter Currency Code';
$aLanguage['en']['paypal'] = 'PayPal';

$aLanguage['en']['yandexkassa'] = 'YandexKassa';
$aLanguage['en']['YandexKassa_shopId_field'] = 'Counterparty ID';
$aLanguage['en']['YandexKassa_scid_field'] = 'Room showcases Counterparty';
$aLanguage['en']['YandexKassa_shopPassword_field'] = 'Password';
$aLanguage['en']['YandexKassa_taxSystem_field'] = 'The taxation system of the store';

$aLanguage['en']['taxSystem_field'] = 'The taxation system of the store';
$aLanguage['en']['taxSystem_1'] = 'General taxation system';
$aLanguage['en']['taxSystem_2'] = 'Simplified taxation system (income)';
$aLanguage['en']['taxSystem_3'] = 'Simplified taxation system (income minus expenses)';
$aLanguage['en']['taxSystem_4'] = 'A single tax on imputed income';
$aLanguage['en']['taxSystem_5'] = 'Unified agricultural tax';
$aLanguage['en']['taxSystem_6'] = 'Patent taxation system';

$aLanguage['en']['tax_field'] = 'VAT rate';
$aLanguage['en']['tax_none'] = 'not VAT';
$aLanguage['en']['tax_vat0'] = 'VAT at a rate of 0%';
$aLanguage['en']['tax_vat10'] = 'VAT at a rate of 10%';
$aLanguage['en']['tax_vat18'] = 'VAT at a rate of 18%';
$aLanguage['en']['tax_vat110'] = 'VAT check at the estimated rate of 10/110';
$aLanguage['en']['tax_vat118'] = 'VAT check at the estimated rate of 18/118';

$aLanguage['en']['YandexKassa_send_client'] = 'Sending a check to a customer';
$aLanguage['en']['YK_send_phone'] = 'on phone';
$aLanguage['en']['YK_send_email'] = 'on email';

$aLanguage['en']['tinkoff'] = 'Tinkoff';
$aLanguage['en']['Tinkoff_terminalKey_field'] = 'Key machine';

$aLanguage['en']['Tinkoff_password_field'] = 'Password';

$aLanguage['en']['payment_canceled'] = 'Payment canceled';
$aLanguage['en']['paid'] = 'Paid';
$aLanguage['en']['not_paid'] = 'Money is detained, but not written off';
$aLanguage['en']['connection_error'] = 'Could not connect to the payment system, contact the administrator';


$aLanguage['en']['button_label'] = 'Proceed to checkout';

return $aLanguage;