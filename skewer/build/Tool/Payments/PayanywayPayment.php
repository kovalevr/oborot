<?php

namespace skewer\build\Tool\Payments;

use skewer\base\site_module\Request;
use skewer\build\Adm\Order\Api as ApiOrder;
use skewer\build\Adm\Order\ar\Goods;
use skewer\build\Adm\Order\ar\Order;

/**
 * Class PayanywayPayment
 * @package skewer\build\Tool\Payments
 */
class PayanywayPayment extends Payment{

    /** Адреса шлюзов */
    const PRODUCTION_WSDL= 'https://www.moneta.ru/services.wsdl';
    const DEVELOPMENT_WSDL = 'https://demo.moneta.ru/services.wsdl';

    /** url */
    const PRODUCTION_URL = 'https://www.payanyway.ru/assistant.htm';
    const DEVELOPMENT_URL = 'https://demo.moneta.ru/assistant.htm';

    /** @var string url шлюза */
    private $wsdl = '';

    /** @var string url страницы */
    private $url = '';

    /** @var string Логин */
    private $sLogin = '';

    /** @var string Пароль */
    private $sPassword = '';

    /** @var string Номер счета */
    private $iInvoice = '';

    /** @var string Код проверки целостности данных */
    private $sCode = '';

    /** @var int Код ставки НДС по умолчанию НДС 0% */
    private $vat = 1104;

    /** @var string Валюта */
    private $currency = 'RUB';

    /** @var bool Дебаг */
    private $bDebug = false;

    /** @var int */
    private $subscriber = '';

    /**
     * @var array Список полей для редактирования
     */
    protected static $aFields = array(
        array( 'login', 'Payanyway_login_field', 's', 'str'),
        array( 'password', 'Payanyway_password_field', 's', 'pass'),
        array( 'invoice', 'Payanyway_invoice_field', 'i', 'str'),
        array( 'code', 'Payanyway_code_field', 's', 'str'),
        array('vat','PayPal_VAT','s','select',array(1104 => 'PayPal_VAT_0',1103 =>'PayPal_VAT_10',1102 => 'PayPal_VAT_18',1105=>'PayPal_VAT_NO',1107 => 'PayPal_VAT_10_1',1106 => 'PayPal_VAT_18_1'))
    );

    /**
     * Конструктор
     */
    public function init(){
    }

    public function getType(){
        return 'payanyway';
    }

    /**
     * Устанавливает логин
     * @param string $sLogin
     */
    public function setLogin($sLogin){
        $this->sLogin = $sLogin;
    }

    /**
     * Возвращает модуль
     * @return string
     */
    public function getLogin(){
        return $this->sLogin;
    }

    /**
     * Установить номер счета
     * @param string $iInvoice
     */
    public function setInvoice($iInvoice)
    {
        $this->iInvoice = $iInvoice;
    }

    /**
     * Номер счета
     * @return string
     */
    public function getInvoice(){
        return $this->iInvoice;
    }

    /**
     * Установить пароль
     * @param string $sPassword
     */
    public function setPassword($sPassword){
        $this->sPassword = $sPassword;
    }

    /**
     * Возвращает пароль
     * @return string
     */
    public function getPassword(){
        return $this->sPassword;
    }

    /**
     * Код проверки
     * @param string $sCode
     */
    public function setCode($sCode){
        $this->sCode = $sCode;
    }

    /**
     * Код проверки
     * @return string
     */
    public function getCode(){
        return $this->sCode;
    }

    /**
     * Установить валюту
     * @param string $currency
     */
    public function setCurrency($currency){
        $this->currency = $currency;
    }

    /**
     * Валюта
     * @return string
     */
    public function getCurrency(){
        return $this->currency;
    }

    /**
     * Построение формы
     * @return string|void
     */
    public function getForm(){

        if (!$this->active){
            return '';
        }

        $this->setDescription(\Yii::t('order', 'order_description', [$this->getOrderId()]));

        $oForm = new Form();
        $oForm->setAction($this->url);

        $aItems = array();
        $aItems['MNT_ID'] = $this->iInvoice;

        $aItems['MNT_TRANSACTION_ID'] = $this->orderId;
        $aItems['MNT_CURRENCY_CODE'] = $this->currency;
        $aItems['MNT_AMOUNT'] = number_format($this->sum, 2, '.', '');
        $aItems['MNT_TEST_MODE'] = (int)$this->bDebug;
        $aItems['MNT_SUBSCRIBER_ID'] = $this->subscriber;
        $aItems['MNT_SIGNATURE'] = $this->createResultSignature();
        $aItems['MNT_DESCRIPTION'] = $this->description;
        $aItems['MNT_TYPE'] = $this->getType();
        $aItems['MNT_CUSTOM1'] = 1; //режим работы с сервисом
        $aItems['MNT_CUSTOM2'] = $this->getJson4Send(); //режим работы с сервисом

        $oForm->setFields( $aItems );

        return $this->parseForm( $oForm );
    }

    /**
     * генерация подписи
     * @param $sOperationId
     * @return string
     */
    private function createResultSignature( $sOperationId = '' ){
        return md5(
            $this->iInvoice . $this->orderId . $sOperationId. number_format($this->sum, 2, '.', '') .
            $this->currency . $this->subscriber . (int)$this->bDebug . $this->sCode
        );
    }

    /** @inheritdoc */
    public function checkResult(){

        $sum = Request::getStr( 'MNT_AMOUNT' );
        $orderId = (int)Request::getStr( 'MNT_TRANSACTION_ID' );
        $sOperationId = (int)Request::getStr( 'MNT_OPERATION_ID' );
        $signature = Request::getStr( 'MNT_SIGNATURE' );

        if ($sum != ApiOrder::getOrderSum( $orderId ))
            return false;

        $this->setOrderId( $orderId );
        $this->setSum( $sum );

        return (strtolower($signature) == $this->createResultSignature( $sOperationId ));

    }

    /**
     * Сообщение об успешной оплате
     * @return string
     */
    public function getSuccess(){
        return 'SUCCESS';
    }

    /**
     * Сообщение о неуспешной оплате
     * @return string
     */
    public function getFail(){
        return 'FAIL';
    }

    /**
     * Инициализация параметров
     * @param array $aParams
     */
    public function initParams( $aParams = array() ){
        if ($aParams){
            foreach($aParams as $sKey => $aParam){
                switch ( $sKey ){
                    case 'active':
                        $this->active = $aParam;
                        break;
                    case 'test_life':
                        $this->test_life = $aParam;
                        $this->url = ( $this->test_life )?static::DEVELOPMENT_URL:static::PRODUCTION_URL;
                        $this->wsdl = ( $this->test_life )?static::DEVELOPMENT_WSDL:static::PRODUCTION_WSDL;
                        $this->bDebug = ( $this->test_life )?true:false;
                        break;
                    case 'rk_login':
                        $this->sLogin = $aParam;
                        break;
                    case 'password':
                        $this->sPassword = $aParam;
                        break;
                    case 'invoice':
                        $this->iInvoice = $aParam;
                        break;
                    case 'code':
                        $this->sCode = $aParam;
                        break;
                    case 'vat':
                        $this->vat = $aParam;
                        break;

                }
            }
            $this->bInitParams = true;
        }
    }

    /**
     * Данные для чека
     * @return mixed
     */
    public function getJson4Send() {
        $aOrderResult = [];
        $aClient = Order::find()->where('id',$this->getOrderId())->asArray()->getOne();
        $aResult['customer'] = ($aClient&&isset($aClient['mail']))?$aClient['mail']:'';

        $aOrder = Goods::find()->where('id_order',$this->getOrderId())->asArray()->getAll();
        foreach ($aOrder as $aValue) {
            $aOrderCheck['n'] = $aValue['title'];
            $aOrderCheck['p'] = $aValue['price'];
            $aOrderCheck['q'] = $aValue['count'];
            $aOrderCheck['t'] = $this->vat;
            $aOrderResult[] = $aOrderCheck;
        }
        $aResult['items'] = $aOrderResult;

        return json_encode($aResult);
    }
}