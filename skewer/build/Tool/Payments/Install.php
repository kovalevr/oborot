<?php

namespace skewer\build\Tool\Payments;


use skewer\build\Adm\Order\ar\Order;
use skewer\components\config\InstallPrototype;

class Install extends InstallPrototype {

    public function init() {
        return true;
    }

    public function install() {

        ar\Params::rebuildTable();
        ar\PayPalPayments::rebuildTable();
        ar\Params::getParam( 'robokassa', 'active', true);
        ar\Params::getParam( 'payanyway', 'active', true);
        ar\Params::getParam( 'paypal', 'active', true);
        ar\Params::getParam( 'yandexkassa', 'active', true);

        Order::rebuildTable();

        return true;
    }

    public function uninstall() {

        Order::rebuildTable();

        return true;
    }
}
