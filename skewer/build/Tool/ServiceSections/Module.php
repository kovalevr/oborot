<?php

namespace skewer\build\Tool\ServiceSections;


use skewer\components\i18n\Languages;
use skewer\components\i18n\models\ServiceSections;
use skewer\base\ui\StateBuilder;
use skewer\build\Tool;
use yii\helpers\ArrayHelper;

/**
 * Модуль для редактирования системных разделов
 * Class Module
 * @package skewer\build\Tool\ServiceSections
 */
class Module extends Tool\LeftList\ModulePrototype {

    protected $lang_filter = false;

    protected function preExecute() {
        $this->lang_filter = $this->get('lang_filter', false);
    }


    public function actionInit(){

        $this->actionList();

    }

    public function actionList(){

        $aLanguages = Languages::getAllActive();

        $oQuery = ServiceSections::find()
            ->asArray()
            ->orderBy(['language' => SORT_ASC]);

        if ($this->lang_filter)
            $oQuery->where(['language' => $this->lang_filter]);

        $aServiceSections = $oQuery->all();

        $this->render(new Tool\ServiceSections\view\Index([
            "bManyLanguages" => (count($aLanguages) > 1),
            "aLanguages" => ArrayHelper::map($aLanguages, 'name', 'title'),
            "sLangFilter" => $this->lang_filter,
            "aServiceSections" => $aServiceSections
        ]));

    }

    public function actionSave(){

        $aData = $this->getInData();

        if (isset($aData['id'])){
            /** @var ServiceSections $oSection */
            $oSection = ServiceSections::findOne(['id' => $aData['id']]);

            if (!is_null($oSection)){
                $oSection->value = (isset($aData['value']))?(int)$aData['value']:0;
                $oSection->save();
            }
        }

        $this->actionList();

    }

}