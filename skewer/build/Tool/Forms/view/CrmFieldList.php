<?php
/**
 * Created by PhpStorm.
 * User: holod
 * Date: 17.01.2017
 * Time: 12:21
 */

namespace skewer\build\Tool\Forms\view;

use skewer\components\ext\view\ListView;

class CrmFieldList extends ListView
{
    public $aItems;

    /**
     * @inheritdoc
     */
    function build()
    {
        $this->_list
            ->fieldHide('id')
            ->fieldString('crm_field_title',\Yii::t('forms','crm_field_title'),array('listColumns' => array('flex' => 1)))
            ->fieldString('form_field_title', \Yii::t('forms','form_field'),array('listColumns' => array('flex' => 1)))
            ->setValue( $this->aItems )
            ->buttonRowUpdate( 'editCrmLink' )
            ->buttonBack('CRMIntegration')
        ;
    }
}