<?php
/**
 * Created by PhpStorm.
 * User: holod
 * Date: 17.01.2017
 * Time: 14:33
 */

namespace skewer\build\Tool\Forms\view;

use skewer\components\ext\view\FormView;

class Answer extends FormView
{
    public $sInfoText;
    public $sAnswerReview;
    public $aValues;
    /**
     * Выполняет сборку интерфейса
     * @return void
     */
    function build() {

        if ($this->sAnswerReview)
            $this->_form
                ->fieldWithValue( \Yii::t('forms', 'label_answer_review'), '', 'show', $this->sAnswerReview );

        $this->_form
            ->field('form_answer', \Yii::t('forms', 'form_answer'), 'check')
            ->field('answer_title', \Yii::t('forms', 'answer_title'), 'str')
            ->field('answer_body', \Yii::t('forms', 'answer_body'), 'wyswyg')
            ->fieldWithValue( 'info', \Yii::t('forms', 'replace_label'), 'show', $this->sInfoText )
            ->setValue( $this->aValues )
            ->buttonSave('answerSave')
            ->buttonBack('FieldList');
    }
}