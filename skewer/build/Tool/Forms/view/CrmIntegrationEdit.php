<?php
/**
 * Created by PhpStorm.
 * User: holod
 * Date: 17.01.2017
 * Time: 11:36
 */

namespace skewer\build\Tool\Forms\view;

use skewer\build\Tool\Crm\Api;
use skewer\components\ext\view\FormView;

class CrmIntegrationEdit extends FormView
{

    public $oFormRow;

    /**
     * Выполняет сборку интерфейса
     * @return void
     */
    function build() {
        $this->_form
            ->field('form_id', 'id', 'hide')
            ->field('form_send_crm', \Yii::t('forms', 'form_send_crm'), 'check')
            ->setValue($this->oFormRow)
            ->buttonSave()
            ->buttonBack('FieldList')
            ->button('CrmLinkList',\Yii::t('forms', 'crm_field_list'),'icon-page');
    }
}