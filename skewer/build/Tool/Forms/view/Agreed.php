<?php
/**
 * Created by PhpStorm.
 * User: holod
 * Date: 17.01.2017
 * Time: 14:55
 */

namespace skewer\build\Tool\Forms\view;

use skewer\components\ext\view\FormView;

class Agreed extends FormView
{
    public $aValues;
    public $sInfoText;
    /**
     * Выполняет сборку интерфейса
     * @return void
     */
    function build() {
        $this->_form
            ->field('form_agreed', \Yii::t('forms', 'form_agreed'), 'check')
            ->field('agreed_title', \Yii::t('forms', 'field_agreed_title'), 'str')
            ->field('agreed_text', \Yii::t('forms', 'field_agreed_text'), 'html')
            ->fieldWithValue( 'info', \Yii::t('forms', 'replace_label'), 'show', $this->sInfoText )
            ->setValue( $this->aValues )
            ->buttonSave('agreedSave')
            ->buttonCancel('FieldList')
        ;
    }
}