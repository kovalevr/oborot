<?php

namespace skewer\build\Tool\Forms\view;

use skewer\components\ext\view\FormView;
use skewer\components\forms\Entity;
use yii\helpers\ArrayHelper;

class SettingsResultPage extends FormView
{
    public $aItems;

    /**
     * @inheritdoc
     */
    function build()
    {
        $this->_form
            ->fieldSelect('form_type_result_page', \Yii::t('forms', 'type_result_page'), self::getTypesResultPage(), ['onUpdateAction' => 'updateSettingResultPageForm'], false);

        $sTypeResultPage = (int) ArrayHelper::getValue($this->aItems, 'form_type_result_page');

        if ( $sTypeResultPage ){

            if ( $sTypeResultPage === Entity::RESULT_PAGE_EXTERNAL )
                $this->_form->fieldString('form_redirect', \Yii::t('forms', 'form_redirect'), ['subtext' => \Yii::t('forms', 'form_redirect_default')] );

            if ( ($sTypeResultPage !== Entity::RESULT_PAGE_EXTERNAL) )
                $this->_form->fieldWysiwyg('form_succ_answer', \Yii::t('forms', 'form_succ_answer'), 300, '');

        }

        $this->_form
            ->setValue($this->aItems)

            ->buttonSave('saveSettingResultPage')
            ->buttonBack('FieldList');
        ;
    }

    /**
     * Вернет названия типов результирующих страниц
     * @return array
     */
    public static function getTypesResultPage(){
        return [
            Entity::RESULT_PAGE_BASE     => \Yii::t('forms', 'type_result_page_base'),
            Entity::RESULT_PAGE_EXTERNAL => \Yii::t('forms', 'type_result_page_external'),
            Entity::RESULT_PAGE_POPUP    => \Yii::t('forms', 'type_result_page_popup'),
        ];
    }


}