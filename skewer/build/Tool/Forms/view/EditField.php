<?php
/**
 * Created by PhpStorm.
 * User: holod
 * Date: 17.01.2017
 * Time: 13:42
 */

namespace skewer\build\Tool\Forms\view;

use skewer\components\ext\view\FormView;

class EditField extends FormView
{
    public $aFieldTypes;
    public $iUploadMaxSize;
    public $aValidatorList;
    public $bIsSystemMode;
    public $aLabelPositionList;
    public $aWidthFactorList;
    public $oFieldRow;
    public $aEntityList;

    /**
     * Выполняет сборку интерфейса
     * @return void
     */
    function build() {
        $this->_form
            ->field('param_id', 'id', 'hide')
            ->field('param_title', \Yii::t('forms', 'param_title'), 'string')
            ->field('param_name', \Yii::t('forms', 'param_name'), 'string')
            ->field('param_description', \Yii::t('forms', 'param_description'), 'text', ['labelAlign' => 'left'])
            ->fieldSelect( 'param_type', \Yii::t('forms', 'param_type'), $this->aFieldTypes, ['onUpdateAction'=>'updFields'], false )
            ->fieldSelect( 'view_type', \Yii::t('forms', 'field_f_link_id'),$this->aEntityList, [], false )
            ->field('param_required', \Yii::t('forms', 'param_required'), 'check')
            ->field('param_default', \Yii::t('forms', 'param_default'), 'text', array(
                'labelAlign' => 'left',
                'subtext' => \Yii::t('forms', 'defaultDesc')))
            ->field('param_maxlength', \Yii::t('forms', 'param_maxlength'), 'string', [
                'labelAlign' => 'left',
                'subtext' => sprintf(\Yii::t('forms', 'maxlength_desc', [$this->iUploadMaxSize]))
            ])
            ->fieldSelect( 'param_validation_type', \Yii::t('forms', 'param_validation_type'), $this->aValidatorList, [], false );

        if ($this->bIsSystemMode) {
            $this->_form
                ->fieldSelect( 'label_position', \Yii::t('forms', 'label_position'),
                    $this->aLabelPositionList, ['margin'=>'15 5 0 0'], false )
                ->field('new_line', \Yii::t('forms', 'new_line'), 'check')
                //->field('group', \Yii::t('forms', 'group'), 'check') //это поле появится в след версии
                ->fieldSelect( 'width_factor', \Yii::t('forms', 'width_factor'), $this->aWidthFactorList, [], false )
                ->field('param_man_params', \Yii::t('forms', 'param_man_params'), 'string', array(
                    'subtext' => \Yii::t('forms', 'manParamsDesc')
                ));
        }
        $this->_form->setValue( $this->oFieldRow )
            ->buttonSave('SaveField')
            ->buttonBack('FieldList');
    }
}