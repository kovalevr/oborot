<?php
/**
 * Created by PhpStorm.
 * User: holod
 * Date: 17.01.2017
 * Time: 12:21
 */

namespace skewer\build\Tool\Forms\view;

use skewer\components\ext\view\ListView;

class FieldList extends ListView
{
    public $aItems;
    public $bHasCatalogModule;
    public $bCrmIntegration;
    /**
     * @inheritdoc
     */
    function build()
    {
        $this->_list
            ->field('param_title', \Yii::t('forms', 'param_title'), 'string', ['listColumns' => ['flex' => 1]])
            ->field('param_name', \Yii::t('forms', 'param_name'), 'string', ['listColumns' => ['flex' => 1]])
            ->field('param_type', \Yii::t('forms', 'param_type'), 'string', ['listColumns' => ['flex' => 1]])
            ->field('param_required', \Yii::t('forms', 'param_required'), 'check', ['listColumns' => ['width' => 140]])
            ->widget( 'param_type', 'skewer\\components\\forms\\FieldTable', 'getTypeTitle' )
            ->enableDragAndDrop( 'sortFieldList' )
            ->setValue( $this->aItems )
            ->buttonRowUpdate( 'editField' )
            ->buttonRowDelete( 'deleteField' )
            ->buttonAddNew('EditField')
            ->buttonBack('list')
            ->buttonSeparator()
            ->buttonEdit('EditForm', \Yii::t('forms', 'from_settings'))
            ->buttonIf( $this->bHasCatalogModule, \Yii::t('forms', 'elConnectText'), 'linkList', 'icon-link' )
            ->buttonIf( $this->bCrmIntegration, \Yii::t('forms', 'elCrmIntegrationText'), 'CRMIntegration', 'icon-link' )
            ->buttonSeparator()
            ->buttonEdit('editSettingsResultPage', \Yii::t('forms', 'settingsResultPage'))
            ->buttonEdit('answer', \Yii::t('forms', 'answerDetailText'))
            ->buttonEdit('agreed', \Yii::t('forms', 'license'))
        ;
    }
}