<?php
/**
 * Created by PhpStorm.
 * User: holod
 * Date: 17.01.2017
 * Time: 15:30
 */

namespace skewer\build\Tool\Forms\view;

use skewer\components\ext\view\FormView;

class CrmLinkEdit extends FormView
{
    public $aFormFields;
    public $aCRMLink;

    /**
     * Выполняет сборку интерфейса
     * @return void
     */
    function build() {
        $this->_form
            ->fieldHide('id')
            ->fieldShow( 'crm_field_title', \Yii::t('forms', 'crm_field_title') )
            ->fieldSelect( 'field_id', \Yii::t('forms', 'form_field'), $this->aFormFields )
            ->setValue( $this->aCRMLink )
            ->buttonSave('saveCrmLink')
            ->buttonCancel('CrmLinkList')
        ;
    }
}