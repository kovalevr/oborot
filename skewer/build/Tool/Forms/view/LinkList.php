<?php
/**
 * Created by PhpStorm.
 * User: holod
 * Date: 17.01.2017
 * Time: 15:16
 */

namespace skewer\build\Tool\Forms\view;

use skewer\components\ext\view\ListView;

class LinkList extends ListView
{
    public $aItems;
    /**
     * Выполняет сборку интерфейса
     * @return void
     */
    function build()
    {
        $this->_list
            ->field('form_field', \Yii::t('forms', 'form_field'), 'string', array('listColumns' => array('flex' => 1)))
            ->field('card_field', \Yii::t('forms', 'card_field'), 'string', array('listColumns' => array('flex' => 1)))
            ->setValue( $this->aItems )
            ->buttonAddNew('addLink')
            ->buttonCancel('FieldList')
            ->buttonRowDelete( 'delLink' )
        ;
    }
}