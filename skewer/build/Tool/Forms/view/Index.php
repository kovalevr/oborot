<?php

/**
 * Created by PhpStorm.
 * User: holod
 * Date: 17.01.2017
 * Time: 11:22
 */
namespace skewer\build\Tool\Forms\view;

use skewer\components\ext\view\ListView;

class Index extends ListView
{
    public $bIsSystemMode;
    public $sHandlerLabel;
    public $aItems;
    /**
     * @inheritdoc
     */
    function build()
    {
        $this->_list
            ->field('form_title', \Yii::t('forms', 'form_title'), 'string', ['listColumns' => ['flex' => 1]])
            ->fieldIf(
                $this->bIsSystemMode, 'form_handler_type_show', \Yii::t('forms', 'form_handler_type'), 'string', ['listColumns' => ['width' => 140]]
            )
            ->field('form_handler_value', $this->sHandlerLabel, 'string', ['listColumns' => ['width' => 240]])
            ->fieldIf($this->bIsSystemMode, 'form_sys', \Yii::t('forms', 'form_sys'), 'check', ['listColumns' => ['width' => 70]])
            ->widget( 'form_handler_type_show', 'skewer\components\forms\Table', 'getTypeTitle' )
            ->setHighlighting('form_sys', \Yii::t('forms', 'form_sys'), '1', 'color: #999999')
            ->setEditableFields(['form_sys'], 'SaveFromList')
            ->setValue( $this->aItems )
            ->buttonAddNew('EditForm', \Yii::t('forms', 'add_new_form'))
            ->buttonRowUpdate( 'FieldList' )
            ->buttonRow('Clone', \Yii::t('adm', 'clone'), 'icon-clone', '')
            ->buttonRowDelete( 'delete' );
    }
}