<?php

namespace skewer\build\Tool\Forms;

use skewer\base\site\Type;
use skewer\build\Tool\Crm\Api as CRMApi;
use skewer\components\catalog;
use skewer\base\orm\Query;
use skewer\base\ui;
use skewer\components\forms;
use skewer\build\Tool;
use skewer\build\Page;
use skewer\components\auth\CurrentAdmin;
use skewer\base\site\Site;
use skewer\components\targets\models\Targets;
use yii\base\UserException;
use yii\helpers\ArrayHelper;

/**
 * Class Module
 * @package skewer\build\Tool\Forms
 */
class Module extends Tool\LeftList\ModulePrototype {

    public $iCurrentForm = 0;
    public $enableSettings = 0;

    protected function preExecute() {

        // id текущего раздела
        $this->iCurrentForm = $this->getInt('form_id');

        if ( !$this->iCurrentForm )
            $this->iCurrentForm = $this->getEnvParam('form_id');

    }


    protected function actionInit() {
        $this->actionList();
    }


    /**
     * Список форм
     */
    protected function actionList() {

        // -- обработка данных
        $this->iCurrentForm = 0;

        $oQuery = forms\Table::find()->order('form_sys');
        if ( !CurrentAdmin::isSystemMode() ) $oQuery
            ->where( 'form_handler_type<>?', 'toMethod' )
            ->andWhere( 'form_sys', 0 )
        ;
        $aItems = $oQuery->getAll();

        // добавляем отдельное поле для отображения надписи,
        // чтобы не перекрывалось при сохранении
        foreach ($aItems as $oItem)
            $oItem->form_handler_type_show = $oItem->form_handler_type;

        $sHandlerLabel = CurrentAdmin::isSystemMode() ? \Yii::t('forms', 'form_handler_value') :
            \Yii::t('forms', 'formHandlerEmailTitle') ;

        $this->setPanelName( \Yii::t('forms', 'form_list') );

        // сформировать интерфейс
        $this->render(new Tool\Forms\view\Index([
            "bIsSystemMode" => CurrentAdmin::isSystemMode(),
            "sHandlerLabel" => $sHandlerLabel,
            "aItems" => $aItems
        ]));

    }


    /**
     * Редактирование параметров формы
     * @param array $aSubData
     * @return int
     */
    protected function actionEditForm( $aSubData = array() ) {

        // -- обработка данных
        $aData = $this->getInData();

        if ( is_array($aSubData) && !empty($aSubData) )
            $aData = array_merge($aData, $aSubData);

        $iFormId =  isset( $aData['form_id'] ) ? (int)$aData['form_id'] : 0;
        $this->iCurrentForm = $iFormId ? $iFormId : $this->iCurrentForm;

        $oFormRow = $this->iCurrentForm ? forms\Table::find( $this->iCurrentForm ) : forms\Table::getNewRow();

        $sHandlerLabel = CurrentAdmin::isSystemMode() ? \Yii::t('forms', 'form_handler_value') :
            \Yii::t('forms', 'formHandlerEmailTitle') ;

        $aFormTypeList = forms\Table::getTypeList();
        if ( ! CurrentAdmin::isSystemMode() && isSet($aFormTypeList['toMethod']) )
            unSet( $aFormTypeList['toMethod'] );

        $this->render(new Tool\Forms\view\EditForm([
            "bIsSystemMode" => CurrentAdmin::isSystemMode(),
            "aFormTypeList" => $aFormTypeList,
            "sHandlerLabel" => $sHandlerLabel,
            "sFormHandlerSubtext" => sprintf(
                '%s <b>%s</b>',
                \Yii::t('forms', 'form_handler_value_default'),
                Site::getAdminEmail()
            ),
            "sFormNotificSubtext" => \Yii::t('forms', 'form_notific_value_default'),
            "bHasFormTarget" => (CurrentAdmin::isSystemMode() or CurrentAdmin::canDo(\skewer\build\Tool\Policy\Module::className(),'useFormsReachGoals')),
            "aYandexTarget" => Targets::getByTypeArray('yandex'),
            "aGoogleTarget" => Targets::getByTypeArray('google'),
            "oFormRow" => $oFormRow,
            "iCurrentForm" => $this->iCurrentForm

        ]));
        return psComplete;
    }

    /** Действие: сохранение параметров формы из списка и возврат в состояние списка форм */
    protected function actionSaveFromList() {
        
        $aData = $this->getInData();
        //пересохранение кнопки
        $sNameButton = $aData['form_button'];
        if ($sNameButton) {
            $aButton = explode('.',$sNameButton);
            $aData['form_button'] = \Yii::t($aButton[0], $aButton[1]);
        }
        $this->set('data',$aData);

        $this->actionSave();
        $this->actionList();
    }


    /**
     * Сохранение параметров формы
     */
    protected function actionSave() {

        try {

            $aData = $this->getInData();
            $aSystemForms = Api::$aModuleForms;

            if (isset($aData['form_name'])&&array_search($aData['form_name'],$aSystemForms)===false)
                Api::validateHandler($aData['form_handler_type'],$aData['form_handler_value']);

            $oFormRow = forms\Table::getById($this->iCurrentForm);
            if ( !$oFormRow ) {
                $oFormRow = forms\Table::getNewRow();
                $oFormRow->form_agreed = 1;
            }
            $oFormRow->setData( $aData );

            //FormOrders теперь абстрагирована от ID формы
            //Api::patchSections($aData['form_id'],$aData['form_handler_type']);

            $oFormRow->save();

            if ( $oFormRow->getError() )
                throw new \Exception( $oFormRow->getError() );

            $this->iCurrentForm = $oFormRow->getId();
            Api::getFormNameButton($this->iCurrentForm,$oFormRow->form_button);
            $oFormRow->setAddData();
            $this->actionFieldList();

        } catch ( \Exception $e ) {
            $this->addError( $e->getMessage() );
        }

    }


    /**
     * Удаление формы
     */
    protected function actionDelete() {

        try {

            $aData = $this->getInData();

            $iFormId = isSet( $aData['form_id'] ) ? $aData['form_id'] : false;

            if ( !$iFormId )
                throw new \Exception("not found form id=$iFormId");

            $oFormRow = forms\Table::find( $iFormId );

            if ( !$oFormRow )
                throw new \Exception("not found form id=$iFormId");

            $oFormRow->delete();

            Query::DeleteFrom('forms_add_data')->where('form_id',$iFormId)->get();
            $this->actionList();

        } catch ( \Exception $e ) {

            $this->addError( $e->getMessage() );
        }
    }


    /**
     * Клонирование формы
     */
    protected function actionClone() {

        $aData = $this->getInData();

        $iFormId = isSet( $aData['form_id'] ) ? (int)$aData['form_id'] : 0;

        if ( $iFormId ) {

            $oForm = forms\Table::getById( $iFormId );

            $oForm->form_id = 0;
            $oForm->form_title .= ' (clone)';

            $oForm->setUniqName();

            $iNewForm = $oForm->save();

            $aFields = forms\FieldTable::find()
                ->where( 'form_id', $iFormId )
                ->getAll();

            /** @var forms\FieldRow $oFieldRow */
            foreach ( $aFields as $oFieldRow ) {

                $oFieldRow->param_id = 0;
                $oFieldRow->form_id = $iNewForm;

                $oFieldRow->save();
            }

            $oForm->save();
            //клонирование соглашения и автоответа
            $aAddParams = Query::SelectFrom( 'forms_add_data' )->where( 'form_id', $iFormId )->asArray()->getOne();
            Query::InsertInto( 'forms_add_data' )
                ->set( 'form_id', $oForm->form_id )
                ->set( 'answer_title', $aAddParams['answer_title'] )
                ->set( 'answer_body', $aAddParams['answer_body'] )
                ->set( 'agreed_title', $aAddParams['agreed_title'] )
                ->set( 'agreed_text', $aAddParams['agreed_text'] )
                ->get();

            $this->addMessage( \Yii::t('forms', 'from_cloned' ) );

        } else {

            $this->addError( 'From not found.' );
        }

        $this->actionList();
    }


    protected function actionFieldList() {

        // -- обработка данных
        $aData = $this->getInData();

        $iFormId = isset( $aData['form_id'] ) ? (int)$aData['form_id'] : 0;
        $this->iCurrentForm = $iFormId ? $iFormId : $this->iCurrentForm;

        /** @var forms\Row $oFormRow */
        $oFormRow = forms\Table::find( $this->iCurrentForm );

        $aItems = forms\FieldTable::find()
            ->where( 'form_id', $this->iCurrentForm )
            ->order( 'param_priority' )
            ->getAll();

        // Преобразовать языковые метки
        if ($aItems) {
            /** @var forms\FieldRow $oItem */
            foreach ($aItems as $oItem)
                $oItem->param_title = \Yii::tSingleString($oItem->param_title);
        }

                $sHeadText = sprintf(
            '%s "%s"',
            \Yii::t('forms', 'field_list'),
            $this->iCurrentForm ? $oFormRow->form_title : \Yii::t('forms', 'new_form')
        );

        $this->setPanelName( $sHeadText );

        $this->render(new Tool\Forms\view\FieldList([
            "aItems" => $aItems,
            "bHasCatalogModule" => Type::hasCatalogModule(),
            "bCrmIntegration" => CRMApi::isCrmInstall()
        ]));

    }


    protected function actionSortFieldList() {

        $aData = $this->getInData();
        $aDropData = $this->get( 'dropData' );
        $sPosition = $this->get( 'position' );

        if( !isSet($aData['param_id']) || !$aData['param_id'] ||
            !isSet($aDropData['param_id']) || !$aDropData['param_id'] || !$sPosition )
            $this->addError( \Yii::t('forms', 'sortParamError') );

        if( ! forms\FieldTable::sort( $aData['param_id'], $aDropData['param_id'], $sPosition ) )
            $this->addError( \Yii::t('forms', 'sortParamError') );

    }


    /**
     * Отображение формы
     */
    protected function actionEditField() {

        // -- обработка данных
        $aData = $this->getInData();
        $iFieldId = isset( $aData['param_id'] ) ? (int)$aData['param_id'] : 0;

        $oFieldRow = $iFieldId ? forms\FieldTable::find( $iFieldId ) : forms\FieldTable::getNewRow();

        if ( !$oFieldRow )
            throw new \Exception( \Yii::t('forms', 'field_not_found') );

        $this->setPanelName( \Yii::t('forms', 'edit_field') );

        $aEntityList = forms\Api::getViewTypesBeFieldTypeId($oFieldRow->param_type);

        $this->render(new Tool\Forms\view\EditField([
            "aFieldTypes" => forms\FieldTable::getTypeList(),
            "iUploadMaxSize" => forms\FieldTable::getUploadMaxSize(),
            "aValidatorList" => forms\FieldTable::getValidatorList(),
            "bIsSystemMode" => CurrentAdmin::isSystemMode(),
            "aLabelPositionList" => forms\FieldTable::getLabelPositionList(),
            "aWidthFactorList" => forms\FieldTable::getWidthFactorList(),
            "aEntityList" => $aEntityList,
            "oFieldRow" => $oFieldRow
        ]));

        return psComplete;
    }

    protected function actionUpdFields(){

        $aFormData = $this->get('formData', array());

        $iParamType = $aFormData['param_type'];

        /*Зная id типа параметра получим класс который обеспечивает этот параметр*/
        $aParams = forms\Api::getViewTypesBeFieldTypeId($iParamType);

        $aOutParams = [
            'aLinkList' => $aParams,
            'bFieldIsNotLinked' => false,
            'aWidgetList' => [
                'check_group'=>'Группа галочек',
                'select'=>'Выпадающий список'
            ],
            'sWidget' =>0
        ];

        if (count($aParams)>1)
            $aOutParams['iLinkId'] = 1;
        else
            $aOutParams['iLinkId'] = 0;

        $view = new view\UpdFields($aOutParams);
        $view->build();
        $this->setInterfaceUpd($view->getInterface());
    }


    /**
     * Сохранение
     */
    protected function actionSaveField() {

        try {

            $aData = $this->getInData();

            if (!isset($aData['view_type']))
                throw new \Exception( 'Необходимо выбрать тип отображения' );

            $iFieldId = isset( $aData['param_id'] ) ? (int)$aData['param_id'] : 0;

            /*Не позволяем создать поле с таким именем*/
            if ($aData['param_name'] == 'cptch_country') {
                throw new \Exception( 'Идентификатор поля cptch_country запрещен к использованию' );
            }

            $oFieldRow = $iFieldId ? forms\FieldTable::find( $iFieldId ) : forms\FieldTable::getNewRow();
            $oFieldRow->setData( $aData );
            $oFieldRow->form_id = $this->iCurrentForm;

            $oFieldRow->save();

            $oForm = forms\Table::getById($oFieldRow->form_id);
            $oForm->last_modified_date = date( "Y-m-d H:i:s", time() );
            $oForm->save();

            if ( $oFieldRow->getError() )
                throw new \Exception( $oFieldRow->getError() );

            /** @var forms\Row $oFormRow */
            $oFormRow =  forms\Table::find( $this->iCurrentForm );
            if ( $oFormRow->form_handler_type == 'toBase' )
                $oFormRow->preSave();

            $this->actionFieldList();

        } catch ( \Exception $e ) {
            $this->addError( $e->getMessage() );
        }

    }


    /**
     * Удаляет запись
     */
    protected function actionDeleteField() {

        try {

            $aData = $this->getInData();

            $aData['form_id'] = $this->iCurrentForm;

            $iFieldId = iSset( $aData['param_id'] ) ? $aData['param_id'] : false;

            if ( !$iFieldId )
                throw new \Exception( "not found form field id=$iFieldId" );

            /** @var forms\FieldRow $oFieldRow */
            $oFieldRow = forms\FieldTable::find( $iFieldId );

            if ( !$oFieldRow )
                throw new \Exception( "not found form field id=$iFieldId" );

            $oFieldRow->delete();

            $oForm = forms\Table::getById($oFieldRow->form_id);
            $oForm->last_modified_date = date( "Y-m-d H:i:s", time() );
            $oForm->save();

            $this->actionFieldList();

        } catch ( \Exception $e ) {

            $this->addError( $e->getMessage() );
        }

    }


    protected function actionAnswer() {

        $sInfoText = \Yii::t('forms', 'head_mail_text', [\Yii::t('app', 'site_label'), \Yii::t('app', 'url_label')]);
        /** @var forms\Row $oFormRow */
        $oFormRow = forms\Table::find( $this->iCurrentForm );

        $aAddParams = Query::SelectFrom( 'forms_add_data' )->where( 'form_id', $this->iCurrentForm )->asArray()->getOne();

        $aValues = array(
            'form_answer' => $oFormRow->form_answer,
            'answer_title' => $aAddParams['answer_title'],
            'answer_body' => $aAddParams['answer_body']
        );

        $this->setPanelName( sprintf(
            '%s "%s"',
            \Yii::t('forms', 'answer_head'),
            $oFormRow->form_title
        ));

        if ($oFormRow->form_name == Tool\Review\Api::NAME_FORM_REVIEW) {
            $sLink = Site::httpDomainSlash().'admin/#out.left.tools=Review;out.tabs=tools_Review';
            $sAnswerReview = \Yii::t('forms', 'answer_review', [$sLink]);
        }

        $this->render(new Tool\Forms\view\Answer([
            "sInfoText" => $sInfoText,
            "aValues" => $aValues,
            'sAnswerReview' => (isset($sAnswerReview))?$sAnswerReview:''
        ]));

        return psComplete;

    }


    protected function actionAnswerSave() {

        // запросить данные
        $aData = $this->getInData();

        if ( !$this->iCurrentForm )
            throw new \Exception('не выбрана форма');

        if ( !isSet($aData['form_answer']) OR !isSet($aData['answer_title']) OR !isSet($aData['answer_body']) )
            throw new \Exception('не пришли данные');

        Query::InsertInto( 'forms_add_data' )
            ->set( 'form_id', $this->iCurrentForm )
            ->set( 'answer_title', $aData['answer_title'] )
            ->set( 'answer_body', $aData['answer_body'] )
            ->onDuplicateKeyUpdate()
            ->set( 'answer_title', $aData['answer_title'] )
            ->set( 'answer_body', $aData['answer_body'] )
            ->get();

        /** @var forms\Row $oFormRow */
        $oFormRow = forms\Table::find( $this->iCurrentForm );
        $oFormRow->form_answer = $aData['form_answer'];
        $oFormRow->save();

        // вывод списка
        $this->actionFieldList();
    }


    /**
     * Форма редактирования лицензионного соглашения
     */
    protected function actionAgreed() {

        $aAddParams = Query::SelectFrom( 'forms_add_data' )->where( 'form_id', $this->iCurrentForm )->asArray()->getOne();

        /** @var forms\Row $oFormRow */
        $oFormRow = forms\Table::find( $this->iCurrentForm );

        $aValues = array(
            'form_agreed' => $oFormRow->form_agreed,
            'agreed_title' => $aAddParams['agreed_title'],
            'agreed_text' => $aAddParams['agreed_text']
        );

        $this->setPanelName( sprintf(
            '%s "%s"',
            \Yii::t('forms', 'agreed_head'),
            $oFormRow->form_title
        ));

        $sInfoText = \Yii::t('forms', 'head_mail_text', [\Yii::t('app', 'site_label'), \Yii::t('app', 'url_label')]);
        
        $this->render(new Tool\Forms\view\Agreed([
            "aValues" => $aValues,
            "sInfoText" => $sInfoText
        ]));

        return psComplete;
    }

    /**
     * Сохранение лицензионного соглашения
     * @throws \Exception
     * @return void
     */
    protected function actionAgreedSave() {

        // запросить данные
        $aData = $this->getInData();

        if ( !$this->iCurrentForm )
            throw new \Exception('не выбрана форма');

        if ( !isSet($aData['form_agreed']) OR !isSet($aData['agreed_title']) OR !isSet($aData['agreed_text']) )
            throw new \Exception('не пришли данные');

        Query::InsertInto( 'forms_add_data' )
            ->set( 'form_id', $this->iCurrentForm )
            ->set( 'agreed_title', $aData['agreed_title'] )
            ->set( 'agreed_text', $aData['agreed_text'] )
            ->onDuplicateKeyUpdate()
            ->set( 'agreed_title', $aData['agreed_title'] )
            ->set( 'agreed_text', $aData['agreed_text'] )
            ->get();

        /** @var forms\Row $oFormRow */
        $oFormRow = forms\Table::find( $this->iCurrentForm );
        $oFormRow->form_agreed = $aData['form_agreed'];
        $oFormRow->save();

        // вывод списка
        $this->actionFieldList();
    }


    /**
     * Список полей формы связанных с каталогом
     */
    protected function actionLinkList() {

        // -- обработка данных
        if ( !$this->iCurrentForm )
            throw new \Exception('Не найдена форма!');

        $aItems = Query::SelectFrom( 'forms_links' )->where( 'form_id', $this->iCurrentForm )->getAll();

        $this->render(new Tool\Forms\view\LinkList([
            "aItems" => $aItems
        ]));
    }


    /**
     * Форма добавление связи
     * @throws \Exception
     * @return int
     */
    protected function actionAddLink() {

        // поля формы
        if ( !$this->iCurrentForm )
            throw new \Exception('Не найдена форма!');

        $aRows = forms\FieldTable::find()->where( 'form_id', $this->iCurrentForm )->getAll();
        $aFormFields = array();
        foreach ( $aRows as $oRow )
            $aFormFields[$oRow->param_name] = $oRow->param_title;

        // поля карточки товара
        $oCard = catalog\Card::get( catalog\Card::DEF_BASE_CARD );
        $aRows = $oCard->getFields();
        $aCardFields = array( 'id' => 'id' );
        if ( $aRows )
            foreach ( $aRows as $oRow )
                $aCardFields[$oRow->name] = $oRow->title;

        $this->render(new Tool\Forms\view\AddLink([
            "aFormFields" => $aFormFields,
            "aCardFields" => $aCardFields
        ]));

        return psComplete;
    }


    protected function actionSaveLink() {

        $aData = $this->getInData();

        if ( !$this->iCurrentForm )
            throw new \Exception(\Yii::t('forms', 'error_form_not_found'));

        if ( !isSet($aData['form_field']) OR !isSet($aData['card_field']) )
            throw new \Exception(\Yii::t('forms', 'error_form_not_found'));

        Query::InsertInto( 'forms_links' )
            ->set( 'form_id', $this->iCurrentForm )
            ->set( 'form_field', $aData['form_field'] )
            ->set( 'card_field', $aData['card_field'] )
            ->get();

        $this->actionLinkList();
    }


    protected function actionDelLink() {

        $aData = $this->getInData();

        if ( !$this->iCurrentForm )
            throw new \Exception(\Yii::t('forms', 'error_form_not_found'));

        if ( !isSet($aData['link_id']) OR !$aData['link_id'] )
            throw new \Exception(\Yii::t('forms', 'error_form_not_found'));

        Query::DeleteFrom( 'forms_links' )
            ->where( 'link_id', $aData['link_id']  )
            ->where( 'form_id', $this->iCurrentForm )
            ->get();

        $this->actionLinkList();
    }

    protected function actionCRMIntegration(){

        if ( !$this->iCurrentForm )
            throw new \Exception(\Yii::t('forms', 'error_form_not_found'));

        $oForm = forms\Table::find( $this->iCurrentForm );

        $this->render( new Tool\Forms\view\CrmIntegrationEdit(['oFormRow' => $oForm]) );

        return psComplete;

    }

    protected function actionCrmLinkList(){

        if ( !$this->iCurrentForm )
            throw new \Exception(\Yii::t('forms', 'error_form_not_found'));

        /** @var forms\Table $oFormRow */
        $oFormRow = forms\Table::find( $this->iCurrentForm );
        $aFormFields = forms\FieldTable::find()->where('form_id',$this->iCurrentForm)->getAll();
        $aFormFieldsTitle = ArrayHelper::map($aFormFields,'param_id','param_title');
        $aCrmFields = CRMApi::getCRMFieldsList();
        $iFields = Query::SelectFrom('crm_link_form')
            ->where('form_id',$this->iCurrentForm)
            ->getCount('1');

        if (!$iFields){
            foreach ($aCrmFields as $key => $field){
                Query::InsertInto('crm_link_form')
                    ->set('crm_field_alias', $key)
                    ->set('form_id', $this->iCurrentForm)
                    ->get();
            }
        }
        
        $aFields = Query::SelectFrom('crm_link_form')
            ->where('form_id',$this->iCurrentForm)
            ->getAll();

        foreach ($aFields as $key => $field ) {
            $aFields[$key]['crm_field_title'] = $aCrmFields[$field['crm_field_alias']];
            if ($field['field_id'] && (isset($aFormFieldsTitle[$field['field_id']])))
                $aFields[$key]['form_field_title'] = $oFormRow->form_sys? \Yii::tSingleString($aFormFieldsTitle[$field['field_id']]) : $aFormFieldsTitle[$field['field_id']];
            else
                $aFields[$key]['form_field_title'] = '-';
        }

        $this->render( new Tool\Forms\view\CrmFieldList(['aItems' => $aFields]));

        return psComplete;
    }

    protected function actionEditCrmLink(){
        $aData = $this->getInData();
        
        $aForm = forms\Table::find($this->iCurrentForm);

        $aRows = forms\FieldTable::find()->where( 'form_id', $this->iCurrentForm )->getAll();
        $aFormFields = array();
        
        foreach ( $aRows as $oRow )
            $aFormFields[$oRow->param_id] = ($aForm->form_sys)? \Yii::tSingleString($oRow->param_title) : $oRow->param_title;

        $this->render(new Tool\Forms\view\CrmLinkEdit([
            "aFormFields" => $aFormFields,
            "aCRMLink" => $aData
        ]));

        return psComplete;
    }
    
    protected function actionSaveCrmLink(){
        $aData = $this->getInData();

        Query::UpdateFrom('crm_link_form')
            ->set('field_id' , $aData['field_id'])
            ->where('id',$aData['id'])
            ->get();

        return $this->actionCrmLinkList();
    }
    
    /**
     * Установка служебных данных
     * @param ui\state\BaseInterface $oIface
     */
    protected function setServiceData(ui\state\BaseInterface $oIface ) {

        // установить данные для передачи интерфейсу
        $oIface->setServiceData( array(
            'form_id' => $this->iCurrentForm,
            'enableSettings' => $this->enableSettings
        ) );

    }

    /** Редактирование настроек результирующей страницы формы */
    protected function actionEditSettingsResultPage(){

        /** @var forms\Row $oFormRow */
        $oFormRow = forms\Table::find( $this->iCurrentForm );

        $aValues = array(
            'form_type_result_page' => $oFormRow->form_type_result_page,
            'form_redirect' => $oFormRow->form_redirect,
            'form_succ_answer' => $oFormRow->form_succ_answer
        );

        $this->setPanelName( sprintf(
            '%s "%s"',
            \Yii::t('forms', 'settings_result_page_head'),
            $oFormRow->form_title
        ));

        $this->render(new Tool\Forms\view\SettingsResultPage([
            "aItems" => $aValues
        ]));

    }

    /** Ajax-обновление формы редактирования настроек результирующей страницы */
    public function actionUpdateSettingResultPageForm(){

        $aData = $this->get('formData', []);

        $this->render(
            new Tool\Forms\view\SettingsResultPage([
                'aItems' => $aData,
            ])
        );
    }

    /** Сохранение настроек результирующей страницы формы */
    protected function actionSaveSettingResultPage(){

        $aData = $this->getInData();

        if ( $oForm  = forms\Table::findOne(['form_id' => $this->iCurrentForm]) ){

            $oForm->setData($aData);
            if ( !$oForm->save() )
                throw new UserException($oForm->getError());
        }

        $this->actionFieldList();

    }

} //class