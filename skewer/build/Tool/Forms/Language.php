<?php

$aLanguage = array();

$aLanguage['ru']['Forms.Tool.tab_name'] = 'Формы';

$aLanguage['ru']['editFormReport'] = "Форма в Конструктор форм изменена";
$aLanguage['ru']['deleteFormReport'] = "Форма в Конструктор форм удалена";
$aLanguage['ru']['sortParamError'] = 'Ошибка! Неверно заданы параметры сортировки';
$aLanguage['ru']['defaultDesc'] =  "<ul>
        <li> • Текстовое поле - строка</li>
        <li> • Параметр галочка - поле \"значение по умолчанию\" должно быть обязательно заполнено текстом, который будет отправлен при установленной галочке!</li>
        <li> • Выпадающий список, переключатели - строка вида <b>value:title;</b> (каждая запись с новой строки)</li>
        <li> • Выпадающий список, переключатели - строка вида <b>ClassName.methodName()</b>
        где <b>methodName()</b> - публичный метод класса, принимающий необязательный аргумент-массив </li>
        <li> • Выпадающий список, вывод справочника - строка вида <b>dictionary(dict_sys_name)</b>, где <b>dict_sys_name</b> - системное имя справочника</li>
    </ul>";
$aLanguage['ru']['maxlength_desc'] = 'Максимальная длина в символах для текстовых полей. Для поля "Загрузка файла" - максимальный размер загружаемого файла в мегабайтах. (Ограничение {0, number}Мб)';
$aLanguage['ru']['deleteButton'] = 'Удалить';
$aLanguage['ru']['license'] = 'Лицензионное соглашение';
$aLanguage['ru']['label_position'] = 'Позиционирование заголовка';
$aLanguage['ru']['new_line'] = 'Вывод с новой строки';
$aLanguage['ru']['group'] = 'Группировать';
$aLanguage['ru']['width_factor'] = 'Множитель ширины';
$aLanguage['ru']['manParamsDesc'] = '<ul><li> • защита от заполнения <b>readonly="readonly"</b></li>
<li> • подсказка для ввода телефона +7 () _-_-__ <b>data-mask="phone"</b></li>
<li> • текст подсказки <b>placeholder="Текст"</b>, где вместо <i>Текст</i> добавляем нужную фразу</li>
<li> • скрытие текста placeholder при наведении <b>data-hide-placeholder="1"</b></li>
</ul>';
$aLanguage['ru']['form_field'] = 'Поле из формы';
$aLanguage['ru']['form_button'] = 'Кнопка для формы';
$aLanguage['ru']['card_field'] = 'Поле из карточки товара';
$aLanguage['ru']['from_settings'] = 'Настройка формы';
$aLanguage['ru']['from_cloned'] = 'Форма успешно клонирована';
$aLanguage['ru']['head_mail_text'] = 'Метки для замены:<br>
[{0}] - Название сайта<br>
[{1}] - Адрес сайта
';

$aLanguage['ru']['form_send_crm'] = 'Отправлять данные в CRM';
$aLanguage['ru']['crm_field_list'] = 'Список полей';
$aLanguage['ru']['crm_field_title'] = 'Поле в CRM';
$aLanguage['ru']['field_modifydate'] = 'Дата последнего изменения';
$aLanguage['ru']['show_phrase_required_fields'] = "Вывод фразы \"* - обязательные для заполнения поля\"";
$aLanguage['ru']['error_form_not_found'] = 'Не найдена форма!';
$aLanguage['ru']['form_succ_answer'] = 'Текст результирующей страницы';
$aLanguage['ru']['form_show_header'] = 'Выводить заголовок формы';
$aLanguage['ru']['invalid_email'] = 'Некорректный e-mail';
$aLanguage['ru']['invalid_method'] = 'Некорректный метод';
$aLanguage['ru']['no_method'] = 'Метод не существует или не может быть вызван';
$aLanguage['ru']['no_class'] = 'Класс не существует';
$aLanguage['ru']['no_send'] = 'Отключить отправку поля';
$aLanguage['ru']['replyTo'] = 'Адрес отправителя в ReplyTo';

$aLanguage['ru']['agreement_title'] = "Я согласен на обработку предоставленных данных";
$aLanguage['ru']['privacy_policy'] = "Настоящим подтверждаю, что я ознакомлен и согласен с условиями Политики в отношении обработки персональных данных. Настоящим я даю разрешение [адрес сайта] (далее – сайт) собирать, записывать, систематизировать, накапливать, хранить, уточнять (обновлять, изменять), извлекать, использовать, передавать (в том числе поручать обработку другим лицам), обезличивать, блокировать, удалять, уничтожать - мои персональные данные: фамилию, имя, пол, дату рождения, номера домашнего и мобильного телефонов, адрес электронной почты, а также данные об интересах на основании анализа моих поисковых запросов и посещений Интернет-сайтов. Согласие может быть отозвано мною в любой момент путем направления письменного уведомления по адресу сайта. <a target=\"_blank\" href = [link_policy] >Полные правила политики конфиденциальности сайта</a>";
$aLanguage['ru']['notification_letter'] = "Не присылать данные в письме";

// ********************************************************************************************************************
// ***************************************************** ENGLISH ******************************************************
// ********************************************************************************************************************

$aLanguage['en']['Forms.Tool.tab_name'] = 'Forms';

$aLanguage['en']['editFormReport'] = "Form in Form Designer changed" ;
$aLanguage['en']['deleteFormReport'] = "Form in Form Designer deleted" ;
$aLanguage['en']['sortParamError'] = 'Error! Incorrectly set collation';
$aLanguage['en']['defaultDesc'] = "<ul>
        <li> • Text field - string </li>
        <li> • Parameter check - the \"default\" must be filled text that will be sent at the set tick ! </li>
        <li> • The drop-down list , the switches - a string of the form <b> value:title; </b> ( each entry on a new line ) </li>
        <li> • The drop-down list , the switches - a string of the form <b> ClassName.methodName() </b> </li>
        <li> • Where <b> methodName()</b> - public class method that accepts an optional argument - array </li>
    </ul> ";
$aLanguage['en']['maxlength_desc'] = 'The maximum length in characters for text fields. For the "File Download" - the maximum upload file size in megabytes. (Restriction {0, number}Mb)';
$aLanguage['en']['deleteButton'] = 'Delete';
$aLanguage['en']['license'] = 'License agreement';
$aLanguage['en']['label_position'] = 'Label position';
$aLanguage['en']['new_line'] = 'Field with new line';
$aLanguage['en']['group'] = 'Group';
$aLanguage['en']['width_factor'] = 'Width factor';
$aLanguage['en']['manParamsDesc'] = '<ul><li> • protection from filling <b>readonly="readonly"</b></li>
<li> • prompt to enter your phone +7 () _-_-__ <b>data-mask="phone"</b></li>
<li> • for placeholder enter <b>placeholder="Text"</b></li>
<li> • to hide placeholder on select or mouse over - enter <b>data-hide-placeholder="1"</b></li>
</ul>';
$aLanguage['en']['form_field'] = 'Field from form';
$aLanguage['en']['form_button'] = 'Button from form';
$aLanguage['en']['card_field'] = 'Field from card';
$aLanguage['en']['from_settings'] = 'Form settings';
$aLanguage['en']['from_cloned'] = 'Form successfully cloned';
$aLanguage['en']['head_mail_text'] = 'Tags for replacement:<br>
[{0}] - Site Title<br>
[{1}] - Site Address
';

$aLanguage['en']['form_send_crm'] = 'Send to CRM';
$aLanguage['en']['crm_field_list'] = 'Fields list';
$aLanguage['en']['crm_field_title'] = 'Field in CRM';
$aLanguage['en']['field_modifydate'] = 'Last modify date';
$aLanguage['en']['show_phrase_required_fields'] = "Show phrase \"* - required fields\"";
$aLanguage['en']['error_form_not_found'] = 'Not found form!';
$aLanguage['en']['form_succ_answer'] = 'Result page text';
$aLanguage['en']['form_show_header'] = 'Show form header';

$aLanguage['en']['invalid_email'] = 'Invalid e-mail';
$aLanguage['en']['invalid_method'] = 'Invalid method';
$aLanguage['en']['no_method'] = 'Unknown method';
$aLanguage['en']['no_class'] = 'Unknown class';
$aLanguage['en']['no_send'] = 'Disable sending field';
$aLanguage['en']['replyTo'] = 'The sender address in ReplyTo';
$aLanguage['en']['agreement_title'] = "I agree to the processing of the data provided";
$aLanguage['en']['privacy_policy'] = "I hereby give permission to the [url] (the \"site\") for the purposes of conclusion and execution of the contract of purchase and sale process - to collect, record, systematize, accumulate, store, clarify (update, change), extract, use, transfer (including to outsource the processing of other persons), depersonalize, block, erase, destroy my personal data: surname, name, numbers home and mobile phone numbers, e-mail address. I also allow the website to inform about the goods, works, services to process the above personal data and send me to the specified e-mail address and/or mobile phone number advertising and information about goods, works, services of the website. Consent may be withdrawn by me at any time by sending a written notice at the address of the website. <a target=\"_blank\" href = [link_policy] > Full terms of the website privacy policy</a>";
$aLanguage['en']['notification_letter'] = "Don't send the date in a letter";

$aLanguage['de']['agreement_title'] = "Gebe Zustimmung zur Verarbeitung der zur Verfügung gestellten Daten";
$aLanguage['de']['privacy_policy'] = "Hiermit gebe ich die Erlaubnis [website adresse] (im folgenden – Webseite) für die Zwecke des Abschlusses und der Ausführung eines Kaufvertrags verarbeiten - zu sammeln, aufzuzeichnen, zu organisieren, zu sammeln, zu speichern, zu präzisieren (Aktualisierung, änderung), zu extrahieren, zu nutzen, zu übertragen (einschließlich der Aufladung der Verarbeitung von anderen Personen), entpersönlichen, zu sperren, zu löschen, zu zerstören - meine persönlichen Daten: name, Vorname, Zimmer, Home und Mobile Telefone, E-Mail-Adresse. Auch ich Erlaube Website, um Informationen über waren, arbeiten, Dienstleistungen zur Verarbeitung der oben genannten personenbezogenen Daten und verweisen auf die von mir angegebene E-Mail-Adresse und/oder Handynummer auf Werbung und Informationen über Produkte, Dienstleistungen würden. Die Zustimmung kann von mir jederzeit widerrufen werden jederzeit durch schriftliche Mitteilung an die Adresse der Website. <a target=\"_blank\" href = [link_policy] href = [link_policy] >Die vollständigen Regeln Datenschutz</a>";
return $aLanguage;