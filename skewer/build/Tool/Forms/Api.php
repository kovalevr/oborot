<?php
namespace skewer\build\Tool\Forms;

use skewer\base\section\models\ParamsAr;
use skewer\components\forms\Entity;
use skewer\components\forms\Table;
use skewer\components\i18n\Languages;
use skewer\components\i18n\Messages;
use skewer\components\i18n\models\LanguageValues;
use yii\base\Exception;
use skewer\build\Tool\Review\Api as ApiReview;

class Api {

    public static $aModuleForms = [
        ApiReview::NAME_FORM_REVIEW,
        'form_faq',
        Entity::NAME_FORM_ORDER,
        Entity::NAME_FORM_ONE_CLICK,
    ];

    /**
     * Метод обойдет все секции в которых подключена эта форма
     * и уберет из параметров Tool\FormOrders
     * @param $iFormId
     */
    public static function patchSections($iFormId,$sHandlerType){

        $aParams = ParamsAr::find()
            ->where(['name'=>'FormId'])
            ->where(['value'=> $iFormId])
            ->asArray()
            ->all();

        $aSections = [];

        foreach ($aParams as $aParam){
            $aSections[] = $aParam['parent'];
        }

        if ($sHandlerType!=='toBase') {
            ParamsAr::deleteAll([
                'parent' => $aSections,
                'value' => 'Tool\FormOrders',
                'name' => 'objectAdm'
            ]);
        }else{

            foreach ($aSections as $section){

                $oParam = new ParamsAr();

                $oParam->group = 'forms_orders';
                $oParam->parent = $section;
                $oParam->value = 'Tool\FormOrders';
                $oParam->name = 'objectAdm';
                $oParam->access_level = '0';
                $oParam->save(false);

            }

        }

    }

    /**
     * Проверяет значение обработчика формы на валидность
     * @param $sType
     * @param $sValue
     * @throws Exception
     * @throws \Exception
     */
    public static function validateHandler($sType,$sValue){

        switch ($sType){
            case 'toMail':
                /*Если пусто. используем системный email*/
                if ($sValue=='') return;
                if (!filter_var($sValue,FILTER_VALIDATE_EMAIL))
                    throw new Exception (\Yii::t('forms','invalid_email'));
                break;
            case 'toMethod':
                $aParams = explode('.',$sValue);
                /*Проверим похоже ли это не метод*/
                if (count($aParams)!='2')
                    throw new Exception (\Yii::t('forms','invalid_method'));

                /*Есть такой класс?*/
                if (!class_exists($aParams[0]))
                    throw new Exception (\Yii::t('forms','no_class'));

                /*Есть такой метод?*/
                $oObject = new $aParams[0]();
                if (!method_exists($oObject,$aParams[1]))
                    throw new Exception (\Yii::t('forms','no_method'));

                /*Этот класс унаследован от прототипа сервиса?*/
                if (get_parent_class($oObject)!=='skewer\base\site\ServicePrototype' )
                    throw new \Exception( \Yii::t( 'forms', 'wrong_class') );

                break;
        }
    }


    /**
     * Обработка метки для кнопки
     * @var $iCurrentId
     * @var $sValueButton
     */
    public static function getFormNameButton($iCurrentId,$sValueButton) {
        if ($sValueButton) {
            $currentLang = \Yii::$app->i18n->getTranslateLanguage();
            $allLang = Languages::getAllActive();
            $oRow = Messages::getByName('forms', 'button_'.$iCurrentId, $currentLang);
            if (!$oRow) {
                foreach ($allLang as $lang) {
                    $oParameters = new LanguageValues();
                    $oParameters->value = ($lang['name']==$currentLang)?$sValueButton:' ';
                    $oParameters->message = 'button_'.$iCurrentId;
                    $oParameters->category = 'forms';
                    $oParameters->language = $lang['name'];
                    $oParameters->override = LanguageValues::overrideYes;
                    $oParameters->status = LanguageValues::statusTranslated;
                    $oParameters->save();
                }

            } else {
                $oRow->value = $sValueButton;
                $oRow->save();
            }

            $oFormRow = Table::getById($iCurrentId);
            $oFormRow->form_button = 'forms.'.'button_'.$iCurrentId;
            $oFormRow->save();
        }
    }

}
