<?php

$aLanguage = array();
$aLanguage['ru']['start']          = 'Начало';
$aLanguage['ru']['finish']         = 'Конец';
$aLanguage['ru']['create_section'] = 'Созданы разделы';
$aLanguage['ru']['status']         = 'Статус';
$aLanguage['ru']['error_task_not_fount']         = 'Задача не найдена!';
$aLanguage['ru']['log_show']  = 'Лог импорта';
$aLanguage['ru']['log_result'] = 'Результат';
$aLanguage['ru']['error_run']                    = 'Не удалось запустить задачу';
$aLanguage['ru']['import_headTitle'] = 'Импорт';
$aLanguage['ru']['import_filename']  = 'Файл - образец для создания дерева разделов';
$aLanguage['ru']['import_filename_news']  = 'Файл - образец для создания новостей';
$aLanguage['ru']['import_filename_reviews']  = 'Файл - образец для создания отзывов';
$aLanguage['ru']['import_filename_articles']  = 'Файл - образец для создания статей';

$aLanguage['en']['start']          = 'Home';
$aLanguage['en']['finish']         = 'End';
$aLanguage['en']['create_section'] = 'Create Partition';
$aLanguage['en']['status']         = 'Status';
$aLanguage['en']['error_task_not_fount']         = 'The problem is not found!';
$aLanguage['en']['log_show']  = 'Log imports';
$aLanguage['en']['log_result'] = 'Result';
$aLanguage['en']['error_run']                    = 'Unable to start task';
$aLanguage['en']['import_headTitle'] = 'Import';
$aLanguage['en']['export_headTitle'] = 'Export';
$aLanguage['en']['import_filename']                    = 'File - sample for create partitions tree';
$aLanguage['en']['export_filename']                    = 'export';


return $aLanguage;