<?php

namespace skewer\build\Tool\ReserveOnline;

use skewer\base\section\Parameters;
use skewer\base\section\Tree;
use skewer\base\SysVar;
use skewer\build\Tool;
use skewer\build\Design;
use skewer\base\ui;
use skewer\components\auth\CurrentAdmin;
use skewer\components\forms\Table;

/**
 * Модуль настроек бронирования гостиниц онлайн
 * Class Module
 * @package skewer\build\Tool\ReserveOnline
 */
class Module extends Tool\LeftList\ModulePrototype {

    private $idFormReserve = '';

    /** Состояние: Инициализация */
    protected function actionInit() {

        /** Список всех форм */
        $aTemplateForms = \skewer\build\Adm\Forms\Module::getFormsTemplates();

        $oForm = ui\StateBuilder::newEdit();

        $this->idFormReserve = Api::getReserveOnlineSectionByLang(Api::ADM_FORM_LANG);

        Table::get4Section($this->idFormReserve, 'Forms', $aForms, true);
        // Добавить для каждой формы раздела настройку
        $iKey = 0;
        foreach ($aForms as $sGroup => $iFormId) {
            $iKey++;

            $sFormTitle = ($sGroupTitle = Parameters::getValByName($this->idFormReserve, $sGroup, Design\Zones\Api::layoutTitleName, true)) ?
                "\"$sGroupTitle\"" :
                ((count($aForms) > 1) ? "№" . ($iKey) : "");

            // Добавить имя группы для каждой формы пользователя sys
            if ( CurrentAdmin::isSystemMode() and (count($aForms) > 1) )
                $sFormTitle .= " [$sGroup]";

            $oForm
                ->fieldSelect("form_$sGroup", rtrim(\Yii::t('forms', 'select_form') . " $sFormTitle"), $aTemplateForms, [], false)
                ->setValue(["form_$sGroup" => $iFormId]);
        }
        $oForm->fieldCheck('show_discount', \Yii::t('ReserveOnline', 'show_discount'), ['groupTitle' => \Yii::t('ReserveOnline', 'widget_group')]);
        $oForm->field('discount_img', \Yii::t('ReserveOnline', 'discount_img'), 'file', ['groupTitle' => \Yii::t('ReserveOnline', 'widget_group')]);
        $oForm->fieldString('discount_link', \Yii::t('ReserveOnline', 'discount_link'), ['groupTitle' => \Yii::t('ReserveOnline', 'widget_group')]);

        $oForm->setValue([
                             'show_discount'              => Api::getParamForWidget('show_discount'),
                             'discount_img'               => Api::getParamForWidget('discount_img'),
                             'discount_link'              => Api::getParamForWidget('discount_link'),
                         ]);

        $oForm->buttonSave('saveSettings');

        return $this->setInterface($oForm->getForm());
    }

    /** Действие: Сохранение настроек */
    protected function actionSaveSettings() {
        $aData = $this->get('data');

        foreach($aData as $sFormLabel => $iFormId) {
            if (strpos($sFormLabel, 'form_') === 0) {
    
                $sGroup = substr($sFormLabel, 5);
                $aIdReserve = Api::getReserveOnlineSection();
                
                foreach ($aIdReserve as $id) {
                    
                    Table::link2Section($iFormId, $id, $sGroup);

                    $oFormRow = Table::find($iFormId);
                    // Если группа будет называться на точку, то объект в ней в админке не выполнится
                    $sGroupFormOrders = (($sGroup == '.') ? 'root' : $sGroup) . "_orders";

                    if ( $oFormRow and ($oFormRow->form_handler_type == 'toBase') ) {
                        Parameters::setParams( $id, $sGroupFormOrders, 'objectAdm', 'Tool\FormOrders' );
                        Parameters::setParams( $id, $sGroupFormOrders, 'formId', $iFormId );
                    } else {
                        Parameters::removeByName( 'objectAdm', $sGroupFormOrders, $this->idFormReserve );
                        Parameters::removeByName( 'formId', $sGroupFormOrders, $this->idFormReserve );
                    }
                }
            }
        }

        $oTree = Tree::getSection($this->idFormReserve);
        $oTree->last_modified_date = date( "Y-m-d H:i:s", time() );
        $oTree->save();

        $this->fireJSEvent( 'reload_section' );
        $this->addModuleNoticeReport( \Yii::t('forms', 'editingFormInSection'), \Yii::t('forms', 'section_id') .": $this->idFormReserve");

        Api::updateModuleParams($this->idFormReserve,
                                $this->getInDataVal('show_discount'),
                                $this->getInDataVal('discount_img'),
                                $this->getInDataVal('discount_link')
        );
        $this->actionInit();
    }

}
