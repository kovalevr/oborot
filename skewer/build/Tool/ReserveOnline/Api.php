<?php

namespace skewer\build\Tool\ReserveOnline;

use skewer\base\section;
use skewer\components\i18n\Languages;

/**
 * Набор методов для настройки модуля "Бронирования гостиниц"
 * Class Module
 * @package skewer\build\Tool\ReserveOnline
 */
class Api {

    /** Имя метки параметров настройки раздела */
    const ADM_GROUP_NAME = 'ReserveOnline';

    /** Имя метки параметров настройки раздела для внутреннего виджета */
    const ADM_WIDGET_GROUP_NAME = 'ReserveOnlineWidget';

    /** Имя параметра для ветки по умолчанию */
    const ADM_FORM_LANG = 'ru';

    /** Картинка акции по умолчанию */
    const DISCOUNT_DEF_IMG = 'bronline.akcii.png';

    /**
     * Получение значения параметра настройки для состояния модуля: виджет
     * @param string $ParamName Имя параметра
     * @return string|int
     */
    public static function getParamForWidget($ParamName) {

        $oParam = section\Parameters::getByName(\Yii::$app->sections->languageRoot(), self::ADM_WIDGET_GROUP_NAME, $ParamName);
        return ($oParam) ? $oParam->value : '';
    }

    /**
     * Обновление параметров модуля в разделе
     * @param int $iSectionId Id раздела
     * @param string $sHotelId Id гостиницы
     * @param int $iShowDiscount Показывать акцию?
     * @param string $sDiscountImg Изображение акции
     * @param string $sDiscountLink Ссылка акции
     */
    public static function updateModuleParams($iSectionId, $iShowDiscount, $sDiscountImg, $sDiscountLink) {

        $sLang = section\Parameters::getLanguage($iSectionId);

        // Сохранить показ акции
        self::setLanguageParam([
                                  'name'   => 'show_discount',
                                  'group'  => self::ADM_WIDGET_GROUP_NAME,
                                  'value'  => $iShowDiscount,
                              ], [$sLang]);

        // Сохранить изображение акции
        self::setLanguageParam([
                                  'name'   => 'discount_img',
                                  'group'  => self::ADM_WIDGET_GROUP_NAME,
                                  'value'  => $sDiscountImg,
                              ], [$sLang]);

        // Сохранить ссылку для акции
        self::setLanguageParam([
                                   'name'   => 'discount_link',
                                   'group'  => self::ADM_WIDGET_GROUP_NAME,
                                   'value'  => $sDiscountLink,
                               ], [$sLang]);
    }

    /** Установка/обновление параметра раздела */
    public static function setSectionParam($aData) {

        if ( !isset($aData['parent']) or !isset($aData['name']) or !isset($aData['group']) )
            return;

        $oParam = section\Parameters::getByName($aData['parent'], $aData['group'], $aData['name']);
        if (!$oParam)
            $oParam = section\Parameters::createParam($aData);
        else
            $oParam->setAttributes($aData);

        $oParam->save();
    }

    /**
     * Установка/обновление языкового параметра
     * @param array $aData Данные параметра
     * @param array $aLangs Массив языковых меток, куда он добавляется. Если пуст, то во все.
     */
    public static function setLanguageParam(array $aData, array $aLangs = []) {

        ($aLangs) or ($aLangs = Languages::getAllActiveNames());

        // Установить значение для каждой языковой ветки, если не задан языковой раздел напрямую
        foreach($aLangs as $sLang) {
            $aData['parent'] = \Yii::$app->sections->getValue(section\Page::LANG_ROOT, $sLang);
            self::setSectionParam($aData);
        }

        // Установить языковой параметр в шаблон новой страницы
        $aData['parent'] = \Yii::$app->sections->tplNew();
        $aData['access_level'] = section\params\Type::paramLanguage;
        $aData['value'] = $aData['show_val'] = '';
        self::setSectionParam($aData);
    }
    /**
     * Найти раздел с установленным модулем бронирования для языка
     * @return array() idReserve раздела
     */
    public static function getReserveOnlineSection() {

        $aParamsReserveOnline = section\Parameters::getList()
            ->group(Api::ADM_GROUP_NAME)
            ->name('object')
            ->get();

        foreach($aParamsReserveOnline as $oParam) {
            $aIdReserve[] = $oParam->parent;
        }
        return $aIdReserve;
    }
    /**
     * Найти раздел с установленным модулем бронирования для языка
     * @param string $sLang Псевдоним языка
     * @return int Id раздела
     */
    public static function getReserveOnlineSectionByLang($sLang) {

        $aParamsReserveOnline = section\Parameters::getList()
            ->group(Api::ADM_GROUP_NAME)
            ->name('object')
            ->get();

        foreach($aParamsReserveOnline as $oParam)
            if (section\Parameters::getLanguage($oParam->parent) == $sLang)
                return $oParam->parent;
    }
}