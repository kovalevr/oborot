<?php

$aLanguage = array();

$aLanguage['ru']['tab_name'] = 'Утилиты';

$aLanguage['ru']['renew_act'] = 'Обновить страницу';
$aLanguage['ru']['clear_logs'] = 'Очистить логи';
$aLanguage['ru']['logs'] = 'Логи';
$aLanguage['ru']['search'] = 'Поиск';
$aLanguage['ru']['optimize_db'] = 'Оптимизация БД';
$aLanguage['ru']['optimize_db_text'] = 'БД оптимизирована';
$aLanguage['ru']['reset_search_index'] = 'Сброс поискового индекса';
$aLanguage['ru']['resetActive'] = 'Сброс метки поискового индекса';
$aLanguage['ru']['reindex'] = 'Обновить индекс';
$aLanguage['ru']['rebuildSitemap'] = 'Перестроить sitemap';
$aLanguage['ru']['SearchDropAll'] = 'Удалить всё';
$aLanguage['ru']['rebuildLanguages'] = 'Перестроить языковые значения';
$aLanguage['ru']['collision'] = 'Обнаружены коллизии "{collisions}"';

$aLanguage['ru']['view_access'] = 'Логи доступа';
$aLanguage['ru']['view_error'] = 'Логи ошибок';
$aLanguage['ru']['search_drop_index'] = 'Поисковый индекс очищен';
$aLanguage['ru']['record_update'] = 'Обновленно записей';
$aLanguage['ru']['new_update_msg'] = 'Запустите скрипт по новой, не все записи обновлены';
$aLanguage['ru']['sitemap_update_error'] = 'Ошибка обновления';
$aLanguage['ru']['sitemap_update_msg'] = 'sitemap обновлен';
$aLanguage['ru']['count_records_processed_for_iteration'] = 'Обработано за итерацию';
$aLanguage['ru']['total_count_records_processed'] = 'Всего обработано';
$aLanguage['ru']['count_all_records'] = 'Количество всех записей';
$aLanguage['ru']['execution_status'] = 'Статус выполнения';

// ********************************************************************************************************************
// ***************************************************** ENGLISH ******************************************************
// ********************************************************************************************************************

$aLanguage['en']['tab_name'] = 'Utils';

$aLanguage['en']['renew_act'] = 'Renew page';
$aLanguage['en']['clear_logs'] = 'Clear logs files';
$aLanguage['en']['logs'] = 'Logs';
$aLanguage['en']['reset_search_index'] = 'Reset search index';
$aLanguage['en']['search'] = 'Search';
$aLanguage['en']['optimize_db'] = 'Optimize DB';
$aLanguage['en']['optimize_db_text'] = 'DB optimized';
$aLanguage['en']['resetActive'] = 'reset active';
$aLanguage['en']['reindex'] = 'reindex';
$aLanguage['en']['rebuildSitemap'] = 'rebuild sitemap';
$aLanguage['en']['SearchDropAll'] = 'remove all';
$aLanguage['en']['rebuildLanguages'] = 'Rebuild linguistic values';

$aLanguage['en']['view_access'] = 'Access logs';
$aLanguage['en']['view_error'] = 'Error logs';
$aLanguage['en']['search_drop_index'] = 'Search index cleared';
$aLanguage['en']['record_update'] = 'update records';
$aLanguage['en']['new_update_msg'] = 'Run the script on the new, not all records are updated';
$aLanguage['en']['sitemap_update_error'] = 'update error';
$aLanguage['en']['sitemap_update_msg'] = 'sitemap updated';
$aLanguage['en']['collision'] = 'Collisions founded "{collisions}"';
$aLanguage['en']['count_records_processed_for_iteration'] = 'Processed for iteration';
$aLanguage['en']['total_count_records_processed'] = 'Total processed';
$aLanguage['en']['count_all_records'] = 'Number of all entries';
$aLanguage['en']['execution_status'] = 'Execution Status';

return $aLanguage;