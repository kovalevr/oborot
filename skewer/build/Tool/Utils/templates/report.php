<?php
/**
 * @var int $iRefreshCountByIteration
 * @var int $iRefreshCount
 * @var int $iTotalCount
 * @var string $sProgress
 */

?>
<table style="width: 100%;">
    <tr>
        <td><b><?=Yii::t('utils', 'count_records_processed_for_iteration');?></b></td>
        <td><b><?=Yii::t('utils', 'total_count_records_processed');?></b></td>
        <td><b><?=Yii::t('utils', 'count_all_records')?></b></td>
        <td><b><?=Yii::t('utils', 'execution_status');?></b></td>
    </tr>
    <tr>
        <td><?=$iRefreshCountByIteration?></td>
        <td><?=$iRefreshCount?></td>
        <td><?=$iTotalCount?></td>
        <td><?=$sProgress?></td>
    </tr>
</table>