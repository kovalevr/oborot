<?php

namespace skewer\build\Tool\Crm\models;
use CanapeCrmApi\ClientLib;
use skewer\base\SysVar;
use skewer\build\Tool\Crm\Api;
use yii\db\ActiveRecord;
use yii\helpers\ArrayHelper;


/**
 * Class DealType
 * @property int $id
 * @property string $name
 * @property boolean $active
 */
class DealType extends ActiveRecord
{

    public $default ;

    public static function tableName()
    {
        return 'crm_deal_types';
    }

    public function rules()
    {
        return [
            [['id','name'], 'required'],
            [['id'], 'integer'],
            [['name'], 'string','max'=> 128],
            [['active'], 'boolean'],

        ];
    }

    /**
     * @return array|\yii\db\ActiveRecord[]
     */
    static function getDealTypesList( $bOnlyActive = false){

        $oQuery = self::find()->indexBy('id');

        if($bOnlyActive)
            $oQuery->where(['active' => 1]);

        return $oQuery->all();
    }

    static function checkList(){
        $aCMSDealTypes = self::getDealTypesList();

        $sDomain = SysVar::get(Api::CRM_SYSVAR_DOMAIN);
        $sToken = SysVar::get(Api::CRM_SYSVAR_TOKEN);

        $aCRMDealTypes = [];
        if ($sDomain && $sToken){
            $oClient = new ClientLib($sDomain, $sToken);
            $aCRMDealTypes = $oClient->disallowSSL()->getDealTypes()->json();
        }
        $aIdCRMDeals = ArrayHelper::map($aCRMDealTypes,'id','id');

        foreach ($aCMSDealTypes as $oDealType){
            if (!in_array($oDealType->id,$aIdCRMDeals)){
                $oDealType->delete();
            }
        }

        foreach ($aCRMDealTypes as $aDealType){
            if (!isset($aCMSDealTypes[$aDealType['id']])) {
                $oDealType = new DealType($aDealType);
                $oDealType->active = 1;
                $oDealType->save();
            }
        }

    }
}