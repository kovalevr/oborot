<?php

/**
 * Created by PhpStorm.
 * User: holod
 * Date: 16.01.2017
 * Time: 11:32
 */

namespace skewer\build\Tool\Crm\view;

use skewer\build\Tool\Crm\models\DealEvent;
use skewer\components\ext\view\FormView;

class DealEventEdit extends FormView
{
    /**
     * @var DealEvent $oDealEvent
     */
    public $oDealEvent;
    /**
     * Выполняет сборку интерфейса
     * @return void
     */
    function build() {

        $this->_form
            ->fieldHide('id')
            ->fieldString('title', \Yii::t('crm', 'name'))
            ->fieldShow('from',\Yii::t('crm', 'from'))
            ->fieldShow('to',\Yii::t('crm', 'to'))
            ->fieldCheck('active', \Yii::t('crm', 'active'))
            ->setValue($this->oDealEvent)
            ->buttonSave('saveDealEvent')
            ->buttonCancel('DealEventList')
        ;

    }
}