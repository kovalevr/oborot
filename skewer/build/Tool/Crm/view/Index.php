<?php

/**
 * Created by PhpStorm.
 * User: holod
 * Date: 16.01.2017
 * Time: 11:32
 */

namespace skewer\build\Tool\Crm\view;

use skewer\build\Tool\Crm\Api;
use skewer\components\ext\view\FormView;

class Index extends FormView
{
    public $sToken;
    public $sTokenEmail;
    public $sMail;
    public $sDomain;
    public $sIntegration;
    public $aValues;
    /**
     * Выполняет сборку интерфейса
     * @return void
     */
    function build() {
        $this->_form
            ->field('email', $this->sMail, 'str')
            ->field('token_email', $this->sTokenEmail, 'str')

            ->field('domain', $this->sDomain, 'str')
            ->field('token', $this->sToken, 'str')

            ->fieldSelect('integration',$this->sIntegration,Api::getCRMIntegrationsList())
            ->setValue($this->aValues)
        ;
        $this->_form
            ->buttonSave('save');

        if ($this->aValues['token'] && $this->aValues['domain'])
            $this->_form
                ->button('dealTypeList',\Yii::t('crm','deal_types_list_title'), 'icon-page')
                ->button('dealEventList',\Yii::t('crm','deal_events_list_title'), 'icon-page');


    }
}