<?php

/**
 * Created by PhpStorm.
 * User: holod
 * Date: 16.01.2017
 * Time: 11:32
 */

namespace skewer\build\Tool\Crm\view;

use CanapeCrmApi\ClientLib;
use skewer\build\Tool\Crm\models\DealType;
use skewer\components\ext\view\FormView;

class DealTypeEdit extends FormView
{
    /**
     * @var DealType $oDealType
     */
    public $oDealType;
    /**
     * Выполняет сборку интерфейса
     * @return void
     */
    function build() {

        $this->_form
            ->fieldHide('id')
            ->fieldString('name', \Yii::t('crm', 'name'))
            ->fieldCheck('active', \Yii::t('crm', 'active'))
            ->setValue($this->oDealType)
            ->buttonSave('saveDealType')
            ->buttonCancel('DealTypeList')
        ;

    }
}