<?php
namespace skewer\build\Tool\Crm;
use CanapeCrmApi\ClientLib;
use CanapeCrmApi\models\Catalog;
use CanapeCrmApi\models\NewDeal;
use skewer\base\orm\Query;
use skewer\base\site\Layer;
use skewer\base\site\Site;
use skewer\base\SysVar;
use skewer\build\Adm\Order\ar\Goods;
use skewer\build\Adm\Order\ar\OrderRow;
use skewer\build\Tool\Crm\models\DealEvent;
use skewer\build\Tool\Crm\models\DealType;
use skewer\components\catalog\GoodsSelector;
use skewer\components\forms\Entity;
use skewer\components\forms\FieldRow;
use skewer\components\forms\Row;
use skewer\components\seo;
use yii\helpers\ArrayHelper;
use yii\helpers\Html;


/**
 * API работы с резевным копированием
 *
 */
class Api {

    const CRM_SYSVAR_TOKEN = 'crm_token';
    const CRM_SYSVAR_DOMAIN = 'crm_domain';

    const CRM_SYSVAR_TOKEN_EMAIL = 'crm_token_email';
    const CRM_SYSVAR_EMAIL = 'crm_email';

    const CRM_SYSVAR_INTEGRATION = 'crm_integration';

    const CRM_EMAIL_INTEGRATION = 'email_integration';
    const CRM_API_INTEGRATION = 'api_integration';

    static public function isCrmInstall(){
        $oInstaller = new \skewer\components\config\installer\Api();
        return $oInstaller->isInstalled('Crm',Layer::TOOL);
    }

    static public function getCRMIntegrationsList(){

        return [
          self::CRM_EMAIL_INTEGRATION => \Yii::t( 'crm', self::CRM_EMAIL_INTEGRATION ),
          self::CRM_API_INTEGRATION => \Yii::t( 'crm', self::CRM_API_INTEGRATION ),
        ];
    }

    /**
     * @param Row $oFormRow
     * @return bool
     */
    static public function needSend2CRM( $oFormRow ){

        return (self::isCrmInstall() && $oFormRow->form_send_crm);
    }

    static public function getCRMFieldsList(){
        return[
            'deal_title' => 'Заголовок сделки',
            'deal_content' => 'Содержание сделки',
            'contact_client' => 'Имя клиента',
            'contact_email' => 'E-mail клиента',
            'contact_phone' => 'Телефон клиента',
            'contact_mobile' => 'Телефон клиента мобильный',
            'deal_event'    => 'Событие',
            'deal_type'    => 'Тип сделки',
        ];
    }

    static public function getDealTypeField4Form($idForm){
        $Res = Query::SelectFrom('crm_link_form')
            ->where('crm_field_alias', 'deal_type' )
            ->where('form_id',$idForm)
            ->asArray()
            ->getOne();

        return $Res? $Res['field_id'] : null;

    }

    static public function getDealEventField4Form($idForm){
        $Res = Query::SelectFrom('crm_link_form')
            ->where('crm_field_alias', 'deal_event' )
            ->where('form_id',$idForm)
            ->asArray()
            ->getOne();

        return $Res? $Res['field_id'] : null;

    }

    static public function getDealTypeList4Form(){

        $aDealTypes = DealType::getDealTypesList( true );

        /**
         * @var DealType  $Type
         */
        foreach( $aDealTypes as $key => $Type){
            $aDealTypes[$key] = $Type->id.':'.$Type->name;
        }

        $sRes = implode(';', $aDealTypes);

        return $sRes;
    }

    static public function getDealEventList4Form(){

        $aDealEvents = DealEvent::getDealEventsList();

        /**
         * @var DealEvent  $Event
         */
        foreach( $aDealEvents as $key => $Event){
            $aDealEvents[$key] = $Event->id.':'.$Event->title;
        }

        $sRes = implode(';', $aDealEvents);

        return $sRes;
    }

    /**
     * @return ClientLib
     */
    static public function getCrmCLientInstance(){

        $sDomain = SysVar::get(self::CRM_SYSVAR_DOMAIN);
        $sToken = SysVar::get(self::CRM_SYSVAR_TOKEN);

        $oCrmClient = new ClientLib($sDomain,$sToken);

        $oCrmClient->disallowSSL();

        return $oCrmClient;
    }

    /**
     * @param Entity $oFormEntity
     * @param null|OrderRow $Order
     * @return NewDeal
     */
    static public function getDealInstance( $oFormEntity, $Order = null ){

        /** @var NewDeal $oCrmDeal */
        $oCrmDeal = new NewDeal();

        $oCrmDeal->setDomain(Site::domain());

        $aData = $oFormEntity->getData();

        $aCrmFields = ArrayHelper::map(
            Query::SelectFrom('crm_link_form')
            ->where(['form_id' => $oFormEntity->getId()])
            ->getAll(),
            'crm_field_alias',
            'field_id'
        );

        $sGoodsList = '';
        if ($Order){
            $aGoodsList = Goods::find()->where('id_order', $Order->id)->asArray()->getAll();

            foreach ($aGoodsList as $aItem){
                $sGoodsList .= sprintf("название: %s, кол-во: %s, цена: %s\r\n", $aItem['title'], $aItem['count'], $aItem['total']);
                $aGood = GoodsSelector::get($aItem['id_goods'], 1);
                $oCrmDeal->addCatalogItem(
                    (new Catalog())
                        ->setIndex(isset($aGood['fields']['article']['value']) ? $aGood['fields']['article']['value'] : '')
                        ->setTitle($aItem['title'])
                        ->setCount($aItem['count'])
                        ->setPrice($aItem['price'])
                        ->setUnits(isset($aGood['fields']['measure']['value']) ? $aGood['fields']['measure']['value'] : '')
                );
            }
        }

        $aFieldsNames = ArrayHelper::map($oFormEntity->getFields(), 'param_id', 'param_name');

        foreach ( $aCrmFields as $key => $idField ) {
            $sData = '';
            $sDealContent = '';
            if (isset($aFieldsNames[$idField]))
                $sData = isset($aData[$aFieldsNames[$idField]])? $aData[$aFieldsNames[$idField]] : '-';
            switch ($key) {
                case 'deal_title' :
                    $oCrmDeal->setDealTitle($sData? : "Сделка с формы '{$oFormEntity->getTitle()}'");
                    break;
                case 'deal_content' :
                    $sDealContent = Html::tag('strong','Сделка с формы:')." '{$oFormEntity->getTitle()}'\r\n".$sGoodsList."\r\n".$sData;

                    break;
                case 'contact_client' :
                    $oCrmDeal->setContactClient($sData);
                    break;
                case 'contact_email' :
                    $oCrmDeal->setContactEmail($sData);
                    break;
                case 'contact_phone' :
                    $oCrmDeal->setContactPhone($sData);
                    break;
                case 'contact_mobile' :
                    $oCrmDeal->setContactPhone($sData);
                    break;
                case 'deal_event' :
                    $oCrmDeal->setEventId($sData);
                    break;
                case 'deal_type' :
                    $oCrmDeal->setDealTypeId($sData);
                    break;
            }

            $sDealContent .= Html::tag('b','Поля с формы:')."\r\n";

            /** @var FieldRow $aField */
            foreach( $oFormEntity->getFields() as $aField ){
                $sDealContent .= $aField->param_title.': '.$aField->param_default."\r\n";
            }

            $oCrmDeal->setDealContent($sDealContent);

        }

        return $oCrmDeal;
    }  

}
