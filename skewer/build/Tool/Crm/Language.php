<?php

$aLanguage = array();

$aLanguage['ru']['tab_name'] = 'CRM';
$aLanguage['ru']['token'] = 'Ключ доступа для API';
$aLanguage['ru']['token_email'] = 'Ключ доступа для интеграции через почту';
$aLanguage['ru']['email'] = 'Email';
$aLanguage['ru']['domain'] = 'Домен';
$aLanguage['ru']['integration'] = 'Тип интеграции';
$aLanguage['ru']['deal_types_list_title'] = 'Список типов сделок';
$aLanguage['ru']['deal_events_list_title'] = 'Список событий';
$aLanguage['ru']['name'] = 'Название';
$aLanguage['ru']['from'] = 'Дата начала';
$aLanguage['ru']['to'] = 'Дата окончания';
$aLanguage['ru']['btn_back'] = 'Назад';
$aLanguage['ru']['active'] = 'Активность';
$aLanguage['ru']['email_integration'] = 'Интеграция посредством почты';
$aLanguage['ru']['api_integration'] = 'Интеграция посредством REST API';

$aLanguage['en']['tab_name'] = 'CRM';
$aLanguage['en']['token'] = 'Access key for API';
$aLanguage['en']['token_email'] = 'Access key for API Email';
$aLanguage['en']['email'] = 'Email';
$aLanguage['en']['domain'] = 'Domain';
$aLanguage['en']['integration'] = 'Integration type';
$aLanguage['en']['deal_types_list_title'] = 'List of deal types';
$aLanguage['en']['deal_events_list_title'] = 'List of events';
$aLanguage['en']['name'] = 'Name';
$aLanguage['en']['from'] = 'Date of the beginning';
$aLanguage['en']['to'] = 'Expiration date';
$aLanguage['en']['btn_back'] = 'Back';
$aLanguage['en']['active'] = 'Active';
$aLanguage['en']['email_integration'] = 'Integration by mail';
$aLanguage['en']['api_integration'] = 'Integration through the REST API';

return $aLanguage;