<?php

namespace skewer\build\Tool\Maps;

use skewer\build\Page\CatalogMaps\Api;
use skewer\build\Tool\Maps\view;
use skewer\components\ext\docked;
use skewer\base\ui\StateBuilder;
use skewer\build\Tool;
use skewer\base\SysVar;


/**
 * Модуль глобальных настроек карт
 * Class Module
 * @package skewer\build\Tool\Maps
 */
class Module extends Tool\LeftList\ModulePrototype {

    protected function actionInit() {

        $oForm = StateBuilder::newEdit();

        $aErrors = [];
        if ( !Api::canShowMap( $aErrors ) && $aErrors )
            $oForm->headText( "<b>". \Yii::t($this->languageCategory,'information') ."</b> " . reset($aErrors)  );

        $oForm
            ->fieldSelect( 'type_map', \Yii::t($this->languageCategory, 'type_map'), Api::getMapProviders(), [], true)
            ->field('default_center', \Yii::t($this->languageCategory, 'default_center'), 'mapSingleMarker')
        ;

        $oForm->setValue(
            [
                'type_map' => SysVar::get($this->languageCategory . '.type_map'),
                'default_center'  => Api::getAddressGeoObjectFormatted( SysVar::get($this->languageCategory . '.default_center'))
            ]
        );

        $oForm
            ->button('settingsYandex', \Yii::t($this->languageCategory, 'settingsYandex'), docked\Api::iconConfiguration)
            ->button('settingsGoogle', \Yii::t($this->languageCategory, 'settingsGoogle'), docked\Api::iconConfiguration)
            ->buttonSave('saveTypeMap');

        $this->setInterface($oForm->getForm());
    }

    /**
     * Сохранение используемого типа карты
     */
    protected function actionSaveTypeMap(){

        $sTypeMap = $this->getInDataVal('type_map', '');
        $sDefault_center = $this->getInDataVal('default_center', '');

        SysVar::set($this->languageCategory . '.type_map', $sTypeMap);

        $sValue = Api::extractGeoObjectIdFromAddress($sDefault_center);

        if ( $sValue === false )
            $sValue = '';

        SysVar::set($this->languageCategory . '.default_center', $sValue);

        $this->actionInit();
    }

    /**
     * Состояние: Редактирование настроек Yandex Maps
     */
    protected function actionSettingsYandex(){

        $this->render(
            new view\YandexSettings()
        );
    }

    /**
     * Состояние: Редактирование настроек Google Maps
     */
    protected function actionSettingsGoogle(){

        $this->render(
            new view\GoogleSettings()
        );
    }

    /**
     * Действие: сохранение настроек Yandex Maps
     */
    protected function actionSaveYandexSettings(){

        $aFormData = $this->get('data', []);

        foreach ($aFormData as $aParamName => $mParamValue) {
            SysVar::set(Api::getSysVarName(Api::providerYandexMap, $aParamName), $mParamValue);
        }

        $this->actionInit();

    }

    /**
     * Действие: сохранение настроек Google Maps
     */
    protected function actionSaveGoogleSettings(){

        $aFormData = $this->get('data', []);

        foreach ($aFormData as $aParamName => $mParamValue) {
            SysVar::set(Api::getSysVarName(Api::providerGoogleMap, $aParamName), $mParamValue);
        }

        $this->actionInit();

    }

}