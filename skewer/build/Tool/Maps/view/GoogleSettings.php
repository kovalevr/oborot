<?php

namespace skewer\build\Tool\Maps\view;

use skewer\base\SysVar;
use skewer\build\Page\CatalogMaps\Api;
use skewer\components\ext\view\FormView;

/**
 * Class GoogleSettings
 * @package skewer\build\Tool\Maps\view
 */
class GoogleSettings extends FormView{

    /**
     * Выполняет сборку интерфейса
     * @return void
     */
    public function build(){

        $this->_form
            ->headText('<h1>' . \Yii::t($this->_module->getCategoryMessage(), 'settingsGoogle') . '</h1>')
            ->fieldString('api_key', \Yii::t($this->_module->getCategoryMessage(), 'api_key'))
            ->field('iconMarkers', \Yii::t($this->_module->getCategoryMessage(), 'iconMarkers'), 'file')
            ->fieldCheck('clusterize', \Yii::t($this->_module->getCategoryMessage(), 'clusterize'))
        ;

        $this->_form
            ->buttonSave('saveGoogleSettings')
            ->buttonBack()
        ;

        $this->_form
            ->setValue([
                'api_key'     => SysVar::get(Api::getSysVarName(Api::providerGoogleMap, 'api_key'), ''),
                'iconMarkers' => SysVar::get(Api::getSysVarName(Api::providerGoogleMap, 'iconMarkers'), ''),
                'clusterize'  => SysVar::get(Api::getSysVarName(Api::providerGoogleMap, 'clusterize'), false),
            ])
        ;

    }

}