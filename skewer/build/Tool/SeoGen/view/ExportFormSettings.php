<?php

namespace skewer\build\Tool\SeoGen\view;

use skewer\build\Tool\SeoGen\Api;
use skewer\components\catalog\Section;
use skewer\components\ext\view\FormView;

class ExportFormSettings extends FormView{

    /**
     * @var string Тип данных
     */
    public $sTypeData = '';

    /**
     * @var array Значения полей формы
     */
    public $aValues = [];

    /**
     * Выполняет сборку интерфейса
     * @return void
     */
    public function build(){

        $this->_form
            ->headText('<h1>' . \Yii::t('SeoGen', 'export_headTitle') .'</h1>')
            ->fieldSelect('data_type', 'Тип данных', Api::getDataTypes(), ['onUpdateAction' => 'updateExportState'])
        ;

        switch ($this->sTypeData){

            case Api::DATATYPE_SECTIONS:

                $this->_form
                    ->fieldString('sectionId', 'Введите Id раздела')
                    ->fieldCheck('enable_add_objects', 'Включить дополнительные объекты')
                    ->fieldCheck('enable_staticContents', 'Включить данные "текст раздела" и "текст раздела 2"')
                ;

                break;

            case Api::DATATYPE_GOODS:

            	$aStructure = ['all' => 'Все каталожные разделы'] + Section::getListWithStructure();
				$aCatalogSections = ['all' => 'Все каталожные разделы'] + Section::getList();
				$aNonCatalogSections = array_diff_key($aStructure, $aCatalogSections);

				//Покрасим элементы, которые не являются каталогом
				foreach ($aStructure as $k=>&$item) {
					if (!isset($aCatalogSections[$k])) {
						$item =  '<i style="color: #b3b3b3">' . $item . '</i>';
					}
				}
				unset($item);

                $this->_form
                    ->fieldMultiSelect('catalog_sections', 'Раздел каталога', $aStructure, [], ['disabledVariants'=> array_keys($aNonCatalogSections)])
                    ->fieldCheck('enable_modification_goods', 'Включить товары модификации')
                ;

                break;

        }

        $this->_form
            ->button('exportRun','Экспортировать', 'icon-reload', 'save',['unsetFormDirtyBlocker' => true])
            ->button('showLastLog', 'Лог последнего запуска', '')
            ->buttonBack()

            ->setValue($this->aValues)
        ;


    }

    public function beforeBuild(){

        // Убираем данные, которые не должны протягиваться между состояниями
        foreach ($this->aValues as $key => &$item) {
            if ( !in_array($key, ['data_type']) )
                unset($this->aValues[$key]);
        }
    }


}