<?php

namespace skewer\build\Tool\SeoGen;

use skewer\base\queue;
use skewer\base\section\Tree;
use skewer\base\section\Visible;
use skewer\base\site\Type;
use skewer\build\Adm\Articles\Seo;
use skewer\build\Adm\FAQ;
use skewer\build\Adm\News;
use skewer\build\Catalog\Goods\SeoGood;
use skewer\build\Catalog\Goods\SeoGoodModifications;
use skewer\build\Page\Main;
use skewer\build\Adm\Gallery;
use skewer\components\catalog\Section;
use skewer\components\import;
use skewer\components\seo\SeoPrototype;
use skewer\helpers\Files;
use yii\helpers\ArrayHelper;
use yii\helpers\StringHelper;


class ExportTask extends queue\Task{

    /** @var import\Config  */
    public $oConfig = null;

    /** @var \skewer\components\import\Logger  */
    public $oLogger = null;

    /** @var XlsWriter */
    public $oProvider = null;


    /** Инициализация задачи */
    public function init(){

        $aArgs = func_get_args();

        try{
            $oTemplate = self::getImportTemplate();

            $this->oLogger = new import\Logger($this->getId(), $oTemplate);
            $this->oLogger->setParam('start', date('Y-m-d H:i:s'));

            $this->oConfig = new import\Config($oTemplate);

            $sSeoDir = PRIVATE_FILEPATH . Api::SEO_DIRECTORY . DIRECTORY_SEPARATOR;

            Files::createFolderPath(Api::SEO_DIRECTORY, true);

            if (!file_exists(Api::getSystemPathExportFile()))
                if (!@touch(Api::getSystemPathExportFile()))
                    throw new \Exception("Не удалось создать файл. Проверьте владельца и права на директорию [{$sSeoDir}]");

            $oProvider = import\Api::getProvider($this->oConfig);
            if ($oProvider instanceof import\provider\Xls)
                $this->oProvider = new XlsWriter($this->oConfig);

            if (isset($aArgs[0]['data_type'])){

                $sDataType = $aArgs[0]['data_type'];

                switch ($sDataType){

                    case Api::DATATYPE_SECTIONS:

                        $this->setConfigVal('sectionId', $aArgs[0]['sectionId']);
                        $this->setConfigVal('bEnableAddObjects', (bool)$aArgs[0]['enable_add_objects']);
                        $this->setConfigVal('bEnableStaticContents', (bool)$aArgs[0]['enable_staticContents']);
                        $this->setConfigVal('sTypeEntity', Main\Seo::className());

                        break;

                    case Api::DATATYPE_GOODS:

                        $this->setConfigVal('sCatalogSections', $aArgs[0]['catalog_sections']);
                        $this->setConfigVal('bEnableModificationGoods', (bool)$aArgs[0]['enable_modification_goods']);
                        $this->setConfigVal('sTypeEntity', SeoGood::className());

                        break;

                }


                $this->setConfigVal('sDataType', $aArgs[0]['data_type']);

            }

            $aHeaderFields = array_values(self::fields4Export($this->getConfigVal('bEnableStaticContents', false)? [] : ['staticContent', 'staticContent2']));
            $this->oProvider->createXlsFile($aHeaderFields);
            $this->oLogger->save();
        }
        catch (\Exception $e){
            $this->fail($e->getMessage());
        }

    }

    /** Востановление задачи */
    public function recovery(){

        $aArgs = func_get_args();

        if (!isset($aArgs[0]['data']))
            throw new \Exception('no valid data');

        /** Собираем конфиг */
        $this->oConfig = new import\Config();
        $this->oConfig->setData(json_decode($aArgs[0]['data'], true));

        $oProvider = import\Api::getProvider($this->oConfig);

        $this->oLogger = new import\Logger($this->getId(), self::getImportTemplate());

        if ($oProvider instanceof import\provider\Xls)
        $this->oProvider = new XlsWriter($this->oConfig);
    }

    /** Заморозка задачи */
    public function reservation(){
        $this->setParams(['data' => $this->oConfig->getJsonData()]);
        $this->oLogger->setParam( 'status', static::stFrozen );
        $this->oLogger->save();
    }

    public function beforeExecute(){
        $this->oLogger->setParam( 'status', static::stProcess );
        $this->oProvider->beforeExecute();
    }


    /** Выполнение очередноой итерации */
    public function execute(){

        $iRowIndex = $this->getConfigVal('row');
        $bError = false;

        $aFields = self::fields4Export();

        switch ($this->getConfigVal('sDataType')){

            case Api::DATATYPE_SECTIONS:
                $aBuffer = $this->executeExportSections();
                $aFields = self::fields4Export($this->getConfigVal('bEnableStaticContents', false)? [] : ['staticContent', 'staticContent2']);

                break;

            case Api::DATATYPE_GOODS:

                if (!Type::hasCatalogModule()){
                    $bError = true;
                    $this->oLogger->setListParam('error_list', 'Не установлен модуль каталога');
                } else{
                    $aBuffer = $this->executeExportGoods();
                }

                break;
        }

        if ($bError){
            $this->setStatus(static::stError);
            return true;
        } elseif ($aBuffer === false){
            $this->setStatus(static::stComplete);
            return true;
        }

        // Урезаем лишние поля
        $aBuffer = array_intersect_key($aBuffer, $aFields);

        $this->oProvider->writeRow($iRowIndex, $aBuffer);
        $this->setConfigVal('row', ++$iRowIndex);

        return true;

    }


    public function afterExecute(){
        $this->setParams(['data' => $this->oConfig->getJsonData()]);
        $this->oProvider->afterExecute();
    }

    public function complete(){
        $this->oLogger->setParam('finish', date('Y-m-d H:i:s'));
        $this->oLogger->setParam('status', static::stComplete);
        $this->oLogger->setParam( 'linkFile', Api::getWebPathExportFile());
        $this->oLogger->save();
    }


    private function fail( $msg ){
        $this->oLogger->setListParam( 'error_list', $msg );
        $this->setStatus(static::stError);
    }

    public function error(){
        if ($this->oLogger){
            $this->oLogger->setParam( 'status', static::stError );
            $this->oLogger->setParam( 'finish', date('Y-m-d H:i:s'));
            $this->oLogger->save();
        }
    }


    /**
     * Получить очередную порцию данных
     * @param string $sTypeEntity - Текущий тип обрабатываемой сущности
     * @param int $iCurrentSectionId - id текущего раздела
     * @param int $iRowIndexEntity - Индекс записи в пределах сущности
     * @param bool|int $iBaseGoodId - id базового товара. используется для выборки товаров-аналогов
     * @return array|bool - вернет массив seo данных или false, если запись не найдена
     */
    public function getChunkData($sTypeEntity, $iCurrentSectionId = 0, $iRowIndexEntity = 0, $iBaseGoodId = false){

        /** @var SeoPrototype $oSeo компонент для получения seo данных */
        if (!class_exists($sTypeEntity) || !(($oSeo = new $sTypeEntity()) instanceof SeoPrototype))
            return false;

        $oSeo->setSectionId($iCurrentSectionId);

        if ($sTypeEntity == SeoGoodModifications::className()){
            if ($iBaseGoodId === false)
                return false;

            $oSeo->setBaseGoodId($iBaseGoodId);
        }


        if (!($aData = $oSeo->getRecordWithinEntityByPosition($iRowIndexEntity)))
            return false;

        $aRow = self::getBlankExportStructure();

        $aRow['id']   = ArrayHelper::getValue($aData, 'id', '');
        $aRow['type'] = ArrayHelper::getValue(Api::getEntities(),$sTypeEntity, '');
        $aRow['url']  = ArrayHelper::getValue($aData, 'url', '');
        $aRow['h1']   = ArrayHelper::getValue($aData,  'h1', '');
        $aRow['visible']        = ArrayHelper::getValue($aData,  'visible', '');
        $aRow['staticContent']  = ArrayHelper::getValue($aData,  'staticContent', '');
        $aRow['staticContent2'] = ArrayHelper::getValue($aData,  'staticContent2', '');


        $aRow['title'] = [
            'value' =>   ArrayHelper::getValue($aData, 'seo.title.value', ''),
            'style' => (!ArrayHelper::getValue($aData, 'seo.title.overriden', true))? Styles::$GREEN : false
        ];

        $aRow['description'] = [
            'value' =>   ArrayHelper::getValue($aData, 'seo.description.value', ''),
            'style' => (!ArrayHelper::getValue($aData, 'seo.description.overriden', true))? Styles::$GREEN : false
        ];

        $aRow['keywords'] = [
            'value' =>   ArrayHelper::getValue($aData, 'seo.keywords.value', ''),
            'style' => (!ArrayHelper::getValue($aData, 'seo.keywords.overriden', true))? Styles::$GREEN : false
        ];

        return $aRow;
    }



    /**
     * Вернет массив id рекурсивно собранных дочерних разделов
     * с учетом сортировки по полю "Позиция"
     * @param $iSectionId - id родительского раздела
     * @param array $aRes
     * @return array
     */
    public static function getAllSubSections($iSectionId, &$aRes = []){
        $aSubSections = Tree::getSubSections($iSectionId,true, true);

        foreach ($aSubSections as $aSubSection) {
            $aRes[] = $aSubSection;
            self::getAllSubSections($aSubSection, $aRes);
        }

        return $aRes;
    }


    /**
     * Выгружаемые поля
     * @param array $aExcludedFields - массив иключенных полей
     * @return array
     */
    public static function fields4Export($aExcludedFields = []){
        $aFields = array(
            'type'       => array(
                'title' => 'Type',
                'width' =>  20
            ),
            'url'        => array(
                'title' => 'URL',
                'width' => 65
            ),
            'h1'    => array(
                'title' => 'Альтернативный заголовок H1',
                'width' => 45
            ),
            'title'       => array(
                'title' => 'TITLE',
                'width' => 65
            ),
            'description' => array(
                'title' => 'DESCRIPTION',
                'width' => 65
            ),
            'keywords'    => array(
                'title' => 'KEYWORDS',
                'width' => 65
            ),
            'staticContent'    => array(
                'title' => 'staticContent',
                'width' => 65
            ),
            'staticContent2'    => array(
                'title' => 'staticContent2',
                'width' => 65
            ),
        );


        foreach ($aFields as $key => &$aValue) {
            if ( in_array($key, $aExcludedFields) )
                unset($aFields[$key]);

        }

        return $aFields;

    }


    /** Вернет массив с ключами выгружаемых данных */
    public static function getBlankExportStructure(){
        return array_fill_keys(array_keys(self::fields4Export()), '');
    }


    /** Имя класса */
    public static function className(){
        return get_called_class();
    }


    /**
     * Шаблон экспорта. Нужен для использования Logger и Config
     * @return \skewer\base\orm\ActiveRecord|import\ar\ImportTemplateRow
     */
    public static function getImportTemplate(){
        return import\ar\ImportTemplate::getNewRow([
            'title'           => 'Шаблон экспорта seo данных',
            'type'            => import\Api::Type_File,
            'provider_type'   => import\Api::ptXLS,
            'source'          => Api::getSystemPathExportFile()
        ]);
    }

    /**
     * Возвращает конфиг текущей задачи
     * @param array $aParam - массив для передачи параметров между итерациями
     * @return array
     */
    public static function getConfig($aParam = []){
        return array(
            'title'        => 'Export разделов(seo)',
            'name'         => 'ExportSeo',
            'class'        => self::className(),
            'parameters'   => $aParam,
            'priority'     => ImportTask::priorityLow,
            'resource_use' => ImportTask::weightLow,
            'target_area'  => 1, // область применения - площадка
        );
    }


    /**
     * Итерация экспорта товаров
     * @return array|bool
     */
    public function executeExportGoods(){

        /** @var bool Флаг, завершения попыток получения данных */
        $bComplete = false;

        /** @var array Массив сущностей. Определяет порядок их обработки */
        $aEntities = [SeoGoodModifications::className()];

        /** @var array Выходной массив */
        $aBuffer = false;

        /** @var array Каталожные разделы */
        $aCatalogSections = StringHelper::explode($this->getConfigVal('sCatalogSections'), ',', true, true);

        if (array_search('all', $aCatalogSections) !== false)
            $aCatalogSections = array_keys(Section::getList());

        while( true ){

            $iRowIndexEntity          = $this->getConfigVal('iRowIndexEntity', 0);
            $iCurrentSectionId        = $this->getConfigVal('iCurrentSectionId', 0);
            $sCurrEntity              = $this->getConfigVal('sTypeEntity');
            $iOffsetSection           = $this->getConfigVal('iOffsetSection', 0);   // смещение раздела внутри выгружаемых каталожных разделов
            $iOffsetBaseGood          = $this->getConfigVal('iOffsetBaseGood', 0);  // смещение базового товара внутри выбранных товаров конкретного раздела
            $bEnableModificationGoods = $this->getConfigVal('bEnableModificationGoods', false);
            $iBaseGoodId              = $this->getConfigVal('iBaseGoodId', false);      // id базового товара

            if ( $sCurrEntity == SeoGood::className() ){

                if ( isset($aCatalogSections[$iOffsetSection]) ){

                    $this->setConfigVal('iCurrentSectionId', $aCatalogSections[$iOffsetSection]);

                    // Если товар не найден
                    if ( ($aBuffer = $this->getChunkData($sCurrEntity, $aCatalogSections[$iOffsetSection], $iOffsetBaseGood) ) === false ){

                        $this->setConfigVal('iOffsetSection', $iOffsetSection + 1);
                        $this->setConfigVal('iOffsetBaseGood', 0);

                    } else{

                        if ( $bEnableModificationGoods ){
                            reset($aEntities);
                            $this->setConfigVal('sTypeEntity', current($aEntities) );
                            $this->setConfigVal('iRowIndexEntity', 0 );
                        }

                        $this->setConfigVal('iBaseGoodId', $aBuffer['id']);
                        $this->setConfigVal('iOffsetBaseGood', $iOffsetBaseGood + 1);

                        break;

                    }

                } else {
                    $bComplete = true;
                    break;
                }


            } elseif ( $bEnableModificationGoods ) {

                if ( ($aBuffer = $this->getChunkData($sCurrEntity, $iCurrentSectionId, $iRowIndexEntity, $iBaseGoodId)) === false ){

                    if ( (($iPos = array_search( $sCurrEntity, $aEntities)) !== false) && isset($aEntities[$iPos + 1]) ){

                        $this->setConfigVal('sTypeEntity', $aEntities[$iPos + 1] );
                        $this->setConfigVal('iRowIndexEntity', 0 );

                    } else
                        $this->setConfigVal('sTypeEntity', SeoGood::className() );

                } else{
                    $this->setConfigVal('iRowIndexEntity', $iRowIndexEntity + 1 );
                    break;
                }

            }

        }


        if ($bComplete)
            return false;

        // Обрезаем лишнее
        $aBuffer = array_intersect_key(
            $aBuffer,
            self::fields4Export()
        );

        return $aBuffer;



    }


    /**
     * Итерация экспорта разделов
     * @return array|bool
     */
    public function executeExportSections(){

        /** @var bool Флаг, завершения попыток получения данных */
        $bComplete = false;

        /** @var array Массив сущностей. Определяет порядок их обработки */
        $aEntities = [Seo::className(), News\Seo::className(), Gallery\Seo::className(), FAQ\Seo::className()];

        /** @var array Выходной массив */
        $aBuffer = false;

        // На каждой итерации цикла происходит выборка одной записи
        // Условия выхода из цикла - запись найдена или все записи обработаны
        while( true ){

            $iOffsetRowEntity      = $this->getConfigVal('iOffsetRowEntity', 0);
            $iCurrentSectionId     = $this->getConfigVal('iCurrentSectionId', 0);
            $sCurrEntity           = $this->getConfigVal('sTypeEntity');
            $iOffsetSection        = $this->getConfigVal('iOffsetSection', 0);
            $iStartSection         = $this->getConfigVal('sectionId', 0);
            $bEnableAddObjects     = $this->getConfigVal('bEnableAddObjects', false);
            $bEnableStaticContents = $this->getConfigVal('bEnableStaticContents', false);


            if ( $sCurrEntity == Main\Seo::className() ){

                $aAllSubSections = self::getAllSubSections( $iStartSection );

                if ( isset($aAllSubSections[$iOffsetSection]) ){

                    $this->setConfigVal('iCurrentSectionId', $aAllSubSections[$iOffsetSection]);
                    $this->setConfigVal('iOffsetSection', $iOffsetSection + 1);

                    if ( ($aBuffer = $this->getChunkData($sCurrEntity, $aAllSubSections[$iOffsetSection], $iOffsetRowEntity) ) === false ){

                        $bComplete = true;
                        break;

                    } else{

                        if ( $bEnableAddObjects ){
                            reset($aEntities);
                            $this->setConfigVal('sTypeEntity', current($aEntities) );
                            $this->setConfigVal('iOffsetRowEntity', 0 );
                        }

                        break;

                    }

                } else {
                    $bComplete = true;
                    break;
                }


            } elseif ( $bEnableAddObjects ) {

                if ( ($aBuffer = $this->getChunkData($sCurrEntity, $iCurrentSectionId, $iOffsetRowEntity)) === false ){

                    if ( (($iPos = array_search( $sCurrEntity, $aEntities)) !== false) && isset($aEntities[$iPos + 1]) ){

                        $this->setConfigVal('sTypeEntity', $aEntities[$iPos + 1] );
                        $this->setConfigVal('iOffsetRowEntity', 0 );

                    } else
                        $this->setConfigVal('sTypeEntity', Main\Seo::className() );

                } else{
                    $this->setConfigVal('iOffsetRowEntity', $iOffsetRowEntity + 1 );
                    break;
                }

            }

        }


        if ($bComplete)
            return false;

        // исключенные из пути разделы красим в красный
        if ( $sCurrEntity == Main\Seo::className() ){

            if ( ArrayHelper::getValue($aBuffer, 'visible') == Visible::HIDDEN_FROM_PATH )
                $this->paint($aBuffer, Styles::$RED);

        }


        // Обрезаем лишнее
        $aBuffer = array_intersect_key(
            $aBuffer,
            self::fields4Export($bEnableStaticContents? [] : ['staticContent', 'staticContent2'])
        );

        return $aBuffer;

    }


    /**
     * Сохраняет значение в конфиг по имени
     * @param string $sName имя параметра
     * @param string $sValue значение
     * @return mixed
     */
    public function setConfigVal($sName, $sValue){
        $this->oProvider->setConfigVal( $sName, $sValue );
    }

    /**
     * Отдает значение из конфига по имени
     * @param string $sName имя параметра (можно вложенное через .)
     * @param string $sDefault значение по умолчанию
     * @return mixed
     */
    public function getConfigVal( $sName, $sDefault = '' ){
        return $this->oProvider->getConfigVal( $sName, $sDefault );
    }

    /**
     * Покрасить строку данных
     * @param $aData - данные
     * @param $aStyle - массив настроек стиля
     */
    private function paint(&$aData, $aStyle){

        foreach ($aData as $key => &$item) {
            if ( !is_array($item) ){
                $item = [
                    'value' => $item,
                    'style' => $aStyle
                ];
            } else
                $item['style'] = $aStyle;

        }

    }


}