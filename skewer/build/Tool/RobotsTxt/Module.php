<?php

namespace skewer\build\Tool\RobotsTxt;

use skewer\base\ft\Editor;
use skewer\base\ui\StateBuilder;
use skewer\build\Tool;
use skewer\build\Tool\Domains;
use skewer\components\ext\docked;
use skewer\components\seo\Service;
use yii\base\UserException;


/**
 * Модуль редактирования файла robots.txt
 * Class Module
 * @package skewer\build\Tool\RobotsTxt
 */
class Module extends Tool\LeftList\ModulePrototype {


    protected function actionInit() {

        $oForm = StateBuilder::newEdit();
        $oForm
            ->headText('<h2>' . \Yii::t('robotstxt', 'field_robots_content') . '</h2>' . \Yii::t('robotstxt', 'subtext'))
            ->field('robots_content', \Yii::t('robotstxt', 'field_robots_content'), Editor::TEXT, [
                'height' => '100%',
                'hideLabel' => true,
            ] );

            if ( Tool\RobotsTxt\Api::getSysVar('content_ovveriden', '0') === '1' )
                $oForm->button('RebuildRobots', \Yii::t('robotstxt', 'rebuildRobots'), docked\Api::iconReload, 'allow_do', ['actionText' => \Yii::t('robotstxt', 'rebuild_warningtext')]);
            else
                $oForm->button('RebuildRobots', \Yii::t('robotstxt', 'rebuildRobots'), docked\Api::iconReload);

            $oForm->buttonSave()
        ;

        $oForm->setValue([
           'robots_content' => ( Api::getSysVar('content_ovveriden', '0') === '1' )
                                ? Api::getSysVar('robots_content', '')
                                : Service::generateDefaultContentRobotsTxtFile( Domains\Api::getMainDomain(), true )
        ]);

        $this->setInterface($oForm->getForm());

    }


    protected function actionSave(){

        $sContent = $this->getInDataVal('robots_content', '');

        if ( preg_match('/(Host|Sitemap)\S*:/i', $sContent) === 1 )
            throw new UserException( \Yii::t('robotstxt', 'subtext') );

        // сохранить данные
        Api::setSysVar('robots_content', $sContent );

        // пересобрать robots.txt
        $bRes = Service::updateRobotsTxt( Domains\Api::getMainDomain() );

        if ( $bRes ){
            Api::setSysVar('content_ovveriden', '1');
            $this->addMessage('',  \Yii::t('robotstxt', 'robots_update_msg') );
        } else
            $this->addError('', \Yii::t('robotstxt', 'robots_update_error') );


        $this->actionInit();

    }

    /**
     * Перестроим роботс
     */
    protected function actionRebuildRobots(){

        Tool\RobotsTxt\Api::setSysVar('content_ovveriden', '0');

        $res = Service::updateRobotsTxt( Domains\Api::getMainDomain() );

        if ($res){
            $this->addMessage( '', \Yii::t('robotstxt', 'robots_update_msg') );
        } else
            $this->addError('', \Yii::t('robotstxt', 'robots_update_error') );

        $this->actionInit();

    }

}