<?php

namespace skewer\build\Tool\GalleryOnPage;


use skewer\base\SysVar;
use skewer\base\ui\StateBuilder;
use skewer\build\Tool\LeftList\ModulePrototype;
use skewer\components\GalleryOnPage\Api;
use skewer\components\GalleryOnPage\GalleryOnPage;

class Module extends ModulePrototype {

    /**
     * Отрисовка формы редактирования
     */
    protected function actionInit() {

        $oGalleryOnPage = new GalleryOnPage();
        $aData = $oGalleryOnPage->getSettings();

        $oForm = StateBuilder::newEdit();

        $oForm->field('items',\Yii::t('GalleryOnPage','items'),'int');
        $oForm->field('margin',\Yii::t('GalleryOnPage','margin'),'int');
        $oForm->field('nav',\Yii::t('GalleryOnPage','nav'),'check');
        $oForm->field('dots',\Yii::t('GalleryOnPage','dots'),'check');
        $oForm->field('autoWidth',\Yii::t('GalleryOnPage','autoWidth'),'check');
        $oForm->field('loop',\Yii::t('GalleryOnPage','loop'),'check');
        $oForm->fieldSelect('slideBy',\Yii::t('GalleryOnPage','slideBy'),[
            '1'=>'1',
            'page'=>'Page'
        ],[],false);

        $oForm->field('shadow',\Yii::t('GalleryOnPage','shadow'),'check');

        /*Если НЕ работает модуль режима адаптивности, скроем параметр адаптивности*/
        $oInstaller = new \skewer\components\config\installer\Api();

        if (!$oInstaller->isInstalled('AdaptiveMode', \skewer\base\site\Layer::PAGE))
            $oForm->field('responsive',\Yii::t('GalleryOnPage','adaptive'),'hide');
        else
            $oForm->field('responsive',\Yii::t('GalleryOnPage','adaptive'),'string');

        $oForm->buttonSave('save');

        $aData['responsive'] = Api::unConvertResponsive($aData['responsive']);

        $oForm->setValue($aData);

        $this->setInterface( $oForm->getForm() );
    }

    /**
     * Сохранение значений
     */
    protected function actionSave(){

        $aData = $this->getInData();

        Api::validateData($aData);

        $aData['responsive'] = Api::convertResponsive($aData['responsive']);

        $oGalleryOnPage = new GalleryOnPage();

        $aDefaults = $oGalleryOnPage->getDefaultValues();

        /*Накладывает на дефолтные сохраняемые значения*/
        foreach ($aDefaults as $key=>&$param) {
            if (isset($aData[$key])) {
                $param = $aData[$key];
            }
        }

        SysVar::set('GalleryOnPageSettings',json_encode($aDefaults));

        $this->addMessage(\Yii::t('GalleryOnPage','success'));
        return $this->actionInit();
    }

}