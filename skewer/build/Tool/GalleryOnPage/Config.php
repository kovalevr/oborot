<?php

use skewer\build\Tool\LeftList;
use skewer\base\site\Layer;

/* main */
$aConfig['name']     = 'GalleryOnPage';
$aConfig['title']    = 'GalleryOnPage';
$aConfig['version']  = '2.000';
$aConfig['description']  = '';
$aConfig['revision'] = '0002';
$aConfig['useNamespace'] = true;
$aConfig['layer']     = Layer::TOOL;
$aConfig['group']     = LeftList\Group::ADMIN;

return $aConfig;
