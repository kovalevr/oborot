<?php

namespace skewer\build\Tool\GalleryOnPage;

use skewer\base\SysVar;
use skewer\components\config\InstallPrototype;
use skewer\components\GalleryOnPage\GalleryOnPage;


class Install extends InstallPrototype {

    public function init() {
        return true;
    }// func

    public function install() {

        $oGalleryOnPage = new GalleryOnPage();

        $aDefaultValues = $oGalleryOnPage->getDefaultValues();

        $aDefaultValues['responsive'] = json_encode($aDefaultValues['responsive']);

        SysVar::set('GalleryOnPageSettings',json_encode($aDefaultValues));

    }// func

    public function uninstall() {

        SysVar::del('GalleryOnPageSettings');

    }// func

}//class
