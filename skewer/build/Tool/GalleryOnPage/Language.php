<?php

$aLanguage = array();

$aLanguage['ru']['tab_name'] = 'Галерея на странице';
$aLanguage['ru']['items'] = 'Количество слайдов';
$aLanguage['ru']['margin'] = 'Отступ между слайдами';
$aLanguage['ru']['nav'] = 'Стрелочки';
$aLanguage['ru']['dots'] = 'Буллиты';
$aLanguage['ru']['autoWidth'] = 'Автоширина слайда';
$aLanguage['ru']['adaptive'] = 'Настройки адаптивности';
$aLanguage['ru']['slideBy'] = 'Листать по';

$aLanguage['ru']['items_fail'] = 'Количество слайдов должно быть больше 0';
$aLanguage['ru']['margin_fail'] = 'Отступы должны быть не меньше 0';
$aLanguage['ru']['adaptive_fail'] = 'Настройки адаптивности не соответствуют патерну';
$aLanguage['ru']['loop'] = 'Зациклить';

$aLanguage['ru']['shadow'] = 'Тень';

$aLanguage['ru']['success'] = 'Значения обновлены';

// ********************************************************************************************************************
// ***************************************************** ENGLISH ******************************************************
// ********************************************************************************************************************

$aLanguage['en']['tab_name'] = 'Gallery on page';
$aLanguage['en']['items'] = 'Items count';
$aLanguage['en']['margin'] = 'Items margin';
$aLanguage['en']['nav'] = 'Navigation';
$aLanguage['en']['dots'] = 'Dots';
$aLanguage['en']['autoWidth'] = 'Autowidth';
$aLanguage['en']['adaptive'] = 'Responsive settings';
$aLanguage['en']['slideBy'] = 'Slide by';

$aLanguage['en']['items_fail'] = 'Slide count must be more than 0';
$aLanguage['en']['margin_fail'] = 'Margin must be more than 0';
$aLanguage['en']['adaptive_fail'] = 'Responsive settings not valid';
$aLanguage['en']['loop'] = 'Loop';
$aLanguage['en']['shadow'] = 'Shadow';

$aLanguage['en']['success'] = 'Updated';


return $aLanguage;
