<?php

use skewer\build\Tool\LeftList;
use skewer\base\site\Layer;

$aConfig['name']     = 'Fonts';
$aConfig['title']    = 'Шрифты';
$aConfig['version']  = '1.0';
$aConfig['description']  = 'Шрифты для сайта';
$aConfig['revision'] = '0001';
$aConfig['useNamespace'] = true;
$aConfig['layer']     = Layer::TOOL;
$aConfig['group']     = LeftList\Group::CONTENT;
$aConfig['languageCategory'] = 'fonts';

return $aConfig;
