<?php

namespace skewer\build\Tool\Fonts;


use skewer\base\site;
use skewer\base\SysVar;
use skewer\components\config\InstallPrototype;
use skewer\components\design\model\Params;
use skewer\components\fonts\Api;


class Install extends InstallPrototype {

    public function init() {
        return true;
    }

    public function install() {
        return Api::setJsonSysVar();
    }// func

    public function uninstall() {
        Api::delCssSysVar();

        /** Восстановление дефолтного шрифта */
        $oParam = Params::findOne(['name' => 'base.userbase.fontfamily.font-family']);
        $oParam->value = $oParam->default_value;
        $oParam->save();
        \Yii::$app->clearAssets();

        return SysVar::del(Api::NAME_SYSVAR_ACTIVE);
    }// func

}//class
