<?php

$aLanguage = array();

$aLanguage ['ru']['Fonts.Tool.tab_name'] = 'Шрифты';
$aLanguage ['ru']['no_description_sysvar'] = 'Не задано описание в таблице sys_vars';
$aLanguage ['ru']['no_data'] = 'Не хватает данных для просмотра шрифтов';
$aLanguage ['ru']['not_save'] = 'Данные не сохранены';
$aLanguage ['ru']['not_file'] = 'Не существует файла';
$aLanguage ['ru']['not_add_css'] = 'Данные не добавлены базу для формирования css-файла';
$aLanguage ['ru']['name'] = 'Наименование';
$aLanguage ['ru']['active'] = 'Активность';
$aLanguage ['ru']['family_fonts'] = 'Семейство шрифтов';
$aLanguage ['ru']['connection'] = 'Подключение';
$aLanguage ['ru']['no_font_found'] = 'Не найден используемый шрифт';
$aLanguage ['ru']['trying_reset_activity_used_font'] = 'Нельзя сбросить активность используемого шрифта. Перейдите в дизайнерский режим и выберите другое семейство шрифта';

$aLanguage ['en']['Fonts.Tool.tab_name'] = 'Fonts';
$aLanguage ['en']['no_description_sysvar'] = 'There is no description in the sys_vars table';
$aLanguage ['en']['no_data'] = 'There is not enough data to view the fonts';
$aLanguage ['en']['not_save'] = 'Data not saved';
$aLanguage ['en']['not_file'] = 'There is no file';
$aLanguage ['en']['not_add_css'] = 'Data is not added to the base for created css-file';
$aLanguage ['en']['name'] = 'Name';
$aLanguage ['en']['active'] = 'Active';
$aLanguage ['en']['family_fonts'] = 'Family fonts';
$aLanguage ['en']['connection'] = 'Connection';
$aLanguage ['en']['no_font_found'] = 'No font found';
$aLanguage ['en']['trying_reset_activity_used_font'] = 'You can not reset the activity of the font you are using. Switch to Design mode and select another font family';

return $aLanguage;