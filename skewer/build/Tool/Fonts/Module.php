<?php

namespace skewer\build\Tool\Fonts;

use skewer\build\Adm;
use skewer\build\Tool;
use skewer\components\fonts\Api;
use yii\base\Exception;
use yii\base\UserException;


/**
 * Настройка дополнительных шрифтов для сайта
 * Class Module
 * @package skewer\build\Tool\Fonts
 */
class Module extends Tool\LeftList\ModulePrototype {

    /**
     * @inheritDoc
     */
    protected function actionInit() {
        
        Api::addFromYmlIntoJson();
        $aFontsFamily = Api::getFamilyFonts();
        
        if (!$aFontsFamily)
            throw new Exception( \Yii::t('fonts','no_description_sysvar') );
        
        $this->render(new Tool\Fonts\view\Index([
            'aFontsFamily' =>$aFontsFamily
        ]));
    }

    protected function actionEdit() {

        $aFamilyFont = $this->get('data');

        if (!isset($aFamilyFont['name']))
            throw new Exception( \Yii::t('fonts','no_data') );

        $aChildrenFont = Api::getCustomizeFonts($aFamilyFont['name']);
        
        $this->render(new Tool\Fonts\view\FontCustomize([
            'aFonts' =>$aChildrenFont,
        ]));
    }

    protected function actionEditActive() {

        $aFamilyFont = $this->get('data');

        if ( !$oUsedFont = Api::getUsedFont() )
            throw new UserException( \Yii::t('fonts','no_font_found') );

        // Попытка изменения активности используемого шрифта
        if ( $oUsedFont->value == $aFamilyFont['name'] )
            throw new UserException( \Yii::t('fonts', 'trying_reset_activity_used_font') );

        $sTemplate = Api::getTempleteCss($aFamilyFont);
        
        $fonts = ($aFamilyFont['active'])?
            Api::insertCss($sTemplate,$aFamilyFont):
            Api::deleteCss($sTemplate);

        if ($fonts !== true)
            throw new Exception( $fonts );

        if (!Api::changeActive($aFamilyFont,$aFamilyFont['active']))
            throw new Exception( \Yii::t('fonts','not_save') );
        
        
    }

}