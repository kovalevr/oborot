<?php

namespace skewer\build\Tool\Fonts\view;

use skewer\components\ext\view\ListView;

class FontCustomize extends ListView
{
    public $aFonts;

    /**
     * Выполняет сборку интерфейса
     * @return void
     */
    function build() {

        $this->_list
            ->fieldString('name', \Yii::t('fonts','name'), ['listColumns' => ['flex' => 3]])
            ->fieldCheck('active', \Yii::t('fonts','active'), ['listColumns' => ['flex' => 1]])
            ->setValue($this->aFonts)
            ->setEditableFields(['active'], 'editActive')
            ->buttonBack();
    }
}