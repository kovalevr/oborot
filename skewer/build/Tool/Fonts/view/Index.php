<?php


namespace skewer\build\Tool\Fonts\view;

use skewer\components\ext\view\ListView;

class Index extends ListView
{
    public $aFontsFamily;
    public $bNotInCluster;

    /**
     * Выполняет сборку интерфейса
     * @return void
     */
    function build()
    {
        $this->_list
            ->fieldString('name',\Yii::t('fonts','family_fonts'),['listColumns' => [ 'flex' => 3 ]])
            ->fieldCheck('active',\Yii::t('fonts','connection'),['listColumns' => [ 'flex' => 1 ]])
            ->setValue($this->aFontsFamily)
            ->buttonRowUpdate('edit');
    }
}