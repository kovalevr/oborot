<?php

namespace skewer\build\Tool\Order;


use skewer\base\ft\Fnc;
use skewer\base\site;
use skewer\build\Design\Zones\Api;
use skewer\build\Tool\LeftList\Group;
use skewer\components\config\InstallPrototype;
use skewer\base\SysVar;

class Install extends InstallPrototype {

    public function init() {
        return true;
    }

    public function install() {

        $idZone = Api::getZoneIdByName(Group::CONTENT,\Yii::$app->sections->main());
        Api::addLabel(\skewer\components\catalog\Api::LANG_GROUP_NAME, $idZone, \Yii::$app->sections->main());

        if (!Fnc::tableHasField('users','reg_date'))
            /*Добавим reg_date в таблицу Users*/
            \Yii::$app->db->createCommand("ALTER TABLE `users` ADD COLUMN `reg_date` datetime NOT NULL")->execute();

        SysVar::set( 'syte_type', site\Type::shop );

        return true;
    }// func

    public function uninstall() {

        if (site\Type::isShop()){
            $this->fail("Нельзя удалить магазин");
        };

        return true;
    }// func

}//class
