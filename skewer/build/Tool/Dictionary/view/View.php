<?php

namespace skewer\build\Tool\Dictionary\view;

use skewer\components\ext\view\ListView;

class View extends ListView
{
    public $aItems;
    public $bBanDelDict;

    function build() {
        $this->_list
            ->fieldShow( 'id', 'id', 'i', ['listColumns.width' => 40] )
            ->fieldString( 'title', \Yii::t('dict', 'title'), ['listColumns.flex' => 1] )
            ->setValue( $this->aItems, $this->onPage, $this->page, $this->total )
            ->setFilterAction('View')

            ->buttonAddNew('ItemEdit', \Yii::t('dict', 'add'))
            ->buttonEdit('FieldList', \Yii::t('dict', 'struct'))
            ->buttonCancel('List', \Yii::t('dict', 'back'));

        if (!$this->bBanDelDict) {
            $this->_list
                ->buttonSeparator( '->' )
                ->buttonDelete('Remove', \Yii::t('dict', 'del'));
        }

        $this->_list
            ->buttonRowUpdate( 'ItemEdit' )
            ->buttonRowDelete( 'ItemRemove' )

            ->setEditableFields(['title'], 'ItemSave')
        ;
        $this->_list->enableDragAndDrop('sort');
    }
}