<?php

$aLanguage = array();

$aLanguage['ru']['SectionSettings.Tool.tab_name'] = 'Разделы';
$aLanguage['ru']['menuIcons'] = 'Иконки пунктов меню';
$aLanguage['ru']['newsDetailLink'] = 'Ссылка "Подробнее" в новостях';
$aLanguage['ru']['hasHideDateInNews'] = 'Скрывать дату в новостях';
$aLanguage['ru']['hasHideDateInArticles'] = 'Скрывать дату в статьях';
$aLanguage['ru']['galleryStatus'] = 'Фотолагерея в новостях';
$aLanguage['ru']['onCheckSizeOpenGraphImage'] = 'Включить проверку размера opengraph - изображений';
$aLanguage['ru']['lock_section_flag'] = 'Использовать сообщение "раздел в стадии наполнения"';
$aLanguage['ru']['lock_section_text'] = 'Сообщение "раздел в стадии наполнения"';
$aLanguage['ru']['lock_section_text_value'] = 'Приносим свои извинения, раздел находится в стадии наполнения.';
$aLanguage['ru']['album_fotorama_flag'] = 'Включить фотораму для фотоальбома';
$aLanguage['ru']['favicon_validate'] = 'Проверять favicon при загрузке';
$aLanguage['ru']['default_img'] = 'Изображение по умолчанию для новостной';
$aLanguage['ru']['type_list_of_sections'] = 'Тип разводки';
$aLanguage['ru']['header_under_image'] = 'Заголовок под изображением';
$aLanguage['ru']['header_on_image'] = 'Заголовок на изображении';
$aLanguage['ru']['data_end_service'] = 'Дата гарантийного обслуживания';
$aLanguage['ru']['warranty_support'] = 'Выводить данные по сроку гарантийной поддержки';
$aLanguage['ru']['min_site_width_for_form'] = 'Минимальная ширина сайта для всплывающей формы';
$aLanguage['ru']['min_site_width_for_form_error'] = 'Некорректное значение параметра "минимальная ширина сайта для всплывающей формы"';
$aLanguage['ru']['warning_global_update_tpl_gallery'] = 'Внимание! Изменения будут применены для всех альбомов на сайте!';
$aLanguage['ru']['hide_adm_copyright'] = 'Отключить копирайты для администратора';

$aLanguage['en']['SectionSettings.Tool.tab_name'] = 'Sections';
$aLanguage['en']['menuIcons'] = 'Icons menu';
$aLanguage['en']['newsDetailLink'] = '"Read more" in the news';
$aLanguage['en']['hasHideDateInNews'] = 'To hide the date in the news';
$aLanguage['en']['hasHideDateInArticles'] = 'To hide the date in the articles';
$aLanguage['en']['galleryStatus'] = 'Photogallery in the news';
$aLanguage['en']['onCheckSizeOpenGraphImage'] = 'Enable check open graph image size';
$aLanguage['en']['lock_section_flag'] = 'Message "Section in development stage"';
$aLanguage['en']['lock_section_text'] = 'Message for development stage';
$aLanguage['en']['lock_section_text_value'] = 'We are sorry. This section in development stage.';
$aLanguage['en']['album_fotorama_flag'] = 'Include Fotorama for photo album';
$aLanguage['en']['favicon_validate'] = 'check favicon on upload';
$aLanguage['en']['default_img'] = 'Default image for news';
$aLanguage['en']['type_list_of_sections'] = 'Type list of sections';
$aLanguage['en']['header_under_image'] = 'Title under the image';
$aLanguage['en']['header_on_image'] = 'Title in the image';
$aLanguage['en']['data_end_service'] = 'Until the end of the warranty service';
$aLanguage['en']['warranty_support'] = 'Provide data on the period of warranty support';
$aLanguage['en']['min_site_width_for_form'] = 'Min site width for popup form';
$aLanguage['en']['min_site_width_for_form_error'] = 'Invalid value for "Min site width for popup form"';
$aLanguage['en']['warning_global_update_tpl_gallery'] = 'Warning! Changes will be applied to all albums on the site!';
$aLanguage['en']['hide_adm_copyright'] = 'Hide copyright for administrator';

return $aLanguage;