<?php

use skewer\build\Tool\LeftList;
use skewer\base\site\Layer;

$aConfig['name']     = 'SiteTester';
$aConfig['title']    = 'Проверка сайта';
$aConfig['version']  = '1.000';
$aConfig['description']  = 'Автоматическое тестирование';
$aConfig['revision'] = '0002';
$aConfig['layer']     = Layer::TOOL;
$aConfig['group']     = LeftList\Group::ADMIN;
$aConfig['useNamespace'] = true;

return $aConfig;
