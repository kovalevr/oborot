<?php
namespace skewer\build\Tool\Patches;

use skewer\base\ui;
use skewer\build\Catalog\Goods\Exception;
use skewer\build\Tool;
use skewer\components\config\UpdateException;
use yii\base\UserException;


class Module extends Tool\LeftList\ModulePrototype {

    protected $aListFields = array('patch_uid', 'install_date', 'description');

    protected function actionInit() {
        $this->actionList();
    }

    protected function actionList() {
        $this->setPanelName(\Yii::t('patches', 'availablepatches'));

        $aPaths = (USECLUSTERBUILD)?Api::getClusterList():Api::getLocalList();

        $aItems = [];

        // но нам надо только за посление пол года
        foreach ($aPaths as $aItem) {
            $oDate = \DateTime::createFromFormat("Y-m-d H:i:s",$aItem['install_date']);

            if ($oDate && $oDate->diff(new \DateTime("now"))->m>6){
                break;
            } else $aItems[] = $aItem;
        }

        $this->render(new Tool\Patches\view\Index([
            "aItems" => $aItems
        ]));
    }

    protected function actionInstallPatchForm() {
        try {

            $this->setPanelName(\Yii::t('patches', 'installPatch'), false);
            $aData = $this->get('data');


            if (!isSet($aData['file']) OR
                empty($aData['file']) OR
                !isSet($aData['patch_uid']) OR
                empty($aData['patch_uid'])
            ) throw new UserException(\Yii::t('patches', 'patchError'));

            /* Относительный путь к директории обновления */

            $aVal = [
                'patch_file' => $aData['file'],
                'patch_uid' => $aData['patch_uid'],
                'status' => ($aData['is_install']) ? \Yii::t('patches', 'installed') . $aData['install_date'] : $aData['install_date'],
                'description' => $aData['description']
            ];

            $this->render(new Tool\Patches\view\InstallPatchForm([
                "aVal" => $aVal,
                "bDescriptionNotEmpty" => !empty($aData['description']),
                "bIsNotInstalled" => !$aData['is_install']
            ]));

        } catch (UserException $e) {
            $this->addError($e->getMessage());
        }

        return psComplete;
    }

    protected function actionInstallPatch() {

        try {

            $aData = $this->get('data');

            if (!isSet($aData['patch_file']) OR empty($aData['patch_file']))
                throw new UpdateException('Wrong parameters!');

            Api::installPatch($aData['patch_file']);

        } catch (UpdateException $e) {

            $this->addError($e->getMessage());
        }

        $this->actionList();

    }
    protected function actionInstallPatches() {

        try {

            $aData = $this->get('data');
            if (isset($aData['items']))
            {
                foreach ($aData['items'] as $aPatch) {
                    if (!$aPatch['is_install']) {
                        Api::installPatch($aPatch['file']);
                    } else
                        $this->addError("Патч ".$aPatch['patch_uid']." был ранее установлен");
                }
            } else
                throw new UpdateException(\Yii::t('patches', 'no_install'));
        } catch (UpdateException $e) {

            $this->addError($e->getMessage());
        }
        $this->actionList();
    }

}
