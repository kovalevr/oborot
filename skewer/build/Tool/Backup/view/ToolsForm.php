<?php
/**
 * Created by PhpStorm.
 * User: holod
 * Date: 12.01.2017
 * Time: 18:13
 */

namespace skewer\build\Tool\Backup\view;

use skewer\components\ext\view\FormView;

class ToolsForm extends FormView
{
    public $aItems;

    /**
     * Выполняет сборку интерфейса
     * @return void
     */
    function build() {
        $this->_form->getForm()->setFields($this->aItems);
        $this->_form->setValue(array());
        $this->_form->buttonSave('saveTools');
        $this->_form->button('init', \Yii::t('backup', 'backToList'), 'icon-cancel', 'init');
    }
}