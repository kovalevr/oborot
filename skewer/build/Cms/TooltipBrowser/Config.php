<?php

use skewer\base\site\Layer;

$aConfig['name']     = 'TooltipBrowser';
$aConfig['title']    = 'Подсказки';
$aConfig['version']  = '1.0';
$aConfig['description']  = 'Подсказки';
$aConfig['revision'] = '0001';
$aConfig['layer']     = Layer::CMS;
$aConfig['languageCategory'] = 'tooltip';

return $aConfig;