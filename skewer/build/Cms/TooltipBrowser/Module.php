<?php

namespace skewer\build\Cms\TooltipBrowser;

use skewer\build\Cms;
use skewer\components\gallery;
use skewer\components\gallery\Profile;
use skewer\base\site_module\Context;


class Module extends Cms\Frame\ModulePrototype {

    protected function actionInit() {

        // подключаем модули
        $this->addChildProcess(new Context('files','skewer\build\Adm\Tooltip\Module',ctModule));


    }

}
