<?php
/**
 * @var int $sessionId
 * @var string $layoutMode
 * @var string $moduleDir
 * @var string $dictVals
 * @var string $ver
 * @var string $lang
 *
 * @var \yii\web\View $this
 */

use skewer\base\site\Layer;use skewer\build\Cms;
use skewer\components\design\Design;
use skewer\base\site\Site;

$bundle = skewer\build\Cms\Frame\Asset::register($this);

$this->beginPage()
?><!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <title><?

        if ($layoutMode == 'fileBrowser' or $layoutMode == 'designFileBrowser')
            echo \Yii::t('adm', 'file_page_title');
        elseif ( $layoutMode == 'galleryBrowser' )
            echo \Yii::t('gallery', 'gallery_browser_title');
        elseif ( $layoutMode == 'editorMap' )
            echo Yii::t('editorMap', 'mapPanelTitle');
        else
            echo \Yii::t('adm', 'admin_page_title');

        ?> - <?= Site::getSiteTitle() ?></title>

    <?
        //todo Перенести подключение внутрь модуля
        if ( ($layoutMode == 'editorMap') && (Yii::$app->register->moduleExists( Cms\EditorMap\Module::getNameModule(), Layer::CMS )) )
            Cms\EditorMap\Asset::register( Yii::$app->view );
    ?>
    <link rel="shortcut icon" href="<?= Design::get('page','favicon') ?>" type="image/png" />

    <script type="text/javascript">
        var sessionId = '<?= $sessionId ?>';
        var layoutMode = '<?= $layoutMode ?>';
        var lang = '<?= $lang ?>';
        var dict = <?= $dictVals ?>;

        var rootPath = '<?= $bundle->baseUrl.'/js' ?>';
        var extJsDir = '<?= $this->getAssetManager()->getBundle(skewer\libs\ext_js\Asset::className())->baseUrl ?>';
        var pmDir = '<?= $this->getAssetManager()->getBundle(skewer\components\ext\Asset::className())->baseUrl ?>';
        var ckedir = '<?= $this->getAssetManager()->getBundle(\skewer\libs\CKEditor\Asset::className())->baseUrl ?>';

    </script>

    <?php $this->head() ?>

</head>
<body>

<?php $this->beginBody() ?>
    <div id="js_admin_preloader" class="admin-preloader">
        <img src="<?= $bundle->baseUrl ?>/img/preloader.gif" />

    </div>

    <form id="history-form" class="x-hide-display">
        <input type="hidden" id="x-history-field" />
        <iframe id="x-history-frame"></iframe>
    </form>

<?php $this->endBody() ?>

</body>
</html>

<?php $this->endPage() ?>
