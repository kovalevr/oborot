<?php

namespace skewer\build\Cms\Header;

use skewer\build\Cms;
use skewer\base\site_module\Context;
use skewer\components\auth\CurrentAdmin;


/**
 * Class Module
 * @package skewer\build\Cms\Header
 */
class Module extends Cms\Frame\ModulePrototype {

    protected function actionInit() {

        $this->addInitParam('renderData' ,[
            'href' => '/admin/',
            'logoImg' => $this->getModuleWebDir().'/img/canape_cms_logo.png'
        ]);

        // добавить панель авторизации
        $this->addChildProcess(new Context('auth','skewer\build\Cms\Auth\Module',ctModule,array()));

        $this->addChildProcess(new Context('lang','skewer\build\Cms\Lang\Module',ctModule,array()));

        $this->addChildProcess(new Context('messages','skewer\build\Cms\Messages\Module',ctModule,array()));

        if ( CurrentAdmin::isSystemMode() )
            $this->addChildProcess(new Context('cache','skewer\build\Cms\Cache\Module',ctModule,array()));

        if ( \Yii::$app->register->moduleExists('Search', 'Cms') )
            $this->addChildProcess(new Context('search','skewer\build\Cms\Search\Module',ctModule,array()));

        return psComplete;

    }// func

}// class
