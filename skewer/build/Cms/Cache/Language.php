<?php

$aLanguage = array();

$aLanguage['ru']['Cache.Cms.tab_name'] = 'Сброс кэша';
$aLanguage['ru']['drop_cache_act'] = 'Сбросить кэш';
$aLanguage['ru']['drop_cache_text'] = 'Кэш сброшен';
$aLanguage['ru']['cache_flag_on'] = 'Сбрасывать кэш';
$aLanguage['ru']['cache_flag_off'] = 'Не сбрасывать кэш';

$aLanguage['en']['Cache.Cms.tab_name'] = 'Cache dropper';
$aLanguage['en']['drop_cache_act'] = 'Drop cache';
$aLanguage['en']['drop_cache_text'] = 'Cache was dropped';
$aLanguage['en']['cache_flag_on'] = 'Unset cache';
$aLanguage['en']['cache_flag_off'] = 'Do not unset cache';

return $aLanguage;