<?php

namespace skewer\build\Cms\Cache;


use yii\web\AssetBundle;

class Asset extends AssetBundle {
    public $sourcePath = '@skewer/build/Cms/Cache/web';
}