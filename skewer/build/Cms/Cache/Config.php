<?php

use skewer\base\site\Layer;

$aConfig['name']     = 'Cache';
$aConfig['title']    = 'Сброс кэша';
$aConfig['version']  = '1.0';
$aConfig['description']  = 'Модуль для быстрого доступа к функциональности сброса кэша';
$aConfig['revision'] = '0001';
$aConfig['layer']     = Layer::CMS;

return $aConfig;