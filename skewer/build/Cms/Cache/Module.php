<?php

namespace skewer\build\Cms\Cache;

use skewer\build\Cms;


/**
 * Модуль выводит кнопку сброса кэша в интерфейс и вызывает соответствующую функциональность
 */
class Module extends Cms\Frame\ModulePrototype {

    protected function actionInit() {

        $this->setModuleLangValues(['drop_cache_act']);


        return psComplete;

    }

    protected function actionDropCache() {

        \Yii::$app->router->updateModificationDateSite();
        \Yii::$app->rebuildRegistry();
        \Yii::$app->rebuildCss();
        \Yii::$app->rebuildLang();
        \Yii::$app->clearParser();

        $this->addMessage(\Yii::t('cache', 'drop_cache_text'));

    }

}
