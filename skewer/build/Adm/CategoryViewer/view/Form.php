<?php

/**
 * Created by PhpStorm.
 * User: holod
 * Date: 19.12.2016
 * Time: 15:19
 */
namespace skewer\build\Adm\CategoryViewer\view;
use skewer\components\ext\view\FormView;
use skewer\components\gallery\Profile;

class Form extends FormView
{
    public $bShowIcons;
    public $aData;
    /**
     * Выполняет сборку интерфейса
     * @return void
     */
    function build() {
        $this->_form
            ->fieldCheck( 'category_parent', \Yii::t('categoryViewer', 'param_parent') )
            ->fieldCheck( 'category_show', \Yii::t('categoryViewer', 'param_show') )
            ->fieldGallery('category_img', \Yii::t('categoryViewer', 'param_image'), Profile::getDefaultId(Profile::TYPE_CATEGORYVIEWER))
            ->fieldInt( 'category_from', \Yii::t('categoryViewer', 'param_from'), ['minValue' => 0] )
            ->fieldWysiwyg('category_description', \Yii::t('categoryViewer','param_description'), 350);
        if ($this->bShowIcons) {
            $this->_form->field('category_icon', \Yii::t('categoryViewer', 'param_icon'), 'imagefile');
        }
        $this->_form
            ->setValue( $this->aData )
            ->buttonSave()
            ->buttonBack('show')
            ->buttonSeparator()
            ->buttonConfirm('setRecursive',\Yii::t('categoryViewer','setRecursive'),\Yii::t('categoryViewer','setRecursiveConfirm'))
            ->buttonConfirm('unsetRecursive',\Yii::t('categoryViewer','unsetRecursive'),\Yii::t('categoryViewer','unsetRecursiveConfirm'));
    }
}