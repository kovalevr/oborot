<?php

namespace skewer\build\Adm\CategoryViewer;


use skewer\base\section\Tree;
use skewer\build\Adm;
use skewer\base\section\Parameters;
use skewer\base\ui;
use skewer\base\SysVar;
use skewer\components\gallery\Profile;
use yii\base\UserException;
use yii\helpers\ArrayHelper;


class Module extends Adm\Tree\ModulePrototype {

    public $category_parent = 0;
    public $category_show = 0;
    public $category_img = '';
    public $category_from = 0;
    public $category_icon = '';

    /**
     * @var string Описание раздела
     */
    public $category_description = '';

    public function actionInit() {
        $this->category_icon = (string)Parameters::getValByName($this->sectionId(), Parameters::settings, 'category_icon');
        $this->category_description = Parameters::getShowValByName($this->sectionId(), $this->getConfigParam('param_group'), 'category_description');

        $this->actionShow();
    }


    protected function actionShow() {
        $aData = array(
            'category_parent' => $this->category_parent,
            'category_show' => $this->category_show,
            'category_img' => $this->category_img,
            'category_from' => $this->category_from,
            'category_icon' => $this->category_icon,
            'category_description' => $this->category_description
        );
        $this->render(new view\Form([
            'bShowIcons' => SysVar::get('Menu.ShowIcons'),
            'aData' => $aData
        ]));
    }

    /**
     * Рекурсивно выставляет всем потомкам вывод категорий
     */
    protected function actionSetRecursive(){

        $aSubSections = Tree::getAllSubsection($this->sectionId());

        $aParamData = [
            'name' => 'category_parent',
            'title' => 'categoryViewer.category_parent',
            'value' => 1,
            'group' => 'CategoryViewer',
            'parent' => $this->sectionId(),
            'access_level'=>0,
            'show_val'=>''
        ];

        Parameters::addParam($aParamData);

        foreach($aSubSections as $iSectionId) {

            $aParamData = [
                'name' => 'category_show',
                'title' => 'categoryViewer.param_show',
                'value' => 1,
                'group' => 'CategoryViewer',
                'parent' => $iSectionId,
                'access_level'=>0,
                'show_val'=>''
            ];

            Parameters::addParam($aParamData);

            $aParamData = [
                'name' => 'category_parent',
                'title' => 'categoryViewer.category_parent',
                'value' => 1,
                'group' => 'CategoryViewer',
                'parent' => $iSectionId,
                'access_level'=>0,
                'show_val'=>''
            ];

            Parameters::addParam($aParamData);

        }

        $this->category_parent = 1;

        $this->actionShow();
    }

    protected function actionUnsetRecursive(){

        $aSubSections = Tree::getAllSubsection($this->sectionId());

        $aParamData = [
            'name' => 'category_parent',
            'title' => 'categoryViewer.category_parent',
            'value' => 0,
            'group' => 'CategoryViewer',
            'parent' => $this->sectionId(),
            'access_level'=>0,
            'show_val'=>''
        ];

        Parameters::addParam($aParamData);

        foreach($aSubSections as $iSectionId) {

            $aParamData = [
                'name' => 'category_show',
                'title' => 'categoryViewer.param_show',
                'value' => 0,
                'group' => 'CategoryViewer',
                'parent' => $iSectionId,
                'access_level'=>0,
                'show_val'=>''
            ];

            Parameters::addParam($aParamData);

            $aParamData = [
                'name' => 'category_parent',
                'title' => 'categoryViewer.category_parent',
                'value' => 0,
                'group' => 'CategoryViewer',
                'parent' => $iSectionId,
                'access_level'=>0,
                'show_val'=>''
            ];

            Parameters::addParam($aParamData);

        }

        $this->category_parent=0;

        $this->actionShow();
    }

    protected function actionSave() {

        $aData = $this->getInData();

        $sGroupName = $this->getConfigParam('param_group');

        // на случай если не найден параметр.
        if ( !$sGroupName )
            throw new UserException( 'Can not find group name' );

        $this->category_parent  = ArrayHelper::getValue($aData, 'category_parent', '');
        $this->category_show    = ArrayHelper::getValue($aData, 'category_show', '');
        $this->category_img     = ArrayHelper::getValue($aData, 'category_img',  '');
        $this->category_from    = ArrayHelper::getValue($aData, 'category_from', '');
        $this->category_icon    = ArrayHelper::getValue($aData, 'category_icon', '');
        $this->category_description = ArrayHelper::getValue($aData, 'category_description', '');

        Parameters::setParams( $this->sectionId(), $sGroupName, 'category_parent', $this->category_parent);
        Parameters::setParams( $this->sectionId(), $sGroupName, 'category_show', $this->category_show);
        Parameters::setParams( $this->sectionId(), $sGroupName, 'category_img', $this->category_img);
        Parameters::setParams( $this->sectionId(), $sGroupName, 'category_from', $this->category_from);
        Parameters::setParams( $this->sectionId(), $sGroupName, 'category_description', null, $this->category_description);

        if ( SysVar::get('Menu.ShowIcons'))
            Parameters::setParams( $this->sectionId(), Parameters::settings, 'category_icon', $this->category_icon);

        $oTree = Tree::getSection($this->sectionId());
        $oTree->last_modified_date = date( "Y-m-d H:i:s", time() );
        $oTree->save();


        $this->actionShow();
    }


} 