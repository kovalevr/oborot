<?php

namespace skewer\build\Adm\FAQ;


use skewer\base\orm\state\StateSelect;
use skewer\components\i18n\Languages;
use skewer\components\i18n\ModulesParams;
use skewer\base\ui;
use skewer\build\Adm;
use skewer\components\seo;
use yii\base\UserException;
use yii\helpers\ArrayHelper;
use skewer\build\Adm\FAQ;

/**
 * Class Module
 * @package skewer\build\Adm\FAQ
 */
class Module extends Adm\Tree\ModulePrototype {

    // число элементов на страницу
    public  $onAdmPage = 20;

    // текущий номер страницы ( с 0, а приходит с 1 )
    protected $iPage = 0;

    protected $iStatusFilter = Api::statusNew;

    protected $sLanguageFilter = '';

    private $paramKeys = [
        'title_admin', 'content_admin',
        'title_user', 'content_user', 'onNotif',
        'notifTitleApprove', 'notifContentApprove', 'notifTitleReject',
        'notifContentReject'
    ];

    /**
     * Метод, выполняемый перед action меодом
     * @throws UserException
     */
    protected function preExecute() {

        // номер страницы
        $this->iPage = $this->getInt('page');

        $this->iStatusFilter = $this->get('filter_status', false);

        $this->sLanguageFilter = $this->get('filter_language', \Yii::$app->language);
    }

    /**
     * Установка служебных данных
     * @param ui\state\BaseInterface $oIface
     */
    protected function setServiceData(ui\state\BaseInterface $oIface ) {

        // установить данные для передачи интерфейсу
        $oIface->setServiceData( array(
            'page' => $this->iPage,
            'filter_status' => $this->iStatusFilter,
            'filter_language' => $this->sLanguageFilter,
        ) );

    }

    protected function actionInit(){
        $this->actionList();
    }

    /**
     * list
     */
    protected function actionList(){

        /** @var StateSelect $aItems */
        $aItems = ar\FAQ::find();
        if ($this->iStatusFilter !== false){
            $aItems = $aItems->where('status', $this->iStatusFilter);
        }

        $iCount = 0;
        $aItems = $aItems
            ->where('parent', $this->sectionId())
            ->order('date_time', 'DESC')
            ->setCounterRef( $iCount )
            ->limit($this->onAdmPage, $this->iPage * $this->onAdmPage)
            ->asArray()
            ->getAll();

        $this->render(
            new view\Index([
                'filterStatus' => $this->iStatusFilter,
                'items' => $aItems,
                'page' => $this->iPage,
                'onPage' => $this->onAdmPage,
                'total' => $iCount
            ])
        );
    }

    /**
     * edit
     */
    protected function actionEdit(){
        $this->showForm($this->getInDataValInt('id'));
    }

    /**
     * new
     */
    protected function actionNew(){
        $this->showForm();
    }

    /**
     * Форма редактирования
     * @param null $id
     */
    private function showForm($id = null){
        /**
         * @var $oFAQRow ar\FAQRow
         */
        if ($id){
            $oFAQRow = ar\FAQ::find($id);
        }else{
            $oFAQRow = ar\FAQ::getNewRow(array('status' => Api::statusNew));
            $oFAQRow->date_time = date('Y-m-d H:i:s');
        }

        $this->render( new view\Form([
                'item' => $oFAQRow,
                'sectionId' => $this->sectionId()
            ])
        );
    }

    /**
     * save
     */
    protected function actionSave(){

        $aData = $this->getInData();
        if( !isset($aData['parent']) )
            $aData['parent'] = $this->sectionId();

        // перекрытие статуса
        $iStatus = $this->get( 'setStatus', null );
        if ( !is_null( $iStatus ) )
            $aData['status'] = $iStatus;

        //todo Вытащено из skewer\build\Adm\FAQ::saveItem
        //todo Переписать этот участок и метод saveItem
        $id = isset($aData['id'])?$aData['id']:false;
        if ($id){
            $oFAQRow = ar\FAQ::find($id);
            $aOldAttributes = $oFAQRow->getData();
        }else{
            $oFAQRow = ar\FAQ::getNewRow();
            $aOldAttributes = $oFAQRow->getData();
        }

        $iId = Api::saveItem($aData);

        /** @var FAQ\ar\FAQRow $aItem */
        $aItem = Adm\FAQ\ar\FAQ::find($iId);

        if (seo\Service::$bAliasChanged)
            $this->addMessage(\Yii::t('tree','urlCollisionFlag',['alias'=>$aItem->alias]));

        $oSearch = new Search();
        $oSearch->updateByObjectId($iId);

        // сохранение SEO данных
        seo\Api::saveJSData(
            new FAQ\Seo(ArrayHelper::getValue($aOldAttributes, 'id', 0), $this->sectionId(), $aOldAttributes),
            new FAQ\Seo($aItem->id, $this->sectionId(), $aItem->getData()),
            $aData, $this->sectionId()
        );

        seo\Api::setUpdateSitemapFlag();

        $this->actionList();
    }

    /**
     * delete
     */
    protected function actionDelete(){
        $id = $this->getInDataValInt('id');
        if ($id){
            $oFaqRow = Adm\FAQ\ar\FAQ::findOne(['id' => $id]);
            $oFaqRow->delete();
            $oSearch = new Search();
            $oSearch->deleteByObjectId($id);
        }
        $this->actionList();
    }


    /**
     * Форма настроек модуля
     */
    protected function actionSettings(){

        $aLanguages = Languages::getAllActive();
        $aLanguages = ArrayHelper::map($aLanguages, 'name', 'title');

        $aModulesData = ModulesParams::getByModule('faq', $this->sLanguageFilter);
        $this->setInnerData('languageFilter', $this->sLanguageFilter);

        $aItems = [];
        $aItems['info'] = \Yii::t('faq', 'head_mail_text', [\Yii::t('app', 'site_label'), \Yii::t('app', 'url_label')]);

        foreach( $this->paramKeys as $sKey ){
            $aItems[$sKey] = ArrayHelper::getValue($aModulesData, $sKey, '');
        }

        $this->render(
            new view\Settings([
                'items' => $aItems,
                'languages' => $aLanguages,
                'languageFilter' => $this->sLanguageFilter
            ])
        );
    }
    
    /**
     * Сохраняем настройки формы
     */
    protected function actionSaveSettings(){

        $aData = $this->getInData();

        $sLanguage = $this->getInnerData('languageFilter');
        $this->setInnerData('languageFilter', '');

        if ($sLanguage){
            foreach( $aData as $sName => $sValue ){

                if (!in_array($sName, $this->paramKeys))
                    continue;

                ModulesParams::setParams( 'faq', $sName, $sLanguage, $sValue);
            }
        }

        $this->actionInit();
    }

}
