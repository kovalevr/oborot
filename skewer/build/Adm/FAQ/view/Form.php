<?php
/**
 * Created by PhpStorm.
 * User: holod
 * Date: 18.11.2016
 * Time: 11:46
 */

namespace skewer\build\Adm\FAQ\view;

use skewer\build\Adm\FAQ\Api;
use skewer\components\ext\view\FormView;
use skewer\build\Adm\FAQ\ar\FAQRow;
use skewer\build\Adm\FAQ;
use skewer\components\seo;

class Form extends FormView
{

    /** @var FAQRow */
    public $item;

    public $sectionId;

    /**
     * Выполняет сборку интерфейса
     * @return void
     */
    function build() {
        // добавление кнопок
        $this->_form
            ->buttonSave('save')
            ->buttonCancel('list');

        if ($this->item->id) {
            if ($this->item->status == Api::statusNew) {
                $this->_form
                    ->buttonSeparator('-')
                    // одобрить
                    ->button('save', \Yii::t('faq', 'approve'), 'icon-commit', 'save', [
                        'addParams' => ['setStatus' => Api::statusApproved],
                        'unsetFormDirtyBlocker' => true
                    ])
                    // отклонить
                    ->button('save', \Yii::t('faq', 'reject'), 'icon-stop', 'save', [
                        'addParams' => ['setStatus' => Api::statusRejected],
                        'unsetFormDirtyBlocker' => true
                    ]);

            }
            // кнопка удаления
            $this->_form
                ->buttonSeparator('->')
                ->button('delete', \Yii::t('adm', 'del'), 'icon-delete', 'list');
        }
        /** Добавим поля */
        $this->_form
            ->fieldHide('id', 'id')
            ->fieldString('name', \Yii::t('faq', 'name'))
            ->fieldString('email', \Yii::t('faq', 'email'))
            ->fieldString('city', \Yii::t('faq', 'city'))
            ->field('date_time', \Yii::t('faq', 'date_time'), 'datetime')
            ->field('content', \Yii::t('faq', 'content'), 'text')
            ->field('answer', \Yii::t('faq', 'answer'), 'wyswyg', ['height'=> 200])
            ->fieldSelect('status', \Yii::t('faq', 'status'), Api::getStatusList(), [], false)

            ->fieldString('alias', \Yii::t('faq', 'alias'));

        $this->_form->setValue($this->item->getData());


        // добавление SEO блока полей
        seo\Api::appendExtForm( $this->_form, new FAQ\Seo($this->item->id, $this->sectionId, $this->item->getData()), $this->sectionId, ['seo_gallery']);
    }
}