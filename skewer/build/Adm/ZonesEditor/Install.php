<?php

namespace skewer\build\Adm\ZonesEditor;

use skewer\components\config\InstallPrototype;

/**
 * Class Install
 * @package skewer\build\Adm\ZonesEditor
 */
class Install extends InstallPrototype {

    public function init() {
        return true;
    }// func

    public function install() {
        return true;
    }// func

    public function uninstall() {
        return true;
    }// func

}//class