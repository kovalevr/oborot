<?php

namespace skewer\build\Adm\Slider;


class Api {

    /**
     * Вернёт список типов навигации
     * @return array
     */
    public static function getNavigations(){
        return array(
            'dots' => \Yii::t('slider', 'fotorama_nav_dots'),
            'thumbs' => \Yii::t('slider', 'fotorama_nav_thumbs'),
            'false' => \Yii::t('slider', 'fotorama_nav_disable')
        );
    }

    /**
     * Вернёт список вариантов отображения стрелок
     * @return array
     */
    public static function getArrows(){
        return array(
            'always' => \Yii::t('slider', 'fotorama_arrows_always'),
            'true' => \Yii::t('slider', 'fotorama_arrows_hover'),
            'false' => \Yii::t('slider', 'fotorama_arrows_disable')
        );
    }

    /**
     * Вернёт список типов анимации
     * @return array
     */
    public static function getTransitions(){
        return array(
            'slide'     => \Yii::t('slider', 'fotorama_transition_slide_effect'),
            'crossfade' => \Yii::t('slider', 'fotorama_transition_crossfade_effect'),
            'dissolve'  => \Yii::t('slider', 'fotorama_transition_dissolve_effect')
        );
    }

}