<?php

namespace skewer\build\Adm\Slider;

use skewer\base\ft\Editor;
use skewer\base\section\params\Type;
use skewer\libs\jquery;
use skewer\base\orm\Query;
use skewer\build\Adm;
use skewer\build\Tool;
use skewer\build\Cms;
use skewer\base\site_module\Context;
use skewer\base\site\Layer;
use skewer\base\site_module;
use yii\base\UserException;


class Module extends Cms\Tabs\ModulePrototype implements site_module\SectionModuleInterface {

    const section = Layer::ADM;
    const tool = Layer::TOOL;

    public $sectionId = 0;

    /**
     * Отдает режим работы модуля
     * @return string
     */
    protected function getWorkMode() {
        return self::section;
    }

    protected function isSectionMode() {
        return $this->getWorkMode()===self::section;
    }

    protected function isToolMode() {
        return $this->getWorkMode()===self::tool;
    }

    function sectionId() {
        return $this->sectionId;
    }

    public function __construct(Context $oContext ) {
        parent::__construct( $oContext );
        $oContext->setModuleLayer( 'Adm' );
        $oContext->setModuleWebDir( '/skewer/build/Adm/Slider' );
    }

    protected $iCurrentBanner = 0;
    protected $iCurrentSlide = 0;

    /** @var int Параметр из раздела */
    protected $currentBanner = 0;


    protected function preExecute() {

        if ( $this->currentBanner )
            $this->iCurrentBanner = $this->currentBanner;

    }


    protected function actionInit() {

        // вывод списка
        if ( $this->currentBanner )
            $this->actionSlideList();
        else
            $this->actionBannerList();

        $this->addCssFile(\Yii::$app->getAssetManager()->getBundle(Asset::className())->baseUrl.'/css/SlideShower.css');
        $this->addJsFile(\Yii::$app->getAssetManager()->getBundle(Asset::className())->baseUrl.'/js/SlideLoadImage.js');
        $this->addJsFile(\Yii::$app->getAssetManager()->getBundle(Asset::className())->baseUrl.'/js/SlideShower.js');

    }


    /**
     * Список баннеров
     */
    protected function actionBannerList() {
        $this->iCurrentBanner = 0;

        $aItems = Banner::find()->getAll();

        $this->setInnerData('currentBanner', false);

        $this->render(new view\Index([
            'items' => $aItems
        ]));
    }


    /**
     * Форма редактирования баннера
     */
    protected function actionEditBannerForm() {

        $iBannerId = $this->getInnerData('currentBanner');

        $oBannerRow = $iBannerId ? Banner::find($iBannerId) : Banner::getBlankRow();

        $this->render(new view\editBannerForm([
            'oItem' => $oBannerRow,
            'iBannerId' => $iBannerId
        ]));

        return psComplete;
    }


    /**
     * Добавление/обновление баннера
     * todo отлов ошибок
     */
    protected function actionSaveBanner() {

        try {

            $aData = $this->getInData();
            $aData['last_modified_date'] = date( "Y-m-d H:i:s", time() );

            $oRow = Banner::getNewRow();
            $oRow->setData( $aData );

            $oRow->save();

            if ( $oRow->getError() )
                throw new \Exception( $oRow->getError() );

            $this->actionBannerList();

        } catch ( \Exception $e ) {
            $this->addError( $e->getMessage() );
        }

    }


    /**
     * Удаление баннера
     */
    protected function actionDelBanner() {

        try {

            $aData = $this->getInData();

            $iBannerId = iSset( $aData['id'] ) ? $aData['id'] : false;

            if ( !$iBannerId )
                throw new \Exception("not found banner id=$iBannerId");

            $oRow = Banner::find( $iBannerId );

            if ( !$oRow )
                throw new \Exception("not found banner id=$iBannerId");

            // удаление слайдов от банера
            Slide::removeBanner( $iBannerId );

            $oRow->delete();

            \Yii::$app->router->updateModificationDateSite();

            $this->actionBannerList();

        } catch ( \Exception $e ) {

            $this->addError( $e->getMessage() );
        }

    }


    /**
     * Список слайдов для баннера
     */
    protected function actionSlideList() {

        $aData = $this->getInData();
        if ( !$this->iCurrentBanner && isSet($aData['id']) && $aData['id'] )
            $this->iCurrentBanner = $aData['id'];

        $this->iCurrentSlide = 0;

        $this->setInnerData('currentBanner', $this->iCurrentBanner);
        if ( !$this->iCurrentBanner )
            throw new \Exception( \Yii::t('slider', 'noFindBanner') );

        $aItems = Slide::find()->where( 'banner_id', $this->iCurrentBanner )->order( 'position' )->getAll();

        //@TODO Активность приходит строкой и приведеним типов в JS превращается в true
        foreach( $aItems as $oItem )
            $oItem->active = (bool)$oItem->active;

        $this->render(new view\slideList([
            'aItems' =>  $aItems,
            'iCurrentBanner' => $this->currentBanner
        ]));

        return psComplete;

    }


    protected function actionSortSlideList() {

        $aData = $this->get( 'data' );
        $aDropData = $this->get( 'dropData' );
        $sPosition = $this->get( 'position' );

        if( !isSet($aData['id']) || !$aData['id'] ||
            !isSet($aDropData['id']) || !$aDropData['id'] || !$sPosition )
            $this->addError( \Yii::t('slider', 'sortError') );

        if( ! Slide::sort( $aData['id'], $aDropData['id'], $sPosition ) )
            $this->addError( \Yii::t('slider', 'sortError') );

    }


    /**
     * Форма редактиорвания слайда
     * @return int
     */
    protected function actionEditSlideForm() {
        $aData = $this->getInData();
        $iItemId = isSet($aData['id']) ? $aData['id'] : $this->iCurrentSlide;
        $this->iCurrentSlide = $iItemId;

        $sUploadImage = isSet($aData['upload_image']) ? $aData['upload_image'] : '';

        $this->addJsFile( \Yii::$app->getAssetManager()->getBundle(jquery\Asset::className())->baseUrl.'/jquery.js' );
        $this->addJsFile( \Yii::$app->getAssetManager()->getBundle(jquery\UiAsset::className())->baseUrl.'/jquery-ui.min.js' );

        if ( $iItemId )
            $oSlideRow = Slide::find( $iItemId );
        else {
            $oSlideRow = Slide::getNewRow();
            $oSlideRow->setVal( 'active', true );
        }


        if ( $sUploadImage )
            $oSlideRow->setVal( 'img', $sUploadImage );

        if ( !$oSlideRow->getVal( 'img' ) )
            $oSlideRow->setVal( 'img', Slide::getEmptyImgWebPath() );

        $this->addInitParam('lang', ['galleryUploadingImage' => \Yii::t('gallery', 'galleryUploadingImage')]);

        $this->render(new view\editSlideForm([
            'oItem' => $oSlideRow,
            'iItemId' => $iItemId
        ]));

        return psComplete;
    }


    /**
     * Форма расширенного редактирования текстов для слайда
     * @return int
     * @throws \Exception
     */
    protected function actionEditSlideText() {

        if ( !$this->iCurrentSlide )
            throw new \Exception( \Yii::t('slider', 'noFindSlide') );

        $oSlideRow = Slide::find( $this->iCurrentSlide );

        $this->render(new view\editSlideText([
            'oItem' => $oSlideRow
        ]));

        return psComplete;
    }


    /**
     * Обработка загруженного изображения для фона слайда
     */
    protected function actionUploadImage() {

        $iItemId = (int) $this->get( 'slideId' );

        $sSourceFN = Slide::uploadFile();

        $this->set(
            'data',
            array(
                'id' => $iItemId,
                'upload_image'=>$sSourceFN
            )
        );

        $this->actionEditSlideForm();
    }


    /**
     * Состояние сохранения слайда
     */
    protected function actionSaveSlide() {

        $aData = $this->getInData();
        $bBackToEdit = isSet( $aData['back2edit'] ) ? $aData['back2edit'] : false;

        try {

            if( !$this->iCurrentBanner )
                throw new \Exception( \Yii::t('slider', 'noFindBanner') );

            $iSlideId = (isSet($aData['id']) && $aData['id']) ? $aData['id'] : false;

            if ( $iSlideId )
                $oSlideRow = Slide::find( $iSlideId );
            else
                $oSlideRow = Slide::getNewRow();

            // при обновлении из списка приходят неактуальные данные
            unset( $aData['position'] );

            $oSlideRow->setData( $aData );

            $oSlideRow->setVal( 'banner_id', $this->iCurrentBanner );

            // не сохраняем картинку, если это загрушка
            if ( $oSlideRow->getVal( 'img' ) == Slide::getEmptyImgWebPath() )
                $oSlideRow->setVal( 'img', '' );

            if ( !$oSlideRow->getVal( 'position' ) )
                $oSlideRow->setVal( 'position', Slide::getMaxPos4Banner($this->iCurrentBanner)+1 );
            // todo position - перенести в модификаор и проверить механику с исключением текущего слайда по id

            $oSlideRow->save();

            if ($oBanner = Banner::findOne(['id' => $this->iCurrentBanner])){
                $oBanner->setVal('last_modified_date', date( "Y-m-d H:i:s", time() ));
                $oBanner->save();
            }

        } catch ( \Exception $e ) {

            $this->addError( $e->getMessage() );
        }

        $this->set( 'data', false );

        if ( $bBackToEdit )
            $this->actionEditSlideForm();
        else
            $this->actionSlideList();
    }


    /**
     * Состояние удаления слайда
     */
    protected function actionDelSlide() {

        $aData = $this->getInData();

        try {

            if( !$this->iCurrentBanner )
                throw new \Exception( \Yii::t('slider', 'noFindBanner') );

            $iSlideId = (isSet($aData['id']) && $aData['id']) ? $aData['id'] : false;

            if ( !$iSlideId )
                throw new \Exception( \Yii::t('slider', 'noFindSlide') );

            $oSlideRow = Slide::find( $iSlideId );

            if ( !$oSlideRow )
                throw new \Exception( \Yii::t('slider', 'noFindSlide') );

            if ($oBanner = Banner::findOne(['id' => $oSlideRow->getVal('banner_id')])){
                $oBanner->setVal('last_modified_date', date( "Y-m-d H:i:s", time() ));
                $oBanner->save();
            }

            $oSlideRow->delete();

        } catch ( \Exception $e ) {

            $this->addError( $e->getMessage() );
        }

        $this->actionSlideList();
    }


    /**
     * Форма настройки параметров показа баннеров
     * @return int
     */
    protected function actionToolsForm() {

        $aToolData = array();
        $aItems = Query::SelectFrom( 'banners_tools' )->getAll();
        foreach ( $aItems as $aItem )
            $aToolData[ $aItem['bt_key'] ] = $aItem['bt_value'];

        $aHeightParams = [
            'minValue' => 0,
            'allowDecimals' => false
        ];

        // #41095 Если задана адаптивность слайдеру, то запретить менять его высоту
        if (isset($aToolData['responsive']) and $aToolData['responsive']) {
            $aHeightParams['disabled'] = 1;
            $aToolData['maxHeight'] = '';
        }

        $this->render(new view\toolsForm([
            'aHeightParams' => $aHeightParams,
            'aToolData' => $aToolData
        ]));
        return psComplete;
    }


    /**
     * Состояние сохранения параметров показа баннеров
     */
    protected function actionSaveTools() {

        $aData = $this->get('data');

        // Максимальная высота должна быть >= минимальных высот
        if ( !empty($aData['maxHeight']) ){
            foreach (['minHeight350', 'minHeight768', 'minHeight1024', 'minHeight1280'] as $item) {
                if ( isset($aData[$item]) && ($aData['maxHeight'] < $aData[$item])  )
                    throw new UserException( \Yii::t('slider', 'error_maxheight_less_minheight', ['paramMax' => \Yii::t('slider', 'fotorama_maxHeight'), 'paramMin' => \Yii::t('slider', $item)]) );
            }
        }

        foreach ( $aData as $sKey => $sValue ) {
            Query::InsertInto( 'banners_tools' )
                ->set( 'bt_key', $sKey )
                ->set( 'bt_value', $sValue )
                ->onDuplicateKeyUpdate()
                ->set( 'bt_value', $sValue )
                ->get();
        }

        \Yii::$app->router->updateModificationDateSite();

        $this->actionBannerList();
    }

}