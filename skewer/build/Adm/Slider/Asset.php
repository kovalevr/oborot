<?php

namespace skewer\build\Adm\Slider;


use yii\web\AssetBundle;

class Asset extends AssetBundle {
    public $sourcePath = '@skewer/build/Adm/Slider/web';
}