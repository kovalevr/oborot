<?php
/**
 * Created by PhpStorm.
 * User: holod
 * Date: 21.11.2016
 * Time: 15:57
 */

namespace skewer\build\Adm\Slider\view;

use skewer\components\ext\view\FormView;
use skewer\components\ext;

class editSlideForm extends FormView
{
    public $oItem;
    public $iItemId;

    /**
     * Выполняет сборку интерфейса
     * @return void
     */
    function build() {
        $this->_form
            ->field('id', 'id', 'hide')
            ->field('img', '', 'hide')
            ->fieldSpec( 'canv', \Yii::t('slider', 'editSlide'), 'SlideShower', $this->oItem )
            ->field('active', \Yii::t('slider', 'active'), 'check')
            ->field('slide_link', \Yii::t('slider', 'slides_link'), 'string')
            ->setValue( $this->oItem )
            ->buttonSave('saveSlide')
            ->buttonCustomExt(
                ext\docked\UserFile::create( \Yii::t('slider', 'backLoad').'!', 'SlideLoadImage' )
                    ->setIconCls( ext\docked\Api::iconAdd )
                    ->setAddParam( 'slideId', $this->iItemId )
            );

        if ( $this->iItemId )
            $this->_form->buttonEdit('editSlideText', \Yii::t('slider', 'editSlideText'));

        $this->_form->buttonCancel('slideList');
    }
}