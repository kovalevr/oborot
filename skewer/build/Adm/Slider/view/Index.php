<?php

/**
 * Created by PhpStorm.
 * User: holod
 * Date: 21.11.2016
 * Time: 14:59
 */

namespace skewer\build\Adm\Slider\view;

use skewer\components\ext\view\ListView;

class Index extends ListView
{
    public $items;

    /**
     * Выполняет сборку интерфейса
     * @return void
     */
    function build() {
        $this->_list
            ->field('title', \Yii::t('slider', 'title'), 'string')
            ->field('preview_img', '', 'addImg', array('listColumns' => array('flex' => 1)))
            ->field('active', \Yii::t('slider', 'active'), 'check')
            ->widget( 'preview_img', 'skewer\\build\\Adm\\Slider\\Banner', 'getPreviewImg' )
            ->setEditableFields( array( 'active' ), 'saveBanner' );

        $this->_list->setValue( $this->items );

        $this->_list
            ->buttonAddNew('editBannerForm', \Yii::t('slider', 'addBanner'))
            ->buttonEdit('toolsForm', \Yii::t('slider', 'displaySettings'))
            ->buttonRowUpdate( 'SlideList' )
            ->buttonRowDelete( 'delBanner' );
    }
}