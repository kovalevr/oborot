<?php

namespace skewer\build\Adm\Slider;


use skewer\base\orm;
use skewer\base\section\Tree;

use skewer\base\ft;
use yii\helpers\ArrayHelper;


/**
 * Объект для работы с сущностью - Баннер
 * Class Banner
 * @package skewer\build\Adm\Slider
 */
class Banner extends orm\TablePrototype {

    protected static $sTableName = 'banners_main';

    protected static function initModel() {

        ft\Entity::get( 'banners_main' )
            ->clear()
            ->setTablePrefix('')
            ->setNamespace( __NAMESPACE__ )
            ->addField( 'title', 'varchar(255)', 'slider.title' )
                ->setDefaultVal( \Yii::t('slider', 'new_banner') )
            ->addField( 'section', 'int', 'slider.section' )
            ->addField( 'active', 'int(1)', 'slider.active' )
                ->setDefaultVal( 1 )
            ->addField( 'on_include', 'int(1)', 'slider.on_include' )
            ->addField( 'bullet', 'varchar(50)', 'slider.bullet' )
            ->addField( 'scroll', 'varchar(50)', 'slider.scroll' )
            ->addField( 'last_modified_date', 'date', 'slider.field_modifydate' )
            ->addColumnSet(
                'list',
                array( 'title', 'active' )
            )
            ->addColumnSet(
                'edit',
                array( 'id','title' )
            )
            ->addColumnSet(
                'list_edit',
                array( 'active' )
            )
            ->save()
        ;
    }

    /**
     * Вернёт ActiveRecord баннера с дефолтными значениями полей
     * @param array $aData
     * @return orm\ActiveRecord
     */
    public static function getBlankRow($aData = array()){
        $oNewBanner = parent::getNewRow($aData);
        $oNewBanner->setVal( 'title', \Yii::t('slider', 'new_banner') );
        $oNewBanner->setVal( 'active', true );
        $oNewBanner->setVal( 'bullet', 'false' );
        $oNewBanner->setVal( 'scroll', 'always' );
        return $oNewBanner;
    }

    /*
     * Получаем поличество слайдов в банере
     * */
    public static function getSlidesCount4Banner( $iBannerId ){

        $aSlides = orm\Query::SelectFrom( 'banners_slides' )
            ->where( 'active' )
            ->andWhere( 'banner_id', $iBannerId )
            ->order( 'position' )
            ->getCount();

        return $aSlides;
    }


    public static function getBanner4Section( $iSectionId ) {

        //$aBanners = \MainBanner2BannersToolMapper::getBannersForSection( $iSectionId );

        $aTreeSection = Tree::getSectionParents( $iSectionId );

        $tempBanners = orm\Query::SelectFrom( 'banners_main' )
            ->where( 'active' )
            ->andWhere( 'section', $iSectionId )
            ->orWhere( 'on_include' )
            ->andWhere( 'section', $aTreeSection )
            ->andWhere( 'active' )
            ->getAll();

        // ищем баннеры заданные для раздела
        $bUseParentBanners = true;

        $aBanners = [];
        foreach ( $tempBanners as $tBanner ){
            if( $tBanner['section'] == $iSectionId )
                $bUseParentBanners = false;
            //проверим есть ли слайды в этом банере
            if (self::getSlidesCount4Banner($tBanner['id'])>0){
                $aBanners[] = $tBanner;
            }

        }

        // если нашли - удаляем баннеры родителей
        if ( !$bUseParentBanners ) {

            foreach($aBanners as $iKey=>$aBanner)
                if( $aBanner['section'] != $iSectionId )
                    unset($aBanners[$iKey]);

            $aBanners = array_values($aBanners);
        }

        return $aBanners;
    }

    /**
     * Вернёт настройки показа слайдера
     * @param  array $aBanner - текущий баннер
     * @return array
     */
    public static function getAllTools( $aBanner = []) {

        $aToolData = array();
        $aItems = orm\Query::SelectFrom( 'banners_tools' )->getAll();

        foreach ( $aItems as $aItem ) {
            $mVal = $aItem['bt_value'];
            if (is_numeric($mVal))
                $mVal = (int)$mVal;
            $aToolData[$aItem['bt_key']] = $mVal;
        }

        if ( !empty($aToolData['maxHeight']) )
            $aToolData['maxHeight'] = (int)$aToolData['maxHeight'] ;
        else {
            unset($aToolData['maxHeight']);
        }

        $fPrependKey = function ($mKey){
            if ( ($mKey === 'false') || ($mKey === 0) || ($mKey === '0')  )
                return false;
            elseif ( ($mKey === 'true') || ($mKey === 1) || ($mKey === '1') )
                return true;
            else
                return $mKey;
        };

        if ( isset($aToolData['loop']) )
            $aToolData['loop'] = $fPrependKey($aToolData['loop']);

        if ( isset($aBanner['bullet']) )
            $aToolData['nav'] = $fPrependKey($aBanner['bullet']);

        if ( isset($aBanner['scroll']) )
            $aToolData['arrows'] = $fPrependKey($aBanner['scroll']);

        // ограничения высоты
        foreach ($aToolData as $sOptionName => &$sOptionValue) {
            if ( strpos($sOptionName, 'minHeight') === 0 ){
                $aToolData['height_limits'][$sOptionName] = $sOptionValue;
                unset($aToolData[$sOptionName]);
            }
        }

        return $aToolData;
    }


    /**
     * Виджет для картинки превью в списке баннеров
     * @param orm\ActiveRecord $oItem
     * @param $sField
     * @return string
     */
    public static function getPreviewImg( $oItem, $sField  ) {

        $iBannerId = $oItem->getVal( 'id' );

        if ( $iBannerId ) {

            /** @var orm\ActiveRecord $oRow */
            $oRow = Slide::find()->where( 'banner_id', $iBannerId )->where( 'active' )->order( 'position' )->getOne();

            if ( $oRow && ( $img = $oRow->getVal( 'img' ) ) ) {

                return $img;
            }
        }


        return Slide::getEmptyImgWebPath();
    }


    /**
     * Получение дерева разделов в виде списка
     * @return array
     */
    public static function getSectionTitle(){
        return Tree::getSectionsTitle( \Yii::$app->sections->root(), true );
    }


    public static function get4Section( $iSectionId ) {


        return array();
    }

    
    /**
     * Возвращает максимальную дату модификации сущности
     * @return array|bool
     */
    public static function getMaxLastModifyDate(){
        return (new \yii\db\Query())->select('MAX(`last_modified_date`) as max')->from(self::$sTableName)->one();
    }


} 