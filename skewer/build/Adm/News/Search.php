<?php


namespace skewer\build\Adm\News;


use skewer\base\orm\Query;
use skewer\components\search\models\SearchIndex;
use skewer\components\search\Prototype;
use skewer\base\section\Parameters;
use skewer\base\section\Tree;
use skewer\components\seo\Api;
use skewer\build\Adm\News\models\News;

class Search extends Prototype{
    /**
     * отдает имя идентификатора ресурса для работы с поисковым индексом
     * @return string
     */
    public function getName() {
       return 'News';
    }

    /**
     * @inheritdoc
     */
    protected function update(SearchIndex $oSearchRow) {

        $oSearchRow->class_name = $this->getName();

        if (!$oSearchRow->object_id)
            return false;

        /** @var News $news */
        $news = News::findOne(['id' => $oSearchRow->object_id]);
        if (!$news)
            return false;

        $bHidden = false;

        // нет данных - не добавлять в индекс
        if (!$news->full_text)
            $bHidden = true;

        if (!$news->active)
            $bHidden = true;

        $oSearchRow->search_text = $this->stripTags($news->full_text);
        $oSearchRow->search_title = $this->stripTags($news->title);
        $oSearchRow->status = 1;
        $oSearchRow->use_in_search = !$bHidden;
        $oSearchRow->use_in_sitemap = !$bHidden;
        $oSearchRow->section_id = $news->parent_section;
        $oSearchRow->language = Parameters::getLanguage($news->parent_section);

        // проверка существования раздела и реального url у него
        $oSection = Tree::getSection( $oSearchRow->section_id );
        if ( !$oSection || !$oSection->hasRealUrl() )
            return false;

        $oSearchRow->href  =
            ( !empty($news->news_alias) ?
                \Yii::$app->router->rewriteURL( sprintf(
                    '[%s][News?news_alias=%s]', // todo проверить урл(имя модуля) после переезда на неймспейс
                    $news->parent_section,
                    $news->news_alias
                )):
                \Yii::$app->router->rewriteURL( sprintf(
                    '[%s][News?id=%s]',
                    $news->parent_section,
                    $news->id
                ))
            );

        $oSearchRow->modify_date = $news->last_modified_date;

        $oSeoComponent = new Seo($news->id, $oSection->id, $news->getAttributes());
        $this->fillSearchRowSeoData( $oSearchRow, $oSeoComponent );

        $oSearchRow->save();

        return true;
    }

    /**
     *  воссоздает полный список пустых записей для сущности, отдает количество добавленных
     * @return int
     */
    public function restore() {
        $sql = "INSERT INTO search_index(`status`,`class_name`,`object_id`)  SELECT '0','{$this->getName()}',id  FROM news";
        Query::SQL($sql);

    }


} 