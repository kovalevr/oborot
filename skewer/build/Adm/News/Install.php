<?php

namespace skewer\build\Adm\News;
use skewer\components\config\InstallPrototype;

/**
 * Class Install
 * @package skewer\build\Adm\News
 */
class Install extends InstallPrototype {

    public function init() {
        return true;
    }// func

    public function install() {
        return true;
    }// func

    public function uninstall() {
        return true;
    }// func

}//class