<?php

$aLanguage = array();

$aLanguage['ru']['tab_name'] = 'Статьи';

$aLanguage['ru']['groups_articles'] = 'Параметры вывода статей';

$aLanguage['ru']['field_id'] = 'ID';
$aLanguage['ru']['field_alias'] = 'Псевдоним';
$aLanguage['ru']['field_parent'] = 'Родительский раздел';
$aLanguage['ru']['field_author'] = 'Автор';
$aLanguage['ru']['field_title'] = 'Название';
$aLanguage['ru']['field_date'] = 'Дата публикации';
$aLanguage['ru']['field_time'] = 'Время публикации';
$aLanguage['ru']['field_preview'] = 'Анонс';
$aLanguage['ru']['field_fulltext'] = 'Полный текст';
$aLanguage['ru']['field_active'] = 'Активность';
$aLanguage['ru']['field_on_main'] = 'На главной';
$aLanguage['ru']['field_hyperlink'] = 'Ссылка';
$aLanguage['ru']['field_source_link'] = 'Источник';
$aLanguage['ru']['field_modifydate'] = 'Дата последнего изменения';
$aLanguage['ru']['title_on_main'] = 'Заголовок статей на главной';
$aLanguage['ru']['on_page'] = 'Статей на странице';
$aLanguage['ru']['on_column'] = 'Записей на главной и в колонке';
$aLanguage['ru']['new_article'] = 'Статья';
$aLanguage['ru']['titleOnMain'] = 'Заголовок статей для главной';
$aLanguage['ru']['error_title'] = 'Название не задано';
$aLanguage['ru']['param_on_page'] = 'Статей на главной';
$aLanguage['ru']["author"] = "Автор";
$aLanguage['ru']['all_section_link']   = 'Все статьи';
$aLanguage['ru']['section_all']  = 'Раздел для ссылки "все статьи"';

$aLanguage['ru']['error_maxvalue_field_title']  = 'Значение поля "{fieldName}" не может быть более {maxValue}';

// ********************************************************************************************************************
// ***************************************************** ENGLISH ******************************************************
// ********************************************************************************************************************

$aLanguage['en']['tab_name'] = 'Articles';

$aLanguage['en']['groups_articles'] = 'Output Options articles';

$aLanguage['en']['field_id'] = 'ID';
$aLanguage['en']['field_alias'] = 'Alias';
$aLanguage['en']['field_parent'] = 'Parent section';
$aLanguage['en']['field_author'] = 'Author';
$aLanguage['en']['field_title'] = 'Title';
$aLanguage['en']['field_date'] = 'Publish date';
$aLanguage['en']['field_time'] = 'Publish time';
$aLanguage['en']['field_preview'] = 'Preview';
$aLanguage['en']['field_fulltext'] = 'Full text';
$aLanguage['en']['field_active'] = 'Active';
$aLanguage['en']['field_on_main'] = 'On main';
$aLanguage['en']['field_hyperlink'] = 'Link';
$aLanguage['en']['field_source_link'] = 'Source';
$aLanguage['en']['field_modifydate'] = 'Last modify date';
$aLanguage['en']['title_on_main'] = 'Title of articles on the main';
$aLanguage['en']['on_page'] = 'Articles on page';
$aLanguage['en']['on_column'] = 'Articles on main and in column';
$aLanguage['en']['new_article'] = 'Article';
$aLanguage['en']['titleOnMain'] = 'Title articles to main';
$aLanguage['en']['error_title'] = 'Empty title';
$aLanguage['en']['param_on_page'] = 'Articles on main';
$aLanguage['en']["author"] = "Author";
$aLanguage['en']['all_section_link']   = 'All the articles';
$aLanguage['en']['section_all']        = 'Section for links "all articles"';

$aLanguage['en']['error_maxvalue_field_title']  = 'The value of the field "{fieldName}" can not be more than {maxValue}';

return $aLanguage;