<?php


namespace skewer\build\Adm\Articles;


use skewer\base\orm\Query;
use skewer\components\search\models\SearchIndex;
use skewer\components\search\Prototype;
use skewer\base\section\Parameters;
use skewer\base\section\Tree;
use skewer\build\Page\Articles\Model\Articles;
use skewer\build\Page\Articles\Model\ArticlesRow;
use skewer\components\seo\Api;

class Search extends Prototype {
    /**
     * отдает имя идентификатора ресурса для работы с поисковым индексом
     * @return string
     */
    public function getName() {
        return 'Articles';
    }

    /**
     * @inheritdoc
     */
    protected function update(SearchIndex $oSearchRow) {

        $oSearchRow->class_name = $this->getName();

        if (!$oSearchRow->object_id)
            return false;

        /** @var ArticlesRow $article */
        $article = Articles::find($oSearchRow->object_id);
        if (!$article)
            return false;

        $bHidden = false;

        // нет данных - не добавлять в индекс
        if (!$article->full_text)
            $bHidden=true;

        if (!$article->active)
            $bHidden=true;

        $oSearchRow->search_text = $this->stripTags($article->full_text);
        $oSearchRow->search_title = $this->stripTags($article->title);
        $oSearchRow->section_id = $article->parent_section;
        $oSearchRow->status = 1;
        $oSearchRow->use_in_search = !$bHidden;
        $oSearchRow->use_in_sitemap = !$bHidden;
        
        $oSearchRow->language = Parameters::getLanguage($article->parent_section);

        // проверка существования раздела и реального url у него
        $oSection = Tree::getSection( $oSearchRow->section_id );
        if ( !$oSection || !$oSection->hasRealUrl() )
            return false;

        $oSearchRow->href  = $sURL = \Yii::$app->router->rewriteURL(sprintf(
            '[%s][Articles?%s=%s]',
            $article->parent_section,
            $article->articles_alias ? 'articles_alias' : 'articles_id',
            $article->articles_alias ? $article->articles_alias : $article->id
        ));

        $oSearchRow->modify_date =  $article->last_modified_date;

        $oSeoComponent = new Seo($article->id, $oSection->id, $article->getData());

        $this->fillSearchRowSeoData( $oSearchRow, $oSeoComponent );

        $oSearchRow->save();
        return true;
    }

    /**
     *  воссоздает полный список пустых записей для сущности, отдает количество добавленных
     * @return int
     */
    public function restore() {
        $sql = "INSERT INTO search_index(`status`,`class_name`,`object_id`)  SELECT '0','{$this->getName()}',id  FROM articles";
        Query::SQL($sql);
    }


} 