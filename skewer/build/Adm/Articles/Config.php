<?php

use skewer\base\site\Layer;
use skewer\base\section\models\TreeSection;
use skewer\components\search;
use \skewer\build\Tool\Rss;

/* main */
$aConfig['name']     = 'Articles';
$aConfig['title']    = 'Статьи (админ)';
$aConfig['version']  = '1.0';
$aConfig['description']  = 'Админ-интерфейс управления статьями';
$aConfig['revision'] = '0002';
$aConfig['layer']     = Layer::ADM;
$aConfig['useNamespace'] = true;

$aConfig['events'][] = [
    'event' => TreeSection::EVENT_BEFORE_DELETE,
    'eventClass' => TreeSection::className(),
    'class' => skewer\build\Page\Articles\Model\Articles::className(),
    'method' => 'removeSection',
];

$aConfig['events'][] = [
    'event' => search\Api::EVENT_GET_ENGINE,
    'class' => skewer\build\Page\Articles\Model\Articles::className(),
    'method' => 'getSearchEngine',
];

$aConfig['events'][] = [
    'event' => Rss\Api::EVENT_REBUILD_RSS,
    'class' => Rss\Api::className(),
    'method' => 'rebuildRss'
];

$aConfig['events'][] = [
    'event' => Rss\Api::EVENT_GET_DATA,
    'class' => skewer\build\Page\Articles\Model\Articles::className(),
    'method' => 'getRssRows',
];

return $aConfig;
