<?php

namespace skewer\build\Adm\Order;

use skewer\components\forms\Entity;
use skewer\components\forms\Table;
use skewer\base\site\Site;
use skewer\components\i18n\Languages;
use skewer\components\i18n\ModulesParams;
use skewer\base\section\Parameters;
use skewer\base\ui;
use skewer\build\Adm;
use skewer\build\Adm\Order\model\Status;
use skewer\components\catalog;
use skewer\components\auth\CurrentAdmin;
use skewer\helpers\Transliterate;
use yii\base\UserException;
use yii\helpers\ArrayHelper;
use skewer\build\Tool\Payments;
use skewer\build\Page\Cart\Api as ApiCart;
use skewer\base\SysVar;

/**
 * Class Module
 * @package skewer\build\Adm\Order
 */
class Module extends Adm\Tree\ModulePrototype {

    /** @var int Текущий номер станицы постраничника */
    var $iPageNum = 0;

    protected $iStatusFilter = 0;

    protected $sLanguageFilter = '';

    protected function preExecute() {

        // Получить номер страницы
        $this->iPageNum = $this->getInt('page');

        $this->iStatusFilter = $this->getInt('filter_status', 0);

        $sLanguage = \Yii::$app->language;
        if ($this->sectionId())
            $sLanguage = Parameters::getLanguage($this->sectionId()) ?: $sLanguage;

        $this->sLanguageFilter = $this->get('filter_language', $sLanguage);

    }

    protected function setServiceData(ui\state\BaseInterface $oIface) {

        parent::setServiceData($oIface);

        // расширение массива сервисных данных
        $oIface->setServiceData(array(
            'filter_status' => $this->iStatusFilter,
            'filter_language' => $this->sLanguageFilter,
        ));

    }


    protected function actionInit() {
        $this->actionList();
    }

    protected function actionDelete() {
        // запросить данные
        $aData = $this->get('data');

        // id записи
        $iItemId = (is_array($aData) and isset($aData['id'])) ? (int)$aData['id'] : 0;

        if (!$iItemId){
            $iItemId = $this->getInnerData('orderId');
        }

        $oOrder = ar\Order::find($iItemId);
        $oOrder->delete();

        // вывод списка
        $this->actionInit();
    }

    /**
     * Сохранение заказы
     */
    protected function actionSave() {

        // запросить данные
        $aData = $this->get('data');
        $iId = $this->getInnerDataInt('orderId');

        if (!$aData)
            throw new UserException('Empty data');

        if ($iId) {
            /**
             * @var ar\OrderRow $oRow
             */
            $oRow = ar\Order::find($iId);

            if (!$oRow)
                throw new UserException("Not found [$iId]");

            if (isset($aData['status']) && $oRow->status != $aData['status']) {
                Service::sendMailChangeOrderStatus($iId, $oRow->status, $aData['status']);
            }

            $oRow->setData($aData);
        } else {
            $oRow = ar\Order::getNewRow($aData);
        }

        $oRow->save();
        // вывод списка
        $this->actionInit();
    }

    protected function actionShow() {

        // номер заказа
        $aData = $this->get('data');

        $iItemId = (is_array($aData) && isset($aData['id'])) ? (int)$aData['id'] : 0;
        $iItemId = $this->getInDataVal('id_order',$iItemId);

        $statusList = Status::getListTitle();
        $paymentList = ArrayHelper::map(ar\TypePayment::find()->asArray()->getAll(),'id','title');
        $deliveryList = ArrayHelper::map(ar\TypeDelivery::find()->asArray()->getAll(),'id','title');

        $aHistoryList = Adm\Order\model\ChangeStatus::find()->asArray()->where(['id_order' => $iItemId])->all();

        $aOrder = ar\Order::find()->where('id',$iItemId)->asArray()->getOne();

        if (!$aOrder)
            throw new UserException('Item not found');


        if ($aHistoryList){
            foreach($aHistoryList as $k=>$item){
                $aHistoryList[$k]['title_old_status'] = ArrayHelper::getValue($statusList, $item['id_old_status'], sprintf('--- ['.$item['id_old_status'].']'));
                $aHistoryList[$k]['title_new_status'] = ArrayHelper::getValue($statusList, $item['id_new_status'], sprintf('--- ['.$item['id_new_status'].']'));
            }
            $aOrder['history'] = $this->renderTemplate('historyList.twig', array('historyList'=>$aHistoryList));
        }

        $aGoods = ar\Goods::find()->where('id_order',$aOrder['id'])->asArray()->getAll();
        foreach($aGoods as $k=>$item){
            // наверно это не правильно, но врятли у нас будет > 100 товаров в заказе
            $aGoods[$k]['object'] = catalog\GoodsSelector::get($item['id_goods'], 1); //fixme #GET_CATALOG_GOODS
        }
        $totalPrice = Api::getOrderSum($aOrder['id']);

        $sText = "";
        if ($aGoods && !empty($aGoods)) {
            $sText = $this->renderTemplate('admin.goods.twig', array('id'=>$aOrder['id'],'aGoods' => $aGoods,'totalPrice'=>$totalPrice));
        }



        $aOrder['good'] = $sText;

        if (isset($aOrder['id']) && $aOrder['id']){
            $this->setInnerData('orderId', $aOrder['id']);
        }
        $this->render(new view\Show([
            'aStatusList' => $statusList,
            'aPaymentList' => $paymentList,
            'aDeliveryList' => $deliveryList,
            'aOrder' => $aOrder,
            'aFormRowKey'=>$this->getExistFieldForm()
        ]));
    }

    /**
     * Получение массива для проверки существования полей
     * @return  array
     */
    private function getExistFieldForm()
    {
        $aFormRowKey = array_keys(Table::getByName(Entity::NAME_FORM_ORDER)->getFields());

        if (in_array('name', $aFormRowKey))
            $aFormRowKey[] = 'person';

        if (in_array('email', $aFormRowKey))
            $aFormRowKey[] = 'mail';
        return $aFormRowKey;
    }


    /**
     * Список заказов
     */
    protected function actionList() {
        $aStatusList =  Status::getListTitle();

        /** Общее число заказов */
        $iCount = 0;

        $oQuery = ar\Order::find()
            ->index('id')
            ->limit(self::maxOrdersOnPage(), $this->iPageNum * self::maxOrdersOnPage())
            ->setCounterRef($iCount)
            ->order('id', 'DESC');

        if ($this->iStatusFilter)
            $oQuery->where('status', $this->iStatusFilter);

        $aOrders = $oQuery->asArray()->getAll();

        // Добавить общую сумму заказа
        $aGoodsSatistic = Api::getGoodsStatistic(array_keys($aOrders));
        foreach ($aOrders as $iOrderId => &$aOrder)
            $aOrder['total_price'] = isset($aGoodsSatistic[$iOrderId]) ? round($aGoodsSatistic[$iOrderId]['sum'], 3) : '-';

        $this->render(new view\Index([
            'aItems' => $aOrders,
            'aStatusList' => $aStatusList,
            'iStatusFilter' => $this->iStatusFilter,
            'onPage' => self::maxOrdersOnPage(),
            'page' => $this->iPageNum,
            'total' => $iCount
        ]));
    }

    /**
     * CRUD для типа доставки (list)
     */
    protected function actionTypeDeliveryList(){
        $this->render(new view\TypeDeliveryList([
            'aItems' => ar\TypeDelivery::find()->asArray()->getAll()
        ]));
    }

    /**
     * CRUD для типа доставки (add)
     */
    protected function actionAddTypeDelivery(){
        $this->render(new view\AddTypeDelivery([
            'oItems' => ar\TypeDelivery::getNewRow()
        ]));
    }

    /**
     * CRUD для типа доставки (delete)
     */
    protected function actionDeleteTypeDelivery(){

        $iId = $this->getInDataValInt('id');

        if ($iId){
            ar\TypeDelivery::delete($iId);
        }
        $this->actionTypeDeliveryList();
    }

    /**
     * CRUD для типа оплаты (save)
     */
    protected function actionSaveTypeDelivery(){

        $oRow = new ar\TypeDeliveryRow();
        $oRow->setData($this->getInData());
        $oRow->save();

        $this->actionTypeDeliveryList();
    }

    /**
     * CRUD для типа оплаты (list)
     */
    protected function actionTypePaymentList(){
        $this->render(new view\TypePaymentList([
            'aItems' => ar\TypePayment::find()->asArray()->getAll()
        ]));
    }

    protected function actionEditTypePayment(){

        $id = $this->getInDataValInt( 'id' );
        $this->showTypePaymentEditForm( $id );

    }

    /**
     * CRUD для типа оплаты (add)
     */
    protected function actionAddTypePayment(){

        $this->showTypePaymentEditForm();

    }

    private function showTypePaymentEditForm( $id = null ){
        /**
         * @var ar\TypePayment $oParameters
         */
        if ($id){
            $oParameters = ar\TypePayment::find($id);
        }else{
            $oParameters = ar\TypePayment::getNewRow();
        }

        $aItems = ArrayHelper::map(Payments\Api::getPaymentsList( true ), 'type', 'title');

        $this->render(new view\TypePaymentEditForm([
            'oParameters' => $oParameters,
            'aItems' => $aItems
        ]));
    }

    /**
     * CRUD для типа оплаты (delete)
     */
    protected function actionDeleteTypePayment(){

        $iId = $this->getInDataValInt('id');

        if ($iId){
            ar\TypePayment::delete($iId);
        }
        $this->actionTypePaymentList();
    }

    /**
     * CRUD для типа оплаты (save)
     */
    protected function actionSaveTypePayment(){

        $oRow = new ar\TypePaymentRow();
        $oRow->setData($this->getInData());

        // #40084 Правка значения "0" при выборе типа оплаты по умолчанию
        if (!Payments\Api::getParams((string)$oRow->payment))
            $oRow->payment = '';

        $oRow->save();

        $this->actionTypePaymentList();
    }


    protected function actionEmailShow() {

        if (!$this->sectionId()){
            $aLanguages = Languages::getAllActive();
            $aLanguages = ArrayHelper::map($aLanguages, 'name', 'title');
        }

        $aModulesData = ModulesParams::getByModule('order', $this->sLanguageFilter);
        $this->setInnerData('languageFilter', $this->sLanguageFilter);

        $aItems = [];
        $aItems['info'] = \Yii::t('order', 'head_mail_text', [\Yii::t('app', 'site_label'), \Yii::t('app', 'url_label')]);
        $aItems['title_change_status_mail'] = ArrayHelper::getValue($aModulesData, 'title_change_status_mail', '');
        $aItems['status_content'] = ArrayHelper::getValue($aModulesData, 'status_content', '');
        $aItems['title_user_mail'] = ArrayHelper::getValue($aModulesData, 'title_user_mail', '');
        $aItems['user_content'] = ArrayHelper::getValue($aModulesData, 'user_content', '');
        $aItems['title_adm_mail'] = ArrayHelper::getValue($aModulesData, 'title_adm_mail', '');
        $aItems['adm_content'] = ArrayHelper::getValue($aModulesData, 'adm_content', '');
        $aItems['status_paid_content'] = ArrayHelper::getValue($aModulesData, 'status_paid_content', '');
        $aItems['title_status_paid'] = ArrayHelper::getValue($aModulesData, 'title_status_paid', '');

        $this->render(new view\EmailShow([
            'sLanguageFilter' => $this->sLanguageFilter,
            'aLanguages' => $aLanguages,
            'aItems' => $aItems
        ]));
    }

    protected function actionEmailSave() {

        $aData = $this->getInData();

        $aKeys =
            [
                'title_change_status_mail', 'status_content',
                'status_paid_content', 'title_status_paid',
                'title_user_mail', 'user_content',
                'title_adm_mail', 'adm_content'
            ];

        $sLanguage = $this->getInnerData('languageFilter');
        $this->setInnerData('languageFilter', '');

        if ($sLanguage){
            foreach( $aData as $sName => $sValue ){

                if (!in_array($sName, $aKeys))
                    continue;

                ModulesParams::setParams( 'order', $sName, $sLanguage, $sValue);
            }
        }

        $this->actionEmailShow();
    }

    /**
     * Быстрое сохранение прям из листа
     */
    protected function actionEditDetailGoods(){

        // запросить данные
        $aData = $this->get('data');

        $sPrice = str_replace(',','.',$aData['price']);
        $sCount = $aData['count'];

        // проверяем, чтоб админ не ввел бурду
        if (is_numeric($sPrice) && is_numeric($sCount)){

            $iId = $this->getInDataValInt('id');
            if ($iId) {
                /**
                 * @var ar\GoodsRow $oRow
                 */
                $oRow = ar\Goods::find($iId);
                if (!$oRow)
                    throw new UserException("Запись [$iId] не найдена");

                if (!$sPrice) {
                    $sPrice = $oRow->price;
                    $aData['price'] = $oRow->price;
                }

                $total = $sPrice*$sCount;
                if ($total>0)
                    $aData['total'] = $total;

                $oRow->setData($aData);
                $oRow->save();

                $oRow = ar\Goods::find($iId);
                $aData = $oRow->getData();

                ui\StateBuilder::updRow($this, $aData);
            }
        } else throw new UserException( \Yii::t('order', 'valid_data_error') );
    }

    protected function actionDeleteDetailGoods(){
        // запросить данные
        $aData = $this->get('data');

        // id записи
        $iItemId = (is_array($aData) and isset($aData['id'])) ? (int)$aData['id'] : 0;
        ar\Goods::delete($iItemId);
        $this->actionGoodsShow($aData['id_order']);
    }

    protected function actionGoodsShow($id = 0) {
        // номер заказа
        $iItemId = $this->getInnerDataInt('orderId');

        if (!$iItemId || $id) $iItemId = $id;

        $aItems =  ar\Goods::find()->where('id_order',$iItemId)->asArray()->getAll();

        $this->render(new view\GoodsShow([
            'aItems' => $aItems,
            'iItemId' => $iItemId,
        ]));
    }


    protected function actionStatusList() {
        $this->render(new view\StatusList([
            'bIsSystemMode' => CurrentAdmin::isSystemMode(),
            'aItems' => Status::getList(),
        ]));
    }

    protected function actionStatusShow(){

        $sName = $this->getInDataVal('name');

        if ($sName) {
            $oStatus = Status::find()
                ->where(['name' => $sName])
                ->multilingual()
                ->one()
            ;
        }
        else {
            $oStatus = new Status();
        }

        $aLanguages = Languages::getAllActive();

        if (count($aLanguages) > 1)
            $sLabel = 'status_title_lang';
        else
            $sLabel = 'status_title';

        $this->render(new view\StatusShow([
            'bIsSystemMode' => CurrentAdmin::isSystemMode(),
            'bIsNewRecord' => $oStatus->isNewRecord,
            'aLanguages' => Languages::getAllActive(),
            'sLabel' => $sLabel,
            'aData' => $oStatus->getAllAttributes(),
        ]));
    }

    protected function actionStatusDelete() {

        $name = ArrayHelper::getValue($this->get('data'), 'name', '');
        if ($name){
            if (!Status::canBeDeleted($name)){
                throw new UserException( \Yii::t('order', 'status_delete_error') );
            }
            Status::deleteAll(['name' => $name]);
        }

        $this->actionStatusList();
    }

    /**
     * Сохранение статуса
     */
    protected function actionStatusSave() {

        $aData = $this->get('data');
        $sName = $this->getInDataVal('name');
        if (empty($sName))
            $sName =  Transliterate::change($aData['title_ru']);

        if (!$aData || !$sName)
            throw new UserException(\Yii::t('order', 'valid_data_error'));

        if (isset($aData['title']))
            $aData['title_'.\Yii::$app->language] = $aData['title'];

        /**
         * @var Status $oStatus
         */
        $oStatus = Status::find()
            ->multilingual()
            ->where(['name' => $sName])
            ->one()
        ;

        if (!$oStatus){
            $oStatus = new Status();
            $oStatus->name = $sName;
        }

        $oStatus->setLangData($aData);

        $oStatus->save();

        $this->actionStatusList();
    }


    protected function actionEditLicense() {
        $viewData = array();
        if (!$this->sectionId()){
            $aLanguages = Languages::getAllActive();
            $aLanguages = ArrayHelper::map($aLanguages, 'name', 'title');
            $viewData['aLanguages'] = $aLanguages;
            $viewData['sLanguageFilter'] = $this->sLanguageFilter;
        }

        $aData['license'] = ModulesParams::getByName('order', 'license', $this->sLanguageFilter);
        $this->setInnerData('languageFilter', $this->sLanguageFilter);

        $viewData['sectionId'] = $this->sectionId();
        $viewData['aData'] = $aData;
        $this->render(new view\EditLicense($viewData));

    }

    /** Состояние: Настройки для всех заказов */
    protected function actionEditSettings() {
        $this->render(new view\EditSettings([
            'aItems' =>[
                'order_max_size' => ApiCart::maxOrderSize(),
                'onpage_profile' => ApiCart::maxOrdersOnPage(),
                'onpage_cms'     => self::maxOrdersOnPage(),
            ]
        ]));
    }

    /** Действие: сохранить настройки для всех заказов */
    protected function actionSaveSettings() {

        $aData = $this->get('data');

        isset($aData['order_max_size']) and ApiCart::maxOrderSize($aData['order_max_size']);
        isset($aData['onpage_profile']) and ApiCart::maxOrdersOnPage($aData['onpage_profile']);
        isset($aData['onpage_cms']) and self::maxOrdersOnPage($aData['onpage_cms']);

        $this->actionList();
    }


    protected function actionSaveLicense() {

        $sLanguage = $this->getInnerData('languageFilter');
        $this->setInnerData('languageFilter', '');

        if ($sLanguage) {
            ModulesParams::setParams('order', 'license', $sLanguage, $this->getInDataVal('license'));
        }

        $this->actionList();
    }

    /** Получить/установить максильное число отображаемых заказов в админке */
    private static function maxOrdersOnPage() {

        return func_num_args() ? SysVar::set('Order.onpage_cms', (int)func_get_arg(0)) : SysVar::get('Order.onpage_cms', 100);
    }
}