<?php
/**
 * Created by PhpStorm.
 * User: holod
 * Date: 22.11.2016
 * Time: 15:24
 */

namespace skewer\build\Adm\Order\view;

use skewer\components\ext\view\ListView;

class TypePaymentList extends ListView
{
    public $aItems;

    /**
     * @inheritdoc
     */
    function build() {
        $this->_list
            ->field('id', 'id', 'string')
            ->field('title', \Yii::t('order', 'payment_field_title'), 'string', array('listColumns' => array('flex' => 3)))
            ->field('payment', \Yii::t('order', 'payment_field_payment'), 'string', array('listColumns' => array('flex' => 1)))
            ->widget( 'payment', 'skewer\\build\\Tool\\Payments\\Api', 'getPaymentsTitle')
            ->setValue($this->aItems)
            ->buttonRowUpdate('EditTypePayment')
            ->buttonRowDelete('DeleteTypePayment')
            ->buttonAddNew('AddTypePayment')
            ->buttonCancel();
    }
}