<?php
/**
 * Created by PhpStorm.
 * User: holod
 * Date: 22.11.2016
 * Time: 15:07
 */

namespace skewer\build\Adm\Order\view;

use skewer\components\ext\view\ListView;

class TypeDeliveryList extends ListView
{
    public $aItems;

    /**
     * @inheritdoc
     */
    function build() {
        $this->_list
            ->field('id', 'id', 'string')
            ->field('title', \Yii::t('order', 'field_delivery'), 'string', array('listColumns' => array('flex' => 3)))
            ->setValue($this->aItems)
            ->buttonRowDelete('DeleteTypeDelivery')
            ->buttonAddNew('AddTypeDelivery')
            ->buttonCancel()
            ->setEditableFields(['title'],'SaveTypeDelivery')
            ;
    }
}