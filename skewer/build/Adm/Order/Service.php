<?php
/**
 *
 * @project Skewer
 * @package Modules
 *
 * @author kolesnikov,max $Author: $
 * @version $Revision:  $
 * @date $Date: $
 */

namespace skewer\build\Adm\Order;

use skewer\base\section\Tree;
use skewer\base\site\ServicePrototype;
use skewer\base\site\Site;
use skewer\base\site_module\Parser;
use skewer\base\site_module\Request;
use skewer\base\SysVar;
use skewer\helpers\Mailer;
use skewer\build\Adm\Order\model\ChangeStatus;
use skewer\build\Adm\Order\model\Status;
use skewer\components\catalog;
use skewer\base\orm\ActiveRecord;
use skewer\base\orm\Query;
use skewer\build\Page\Auth\Api as ApiAuth;
use skewer\components\forms;
use skewer\build\Page\Cart as Cart;
use skewer\components\i18n\ModulesParams;
use skewer\components\auth\CurrentUser;
use yii\base\Exception;
use yii\helpers\ArrayHelper;
use skewer\build\Adm\Order as AdmOrder;
use skewer\components\crm\Crm;
use skewer\build\Tool\Payments;
use skewer\build\Tool\Crm\Api as CrmApi;

/**
 * @todo отрефакторить класс. Добавить языки в заказы (пользователи). Парсинг письма клиенту с нужным языком.
 * Class Service
 * @package skewer\build\Adm\Order
 */
class Service extends ServicePrototype {

    private static $aStatus = array();

    protected static function buildArrayStatus() {

        static::$aStatus = ArrayHelper::map(Status::getList(false, \Yii::$app->language), 'id', 'title');
    }

    /**
     * Список статусов
     * @return array
     */
    public static function getStatusList() {

        if (empty(static::$aStatus)) {
            static::buildArrayStatus();
        }

        return static::$aStatus;
    }

    /**
     * Метод отправки письма
     * @param string $sMail email кому отправляем
     * @param string $sTitle заголовок письма
     * @param string $sBody текст письма
     * @param array $aOptions массив меток для автозамены
     * @return bool
     */
    public static function sendMail($sMail, $sTitle, $sBody, $aOptions = array()) {

        if (isset($aOptions['token']))
            $aOptions['link'] = '<a href="' . Site::httpDomain() . ApiAuth::getProfilePath() . '?cmd=detail&token=' . $aOptions['token'] . '">'.\Yii::t('adm','by_link').'</a>';

        return $oMailer = Mailer::sendMail($sMail, $sTitle, $sBody, $aOptions);

    }


    /**
     * Заголовок статуса (для виджета)
     * @param ActiveRecord $oItem
     * @param $sField
     * @return string
     */
    public static function getStatusValue($oItem,
        /** @noinspection PhpUnusedParameterInspection */ $sField){

        $aStatulList = static::getStatusList();

        if (isset($aStatulList[$oItem['status']])){
            return $aStatulList[$oItem['status']];
        } else return '';
    }

    /**
     * Письмо о смене статуса
     * @param int $iOrderId
     * @param int $iBeforeStatus
     * @param int $iAfterStatus
     */
    public static function sendMailChangeOrderStatus($iOrderId, $iBeforeStatus, $iAfterStatus) {

        $aStatus = Status::getListTitle();

        /**
         * @var ar\OrderRow $oRow
         */
        $oRow = ar\Order::find($iOrderId);

        // тут надо отправить email о смене статуса, сделал в лоб, скорее всего надо будет переписать
        if (isset($aStatus[$oRow->status]) || $aStatus[$iAfterStatus]) {

	        //Если есть привязка статуса к методу в модуле Payment, то стараемся выполнить.
	        /** @var ar\TypePaymentRow $oPayment */
	        $oPayment = ($oRow->type_payment) ? ar\TypePayment::find($oRow->type_payment) : false;
	        if ($oPayment) {
		        $oStatus = Status::findOne(['id' => $iAfterStatus]);

                if (is_null($oStatus))
                    throw new Exception('Invalid status');

                $statusName = $oStatus->name;

		        $oPaymentModule = Payments\Api::make($oPayment->payment);
		        if (method_exists($oPaymentModule, 'bindedStatusMethod') == true and method_exists($oPaymentModule, 'executeStatusMethod') == true) {
			        $oPaymentModule->setOrderId($oRow->id);
			        $bRes = $oPaymentModule->bindedStatusMethod($statusName);
			        if ($bRes) { //Есть разрешение
				        $bRes = $oPaymentModule->executeStatusMethod($statusName);
				        if (!$bRes) { //Не удалось совершить операцию
					        $iAfterStatus = Status::getIdByFail();
				        }
			        }
		        }

	        }

            $oChangeRowStatus = new ChangeStatus();
            $oChangeRowStatus->change_date = date('Y-m-d H:i:s');
            $oChangeRowStatus->id_old_status = $iBeforeStatus;
            $oChangeRowStatus->id_new_status = $iAfterStatus;
            $oChangeRowStatus->id_order = $iOrderId;
            $oChangeRowStatus->save();

            $aVars = $oRow->getData();

            $aDataOrder = $oRow->getDataOrder();

            $aGoods = ar\Goods::find()
                ->where('id_order', $iOrderId)
                ->asArray()->getAll();

            $totalPrice = 0;

            $aGoodsListId = array();
            foreach($aGoods as $item){
                $aGoodsListId[] = $item['id_goods'];
                $totalPrice+=$item['total'];
            }

            $webrootpath = Site::httpDomain();
            if ($aGoodsListId){

                $aGoodsList = catalog\GoodsSelector::getList( catalog\Card::DEF_BASE_CARD )
                    ->condition('id IN ?', $aGoodsListId)
                    ->parse()
                ;

                $aGoodsList = ArrayHelper::index( $aGoodsList, 'id' );

                foreach($aGoods as &$item){
                    $item['object'] = (isset($aGoodsList[$item['id_goods']]))?$aGoodsList[$item['id_goods']]:false;
                    $item['webrootpath'] = $webrootpath;
                }
            }

            /**
             * @var ar\OrderRow $oOrder
             */
            $oOrder = ar\Order::find()->where('id',$iOrderId)->getOne();

            $out = Parser::parseTwig('mail.twig', array('totalPrice'=>$totalPrice,'orderId'=>$iOrderId,'items' => $aDataOrder ,'date' => $aVars['date'], 'aGoods' => $aGoods), __DIR__ . '/templates/');

            $sTitle = ModulesParams::getByName('order', 'title_change_status_mail');
            $sBody = ModulesParams::getByName('order', 'status_content');

            Service::sendMail($oRow->mail, $sTitle, $sBody, [
                'order_id' => $iOrderId,
                'before_status' => $aStatus[$iBeforeStatus],
                "order_info" => $out,
                'token' => $oOrder->token,
                'after_status' => $aStatus[$iAfterStatus]
            ]);

            if ($oChangeRowStatus->id_new_status == Status::getIdByPaid()) {

                $sBody = ModulesParams::getByName('order', 'status_paid_content');
                $sTitle = ModulesParams::getByName('order', 'title_status_paid');

                Service::sendMail(Site::getAdminEmail(), $sTitle, $sBody, [
                    'order_id' => $iOrderId,
                    'before_status' => $aStatus[$iBeforeStatus],
                    "order_info" => $out,
                    'token' => $oOrder->token,
                    'after_status' => $aStatus[$iAfterStatus],
                ]);
            }
        }
    }

    /**
     * Меняем статус заказа (для робокассы)
     * @param $iOrderId
     * @param $iStatus
     * @param $iTotal
     * @return bool
     */
    public static function changeStatus($iOrderId, $iStatus, $iTotal) {

        $oResult = Query::SQL(
            "SELECT orders_goods.id_order, SUM(orders_goods.total) as total_price
            FROM orders_goods
            WHERE orders_goods.id_order = :id
            GROUP BY orders_goods.id_order",
            array( 'id' => $iOrderId )
        );

        if ( $aRow = $oResult->fetchArray() ) {

            return Query::SQL(
                "UPDATE orders SET status=:status WHERE id=:order",
                array(
                    'order' => $iOrderId,
                    'status' => ( $aRow['total_price'] <= $iTotal ) ? $iStatus : Status::getIdByFail()
                )
            );

        }

        return false;
    }


    /**
     * Форма сохранения заказа из формбилдера
     * @param forms\Entity $oForm
     * @return bool
     */
    public function saveOrder(forms\Entity $oForm) {

        $oCartForm = new Cart\OrderForm();

        $aFieldList = $oForm->getFields();
        $aData = $oForm->getData();
        $bFast = isset($aData['fastBuy'])?$aData['fastBuy']:false;
        $oCartForm->setFast($bFast);

        $aFields = array(
            'address' => 'address',
            'name' => 'person',
            'phone' => 'phone',
            'email' => 'mail',
            'postcode' => 'postcode',
            'text' => 'text',
            'tp_deliv' => 'tp_deliv',
            'tp_pay' => 'tp_pay',
        );

        foreach ($aFieldList as $oItem) {
            if ( array_key_exists( $oItem->param_name, $aFields ) )
                $oCartForm->$aFields[$oItem->param_name] = $oItem->param_default;
        }

        return $this->saveOrderForm( $oCartForm, $oForm );
    }

    /**
     * Форма сохранения заказа из формбилдера
     * @param Cart\OrderForm $oForm
     * @param null|forms\Entity $oFormEntity
     * @return int
     * @throws \Exception
     */
    public function saveOrderForm( Cart\OrderForm $oForm, $oFormEntity = null ) {

        $aFieldList = $oForm->getFields();

        $oOrder = new ar\OrderRow();

        foreach ($aFieldList as $oItem) {

            switch ($oItem->name) {
                case 'address':
                    $oOrder->address = $oItem->value;
                    break;
                case 'person':
                    $oOrder->person = $oItem->value;
                    break;
                case 'phone':
                    $oOrder->phone = $oItem->value;
                    break;
                case 'mail':
                    $oOrder->mail = $oItem->value;
                    break;
                case 'postcode':
                    $oOrder->postcode = $oItem->value;
                    break;
                case 'text':
                    $oOrder->text = $oItem->value;
                    break;
                case 'tp_deliv':
                    $oOrder->type_delivery = $oItem->value;
                    break;
                case 'tp_pay':
                    $oOrder->type_payment = $oItem->value;
                    break;
                case 'is_mobile':
                    $oOrder->is_mobile = $oItem->value;
                    break;
            }
        }

        $dNow = new \DateTime('NOW');
        $oOrder->date = $dNow->format("Y-m-d H:i:s");
        $oOrder->status = Status::getIdByNew();


        // готовим токен
        $salt = "#*#BaraNeKontritsya322#*#";
        $sha512 = hash('sha512', rand(0, 1000) . $salt . $oOrder->date);
        $oOrder->token = $sha512;

        // если авторизован, сохраняем id клиента
        if (CurrentUser::isLoggedIn()) {
            $oOrder->auth = CurrentUser::getId();
        }

        $orderId = $oOrder->save();

        $bFast = $oForm->getFast();

        /** Заказ корзины */
        $oOrderCart = Cart\Api::getOrder($bFast);

        $iTotal = 0;

        // Сохранить товары заказа в БД
        foreach ($oOrderCart->getItems() as $paOrderItem) {

            $iTotal +=$paOrderItem['total'];

            $oOrderGood = new ar\GoodsRow();
            $oOrderGood->setData($paOrderItem + ['id_order' => $orderId]);
            $oOrderGood->save();
        }

        Cart\Api::clearOrder($bFast);

        $aFields = ar\Order::getModel()->getColumnSet('mail');
        $aValues = $oOrder->getData();

        // собираем данные для мыла
        $aOrderMailData = array();

        foreach ($aFields as $sField) {

            if ($sField == 'type_delivery') $aValues[$sField] = ar\TypeDelivery::getValue($aValues[$sField]);
            if ($sField == 'type_payment') $aValues[$sField] = ar\TypePayment::getValue($aValues[$sField]);

            if (isset($aValues[$sField]))
                $aOrderMailData[] = array('title' => \Yii::t('order', ar\Order::getModel()->getFiled($sField)->getTitle()),
                    'value' => $aValues[$sField]
                );
        }

        $aItems = $oOrderCart->getItems();

        foreach ($aItems as &$item){
            $sGoods = catalog\GoodsRow::get($item['id_goods']);

            $item['show_detail'] = !catalog\Card::isDetailHiddenByCard($sGoods->getExtRow()->getModel()->getName());
        }

        $oOrderCart->setItems($aItems);

        $aOptionsUser = $this->letterForSend($oOrderCart->getTotalPrice(),$oOrderCart->getItems(),$aOrderMailData,$oOrder->date,$orderId,$sha512);

        // письмо клиенту
        if ($oOrder->mail) {

            $sTitle = ModulesParams::getByName('order', 'title_user_mail');
            $sBody = ModulesParams::getByName('order', 'user_content');

            static::sendMail($oOrder->mail, $sTitle, $sBody, $aOptionsUser);
        }

        $sTitle = ModulesParams::getByName('order', 'title_adm_mail');
        $sBody = ModulesParams::getByName('order', 'adm_content');
        $aOptionsAdm = $this->letterForSend($oOrderCart->getTotalPrice(),$oOrderCart->getItems(),$aOrderMailData,$oOrder->date,$orderId,$sha512,1);


        /*Если форма купить в 1 клик, пытаемся навесить дополнительный контент результирующей*/
        if ($iTotal>0){

            if ($oOrder->status == AdmOrder\model\Status::getIdByNew()) {
                /**
                 * @var $oPaymentType AdmOrder\ar\TypePaymentRow
                 */
                $oPaymentType = AdmOrder\ar\TypePayment::find( $oOrder->type_payment );

                if ($oPaymentType && $oPaymentType->payment){
                    /**
                     * @var Payments\Payment $oPayment
                     */
                    $oPayment = Payments\Api::make( $oPaymentType->payment );

                    if ($oPayment){
                        $oPayment->setOrderId($oOrder->id);
                        $oPayment->setSum($iTotal);

                        forms\Api::$sRedirectUri = Tree::getSectionAliasPath(\Yii::$app->sections->getValue('cart'))."done?token=$oOrder->token";
                        forms\Api::$sAnswerText = \Yii::t('order', 'success_1_click');
                    }
                }

            }
        }

        // письмо админу
        static::sendMail(Site::getAdminEmail(), $sTitle, $sBody, $aOptionsAdm);

        if ($oFormEntity && CrmApi::isCrmInstall() && $oFormEntity->getFormParam('form_send_crm') )
            $oFormEntity->send2Crm($oOrder);
        else
            $this->sendToCrm($oOrder);

        return $orderId;
    }

    private function letterForSend($totalPrice,$aGood,$aOrderMailData,$date,$orderId,$sha512,$bAdmin = 0) {
        $sOut = Parser::parseTwig(
            'mail.twig',
            [
                'totalPrice'  => $totalPrice,
                'aGoods'      => $aGood,
                'items'       => $aOrderMailData,
                'date'        => $date,
                'orderId'     => $orderId,
                'webrootpath' => Site::httpDomain(),
                'bAdmin' => $bAdmin
            ],
            __DIR__ . '/templates/'
        );
        $aOptions = array('order_id' => $orderId, 'token' => $sha512, 'order_info' => $sOut);

        return $aOptions;
    }

    /**
     * Отправляет данные заказа в CMR
     * @param ar\OrderRow $oOrder
     * @return bool
     */
    private function sendToCrm( ar\OrderRow $oOrder ) {

        if ( !$oOrder )
            return false;

        $sCrmToken = SysVar::get('crm_token');
        $sCrmEmail = SysVar::get('crm_email');

        if ( !$sCrmToken or !$sCrmEmail )
            return false;

        $aGoods = AdmOrder\ar\Goods::find()->where('id_order', $oOrder->id)->asArray()->getAll();

        $crmSender = new Crm();

        $sGoodsList = '';
        foreach ($aGoods as $aItem) {
            $aGood = catalog\GoodsSelector::get($aItem['id_goods'], 1);
            $sGoodsList .= sprintf("название: %s, кол-во: %s, цена: %s\r\n", $aItem['title'], $aItem['count'], $aItem['total']);
            $crmSender->addItem(array(
                'index' => isset($aGood['fields']['article']['value']) ? $aGood['fields']['article']['value'] : '',
                'title' => $aItem['title'],
                'count' => $aItem['count'],
                'price' => $aItem['price'],
                'units' => isset($aGood['fields']['measure']['value']) ? $aGood['fields']['measure']['value'] : '',
            ));
        };

        // Собрать данные о всех полях заказа
        $aMailFields = AdmOrder\ar\Order::getModel()->getColumnSet('edit');
        foreach($aMailFields as $iKey => &$sField) {

            $sSysFieldName = AdmOrder\ar\Order::getModel()->getFiled($sField)->getTitle();
            $sFieldTitle = \Yii::t('order', $sSysFieldName);

            $aOrder = $oOrder->getData();

            if ( isset($aOrder[$sField]) and ($sFieldTitle != $sSysFieldName) ) {

                switch ($sField) {
                    case 'type_delivery':
                        $sVal = AdmOrder\ar\TypeDelivery::getValue($aOrder[$sField]);
                        break;

                    case 'type_payment':
                        $sVal = AdmOrder\ar\TypePayment::getValue($aOrder[$sField]);
                        break;

                    case 'status':
                        $sVal = ArrayHelper::getValue(AdmOrder\Service::getStatusList(), $aOrder[$sField]);
                        break;

                    default:
                        $sVal = $aOrder[$sField];
                };
                $sField = "$sFieldTitle: " . ($sVal ? : '---');

            } else
                unset($aMailFields[$iKey]);
        };

        $crmSender->setDealContent($sGoodsList . "\r\n" . implode("\r\n", $aMailFields));

        $crmSender->setToken($sCrmToken);
        $crmSender->setEmail($sCrmEmail);
        $crmSender->setDomain(\Yii::$app->request->getServerName());
        $crmSender->setDealTitle('Заявка с сайта '.\Yii::$app->request->getServerName().' от '. date('d-m-Y H:i:s'));
        $crmSender->setContactClient($oOrder->person);
        $crmSender->setContactPhone($oOrder->phone);
        $crmSender->setContactEmail($oOrder->mail);
        //$crmSender->setContactMobile('');
        $crmSender->setCanapeuuid(Request::getStr('_canapeuuid'));
        return $crmSender->sendMail();

    }

}
