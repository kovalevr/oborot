<?php
/**
 * User: Max
 * Date: 25.06.14
 */

namespace skewer\build\Adm\Order\ar;
use skewer\base\orm;
use skewer\base\ft;
use skewer\build\Adm\Params;

class TypeDeliveryRow extends orm\ActiveRecord {

    public $id = 'NULL';
    public $title = '';

    function __construct() {
        $this->setTableName( 'orders_delivery' );
        $this->setPrimaryKey( 'id' );
    }
}