<?php
/**
 * User: Max
 * Date: 25.06.14
 */

namespace skewer\build\Adm\Order\ar;
use skewer\base\orm;
use skewer\base\ft;
use skewer\build\Adm\Params;

class TypePaymentRow extends orm\ActiveRecord {

    public $id = 'NULL';
    public $title = '';
    public $payment = '';

    function __construct() {
        $this->setTableName( 'orders_payment' );
        $this->setPrimaryKey( 'id' );
    }
}