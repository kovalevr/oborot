<?php

namespace skewer\build\Adm\Gallery\view;

use skewer\components\ext\view\FormView;
use skewer\build\Page;
use skewer\build\Adm;

class SettingsInSection extends FormView{

    public $AlbumDetail;

    function build() {

        /** @var Adm\Gallery\Module $oModule */
        $oModule = $this->_module;

        $this->_form
            ->fieldSelect('AlbumDetail', \Yii::t('gallery', 'detailTemplate'), Page\Gallery\Module::getDetailTemplates(), ['onUpdateAction' => 'updateSettingsInSection'], false)
            ->fieldCheck('openAlbum', \Yii::t('gallery', 'openAlbum'));

        $aData = [
            'openAlbum'   => $oModule->getParamValue('openAlbum')
        ];

        $aDataByAjaxUpdated = ['AlbumDetail' => $this->AlbumDetail];

        if ( $this->AlbumDetail == Page\Gallery\Module::ALBUM_DETAIL_TPL_INLINE ){
            $this->_form
                ->fieldInt('justifiedGalleryOption_rowHeight', \Yii::t('gallery', 'justifiedGalleryOption_rowHeight'), ['minValue' => 100])
                ->fieldInt('justifiedGalleryOption_maxRowHeight', \Yii::t('gallery', 'justifiedGalleryOption_maxRowHeight'), ['minValue' => 100])
            ;

            $aConfig = Adm\Gallery\Api::getConfigJustifiedGallery( $oModule->sectionId(), true );
            $aData += $aConfig;

        }

        $aData = array_merge($aData, $aDataByAjaxUpdated);

        $this->_form
            ->setValue($aData)
            ->buttonSave('saveSettings')
            ->buttonCancel('getAlbums')
        ;

    }

}