<?php
namespace skewer\build\Adm\Gallery;

use skewer\base\section\Tree;
use skewer\components\gallery\Album;
use skewer\components\gallery\models\Albums;
use skewer\components\seo\SeoPrototype;

class Seo extends SeoPrototype{

    public static function getGroup(){
        return 'gallery';
    }

    /**
     * @inheritdoc
     */
    public static function getAlias(){
        return 'galleryDetail';
    }

    /**
     * @inheritdoc
     */
    public function extractReplaceLabels( $aParams ){

        $aDataEntity = $this->getDataEntity();

        $aData = array(
            'label_gallery_title_upper'  => $aDataEntity['title'],
            'label_gallery_title_lower' => mb_strtolower( $aDataEntity['title'] )
        );

        if ( isset($aParams['label_number_photo']) )
            $aData['label_number_photo'] = $aParams['label_number_photo'];

        return $aData;
    }


    public function loadDataEntity(){
        if ($oAlbum = Album::getById($this->iEntityId))
            $this->aDataEntity = $oAlbum->getAttributes();
    }

    /**
     * @inheritdoc
     */
    protected function getSearchClassName(){
        return Search::className();
    }

    /**
     * @inheritdoc
     */
    public function getRecordWithinEntityByPosition($iPosition){

        $aResult = Albums::find()
            ->where(['section_id' => $this->iSectionId])
            ->orderBy('priority DESC')
            ->limit(1)->offset($iPosition)
            ->all();

        if (!isset($aResult[0]))
            return false;

        /** @var Albums $oCurrentRecord */
        $oCurrentRecord = $aResult[0];
        $this->setDataEntity($oCurrentRecord->getAttributes());

        $aRow = array_merge($oCurrentRecord->getAttributes(),[
            'url' => \Yii::$app->router->rewriteURL($oCurrentRecord->getUrl()),
            'seo' => $this->parseSeoData( ['sectionId' => $this->iSectionId] )
        ]);

        return $aRow;

    }

    /**
     * @inheritdoc
     */
    public function doExistRecord($sPath){

        $sTail = '';
        $iSectionId = Tree::getSectionByPath($sPath, $sTail);
        $sTail = trim($sTail, '/');

        return ($aRecord = Album::getByAlias($sTail, $iSectionId))
            ? $aRecord['id']
            : false
        ;

    }

    /**
     * @inheritdoc
     */
    public function doSupportAltTitle(){
        return true;
    }

}