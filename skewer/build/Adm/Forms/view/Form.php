<?php

namespace skewer\build\Adm\Forms\view;

use skewer\components\ext\view\FormView;
use skewer\build\Adm\Forms;

class Form extends FormView
{
    public $aTemplateForms;
    public $formInfo;
    public $formTitles;

    /**
     * Выполняет сборку интерфейса
     * @return void
     */
    function build() {
        $i = 0;
        foreach ($this->formInfo as $sGroup => $iFormId) {
            $this->_form
                ->fieldSelect("form_$sGroup", rtrim(\Yii::t('forms', 'select_form') . " {$this->formTitles[$i]}"), $this->aTemplateForms, [], false)
                ->setValue(["form_$sGroup" => $iFormId]);
            ++$i;
        }

        $this->_form->buttonSave('linkFormToSection');
    }
}