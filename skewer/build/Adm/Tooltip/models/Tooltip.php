<?php

namespace skewer\build\Adm\Tooltip\models;

use skewer\base\section\Tree;
use skewer\build\Tool\Rss;
use skewer\components\seo\Api;
use skewer\components\seo\Service;
use skewer\helpers\ImageResize;
use skewer\helpers\Transliterate;
use Yii;
use yii\data\ActiveDataProvider;
use \yii\base\ModelEvent;
use skewer\build\Adm\News\Search;
use yii\helpers\ArrayHelper;

/**
 * This is the model class for table "news".
 *
 * @property integer $id
 * @property string $name
 * @property string $text
 */
class Tooltip extends \yii\db\ActiveRecord {

    /**
     * @inheritdoc
     */
    public static function tableName() {
        return 'tooltips';
    }

    /**
     * @inheritdoc
     */
    public function rules() {
        return [
            [['id', 'name', 'text'], 'required'],
            [['name', 'text'], 'string'],
            [['text'], 'string', 'max' => 255],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels() {
        return [
            'id'                 => 'id',
            'name'         => 'name',
            'text'     => 'text',
        ];
    }

    public static function getDefaultValues(){
        $aValues = [
            'id'=>0,
            'name'=>'',
            'text'=>''
        ];

        return $aValues;
    }
}
