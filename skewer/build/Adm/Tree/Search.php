<?php


namespace skewer\build\Adm\Tree;


use skewer\base\orm\Query;
use skewer\build\Page\Text\Api as TextApi;
use skewer\components\search\models\SearchIndex;
use skewer\components\search\Prototype;
use skewer\base\section\Page;
use skewer\base\section\Tree;
use skewer\base\section\Parameters;
use skewer\components\seo\Service;
use skewer\build\Page\Main;

class Search extends Prototype {

    /**
     * Флаг необходимости рекурсивного сброса данных по дереву разделов
     * @var bool
     */
    private $bRecursiveReset = false;

    /**
     * отдает имя идентификатора ресурса для работы с поисковым индексом
     * @return string
     */
    public function getName() {
        return 'Page';
    }

    /**
     * @inheritDoc
     */
    public function getModuleTitle() {
        return \Yii::t('page', 'tab_name');
    }

    /**
     * @inheritdoc
     */
    protected function update(SearchIndex $oSearchRow) {

        $oSearchRow->class_name = $this->getName();

        if (!$oSearchRow->object_id)
            return false;

        if ( $this->bRecursiveReset ){
            $this->resetSectionRecursive($oSearchRow->object_id);
            Service::updateSearchIndex();
        }

        $oSection = Tree::getSection( $oSearchRow->object_id );
        if ($oSection->parent == \Yii::$app->sections->templates()){
            $this->resetSectionAndEntitiesByTemplate($oSearchRow->object_id);
            Service::updateSearchIndex();
        }

        /** Исключаем корневые разделы языковых версий */
        if (in_array($oSearchRow->object_id, \Yii::$app->sections->getValues(Page::LANG_ROOT)))
            return false;

        // отсечь все ветви дерева, кроме основной (3)
        if ( !in_array(\Yii::$app->sections->root(), Tree::getSectionParents($oSearchRow->object_id)) )
            return false;

        $sText = TextApi::getTextContentFromZone($oSearchRow->object_id);

        /** Список шаблонов LP */
        static $aLPTplSections = null;
        if ( ($aLPTplSections === null) and ($iLPTpl = \Yii::$app->sections->landingPageTpl()) )
            $aLPTplSections = Tree::getAllSubsection(\Yii::$app->sections->landingPageTpl());
        $aLPTplSections or $aLPTplSections = [];

        // Обработать LP разделы
        if ( $aLPTplSections ) {
            $iTemplateId = (int)Parameters::getValByName($oSearchRow->object_id, Parameters::settings, Parameters::template, true);
            // Убрать подразделы LP из карты сайта и поиска
            if ($iTemplateId and isset($aLPTplSections[$iTemplateId]))
                return false;
            // и сами шаблон lp тоже
            if (isset($aLPTplSections[$oSearchRow->object_id]))
                return false;
        }

        $sTitle = Tree::getSectionsTitle( $oSearchRow->object_id );

        // проверка существования раздела и реального url у него
        if ( !$oSection || !$oSection->hasRealUrl() )
            return false;

        /** В случае если раздел виден и имеет ссылку, т.е. есть редирект куда-то он не должен попадать в поиск. */
        if ( $oSection->link )
            return false;

        $oSearchRow->language = Parameters::getLanguage($oSearchRow->object_id);
        
        $oSearchRow->href = \Yii::$app->router->rewriteURL('['.$oSearchRow->object_id.']');

        $oSearchRow->section_id = $oSearchRow->object_id;
        $oSearchRow->search_text = $this->stripTags($sText);
        $oSearchRow->search_title = $sTitle;
        $oSearchRow->use_in_search = true;
        $oSearchRow->use_in_sitemap = true;
        $oSearchRow->status = 1;
        $oSearchRow->modify_date = $oSection->last_modified_date;

        $oSeoComponent = new Main\Seo($oSection->id, $oSection->id, $oSection->attributes + ['text' => $sText]);
        $this->fillSearchRowSeoData( $oSearchRow, $oSeoComponent );

        $oSearchRow->save();
        return true;
    }


    /**
     * Сброс индекса разделов, созданных на основе шаблона $iTplId
     * и записей сущностей, принадлежащих этим разделам.
     * @param $iTplId - id шаблона
     */
    public function resetSectionAndEntitiesByTemplate($iTplId){
        $aSections = Tree::getSubSectionsByTemplate($iTplId);

        if ($aSections){
            SearchIndex::updateAll(['status'=> 0],['section_id' => $aSections]);
        }

    }

    /**
     * @param $id
     * рекурсивный сброс индекса раздела и подчиненных ему
     */
    private function resetSectionRecursive($id){

        // сбросить статус для сущностей, привязанных к этому разделу
        SearchIndex::updateAll(['status'=> 0],['section_id'=> $id]);


        // найти подразделы
        $section = Tree::getSection( $id );
        $aSubSections = $section->getSubSections();

        // выполнить сброс для подразделов
        foreach ( $aSubSections as $item ) {
            $this->resetToId( $item->id );
            $this->resetSectionRecursive( $item->id );
        }

    }

    /**
     *  воссоздает полный список пустых записей для сущности, отдает количество добавленных
     */
    public function restore() {
        $sql = "INSERT INTO search_index(`status`,`class_name`,`object_id`)  SELECT '0','{$this->getName()}',id  FROM tree_section WHERE parent>3";
        Query::SQL($sql);

    }

    /**
     * Устанавливает флаг рекурсивного сброса поискового индекса по дереву разделов
     */
    public function setRecursiveResetFlag() {
        $this->bRecursiveReset = true;
    }

}