<?php

namespace skewer\build\Design\Settings;

use skewer\base\section\Parameters;
use skewer\build\Cms;
use skewer\components\design\Template;
use skewer\build\Design\Zones;

class Module extends Cms\Tabs\ModulePrototype {

    /**
     * Состояние начальное
     */
    protected function actionInit() {
        $this->render(new view\Index());
    }

    /**
     * Сохранение настроек
     */
    protected function actionSaveSettings(){

        \Yii::$app->session->set('Settings.DeleteParams',$this->getInDataVal('DeleteParams'));

        $this->addMessage('Сохранено');
        $this->actionInit();

        $this->fireJSEvent('reload_all');
    }

    /**
     * Форма настроек
     */
    protected function actionSettingsForm()
    {
        $this->render(new view\Settings([
            'bDeleteParams' => (bool)\Yii::$app->session->get('Settings.DeleteParams')
        ]));

    }

    protected function actionHeadTplForm() {

        $sTpl =  Parameters::getShowValByName(
            \Yii::$app->sections->root(),
            Zones\Api::layoutGroupName,
            'head_tpl'
        );

        $this->render(new view\Head([
            'aTplList' => Template::getTplList('head'),
            'sCurrentTpl' => $sTpl
        ]));

    }

    protected function actionChangeHead() {

        $sName = $this->getInDataVal('tpl');
        $sUpdContent = $this->getInDataVal('setContent');

        Template::change('head', $sName, $sUpdContent);

        $this->fireJSEvent('reload_display_form');

        $this->actionInit();

    }

    protected function actionFooterTplForm() {

        $sTpl =  Parameters::getShowValByName(
            \Yii::$app->sections->root(),
            Zones\Api::layoutGroupName,
            'footer_tpl'
        );

        $this->render(new view\Footer([
            'aTplList' => Template::getTplList('footer'),
            'sCurrentTpl' => $sTpl
        ]));

    }

    protected function actionChangeFooter() {

        $sName = $this->getInDataVal('tpl');
        $sUpdContent = $this->getInDataVal('setContent');

        Template::change('footer', $sName, $sUpdContent);

        $this->fireJSEvent('reload_display_form');

        $this->actionInit();

    }

    protected function actionFormTplForm() {

        $sTpl =  Parameters::getShowValByName(
            \Yii::$app->sections->root(),
            Zones\Api::layoutGroupName,
            'form_tpl'
        );

        $this->render(new view\Form([
            'aTplList' => Template::getTplList('form'),
            'sCurrentTpl' => $sTpl
        ]));

    }

    protected function actionChangeForm() {

        $sName = $this->getInDataVal('tpl');
        $sUpdContent = $this->getInDataVal('setContent');

        Template::change('form', $sName, $sUpdContent);

        $this->fireJSEvent('reload_display_form');

        $this->actionInit();

    }

}
