<?php

use skewer\base\site\Layer;

$aConfig['name']     = 'Settings';
$aConfig['title']    = 'Настройки дизайна';
$aConfig['version']  = '1.0';
$aConfig['description']  = 'Настройки дизайна';
$aConfig['revision'] = '0001';
$aConfig['layer']     = Layer::DESIGN;
$aConfig['languageCategory']     = 'design';

return $aConfig;
