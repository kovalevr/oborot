<?php
/**
 * Created by PhpStorm.
 * User: Александр
 * Date: 17.03.2017
 * Time: 18:01
 */

namespace skewer\build\Design\Settings\view;

use skewer\components\ext\view\FormView;

class Index extends FormView {

    /**
     * Выполняет сборку интерфейса
     * @return void
     */
    function build() {

        $this->_form
            ->button('settingsForm', 'Настройки')
            ->button('HeadTplForm', 'Шаблон шапки')
            ->button('FooterTplForm', 'Шаблон подвала')
          //  ->button('FormTplForm', 'Шаблон форм') //Эта кнопка появится в следующей версии
        ;

    }
}
