<?php

namespace skewer\build\Design\Settings\view;

use skewer\components\ext\view\FormView;

class Form extends FormView {

    public $aTplList;
    public $sCurrentTpl;

    /**
     * Выполняет сборку интерфейса
     * @return void
     */
    function build() {

        $this->_form
            ->fieldSelect('tpl', 'Шаблон', $this->aTplList, [], false)
            ->fieldCheck('setContent', 'Сбросить контент', [
                'subtext'=> 'Уже заданный контент некоторых блоков будет заменен стандартным'
            ])
            ->buttonSave('changeForm')
            ->buttonCancel()
        ;

        $this->_form->setValue([
            'tpl' => $this->sCurrentTpl
        ]);

    }
}
