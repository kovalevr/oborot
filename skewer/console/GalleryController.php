<?php

namespace app\skewer\console;


use skewer\base\log\Logger;
use skewer\components\gallery\models\Albums;
use skewer\components\gallery\models\Formats;
use skewer\components\gallery\models\Photos;
use skewer\components\gallery\models\Profiles;
use skewer\components\gallery\Photo;
use skewer\components\gallery\Profile;
use skewer\helpers\Files;
use yii\helpers\ArrayHelper;

class GalleryController extends Prototype{


    /**
     * Преобразует изображения всех альбомов профиля в заданный формат
     * @return bool
     */
    public function actionRecrop(){

        $this->br();

        $iProfileId = $this->prompt(
            'Введите id профиля:',
            ['required' => true, 'validator' => function($input, &$error){

                if ( Profile::getById( $input ) === false ){
                    $error = "Профиль [$input] не существует";
                    return false;
                }

                return true;

            }]
        );


        $aAlbums = Albums::find()
            ->where(['profile_id' => $iProfileId])
            ->asArray()
            ->all();

        $aAlbumsId = ArrayHelper::getColumn($aAlbums, 'id', []);

        if ( ($aPhotos = Photo::getFromAlbum($aAlbumsId, false)) === false ){
            $this->stderr("Альбомы профиля [$iProfileId] не имеют изображений\r\n");
            return false;
        }

        $aFormats = Formats::find()
            ->where(['profile_id' => $iProfileId])
            ->asArray()
            ->all();

        $aNameFormats = ArrayHelper::getColumn($aFormats, 'name', []);

        $this->stdout( sprintf("Доступные форматы: %s\r\n", implode(', ',$aNameFormats)) );

        /**
         * @var callable Проверяет есть ли формат $input в списке форматов $aNameFormats
         * @param string $sFormatName - имя формата
         * @param string $sError     - сообщение об ошибке
         * @return bool
         *
         */
        $funcValidateFormats = function($sFormatName, &$sError) use ($aNameFormats,$iProfileId){
            if ( !in_array($sFormatName, $aNameFormats) ){
                $sError = sprintf("Формат '%s' не найден в списке форматов профиля %d", $sFormatName, $iProfileId);
                return false;
            }
            return true;
        };

        $sTargetFormat = $this->prompt(
            'Введите имя целевого формата:',
            ['required' => true, 'validator' => $funcValidateFormats]
        );

        $aErrors = [];
        $iTotalPhotos = sizeof($aPhotos);

        $mProfileId = array(
            'crop'       => array( $sTargetFormat => array() ),
            'iProfileId' => $iProfileId
        );

        $this->recropPhotos($aPhotos, $mProfileId,

            function($index, $photo, $ArPhoto) use ($iTotalPhotos, $sTargetFormat){

                /** @var Photos $ArPhoto */
                $aDataPhoto = $ArPhoto->getPictures();

                // Удаляем старое изображение
                if ( isset($aDataPhoto[$sTargetFormat]['file']) ){
                    $sOldFilePath = WEBPATH . $aDataPhoto[$sTargetFormat]['file'];
                    @unlink($sOldFilePath);
                }

                $aDataPhoto = ArrayHelper::merge($aDataPhoto, $photo);
                $ArPhoto->images_data = json_encode($aDataPhoto);
                $ArPhoto->save();

                $this->stdout("\rЗагруженно изображений  {$index}/{$iTotalPhotos} ");
            },

            function($error) use (&$aErrors){
                $aErrors[] = $error;
            }

        );


        $this->br();

        if ( $aErrors ){

            $this->stdout("Ошибки:\r\n");

            foreach ($aErrors as $sError)
                $this->stderr("$sError\r\n");

        }


        $this->br();

    }


    /**
     * Итеративное создание изображения нового формата
     * @param Photos[] $aPhotos - изображения
     * @param $mProfileId - настройки целевого формата
     * @param callable $fSuccessCallBack - функция, запускаемая при успешном создании изображения
     * @param callable $fErrorCallBack   - функция, запускаемая при не успешном создании изображения
     */
    protected function recropPhotos(array $aPhotos, $mProfileId, $fSuccessCallBack = null, $fErrorCallBack = null ){

        Files::init(FILEPATH, PRIVATE_FILEPATH);

        $iLoadedPhotos = 0;

        foreach ($aPhotos as &$oPhoto) {

            $sError = $sSource = '';

            $sSource = ( $oPhoto->source && file_exists(WEBPATH . $oPhoto->source) )
                ? WEBPATH . $oPhoto->source
                : $this->getMaxImage($oPhoto->getPictures());

            if (!$sSource){

                if ( is_callable($fErrorCallBack) ){
                    $sError = sprintf("Не найдено исходное изображение [id=%d]", $oPhoto->id);
                    call_user_func_array($fErrorCallBack, [ 'error' => $sError]);
                }

            } else {

                $aLoadPhotos = Photo::processImage($sSource, $mProfileId, $oPhoto->album_id, false, false, $sError );

                if ($aLoadPhotos === false){

                    if ( is_callable($fErrorCallBack) ){
                        $sError = sprintf('%s %s', $oPhoto->id, $sError);
                        call_user_func_array($fErrorCallBack, [ 'error' => $sError]);
                    }

                } else{

                    $iLoadedPhotos++;

                    if ( is_callable($fSuccessCallBack) )
                        call_user_func_array($fSuccessCallBack, [ 'index' => $iLoadedPhotos, 'photo' => $aLoadPhotos, 'ArPhoto' => $oPhoto] );

                }

            }


        }

    }

    /**
     * Список доступных профилей
     */
    public function actionListProfiles(){

        $this->br();

        $aProfiles = Profiles::find()->all();

        /** @var Profiles $oProfile */
        foreach ($aProfiles as $oProfile) {
            $sName = sprintf("%d - %s [ %s ]\r\n", $oProfile->id, $oProfile->title, $oProfile->alias);
            $this->stdout($sName);
        }

        $this->br();

    }

    /**
     * Вернет полный путь к изображению максимального размера
     * @param $aFormats - список форматов
     * @return string
     */
    protected function getMaxImage($aFormats){
        $sSource = '';
        $aSizeFormat = [];
        foreach ($aFormats as $sFormatName => $aPhoto) {

            if (($aImageSize = @getimagesize(WEBPATH . $aPhoto['file'])) !== false)
                $aSizeFormat[$sFormatName] = $aImageSize[0] * $aImageSize[1];
        }

        arsort($aSizeFormat);

        foreach ($aSizeFormat as $sFormat => $Value) {
            $sFilePath = WEBPATH . $aFormats[$sFormat]['file'];
            if (file_exists($sFilePath)) {
                $sSource = $sFilePath;
                break;
            }
        }

        return $sSource;
    }


}