<?php
namespace skewer\libs\owlcarousel;

use yii\web\AssetBundle;
use yii\web\View;

class Asset extends AssetBundle{

    public $sourcePath = '@skewer/libs/owlcarousel/web/';

    public $css = [
        'owl.carousel.css'
    ];

    public $js = [
        'owl.carousel.js'
    ];

    public $jsOptions = [
        'position'=>View::POS_HEAD
    ];


}