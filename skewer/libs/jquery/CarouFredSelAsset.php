<?php
namespace skewer\libs\jquery;

use yii\web\AssetBundle;

/**
 * Библиотека карусели для клиентской части сайта
 */
class CarouFredSelAsset extends AssetBundle{

    public $sourcePath = '@skewer/libs/jquery/web/';

    public $js = [
        'jquery.carouFredSel-6.2.1-packed.js',
    ];

    public $depends = [
        'skewer\libs\jquery\Asset'
    ];

}