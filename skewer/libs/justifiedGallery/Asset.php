<?php
namespace skewer\libs\justifiedGallery;

use yii\web\AssetBundle;
use yii\web\View;

class Asset extends AssetBundle{

    public $sourcePath = '@skewer/libs/justifiedGallery/web/';

    public $css = [
        'justifiedGallery.min.css'
    ];

    public $js = [
        'jquery.justifiedGallery.min.js'
    ];

    public $jsOptions = [
        'position'=>View::POS_HEAD
    ];

}